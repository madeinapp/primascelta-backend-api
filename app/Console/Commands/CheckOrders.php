<?php

namespace App\Console\Commands;

use App\Notifications\AlertNotConfirmed;
use App\Notifications\AlertConfirmed;
use App\Notifications\AlertConfirmedWithReserve;
use App\Notifications\AlertConfirmedWithReserveUser;
use App\Notifications\OrderCancelledAuto;
use App\Repositories\OrderRepository;
use App\Repositories\UserRepository;
use Illuminate\Console\Command;
use Log;
use Carbon\Carbon;

class CheckOrders extends Command
{

    private $orderRepository,
            $userRepository;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:orders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Esegue i controlli sugli Ordini';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(OrderRepository $orderRepository, UserRepository $userRepository)
    {
        parent::__construct();

        $this->orderRepository = $orderRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // Avvisi per gli ordini ancora da confermare con data di consegna trascorsa da meno di 24 ore
        $orders = $this->orderRepository->getNotConfirmedExpiredLess24Hours();
        foreach($orders as $order):
            if ( env('APP_ENV') === 'production' ):
                if( in_array($order->shop->status, ['active', 'invisible']) ):
                    $order->shop->user->notify(new AlertNotConfirmed($order));
                endif;
            else:
                $user = \App\Models\User::where('email', 'flavio.spugna@gmail.com')->first();
                $user->notify(new AlertNotConfirmed($order));
            endif;
        endforeach;


        // Cancellazione automatica per gli ordini ancora da confermare con data di consegna trascorsa da 24 ore
        $orders = $this->orderRepository->getNotConfirmedExpiredMore24Hours();
        foreach($orders as $order):
            $order = $this->orderRepository->update(['order_status_id' => 7], $order->id);
            if (env('APP_ENV') === 'production'):
                if( in_array($order->shop->status, ['active', 'invisible']) ):
                    $order->shop->user->notify(new OrderCancelledAuto($order));
                endif;

                if( in_array($order->user->status, ['active', 'invisible']) ):
                    $order->user->notify(new OrderCancelledAuto($order));
                endif;
            else:
                $user = \App\Models\User::where('email', 'flavio.spugna@gmail.com')->first();
                $user->notify(new AlertNotConfirmed($order));
            endif;
        endforeach;


        // Avvisi per gli ordini confermati
        // con la data di spedizione trascorsa da meno di 24 ore
        /*
        $orders = $this->orderRepository->getConfirmedExpired();
        foreach($orders as $order):
            if (env('APP_ENV') === 'production'):
                $order->shop->user->notify(new AlertConfirmed($order));
            else:
                $user = \App\Models\User::where('email', 'flavio.spugna@gmail.com')->first();
                $user->notify(new AlertConfirmed($order));
            endif;
        endforeach;
        */


        // Cancellazione automatica degli ordini confermati con riserva e non ancora modificati o annullati
        $orders = $this->orderRepository->getConfirmedWithReserveExpired();
        foreach($orders as $order):

            $order = $this->orderRepository->update(['order_status_id' => 7], $order->id);
            if( $order ):
                $user = $this->userRepository->find($order->user_id);

                if( $user ):
                    $points = $user->trust_points - 3;

                    if( $points < 0 ) $points = 0;

                    $user = $this->userRepository->update([
                        'trust_points' => $points,
                    ], $user->id);

                    $order = $this->orderRepository->update([
                        'flag_trust_points' => 'subtract'
                    ], $order->id);

                    $order->shop;

                    if (env('APP_ENV') === 'production'):
                        if( in_array($order->shop->status, ['active', 'invisible']) ):
                            $order->shop->user->notify(new AlertConfirmedWithReserve($order));
                        endif;

                        if( in_array($order->user->status, ['active', 'invisible']) ):
                            $order->user->notify(new AlertConfirmedWithReserveUser($order));
                        endif;
                    else:
                        $user = \App\Models\User::where('email', 'flavio.spugna@gmail.com')->first();
                        $user->notify(new AlertConfirmedWithReserve($order));
                    endif;
                endif;
            endif;
        endforeach;
    }
}
