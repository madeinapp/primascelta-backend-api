<?php

namespace App\Console\Commands;

use App\Notifications\AlertNotConfirmed;
use App\Notifications\AlertConfirmed;
use App\Notifications\AlertConfirmedWithReserve;
use App\Notifications\AlertConfirmedWithReserveUser;
use App\Notifications\OrderCancelledAuto;
use App\Repositories\OrderRepository;
use App\Repositories\UserRepository;
use Illuminate\Console\Command;
use Log;
use Carbon\Carbon;

class CheckOrdersOnce extends Command
{

    private $orderRepository,
            $userRepository;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:orders_once';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Esegue i controlli sugli Ordini ona volta al giorno';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(OrderRepository $orderRepository, UserRepository $userRepository)
    {
        parent::__construct();

        $this->orderRepository = $orderRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // Avvisi per gli ordini onfermati
        // con la data di spedizione trascorsa da più di 24 ore
        $orders = $this->orderRepository->getConfirmedExpiredMore24Hours();
        foreach($orders as $order):
            if (env('APP_ENV') === 'production'):
                $order->shop->user->notify(new AlertConfirmed($order));
            else:
                $user = \App\Models\User::where('email', 'flavio.spugna@gmail.com')->first();
                $user->notify(new AlertConfirmed($order));
            endif;
        endforeach;

    }
}
