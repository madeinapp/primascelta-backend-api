<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Repositories\UserRepository;
use App\Notifications\AlertSubscription;

class CheckSubscriptions extends Command
{
    private $userRepository;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:subscriptions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Esegue i controlli sugli abbonamenti in scadenza';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(UserRepository $userRepository)
    {
        parent::__construct();
        $this->userRepository = $userRepository;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $users = $this->userRepository->subscriptionExpiring(7);

        foreach($users as $user):
            if (env('APP_ENV') === 'production'):
                $user->notify(new AlertSubscription(7));
            else:
                $user = \App\Models\User::where('email', 'flavio.spugna@gmail.com')->first();
                $user->notify(new AlertSubscription(7));
            endif;
        endforeach;

        $users = $this->userRepository->subscriptionExpiring(1);

        foreach($users as $user):
            if (env('APP_ENV') === 'production'):
                $user->notify(new AlertSubscription(1));
            else:
                $user = \App\Models\User::where('email', 'flavio.spugna@gmail.com')->first();
                $user->notify(new AlertSubscription(1));
            endif;
        endforeach;
    }
}
