<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User;

class DeletePendingUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'delete:pending_users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Elimina gli utenti che non hanno validato l'Otp";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        User::whereIn('status', ['pending','inactive'])
            ->whereNotNull('otp')
            ->whereRaw('created_at < NOW() - interval 5 minute')
            ->delete();

        return 0;
    }
}
