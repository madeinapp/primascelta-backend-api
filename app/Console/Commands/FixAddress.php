<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\Shop;
use Log;

class FixAddress extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fix:address';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Aggiorna gli indirizzi dei negozi';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $shops = Shop::whereIn('status', ['pending', 'active', 'invisible'])
                     ->whereNull('prov')
                     ->get();

        foreach($shops as $shop):
            $res = getAddressComponents($shop->address);

            $prov = "";

            if($res){
                foreach($res['address_components'] as $val){
                    if( isset($val["types"]) && $val["types"][0] === "administrative_area_level_2" ){
                        $prov = $val["short_name"];
                        Log::info("prov: " . $prov);
                    }
                }

                $shop->prov = $prov;
                $shop->save();

            }
        endforeach;

        return 0;
    }
}
