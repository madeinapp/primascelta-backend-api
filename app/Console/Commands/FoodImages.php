<?php

namespace App\Console\Commands;

use App\Repositories\FoodCategoryRepository;
use App\Repositories\FoodRepository;
use Exception;
use Illuminate\Console\Command;
use Image;
use Log;

class FoodImages extends Command
{

    private $foodRepository;
    private $foodCategoryRepository;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'foods:images {folder?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fix foods images sizes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(FoodRepository $foodRepository, FoodCategoryRepository $foodCategoryRepository)
    {
        parent::__construct();

        $this->foodRepository         = $foodRepository;
        $this->foodCategoryRepository = $foodCategoryRepository;
    }


    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $folders = [];
        $folder = $this->argument('folder');
        $folders[] = $folder;

        if( empty($folders) ):

            $folders=[
                "CARNE",
                "DA_BAR",
                "DA_BERE",
                "DA_DISPENSA",
                "DA_FRIGO",
                "DOLCE",
                "FRUTTA",
                "GELATO",
                "PANE",
                "PASTA_FRESCA",
                "PESCE",
                "PIATTO_PREPARATO",
                "PIZZA",
                "VERDURA",
            ];

        endif;

        foreach ($folders as $folder) {

            $foodCategory = $this->foodCategoryRepository->all(['name' => $folder]);

            if( empty($foodCategory) || count($foodCategory) !== 1 ):
                echo "Categoria non trovata o errata";
                return false;
            endif;

            $foods = $this->foodRepository->byCategory($foodCategory[0]->id);

            foreach($foods as $food):

                $path = public_path('/storage/images/foods/'.NormalizeString($food->foodCategory->name).'/'.NormalizeString($food->name).'.jpg');

                dump($path);

                if( file_exists($path) ):

                    $res = $this->foodRepository->update(['image' => basename($path)], $food->id);

                    if( $res ):

                        $image = Image::make($path)->orientate();
                        if( $image ):
                            cropImages($image, $path, 'food');
                        endif;

                    endif;

                endif;

                /*
                dd("OK");

                if ($handle = opendir($basePath)) {

                    while (false !== ($entry = readdir($handle))) {

                        if ($entry != "." && $entry != "..") {

                            dump($entry);

                            $path = public_path('/storage/images/foods/'.NormalizeString($folder).'/'.$entry);

                            if( file_exists($path) &&
                                (
                                    !file_exists(getResizedImage($path, '100x100')) ||
                                    !file_exists(getResizedImage($path, '230x154')) ||
                                    !file_exists(getResizedImage($path, '512x340')) ||
                                    !file_exists(getResizedImage($path, '646x430'))
                                )
                            ):

                                try{
                                    $image = Image::make($path)->orientate();
                                    if( $image ):
                                        cropImages($image, $path, 'food');
                                    endif;
                                }catch(Exception $e){
                                }

                            endif;

                        }

                    }
                }
                */

            endforeach;
        }
    }
}
