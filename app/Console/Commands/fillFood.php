<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use App\Models\FoodCategory;
use App\Models\Food;
use Image;
use DB;
use Log;

class fillFood extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fillFood';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		//Get unit of Measures
		$obj_uom = DB::table('unit_of_measures')->get();
		$arr_uom = [];
		foreach($obj_uom as $uom){
			$arr_uom[strtolower($uom->name)] = $uom->id;
        }


		//Start to fill table
		//Truncate table
        //Food::truncate();

                /*Code to clean directories foods*/
	            /*
                $arr_dir[] = '/var/www/primascelta/storage/app/public/images/foods/CARNE/';
                $arr_dir[] = '/var/www/primascelta/storage/app/public/images/foods/DA_BAR/';
                $arr_dir[] = '/var/www/primascelta/storage/app/public/images/foods/DA_BERE/';
                $arr_dir[] = '/var/www/primascelta/storage/app/public/images/foods/DA_DISPENSA/';
                $arr_dir[] = '/var/www/primascelta/storage/app/public/images/foods/DA_FRIGO/';
                $arr_dir[] = '/var/www/primascelta/storage/app/public/images/foods/DOLCE/';
                $arr_dir[] = '/var/www/primascelta/storage/app/public/images/foods/FRUTTA/';
                $arr_dir[] = '/var/www/primascelta/storage/app/public/images/foods/GELATO/';
                $arr_dir[] = '/var/www/primascelta/storage/app/public/images/foods/PANE/';
                $arr_dir[] = '/var/www/primascelta/storage/app/public/images/foods/PASTA_FRESCA/';
                $arr_dir[] = '/var/www/primascelta/storage/app/public/images/foods/PESCE/';
                $arr_dir[] = '/var/www/primascelta/storage/app/public/images/foods/PIATTO_PREPARATO/';
                $arr_dir[] = '/var/www/primascelta/storage/app/public/images/foods/PIZZA/';
                $arr_dir[] = '/var/www/primascelta/storage/app/public/images/foods/VERDURA/';
				foreach($arr_dir as $dir){
					echo $dir."\n";
					$files1 = scandir($dir);
					$count = 0;
					//print_r($files1);exit;
					foreach($files1 as $file){
						if($file == '.' || $file == '..' ) continue;
						//if($count > 0){
							//echo $file."\n";
							$new_filename = NormalizeString($file);
							//echo $new_filename."\n";
							//echo "\n";
							rename($dir.$file, trim($dir.str_replace('jpg','.jpg', $new_filename), "'"));
						//}
						//$count++;
					}
				}
                exit;
	    */
        $arr_foods_from_file = Storage::disk('local_data')->files('foods/');

        $arr_images_not_found = [];
        $count_food_with_no_image = 0;

		if(!empty($arr_foods_from_file)){
			//Get food categories from table
			$arr_food_categories = [];
			$obj_food_categories = FoodCategory::all();
			foreach($obj_food_categories as $food_category){
				$arr_food_categories[strtoupper($food_category->name)] = $food_category->id;
            }

			//Get food from table
			$arr_foods = [];
			$obj_foods = Food::all();
			foreach($obj_foods as $food){
				$arr_foods[strtoupper($food->name)] = $food->id;
            }

			foreach($arr_foods_from_file as $file){
			//if ($file == "foods/db_piatto_preparato.csv"){
				$food_file = storage_path('data/').$file;

				$h = fopen($food_file, 'r');
				$count = 0;
				while($data = fgetcsv($h, 1000, ',')){
					if($count > 0){//First row is header
						//$category_name = $fc->name;
                        $category = strtoupper(trim($data[0]));

                        dump($category);

						if(array_key_exists($category, $arr_food_categories)){//get id from array
							$food_category_id = $arr_food_categories[$category];
						} else {//Insert new food category
							$fc = new FoodCategory();
							$fc->name = strtoupper($data[0]);
							$fc->save();
							$food_category_id = $fc->id;
							$arr_food_categories[$category] = $food_category_id;
                        }

						//Update foods table
						if(array_key_exists(ucfirst(strtolower($data[2])), $arr_foods)){//get id from array
							$food_id = $arr_foods[ucfirst(strtolower($data[2]))];
						} else {//Insert new food

                            // Search if food is already present
                            $search_food = Food::where('name', '=', ucfirst(strtolower($data[2])))
                                                ->where('food_category_id', $food_category_id)
                                                ->first();

                            if( !$search_food ):

                                $f = new Food();
                                $f->food_category_id = $food_category_id;
                                $f->type = strtoupper(trim($data[1]));
                                $f->name = ucfirst(strtolower($data[2]));
                                $f->description = $data[3];

                                $image_name = NormalizeString($data[2]).".jpg";
                                //print_r($image_name);exit;
                                $dir_category = strtolower(str_replace(' ', '_', $category));
                                $dir_image_path = storage_path('app/public/images/foods/').$dir_category.'/';
                                $path_image_name = storage_path('app/public/images/foods/').$dir_category.'/'.$image_name;

                                //$image_url = asset('/storage/images/foods/').'/'.$data[0]. '/'.NormalizeString($f->name) .'.jpg';

                                if (file_exists($path_image_name)){
                                    $image = Image::make($path_image_name)->orientate();
                                    //print_r($image_path);exit;
                                    cropImages($image,$path_image_name, 'food');
                                }else{
                                    $path_image_name = str_replace('__','_', $path_image_name);
                                    if(file_exists($path_image_name)){
                                        $image_name = str_replace('__','_', $image_name);
                                        $image = Image::make($path_image_name)->orientate();
                                        cropImages($image,$path_image_name, 'food');
                                    } else {
                                        if(!in_array($path_image_name, $arr_images_not_found)){
                                            $arr_images_not_found[] = $path_image_name;
                                        };
                                        $count_food_with_no_image++;
                                        //$path_image_name="/images/food.jpg";
                                        $image_name = "";
                                    }
                                }
                                $f->image = $image_name;
                                if(empty($data[4])){
                                    $f->unit_of_measure_id = 1;
                                } else {
                                    $f->unit_of_measure_id = $arr_uom[strtolower($data[4])];
                                }
                                $f->save();
                                $food_id = $f->id;
                                $arr_foods[ucfirst(strtolower($data[2]))] = $food_id;

                                dump( ucfirst(strtolower($data[2])) . ' INSERITO in ' . $category);

                                Log::channel('new_foods')->info(ucfirst(strtolower($data[2])) . ' INSERITO in ' . $category);

                            else:

                                dump( ucfirst(strtolower($data[2])) . ' già presente in ' . $category);

                                Log::channel('old_foods')->info(ucfirst(strtolower($data[2])) . ' già presente in ' . $category);

                            endif;
						}
					} else {
						$count++;
					}
				}
			//}
			}
			echo 'Prodotti senza immagini: '.$count_food_with_no_image."\n";
			echo 'Immagini non trovate: '.count($arr_images_not_found)."\n";
			print_r($arr_images_not_found);

		}
	}

}
