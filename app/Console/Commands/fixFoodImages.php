<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use App\Models\FoodCategory;
use App\Models\Food;
use Image;
use DB;

class fixFoodImages extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fixFoodImages';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    public function __construct()
    {
        parent::__construct();
    }
     /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $folders=[
            "CARNE",
            "DA_BAR",
            "DA_BERE",
            "DA_DISPENSA",
            "DA_FRIGO",
            "DOLCE",
            "FRUTTA",
            "GELATO",
            "PANE",
            "PASTA_FRESCA",
            "PESCE",
            "PIATTO_PREPARATO",
            "PIZZA",
            "VERDURA",
        ];

        foreach ($folders as $folder) {
            $basePath = $_SERVER['DOCUMENT_ROOT'].'/storage/images/foods/'.$folder. "/";


            if ($handle = opendir($basePath)) {

                while (false !== ($entry = readdir($handle))) {

                    if ($entry != "." && $entry != "..") {

                    if ((strpos($entry, "100x100.jpg") === false && strpos($entry, "230x154.jpg") === false && strpos($entry, "310x240.jpg") === false && strpos($entry, "646x430.jpg") === false) && strpos($entry, ".jpg") !== false ){
                            $change = str_replace(' ','_',$entry);
                            $change = str_replace('(','_',$change);
                            $change = str_replace(')','_',$change);
                            $change = str_replace('__','_',$change);
                            $change = str_replace('_.','.',$change);
                            $change = str_replace('pasta_fresca_','.',$change);
                            rename($basePath.$entry,$basePath.strtolower($change));
                            echo "$folder--$entry\n";
                        }else{
                            unlink($basePath.$entry);
                        }
                    }
                }

                closedir($handle);
            }
        }
    }
}
