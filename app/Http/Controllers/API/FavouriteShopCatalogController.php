<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\AddFavouriteShopCatalogRequest;
use App\Repositories\ShopCatalogRepository;

class FavouriteShopCatalogController extends Controller
{
    private $shopCatalogRepository;

    public function __construct(ShopCatalogRepository $shopCatalogRepository)
    {
        $this->shopCatalogRepository = $shopCatalogRepository;
    }

    public function index(){
        $favourites = Auth::user()->favouriteShopCatalogs->where('visible_in_search', 1);
        foreach($favourites as $k => $fav):
            if( !empty($fav->food_image) ):
                $favourites[$k]['food_image'] = getResizedImage(getOriginalImage($fav->food_image),'230x154');
                if( isset($favourites[$k]['pivot'])) unset($favourites[$k]['pivot']);
            endif;
        endforeach;
        return response()->json(array('status' => 'ok', 'favourite_shop_catalogs'=>$favourites->toArray()));
    }

    public function store(AddFavouriteShopCatalogRequest $request){
        $input = $request->all();

        $shopCatalog = $this->shopCatalogRepository->find($input['shop_catalog_id']);
        if( $shopCatalog ):
            Auth::user()->favouriteShopCatalogs()->attach($shopCatalog);
        endif;

        return response()->json(array('status' => 'ok', 'favourite_shop_catalogs'=>Auth::user()->favouriteShopCatalogs));
    }

    public function destroy(AddFavouriteShopCatalogRequest $request){
        $input = $request->all();

        $shopCatalog = $this->shopCatalogRepository->find($input['shop_catalog_id']);
        if( $shopCatalog ):
            Auth::user()->favouriteShopCatalogs()->detach($shopCatalog);
        endif;

        return response()->json(array('status' => 'ok', 'favourite_shop_catalogs'=>Auth::user()->favouriteShopCatalogs));
    }
}
