<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\AddFavouriteShopRequest;
use App\Repositories\ShopRepository;

class FavouriteShopController extends Controller
{
    private $shopRepository;

    public function __construct(ShopRepository $shopRepository)
    {
        $this->shopRepository = $shopRepository;
    }

    public function index(){
        $favourites = Auth::user()->favouriteShops;
        foreach($favourites as $k => $fav):
            $fav->shopTypes;
            if( !empty($fav->user->image_url) ):
                $favourites[$k]['image_url'] = getResizedImage(getOriginalImage($fav->user->image_url),'640x320');
                if( isset($favourites[$k]['user'])) unset($favourites[$k]['user']);
                if( isset($favourites[$k]['pivot'])) unset($favourites[$k]['pivot']);
            endif;
        endforeach;
        return response()->json(array('status' => 'ok', 'favourite_shops'=>$favourites->toArray()));
    }

    public function store(AddFavouriteShopRequest $request){
        $input = $request->all();

        $shop = $this->shopRepository->find($input['shop_id']);
        if( $shop ):
            Auth::user()->favouriteShops()->attach($shop);
        endif;

        return response()->json(array('status' => 'ok', 'favourite_shops'=>Auth::user()->favouriteShops));
    }

    public function destroy(AddFavouriteShopRequest $request){
        $input = $request->all();

        $shop = $this->shopRepository->find($input['shop_id']);
        if( $shop ):
            Auth::user()->favouriteShops()->detach($shop);
        endif;

        return response()->json(array('status' => 'ok', 'favourite_shops'=>Auth::user()->favouriteShops));
    }

}
