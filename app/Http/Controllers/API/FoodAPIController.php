<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateFoodAPIRequest;
use App\Http\Requests\API\UpdateFoodAPIRequest;
use App\Models\Food;
use App\Models\FoodCategory;
use App\Models\UnitOfMeasure;
use App\Repositories\FoodRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Repositories\ShopCatalogRepository;
use App\Repositories\SpecialRepository;
use Cache;
use Response;
use Log;

use function PHPSTORM_META\elementType;

/**
 * Class FoodController
 * @package App\Http\Controllers\API
 */

class FoodAPIController extends AppBaseController
{
    /** @var  FoodRepository */
    private $foodRepository, $shopCatalogRepository;

    public function __construct(FoodRepository $foodRepo,
                                ShopCatalogRepository $shopCatalogRepository,
                                SpecialRepository $specialRepository)
    {
        $this->foodRepository = $foodRepo;
        $this->shopCatalogRepository = $shopCatalogRepository;
        $this->specialRepository = $specialRepository;
    }

    /**
     * Display a listing of the Food.
     * GET|HEAD /food
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $food = $this->foodRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($food->toArray(), 'Food retrieved successfully');
    }

    /**
     * Store a newly created Food in storage.
     * POST /food
     *
     * @param CreateFoodAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateFoodAPIRequest $request)
    {
        $input = $request->all();

        $food = $this->foodRepository->create($input);

        return $this->sendResponse($food->toArray(), 'Food saved successfully');
    }

    /**
     * Display the specified Food.
     * GET|HEAD /food/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Food $food */
        $food = $this->foodRepository->find($id);

        if (empty($food)) {
            return response()->json(['errors' => ['Not found' => 'Il prodotto selezionato non è acquistabile in questo momento']], 400);
        }

        if( !empty($food->image) ):
            $food->image = getResizedImage($food->image, '646x430');
        endif;

        return $this->sendResponse($food->toArray(), 'Food retrieved successfully');
    }

    /**
     * Update the specified Food in storage.
     * PUT/PATCH /food/{id}
     *
     * @param int $id
     * @param UpdateFoodAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateFoodAPIRequest $request)
    {
        $input = $request->all();

        /** @var Food $food */
        $food = $this->foodRepository->find($id);

        if (empty($food)) {
            return $this->sendError('Il prodotto non è stato trovato');
        }

        $food = $this->foodRepository->update($input, $id);

        return $this->sendResponse($food->toArray(), 'Alimento modificato con successo');
    }

    /**
     * Remove the specified Food from storage.
     * DELETE /food/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {

        /** Cerco prodotti collegati nei cataloghi
         *  Se li trovo non posso cancellare l'alimento
         */

        $count = $this->shopCatalogRepository->allQuery(['food_id' => $id])->get()->count();
        if( $count > 0 ):
            return $this->sendError('Delete not allowed. There are related catalogs');
        endif;

        /** @var Food $food */
        $food = $this->foodRepository->find($id);

        if (empty($food)) {
            return $this->sendError('Il prodotto non è stato trovato');
        }

        $food->delete();

        return $this->sendSuccess('Food deleted successfully');
    }

	/**
     * Get Food type list.
     * Get /food/{id}
     *
     * @param
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function getFoodTypelist($food_category_id = null)
	{
        //$food_type_list = Food::select('type')->distinct()->orderBy('type', 'ASC')->get();
        $food_type_list = $this->foodRepository->getFoodTypeList($food_category_id);
        return response()->json(['Food types' => $food_type_list], 200);
	}

	/**
     * Get Food category list.
     * Get /food/{id}
     *
     * @param
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function getFoodCategorylist()
	{
        $food_category_list = FoodCategory::orderBy('name')->get();
		return response()->json(['Food categories' => $food_category_list], 200);
	}

	/**
     * Get Food iunit of measure list.
     * Get /food/{id}
     *
     * @param
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function getFoodUnitOfMeasurelist()
	{
		$food_unit_of_measure_list = UnitOfMeasure::orderBy('name')->get();
		return response()->json(['Food Unit of measure' => $food_unit_of_measure_list], 200);
	}

	/**
     * Search Foods.
     * Get /food/{id}
     *
     * @param
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function search(Request $request)
	{
        $input = $request->all();

        if( !empty($input['category_id']) && empty($input['type']) && empty($input['food_name']) ):

            $total_foods = Cache::get("total_foods_".$input['category_id']);
            $arr_food = Cache::get("arr_food_".$input['category_id']);

            if($total_foods && $arr_food){

                $count = count($arr_food);

                return response()->json(['total' => $total_foods,
                    'count' => $count,
                    'Food list' => $arr_food], 200);
            }
        endif;

		$food_list = Food::where('available', 1);
		if(!empty($input['category_id'])){
			$food_list->where('food_category_id', $input['category_id']);
		}
		if(!empty($input['type'])){
			$food_list->where('type', $input['type']);
		}
		if(!empty($input['food_name'])){
			$food_list->where('name', 'like', '%'.str_replace(' ', '%', $input['food_name']).'%');
        }

        $total_foods =  count($food_list->get());

        if(!empty($input['offset'])){
            $food_list->offset($input['offset']);
        }
        if(!empty($input['limit'])){
            $food_list->limit($input['limit']);
        }
        $arr_food = $food_list->with('foodCategory')
                              ->with('specials')
                              ->orderBy('name')
                              ->get();

        if( isset($input['category_id']) ){
            Cache::put("arr_food_".$input['category_id'], $arr_food, 5);
            Cache::put("total_foods_".$input['category_id'], $total_foods, 5);
        }

        $count = count($arr_food);

        foreach ($arr_food as $food){
            if (empty($food->image)){
                if ($food->foodCategory->logo) $food->image = asset("/storage/images/foods/".NormalizeString($food->foodCategory->name)."/").$food->foodCategory->logo;
            }else{
                $food->image = asset('storage/images/foods/'.NormalizeString($food->foodCategory->name).'/'.$food->image);
            }

            $food->other_images = [
                'original' => $food->image,
                '512x340' => getResizedImage($food->image, '512x340'),
                '646x430' => getResizedImage($food->image, '646x430')
            ];

            $food->image = getResizedImage($food->image, '230x154');

        }

        return response()->json(['total' => $total_foods,
                                 'count' => $count,
                                 'Food list' => $arr_food], 200);
    }

    public function specials(){
        $specials = $this->specialRepository->allSpecials();
        return response()->json(['specials' => $specials->toArray()], 200);
    }

}
