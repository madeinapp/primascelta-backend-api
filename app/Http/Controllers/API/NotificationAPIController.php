<?php

namespace App\Http\Controllers\API;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class NotificationAPIController extends Controller
{

    public function unread($notificationId){

        $notification = Auth::user()->notifications()
                                ->where('id', $notificationId)
                                ->first();
                                
        $notification->read_at = null;
        $notification->save();

        return response()->json(array('status' => 'ok', 'notification'=>$notification));


    }

    public function read($notificationId){

        $notification = Auth::user()->notifications()
                                ->where('id', $notificationId)
                                ->first();
                                
        $notification->markAsRead();

        return response()->json(array('status' => 'ok', 'notification'=>$notification));


    }

    public function readAll(){

        $notifications = Auth::user()->unreadNotifications;
        
        foreach($notifications  as $notification ):        
            $notification->markAsRead();
        endforeach;                                                                        

        return response()->json(array('status' => 'ok'));

    }

    public function destroy($notificationId){

        $res = Auth::user()->notifications()
                    ->where('id', $notificationId)
                    ->first()
                    ->delete();
        if($res): 
            return response()->json(array('status' => 'ok', 'notificationId' => $notificationId, 'unreadNotifications' => Auth::user()->unreadNotifications()->count()));
        else:
            return response()->json(array('status' => 'error'));
        endif;

    }

    public function destroyAll(){

        $res = Auth::user()->notifications()->delete();
        if($res): 
            return response()->json(array('status' => 'ok'));
        else:
            return response()->json(array('status' => 'error'));
        endif;

    }
}
