<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\API\UpdateOrderAPIRequest;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

use Illuminate\Http\Request;

use App\Models\Order;
use App\Models\OrderFood;
use App\Models\OrderChat;
use App\Models\ShopOffer;
use App\Models\ShopOfferFood;
use App\Models\ShopCatalog;
use App\Models\Food;

use App\Repositories\OrderRepository;
use App\Repositories\OrderChatRepository;
use App\Repositories\ShopRepository;
use App\Repositories\ShopCatalogRepository;
use App\Repositories\ShopOfferFoodRepository;
use App\Repositories\UserRepository;
use App\Repositories\UnitOfMeasureRepository;

use Response;
use OneSignal;
use Carbon\Carbon;

use App\Notifications\OrderSent;
use App\Notifications\OrderReceived;
use App\Notifications\OrderConfirmed;
use App\Notifications\OrderConfirmedWithReserve;
use App\Notifications\OrderModified;
use App\Notifications\OrderReserveAccepted;
use App\Notifications\OrderCancelled;
use App\Notifications\OrderNotDelivered;
use App\Notifications\OrderNotDeliveredCopy;
use App\Notifications\OrderDelivered;
use App\Notifications\MessageReceived;

use PDF;
use Log;
use Storage;
use Config;

/**
 * Class OrderController
 * @package App\Http\Controllers\API
 */

class OrderAPIController extends AppBaseController
{
    /** @var  OrderRepository */
	private $orderRepository,
			$shopRepository,
			$orderChatRepository,
			$userRepository,
			$shopCatalogRepository,
            $shopOfferFoodRepository,
            $unitOfMeasureRepository
            ;

	public function __construct(OrderRepository $orderRepo,
								ShopRepository $shopRepository,
								OrderChatRepository $orderChatRepository,
								UserRepository $userRepository,
								ShopCatalogRepository $shopCatalogRepository,
                                ShopOfferFoodRepository $shopOfferFoodRepository,
                                UnitOfMeasureRepository $unitOfMeasureRepository)
    {
		$this->orderRepository = $orderRepo;
		$this->shopRepository = $shopRepository;
		$this->orderChatRepository = $orderChatRepository;
		$this->userRepository = $userRepository;
		$this->shopCatalogRepository = $shopCatalogRepository;
        $this->shopOfferFoodRepository = $shopOfferFoodRepository;
        $this->unitOfMeasureRepository = $unitOfMeasureRepository;
    }

    /**
     * Display a listing of the Order.
     * GET|HEAD /orders
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $orders = $this->orderRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($orders->toArray(), 'Orders retrieved successfully');
    }

    /**
     * Display the specified Order.
     * GET|HEAD /orders/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show(Request $request)
    {
		$input = $request->all();

        /*
        Log::info("Order show");
        Log::info($input);
        */

		if( auth()->user()->user_type == 'private' ){
			$order = Order::where('user_id', auth()->user()->id);
		}elseif( auth()->user()->user_type == 'company' ){
			$order = Order::where('shop_id', auth()->user()->shop->id);
        }

		if( $order ){

			if(!empty($input['order_id'])){
				$order = $order->where('id', $input['order_id']);
			}else{
				$order = $order->where('order_status_id', Config::get('constants.orders.draft'));
			}

            $order = $order->with('shop.shopTypes')
                           ->with('OrderFoods')
                           ->orderBy('created_at', 'desc')
                           ->first();

			if( $order ):

                $order->total_products = 0;
                $total = 0;
                foreach( $order->orderFoods as $k => $orderFood ):
                    $total += $orderFood->quantity;

                    if( !empty($orderFood->shop_catalog_id) ):
                        if( $order->orderFoods[$k]->shopCatalog ):
                            if( $order->orderFoods[$k]->shopCatalog->unit_of_measure_id ):
                                $order->orderFoods[$k]->shopCatalog->unitOfMeasure;
                            endif;

                            if( $order->orderFoods[$k]->shopCatalog->unit_of_measure_buy ):
                                $order->orderFoods[$k]->shopCatalog->unitOfMeasureBuy;
                            endif;
                        endif;
                    else:
                        $order->orderFoods[$k]->offerFood;
                        if( $order->orderFoods[$k]->offerFood->shopCatalog ):
                            if( $order->orderFoods[$k]->offerFood->shopCatalog->unit_of_measure_id ):
                                $order->orderFoods[$k]->offerFood->shopCatalog->unitOfMeasure;
                            endif;

                            if( $order->orderFoods[$k]->offerFood->shopCatalog->unit_of_measure_buy ):
                                $order->orderFoods[$k]->offerFood->shopCatalog->unitOfMeasureBuy;
                            endif;
                        endif;
                    endif;
                endforeach;

                $order->total_product = $total;

            else:
				return response()->json(array('success' => false, 'data' => null, 'message' => 'Ordine non trovato'));

            endif;

            $order->orderChats;
            $order->shop->PaymentMethodsShop;
            $order->shop->PaymentMethodsDelivery;

            return response()->json(["success" => true, "message" => "Ordine", "data" => $order], 200, [], JSON_NUMERIC_CHECK);

		}else{
			return response()->json(array('success' => false, 'data' => null, 'message' => 'Ordine non trovato'));
		}
    }

    /**
     * Update the specified Order in storage.
     * PUT/PATCH /orders/{id}
     *
     * @param int $id
     * @param UpdateOrderAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOrderAPIRequest $request)
    {
        $input = $request->all();

        /** @var Order $order */
        $order = $this->orderRepository->find($id);

        if (empty($order)) {
            return $this->sendError('Ordine non trovato');
        }

        $order = $this->orderRepository->update($input, $id);

        return $this->sendResponse($order->toArray(), 'Ordine modificato con successo');
    }

    /**
     * Remove the specified Order from storage.
     * DELETE /orders/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Order $order */
        $order = $this->orderRepository->find($id);

        if (empty($order)) {
            return $this->sendError('Ordine non trovato');
        }

        $order->delete();

        return $this->sendSuccess('Order deleted successfully');
    }

	/**
     * Put  offer food into order.
     * Put /shopOffers/ad-to-order
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function store(Request $request)
	{
        if($user = auth()->user()) {
            $input = $request->all();

            Log::info("Order store");
            Log::info($input);

            if( in_array($input['action'], ['add','save','subtract']) && $user->email == "demo@demo.it" ){
                return response()->json(['errors' => ['Blacklist' => 'DEMOUSER']], 400);
            }

            if( isset($input['order_id']) && $input['order_id'] == 'undefined' ):
                $input['order_id'] = "";
            endif;

            if( in_array($input['action'], ['add', 'save','subtract']) && Auth::user()->blacklistShops->contains('id', $input['shop_id']) ){
                return response()->json(['errors' => ['Blacklist' => 'Il negozio ha bloccato gli acquisti da parte della tua utenza']], 400);
            }

            if( isset($input['shop_id']) && $input['action'] == 'add' ):
                $shop = $this->shopRepository->find($input['shop_id']);
                if( $shop && $shop->min_trust_points_percentage_bookable > Auth::user()->trust_points ):
                    return response()->json(['errors' => ['not_enough_trust_points' => 'Non hai punti fiducia sufficienti per effettuare un ordine a questo negozio']], 400);
                endif;
                unset($shop);
            endif;

            $order = null;

			if( in_array($input['action'], ['add', 'delete', 'subtract']) )
			{
                $input['user_id'] = $user->id;

                if( !empty($input['order_id']) ):

                    $order = Order::where('id', $input['order_id'])
                            ->where('user_id', $user->id);

                            if( isset($input['shop_id']) && !empty($input['shop_id']) ){
                                $order = $order->where('shop_id', $input['shop_id']);
                            }

                            $order = $order->whereIn('order_status_id', [Config::get('constants.orders.draft'),
                                                            Config::get('constants.orders.not_confirmed'),
                                                            Config::get('constants.orders.confirmed_with_reserve')])
                            ->orderBy('created_at', 'desc')
                            ->first();

                endif;

				if(empty($order))
				{
                    //New order (cart)
					if($input['action'] == 'delete' || $input['action'] == 'subtract'){
                        if ($user->email == "demo@demo.it")
                            return response()->json(['errors' => ['Blacklist' => 'DEMOUSER']], 400);
                        else
						    return response()->json(['errors' => ['Not found' => 'Non é possibile cancellare/rimuovere questo prodotto/offerta']], 400);
                    }

					//Check if there's a dangling order
					$order = Order::where('user_id', $user->id)
								->where('order_status_id', Config::get('constants.orders.draft'))
								->orderBy('created_at', 'desc')
								->get();

                    if(count($order) > 0){//Dangling orders
						return response()->json(['errors' => ['More than one order' => 'Non puoi aggiungere prodotti di altri negozi']],400);
					} else {

						$input['order_status_id'] = Config::get('constants.orders.draft');
						$input['pickup_on_site'] = empty($input['pickup_on_site']) ? 0 : $input['pickup_on_site'];
						$input['home_delivery'] = empty($input['home_delivery']) ? 0 : $input['home_delivery'];
						$input['delivery_host_on_site'] = empty($input['delivery_host_on_site']) ? 0 : $input['delivery_host_on_site'];
						$input['note'] = $input['note'] ?? '';
						$order = Order::create($input);
					}
                }

				if($input['action'] != 'delete' ){
					$shop_catalog_id = null;
					$shop_offer_food_id = null;

					if($input['food_type'] == 'catalog'){
                        $shop_catalog_id = $input['food_type_id'];
                        $shop_catalog = $this->shopCatalogRepository->find($shop_catalog_id);
						if(empty($shop_catalog)){
							return response()->json(['errors' => ['Not found' => 'Il prodotto non è stato trovato']], 400);
						}else{
                            if( isset($input['new_shop']) ){
                                if( $shop_catalog->shop_id != $input['new_shop'] ){
                                    return response()->json(['errors' => ['shop_catalog_error' => 'The food catalog does not belongs to the new shop']], 400);
                                }
                            }else{
                                if( $shop_catalog->shop_id != $input['shop_id'] ){
                                    return response()->json(['errors' => ['shop_catalog_error' => 'The food catalog does not belongs to the shop']], 400);
                                }
                            }
                        }
						$food_name = $shop_catalog->food_name;
						$food_description = $shop_catalog->food_description;
						$food_image = '';
						if(!empty($shop_catalog->food_image)){
							$food_image = getResizedImage(getOriginalImage($shop_catalog->food_image), '100x100');
						}else{
                            if( !empty($shop_catalog->food_id) ){
                                $food = Food::find($shop_catalog->food_id);
                                if( $food ){
                                    $url_image = '/storage/images/foods/'.NormalizeString($food->foodCategory->name).'/'.getResizedImage(getOriginalImage($food->image), '100x100');
                                    $food_image = asset($url_image);
                                }
                            }
                        }
					}
					elseif($input['food_type'] == 'offer'){
						$shop_offer_food_id = $input['food_type_id'];
                        $shop_offer_food = $this->shopOfferFoodRepository->find($shop_offer_food_id);
						if(empty($shop_offer_food)){
							return response()->json(['errors' => ['Not found' => "L'offerta non è più disponibile"]], 400);
						}else{
                            if( isset($input['new_shop']) ){
                                if( $shop_offer_food->shopCatalog ){
                                    if( $shop_offer_food->shopCatalog->shop_id != $input['new_shop'] ){
                                        return response()->json(['errors' => ['shop_offer_food_error' => "L'offerta non appartiene al negozio selezionato"]], 400);
                                    }
                                }else{
                                    return response()->json(['errors' => ['shop_catalog_not_found_error' => "L'offerta è collegata ad un prodotto del catalogo inesistente"]], 400);
                                }
                            }else{
                                if( $shop_offer_food->shopCatalog ){
                                    if( $shop_offer_food->shopCatalog->shop_id != $input['shop_id'] ){
                                        return response()->json(['errors' => ['shop_offer_food_error' => "L'offerta non appartiene al negozio selezionato"]], 400);
                                    }
                                }else{
                                    return response()->json(['errors' => ['shop_offer_food_error' => "L'offerta non apparteiene al negozio selezionato"]], 400);
                                }
                            }
                        }
						$food_name = $shop_offer_food->food_name;
						$food_description = $shop_offer_food->food_description;
						$food_image = '';
						if(!empty($shop_offer_food->food_image)){
                            $food_image = getResizedImage(getOriginalImage($shop_offer_food->food_image), '100x100');
						}else{
                            if( !empty($shop_offer_food->shop_catalog_id) ){
                                $shop_catalog = ShopCatalog::find($shop_offer_food->shop_catalog_id);
                                if(!empty($shop_catalog->food_image)){
                                    $food_image = getResizedImage(getOriginalImage($shop_catalog->food_image), '100x100');
                                }else{
                                    if( !empty($shop_catalog->food_id) ){
                                        $food = Food::find($shop_catalog->food_id);
                                        if( $food ){
                                            $url_image = '/storage/images/foods/'.NormalizeString($food->foodCategory->name).'/'.getResizedImage(getOriginalImage($food->image), '100x100');;
                                            $food_image = asset($url_image);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if( !empty($shop_catalog_id) ):

                        $order_food = OrderFood::where('order_id', $order->id)
                                                ->where('shop_catalog_id', $shop_catalog_id)
                                                ->first();

                    elseif( !empty($shop_offer_food_id) ):

                        $order_food = OrderFood::where('order_id', $order->id)
                                                ->where('shop_offer_food_id', $shop_offer_food_id)
                                                ->first();


                    endif;
				}

			}else{

				$order = $this->orderRepository->find($input['order_id']);

			}

			switch($input['action']){

                case 'delete':

                    if( in_array($order->order_status_id, [Config::get('constants.orders.not_confirmed'),
                                                           Config::get('constants.orders.confirmed_with_reserve')])  &&
                                                           $order->orderFoods->count() === 1 ){
                        return response()->json(['errors' => ['cancel_order_requested' => "Vuoi cancellare quest'ordine?"]], 400);
                    }

                    //Delete OrderFood
                    $orderFood = OrderFood::where('id', $input['order_food_id'])->first();

                    if( $order->order_status_id == Config::get('constants.orders.confirmed_with_reserve') ){
                        $note = $order->note;
                        $note.="Eliminato prodotto " . $orderFood->food_name . PHP_EOL;
                        $input['note'] = $note;
                    }

                    $orderFood->delete();

                    $resTotal = $this->getTotalPrice($order);
                    $input['total_price'] = $resTotal['total_price'];
                    $input['shipping_price'] = $resTotal['shipping_price'];



                    $order = $this->orderRepository->update($input, $input['order_id']);

                    if( $order->order_status_id == Config::get('constants.orders.draft') && $order->orderFoods->count() === 0 ){
                        $order->delete();
                        $order = null;
                        return response()->json(['errors' => ['empty_cart' => "Il carrello è vuoto"]], 400);
                    }

					break;

                case 'add':

                    Log::info("add product");

                    if( isset($input['new_shop']) ):
                        $this->orderRepository->update(['shop_id' => $input['new_shop']], $input['order_id']);
                        $order = $this->orderRepository->find($input['order_id']);
                        $input['shop_id'] = $input['new_shop'];
                        unset($input['new_shop']);
                        OrderFood::where('order_id', $input['order_id'])->delete();
                    endif;

					$add_rule = [
						'food_type' => 'bail|required',
						'food_type_id' => 'bail|required'
					];

					$add_messages = [
						'food_type.required' => 'food_type è obbligatorio',
						'food_type_id.required' => 'food_type_id è obbligatorio'
					];

					$add_validator = Validator::make($input, $add_rule, $add_messages);
					if($add_validator->fails()){
                        Log::info("validator fails");
						return response()->json(['errors' => $add_validator->errors()], 400);
                    }

					if( empty($order_food) ){
						$input['order_id'] = $order->id;
						$input['shop_catalog_id'] = $shop_catalog_id;
						$input['shop_offer_food_id'] = $shop_offer_food_id;
						$input['food_name'] = $food_name;
                        $input['food_description'] = $food_description;
						$input['food_image'] = $food_image;
                        $order_food = OrderFood::create($input);
                    }

                    Log::info("add quantity");
                    $order_food->quantity = $order_food->quantity + 1;

                    $buyPrice = $this->getBuyPrice($order_food);
                    Log::info("buy price");
                    Log::info($buyPrice);
                    if( !$buyPrice ):
                        Log::info("buy price error");
                        return response()->json(['errors' => ['buy_price' => 'Errore nel calcolo del prezzo']], 400);
                    endif;
                    $order_food->price = floatval($buyPrice);
                    $order_food->save();

                    $resTotal = $this->getTotalPrice($order);

                    $input['total_price'] = $resTotal['total_price'];
                    $input['shipping_price'] = $resTotal['shipping_price'];

                    if( $order->order_status_id == Config::get('constants.orders.confirmed_with_reserve') ){
                        $note = $order->note;
                        $note.="Aggiunta 1 quantità al prodotto " . $food_name . PHP_EOL;
                        $input['note'] = $note;
                    }

                    $order = $this->orderRepository->update($input, $input['order_id']);

                    break;

                case 'shipping_price':

                    if( isset($input['home_delivery']) ){
                        $input['home_delivery'] = 1;
                        $input['pickup_on_site'] = 0;
                        $input['delivery_host_on_site'] = 0;
                    }elseif( isset($input['pickup_on_site']) ){
                        $input['pickup_on_site'] = 1;
                        $input['home_delivery'] = 0;
                        $input['delivery_host_on_site'] = 0;
                    }elseif( isset($input['delivery_host_on_site']) ){
                        $input['delivery_host_on_site'] = 1;
                        $input['pickup_on_site'] = 0;
                        $input['home_delivery'] = 0;
                    }

                    $order = $this->orderRepository->update($input, $input['order_id']);

                    $resTotal = $this->getTotalPrice($order);
                    $input['total_price'] = $resTotal['total_price'];
                    $input['shipping_price'] = $resTotal['shipping_price'];
                    $order = $this->orderRepository->update($input, $input['order_id']);
                    break;

                case 'subtract':

                    if( in_array($order->order_status_id, [Config::get('constants.orders.not_confirmed'),
                                                           Config::get('constants.orders.confirmed_with_reserve')])  &&
                                                           $order_food->quantity == 1 &&
                                                           $order->orderFoods->count() === 1
                                                           ){
                        return response()->json(['errors' => ['cancel_order_requested' => "Vuoi cancellare quest'ordine?"]], 400);
                    }

                    $order_food->quantity = $order_food->quantity-1;

                    if($order_food->quantity > 0){
                        $buyPrice = $this->getBuyPrice($order_food);
                        if( !$buyPrice ):
                            return response()->json(['errors' => ['buy_price' => 'Errore nel calcolo del prezzo']], 400);
                        endif;
                        $order_food->price = floatval($buyPrice);
						$order_food->update();
                    }else{
                        $order_food->delete();
                    }

                    $resTotal = $this->getTotalPrice($order);
                    $input['total_price'] = $resTotal['total_price'];
                    $input['shipping_price'] = $resTotal['shipping_price'];

                    if( $order->order_status_id == Config::get('constants.orders.confirmed_with_reserve') ){
                        $note = $order->note;
                        $note.="Sottratta 1 quantità al prodotto " . $food_name . PHP_EOL;
                        $input['note'] = $note;
                    }

                    $order = $this->orderRepository->update($input, $order->id);

                    if( $order->order_status_id == Config::get('constants.orders.draft') && $order->orderFoods->count() === 0 ){
                        $order->delete();
                        $order = null;
                        return response()->json(['errors' => ['empty_cart' => "Il carrello è vuoto"]], 400);
                    }

                    break;

                case 'delivery':
                    $resTotal = $this->getTotalPrice($order);
                    if( $resTotal['status'] === 'ok' ):
                        $input['total_price'] = $resTotal['total_price'];
                        $input['shipping_price'] = $resTotal['shipping_price'];
                    elseif( $resTotal['status'] === 'error' ):
                        return response()->json(['errors' => $resTotal['error_msg']], 400);
                    endif;
                    $order = $this->orderRepository->update($input, $input['order_id']);
                    break;

				case 'save':
					/** Invio dell'ordine
					 *  o modifica ordine dopo essere stato confermato con riserva
					 * */
                    if( Auth::id() == $order->user_id && in_array($order->order_status_id, [Config::get('constants.orders.draft'),
                                                                                            Config::get('constants.orders.not_confirmed'),
                                                                                            Config::get('constants.orders.confirmed_with_reserve')]) ):

                        if( $order->shop->shop_status_orders == 0 ){
                            // Flavio : not_enough_trust_points è temporaneo, andrebbe cambiato con un altro indice che però deve leggere l'app
                            return response()->json(['errors' => ['not_enough_trust_points' => "L'attività non accetta ordini in questo momento"]], 400);
                        }

                        if( $order->shop->min_trust_points_percentage_bookable > Auth::user()->trust_points ):
                            return response()->json(['errors' => ['not_enough_trust_points' => 'Non hai punti fiducia sufficienti per effettuare un ordine a questo negozio']], 400);
                        endif;

                        if( !empty($input['pickup_on_site_date']) ){
                            $input['pickup_on_site_date'] = Carbon::parse($input['pickup_on_site_date'] . ' ' . $input['pickup_on_site_time']);
                            //$input['pickup_on_site_date'] = Carbon::parse($input['pickup_on_site_date']);
                        }

                        if( !empty($input['home_delivery_date']) ){
                            $input['home_delivery_date'] = Carbon::parse($input['home_delivery_date'] . ' ' . $input['home_delivery_time']);
                            //$input['home_delivery_date'] = Carbon::parse($input['home_delivery_date']);
                        }

                        if( !empty($input['delivery_host_on_site_date']) ){
                            $input['delivery_host_on_site_date'] = Carbon::parse($input['delivery_host_on_site_date'] . ' ' . $input['delivery_host_on_site_time']);
                            //$input['delivery_host_on_site_date'] = Carbon::parse($input['delivery_host_on_site_date']);
                        }

                        $min_delivery_date = Carbon::now()->addHour();

						$order_rules = [
							'pickup_on_site' => 'required_without:home_delivery|nullable|integer|min:1|max:1',
							'pickup_on_site_date' => 'nullable|required_if:pickup_on_site,1|after_or_equal:' . $min_delivery_date->format('Y-m-d H:i'),
							'pickup_on_site_time' => 'nullable|required_if:pickup_on_site,1',
							'home_delivery' => 'required_without:pickup_on_site|nullable|integer|min:1|max:1',
							'home_delivery_address' => 'nullable|required_if:home_delivery,1',
							'home_delivery_country' => 'nullable|required_if:home_delivery,1',
							'home_delivery_route' => 'nullable|required_if:home_delivery,1',
							//'home_delivery_city' => 'nullable|required_if:home_delivery,1',
							'home_delivery_region' => 'nullable|required_if:home_delivery,1',
							'home_delivery_postal_code' => 'nullable|required_if:home_delivery,1',
							//'home_delivery_street_number' => 'nullable|required_if:home_delivery,1',
							'home_delivery_lat' => 'nullable|required_if:home_delivery,1',
							'home_delivery_long' => 'nullable|required_if:home_delivery,1',
							'home_delivery_name' => 'nullable|required_if:home_delivery,1',
							'home_delivery_phone' => 'nullable|required_if:home_delivery,1',
							'home_delivery_date' => 'nullable|required_if:home_delivery,1|after_or_equal:' . $min_delivery_date->format('Y-m-d H:i'),
							'home_delivery_time' => 'nullable|required_if:home_delivery,1',
							'home_delivery_payment_method_id' => 'nullable|string|required_if:home_delivery,1|min:1',
							//'delivery_host_on_site' => 'required_without_all:pickup_on_site,home_delivery|nullable|integer|min:1|max:1',
							//'delivery_host_on_site_date' => 'nullable|required_if:delivery_host_on_site,1|after:' . $min_delivery_date->format('Y-m-d H:i'),
							//'delivery_host_on_site_time' => 'nullable|required_if:delivery_host_on_site,1',
							'delivery_host_on_site_note' => 'nullable',
							'delivery_host_on_site_payment_method_id' => 'nullable|string|required_if:delivery_host_on_site,1|min:1'
						];

						$order_messages = [
							'pickup_on_site.min' => 'The only value accepted is 1, leave empty if field is not checked',
							'pickup_on_site.required_without' => 'Seleziona Ritiro in sede o Consegna a domicilio per la consegna',
							'pickup_on_site_date.required_if' => 'Data del ritiro in sede assente',
							'pickup_on_site_time.required_if' => 'Orario del ritiro in sede assente',
							'pickup_on_site_date.after_or_equal' => "La data del ritiro deve essere successiva almeno di un'ora",
							'home_delivery.min' => 'The only value accepted is 1, leave empty if field is not checked',
							'home_delivery.required_without' => 'Seleziona Consegna a domicilio o Ritiro in sede per la consegna',
							'home_delivery_address.required_if' => 'L\'indirizzo per la spedizione a domicilio è assente',
							'home_delivery_name.required_if' => 'Il nome è assente',
							'home_delivery_phone.required_if' => 'Il telefono è assente',
							'home_delivery_date.required_if' => 'La data per la spedizione a domicilio è assente',
							'home_delivery_date.after_or_equal' => "La data di consegna a domicilio deve essere successiva di almeno un'ora",
                            'home_delivery_time.required_if' => 'L\'orario per la spedizione a domicilio è assente',
                            'home_delivery_payment_method_id.required_if' => 'Il metodo di pagamento è assente',
                            'home_delivery_payment_method_id.min' => 'Devi selezionare almeno un metodo di pagamento'
                            /*
							'delivery_host_on_site.min' => 'The only value accepted is 1, leave empty if field is not checked',
							'delivery_host_on_site.required_without_all' => 'Seleziona un metodo per la consegna',
							'delivery_host_on_site_date.required_if' => 'La data è assente',
							'delivery_host_on_site_date.after_or_equal' => 'La data di consegna deve essere uguale o successiva al '  . $min_delivery_date->format('d/m/Y') . ' alle ' . $min_delivery_date->format('H:i'),
							'delivery_host_on_site_time.required_if' => 'L\'orario è assente',
                            'delivery_host_on_site_payment_method_id.required_if' => 'Il metodo di pagamento è assente',
                            'delivery_host_on_site_payment_method_id.min' => 'Devi selezionare almeno un metodo di pagamento'
                            */
						];

						$order_validator = Validator::make($input, $order_rules, $order_messages);
						if($order_validator->fails()){
							return response()->json(['errors' => $order_validator->errors()], 400);
                        }


                        if( !isset($input['home_delivery']) ){
							$input['home_delivery'] = 0;
						}

						if( !isset($input['pickup_on_site']) ){
							$input['pickup_on_site'] = 0;
						}

						if( !isset($input['delivery_host_on_site']) ){
							$input['delivery_host_on_site'] = 0;
						}


                        /** Verifica distanza consegna */
                        $resDeliveryDistance = $this->checkDevliveryDistance($input, $order);
                        if( $resDeliveryDistance['status'] === 'error' ):
                            return response()->json(['errors' => $resDeliveryDistance['error_msg']], 400);
                        endif;


                        /** Get total price and shipping price */
                        $resTotal = $this->getTotalPrice($order);
                        if( $resTotal['status'] === 'ok' ):
                            $input['total_price'] = $resTotal['total_price'];
                            $input['shipping_price'] = $resTotal['shipping_price'];
                        elseif( $resTotal['status'] === 'error' ):
                            return response()->json(['errors' => $resTotal['error_msg']], 400);
                        endif;


                        /** Check shop closing days */
                        $resCheckClosing = $this->checkClosedShop($input, $order);
						if( isset($resCheckClosing['errors']) ){
							return response()->json($resCheckClosing, 400);
						}

                        /** Check home delivery order	*/
                        $res = $this->checkHomeDelivery($input, $order);
                        if( !empty($res) ):
                            return $res;
                        endif;

                        /* Ceck Delivery Date */
						$resCheckDate = $this->checkOrderDeliveryDate($input, $order);
						if( isset($resCheckDate['errors']) ){
							return response()->json($resCheckDate, 400);
                        }

                        $previous_order_status = $order->order_status_id;

                        if( $order->order_status_id == Config::get('constants.orders.draft') ||
                            ( $order->order_status_id == Config::get('constants.orders.confirmed_with_reserve') && !empty($order->note)) ):

                            $input['order_status_id'] = Config::get('constants.orders.not_confirmed');

                        elseif( $order->order_status_id == Config::get('constants.orders.confirmed_with_reserve') && empty($order->note) ):

                            if(isset($input['pickup_on_site']) &&
                                $input['pickup_on_site'] == 1 &&
                                $order->pickup_on_site == 1 &&
                                Carbon::parse(Carbon::parse($input['pickup_on_site_date'])->format('Y-m-d') . ' ' . $input['pickup_on_site_time']) == $order->pickup_on_site_within
                            ):

                                $input['order_status_id'] = Config::get('constants.orders.confirmed');

                            elseif(isset($input['home_delivery']) &&
                                $input['home_delivery'] == 1 &&
                                $order->home_delivery == 1 &&
                                Carbon::parse(Carbon::parse($input['home_delivery_date'])->format('Y-m-d') . ' ' . $input['home_delivery_time']) == $order->home_delivery_within
                            ):

                                $input['order_status_id'] = Config::get('constants.orders.confirmed');

                            else:

                                if( isset($input['pickup_on_site']) &&
                                    $input['pickup_on_site'] == 1 &&
                                    $order->pickup_on_site == 0
                                    ):

                                    $note = $order->note;
                                    $note.="Il cliente ha cambiato il tipo di consegna in Ritiro in sede" . PHP_EOL;
                                    $input['note'] = $note;

                                elseif( isset($input['home_delivery']) &&
                                    $input['home_delivery'] == 1 &&
                                    $order->home_delivery == 0
                                    ):

                                    $note = $order->note;
                                    $note.="Il cliente ha cambiato il tipo di consegna in Consegna a domicilio" . PHP_EOL;
                                    $input['note'] = $note;
                                endif;

                                $input['order_status_id'] = Config::get('constants.orders.not_confirmed');

                                $note = $input['note'];
                                $note .= "Il cliente ha cambiato la data di consegna" . PHP_EOL;
                                $input['note'] = $note;

                            endif;

                        endif;

                        $input['created_at'] = Carbon::now()->timezone('Europe/Rome')->toDateTimeString();

                        /** Aggiorno data annullabile entro.. */
                        //$order = $this->orderRepository->update($input, $order->id);

						/**
						 * send notification to Shop User
						*/
						$shop = $this->shopRepository->find($order->shop_id);
                        $shopUser = $this->userRepository->find($shop->user_id);
                        $orderUser = $this->userRepository->find($order->user_id);

                        $order->shop; //simply adds order shop to object

                        $note = $input['note'] ?? $order->note;

                        if( !empty($note) && in_array(intval($order->order_status_id), [Config::get('constants.orders.draft'),
                                                                                        Config::get('constants.orders.confirmed_with_reserve')])):

                            $messageFrom = Auth::id() === $order->shop->user->id ? 1 : 0;

                            if( intval($order->order_status_id) === Config::get('constants.orders.confirmed_with_reserve') ):

                                $note = "NOTE DI SERVIZIO: " . PHP_EOL . $note;

                            endif;

                            $this->orderChatRepository->create([
                                'order_id' => $order->id,
                                'from' => $messageFrom,
                                'message' => $note
                            ]);

                            $note = null;

                        endif;

                        $order = $this->orderRepository->update($input, $order->id);

                        $cancellable_within = $this->setCancellableWithin($this->orderRepository->find($order->id));

                        $input['cancellable_within'] = $cancellable_within->format('Y-m-d H:i');
                        $input['confirm_reserve_within'] = null;
                        $input['pickup_on_site_within'] = null;
                        $input['home_delivery_within'] = null;
                        $input['delivery_host_on_site_within'] = null;
                        $input['note'] = $note;

                        $order = $this->orderRepository->update($input, $order->id);

                        if (env('APP_ENV') === 'production'):
                            if( $previous_order_status == Config::get('constants.orders.draft') ):

                                $shopUser->notify(new OrderReceived(Auth::user(), $order));
                                $orderUser->notify(new OrderSent(Auth::user(), $order));

                            elseif( in_array($previous_order_status, [Config::get('constants.orders.not_confirmed'),
                                                                      Config::get('constants.orders.confirmed_with_reserve')]) ):

                                if( intval($order->order_status_id) === Config::get('constants.orders.confirmed') ):

                                    $shopUser->notify(new OrderReserveAccepted(Auth::user(), $order));
                                    $orderUser->notify(new OrderConfirmed(Auth::user(), $order));

                                elseif( intval($order->order_status_id) === Config::get('constants.orders.not_confirmed') ):

                                    $shopUser->notify(new OrderModified(Auth::user(), $order));
                                    $orderUser->notify(new OrderSent(Auth::user(), $order));

                                endif;
                            endif;
                        endif;

					else:
						if( Auth::id() != $order->user_id ):
							return response()->json(['errors' => 'Non hai i permessi per di accesso a quest\'ordine'], 400);
						endif;

                        if( $order->order_status_id != Config::get('constants.orders.draft') &&
                            $order->order_status_id != Config::get('constants.orders.confirmed_with_reserve') ):
							return response()->json(['errors' => 'L\'ordine non è nel carrello nè confermato con riserva'], 400);
						endif;

					endif;
					break;

				case 'confirmed_with_reserve':
					/** This action can be peformed only by the company user */
					if( isset(Auth::user()->shop) ):

                        $order = $this->orderRepository->find($input['order_id']);

						if( $order->shop_id == Auth::user()->shop->id ):

                            $after_date = Carbon::now()->timezone('Europe/Rome')->addHours(1);

							$order_rules = [
                                'pickup_on_site_within' => 'nullable|date_format:Y-m-d H:i|after:'.$after_date->format('Y-m-d H:i'),
                                'home_delivery_within' => 'nullable|date_format:Y-m-d H:i|after:'.$after_date->format('Y-m-d H:i'),
                                'delivery_host_on_site_within' => 'nullable|date_format:Y-m-d H:i|after:'.$after_date->format('Y-m-d H:i'),
								'confirm_reserve_within' => 'required|date_format:Y-m-d H:i|after:'.$after_date->format('Y-m-d H:i')
							];

							$order_messages = [
								'pickup_on_site_within.date_format' => 'Data del ritiro in sede errata',
                                'pickup_on_site_within.after' => 'La data del ritiro in sede deve essere successiva a ' . $after_date->format('d/m/Y H:i'),
                                'home_delivery_within.date_format' => 'Data del ritiro in sede errata',
                                'home_delivery_within.after' => 'La data del ritiro in sede deve essere successiva a ' . $after_date->format('d/m/Y H:i'),
                                'delivery_host_on_site_within.date_format' => 'Data del ritiro in sede errata',
                                'delivery_host_on_site_within.after' => 'La data del ritiro in sede deve essere successiva a ' . $after_date->format('d/m/Y H:i'),
								'confirm_reserve_within.required' => 'La data di "conferma entro" è assente',
								'confirm_reserve_within.date_format' => 'La data di "conferma entro" è assente',
								'confirm_reserve_within.after' => 'La data di conferma deve essere successiva a ' . $after_date->format('d/m/Y H:i')
							];

							$order_validator = Validator::make($input, $order_rules, $order_messages);
							if($order_validator->fails()){
								return response()->json(['errors' => $order_validator->errors()], 400);
                            }

                            $input['order_status_id'] = Config::get('constants.orders.confirmed_with_reserve');

                            if( !empty($input['pickup_on_site_within']) ):
                                $input['pickup_on_site_date'] = Carbon::parse($input['pickup_on_site_within'])->format('Y-m-d');
                                $input['pickup_on_site_time'] = Carbon::parse($input['pickup_on_site_within'])->format('H:i');
                            elseif( !empty($input['home_delivery_within']) ):
                                $input['home_delivery_date'] = Carbon::parse($input['home_delivery_within'])->format('Y-m-d');
                                $input['home_delivery_time'] = Carbon::parse($input['home_delivery_within'])->format('H:i');
                            elseif( !empty($input['delivery_host_on_site_within']) ):
                                $input['delivery_host_on_site_date'] = Carbon::parse($input['delivery_host_on_site_within'])->format('Y-m-d');
                                $input['delivery_host_on_site_time'] = Carbon::parse($input['delivery_host_on_site_within'])->format('H:i');
                            endif;

                            $input['note'] = null;

                            if( !empty($input['confirm_reserve_within']) ){
                                if( !empty($input['pickup_on_site_within']) ){
                                    if( Carbon::parse($input['confirm_reserve_within'])->gt($input['pickup_on_site_within']) ){
                                        return response()->json(['errors' => "La data del ritiro in sede non può precedere la data per dell'annullamento"], 400);
                                    }
                                }else if( !empty($input['home_delivery_within']) ){
                                    if( Carbon::parse($input['confirm_reserve_within'])->gt($input['home_delivery_within']) ){
                                        return response()->json(['errors' => "La data della consegna a domicilio non può precedere la data per dell'annullamento"], 400);
                                    }
                                }
                            }

                            /* Ceck Delivery Date */
                            $resCheckDate = $this->checkOrderDeliveryDate($input, $order);
                            if( isset($resCheckDate['errors']) ){
                                return response()->json($resCheckDate, 400);
                            }

                            $order = $this->orderRepository->update($input, $order->id);

                            $cancellable_within = $this->setCancellableWithin($this->orderRepository->find($order->id));
                            $input['cancellable_within'] = $cancellable_within->format('Y-m-d H:i');

                            $order = $this->orderRepository->update($input, $order->id);

                            $order->shop; //simply adds order shop to object

                            if( env('APP_ENV') === 'production' ):
                                //send notification to User
							    $orderUser = $this->userRepository->find($order->user_id);
                                $orderUser->notify(new OrderConfirmedWithReserve(Auth::user(), $order));
                            endif;

						else:
							return response()->json(['errors' => 'This order does not belongs to your shop'], 400);
						endif;
					else:
						return response()->json(['errors' => 'You are not a company'], 400);
					endif;
					break;

				case 'confirmed':

					/** This action can be peformed only by the company user */
					if( Auth::user()->user_type == 'company' &&
						$order->shop_id == Auth::user()->shop->id &&
						$order->order_status_id == Config::get('constants.orders.not_confirmed') ):

                            $order = $this->orderRepository->find($input['order_id']);

                            $input['order_status_id'] = Config::get('constants.orders.confirmed');

                            $cancellable_within = $this->setCancellableWithin($this->orderRepository->find($order->id));
                            $input['cancellable_within'] = $cancellable_within->format('Y-m-d H:i');

							$order = $this->orderRepository->update($input, $order->id);

							$order->shop; //simply adds order shop to object

							/** Send Notification */
							if( Auth::user()->user_type == 'company' ){

                                if (env('APP_ENV') === 'production'):
                                    /** to User */
                                    $orderUser = $this->userRepository->find($order->user_id);
                                    $orderUser->notify(new OrderConfirmed(Auth::user(), $order));
                                endif;

							}else{

                                if (env('APP_ENV') === 'production'):
                                    /** to Shop */
                                    $shop = $this->shopRepository->find($order->shop_id);
                                    $shopUser = $this->userRepository->find($shop->user_id);
                                    $shopUser->notify(new OrderConfirmed(Auth::user(), $order));
                                endif;

							}

					else:
						return response()->json(['errors' => 'Action not allowed'], 400);
					endif;
					break;

				case 'cancelled':
                    $order = $this->orderRepository->find($input['order_id']);
					$error = $this->orderCancelled($order);
					if( $error == 1 ):
						return response()->json(['errors' => 'Action not allowed'], 400);
					endif;
					break;

				case 'delivered':
                    $order = $this->orderRepository->find($input['order_id']);
					$error = $this->orderDelivered($order);

					if( $error == 1 ):

						return response()->json(['errors' => 'Action not allowed'], 400);

					endif;
					break;

				case 'delivered_add_points':
                    $order = $this->orderRepository->find($input['order_id']);
                    $error = $this->orderDelivered($order, 1, 1);
                    if( $error === 1 ):
                        return response()->json(['errors' => 'Action not allowed'], 400);
                    endif;
					break;

				case 'delivered_subtract_points':
                    $order = $this->orderRepository->find($input['order_id']);
					$error = $this->orderDelivered($order, 0, 3);
                    if( $error === 1 ):
                        return response()->json(['errors' => 'Action not allowed'], 400);
                    endif;
					break;

				case 'not_delivered_subtract_points':
                    $order = $this->orderRepository->find($input['order_id']);
                    $error = $this->orderNotDelivered($order, 0, 3);
                    if( $error == 1 ):
                        return response()->json(['errors' => 'Action not allowed'], 400);
                    endif;
					break;

				case 'not_delivered_subtract_points_and_blacklist':
                    $order = $this->orderRepository->find($input['order_id']);
					$error = $this->orderNotDelivered($order, 0, 3, 1);
					if( !$error ):

                        $order->user->blacklistShops()->attach( $order->shop );

					elseif( $error == 1 ):

						return response()->json(['errors' => 'Action not allowed'], 400);

					endif;
					break;

			}

            /** OrderChat */
            if( isset($input['message']) && !empty($input['message']) ):

                $messageFrom = Auth::id() === $order->shop->user->id ? 1 : 0;

				$orderChat = [
					'order_id' => $order->id,
					'from' => $messageFrom,
					'message' => $input['message']
                ];

                $orderChat = $this->orderChatRepository->create($orderChat);
                if( $orderChat ):

                    if (env('APP_ENV') === 'production'):
                        /** Send Notification */
                        $orderUser = $order->user;
                        $orderUser->notify(new MessageReceived(Auth::user(), $order, $input['message'], $messageFrom));
                    endif;
                endif;

			endif;

            if( $order ):
                //$order->orderFoods;
                //$order->shop;
                //$order->user;
                $order = Order::where('id', $order->id)
                ->where('user_id', $user->id)
                ->with('shop')
                ->with('orderFoods.offerFood.shopCatalog.unitOfMeasure')
                ->with('orderFoods.offerFood.shopCatalog.unitOfMeasureBuy')
                ->with('orderFoods.shopCatalog.unitOfMeasure')
                ->with('orderFoods.shopCatalog.unitOfMeasureBuy')
                ->with('user')
                ->first();
                return response()->json(['success' => $order], 200);
            endif;

		}

    }

    public function checkHomeDelivery($input, $order){
        if( isset($input['home_delivery']) && $input['home_delivery'] == 1 && $order->shop->home_delivery == 1 ):

            $home_lat = $input['home_delivery_lat'];
            $home_long = $input['home_delivery_long'];

            $distance = haversineGreatCircleDistance($home_lat, $home_long, $order->shop->lat, $order->shop_long);
            if( $distance > 0 ):
                $distance = $distance / 1000;
            endif;

            if( $distance > $order->shop->delivery_range_km ):
                return response()->json(['errors' => 'Questa attività non effettua consegne a domicilio per distanze superiori a ' . $order->shop->delivery_range_km . ' Km'], 400);
            endif;

            foreach( $order->orderFoods as $orderFood ):
                if( !empty($orderFood->shop_catalog_id) ):

                    $shopCatalog = $this->shopCatalogRepository->find($orderFood->shop_catalog_id);
                    if( $shopCatalog ):
                        if( $shopCatalog->home_delivery == 0 ):
                            return response()->json(['errors' => 'Alcuni prodotti di questo ordine non vengono consegnati a domicilio'], 400);
                        endif;
                    else:
                        return response()->json(['errors' => 'Non è stato possibile trovare un prodotto nel catalogo', 'shop_catalog_id' => $orderFood->shop_catalog_id], 400);
                    endif;

                elseif( !empty($orderFood->shop_offer_food_id) ):

                    $offerFood = $this->shopOfferFoodRepository->find($orderFood->shop_offer_food_id);
                    if( $offerFood ):
                        if( $offerFood->shopCatalog ):
                            if( $offerFood->shopCatalog->home_delivery == 0 ):
                                return response()->json(['errors' => 'Alcuni prodotti di questo ordine non vengono consegnati a domicilio'], 400);
                            endif;
                        else:
                            return response()->json(['errors' => 'Non è stato possibile trovare un prodotto nel catalogo', 'shop_offer_food_id' => $orderFood->shop_offer_food_id], 400);
                        endif;
                    else:
                        return response()->json(['errors' => 'Non è stato possibile trovare un prodotto dell\'offerta'], 400);
                    endif;

                endif;
            endforeach;

        elseif( $input['home_delivery'] == 1 && $order->shop->home_delivery == 0 ):

            return response()->json(['errors' => 'Il negozio non effettua consegne a domicilio'], 400);

        endif;

        return null;
    }


    public function getBuyPrice($order_food){

        $price = false;

        if( $order_food->offerFood ):

            $unitOfMeasure = $order_food->offerFood->shopCatalog->unitOfMeasure;
            $unitOfMeasureBuy = $order_food->offerFood->shopCatalog->unitOfMeasureBuy ?? $unitOfMeasure;

            //If is an offer
            if( $order_food->quantity >= $order_food->offerFood->min_quantity ):

                //and the ordered quantity is greater or equal the min quantity of the offer
                $price = $order_food->quantity * $order_food->offerFood->discounted_price;

            else:

                //otherwise I take the catalog price
                $price = $order_food->quantity * $order_food->offerFood->shopCatalog->food_price;

            endif;

            $quantity = $order_food->offerFood->shopCatalog->food_unit_of_measure_quantity;

            $price = $price / $quantity;

            // Altrimenti se l'unità di misura di acquisto è quella del livello precedente
            if( $unitOfMeasure->id === $unitOfMeasureBuy->next_unit_of_measure ):

                $price = $price / $unitOfMeasureBuy->next_unit_of_measure_quantity;
                $quantity = $quantity * $unitOfMeasureBuy->next_unit_of_measure_quantity;

            elseif( $unitOfMeasure->id !== $unitOfMeasureBuy->id && $unitOfMeasure->id !== $unitOfMeasureBuy->next_unit_of_measure ):

                // Devo scorrere le unità di misura
                $uom_multiplier = null;

                $uom = $unitOfMeasureBuy;

                if( $uom->next_unit_of_measure ):
                    while( $unitOfMeasure->id !== $uom->next_unit_of_measure && !empty($uom->next_unit_of_measure) ):

                        if( empty($uom_multiplier) ):
                            $uom_multiplier = $uom->next_unit_of_measure_quantity;
                        else:
                            $uom_multiplier = $uom_multiplier * $uom->next_unit_of_measure_quantity;
                        endif;

                        $uom = $this->unitOfMeasureRepository->find($uom->next_unit_of_measure);

                    endwhile;
                endif;

                $uom_multiplier = $uom_multiplier * $uom->next_unit_of_measure_quantity;

                if( $unitOfMeasure->id === $uom->next_unit_of_measure ):
                    $price = $price / $uom_multiplier;
                    $quantity = $quantity * $uom_multiplier;
                endif;

            endif;

            $price = $price * $order_food->offerFood->shopCatalog->unit_of_measure_buy_quantity;

        elseif( $order_food->shopCatalog ):

            $unitOfMeasure = $order_food->shopCatalog->unitOfMeasure;
            $unitOfMeasureBuy = $order_food->shopCatalog->unitOfMeasureBuy ?? $unitOfMeasure;

            //otherwise I take the catalog price
            $price = $order_food->quantity * $order_food->shopCatalog->food_price;
            $quantity = $order_food->shopCatalog->food_unit_of_measure_quantity;
            $price = $price / $quantity;

            // L'unità di misura di acquisto è quella del livello precedente
            if( $unitOfMeasure->id === $unitOfMeasureBuy->next_unit_of_measure ):

                $price = $price / $unitOfMeasureBuy->next_unit_of_measure_quantity;

            elseif( $unitOfMeasure->id !== $unitOfMeasureBuy->id && $unitOfMeasure->id !== $unitOfMeasureBuy->next_unit_of_measure ):

                // Devo scorrere le unità di misura
                $uom_multiplier = null;

                $uom = $unitOfMeasureBuy;

                if( $uom->next_unit_of_measure ):
                    while( $unitOfMeasure->id !== $uom->next_unit_of_measure && !empty($uom->next_unit_of_measure) ):

                        if( empty($uom_multiplier) ):
                            $uom_multiplier = $uom->next_unit_of_measure_quantity;
                        else:
                            $uom_multiplier = $uom_multiplier * $uom->next_unit_of_measure_quantity;
                        endif;

                        $uom = $this->unitOfMeasureRepository->find($uom->next_unit_of_measure);

                    endwhile;
                endif;

                $uom_multiplier = $uom_multiplier * $uom->next_unit_of_measure_quantity;

                if( $unitOfMeasure->id === $uom->next_unit_of_measure ):
                    $price = $price / $uom_multiplier;
                endif;

            endif;

            $price = $price * $order_food->shopCatalog->unit_of_measure_buy_quantity;

        endif;

        return $price;
    }

    public function getTotalPrice($order){

        //update total price
        $order_foods = OrderFood::where('order_id', $order->id)->get();

        if( $order_foods->count() > 0){ //Cart empty

            $total_price = 0;
            foreach($order_foods as $food){
                //$total_price += $food->quantity*$food->price;
                $total_price += $food->price;
            }

            $res = $this->getShippingPrice($order, $total_price);

            $total_price += $res['shipping_price'] ?? 0;

            return [
                'status' => $res['status'],
                'error_msg' => $res['error_msg'],
                'shipping_price' => $res['shipping_price'] ?? 0,
                'total_price' => $total_price
            ];

        }

    }


	public function notConfirmed(){

		$user_id = Auth::user()->user_type === 'company' ? null : Auth::user()->id;
		$shop_id = Auth::user()->user_type === 'company' ? Auth::user()->shop->id : null;

		$orders = $this->orderRepository->notConfirmed($user_id, $shop_id);

		return response()->json(array('orders_not_confirmed' => $orders));

	}

	public function confirmed(){

		$user_id = Auth::user()->user_type === 'company' ? null : Auth::user()->id;
		$shop_id = Auth::user()->user_type === 'company' ? Auth::user()->shop->id : null;

		$orders = $this->orderRepository->confirmed($user_id, $shop_id);

		return response()->json(array('orders_not_confirmed' => $orders));

	}

	public function search(Request $request){
        $input = $request->all();

		$user_id = Auth::user()->user_type === 'company' ? null : Auth::user()->id;
		$shop_id = Auth::user()->user_type === 'company' ? Auth::user()->shop->id : null;

		if( $shop_id ):

			$search = null;
			if( isset($input['search'])):
				$search = !empty($input['search']) ? $input['search'] : null;
			endif;

			if( $input['confirmed'] == 1 ):
				$orders = $this->orderRepository->confirmed($user_id, $shop_id, $search);
			else:
				$orders = $this->orderRepository->notConfirmed($user_id, $shop_id, $search);
			endif;
        else:
            $orders = $this->orderRepository->list($user_id);
		endif;

		return response()->json(['orders_search' => $orders]);
	}

	private function orderDelivered($order, $add_trust_points = null, $trust_points = null)
	{
		if( Auth::user()->user_type == 'company' && $order->shop_id == Auth::user()->shop->id && $order->order_status_id == Config::get('constants.orders.confirmed') ):

			$input['order_status_id'] = Config::get('constants.orders.delivered');
			$order = $this->orderRepository->update($input, $order->id);

            $order->shop; //simply adds order shop to object

            if( $add_trust_points == 1 ):
                $this->addTrustPoints($order, $trust_points);
            elseif( $add_trust_points == 0 && !is_null($add_trust_points) ):
                $this->subtractTrustPoints($order, $trust_points);
            endif;


            if (env('APP_ENV') === 'production'):
                /** Send notifications both to User and Shop */
                $orderUser = $this->userRepository->find($order->user_id);
                $orderUser->notify(new OrderDelivered(Auth::user(), $order, $add_trust_points, $trust_points));

                /** to Shop */
                $shop = $this->shopRepository->find($order->shop_id);
                $shopUser = $this->userRepository->find($shop->user_id);
                $shopUser->notify(new OrderDelivered(Auth::user(), $order, $add_trust_points, $trust_points));
            endif;

			return 0;

		else:

			return 1;

		endif;
	}

	private function orderNotDelivered($order, $add_trust_points = null, $trust_points = null, $blacklist = null)
	{
		if( Auth::user()->user_type == 'company' &&
			$order->shop_id == Auth::user()->shop->id &&
			$order->order_status_id == Config::get('constants.orders.confirmed') ):

			$input['order_status_id'] = Config::get('constants.orders.cancelled');
			$order = $this->orderRepository->update($input, $order->id);

            $order->shop; //simply adds order shop to object

            if( $add_trust_points == 1 ):
                $this->addTrustPoints($order, $trust_points);
            elseif( $add_trust_points == 0 ):
                $this->subtractTrustPoints($order, $trust_points);
            endif;

            if (env('APP_ENV') === 'production'):
                /** Send notifications both to User and Shop */
                $orderUser = $this->userRepository->find($order->user_id);
                $orderUser->notify(new OrderNotDelivered(Auth::user(), $order, $add_trust_points, $trust_points, $blacklist ?? 0));

                /** to Shop
                $shop = $this->shopRepository->find($order->shop_id);
                $shopUser = $this->userRepository->find($shop->user_id);
                $shopUser->notify(new OrderNotDelivered(Auth::user(), $order, $add_trust_points, $trust_points));
                */
            endif;

			return 0;

		else:

			return 1;

		endif;
	}

	private function orderCancelled($order)
	{
        if( is_object($order) ):
            if( $order->can_cancel ):

                $input['order_status_id'] = Config::get('constants.orders.cancelled');
                $order = $this->orderRepository->update($input, $order->id);

                $order->shop; //simply adds order shop to object

                if (env('APP_ENV') === 'production'):
                    /** Send notifications both to User and Shop */
                    $orderUser = $this->userRepository->find($order->user_id);
                    $orderUser->notify(new OrderCancelled(Auth::user(), $order));

                    /** to Shop */
                    $shop = $this->shopRepository->find($order->shop_id);
                    $shopUser = $this->userRepository->find($shop->user_id);
                    $shopUser->notify(new OrderCancelled(Auth::user(), $order));
                endif;

                return 0;

            else:

                return 1;

            endif;
        endif;
	}

	private function addTrustPoints($order, $trust_points)
	{

		$user = $this->userRepository->find($order->user_id);

        if( $user ):
            $points = $user->trust_points + $trust_points;

			if( $points > 100 ) $points = 100;

            $user = $this->userRepository->update([
                'trust_points' => $points,
            ], $user->id);

            $order = $this->orderRepository->update([
				'flag_trust_points' => 'add'
			], $order->id);

			$order->shop;

			//$user->notify(new GivenPoints(Auth::user(), 1, $order));
			return 0;

        else:

            if( empty($user) ):
                return 1;
            endif;

        endif;
	}

	private function subtractTrustPoints($order, $trust_points)
	{

		$user = $this->userRepository->find($order->user_id);

        if( $user ):
            $points = $user->trust_points - $trust_points;

			if( $points < 0 ) $points = 0;

            $user = $this->userRepository->update([
                'trust_points' => $points,
            ], $user->id);

            $order = $this->orderRepository->update([
				'flag_trust_points' => 'subtract'
			], $order->id);

			$order->shop;

			//$user->notify(new RemovedPoints(Auth::user(), 3, $order));
			return 0;

        else:

            if( empty($user) ):
                return 1;
            endif;

        endif;
    }

    private function getShippingPrice($order, $total_price){

        $status = 'ok';
        $error_msg = '';

        $shop = $order->shop;

        if( $shop->home_delivery == 1 && $order->home_delivery == 1 ):

            if( $shop->home_delivery_min > 0 && $total_price < $shop->home_delivery_min ):

                $status = 'error';
                $error_msg = "Non hai raggiunto l'importo minimo richiesto di " . number_format($shop->home_delivery_min, 2, ',', '.') . " € per la consegna a domicilio";

            endif;

            if( $shop->delivery_free_price_greater_than > 0 && $total_price > $shop->delivery_free_price_greater_than ):

                return [
                    'status' => $status,
                    'error_msg' => $error_msg,
                    'shipping_price' => 0
                ];

            elseif( $shop->delivery_percentage_cost_price > 0 ):

                $shipping_price = ($total_price * $shop->delivery_percentage_cost_price) / 100;

                return [
                    'status' => $status,
                    'error_msg' => $error_msg,
                    'shipping_price' => $shipping_price
                ];

            endif;

            return [
                    'status' => $status,
                    'error_msg' => $error_msg,
                    'shipping_price' => $shop->delivery_forfait_cost_price ?? 0
                ];

        else:

            return [
                    'status' => $status,
                    'error_msg' => $error_msg,
                    'shipping_price' => 0
                ];

        endif;
    }

    private function checkClosedShop($input, $order){

        $shopStatus = $order->shop->shop_status;
        $shopStatusOrders = $order->shop->shop_status_orders;

        if( $shopStatus !== 'Aperto' ):
            // Se l'attività è chiusa
            if( !$shopStatusOrders ):
                /* E se l'attività non accetta più ordini per i giorni di chiusura
                *  Verifico se la data scelta per la consegna
                *  ricade nei giorni di chiusura dell'attività
                */
                $closed_date_start = $order->shop->closed_date_start;
                $closed_date_end = $order->shop->closed_date_end;

                $delivery_date = null;

                if( $input['pickup_on_site'] == 1 ):
                    $delivery_date = $input['pickup_on_site_date'];
                elseif( $input['home_delivery'] == 1 ):
                    $delivery_date = $input['home_delivery_date'];
                elseif( $input['delivery_host_on_site'] == 1 ):
                    $delivery_date = $input['delivery_host_on_site_date'];
                endif;

                if( $delivery_date->gte($closed_date_start) && $delivery_date->lte($closed_date_end) ):
                    return ['errors' => 'Il negozio è ' . strtolower($shopStatus) . ' nella data di consegna selezionata'];
                endif;

            endif;
        endif;

        return ['success' => 'ok'];

    }

	private function checkOrderDeliveryDate($input, $order){
		/** Check if Shop is Open */

		$weekMap = [
			0 => 'sunday',
			1 => 'monday',
			2 => 'tuesday',
			3 => 'wednesday',
			4 => 'thursday',
			5 => 'friday',
			6 => 'saturday',
		];

		if( $input['pickup_on_site'] == 1 ):
			// Se ritiro in sede, verifico l'orario di apertura del negozio
			$weekday = $weekMap[Carbon::parse($input['pickup_on_site_date'])->dayOfWeek];

			if( !$order->shop->obj_opening_timetable->{$weekday}->active ):
                return ['errors' => 'Il negozio non effettua consegne nella data selezionata'];
			else:

				$morning_open    = !empty($order->shop->obj_opening_timetable->{$weekday}->first_half->opening_time) ? Carbon::parse($order->shop->obj_opening_timetable->{$weekday}->first_half->opening_time) : null;
				$morning_close   = !empty($order->shop->obj_opening_timetable->{$weekday}->first_half->closing_time) ? Carbon::parse($order->shop->obj_opening_timetable->{$weekday}->first_half->closing_time) : null;
				$afternoon_open  = !empty($order->shop->obj_opening_timetable->{$weekday}->second_half->opening_time) ? Carbon::parse($order->shop->obj_opening_timetable->{$weekday}->second_half->opening_time) : null;
                $afternoon_close = !empty($order->shop->obj_opening_timetable->{$weekday}->second_half->closing_time) ? Carbon::parse($order->shop->obj_opening_timetable->{$weekday}->second_half->closing_time) : null;

                if( $morning_open === $morning_close ):
                    $morning_open = null;
                    $morning_close = null;
                endif;

                if( $afternoon_open === $afternoon_close ):
                    $afternoon_open = null;
                    $afternoon_close = null;
                endif;

                $time_error_msg = "Il negozio non effettua consegne nell'orario selezionato. ";
                $time_error_msg .= "Gli orari per il " . Carbon::parse($input['pickup_on_site_date'])->format('d/m/Y') . " sono i seguenti: \n";
                if(  $morning_close->format('H:i') === $afternoon_open->format('H:i') ):
                    $time_error_msg .= "Orario continuato: " . $morning_open->format('H:i') . "-" . $afternoon_close->format('H:i');
                else:
                    if( $morning_open->format('H:i') !== $morning_close->format('H:i') ):
                        $time_error_msg .= "Mattina: " . $morning_open->format('H:i') . '-' . $morning_close->format('H:i') . "\n";
                    endif;

                    if( $afternoon_open->format('H:i') !== $afternoon_close->format('H:i') ):
                        $time_error_msg .= "Pomeriggio: " . $afternoon_open->format('H:i') . '-' . $afternoon_close->format('H:i');
                    endif;
                endif;

                $pickup_on_site_time = Carbon::parse($input['pickup_on_site_time']);

                if( !empty($morning_open) && !empty($morning_close) && !empty($afternoon_open) && !empty($afternoon_close) ):

					if( $pickup_on_site_time->isBefore($morning_open) ||
						(
							$pickup_on_site_time->isAfter($morning_close) &&
							$pickup_on_site_time->isBefore($afternoon_open)
						) ||
						$pickup_on_site_time->isAfter($afternoon_close) ):

						return ['errors' => $time_error_msg];

					endif;

				elseif( !empty($morning_open) && !empty($morning_close) && empty($afternoon_open) && empty($afternoon_close) ):

					if( $pickup_on_site_time->isBefore($morning_open) ||
						$pickup_on_site_time->isAfter($morning_close) ):

						return ['errors' => $time_error_msg];

					endif;

				elseif( empty($morning_open) && empty($morning_close) && !empty($afternoon_open) && !empty($afternoon_close) ):

					if( $pickup_on_site_time->isBefore($afternoon_open) ||
						$pickup_on_site_time->isAfter($afternoon_close) ):

						return ['errors' => $time_error_msg];

					endif;

				elseif( !empty($morning_open) && empty($morning_close) && empty($afternoon_open) && !empty($afternoon_close) ):

					if( $pickup_on_site_time->isBefore($morning_open) ||
						$pickup_on_site_time->isAfter($afternoon_close) ):

						return ['errors' => $time_error_msg];

					endif;

				endif;

            endif;

		elseif( $input['home_delivery'] == 1 ):
			// Se ritiro in sede, verifico l'orario di apertura del negozio
			$weekday = $weekMap[Carbon::parse($input['home_delivery_date'])->dayOfWeek];

			if( !$order->shop->obj_delivery_opening_timetable->{$weekday}->active ):
                return ['errors' => 'Il negozio non effettua consegne nella data selezionata'];
			else:

				$morning_open    = !empty($order->shop->obj_delivery_opening_timetable->{$weekday}->first_half->opening_time) ? Carbon::parse($order->shop->obj_delivery_opening_timetable->{$weekday}->first_half->opening_time) : null;
				$morning_close   = !empty($order->shop->obj_delivery_opening_timetable->{$weekday}->first_half->closing_time) ? Carbon::parse($order->shop->obj_delivery_opening_timetable->{$weekday}->first_half->closing_time) : null;
				$afternoon_open  = !empty($order->shop->obj_delivery_opening_timetable->{$weekday}->second_half->opening_time) ? Carbon::parse($order->shop->obj_delivery_opening_timetable->{$weekday}->second_half->opening_time) : null;
                $afternoon_close = !empty($order->shop->obj_delivery_opening_timetable->{$weekday}->second_half->closing_time) ? Carbon::parse($order->shop->obj_delivery_opening_timetable->{$weekday}->second_half->closing_time) : null;

                if( $morning_open === $morning_close ):
                    $morning_open = null;
                    $morning_close = null;
                endif;

                if( $afternoon_open === $afternoon_close ):
                    $afternoon_open = null;
                    $afternoon_close = null;
                endif;

                $time_error_msg = "Il negozio non effettua consegne nell'orario selezionato. ";
                $time_error_msg .= "Gli orari per il " . Carbon::parse($input['home_delivery_date'])->format('d/m/Y') . " sono i seguenti: \n";
                if(  $morning_close->format('H:i') === $afternoon_open->format('H:i') ):
                    $time_error_msg .= "Orario continuato: " . $morning_open->format('H:i') . "-" . $afternoon_close->format('H:i');
                else:
                    if( $morning_open->format('H:i') !== $morning_close->format('H:i') ):
                        $time_error_msg .= "Mattina: " . $morning_open->format('H:i') . '-' . $morning_close->format('H:i') . "\n";
                    endif;

                    if( $afternoon_open->format('H:i') !== $afternoon_close->format('H:i') ):
                        $time_error_msg .= "Pomeriggio: " . $afternoon_open->format('H:i') . '-' . $afternoon_close->format('H:i');
                    endif;
                endif;

				$home_delivery_time = Carbon::parse($input['home_delivery_time']);

				if( !empty($morning_open) && !empty($morning_close) && !empty($afternoon_open) && !empty($afternoon_close) ):

					if( $home_delivery_time->isBefore($morning_open) ||
						(
							$home_delivery_time->isAfter($morning_close) &&
							$home_delivery_time->isBefore($afternoon_open)
						) ||
						$home_delivery_time->isAfter($afternoon_close) ):
						return ['errors' => $time_error_msg];

					endif;

				elseif( !empty($morning_open) && !empty($morning_close) && empty($afternoon_open) && empty($afternoon_close) ):

					if( $home_delivery_time->isBefore($morning_open) ||
						$home_delivery_time->isAfter($morning_close) ):

						return ['errors' => $time_error_msg];

					endif;

				elseif( empty($morning_open) && empty($morning_close) && !empty($afternoon_open) && !empty($afternoon_close) ):

					if( $home_delivery_time->isBefore($afternoon_open) ||
						$home_delivery_time->isAfter($afternoon_close) ):

						return ['errors' => $time_error_msg];

					endif;

				elseif( !empty($morning_open) && empty($morning_close) && empty($afternoon_open) && !empty($afternoon_close) ):

					if( $home_delivery_time->isBefore($morning_open) ||
						$home_delivery_time->isAfter($afternoon_close) ):

						return ['errors' => $time_error_msg];

					endif;

				endif;

            endif;

		elseif( $input['delivery_host_on_site'] == 1 ):
			// Se ritiro in sede, verifico l'orario di apertura del negozio
			$weekday = $weekMap[Carbon::parse($input['delivery_host_on_site_date'])->dayOfWeek];

			if( !$order->shop->obj_delivery_opening_timetable->{$weekday}->active ):
                return ['errors' => 'Il negozio non effettua consegne nella data selezionata'];
			else:

				$morning_open    = !empty($order->shop->obj_delivery_opening_timetable->{$weekday}->first_half->opening_time) ? Carbon::parse($order->shop->obj_delivery_opening_timetable->{$weekday}->first_half->opening_time) : null;
				$morning_close   = !empty($order->shop->obj_delivery_opening_timetable->{$weekday}->first_half->closing_time) ? Carbon::parse($order->shop->obj_delivery_opening_timetable->{$weekday}->first_half->closing_time) : null;
				$afternoon_open  = !empty($order->shop->obj_delivery_opening_timetable->{$weekday}->second_half->opening_time) ? Carbon::parse($order->shop->obj_delivery_opening_timetable->{$weekday}->second_half->opening_time) : null;
				$afternoon_close = !empty($order->shop->obj_delivery_opening_timetable->{$weekday}->second_half->closing_time) ? Carbon::parse($order->shop->obj_delivery_opening_timetable->{$weekday}->second_half->closing_time) : null;

                if( $morning_open === $morning_close ):
                    $morning_open = null;
                    $morning_close = null;
                endif;

                if( $afternoon_open === $afternoon_close ):
                    $afternoon_open = null;
                    $afternoon_close = null;
                endif;

                $time_error_msg = "Il negozio non effettua consegne nell'orario selezionato. ";
                $time_error_msg .= "Gli orari per il " . $input['delivery_host_on_site_date']->format('d/m/Y') . " sono i seguenti: \n";
                if(  $morning_close->format('H:i') === $afternoon_open->format('H:i') ):
                    $time_error_msg .= "Orario continuato: " . $morning_open->format('H:i') . "-" . $afternoon_close->format('H:i');
                else:
                    if( $morning_open->format('H:i') !== $morning_close->format('H:i') ):
                        $time_error_msg .= "Mattina: " . $morning_open->format('H:i') . '-' . $morning_close->format('H:i') . "\n";
                    endif;

                    if( $afternoon_open->format('H:i') !== $afternoon_close->format('H:i') ):
                        $time_error_msg .= "Pomeriggio: " . $afternoon_open->format('H:i') . '-' . $afternoon_close->format('H:i');
                    endif;
                endif;

				$delivery_host_on_site_time = Carbon::parse($input['delivery_host_on_site_time']);

				if( !empty($morning_open) && !empty($morning_close) && !empty($afternoon_open) && !empty($afternoon_close) ):

					if( $delivery_host_on_site_time->isBefore($morning_open) ||
						(
							$delivery_host_on_site_time->isAfter($morning_close) &&
							$delivery_host_on_site_time->isBefore($afternoon_open)
						) ||
						$delivery_host_on_site_time->isAfter($afternoon_close) ):

						return ['errors' => $time_error_msg];

					endif;

				elseif( !empty($morning_open) && !empty($morning_close) && empty($afternoon_open) && empty($afternoon_close) ):

					if( $delivery_host_on_site_time->isBefore($morning_open) ||
						$delivery_host_on_site_time->isAfter($morning_close) ):

						return ['errors' => $time_error_msg];

					endif;

				elseif( empty($morning_open) && empty($morning_close) && !empty($afternoon_open) && !empty($afternoon_close) ):

					if( $delivery_host_on_site_time->isBefore($afternoon_open) ||
						$delivery_host_on_site_time->isAfter($afternoon_close) ):

						return ['errors' => $time_error_msg];

					endif;

				elseif( !empty($morning_open) && empty($morning_close) && empty($afternoon_open) && !empty($afternoon_close) ):

					if( $delivery_host_on_site_time->isBefore($morning_open) ||
						$delivery_host_on_site_time->isAfter($afternoon_close) ):

						return ['errors' => $time_error_msg];

					endif;

				endif;

			endif;

		endif;

		return ['success' => 'ok'];
    }


    public function setCancellableWithin($order){

        $weekMap = [
			0 => 'sunday',
			1 => 'monday',
			2 => 'tuesday',
			3 => 'wednesday',
			4 => 'thursday',
			5 => 'friday',
			6 => 'saturday',
		];

        $delivery_date = null;

        /*
        if( $order->pickup_on_site == 1 ){
            $weekday = $weekMap[$order->pickup_on_site_date->dayOfWeek];
        }

        if( $order->home_delivery == 1){
            $weekday = $weekMap[$order->home_delivery_date->dayOfWeek];
        }

        if( $order->delivery_host_on_site == 1 ){
            $weekday = $weekMap[$order->delivery_host_on_site_date->dayOfWeek];
        }

        $time_limit = "24:00";

        if( $order->shop->obj_opening_timetable->{$weekday}->active ):
            if( $order->shop->obj_opening_timetable->{$weekday}->second_half->closing_time ):
                $time_limit = $order->shop->obj_opening_timetable->{$weekday}->second_half->closing_time;
            elseif( $order->shop->obj_opening_timetable->{$weekday}->first_half->closing_time ):
                $time_limit = $order->shop->obj_opening_timetable->{$weekday}->first_half->closing_time;
            endif;
        endif;
        */

        /*
        if( $time_limit === "24:00"){
            $time_limit = "23:59";
        }
        */

        if( $order->pickup_on_site == 1 ){
            if( empty($order->pickup_on_site_within) ){
                $datetime = $order->pickup_on_site_date->format('Y-m-d') . ' ' . $order->pickup_on_site_time;
                //$datetime = $order->pickup_on_site_date->format('Y-m-d') . ' ' . $time_limit;
            }else{
                $datetime = $order->pickup_on_site_within->format('Y-m-d H:i');
            }
        }

        if( $order->home_delivery == 1){
            if( empty($order->home_delivery_within)){
                if( $order->home_delivery_date ) $datetime = $order->home_delivery_date->format('Y-m-d') . ' ' . $order->home_delivery_time;
                //$datetime = $order->home_delivery_date->format('Y-m-d') . ' ' . $time_limit;
            }else{
                $datetime = $order->home_delivery_within->format('Y-m-d H:i');
            }
        }

        if( $order->delivery_host_on_site == 1 ){
            if( empty($order->delivery_host_on_site_within) ){
                if( $order->home_delivery_date ) $datetime = $order->delivery_host_on_site_date->format('Y-m-d') . ' ' . $order->delivery_host_on_site_time;
                //$datetime = $order->delivery_host_on_site_date->format('Y-m-d') . ' ' . $time_limit;
            }else{
                $datetime = $order->delivery_host_on_site_within->format('Y-m-d H:i');
            }
        }

        if( $datetime ){
            $delivery_date  = Carbon::parse($datetime);
            return $delivery_date->subHours($order->shop->delivery_cancellable_hours_limit);
        }else{
            return Carbon::now()->addMinutes(10); //non dovrebbe mai passare di qui, messi 10 minuti per non mandarlo in errore
        }

    }

    private function checkDevliveryDistance($input, $order){
        $res = [];
        $res['status'] = 'success';
        $res['error_msg'] = '';

        // Se Consegna a domicilio
        // verifico che l'indirizzo impostato per la consegna
        // rientri nel raggio impostato dal negozio
        if( $order->home_delivery == 1 ){
            if( !empty($input['home_delivery_lat']) && !empty($input['home_delivery_long']) ){

                // Calcolo la distanza in km tra l'indirizzo della consegna
                // e l'indirizzo del negozio
                $distance = haversineGreatCircleDistance($input['home_delivery_lat'],
                                                         $input['home_delivery_long'],
                                                         $order->shop->lat,
                                                         $order->shop->long);

                if( $distance > $order->shop->delivery_range_km ){
                    $res['status'] = 'error';
                    $res['error_msg'] = "L'indirizzo impostato per la consegna si trova a " . round($distance) . " Km dall'attività che però effettua consegne solo fino ad un raggio di " . $order->shop->delivery_range_km . " Km";
                }
            }else{
                $res['status'] = 'error';
                $res['error_msg'] = "L'indirizzo per la consegna non è completo";
            }
        }
        return [
            'status' => $res['status'],
            'error_msg' => $res['error_msg']
        ];
    }

    public function pdf($order_id){
        $order = $this->orderRepository->find($order_id);

        if( $order ):
            if( Auth::id() == $order->shop->user->id || Auth::id() == $order->user_id ):

                $folder = '/storage/invoices/orders/'.$order->user_id;
                $filename = 'Primascelta_'.$order->id.'.pdf';

                $path = public_path($folder.'/'.$filename);

                if( !is_dir(public_path($folder)) ):
                    mkdir(public_path($folder));
                endif;

                PDF::loadView('orders.pdf', ['order' => $order])->save($path);

                $url = $folder.'/'.$filename;
                return url($url);
            else:
                return response()->json(['msg' => 'Non puoi visualizzare quest\'ordine'], 400);
            endif;
        else:
            return response()->json(['msg' => 'Ordine non trovato'], 400);
        endif;
    }

    public function cancelCopy($order_id){
        $order = $this->orderRepository->find($order_id);
        if( $order->user_id != Auth::id() ){
            return response()->json(['not_allowed' => "Non hai i permessi per modificare quest'ordine"], 200);
        }

        $order->order_status_id = Config::get('constants.orders.cancelled');
        $order->save();

        if (env('APP_ENV') === 'production'):
            /** Send notifications both to User and Shop */
            $orderUser = $this->userRepository->find($order->user_id);
            $orderUser->notify(new OrderNotDeliveredCopy(Auth::user(), $order, null, 0, 0));

            /** to Shop */
            $shop = $this->shopRepository->find($order->shop_id);
            $shopUser = $this->userRepository->find($shop->user_id);
            $shopUser->notify(new OrderCancelled(Auth::user(), $order, null, 0));

        endif;

        /** Verifico la presenza di un carrello */
        $cart = Order::where('user_id', auth()->user()->id)
                     ->where('order_status_id', Config::get('constants.orders.draft'));

        //Cancello il vecchio carrello
        if( $cart ){
            $cart->delete();
        }

        // Creo una copia dell'ordine
        $newOrder = $order->toArray();
        unset($newOrder['id']);
        $newOrder['order_status_id']=Config::get('constants.orders.draft');

        $cart = Order::create($newOrder);

        foreach($order->orderFoods as $orderFood){
            $cartOrderFood = $orderFood->toArray();
            unset($cartOrderFood['id']);
            $cartOrderFood['order_id'] = $cart->id;
            OrderFood::create($cartOrderFood);
        }

        $cart->orderFoods;
        $cart->shop->shopTypes;
        $cart->user;
        return response()->json(['success' => $cart], 200);
    }

    public function emptyCart(Request $request){
        if($user = auth()->user()) {
            $input = $request->all();
            $order = Order::where('id', $input['order_id'])
                            ->where('order_status_id', 1)
                            ->first();
            if( $order && $order->user_id == $user->id ){

                OrderFood::where('order_id', $order->id)->delete();

                $order->orderFoods;
                $order->shop;
                $order->user;

                return response()->json(['success' => $order], 200);

            }else{
                return response()->json(['msg' => 'Azione non permessa!'], 400);
            }
        }
    }
}
