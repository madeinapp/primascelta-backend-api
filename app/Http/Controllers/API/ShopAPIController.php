<?php

namespace App\Http\Controllers\API;

use Illuminate\Support\Facades\Auth;
use App\Http\Requests\API\CreateShopAPIRequest;
use App\Http\Requests\API\UpdateShopAPIRequest;
use App\Models\User;
use App\Models\Agent;
use App\Models\Food;
use App\Models\Shop;
use App\Models\ShopType;
use App\Models\ShopCatalog;
use App\Models\ShopOffer;
use App\Models\ShopOfferFood;
use App\Models\PaymentMethod;
use App\Models\UnitOfMeasure;
use App\Models\TaxType;
use App\Repositories\ShopRepository;
use App\Repositories\ShopOfferRepository;
use App\Repositories\ShopOfferFoodRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Repositories\ShopCatalogRepository;
use App\Repositories\ShopTypeRepository;
use App\Repositories\SpecialRepository;
use App\Repositories\UnitOfMeasureRepository;
use App\Repositories\UserRepository;
use Response;
use Carbon\Carbon;
use DB;
use Log;
use smsCls;

/**
 * Class ShopController
 * @package App\Http\Controllers\API
 */

class ShopAPIController extends AppBaseController
{
    /** @var  ShopRepository */
    private $shopRepository,
            $shopOfferRepository,
            $shopOfferFoodRepository,
            $unitOfMeasureRepository,
            $userRepository;

    public function __construct(ShopRepository $shopRepo,
                                ShopOfferRepository $shopOfferRepository,
                                ShopOfferFoodRepository $shopOfferFoodRepository,
                                UnitOfMeasureRepository $unitOfMeasureRepository,
                                UserRepository $userRepository
                                )
    {
        $this->shopRepository = $shopRepo;
        $this->shopOfferRepository = $shopOfferRepository;
        $this->shopOfferFoodRepository = $shopOfferFoodRepository;
        $this->unitOfMeasureRepository = $unitOfMeasureRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * Display a listing of the Shop.
     * GET|HEAD /shops
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $shops = $this->shopRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($shops->toArray(), 'Shops retrieved successfully');
    }

    /**
     * Get Shop Type
     */
    public function getShopTypeList()
    {
        $types = ShopType::orderBy('name')->get();
        return $this->sendResponse($types->toJson(), 'Shops types ');
    }


    public function getShopTypeListDistinct()
    {
        $types = ShopType::orderBy('name')->get();

        $list = [];

        foreach($types as $type):

            $appo = explode("/", $type->name);
            foreach( $appo as $t ):
                $list[] = utf8_decode(trim($t));
            endforeach;

        endforeach;

        sort($list);
        $list = array_map( 'strtolower', $list );
        $list = array_map( 'ucfirst', $list );
        $list = array_unique($list);
        $list = array_values($list);

        $shopTypes = [];
        foreach($list as $k => $l):
            $el = [];
            $el['id'] = $k;
            $el['name'] = utf8_encode($l);
            $shopTypes[] = $el;
        endforeach;

        return $this->sendResponse($shopTypes, 'Shops types');
    }

	/**
     * Get Payment Methods
     */
    public function getShopPaymentMethodList()
    {
        $types = PaymentMethod::orderBy('name')->get();
        return $this->sendResponse($types->toArray(), 'Shops payment methods ');
    }

    /**
     * Get Tax Type
     */
    public function getTaxTypeList()
    {
        $types = TaxType::orderBy('name')->get();
        return $this->sendResponse($types->toArray(), 'Tax types ');
    }


    /**
     * Display the specified Shop.
     * GET|HEAD /shops/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Shop $shop */
        $shop = $this->shopRepository->shopDetail($id);

        if (empty($shop)) {
            return $this->sendError('Shop not found');
        }

        $coords = coordsFromAddress($shop->address);

		//Payments
		$obj_payments = DB::table('payment_method_shop')->where('shop_id', $id)->orderBy('payment_for')->get();
		$arr_payment = [];
  	    foreach($obj_payments as $k => $payment){
            $payment_method = PaymentMethod::find($payment->payment_method_id);
            if( $payment_method ):
                $arr_payment[$k]['payment_method_name'] = $payment_method->name;
                $arr_payment[$k]['payment_method_id'] = $payment->payment_method_id;
                $arr_payment[$k]['payment_for'] = $payment->payment_for;
            endif;
		}
        $shop->payment_methods = $arr_payment;

        $shop['image_url'] = getResizedImage(getOriginalImage($shop->user->image_url), '640x320');

        $shop['favourite'] = 0;
        if( Auth::user()->favouriteShops->contains('id', $shop->id) ):
            $shop['favourite'] = 1;
        endif;

        if( isset($shop['user'])) unset($shop['user']);

        $arr_shop = $shop->toArray();

        $arr_shop['closed_date_start'] = $shop->closed_date_start ? $shop->closed_date_start->format('d/m/Y') : '';
        $arr_shop['closed_date_end'] = $shop->closed_date_end ? $shop->closed_date_end->format('d/m/Y') : '';

        /*
        foreach($arr_shop['shop_catalogs'] as $k => $shop_catalog){
            if( $arr_shop['shop_catalogs'][$k]['visible_in_search'] ){
                if( !empty($arr_shop['shop_catalogs'][$k]['food_image']) ){
                    $arr_shop['shop_catalogs'][$k]['food_image'] = getResizedImage(getOriginalImage($arr_shop['shop_catalogs'][$k]['food_image']), '512x340');
                }
            }else{
                Log::info("Cancello prodotto non attivo");
                Log::info($arr_shop['shop_catalogs'][$k]["food_name"]);
                unset($arr_shop['shop_catalogs'][$k]);
                $arr_shop['shop_catalogs'] = array_values($arr_shop['shop_catalogs']);
            }
        }

        Log::info("arr_shop");
        Log::info($arr_shop);
        */

        unset($arr_shop['shop_catalogs']);

        return $this->sendResponse($arr_shop, 'Shop retrieved successfully');
    }

    /**
     * Update the specified Shop in storage.
     * PUT/PATCH /shops/{id}
     *
     * @param int $id
     * @param UpdateShopAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateShopAPIRequest $request)
    {
        $input = $request->all();

        /** @var Shop $shop */
        $shop = $this->shopRepository->find($id);

        if (empty($shop)) {
            return $this->sendError('Shop not found');
        }

        $shop = $this->shopRepository->update($input, $id);

        return $this->sendResponse($shop->toArray(), 'Negozio modificato con successo');
    }

    /**
     * Remove the specified Shop from storage.
     * DELETE /shops/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Shop $shop */
        $shop = $this->shopRepository->find($id);

        if (empty($shop)) {
            return $this->sendError('Shop not found');
        }

        $shop->delete();

        return $this->sendSuccess('Shop deleted successfully');
    }

    /**
     * Display a listing of the Shop searched.
     * GET|HEAD /shops
     *
     * @param Request $request
     * @return Response
	 */

	public function search(Request $request)
	{
        $input = $request->all();

        Log::info("Shop search");
        Log::info($input);

        if( !isset($input['name']) )                $input['name'] = null;
        if( !isset($input['food_name']) )           $input['food_name'] = null;
        if( !isset($input['specials']) )            $input['specials'] = null;
        if( !isset($input['shop_type_id']) )        $input['shop_type_id'] = null;
        if( !isset($input['delivery_on_site']) )    $input['delivery_on_site'] = null;
        if( !isset($input['home_delivery']) )       $input['home_delivery'] = null;
        if( !isset($input['discount']) )            $input['discount'] = null;

		//Unit of measure list
		$arr_unit_of_measure = [];
		$query_unit_of_measure = UnitOfMeasure::orderBy('name')->get();
		foreach($query_unit_of_measure as $um){
			$arr_unit_of_measure[$um->id] = $um->name;
		}
        //Get shop list
        $query_shops = Shop::select('user_id', 'shops.id', 'shops.name', 'address', 'lat', 'long', 'shops.delivery_on_site', 'shops.home_delivery', 'shops.delivery_host_on_site', 'shops.show_phone_on_app');

        $query_shops = $query_shops->where('status', 'active');

        if($input['radius'] == 0 || empty($input['radius'])){
            $radius = 2000; // Valore di default per cercare in tutta Italia
        }else{
            $radius = intval($input['radius']);
        }

        $orderByDistance = false;

        //Search by position
        if(!empty($input['lat']) && !empty($input['lon'])){
            $coordinates['latitude'] = $input['lat'];
            $coordinates['longitude'] = $input['lon'];
            $haversine = "(6371 * acos(cos(radians(" . $coordinates['latitude'] . "))
                            * cos(radians(shops.`lat`))
                            * cos(radians(shops.`long`)
                            - radians(" . $coordinates['longitude'] . "))
                            + sin(radians(" . $coordinates['latitude'] . "))
                            * sin(radians(shops.`lat`))))";

            $query_shops = $query_shops->selectRaw("{$haversine} AS distance")->whereRaw("{$haversine} < ?", [$radius]);
            $query_shops = $query_shops->withCount('shopCatalogs')
                                       ->having('shop_catalogs_count', '>', 0);
            $orderByDistance = true;
        }

        //Shop Name
        if(!empty($input['name']) && strtolower($input['name']) !== 'null' ){
            $query_shops = $query_shops->where('shops.name', 'like', '%'.$input['name'].'%');
        }
        //Shop type
        if( !empty($input['shop_type_id']) && strtolower($input['shop_type_id']) !== 'null' ){
            $shopType = $input['shop_type_id'];
            $query_shops = $query_shops->whereHas('shopTypes', function($shop_type) use($shopType) {
                $shop_type->where('name', 'like', "%$shopType%");
            });
        }
        //Delivery on site
        if(!empty($input['delivery_on_site']) && $input['delivery_on_site'] == 1){
            $query_shops = $query_shops->where('delivery_on_site', 1);
        }
        //Home delivery
        if(!empty($input['home_delivery']) && $input['home_delivery'] == 1){
            $query_shops = $query_shops->where('home_delivery', 1);
        }
        //Delivery on site
        if(!empty($input['delivery_host_on_site']) && $input['delivery_host_on_site'] == 1 ){
            $query_shops = $query_shops->where('delivery_host_on_site', 1);
        }

        $query_shops = $query_shops->whereHas('user', function($user){
            $user->whereHas('subscription', function($subscription){
                $subscription->where('expire_date', '>', 'CURDATE()');
            });
        });

        if(!empty($orderByDistance)){
            $query_shops = $query_shops->orderBy('distance', 'ASC');
        }

        $shops = $query_shops->get();

		//Query Foods in offer
		$query_foods = ShopCatalog::select('shop_catalogs.id', 'shop_id', 'food_name', 'food_price', 'food_image', 'food_unit_of_measure_quantity', 'unit_of_measures.name as um_name')
                                    ->leftJoin('unit_of_measures', 'unit_of_measures.id', '=', 'unit_of_measure_id')
                                    ->where('shop_catalogs.visible_in_search', 1)
                                    ->whereNotNull('shop_catalogs.id')
                                    ->whereNull('shop_catalogs.deleted_at');

        if( empty($input['food_name']) && empty($input['specials']) ):
            $query_foods = $query_foods->inRandomOrder();
        endif;

		$query_food_offers = ShopOffer::select('shop_offers.id as shop_offer_id','shop_offer_foods.shop_catalog_id', 'shop_offer_foods.id', 'shop_offers.shop_id', 'shop_offer_foods.food_name', 'shop_offer_foods.discounted_price', 'shop_offer_foods.min_quantity', 'shop_offer_foods.food_image', 'shop_catalogs.food_unit_of_measure_quantity')
                                      ->leftJoin('shop_offer_foods', 'shop_offer_foods.shop_offer_id', '=', 'shop_offers.id')
                                      ->leftJoin('shop_catalogs', 'shop_offer_foods.shop_catalog_id', '=', 'shop_catalogs.id')
                                      ->where('shop_offers.status', 'available')
                                      ->where('shop_catalogs.visible_in_search', 1)
                                      ->whereRaw('CURDATE() >= shop_offers.start_date')
                                      ->whereRaw('CURDATE() <= shop_offers.end_date')
                                      ->whereNotNull('shop_offer_foods.id')
                                      ->whereNull('shop_offers.deleted_at')
                                      ->whereNull('shop_offer_foods.deleted_at')
                                      ;

        if( empty($input['food_name']) && empty($input['specials']) ):
            $query_food_offers = $query_food_offers->inRandomOrder();
        endif;

        if( !empty($input['specials']) ){

            $specials = explode(",", $input['specials']);

            $query_foods = $query_foods->whereHas('specials', function($special) use($specials) {
                $special->whereIn('specials.id', $specials);
            });

            $query_food_offers = $query_food_offers->whereHas('shopOfferFoods', function($shopOfferFood) use($specials) {
                            $shopOfferFood->whereHas('shopCatalog', function($shopCatalog) use($specials) {
                                $shopCatalog->whereHas('specials', function($special) use($specials) {
                                    $special->whereIn('specials.id', $specials);
                                });
                            });
                        });

        }

        if(!empty($input['limit'])){
            $query_foods = $query_foods->limit($input['limit']);
            $query_food_offers = $query_food_offers->limit($input['limit']);
        }

        if(!empty($input['offset']) && !empty($input['limit'])){
            if($input['offset'] > 0){
                return response()->json(['error' => 'Mostriamo solo i risultati della prima pagina'], 400);
            }
            $offset = $input['offset'] * $input['limit'];
            $query_foods = $query_foods->offset($offset);
            $query_food_offers = $query_food_offers->offset($offset);
        }

        $arr_shop_distance = [];

        $shops_found = true; // Forzo sempre a true, Flavio

        if(count($shops) > 0){

            $shops_found = true;

        }elseif(
            (
            count($shops) === 0 &&

            empty(str_replace("null", "", strtolower($input['name']))) &&
            empty(str_replace("null", "", strtolower($input['food_name']))) &&
            empty(str_replace("null", "", strtolower($input['specials']))) &&
            empty(str_replace("null", "", strtolower($input['shop_type_id']))) &&
            empty(str_replace("null", "", strtolower($input['delivery_on_site']))) &&
            empty(str_replace("null", "", strtolower($input['home_delivery']))) &&
            empty(str_replace("null", "", strtolower($input['discount'])))
            )
            || !$orderByDistance ) {

            //$shops_found = false;

            /**
             * Se non trovo nessun negozio e non avevo specificato nessun filtro di ricerca
             * mostro 5 negozi random
             */
            unset($query_shops);

            $query_shops = Shop::select('user_id', 'shops.id', 'shops.name', 'address', 'lat', 'long', 'shops.delivery_on_site', 'shops.home_delivery', 'shops.delivery_host_on_site', 'shops.show_phone_on_app')
                         ->selectRaw("{$haversine} AS distance")
                         ->whereRaw("{$haversine} < ?", [2000])
                         ->where('status', 'active')
                         ->whereHas('user', function($user){
                            $user->whereHas('subscription', function($subscription){
                                $subscription->where('expire_date', '>', 'CURDATE()');
                            });
                         })
                         ->withCount('shopCatalogs')
                         ->having('shop_catalogs_count', '>', 0);

            if($orderByDistance):
                $query_shops = $query_shops->orderBy('distance', 'ASC');
            else:
                $query_shops = $query_shops->orderBy('id', 'DESC');
            endif;

            $query_shops = $query_shops->limit(20)->distinct();

            $shops = $query_shops->get();
        }

        $shop_ids = [];
        //Get shop id list

        foreach($shops as $shop){
            $shop_ids[] = $shop->id;
            if(!empty($orderByDistance)){
                $arr_shop_distance[$shop->id] = $shop->distance;
            }
        }

        //print_r($shop_ids); exit;
        $query_foods = $query_foods->whereIn('shop_id', $shop_ids);
        $query_food_offers = $query_food_offers->whereIn('shop_offers.shop_id', $shop_ids);

        //Food Name
        if( !empty($input['food_name']) && strtolower($input['food_name']) !== 'null' ){
            $query_foods = $query_foods->where('food_name', 'like', '%'.$input['food_name'].'%');
            $query_food_offers = $query_food_offers->where('shop_offer_foods.food_name', 'like', '%'.$input['food_name'].'%');
        }
        //Discount
        if( !empty($input['discount']) ){
            $query_food_offers = $query_food_offers->where('shop_offer_foods.discounted_price', '!=', null);
        }

        $food_offers = $query_food_offers->get();

		//Offer
        $food_offer_list = [];
        $catalogs_food_offers = [];

        define("MAX_PROD_FOR_SHOP", 1);

        $arr_shops_found = [];

		foreach($food_offers as $offer){
			$shop = Shop::find($offer->shop_id);
            $user_shop = $shop->user;

            $distance = '';
            if(!empty($coordinates)){

                $distance = DB::select(DB::raw('select ST_Distance_sphere(
                                                    point(:lonA, :latA),
                                                    point(:lonB, :latB)
                                                ) as distance'),
                                                [
                                                    'lonA' => $coordinates['longitude'],
                                                    'latA' => $coordinates['latitude'],
                                                    'lonB' => $shop->long,
                                                    'latB' => $shop->lat
                                                ]);

            }

            if( $distance[0]->distance > 0 ):
                if( $distance[0]->distance >= 1000 ):
                    $distance[0]->distance = round(floatVal($distance[0]->distance / 1000), 3);
                    $distance[0]->uom = ' Km';
                else:
                    $distance[0]->distance = round($distance[0]->distance, 3);
                    $distance[0]->uom = ' m';
                endif;
            endif;

            $original_price = '';
            $price = ShopCatalog::find($offer->shop_catalog_id);
            if( $price ){
                !empty($offer->min_quantity) ? $original_price =  $price->food_price * $offer->min_quantity : $original_price = $price->food_price;
            }

            // Immagine prodotto
            $offer_image = null;
            if( $offer->food_image ){
                $offer_image = $offer->food_image;
            }elseif( $offer->shopCatalog ){
                if( !empty( $offer->shopCatalog->food_image ) ){
                    $offer_image = $offer->shopCatalog->food_image;
                }else{
                    if( $offer->shopCatalog->food ){
                        $offer_image = url('/storage/images/foods/'.NormalizeString($offer->shopCatalog->food->foodCategory->name)).'/'.$offer->shopCatalog->food->image;
                    }
                }
            }

            if( $offer_image === null ){
                $offer_image = url('/images/food.jpg');
            }else{
                $offer_image = getResizedImage(getOriginalImage($offer_image), '230x154');
            }

            $catalogs_food_offers[] = $offer->shop_catalog_id;

            $shopOffer = $this->shopOfferRepository->find($offer->shop_offer_id);
            if( $shopOffer ):

                if( !isset(array_count_values(array_column($food_offer_list, 'shop_id'))[$shopOffer->shop_id]) ||
                    ( isset(array_count_values(array_column($food_offer_list, 'shop_id'))[$shopOffer->shop_id]) &&
                      array_count_values(array_column($food_offer_list, 'shop_id'))[$shopOffer->shop_id] < MAX_PROD_FOR_SHOP )):

                    $food_offer_list[] = Array(
                            'id' => $offer->id,
                            'shop_id' => $shopOffer->shop_id,
                            'shop_offer_id' => $offer->shop_offer_id,
                            'shop_name' => $shop->name,
                            'food_name' =>	$offer->food_name,
                            'quantity' => trim(( $offer->unit_of_measure_buy_quantity > 1 ? ' / ' . round($offer->unit_of_measure_buy_quantity,0) : ' / ') . ' ' . (isset($price->unit_of_measure_id) ? $arr_unit_of_measure[$price->unit_of_measure_id] : '')),
                            'original_price' => number_format($original_price, 2).'€',
                            'discounted_price' => $this->getBuyPrice($this->shopOfferFoodRepository->find($offer->id), true), //number_format($offer->discounted_price,2).'€',
                            'distance' => $distance,
                            'food_image' => $offer_image,
                            'type' => 'offer'
                        );

                    if(!in_array($shop->id, $arr_shops_found)):
                        $arr_shops_found[] = $shop->id;
                    endif;

                endif;
            endif;
        }

        $foods = $query_foods->get();

		//Catalog
		$food_list = [];
		foreach($foods as $food){

            if( !in_array($food->id, $catalogs_food_offers) ){

                $shop = Shop::find($food->shop_id);
                $user_shop = $shop->user;

                $distance = '';
                if(!empty($coordinates)){

                    $distance = DB::select(DB::raw('select ST_Distance_sphere(
                                                        point(:lonA, :latA),
                                                        point(:lonB, :latB)
                                                    ) as distance'),
                                                    [
                                                        'lonA' => $coordinates['longitude'],
                                                        'latA' => $coordinates['latitude'],
                                                        'lonB' => $shop->long,
                                                        'latB' => $shop->lat
                                                    ]);

                }

                if( $distance[0]->distance > 0 ):
                    if( $distance[0]->distance >= 1000 ):
                        $distance[0]->distance = round(floatVal($distance[0]->distance / 1000), 3);
                        $distance[0]->uom = ' Km';
                    else:
                        $distance[0]->distance = round($distance[0]->distance, 3);
                        $distance[0]->uom = ' m';
                    endif;
                endif;

                // Immagine prodotto
                $food_image = null;
                if( !empty($food->food_image) ){
                    $food_image = $food->food_image;
                }elseif( $food->food ){
                    $food_image = url('/storage/images/foods/'.NormalizeString($food->food->foodCategory->name)).'/'.$food->food->image;
                }

                if( $food_image === null ){
                    $food_image = url('/images/food.jpg');
                }else{
                    $food_image = getResizedImage(getOriginalImage($food_image), '230x154');
                }

                if( (!isset(array_count_values(array_column($food_offer_list, 'shop_id'))[$food->shop_id]) ||
                    ( isset(array_count_values(array_column($food_offer_list, 'shop_id'))[$food->shop_id]) &&
                      array_count_values(array_column($food_offer_list, 'shop_id'))[$food->shop_id] < MAX_PROD_FOR_SHOP )) &&
                    (!isset(array_count_values(array_column($food_list, 'shop_id'))[$food->shop_id]) ||
                      ( isset(array_count_values(array_column($food_list, 'shop_id'))[$food->shop_id]) &&
                        array_count_values(array_column($food_list, 'shop_id'))[$food->shop_id] < MAX_PROD_FOR_SHOP )) ):

                        $food_list[] = Array(
                                'shop_id' => $food->shop_id,
                                'shop_catalog_id' => $food->id,
                                'shop_name' => $shop->name,
                                'food_name' =>	$food->food_name,
                                'original_price' => $food->food_price.'€',
                                'quantity' => trim(($food->unit_of_measure_buy_quantity > 1 ? ' / '. round($food->unit_of_measure_buy_quantity,0) : ' / ') .' '.$food->um_name),
                                'distance' => $distance,
                                'food_image' => $food_image,
                                'uncertain_weight' => $food->uncertain_weight,
                                'type' => 'catalog'
                                //,'buy_price' => $this->getBuyPrice($this->shopCatalogRepository->find($food->id), false)
                        );

                        if(!in_array($shop->id, $arr_shops_found)):
                            $arr_shops_found[] = $shop->id;
                        endif;

                endif;

            }

        }

		$food_result_list = array_merge($food_offer_list, $food_list);
        $food_count = count($food_result_list);

        /** Se filtro i prodotti,
         *  elimino dai risultati quei negozi per i quali non esiste
         *  nessun prodotto che soddisfa i requisiti
         */
        if( !empty($input['food_name']) or !empty($input['specials'])):
            foreach($shops as $k => $shop):
                if( !in_array($shop->id, $arr_shops_found)):
                    //unset($shops[$k]);
                    $shops->forget($k);
                endif;
            endforeach;

            $shops = $shops->values();

        endif;

		foreach($shops as $k => $shop):
			$distance = $shop->distance*1000;
			 if( $distance > 0 ):
                if( $distance >= 1000 ):
					$shop->distance = round(floatVal($distance / 1000), 3);
                    $shop->uom = ' Km';
                else:
					$shop->distance = round($distance, 3);
                    $shop->uom = ' m';
                endif;
            endif;

            //$shop->shop_type = $shop->shopType;

            if( !in_array($shop->id, $arr_shops_found)):
                //unset($shops[$k]);
                $shops->forget($k);
            endif;

		endforeach;


        unset($shops->shop_catalogs);
        unset($shops->obj_opening_timetable);
        unset($shops->obj_delivery_opening_timetable);



        return response()->json(['success' => [ 'Found' => $shops_found,
                                                'Shops' => $shops->values(),
                                                'total_shops' => count($shops),
                                                'total' => $food_count,
                                                'Foods' => $food_result_list]], 200);

    }

    public function getBuyPrice($food, $offer){

        $price = false;

        if( $offer ):

            $unitOfMeasure = $food->shopCatalog->unitOfMeasure;
            $unitOfMeasureBuy = $food->shopCatalog->unitOfMeasureBuy ?? $unitOfMeasure;

            $quantity = $food->shopCatalog->food_unit_of_measure_quantity;

            $price = $food->discounted_price / $quantity;

            //$price = $price / $quantity;

            // Altrimenti se l'unità di misura di acquisto è quella del livello precedente
            if( $unitOfMeasure->id === $unitOfMeasureBuy->next_unit_of_measure ):

                $price = $price / $unitOfMeasureBuy->next_unit_of_measure_quantity;
                $quantity = $quantity * $unitOfMeasureBuy->next_unit_of_measure_quantity;

            elseif( $unitOfMeasure->id !== $unitOfMeasureBuy->id && $unitOfMeasure->id !== $unitOfMeasureBuy->next_unit_of_measure ):

                // Devo scorrere le unità di misura
                $uom_multiplier = null;

                $uom = $unitOfMeasureBuy;

                if( $uom->next_unit_of_measure ):
                    while( $unitOfMeasure->id !== $uom->next_unit_of_measure && !empty($uom->next_unit_of_measure) ):

                        if( empty($uom_multiplier) ):
                            $uom_multiplier = $uom->next_unit_of_measure_quantity;
                        else:
                            $uom_multiplier = $uom_multiplier * $uom->next_unit_of_measure_quantity;
                        endif;

                        $uom = $this->unitOfMeasureRepository->find($uom->next_unit_of_measure);

                    endwhile;
                endif;

                $uom_multiplier = $uom_multiplier * $uom->next_unit_of_measure_quantity;

                if( $unitOfMeasure->id === $uom->next_unit_of_measure ):
                    $price = $price / $uom_multiplier;
                    $quantity = $quantity * $uom_multiplier;
                endif;

            endif;

            $price = $price * $food->shopCatalog->unit_of_measure_buy_quantity;

        else:

            $unitOfMeasure = $food->unitOfMeasure;
            $unitOfMeasureBuy = $food->unitOfMeasureBuy ?? $unitOfMeasure;

            //otherwise I take the catalog price
            $quantity = $food->food_unit_of_measure_quantity;
            $price = $quantity * $food->food_price;
            $price = $price / $quantity;

            // L'unità di misura di acquisto è quella del livello precedente
            if( $unitOfMeasure->id === $unitOfMeasureBuy->next_unit_of_measure ):

                $price = $price / $unitOfMeasureBuy->next_unit_of_measure_quantity;

            elseif( $unitOfMeasure->id !== $unitOfMeasureBuy->id && $unitOfMeasure->id !== $unitOfMeasureBuy->next_unit_of_measure ):

                // Devo scorrere le unità di misura
                $uom_multiplier = null;

                $uom = $unitOfMeasureBuy;

                if( $uom->next_unit_of_measure ):
                    while( $unitOfMeasure->id !== $uom->next_unit_of_measure && !empty($uom->next_unit_of_measure) ):

                        if( empty($uom_multiplier) ):
                            $uom_multiplier = $uom->next_unit_of_measure_quantity;
                        else:
                            $uom_multiplier = $uom_multiplier * $uom->next_unit_of_measure_quantity;
                        endif;

                        $uom = $this->unitOfMeasureRepository->find($uom->next_unit_of_measure);

                    endwhile;
                endif;

                $uom_multiplier = $uom_multiplier * $uom->next_unit_of_measure_quantity;

                if( $unitOfMeasure->id === $uom->next_unit_of_measure ):
                    $price = $price / $uom_multiplier;
                endif;

            endif;

            $price = $price * $food->unit_of_measure_buy_quantity;

        endif;

        return number_format($price, 2);
    }

    public function updateWhatsapp(Request $request){
        $input = $request->all();
        $user = $this->userRepository->find($input['user_id']);

        $shop = $this->shopRepository->find($user->shop->id);
        $shop->whatsapp = $input['whatsapp'];
        $shop->save();

        $otp = mt_rand(1000,10000);
        $user->otp = $otp;
        $user->save();

        if(!empty($shop->whatsapp)){
            /** Invia SMS */
            $sms = new smsCls();
            $sms->inviaSms('Codice di attivazione Primascelta.biz: ' . $otp, [ $shop->whatsapp] );
            return response()->json(['success' => ['OK' => 'Abbiamo inviato un sms al numero da te indicato']], 200);
        }else{
            return response()->json(['errors' => ['phone_empty' => 'Il numero di telefono non può essere vuoto']], 200);
        }
    }
}
