<?php

namespace App\Http\Controllers\API;

use Illuminate\Support\Facades\Auth;
use App\Http\Requests\API\CreateShopCatalogAPIRequest;
use App\Http\Requests\API\UpdateShopCatalogAPIRequest;
use Illuminate\Support\Facades\Validator;
use App\Models\ShopCatalog;
use App\Models\Food;
use App\Models\FoodCategory;
use App\Models\Shop;
use App\Models\UnitOfMeasure;
use App\Repositories\ShopCatalogRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Repositories\UnitOfMeasureRepository;
use Response;
use File;
use Image;
use Log;

/**
 * Class ShopCatalogController
 * @package App\Http\Controllers\API
 */

class ShopCatalogAPIController extends AppBaseController
{
    /** @var  ShopCatalogRepository */
    private $shopCatalogRepository, $unitOfMeasureRepository;

    public function __construct(ShopCatalogRepository $shopCatalogRepo, UnitOfMeasureRepository $unitOfMeasureRepository)
    {
        $this->shopCatalogRepository = $shopCatalogRepo;
        $this->unitOfMeasureRepository = $unitOfMeasureRepository;
    }

    /**
     * Display a listing of the ShopCatalog.
     * GET|HEAD /shopCatalogs
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request, $id = null)
    {

        Log::info("shopcatalog api controller index: " . $id);

        if($user = auth()->user()) {

				$input = $request->all();
				$count_available_foods = Food::count();
				if($id){
					$shop_id = $id;
				}else{

					if($user->user_type !== 'company'){
						return response()->json(['errors' => ['User not valid' => 'This use is for company user only ']], 400);
					}

					$shop_id = $user->shop->id;
				}

				$arr_food_categories = [];
				$obj_food_categories = FoodCategory::select('id','name', 'logo','background')->get();
				foreach($obj_food_categories as $fc){
					$arr_food_categories[$fc->id]['name'] = $fc->name;
					$arr_food_categories[$fc->id]['logo'] = $fc->logo;
					$arr_food_categories[$fc->id]['background'] = $fc->background;
				}
				$arr_unit_of_measures = [];
				$obj_unit_of_measures = UnitOfMeasure::select('id', 'name')->get();
				foreach($obj_unit_of_measures as $um){
					$arr_unit_of_measures[$um->id] = $um->name;
				}
				//var_dump($arr_food_categories);
				//var_dump($arr_unit_of_measures);exit;
				$shop_foods = ShopCatalog::where('shop_id', $shop_id);
				if(!empty($input['food_name'])){
					$shop_foods->where('food_name', 'like', $input['food_name'].'%');
				}
				if(!empty($input['food_category_id'])){
					$shop_foods->where('food_category_id', $input['food_category_id']);
				}
				if(!empty($input['food_type'])){
					$shop_foods->where('food_type', $input['food_type']);
				}

                $shop_foods->where('visible_in_search', 1);

				$obj_shop_catalog = $shop_foods->orderBy('food_category_id')->get();
                /*
                $shop_foods->where('visible_in_search', 1)
                           ->join('food_categories', 'shop_catalogs.food_category_id', '=', 'food_categories.id');

				$obj_shop_catalog = $shop_foods->orderBy('food_categories.name')->get();
                */
				$shop_catalog = [];
				foreach($obj_shop_catalog as $sc){

					$category_name = $arr_food_categories[$sc->food_category_id]['name'];
                    //$dir_food_category_image = str_replace(' ','_', $arr_food_categories[$sc->food_category_id]['name']);
                    $logo = null;
					$logo = empty($arr_food_categories[$sc->food_category_id]['logo']) ? asset("/images/food.jpg") : $arr_food_categories[$sc->food_category_id]['logo'];
					$food_image = null;
					if (empty($sc->food_image)){
						if (!empty($sc->food->image)){
                            $food_image = asset('storage/images/foods/'.NormalizeString($sc->food->foodCategory->name).'/'.$sc->food->image);
                        }
					}else{
						$food_image = $sc->food_image;
                    }
                    if( $food_image !== null ){
                        $food_image = getResizedImage(getOriginalImage($food_image), '100x100');
                    }
					$shop_catalog[$category_name]['image_url'] = $logo. '?' . time();

					$shop_catalog[$category_name]['background_url'] = empty($arr_food_categories[$sc->food_category_id]['background']) ? "" : $arr_food_categories[$sc->food_category_id]['background'];
					$shop_catalog[$category_name][$sc->food_type][] = [
																		'shop_catalog_id' => $sc->id,
																		'food_id' => $sc->food_id,
																		'food_name' => $sc->food_name,
																		'food_description' => $sc->food_description,
																		'food_unit_of_measure' => $arr_unit_of_measures[$sc->unit_of_measure_id],
																		'food_unit_of_measure_quantity' => $sc->food_unit_of_measure_quantity,
																		'food_price' => $sc->food_price."€",
																		'food_image' => $food_image,
																		'food_visible_in_search' => intval($sc->visible_in_search) ?? 0,
                                                                        'uncertain_weight' => $sc->uncertain_weight,
                                                                        'is_offer' => $sc->isOffer
																	];
				}

                Log::info("Shop catalog");
                Log::info($shop_catalog);

            	return response()->json(["Available Foods" => $count_available_foods, "Count" => count($obj_shop_catalog), "Catalog" => $shop_catalog], 200);

        }
    }

    /**
     * Store a newly created ShopCatalog in storage.
     * POST /shopCatalogs
     *
     * @param CreateShopCatalogAPIRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
		//Get shop id
		$user = auth()->user();
		$shop = $user->shop;


        $input = $request->all();
		//Pass shop id
		$input['shop_id'] = $shop->id;

		if(!empty($input['shop_catalog_id'])){
			//Update
			return $this->update($input['shop_catalog_id'], $request);
		} else {
			//Create
			$shopCatalog = $this->shopCatalogRepository->create($input);

			//Image upload
			if(!empty($input['food_image'])){//Store image
				$image_rule = [
					'food_image' => 'image|mimes:jpeg,png,jpg,gif|max:2048',
				];
				$image_messages = [
                    'food_image.image' => 'Image type not allowed',
                    'food_image.mimes' => 'Mime type not allowed',
                    'food_image.uploaded' => 'Size of image is too big. Max 2Mb allowed',
                ];
				$image_validator = Validator::make(['food_image' => $input['food_image']], $image_rule, $image_messages);
				if($image_validator->fails()){
					$shopCatalog->delete();
					return response()->json(['errors' => $image_validator->errors()], 400);
				}
				$imageName = NormalizeString($input['food_name']).'_'.time().'.'.$request->food_image->extension();
				$path = storage_path('app/public/images/shops/'.$input['shop_id']);
				if(!File::isDirectory($path)){
					File::makeDirectory($path, 0777, true, true);
				}
				$request->food_image->move(storage_path('app/public/images/shops/'.$input['shop_id'].'/'), $imageName);
				//Generate cropped thumbs image
				$path_image_name = storage_path('app/public/images/shops/'.$input['shop_id'].'/').$imageName;
				$image = Image::make($path_image_name)->orientate();
				cropImages($image,$path_image_name,'food');
				$shopCatalog->food_image = asset('/storage/images/shops/'.$input['shop_id'].'/'.$imageName);
				$shopCatalog->save();
			}
		}

        return $this->sendResponse($shopCatalog->toArray(), 'Shop Catalog saved successfully');
    }

    /**
     * Display the specified ShopCatalog.
     * GET|HEAD /shopCatalogs/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var ShopCatalog $shopCatalog */
        $shopCatalog = $this->shopCatalogRepository->find($id);

        if (empty($shopCatalog)) {
            return response()->json(['errors' => ['Not found' => 'Shop Catalog not found']], 400);
		}

		if( !empty($shopCatalog->food_image) ):
			$shopCatalog->food_image = getResizedImage(getOriginalImage($shopCatalog->food_image), '646x430');
		endif;

		$shopCatalog['favourite'] = 0;
		if( Auth::user()->favouriteShopCatalogs->contains('id', $shopCatalog->id) ):
			$shopCatalog['favourite'] = 1;
        endif;

        $shopCatalog['buy_unit_price'] = $this->getBuyPrice($shopCatalog);

        return $this->sendResponse($shopCatalog->toArray(), 'Shop Catalog retrieved successfully');
    }

    public function getBuyPrice($shopCatalog){

        $price = false;

        $unitOfMeasure = $shopCatalog->unitOfMeasure;
        $unitOfMeasureBuy = $shopCatalog->unitOfMeasureBuy ?? $unitOfMeasure;

        //otherwise I take the catalog price
        $price = $shopCatalog->food_price;
        $quantity = $shopCatalog->food_unit_of_measure_quantity;
        $price = $price / $quantity;

        // L'unità di misura di acquisto è quella del livello precedente
        if( $unitOfMeasure->id === $unitOfMeasureBuy->next_unit_of_measure ):

            $price = $price / $unitOfMeasureBuy->next_unit_of_measure_quantity;

        elseif( $unitOfMeasure->id !== $unitOfMeasureBuy->id && $unitOfMeasure->id !== $unitOfMeasureBuy->next_unit_of_measure ):

            // Devo scorrere le unità di misura
            $uom_multiplier = null;

            $uom = $unitOfMeasureBuy;

            if( $uom->next_unit_of_measure ):
                while( $unitOfMeasure->id !== $uom->next_unit_of_measure && !empty($uom->next_unit_of_measure) ):

                    if( empty($uom_multiplier) ):
                        $uom_multiplier = $uom->next_unit_of_measure_quantity;
                    else:
                        $uom_multiplier = $uom_multiplier * $uom->next_unit_of_measure_quantity;
                    endif;

                    $uom = $this->unitOfMeasureRepository->find($uom->next_unit_of_measure);

                endwhile;
            endif;

            $uom_multiplier = $uom_multiplier * $uom->next_unit_of_measure_quantity;

            if( $unitOfMeasure->id === $uom->next_unit_of_measure ):
                $price = $price / $uom_multiplier;
            endif;

        endif;

        //$price = $price * ( $quantity / $shopCatalog->unit_of_measure_buy_quantity );
        $price = $price * $shopCatalog->unit_of_measure_buy_quantity;

        return $price;
    }


    /**
     * Update the specified ShopCatalog in storage.
     * PUT/PATCH /shopCatalogs/{id}
     *
     * @param int $id
     * @param UpdateShopCatalogAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
		//Get shop id
		$user = auth()->user();
		$shop = $user->shop;

        $input = $request->all();

		//Pass shop id
		$input['shop_id'] = $shop->id;

        /** @var ShopCatalog $shopCatalog */
        $shopCatalog = $this->shopCatalogRepository->find($id);

        if (empty($shopCatalog)) {
            return response()->json(['errors' => ['Not found' => 'Shop Catalog not found']], 400);
        }

		//Image upload
		if(!empty($input['food_image'])){//Store image
			$image_rule = [
				'food_image' => 'image|mimes:jpeg,png,jpg,gif|max:2048',
			];
			$image_messages = [
							'food_image.image' => 'Image type not allowed',
							'food_image.mimes' => 'Mime type not allowed',
							'food_image.uploaded' => 'Size of image is too big. Max 2Mb allowed',
						];
			$image_validator = Validator::make(['food_image' => $input['food_image']], $image_rule, $image_messages);
			if($image_validator->fails()){
				$shopCatalog->delete();
				return response()->json(['errors' => $image_validator->errors()], 400);
			}
			$imageName = NormalizeString($input['food_name']).'_'.time().'.'.$request->food_image->extension();
			$path = storage_path('app/public/images/shops/'.$input['shop_id']);
			if(!File::isDirectory($path)){
				File::makeDirectory($path, 0777, true, true);
			}
			$request->food_image->move(storage_path('app/public/images/shops/'.$input['shop_id'].'/'), $imageName);
			//Generate cropped thumbs image
			$path_image_name = storage_path('app/public/images/shops/'.$input['shop_id'].'/').$imageName;
			$image = Image::make($path_image_name)->orientate();
			cropImages($image,$path_image_name,'food');
			$shopCatalog->food_image = asset('/storage/images/shops/'.$input['shop_id'].'/'.$imageName);
			$shopCatalog->save();
			unset($input['food_image']);
		}

        $shopCatalog = $this->shopCatalogRepository->update($input, $id);

        return $this->sendResponse($shopCatalog->toArray(), 'Prodotto modificato con successo');
    }

    /**
     * Remove the specified ShopCatalog from storage.
     * DELETE /shopCatalogs/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var ShopCatalog $shopCatalog */
        $shopCatalog = $this->shopCatalogRepository->find($id);

        if (empty($shopCatalog)) {
            return response()->json(['errors' => ['Not found' => 'Shop Catalog not found']], 400);
        }

		foreach($shopCatalog->shopOfferFoods as $shopOfferFood):
            if( $shopOfferFood->shopOffer->start_date->isPast() && $shopOfferFood->shopOffer->end_date->addDays(1)->isFuture() ):
				return response()->json(['errors' => ['Msg' => 'Non puoi cancellare il prodotto perché é presente in una offerta in corso.']], 400);
            endif;
		endforeach;

        $shopCatalog->delete();

        return $this->sendSuccess('Shop Catalog deleted successfully');
    }

    /**
     * Set visible in search value.
     * POST /shopCatalogs/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function setVisibleInSearch(Request $request)
	{
		$input = $request->all();
		if(!empty($input['shop_catalog_id'])){
			$shop_catalog = ShopCatalog::find($input['shop_catalog_id']);
			$shop_catalog->visible_in_search = $input['visible_in_search'] ?? 0;
			$shop_catalog->save();
        	return $this->sendSuccess('Prodotto modificato con successo');

		}
    }


    /**
     * Search Catalog.
     * Get /shopCatalogs/search
     *
     * @param
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function search(Request $request)
	{
        if($user = auth()->user()) {
			if($user->user_type == 'company'){
                $input = $request->all();

                if( empty($input['food_name']) && empty($input['food_category_id']) ):
                    return response()->json(['total' => 0, 'count' => 0, 'Catalog list' => [], 'error' => 'Selezionare almeno un criterio di ricerca' ], 200);
                endif;

				$shop_id = $user->shop->id;

				$shop_foods = ShopCatalog::where('shop_id', $shop_id);

				if(!empty($input['food_name'])){
					$shop_foods->where(function($q) use($input) {
                                    $q->where('food_name', 'LIKE', '%'.str_replace(' ', '%', $input['food_name']).'%')
                                    ->orWhere('food_description', 'LIKE', '%'.$input['food_name'].'%');
                                });
				}
				if(!empty($input['food_category_id']) && $input['food_category_id'] !== 'SELECT_ALL'){
					$shop_foods->where('food_category_id', $input['food_category_id']);
				}
				if(!empty($input['food_type'])){
					$shop_foods->where('food_type', $input['food_type']);
				}
				if(isset($input['visible_in_search'])){
					$shop_foods->where('visible_in_search', $input['visible_in_search']);
				}
				$total_foods =  count($shop_foods->get());
				if(!empty($input['offset'])){
					$shop_foods->offset($input['offset']);
				}
				if(!empty($input['limit'])){
					$shop_foods->limit($input['limit']);
				}
				if(isset($input['ids'])){
					$shop_foods->whereNotIn('id', $input['ids']);
				}
				$arr_catalog = $shop_foods->with('foodCategory')
								->with('food')
                                ->with('unitOfMeasure')
                                ->with('unitOfMeasureBuy')
                                ->orderBy('food_name')
                                ->get();

                                //dd($arr_catalog);

                $count = count($arr_catalog);

                foreach($arr_catalog as $shopCatalog){
                    if( empty($shopCatalog->food_image) ){
                        if( $shopCatalog->food ):
                            $shopCatalog->food_image = asset('/storage/images/foods/'.NormalizeString($shopCatalog->food->foodCategory->name).'/'.getResizedImage(getOriginalImage($shopCatalog->food->image), '512x340'));
                        endif;
                    }else{
                        $shopCatalog->food_image = getResizedImage(getOriginalImage($shopCatalog->food_image), '512x340');
                    }
                }

				return response()->json(['total' => $total_foods, 'count' => $count, 'Catalog list' => $arr_catalog], 200);
			}
		} else {
			return response()->json(['errors' => ['User not valid' => 'This use is not company type']], 400);
		}
	}

	public function getFoodCategoryList(){
		$categoryList = $this->shopCatalogRepository->getFoodCategoryList();
		return response()->json(['Food categories' => $categoryList], 200);
	}


	public function getTypesList($food_category_id, Request $request)
	{
		$input = $request->all();

		$typesList = $this->shopCatalogRepository->getTypesList($food_category_id);
		return response()->json(['count' => count($typesList), 'Food Type List' => $typesList], 200);
    }

    public function getRelatedUoms($unit_of_measure_id){
        $relatedUoms = $this->unitOfMeasureRepository->getRelateds($unit_of_measure_id);
        $relatedUoms = $this->unitOfMeasureRepository->getUoms($relatedUoms);
        return response()->json(['related_uoms' => $relatedUoms]);
    }
}
