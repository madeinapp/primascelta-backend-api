<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateShopOfferAPIRequest;
use App\Http\Requests\API\UpdateShopOfferAPIRequest;
use Illuminate\Support\Facades\Validator;
use App\Models\ShopOffer;
use App\Models\Order;
use App\Models\ShopOfferFood;
use App\Repositories\ShopOfferRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Repositories\ShopOfferFoodRepository;
use App\Repositories\UnitOfMeasureRepository;
use Response;
use Log;
/**
 * Class ShopOfferController
 * @package App\Http\Controllers\API
 */

class ShopOfferAPIController extends AppBaseController
{
    /** @var  ShopOfferRepository */
    private $shopOfferRepository,
            $shopOfferFoodRepository,
            $unitOfMeasureRepository;

    public function __construct(ShopOfferRepository $shopOfferRepo, ShopOfferFoodRepository $shopOfferFoodRepo, UnitOfMeasureRepository $unitOfMeasureRepository)
    {
        $this->shopOfferRepository = $shopOfferRepo;
        $this->shopOfferFoodRepository = $shopOfferFoodRepo;
        $this->unitOfMeasureRepository = $unitOfMeasureRepository;
    }

    /**
     * Display a listing of the ShopOffer.
     * GET|HEAD /shopOffers
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request, $id = null)
    {
        if($user = auth()->user()) {
            $input = $request->all();

            if($id){
                $shop_id = $id;
            }else{
                if($user->user_type !== 'company'){
                    return response()->json(['errors' => ['User not valid' => 'This use is for company user only']], 400);
                }
                $shop_id = $user->shop->id;
            }

            $shop_offer_query = ShopOffer::where('shop_id', $shop_id)
                                        ->whereHas('shopOfferFoods', function($shopOfferFoods){
                                            $shopOfferFoods->whereHas('shopCatalog', function($shopCatalog){
                                                $shopCatalog->where('visible_in_search', 1);
                                            });
                                        });

			if(!empty($input['name'])){
				$shop_offer_query->where('name', 'like', $input['name'].'%');
            }

			if(!empty($input['status'])){
				$shop_offer_query->where('status', $input['status']);
			}else{
                $shop_offer_query->where('status', 'available')
                                ->whereRaw('CURDATE() >= shop_offers.start_date')
                                ->whereRaw('CURDATE() <= shop_offers.end_date');
            }



            $shop_offers = $shop_offer_query->with('ShopOfferFoods.ShopCatalog.UnitOfMeasure')
                                            ->get()
                                            ->toArray()
                                            ;



            foreach($shop_offers as $k => $shop_offer):

                if( count($shop_offer['shop_offer_foods']) === 1 ):
                    $shop_offers[$k]['image_path'] = getResizedImage(getOriginalImage($shop_offer['shop_offer_foods'][0]['food_image']), '230x154');
                endif;

                if( isset($shop_offer['shop_offer_foods']) ):
                    foreach( $shop_offer['shop_offer_foods'] as $j => $shop_offer_food ):
                        $shop_offers[$k]['shop_offer_foods'][$j]['food_image'] = getResizedImage(getOriginalImage($shop_offers[$k]['shop_offer_foods'][$j]['food_image']), '230x154');
                        if( isset($shop_offers[$k]['shop_offer_foods'][$j]['shop_catalog']) ):
                            $shop_offers[$k]['shop_offer_foods'][$j]['shop_catalog']['food_image'] = getResizedImage(getOriginalImage($shop_offers[$k]['shop_offer_foods'][$j]['shop_catalog']['food_image']), '230x154');
                        endif;
                    endforeach;
                endif;

            endforeach;

            return response()->json(["Shop Offers" => $shop_offers], 200);
        }
    }

    /**
     * Store a newly created ShopOffer in storage.
     * POST /shopOffers
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
		//Get shop id
		$user = auth()->user();
        $input = $request->all();
		//Pass shop id
		$input['shop_id'] = $user->shop->id;
		$rules = [
			'name' => 'required',
			'start_date' => 'date|required',
			'end_date' => 'date|required',
		];
		$messages = [
			'name.required' => 'Name is required',
			'start_date.required' => 'Start date is required',
			'end_date.required' => 'End date is required',
			'start_date.date' => 'Date is not valid',
			'end_date.date' => 'Date is not valid',
		];
		$validator = Validator::make($input , $rules, $messages);
		if($validator->fails()){
			return response()->json(['errors' => $validator->errors()], 400);
		}
		//DEBUG
		if(!empty($input['debug_food_array'])){
			$a = Array(
					 Array(
						 	"shop_catalog_id" => 1,
							"food_name" => "Melanzana nera lunga",
							"food_unit_of_measure_quantity" => 3,
							"food_price" => 5.5,
							"unit_of_measure" => 'Kg',
							"min_quantity" => 2,
							"discounted_price" => 3,
						),
					Array(
							"shop_catalog_id" => 2,
							"food_name" => "Cavolfiore rosetto",
							"food_unit_of_measure_quantity" => 3,
							"food_price" => 15.5,
							"unit_of_measure" => 'Kq',
							"min_quantity" => 23,
							"discounted_price" => 4,
						)
			);
			//print_r($a);
			return response()->json($a, 200);
		};
		if(!empty($input['id'])){//Update
			$shop_offer = ShopOffer::find($input['id']);
			if(!empty($shop_offer)){
                $shop_offer->name = $input['name'];
                $shop_offer->description = $input['description'];
				$shop_offer->start_date = $input['start_date'];
				$shop_offer->end_date = $input['end_date'];
				$shop_offer->status = $input['status'];
				$shop_offer->save();
				ShopOfferFood::where('shop_offer_id', $input['id'])->delete();
			} else {
				return response()->json(['errors' => ['Not found' => 'Shop Offer not found']], 400);
			}
		} else {//Create
			$shop_offer = ShopOffer::create($input);
		}


		if(!empty($input['foods'])){
			$arr_unit_of_measures = '';
			//$json_foods = utf8_decode($input['foods']);
			//echo $json_foods;exit;
			$arr_foods = json_decode($input['foods']);
			//echo json_last_error_msg();Exit;
			//print_r($arr_foods);Exit;
			$input_food['shop_offer_id'] = $shop_offer->id;
			foreach($arr_foods as $k => $food){
				$input_food['shop_catalog_id'] = $food->shop_catalog_id;

				$input_food['food_name'] =str_replace("\n", " ", $food->food_name.' '.$food->food_price.'€ ('.$food->food_unit_of_measure_quantity.' '.$food->unit_of_measure.')');
				$input_food['food_description'] = '';
				$input_food['min_quantity'] = $food->min_quantity;
				$input_food['discounted_price'] = $food->discounted_price;
				ShopOfferFood::create($input_food);
			}
		}
        return response()->json(['success' => 'Shop Offer saved successfully'], 200);
    }

    /**
     * Display the specified ShopOffer.
     * GET|HEAD /shopOffers/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var ShopOffer $shopOffer */
        $shopOffer = ShopOffer::where('id', $id)->with('ShopOfferFoods.shopCatalog.unitOfMeasure')
                                                ->with('ShopOfferFoods.shopCatalog.unitOfMeasureBuy')
                                                ->with('ShopOfferFoods.shopCatalog.specials')
                                                ->with('shop')
                                                ->get()->toArray();

        if (empty($shopOffer) || count($shopOffer) == 0) {
            return response()->json(['errors' => ['Not found' => 'Shop Offer not found']], 400);
        }


        /** Set food image if there is only 1 */
        if( count($shopOffer[0]['shop_offer_foods']) == 1 ):
            if( isset($shopOffer[0]['shop_offer_foods'][0])):
                $is_Cropped = strpos($shopOffer[0]['shop_offer_foods'][0]['food_image'],"_512x340");
                if ($is_Cropped === false) {
                $shopOffer[0]['image_path'] = getResizedImage($shopOffer[0]['shop_offer_foods'][0]['food_image'], '230x154');
                }else{
                    $shopOffer[0]['image_path'] = $shopOffer[0]['shop_offer_foods'][0]['food_image'];
                }
            endif;
        endif;

        foreach($shopOffer[0]['shop_offer_foods'] as $i => &$shopOfferFood)
        {
            $offerFoodId = $shopOfferFood['id'];
            $offerFood = $this->shopOfferFoodRepository->find($offerFoodId);

            $shopOffer[0]['shop_offer_foods'][$i]['buy_unit_price'] = number_format($this->getBuyPrice($offerFood), 2, ",", ".");

            if( empty($shopOfferFood[0]->food_image) ){
                if( $shopOfferFood["food_image"] ){

                    $is_Cropped = strpos($shopOfferFood["food_image"],"_512x340");
                    if ($is_Cropped === false) {
                        $pos = strrpos($shopOfferFood["food_image"], ".");
                        if($pos !== false)
                        $shopOfferFood["food_image"] = substr_replace($shopOfferFood["food_image"], "_512x340.", $pos, strlen("."));
                    }

                }else{
                    if( !empty($shopOfferFood[0]->shopCatalog) && !empty($shopOfferFood[0]->shopCatalog->food_image) ){
                        $shopOfferFood[0]->food_image = $shopOfferFood[0]->shopCatalog->food_image;
                    }elseif(!empty($shopOfferFood[0]->shopCatalog) && empty($shopOfferFood[0]->shopCatalog->food_image)){
                        if( !empty($shopOfferFood[0]->shopCatalog->food) && !empty($shopOfferFood[0]->shopCatalog->food->image) ){
                            $shopOfferFood[0]->food_image = getResizedImage(asset('/storage/images/foods/').'/'.NormalizeString($shopOfferFood[0]->shopCatalog->food->foodCategory).'/'.$shopOfferFood[0]->shopCatalog->food->image,"512x340");
                        }
                    }
                 }

            }else{
                $is_Cropped = strpos($shopOfferFood[0]->food_image,"_512x340");
                if ($is_Cropped === false) {
                    $pos = strrpos($shopOfferFood[0]->food_image, ".");

                    if($pos !== false)
                    $shopOfferFood[0]->food_image = substr_replace($shopOfferFood[0]->food_image, "_512x340.", $pos, strlen("."));
                }
            }
           // print_r($shopOfferFood);exit;
        }

        return response()->json(['success' => ['Shop Offer' => $shopOffer]], 200);
    }

	/**
     * Display the specified ShopOfferFood.
     * GET|HEAD /shopOffers/iget-food/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function showFood($id)
    {
        /** @var ShopOffer $shopOffer */
        $shopOfferFood = ShopOfferFood::where('id', $id)
                                        ->with('shopCatalog.unitOfMeasure')
                                        ->with('shopCatalog.unitOfMeasureBuy')
                                        ->with('shopOffer.shop')
                                        ->get();

        if( empty($shopOfferFood) || count($shopOfferFood) == 0 ) {
            return response()->json(['errors' => ['Not found' => 'L\'offerta non è stata trovata']], 400);
        }

        if( !empty($shopOfferFood[0]) ){
            if( empty($shopOfferFood[0]->food_image) ){
                if( !empty($shopOfferFood[0]->shopCatalog) && !empty($shopOfferFood[0]->shopCatalog->food_image) ){
                    $shopOfferFood[0]->food_image = $shopOfferFood[0]->shopCatalog->food_image;
                }elseif(!empty($shopOfferFood[0]->shopCatalog) && empty($shopOfferFood[0]->shopCatalog->food_image)){
                    if( !empty($shopOfferFood[0]->shopCatalog->food) && !empty($shopOfferFood[0]->shopCatalog->food->image) ){
                        $shopOfferFood[0]->food_image = getResizedImage(asset('/storage/images/foods/').'/'.NormalizeString($shopOfferFood[0]->shopCatalog->food->foodCategory).'/'.$shopOfferFood[0]->shopCatalog->food->image,"512x340");
                    }
                }
            }else{
                $is_Cropped = strpos($shopOfferFood[0]->food_image,"_512x340");
                if ($is_Cropped === false) {
                    $pos = strrpos($shopOfferFood[0]->food_image, ".");

                    if($pos !== false)
                    $shopOfferFood[0]->food_image = substr_replace($shopOfferFood[0]->food_image, "_512x340.", $pos, strlen("."));
                }
            }

            $shopOfferFood[0]['buy_unit_price'] = $this->getBuyPrice($shopOfferFood[0]);

        }

        Log::info($shopOfferFood);

        return response()->json(['success' => ['Shop Offer Food' => $shopOfferFood]], 200);
    }

    public function getBuyPrice($offerFood){

        $price = false;

        if( $offerFood->shopCatalog) {

            $unitOfMeasure = $offerFood->shopCatalog->unitOfMeasure;
            $unitOfMeasureBuy = $offerFood->shopCatalog->unitOfMeasureBuy ?? $unitOfMeasure;

            //and the ordered quantity is greater or equal the min quantity of the offer
            $price = $offerFood->discounted_price;
            $quantity = $offerFood->shopCatalog->food_unit_of_measure_quantity;
            $price = $price / $quantity;

            // Altrimenti se l'unità di misura di acquisto è quella del livello precedente
            if( $unitOfMeasure->id === $unitOfMeasureBuy->next_unit_of_measure ):

                $price = $price / $unitOfMeasureBuy->next_unit_of_measure_quantity;

            elseif( $unitOfMeasure->id !== $unitOfMeasureBuy->id && $unitOfMeasure->id !== $unitOfMeasureBuy->next_unit_of_measure ):

                // Devo scorrere le unità di misura
                $uom_multiplier = null;

                $uom = $unitOfMeasureBuy;

                if( $uom->next_unit_of_measure ):
                    while( $unitOfMeasure->id !== $uom->next_unit_of_measure && !empty($uom->next_unit_of_measure) ):

                        if( empty($uom_multiplier) ):
                            $uom_multiplier = $uom->next_unit_of_measure_quantity;
                        else:
                            $uom_multiplier = $uom_multiplier * $uom->next_unit_of_measure_quantity;
                        endif;

                        $uom = $this->unitOfMeasureRepository->find($uom->next_unit_of_measure);

                    endwhile;
                endif;

                $uom_multiplier = $uom_multiplier * $uom->next_unit_of_measure_quantity;

                if( $unitOfMeasure->id === $uom->next_unit_of_measure ):
                    $price = $price / $uom_multiplier;
                endif;

            endif;

            $price = $price * $offerFood->shopCatalog->unit_of_measure_buy_quantity;
        }

        return $price;
    }


    /**
     * Update the specified ShopOffer in storage.
     * PUT/PATCH /shopOffers/{id}
     *
     * @param int $id
     * @param UpdateShopOfferAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateShopOfferAPIRequest $request)
    {
        $input = $request->all();

        /** @var ShopOffer $shopOffer */
        $shopOffer = $this->shopOfferRepository->find($id);

        if (empty($shopOffer)) {
            return $this->sendError('Shop Offer not found');
        }

        $shopOffer = $this->shopOfferRepository->update($input, $id);

        return $this->sendResponse($shopOffer->toArray(), 'Offerta modificata con successo');
    }

    /**
     * Remove the specified ShopOffer from storage.
     * DELETE /shopOffers/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var ShopOffer $shopOffer */
        $shopOffer = $this->shopOfferRepository->find($id);

        if (empty($shopOffer)) {
            return $this->sendError('Shop Offer not found');
        }


        if( $shopOffer->start_date->isFuture() ):

            ShopOfferFood::where('shop_offer_id', $id)->delete();
            $this->shopOfferRepository->delete($id);

        else:
            return response()->json(['error' => 'You can\'t remove this offer'], 200);
        endif;


        return response()->json(['success' => 'Shop Offer deleted successfully'], 200);
    }



	/**
     * Set status of shop offer (draft|available|....).
     * Put /shopOffers/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function setStatus(Request $request)
	{
		$input = $request->all();
		if(!empty($input['shop_offer_id'])){
			$shop_offer = ShopOffer::find($input['shop_offer_id']);
			if(empty($shop_offer)){
				return response()->json(['errors' => ['Not found' => 'Shop Offer not found']], 400);
			}
		} else {
			return response()->json(['errors' => ['shop_offer_id' => 'Shop Offer id required']], 400);
		}
		if(!empty($input['status'])){
			$shop_offer->status = $input['status'];
			$shop_offer->save();
		}
		return response()->json(['success' => 'Shop offer status saved correctly'], 200);
	}

}
