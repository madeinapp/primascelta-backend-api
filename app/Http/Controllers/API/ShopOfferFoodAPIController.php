<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\ShopOfferFoodRepository;

use Flash;

class ShopOfferFoodAPIController extends Controller
{

    /** @var  ShopOfferRepository */
    private $shopOfferRepository;

    public function __construct(ShopOfferFoodRepository $shopOfferFoodRepo)
    {
        $this->shopOfferFoodRepository = $shopOfferFoodRepo;
    }

    /**
     * Remove the specified ShopOfferFood from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $shopOfferFood = $this->shopOfferFoodRepository->find($id);

        if (empty($shopOfferFood)) {
            Flash::error('Prodotto non trovato');
            return response()->json(['error' => 'Prodotto non trovato'], 200);
        }

        if( ($shopOfferFood->shopOffer->status === 'available' && $shopOfferFood->shopOffer->start_date->isFuture()) ||
            ($shopOfferFood->shopOffer->status === 'available' && $shopOfferFood->shopOffer->end_date->addDays(1)->isPast()) ||
            $shopOfferFood->shopOffer->status !== 'available' ):

            $this->shopOfferFoodRepository->delete($id);

        elseif( $shopOfferFood->shopOffer->status === 'available' && $shopOfferFood->shopOffer->start_date->isPast() && $shopOfferFood->shopOffer->end_date->addDays(1)->isFuture() ):

            return response()->json(['error' => 'Non puoi rimuovere un prodotto da un\'offerta attiva'], 200);

        endif;


        Flash::success('Shop Offer Food deleted successfully.');
        return response()->json(['success' => 'Shop Offer Food deleted successfully'], 200);
    }
}
