<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Repositories\FoodCategoryRepository;
use App\Repositories\FoodRepository;
use App\Repositories\ShopCatalogRepository;
use App\Repositories\UnitOfMeasureRepository;
use Illuminate\Http\Request;

class UnitOfMeasuresAPIController extends Controller
{
    private $unitOfMeasureRepository,
            $foodCategoryRepository,
            $foodRepository,
            $shopCatalogRepository;

    public function __construct(UnitOfMeasureRepository $unitOfMeasureRepository,
                                FoodCategoryRepository $foodCategoryRepository,
                                FoodRepository $foodRepository,
                                ShopCatalogRepository $shopCatalogRepository)
    {
        $this->unitOfMeasureRepository = $unitOfMeasureRepository;
        $this->foodCategoryRepository = $foodCategoryRepository;
        $this->foodRepository = $foodRepository;
        $this->shopCatalogRepository = $shopCatalogRepository;
    }

    public function index(){
        $uom = $this->unitOfMeasureRepository->all([], null, null, ['*'], 'name');
        return $uom;
    }

    public function replace(Request $request){
        $input = $request->all();

        $foodCategories = $this->foodCategoryRepository->byUnitOfMeasure($input['old_uom']);
        $foods = $this->foodRepository->byUnitOfMeasure($input['old_uom']);
        $shopCatalogs = $this->shopCatalogRepository->byUnitOfMeasure($input['old_uom']);
        $shopCatalogsBuy = $this->shopCatalogRepository->byUnitOfMeasureBuy($input['old_uom']);

        foreach($foodCategories as $foodCategory):
            $this->foodCategoryRepository->update(['unit_of_measure' => $input['replace_uom']], $foodCategory->id);
        endforeach;

        foreach($foods as $food):
            $this->foodRepository->update(['unit_of_measure_id' => $input['replace_uom']], $food->id);
        endforeach;

        foreach($shopCatalogs as $shopCatalog):
            $this->shopCatalogRepository->update(['unit_of_measure_id' => $input['replace_uom']], $shopCatalog->id);
        endforeach;

        foreach($shopCatalogsBuy as $shopCatalog):
            $this->shopCatalogRepository->update(['unit_of_measure_buy' => $input['replace_uom']], $shopCatalog->id);
        endforeach;

        if( $input["flag_delete"] == "true" ){
            $this->unitOfMeasureRepository->find($input['old_uom'])->delete();
        }

        return response()->json(['status' => 'ok']);
    }
}
