<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;

use App\Http\Requests\API\CreateUserAPIRequest;
use App\Http\Requests\API\CreateShopAPIRequest;
use App\Http\Requests\API\UpdateUserAPIRequest;
use App\Http\Requests\API\GiveRemovePointsRequest;
use App\Http\Requests\API\SendMessageAPIRequest;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

use App\Models\User;
use App\Models\FoodCategory;
use App\Models\Shop;
Use App\Models\ShopCatalog;
Use App\Models\ShopOffer;
use App\Models\Agent;
use App\Models\DeviceToken;
use App\Models\UserSubscription;

use App\Repositories\UserRepository;
use App\Repositories\OrderRepository;
use App\Repositories\OrderChatRepository;
use App\Repositories\ShopRepository;
use App\Repositories\ShopTypeRepository;
use App\Repositories\PaymentMethodRepository;
use App\Repositories\PaymentMethodShopRepository;

use App\Notifications\CompanyRegistered;
use App\Notifications\PrivateRegistered;
use App\Notifications\CompanyRegisteredAdmin;
use App\Notifications\RegisterOTP;
use App\Notifications\GivenPoints;
use App\Notifications\RemovedPoints;
use App\Notifications\MessageReceived;

use App\Rules\StrongPassword;
use Response;
use Carbon\Carbon;
use Image;
use Notification;
use DB;
use smsCls;
use Log;
use Route;
use File;

/**
 * Class UserController
 * @package App\Http\Controllers\API
 */

class UserAPIController extends AppBaseController
{
    /** @var  UserRepository */
    private $userRepository,
            $orderRepository,
            $orderChatRepository,
            $shopRepository,
            $shopTypeRepository,
            $paymentMethodRepository,
            $paymentMethodShopRepository;

    public function __construct(UserRepository $userRepo,
                                OrderRepository $orderRepository,
                                OrderChatRepository $orderChatRepository,
                                ShopRepository $shopRepository,
                                ShopTypeRepository $shopTypeRepository,
                                PaymentMethodRepository $paymentMethodRepository,
                                PaymentMethodShopRepository $paymentMethodShopRepository)
    {
        $this->userRepository = $userRepo;
        $this->orderRepository = $orderRepository;
        $this->orderChatRepository = $orderChatRepository;
        $this->shopRepository = $shopRepository;
        $this->shopTypeRepository = $shopTypeRepository;
        $this->paymentMethodRepository = $paymentMethodRepository;
        $this->paymentMethodShopRepository = $paymentMethodShopRepository;
    }

    /**
     * Display a listing of the User.
     * GET|HEAD /users
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $users = $this->userRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($users->toArray(), 'Users retrieved successfully');
    }

    /**
     * User Login .
     * POST /login
     *
     * @param Request $request
     * @return api_token
     */
    public function login(Request $request)
    {
        if ($request->input('email') == "demo@demo.it")
            $token = "qnecYmKikABMrwbdURpIt9QX2nOgvyaX";
        else
            $token = Str::random(60);


        Log::info("login");
        Log::info($request->all());

        //Facebook authentication
        if(!empty($request->input('facebook_id'))){
            $user = User::where('facebook_id', $request->input('facebook_id'))->first();
        } else {
            //Standard authentication
            $messages = [
                            'email.required' => 'Email is required',
                            'password.required' => 'Password is required',
                        ];

            $validator = Validator::make($request->all(), [
                'email' => 'required',
                'password' => 'required'
            ], $messages);

            if($validator->fails()){
                Log::info("Login validator fails");
                Log::info($validator->errors());
                return response()->json([
                   'errors' =>  $validator->errors()
                ], 400);
            }
            if( auth()->attempt(['email' => $request->input('email'), 'password' => $request->input('password')]) ){

                Log::info("Login OK");

                $user = auth()->user();

                if( $user->status == 'inactive' ){
                    return response()->json(['errors' => ['Inactive' => 'Utente non attivo']], 400);
                }

                /*
                if(!empty($user->subscription->expire_date)){
                    if(!$user->subscription->expire_date->isFuture()){
                        $user->api_token = null;
                        $user->save();
                        return response()->json(['errors' => ['Subscription' => "L'abbonamento è scaduto"]], 400);
                    }
                }
                */
			}else{
                Log::info("Credenziali errate");
                return response()->json(['errors' => ['Inactive' => 'Credenziali errate']], 400);
            }
        }

        if(!empty($user)){

            $device_token = null;

            $user->api_token = hash('sha256', $token);

            if(!empty($request->input('device_token'))){
                //Update device_token if passed
                $device_token = DeviceToken::where('user_id' , $user->id)
                                            ->where('token' , $request->input('device_token'))
                                            ->first();

                if( !$device_token ):

                    $input = $request->all();

                    $device_token['user_id'] = $user->id;
                    $device_token['token'] = $input['device_token'];
                    $device_token['so'] = $input['so'] ?? 'android';
                    $messages = [
                                    'token.required' => 'Token is required'
                                    //,'token.unique' => 'This device token is still in use',
                                ];
                    $validator = Validator::make($device_token, DeviceToken::$rules, $messages);
                    if($validator->fails()){//Rollback
                        Log::info("Post login validator fails");
                        Log::info($validator->errors());
                        return response()->json(['errors' => $validator->errors()], 400);
                    } else {

                        DeviceToken::create($device_token);

                        // Rimuovo altri utenti collegati allo stesso dispositivo
                        /*
                        DeviceToken::where('token', $input['device_token'])
                                   ->where('user_id', '!=', $user->id)
                                   ->delete();
                                   */
                    }
                endif;

                $device_token = $request->input('device_token');

            } else {
                //Get Device Token
                $obj_device_token = DeviceToken::select('token')->where('user_id' , $user->id)->first();
                if(!empty($obj_device_token) && !empty($obj_device_token->token)){
                    $device_token = $obj_device_token->token;
                }
            }
            $user->save();

            //send in response not hashed token
            $user->api_token = $token;

            if(!empty($device_token)){
                $user->device_token = $device_token;
            }
			//Get all other user stuff
            $user = $this->getDetails($user);

            if(!empty($device_token)){
                addOsTags($user, $device_token);
            }

            $user->url_app_ios = env('URL_APP_IOS', '') ;
            $user->url_app_android = env('URL_APP_ANDROID', '');

            return response()->json(['user' => $user,], 200);
        } else {
            return response()->json(['errors' => ['Auth failed' => 'Wrong email or password or facebook login fails or user not Active',]], 400);
        }
    }

    /**
     * User Detail .
     * private
     *
     * @param User $user
     * @return User
     */
	private function getDetails(User $user)
	{
		if(!empty($user)){
			if(!empty($user->subscription)){
				$user->subscription = $user->subscription;
				$user->subscription->max_producs = UserSubscription::$subscriptions[$user->subscription->subscription];
				//Days to expiration date
				$subscription_end = new Carbon($user->subscription->expire_date);
				$days_left = $subscription_end->diffInDays();
				$hours_left = $subscription_end->diffInHours();
				$user->subscription->expiration_date_info = [
											'in_days' => $days_left,
											'in_hours' => $hours_left,
										 ];
			}
			if(!empty($user->shop)){
                $user->shop = $user->shop;
                $user->shop->agent;
				$shop_catalog = ShopCatalog::where('shop_id',$user->shop->id)->get();
				$user->shop_catalog = $shop_catalog;
				$shop_offer = ShopOffer::where('shop_id',$user->shop->id)->get();
				$user->shop_offer = $shop_offer;
				$payment_methods = DB::table('payment_method_shop')->where('shop_id', $user->shop->id)->get();
				if(!empty($payment_methods)){
					$payment_method_ids = '';
					$delivery_payment_method_ids = '';
					foreach($payment_methods as $pm){
						if($pm->payment_for == 'delivery'){
							$delivery_payment_method_ids .= $pm->payment_method_id.',';
						} else {
							$payment_method_ids .= $pm->payment_method_id.',';
						}
					}
					$delivery_payment_method_ids = trim($delivery_payment_method_ids, ',');
					$payment_method_ids = trim($payment_method_ids, ',');
				}
				$user->shop->payment_method_ids = $payment_method_ids;
				$user->shop->delivery_payment_method_ids = $delivery_payment_method_ids;
			}
			//Food categories for offer and catalog listing
			$food_category = FoodCategory::orderBy('name')->get();
            $user->food_categories = $food_category;

            $user->tot_notifications = 0; //count($user->unreadNotifications );

            foreach($user->unreadNotifications as $k => $notification):
                //dd($notification->type);
                if (
                    $notification->type === "App\Notifications\MessageReceived" ||
                    $notification->type === "App\Notifications\OrderModifiedUser" ||
                    $notification->type === "App\Notifications\OrderModified" ||
                    $notification->type === "App\Notifications\OrderSent" ||
                    $notification->type === "App\Notifications\OrderConfirmed" ||
                    $notification->type === "App\Notifications\AlertConfirmedWithReserveUser" ||
                    $notification->type === "App\Notifications\OrderConfirmedWithReserve" ||
                    $notification->type === "App\Notifications\OrderNotDeliveredCopy" ||
                    $notification->type === "App\Notifications\OrderNotDelivered" ||
                    $notification->type === "App\Notifications\OrderDelivered" ||
                    $notification->type === "App\Notifications\OrderCancelled" ||
                    $notification->type === "App\Notifications\OrderCancelledAuto"
                ){
                    $user->notifications[$k]->name = notificationName($notification);
                    $user->tot_notifications++;
                }

            endforeach;

            //dd($user->unreadNotifications);
        }


		return $user;
	}

    /**
     * User Login .
     * POST /logout
     *
     * @param Request $request
     * @return void
     */
    public function logout()
    {
        if($user = auth()->user()) {
            $user->api_token = '' ;
            $user->save();
        }
        return response()->json([
            'success' => 'user logout ok',
        ], 200);
    }

	/**
	 * Main store function to store user
	 *
	 */
    public function mainStore($input = [])
    {

        Log::info("mainStore");
        Log::info($input);

        $rules = [
            'facebook_id' => 'nullable|unique:users',
            'birthdate' => 'date|required'
        ];

        $messages = [
            'email.required'  => 'L\email è obbligatoria',
            'email.unique' => 'L\'email digitata risulta già utilizzata',
            'birthdate.date' => 'Il formato della data di nascita non è valido',
            'birthdate.required' => 'La data di nascita è obbligatoria',
            'facebook_id.unique' => 'L\'ID Facebook risulta già utilizzato',
            'password.required' => 'La password è obligatoria',
            'paasword.confirmed' => 'La password di conferma non corrisponde'
        ];

        $route = basename(url()->current());

        switch( $route ){
            case 'register':
            case 'register-company':

                $rules = array_merge($rules, [
                    'password' => ['required', 'confirmed', new StrongPassword],
                    'email' => 'required|email|unique:users,email,'.Auth::id(),
                ]);

                break;

            case 'store':

                $rules = array_merge($rules, [
                    'password' => ['nullable', 'confirmed', new StrongPassword],
                    'email' => 'email|unique:users,email,'.Auth::id(),
                ]);

                break;
        }


        $input_validator = Validator::make($input, $rules, $messages);
        if($input_validator->fails()){
            return response()->json(['errors' => $input_validator->errors()], 400);
        }

        $imageName = null;

        if(!empty($input['user_image'])){//Store image

			$image_rule = [
				//'user_image' => 'image|mimes:jpeg,png,jpg,gif|max:2048',
			];
			$image_messages = [
							'user_image.image' => 'Image type not allowed',
							'user_image.mimes' => 'Mime type not allowed',
							'user_image.max' => 'Size of image is too big. Max 2Mb allowed',
						];
			//echo $request->user_image;exit;
			$image_validator = Validator::make(['user_image' => $input['user_image']], $image_rule, $image_messages);
			if($image_validator->fails()){
				return response()->json(['errors' => $image_validator->errors()], 400);
			}
            $imageName = NormalizeString($input['name']).'_'.time().'.'.$input['user_image']->extension();
        }else{
            if( $route === 'register' ){
                Log::info("errore immagine profilo");
                return response()->json(['errors' => ['user_image' => "Inserisci una foto del tuo volto"]], 400);
            }
        }


		//User Update
		if(isset($input['action']) && !empty($input['action']) && $input['action'] == 'update'){

            $user = auth()->user();

            /** Check user age */
            $user_age = Carbon::parse($input['birthdate'])->age;
            if($user_age < 14 && $user->user_type === 'private' ){
                return response()->json(['errors' => ['User age' => 'Questa App non può essere usata da minori di 14 anni']], 400);
            }elseif($user_age < 18 && $user->user_type === 'company' ){
                return response()->json(['errors' => ['User age' => 'Il titolare di un attività non può essere minorenne']], 400);
            }

			if(empty($user)){
				return response()->json(['errors' => ['Not found' => 'User not found']], 400);
			}
			$input['user_type'] = $user->user_type;
			$user->birthdate = $input['birthdate'];
			$user->name = $input['name'];
			$user->phone = $input['phone'];
			$user->other_phone = !empty($input['other_phone']) ? $input['other_phone'] : '';
			$user->delivery_address = !empty($input['delivery_address']) ? $input['delivery_address'] : '';
			$user->delivery_country = !empty($input['delivery_country']) ? $input['delivery_country'] : '';
			$user->delivery_route = !empty($input['delivery_route']) ? $input['delivery_route'] : '';
			$user->delivery_city = !empty($input['delivery_city']) ? $input['delivery_city'] : '';
			$user->delivery_region = !empty($input['delivery_region']) ? $input['delivery_region'] : '';
			$user->delivery_postal_code = !empty($input['delivery_postal_code']) ? $input['delivery_postal_code'] : '';
			$user->delivery_street_number = !empty($input['delivery_street_number']) ? $input['delivery_street_number'] : '';
			$user->delivery_lat = !empty($input['delivery_lat']) ? $input['delivery_lat'] : '';
			$user->delivery_long = !empty($input['delivery_long']) ? $input['delivery_long'] : '';
            $user->note_delivery_address = !empty($input['note_delivery_address']) ? $input['note_delivery_address'] : '';

            if( isset($input['password']) &&
                !empty($input['password']) &&
                isset($input['email']) &&
                $input['email'] !== 'demo@demo.it'):

                $user->password = Hash::make($input['password']);

            endif;

		} else {
            //New user registration
            Log::info("New user registration");

            if(empty($input['user_type'])){
				$input['user_type'] = 'private';
			}

            /** Check user age */
            $user_age = Carbon::parse($input['birthdate'])->age;
            if($user_age < 14 && $input['user_type'] === 'private' ){
                return response()->json(['errors' => ['User age' => 'Questa App non può essere usata da minori di 14 anni']], 400);
            }elseif($user_age < 18 && $input['user_type'] === 'company' ){
                return response()->json(['errors' => ['User age' => 'Il titolare di un attività non può essere minorenne']], 400);
            }

            //Set status to active directly
            //if( $input['user_type'] == 'private' ){
            //    $input['status'] = 'active';
            //}else{
                $input['status'] = 'inactive';
            //}
			$input['trust_points'] = 100;
			$input['password'] = Hash::make($input['password']);

			///Authenticate after having registered
			$api_token = Str::random(60);
			$input['api_token'] = hash('sha256', $api_token);
			//If register through FB status is active and email not to send
			/*
			if(!empty($input['facebook_id'])){
				$input['status'] = 'active';
			}
            */

	        $user = User::create($input);
            $user->api_token = $api_token;
            $user->save();
        }

        //Set device token if passed
        if(!empty($input['device_token']) && !empty($input['so'])){

            $device_token['user_id'] = $user->id;
            $device_token['token'] = $input['device_token'];
            $device_token['so'] = $input['so'];
            $messages = [
                            'token.required' => 'Token is required',
                            'token.unique' => 'This device token is still in use',
                        ];
            $validator = Validator::make($device_token, DeviceToken::$rules, $messages);
            if($validator->fails()){//Rollback
                User::destroy($user->id);
                return response()->json(['errors' => $validator->errors()], 400);
            } else {
                DeviceToken::create($device_token);
            }

        }

		//Image upload
        if( !empty($imageName) ){
            /** Remove old image if present */
            if( !empty( $user->image_url ) ):
                @unlink(storage_path('app/public/images/users/'.$user->id.'/').basename($user->image_url));
                @unlink(storage_path('app/public/images/users/'.$user->id.'/').getResizedImage(basename($user->image_url), '100x100'));
                @unlink(storage_path('app/public/images/users/'.$user->id.'/').getResizedImage(basename($user->image_url), '640x320'));
                /*
                @unlink(storage_path('app/public/images/users/').getResizedImage(basename($user->image_url), '310x240'));
                @unlink(storage_path('app/public/images/users/').getResizedImage(basename($user->image_url), '750x306'));
                */
            endif;

            $folderPath = storage_path('app/public/images/users/'.$user->id);

            if( !File::isDirectory($folderPath) ) {
                File::makeDirectory($folderPath, 0775, true); //creates directory
            }
	        $input['user_image']->move($folderPath, $imageName);
            $user->image_url = asset('/storage/images/users/'.$user->id.'/'.$imageName);
            //Generate cropped thumbs image
            $path_image_name = $folderPath.'/'.$imageName;
            $image = Image::make($path_image_name)->orientate();
            cropImages($image,$path_image_name,'user');
			$user->save();
		}

        if(!isset($input['action']) || empty($input['action'])){ //New user
			//Insert into user_subscription
			$subscription_input['user_id'] = $user->id;
			$subscription_input['subscription'] = 'free';
			$subscription_input['submission_date'] = Carbon::now();

			//Agent
            //Log::info("Coupon: " . $input['coupon']);

			if(!empty($input['coupon'])){
                //Add 30 days to expire date
                $subscription_input['expire_date'] = Carbon::now()->add(30, 'day');
			}else{
                //Expire date
			    $subscription_input['expire_date'] = Carbon::now()->addYears(10);
            }
			UserSubscription::create($subscription_input);
			//return api_token not hashed

		}

		return $user;
    }

    public function register(Request $request)
    {

        $input = $request->all();

		$response = $this->mainStore($request->all());

        Log::info("response register");
        Log::info($response);

		//Check response
		if($response instanceof \Illuminate\Http\JsonResponse){
            Log::info("exit register");
			return $response;
		};

		$user = $response;
		if(empty($user->facebook_id)){
			//XXX Send activation email
        }

        // Gerate OTP
        $found = false;
        do{
            $otp = mt_rand(1000,10000);
            $found = $this->userRepository->checkOtp($otp);
        }while( $found );

        $user->otp = $otp;
        $user->save();

        $sms_sent = false;
        $email_sent = true;  //abbiamo tolto l'invio per email. - Flavio 20/11/2020

        if(!empty($user->phone)){
            /** Invia SMS */
            $sms = new smsCls();
            $sms->inviaSms('Codice di attivazione Primascelta.biz: ' . $otp, [ $user->phone] );
            $sms_sent = true;
        }

        /*
        if($user->email){
            Mail::to($user->email)->send(new \App\Mail\RegisterOTP($otp, $user));
            $email_sent = true;
        }
        */

        addOsTags($user, $input['device_token']);

        if( !$sms_sent && !$email_sent ){
            return response()->json(['errors' => ['Errore invio OTP' => 'Si è verificato un errore nell\'invio dell OTP']], 400);
        }

        return response()->json(['Success' => 'User Registered successfully', 'user' => $user->toArray()], 200);
    }

    /**
     * Stores a newly User in storage.
     * POST /users
     *
     * @param CreateUserAPIRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {

        if (Auth::user()->email === "demo@demo.it"){
            return response()->json(['Success' => 'Dati salvati con successo', 'user' => Auth::user()], 200);
        }

		$response = $this->mainStore($request->all());

		//Check response
		if($response instanceof \Illuminate\Http\JsonResponse){
			return $response;
		};

		$user = $response;

        return response()->json(['Success' => 'Dati salvati con successo', 'user' => $user->toArray()], 200);

    }

    /**
     * Store a newly created User Company in storage.
     *
     *
     * @param CreateShopAPIRequest $request
     *
     * @return Response
     */
    public function registerCompany(CreateShopAPIRequest $request)
    {
        $input = $request->all();

        Log::info("register company");
        Log::info($input);

        $userData = $input;
        $userData['name'] = $input['user_name'];
        $userData['birthdate'] = $input['user_birthdate'];
        $userData['user_type'] = 'company';
        $response = $this->mainStore($userData);

		//Check response
		if($response instanceof \Illuminate\Http\JsonResponse){
			return $response;
		};
        $user = $response;


		$input['user_id'] = $user->id;
		//Passed from main registration
		$input['agent_id'] = $user->shop_agent_id;
		//remove from params passed by mainStore
		unset($user->shop_agent_id);

        if(isset($input['action']) && !empty($input['action']) && $input['action'] == 'update' ){ //Update shop
            //Update shop
			$shop = Shop::find($user->shop->id);
		} else {
            //Create new shop
            $shop = new Shop;
            $shop->user_id = $user->id;
        }

        // check coords
        if( !empty($input['lat']) && !empty($input['long']) ){
            $shop->lat = !empty($input['lat']) ? $input['lat'] : '';
            $shop->long = !empty($input['long']) ? $input['long'] : '';
        }else{
            $coords = coordsFromAddress($input['address']);
            $shop->lat = $coords['lat'];
            $shop->long = $coords['lon'];
        }

        $shop->name = !empty($input['shop_name']) ? $input['shop_name'] : '';
        $shop->address = !empty($input['address']) ? $input['address'] : '';
        $shop->website = !empty($input['website']) ? $input['website'] : '';
        $shop->country = !empty($input['country']) ? $input['country'] : '';
        $shop->route = !empty($input['route']) ? $input['route'] : '';
        $shop->city = !empty($input['city']) ? $input['city'] : '';
        $shop->region = !empty($input['region']) ? $input['region'] : '';
        $shop->postal_code = !empty($input['postal_code']) ? $input['postal_code'] : '';
        $shop->street_number = !empty($input['street_number']) ? $input['street_number'] : '';
        $shop->whatsapp = !empty($input['whatsapp']) ? $input['whatsapp'] : '';
        $shop->tax_business_name = !empty($input['tax_business_name']) ? $input['tax_business_name'] : '';
        $shop->tax_type_id = !empty($input['tax_type_id']) ? $input['tax_type_id'] : 0;
        $shop->tax_vat = !empty($input['tax_vat']) ? $input['tax_vat'] : '';
        $shop->tax_fiscal_code = !empty($input['tax_fiscal_code']) ? $input['tax_fiscal_code'] : '';
        $shop->tax_address = !empty($input['tax_address']) ? $input['tax_address'] : '';
        $shop->tax_address_country = !empty($input['tax_address_country']) ? $input['tax_address_country'] : '';
        $shop->tax_address_route = !empty($input['tax_address_route']) ? $input['tax_address_route'] : '';
        $shop->tax_address_city = !empty($input['tax_address_city']) ? $input['tax_address_city'] : '';
        $shop->tax_address_prov = !empty($input['tax_address_prov']) ? $input['tax_address_prov'] : '';
        $shop->tax_address_region = !empty($input['tax_address_region']) ? $input['tax_address_region'] : '';
        $shop->tax_address_postal_code = !empty($input['tax_address_postal_code']) ? $input['tax_address_postal_code'] : '';
        $shop->tax_address_street_number = !empty($input['tax_address_street_number']) ? $input['tax_address_street_number'] : '';
        $shop->tax_address_lat = !empty($input['tax_address_lat']) ? $input['tax_address_lat'] : '';
        $shop->tax_address_long = !empty($input['tax_address_long']) ? $input['tax_address_long'] : '';
        $shop->status = 'pending';

        Log::info("save shop");
        Log::info($shop->toArray());

        if(!isset($input['action']) || empty($input['action'])){ //New user

            Log::info("Coupon: " . $input['coupon']);

			if(!empty($input['coupon'])){
				$agent = Agent::select('id')->where('coupon', $input['coupon'])->get();
                Log::info("Agent");
                Log::info($agent);
				if(count($agent) == 1){
					$shop->agent_id = $agent[0]->id;
				}
			}
		}

        $shop->save();

        if( !$shop ){
            $user->delete();
            return response()->json(['errors' => ['Errore' => 'Si è verificato un errore durante la creazione dell\'utenza']], 400);
        }

        if(empty($user->facebook_id)){
            //XXX Send activation email
        }

        // Shop types
        if(!empty(trim($input['shop_type_id']))){			;
            $arr_shop_types = explode(',', $input['shop_type_id']);
            foreach($arr_shop_types as $shop_type_id){
                $shopType = $this->shopTypeRepository->find($shop_type_id);
                try{
                    $shop->shopTypes()->attach($shopType);
                }catch(\Exception $e){

                }
            }
        }

        // Gerate OTP
        $found = false;
        do{
            $otp = mt_rand(1000,10000);
            $found = $this->userRepository->checkOtp($otp);
        }while( $found );

        $user->otp = $otp;
        $user->save();

        $sms_sent = false;
        $email_sent = true;

        if(!empty($shop->whatsapp)){
            /** Invia SMS */
            $sms = new smsCls();
            $sms->inviaSms('Codice di attivazione Primascelta.biz: ' . $otp, [ $shop->whatsapp] );
            $sms_sent = true;
        }

        /*
        if($user->email){
            Mail::to($user->email)->send(new \App\Mail\RegisterOTP($otp, $user));
            $email_sent = true;
        }
        */

        if( !$sms_sent && !$email_sent ){
            return response()->json(['errors' => ['Errore invio OTP' => 'Si è verificato un errore nell\'invio dell OTP']], 400);
        }

		return response()->json(['Success' => 'Dati salvati con successo', 'user' => $user, 'shop' => $shop], 200);
    }

    /**
     * Update User Company
     *
     *
     * @param CreateShopAPIRequest $request
     *
     * @return Response
     */
    public function storeCompany(CreateShopAPIRequest $request)
    {
        $input = $request->all();

        if (Auth::user()->email === "demo@demo.it"){
            return response()->json(['Success' => 'Dati salvati con successo', 'user' => Auth::user(), 'shop' => Auth::user()->shop], 200);
        }

		//User type is company
        $userData = $input;
        $userData['name'] = $input['user_name'];
        $userData['birthdate'] = $input['user_birthdate'];
        $userData['user_type'] = 'company';
        $response = $this->mainStore($userData);

		//Check response
		if($response instanceof \Illuminate\Http\JsonResponse){
			return $response;
		};
        $user = $response;


		$input['user_id'] = $user->id;
		//Passed from main registration
		$input['agent_id'] = $user->shop_agent_id;
		//remove from params passed by mainStore
		unset($user->shop_agent_id);


        if(isset($input['action']) && !empty($input['action']) && ($input['action'] == 'update')){//Update shop
            //Update shop
			$shop = Shop::find($user->shop->id);
		} else {
            //Create new shop
            $shop = new Shop;
            $shop->user_id = $user->id;
        }

        //$shop->shop_type_id = $input['shop_type_id'];

        if(!empty($input['shop_name'])){
            $shop->name = $input['shop_name'];
        }
        if(!empty($input['address'])){
            $shop->address = $input['address'];
        }
        //var_dump($input);exit;
        $shop->website = !empty($input['website']) ? $input['website'] : '';
        $shop->country = !empty($input['country']) ? $input['country'] : '';
        $shop->route = !empty($input['route']) ? $input['route'] : '';
        $shop->city = !empty($input['city']) ? $input['city'] : '';
        $shop->region = !empty($input['region']) ? $input['region'] : '';
        $shop->postal_code = !empty($input['postal_code']) ? $input['postal_code'] : '';
        $shop->street_number = !empty($input['street_number']) ? $input['street_number'] : '';
        $shop->lat = !empty($input['lat']) ? $input['lat'] : '';
        $shop->long = !empty($input['long']) ? $input['long'] : '';
        $shop->whatsapp = !empty($input['whatsapp']) ? $input['whatsapp'] : '';
        $shop->facebook_page = !empty($input['facebook_page']) ? $input['facebook_page'] : '';
        $shop->instagram_page = !empty($input['instagram_page']) ? $input['instagram_page'] : '';
        $shop->description = !empty($input['description']) ? $input['description'] : '';
        $shop->opening_timetable = !empty($input['opening_timetable']) ? $input['opening_timetable'] : '';
        $shop->delivery_on_site = !empty($input['delivery_on_site']) ? $input['delivery_on_site'] : 0;
        $shop->delivery_address = !empty($input['delivery_address']) ? $input['delivery_address'] : '';
        $shop->delivery_address_country = !empty($input['delivery_address_country']) ? $input['delivery_address_country'] : '';
        $shop->delivery_address_route = !empty($input['delivery_address_route']) ? $input['delivery_address_route'] : '';
        $shop->delivery_address_city = !empty($input['delivery_address_city']) ? $input['delivery_address_city'] : '';
        $shop->delivery_address_region = !empty($input['delivery_address_region']) ? $input['delivery_address_region'] : '';
        $shop->delivery_address_postal_code = !empty($input['delivery_address_postal_code']) ? $input['delivery_address_postal_code'] : '';
        $shop->delivery_address_street_number = !empty($input['delivery_address_street_number']) ? $input['delivery_address_street_number'] : '';
        $shop->delivery_address_lat = !empty($input['delivery_address_lat']) ? $input['delivery_address_lat'] : '';
        $shop->delivery_address_long = !empty($input['delivery_address_long']) ? $input['delivery_address_long'] : '';
        $shop->delivery_opening_timetable = !empty($input['delivery_opening_timetable']) ? $input['delivery_opening_timetable'] : '';
        $shop->home_delivery = !empty($input['home_delivery']) ? $input['home_delivery'] : 0;
        $shop->home_delivery_min = !empty($input['home_delivery_min']) ? $input['home_delivery_min'] : null;
        $shop->delivery_range_km = !empty($input['delivery_range_km']) ? $input['delivery_range_km'] : null;
        $shop->delivery_range_notes = !empty($input['delivery_range_notes']) ? $input['delivery_range_notes'] : null;
        $shop->delivery_host_on_site = !empty($input['delivery_host_on_site']) ? $input['delivery_host_on_site'] : 0;
        $shop->delivery_always_free = !empty($input['delivery_always_free']) ? $input['delivery_always_free'] : 0;
        $shop->delivery_free_price_greater_than = !empty($input['delivery_free_price_greater_than']) ? $input['delivery_free_price_greater_than'] : null;
        $shop->delivery_forfait_cost_price = !empty($input['delivery_forfait_cost_price']) ? $input['delivery_forfait_cost_price'] : null;
        $shop->delivery_percentage_cost_price = !empty($input['delivery_percentage_cost_price']) ? $input['delivery_percentage_cost_price'] : null;
        $shop->delivery_cancellable_hours_limit = !empty($input['delivery_cancellable_hours_limit']) ? $input['delivery_cancellable_hours_limit'] : 0;
        $shop->delivery_host_on_site = !empty($input['delivery_host_on_site']) ? $input['delivery_host_on_site'] : 0;
        $shop->min_trust_points_percentage_bookable = !empty($input['min_trust_points_percentage_bookable']) ? $input['min_trust_points_percentage_bookable'] : 0;
        $shop->tax_business_name = !empty($input['tax_business_name']) ? $input['tax_business_name'] : '';
        $shop->tax_type_id = !empty($input['tax_type_id']) ? $input['tax_type_id'] : 0;
        $shop->tax_vat = !empty($input['tax_vat']) ? $input['tax_vat'] : '';
        $shop->tax_fiscal_code = !empty($input['tax_fiscal_code']) ? $input['tax_fiscal_code'] : '';
        $shop->tax_code = !empty($input['tax_code']) ? $input['tax_code'] : '';
        $shop->tax_address = !empty($input['tax_address']) ? $input['tax_address'] : '';
        $shop->tax_address_country = !empty($input['tax_address_country']) ? $input['tax_address_country'] : '';
        $shop->tax_address_route = !empty($input['tax_address_route']) ? $input['tax_address_route'] : '';
        $shop->tax_address_city = !empty($input['tax_address_city']) ? $input['tax_address_city'] : '';
        $shop->tax_address_prov = !empty($input['tax_address_prov']) ? $input['tax_address_prov'] : '';
        $shop->tax_address_region = !empty($input['tax_address_region']) ? $input['tax_address_region'] : '';
        $shop->tax_address_postal_code = !empty($input['tax_address_postal_code']) ? $input['tax_address_postal_code'] : '';
        $shop->tax_address_street_number = !empty($input['tax_address_street_number']) ? $input['tax_address_street_number'] : '';
        $shop->tax_address_lat = !empty($input['tax_address_lat']) ? $input['tax_address_lat'] : '';
        $shop->tax_address_long = !empty($input['tax_address_long']) ? $input['tax_address_long'] : '';
        $shop->status = 'pending';
        $shop->save();

        if( !$shop ){
            $user->delete();
            return response()->json(['errors' => ['Errore' => 'Si è verificato un errore durante la creazione dell\'utenza']], 400);
        }

        //ShopTypes
        $shop->shopTypes()->detach();
        foreach($input['shop_type_id'] as $shop_type_id):
            $shopType = $this->shopTypeRepository->find($shop_type_id);
            try{
                $shop->shopTypes()->attach($shopType);
            }catch(\Exception $e){

            }
        endforeach;

		//payment methods
        if(!empty(trim($input['payment_method_ids']))){			;
            $arr_payment_methods = explode(',', $input['payment_method_ids']);
            foreach($user->shop->PaymentMethodsShop as $paymentMethod){
                if( !in_array($paymentMethod->id, $arr_payment_methods) ){
                    $this->paymentMethodShopRepository->search($user->shop->id, $paymentMethod->id)->delete();
                }
            }

            if( count($arr_payment_methods) > 0 ){
                foreach($arr_payment_methods as $payment_method_id){
                    if( !in_array($payment_method_id, $user->shop->PaymentMethodsShop->pluck('id')->toArray()) ){
                        $paymentMethod = $this->paymentMethodRepository->find($payment_method_id);
                        if( $paymentMethod ){
                            $this->paymentMethodShopRepository->create([
                                'shop_id' => $user->shop->id,
                                'payment_method_id' => $paymentMethod->id
                            ]);
                        }
                    }
                }
            }
        }else{
            $paymentMethods = $this->paymentMethodShopRepository->paymentMethods($user->shop->id);
            foreach($paymentMethods as $paymentMethod){
                $paymentMethod->delete();
            }
        }

		//delivery payment methods
		if(!empty(trim($input['delivery_payment_method_ids']))){
            $arr_payment_methods = explode(',', $input['delivery_payment_method_ids']);

            foreach($user->shop->PaymentMethodsDelivery as $paymentMethod){
                if( !in_array($paymentMethod->id, $arr_payment_methods) ){
                    $this->paymentMethodShopRepository->search($user->shop->id, $paymentMethod->id, 'delivery')->delete();
                }
            }

            if( count($arr_payment_methods) > 0 ){
                foreach($arr_payment_methods as $payment_method_id){
                    if( !in_array($payment_method_id, $user->shop->PaymentMethodsDelivery->pluck('id')->toArray()) ){
                        $paymentMethod = $this->paymentMethodRepository->find($payment_method_id);
                        if( $paymentMethod ){
                            $this->paymentMethodShopRepository->create([
                                'shop_id' => $user->shop->id,
                                'payment_method_id' => $paymentMethod->id,
                                'payment_for' => 'delivery'
                            ]);
                        }
                    }
                }
            }
		}else{
            $paymentMethods = $this->paymentMethodShopRepository->paymentMethods($user->shop->id, 'delivery');
            foreach($paymentMethods as $paymentMethod){
                $paymentMethod->delete();
            }
        }

        addOsTags($user, $input['device_token']);

		return response()->json(['Success' => 'Dati salvati con successo', 'user' => $user, 'shop' => $shop], 200);
    }

    /**
     * Display the specified User.
     * GET|HEAD /users/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id = null)
    {
		if(empty($id)){
			$user = auth()->user();
		} else {
			/** @var User $user */
			$user = $this->userRepository->find($id);
		}

		if (empty($user)) {
			return response()->json(['errors' => ['Not found' => 'User not found']], 400);
		} else {
            $user->image_url = getResizedImage(getOriginalImage($user->image_url),'640x320');

			//retrieve all informationi
			$user = $this->getDetails($user);
	        return response()->json(['success' => $user], 200);
		}
    }

    /**
     * Update the specified User in storage.
     * PUT/PATCH /users/{id}
     *
     * @param int $id
     * @param UpdateUserAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUserAPIRequest $request)
    {
        $input = $request->all();

        /** @var User $user */
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            return $this->sendError('User not found');
        }

        $user = $this->userRepository->update($input, $id);

        return $this->sendResponse($user->toArray(), 'Utente modificato con successo');
    }

    /**
     * Remove the specified User from storage.
     * DELETE /users/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var User $user */
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            return $this->sendError('User not found');
        }

		UserSubscription::where('user_id', $user->id)->delete();
		DeviceToken::where('user_id', $user->id)->delete();
        $user->delete();

        return $this->sendSuccess('User deleted successfully');
    }

    /**
    * Generate OTP User .
    *
    *
    * @return string
    */
    private function generateOTP()
    {
        $otp = mt_rand(1000,10000);
        return $otp;
    }


    /**
    * Send OTP to user by Email and SMS
    * POST /users
    *
    * @param email, otp
    *
    * @return string
    */
    public function sendUserOtp(Request $request)
    {
        $input = $request->all();

        $sms_sent = false;
        $email_sent = true;

        $user = null;
        if( !empty($input['email']) ):
            $user = User::where('email', $input['email'])
                            ->whereIn('status', ['pending','active'])
                            ->first();

            if( $user ):
                $otp = $this->generateOTP();

                $phone = $user->user_type === 'company' ? $user->shop->whatsapp : $user->phone;

                if(!empty($phone) && !empty($otp)) {

                    $user->otp = $otp;
                    $user->save();

                    /** Invia SMS */
                    $sms = new smsCls();
                    $sms->inviaSms('Codice di attivazione Primascelta.biz: ' . $otp, [ $phone ]);
                    $sms_sent = true;
                }

                if( $sms_sent || $email_sent ){
                    return response()->json([
                        'success' => 'OTP inviato con success',
                    ], 200);
                }else{
                    return response()->json([
                        'errors' => ['OTP' => "Si è verificato un errore durante l'invio dell'OTP"],
                    ], 400);
                }
            else:
                return response()->json([
                    'errors' => ['user_not_found' => "Non è stato trovato nessun utente con l'email digitata"],
                ], 400);
            endif;
        else:
            return response()->json([
                'errors' => ['email_missing' => "Devi digitare un indirizzo mail valido"],
            ], 400);
        endif;

    }

    /**
    * Get OTP for User .
    * POST /users
    *
    * @param email, phone
    *
    * @return string
    */
    public function sendOtp(Request $request)
    {
        Log::info("STO INVIANDO OTP");
        $input = $request->all();

        if(!empty($input['email'])){
            $user = User::where('email', $input['email'])->first();
        }
        if(empty($user)){
            if(!empty($input['whatsapp'])){
                $user = User::where('whatsapp', $input['phone'])->first();
            }
        }
        if(!empty($user)){
            $otp = $this->generateOTP();
            $user->otp = $otp;
            $user->save();
            return response()->json([
                'otp' => $otp,
            ], 200);
            Log::info("OTP INIVIATO: ". $otp);
        } else {
            return response()->json([
                'errors' => ['User' => 'User not found'],
            ], 400);
            Log::info("OTP NON INVIATO");
        }

    }

    /**
    * Check email exists when registrating
    * POST /users
    *
    * @param email
    *
    * @return string
    */
    public function checkUserEmail(Request $request)
    {
        $input = $request->all();
        if(!empty($input['email'])){
            $user = User::where('email', $input['email'])->get();
            if(count($user) > 0){
                //User exists
                return response()->json(['Warning' => 'L\'email '.$input['email']. ' è già utilizzata'], 400);
            } else {
                return response()->json(['Success' => 'L\'email '.$input['email']. ' è libera'], 200);
            }
        }
    }

    /**
    * Validate OTP for User .
    * POST /users
    *
    * @param email, phone
    *
    * @return string
    */
    public function validateOtp(Request $request)
    {
        $input = $request->all();

        if(!empty($input['otp'])){
            if(!empty($input['email'])){
                $user = User::where('email',$input['email'])->where('otp', $input['otp'])->first();
            }
            if(empty($user)){
                if(!empty($input['phone'])){
                    $user = User::where('whatsapp', $input['phone'])->where('otp', $input['otp'])->first();
                }
            }
            if(!empty($user)){
                $user->otp = null;
                //Generate api_token if requested
                $token = '';
                if(isset($input['action']) && !empty($input['action']) && in_array($input['action'], ['login', 'register', 'forget-password'])){
                    $token = Str::random(60);
                    $user->api_token = hash('sha256', $token);
                }
                if( $user->user_type == 'private' ){
                    $user->status = 'active';
                }elseif( $user->user_type == 'company' ){
                    $user->status = 'pending';
                }
                //$user->status = 'active';
                $user->save();

                /** Send Welcome Notification */
                if (env('APP_ENV') === 'production'):
                    try {
                        if( $user->user_type === 'company' ):
                            $user->notify(new CompanyRegistered());

                            /** Send notification to Administrators */
                            $administrators = $this->userRepository->administrators();
                            foreach($administrators as $admin):
                                $admin->notify(new CompanyRegisteredAdmin($user));
                            endforeach;

                        elseif( $user->user_type === 'private' ):
                            $user->notify(new PrivateRegistered());
                        endif;
                    }catch(\Exception $e){
						//Storage::disk('local_data')->put('error_send_email.log', $e->getMessage());
                    }
                endif;

                return response()->json([
                    'message' => 'otp validated',
                    'api_token' => $token,
                ], 200);
            }else{
                return response()->json([
                    'errors' => ['user_not_found' => 'User not found'],
                ], 400);
            }
        }else{
            return response()->json([
                'errors' => ['Otp' => 'Otp non valido'],
            ], 400);
        }

    }

    /**
     * DEVELOP API
     * POST /develop
     *
     * @param status (useer status)
     *
     * return json
     */
    public function change_status(Request $request)
    {
        if($user = auth()->user()){
            $arr_status = ['active', 'inactive', 'pending'];
            $input = $request->all();
            if(!empty($input['status']) && in_array($input['status'], $arr_status)){
                $old_status = $user->status;
                $user->status = $input['status'];
                $user->save();
                return response()->json([
                    'message' => 'User status changed from '.$old_status.' to '.$user->status,
                ], 200);
            }
        }
    }

	/**
    * Change password request fot User .
    * POST /auth/password-recover
    *
    * @param email, phone
    * @description Request to change password
    * @return string
    */
    public function passwordRecover(Request $request)
    {
		$input = $request->all();
		if(empty($input['email'])){
			return response()->json(['errors' => ['Email mandatory' => 'L\'indirizzo email è obbligatorio']], 400);
		}

		$getuser = User::where('email', $input['email'])->get();
		if(empty($getuser[0]) || $input['email'] === 'demo@demo.it'){
			return response()->json(['errors' => ['Email not valid' => 'L\'indirizzo email non è valido']], 400);
		}
		//Generate Otp and save it on user table
		$otp = $this->generateOtp();
		$user = User::find($getuser[0]->id);
		$user->otp = $otp;
		$user->save();
		//Send email with otp
        Mail::to($input['email'])->send(new \App\Mail\SendEmailForPasswordRecover($input['email'],$otp));
		return response()->json(['success' => ['OK' => 'L\'Email di recupero password è stata inviata con successo']], 200);
    }

	/**
    * Change password action fot User .
    * POST /auth/password-recover
    *
    * @param otp, email, phone
    * @description Change password action
    * @return string
    */
    public function passwordChange(Request $request)
    {
		$input = $request->all();

		if(empty($input['email']) && !empty($input['otp']) && !empty($input['password'])){
			return response()->json(['errors' => ['required' => 'Email, Otp e Password sono campi obbligatori']], 400);
		}

        if($input['email'] === 'demo@demo.it'){
			return response()->json(['errors' => ['email_not_valid' => 'Email non valida']], 400);
		}

        $user = User::where('email', $input['email'])
                       ->whereIn('status', ['active', 'pending'])
                       ->first();

		if(empty($user)){
            return response()->json(['errors' => ['user_not_found' => "Non è stato trovato nessun utente con l'email digitata"]], 400);
        }

		if($user->otp != $input['otp']){
            return response()->json(['errors' => ['otp_not_valid' => "Il codice inserito non è valido"]], 400);
        }

		$user->password = Hash::make($input['password']);

        //RESET OTP
		$user->otp = null;
        $user->save();

		return response()->json(['ok' => 'Password cambiata'], 200);
	}

    public function givePoints($user_id, GiveRemovePointsRequest $request){

        $input = $request->all();

        $user = $this->userRepository->find($user_id);
        if( $user && $input['points'] > 0 ):
            $points = $user->trust_points + $input['points'];
            if($points > 100) $points = 100;

            $user = $this->userRepository->update([
                'trust_points' => $points,
            ], $user->id);

            $order = $this->orderRepository->find($input['order_id']);

            if( $order ):

                $order->shop;

                $user->notify(new GivenPoints(Auth::user(), $input['points'], $order));
                return response()->json(['success' => ['OK' => 'Trust points added successfully']], 200);

            else:

                return response()->json(['errors' => ['order_not_found' => 'Order not found']], 200);

            endif;

        else:

            if( empty($user) ):
                return response()->json(['errors' => ['user_not_found' => 'User not found']], 200);
            endif;

            if( $input['points'] < 1 ):
                return response()->json(['errors' => ['points' => 'Points should be 1 or more']], 200);
            endif;

        endif;

    }

    public function removePoints($user_id, GiveRemovePointsRequest $request){

        $input = $request->all();

        $user = $this->userRepository->find($user_id);
        if( $user && $input['points'] > 0 ):
            $points = $user->trust_points - $input['points'];
            if( $points < 0 ) $points = 0;

            $user = $this->userRepository->update([
                'trust_points' => $points,
            ], $user->id);

            if( $user ):

                $order = $this->orderRepository->find($input['order_id']);

                if( $order ):

                    $order->shop;

                    $user->notify(new RemovedPoints(Auth::user(), $input['points'], $order));
                    return response()->json(['success' => ['OK' => 'Trust points removed successfully']], 200);

                else:

                    return response()->json(['errors' => ['user_not_found' => 'Order not found']], 200);

                endif;

            endif;

        else:

            if( empty($user) ):
                return response()->json(['errors' => ['user_not_found' => 'User not found']], 200);
            endif;

            if( $input['points'] < 1 ):
                return response()->json(['errors' => ['points' => 'Points should be 1 or more']], 200);
            endif;

        endif;

    }

    public function sendMessage(SendMessageAPIRequest $request)
    {
        $input = $request->all();

        $order = $this->orderRepository->find($input['order_id']);

        if( $order && ( $order->user_id == Auth::id() || $order->shop_id == Auth::user()->shop->id) ):

            $messageFrom = Auth::id() === $order->shop->user->id ? 1 : 0;

            $msg = $this->orderChatRepository->create([
                'order_id' => $order->id,
                'from' => $messageFrom,
                'message' => $input['message']
            ]);

            if( $msg ):

                $order->shop;

                if( $order->user_id == Auth::id() ):

                    if( env('APP_ENV') === 'production' ):
                        /** Send notification to company user */
                        $shop = $this->shopRepository->find($order->shop_id);
                        $shopUser = $this->userRepository->find($shop->user->id);
                        $shopUser->notify( new MessageReceived(Auth::user(), $order, $input['message'], $messageFrom ) );
                    endif;

                endif;

                if( $order->shop->user->id == Auth::id() ):

                    if( env('APP_ENV') === 'production' ):
                        /**Send notification to private user */
                        $user = $this->userRepository->find($order->user->id);
                        $user->notify( new MessageReceived(Auth::user(), $order, $input['message'], $messageFrom ) );
                    endif;

                endif;

                return response()->json(['success' => ['OK' => 'Message sent successfully']], 200);

            endif;

        else:

            if( empty($order) ):
                return response()->json(['errors' => ['order_not_found' => 'Order not found']], 200);
            else:
                return response()->json(['errors' => ['action_not_allowed' => 'You are not allowed to send a message for this order']], 200);
            endif;

        endif;
    }

    public function updatePhone(Request $request){
        $input = $request->all();
        $user = $this->userRepository->find($input['user_id']);
        $user->phone = $input['phone'];
        $otp = mt_rand(1000,10000);
        $user->otp = $otp;
        $user->save();

        if(!empty($user->phone)){
            /** Invia SMS */
            $sms = new smsCls();
            $sms->inviaSms('Codice di attivazione Primascelta.biz: ' . $otp, [ $user->phone] );
            return response()->json(['success' => ['OK' => 'Abbiamo inviato un sms al numero da te indicato']], 200);
        }else{
            return response()->json(['errors' => ['phone_empty' => 'Il numero di telefono non può essere vuoto']], 200);
        }
    }
}
