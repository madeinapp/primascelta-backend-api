<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateUserShopAPIRequest;
use App\Http\Requests\API\UpdateUserShopAPIRequest;
use App\Models\User;
use App\Models\Shop;
use App\Repositories\UserRepository;
use App\Exceptions\Handler;
use App\Repositories\ShopRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Models\UserSubscription;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Exception;
use Response;
use Log;

/**
 * Class UserController
 * @package App\Http\Controllers\API
 */

class UserShopAPIController extends AppBaseController
{
    /** @var  UserRepository */
    private $userRepository;

    /** @string */
    private $user_password = '_JnR390XB';

    public function __construct(UserRepository $userRepo)
    {
        $this->userRepository = $userRepo;
    }

    /**
     * Display a listing of the User.
     * GET|HEAD /users
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $users = $this->userRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($users->toArray(), 'Users retrieved successfully');
    }

    /**
     * Store a newly created User in storage.
     * POST /users
     *
     * @param CreateUserAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateUserShopAPIRequest $request)
    {
        $input = $request->all();

        $user_input['api_token'] = Str::random(80);
        $user_input['password'] = Hash::make($this->user_password);
        $user_input['user_type'] = 'company';
        $user_input['status'] = 'active';

        $user_input['name'] = $input['nome'];
        $user_input['phone'] = $input['tel'];
        $user_input['email'] = $input['email'];


        $user = User::Create($user_input);

        $user_subscription = [
            'user_id' => $user->id,
            'subscription' => 'free',
            'submission_date' => Carbon::now(),
            'expire_date' => Carbon::now()->addYears(10)
        ];

        try{
            UserSubscription::create($user_subscription);
        }catch(Throwable $e){
            $user->destroy();
            report($e);
            return false;
        }

        //Shop create
        $shop_input['user_id'] = $user->id;
        $shop_input['name'] = $input['negozio'];
        $shop_input['shop_type_id'] = $input['description'];
        $shop_input['address'] = $input['citta'];
        $shop_input['phone'] = $input['tel'];
        $shop_input['tax_business_name'] = $input['negozio'];
        /*
        $shop_input['tax_vat'] = Str::random(11);
        $shop_input['tax_fiscal_code'] = Str::random(16);
        */
        $shop_input['country'] = $input['country'];
        $shop_input['route'] = $input['route'];
        $shop_input['city'] = $input['locality'];
        $shop_input['region'] = $input['administrative_area_level_1'];
        $shop_input['lat'] = $input['lat'];
        $shop_input['long'] = $input['long'];
        $shop_input['street_number'] = $input['street_number'];
        $shop_input['delivery_range_km'] = $input['delivery'];
        $shop_input['tax_type_id'] = 1;
        try {
            $shop = Shop::create($shop_input);
        } catch (Throwable $e) {
            $user->destroy();
            report($e);
            return false;
        }
        return $this->sendResponse($user->toArray(), 'User saved successfully');
    }

    /**
     * Display the specified User.
     * GET|HEAD /users/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var User $user */
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            return $this->sendError('User not found');
        }

        return $this->sendResponse($user->toArray(), 'User retrieved successfully');
    }

    /**
     * Update the specified User in storage.
     * PUT/PATCH /users/{id}
     *
     * @param int $id
     * @param UpdateUserAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUserAPIRequest $request)
    {
        $input = $request->all();

        /** @var User $user */
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            return $this->sendError('User not found');
        }

        $user = $this->userRepository->update($input, $id);

        return $this->sendResponse($user->toArray(), 'Utente modificato con successo');
    }

    /**
     * Remove the specified User from storage.
     * DELETE /users/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var User $user */
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            return $this->sendError('User not found');
        }

        $user->delete();

        return $this->sendSuccess('User deleted successfully');
    }
}
