<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

class ArtigianoController extends Controller
{
    public function show(Request $request, $command, $param) {

        $input = $request->all();

        $params = '';

        if( !empty($input) ):
            $params = ' ' . implode(' ', $input);
        endif;

        $artisan = Artisan::call($command.":".$param.$params);
        $output = Artisan::output();
        return $output;
        /*
        $artisan = Artisan::call($command,['flag'=>$param]);
        $output = Artisan::output();
        return $output;
        */
    }

    public function showSingle($command) {
        $artisan = Artisan::call($command);
        $output = Artisan::output();
        return $output;
        /*
        $artisan = Artisan::call($command,['flag'=>$param]);
        $output = Artisan::output();
        return $output;
        */
    }
}
