<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    public function login(LoginRequest $request)
    {
        // => "company"
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            $user = Auth::user();

            if ( in_array($user->status, ['active', 'pending', 'invisible']) && ($user->user_type == "company" || $user->role == "admin")){
                if( !empty($request->cta) ):
                    $action = explode(".", $request->cta);
                    if( $action[0] === "order" ):
                        return redirect()->route('orders.edit', ['order' => $action[1]]);
                    endif;
                else:
                    return redirect()->intended(route('home'));
                endif;
            }else{
                Auth::logout();
                if ( $user->user_type == "private" ){
                    return redirect()->route('login')->withErrors(['password' => 'Le credenziali sono associate ad un utente privato: l\'accesso è consentito unicamente con l\'App Primascelta.']);
                }else{
                    return redirect()->route('login')->withErrors(['email' => 'Utente non attivo']);
                }
            }
        }else{
            return redirect()->route('login')->withErrors(['email' => 'Nome utente o password errate']);
        }
    }
}
