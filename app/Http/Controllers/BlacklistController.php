<?php

namespace App\Http\Controllers;

use App\Repositories\BlacklistRepository;
use Illuminate\Http\Request;
use Auth;
use DB;
use DataTables;

class BlacklistController extends Controller
{
    private $blacklistRepository;

    public function __construct(BlacklistRepository $blacklistRepository)
    {
        $this->blacklistRepository = $blacklistRepository;
    }

    public function index(Request $request){

        $blacklist = DB::table('blacklist_shops as bs')
                                ->join('users as u', 'bs.user_id', '=', 'u.id')
                                ->join('shops as s', 'bs.shop_id', '=', 's.id')
                                ;

        if( Auth::user()->can('is_company') ){
            $blacklist = $blacklist->where('shop_id', Auth::user()->shop->id)
                                    ->orderBy('u.name');

            if( $request->has('user_filter') ){
                $blacklist = $blacklist->where('u.name', 'like', '%'.$request->user_filter.'%');
            }

            if ($request->ajax()) {

                $blacklist = $blacklist->select('s.id as shop_id', 'u.id as user_id', 'u.name as user_name', 'u.trust_points as trust_points', 'u.email');

                $datatables = Datatables::of($blacklist)

                    ->editColumn('action', function($row){
                        return '<button class="btn btn-success" onclick="remove(' . $row->shop_id . ', ' . $row->user_id . ');">Rimuovi utente da Blacklist</button>';
                    })

                    ->editColumn('user_name', function($row){

                        return $row->user_name . '<br/>' . $row->trust_points . ' punti fiducia' . '<br/>' . $row->email;

                    })

                    ->rawColumns(['action', 'user_name']);

                return $datatables->make(true);

            }else{

                $totalData = $blacklist->count();

            }

            return view('blacklist.company.index')
                    ->with("tot_blacklist",empty($totalData) ? 0 : $totalData)
                ;


        }elseif(Auth::user()->can('is_admin') ) {

            if( $request->has('user_filter') ){
                $blacklist = $blacklist->where('u.name', 'like', '%'.$request->user_filter.'%');
            }
            if( $request->has('shop_filter') ){
                $blacklist = $blacklist->where('s.name', 'like', '%'.$request->shop_filter.'%');
            }

            $blacklist = $blacklist->orderBy('s.name')
                                   ->orderBy('u.name');

            if ($request->ajax()) {

                $blacklist = $blacklist->select('s.id as shop_id', 'u.id as user_id', 's.name as shop_name', 's.route', 's.street_number', 's.city', 's.prov', 's.region', 's.postal_code', 'u.name as user_name', 'u.trust_points as trust_points', 'u.email');

                $datatables = Datatables::of($blacklist)

                    ->editColumn('action', function($row){
                        return '<button class="btn btn-success" onclick="remove(' . $row->shop_id . ', ' . $row->user_id . ');">Rimuovi utente da Blacklist</button>';
                    })

                    ->editColumn('shop_name', function($row){
                        return $row->shop_name . '<br />' . $row->route . ' ' . $row->street_number . '<br />' . $row->prov . ' ' . $row->postal_code . '<br/>' . $row->region;
                    })

                    ->editColumn('user_name', function($row){
                        return $row->user_name . '<br/>' . $row->trust_points . ' punti fiducia' . '<br/>' . $row->email;
                    })

                    ->rawColumns(['action', 'shop_name', 'user_name']);

                return $datatables->make(true);

            }else{

                $totalData = $blacklist->count();

            }

            return view('blacklist.admin.index')
                    ->with("tot_blacklist",empty($totalData) ? 0 : $totalData)
                ;

        }

    }

    public function remove($shop_id, $user_id) {
        $user_blacklist = $this->blacklistRepository->all(['shop_id' => $shop_id, 'user_id' => $user_id]);
        foreach($user_blacklist as $ub):
            $ub->delete();
            return response()->json(['success' => 'Utente rimosso dalla blacklist']);
        endforeach;
    }
}
