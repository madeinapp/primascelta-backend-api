<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateDeliveryMethodRequest;
use App\Http\Requests\UpdateDeliveryMethodRequest;
use App\Repositories\DeliveryMethodRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class DeliveryMethodController extends AppBaseController
{
    /** @var  DeliveryMethodRepository */
    private $deliveryMethodRepository;

    public function __construct(DeliveryMethodRepository $deliveryMethodRepo)
    {
        $this->deliveryMethodRepository = $deliveryMethodRepo;
    }

    /**
     * Display a listing of the DeliveryMethod.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $deliveryMethods = $this->deliveryMethodRepository->all();

        return view('delivery_methods.index')
            ->with('deliveryMethods', $deliveryMethods);
    }

    /**
     * Show the form for creating a new DeliveryMethod.
     *
     * @return Response
     */
    public function create()
    {
        return view('delivery_methods.create');
    }

    /**
     * Store a newly created DeliveryMethod in storage.
     *
     * @param CreateDeliveryMethodRequest $request
     *
     * @return Response
     */
    public function store(CreateDeliveryMethodRequest $request)
    {
        $input = $request->all();

        $deliveryMethod = $this->deliveryMethodRepository->create($input);

        Flash::success('Modalità di consegna creata con successo.');

        return redirect(route('deliveryMethods.index'));
    }

    /**
     * Display the specified DeliveryMethod.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $deliveryMethod = $this->deliveryMethodRepository->find($id);

        if (empty($deliveryMethod)) {
            Flash::error('Metodo di pagamento non trovato');

            return redirect(route('deliveryMethods.index'));
        }

        return view('delivery_methods.show')->with('deliveryMethod', $deliveryMethod);
    }

    /**
     * Show the form for editing the specified DeliveryMethod.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $deliveryMethod = $this->deliveryMethodRepository->find($id);

        if (empty($deliveryMethod)) {
            Flash::error('Metodo di pagamento non trovato');

            return redirect(route('deliveryMethods.index'));
        }

        return view('delivery_methods.edit')->with('deliveryMethod', $deliveryMethod);
    }

    /**
     * Update the specified DeliveryMethod in storage.
     *
     * @param int $id
     * @param UpdateDeliveryMethodRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDeliveryMethodRequest $request)
    {
        $deliveryMethod = $this->deliveryMethodRepository->find($id);

        if (empty($deliveryMethod)) {
            Flash::error('Metodo di pagamento non trovato');

            return redirect(route('deliveryMethods.index'));
        }

        $deliveryMethod = $this->deliveryMethodRepository->update($request->all(), $id);

        Flash::success('Metodo di spedizione modificato con successo.');

        return redirect(route('deliveryMethods.index'));
    }

    /**
     * Remove the specified DeliveryMethod from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $deliveryMethod = $this->deliveryMethodRepository->find($id);

        if (empty($deliveryMethod)) {
            Flash::error('Metodo di pagamento non trovato');

            return redirect(route('deliveryMethods.index'));
        }

        $this->deliveryMethodRepository->delete($id);

        Flash::success('Modalità di consegna eliminata con successo.');

        return redirect(route('deliveryMethods.index'));
    }
}
