<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateFoodCategoryRequest;
use App\Http\Requests\UpdateFoodCategoryRequest;
use App\Repositories\FoodCategoryRepository;
use App\Http\Controllers\AppBaseController;
use App\Repositories\FoodRepository;
use App\Repositories\ShopCatalogRepository;
use App\Repositories\UnitOfMeasureRepository;
use Illuminate\Http\Request;
use Flash;
use Response;
use File;
use Image;
use Storage;

class FoodCategoryController extends AppBaseController
{
    /** @var  FoodCategoryRepository */
    private $foodCategoryRepository;
    private $shopCatalogRepository;
    private $unitOfMeasureRepository;
    private $foodRepository;

    private $logo = "_logo.png";
    private $background = "_background.png";

    public function __construct(FoodCategoryRepository $foodCategoryRepo, ShopCatalogRepository $shopCatalogRepository, UnitOfMeasureRepository $unitOfMeasureRepository, FoodRepository $foodRepository)
    {
        $this->foodCategoryRepository = $foodCategoryRepo;
        $this->shopCatalogRepository = $shopCatalogRepository;
        $this->unitOfMeasureRepository = $unitOfMeasureRepository;
        $this->foodRepository = $foodRepository;
    }

    /**
     * Display a listing of the FoodCategory.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $foodCategories = $this->foodCategoryRepository->all();

        return view('food_categories.index')
            ->with('foodCategories', $foodCategories);
    }

    /**
     * Show the form for creating a new FoodCategory.
     *
     * @return Response
     */
    public function create()
    {
        $unitOfMeasures = $this->unitOfMeasureRepository->all([], null, null, ['*'], 'name')->pluck('name', 'id');
        return view('food_categories.create')->with(['logo_exists' => false,
                                                     'background_exists' => false,
                                                     'unitOfMeasures' => $unitOfMeasures
                                                     ]);
    }

    /**
     * Store a newly created FoodCategory in storage.
     *
     * @param CreateFoodCategoryRequest $request
     *
     * @return Response
     */
    public function store(CreateFoodCategoryRequest $request)
    {
        $input = $request->all();

        $foodCategory = $this->foodCategoryRepository->create($input);

        if( $request->hasFile('filelogo') ):
            $path = public_path('storage/images/foods/'.NormalizeString($foodCategory->name));
            if( !File::isDirectory($path) ) {
                File::makeDirectory($path, 0775, true); //creates directory
            }

            $path = $request->file('filelogo')->storeAs(
                'public/images/foods/'.NormalizeString($foodCategory->name), $this->logo
            );

            if( $path ):
                $this->foodCategoryRepository->update(['logo' => $this->logo], $foodCategory->id);
            endif;

        endif;

        if( $request->hasFile('filebackground') ):

            /** File type validation */
            $filename = $request->filebackground->getClientOriginalName();
            $extension = pathinfo($filename, PATHINFO_EXTENSION );

            if( $extension !== 'png' ):

                Flash::error('File type not accepted');
                return redirect(route('foodCategories.edit', ['foodCategory' => $foodCategory->id]));

            endif;

            $image = Image::make($request->filebackground->getRealPath())->orientate();
            $path = public_path('storage/images/foods/'.NormalizeString($foodCategory->name).'/'.$this->background);

            if( !File::isDirectory(dirname($path)) ) {
                File::makeDirectory(dirname($path), 0775, true); //creates directory
            }

            if( $image->save($path) ):
                $this->foodCategoryRepository->update(['background' => $this->background], $foodCategory->id);
            endif;

        endif;

        Flash::success('Categoria creata con successo.');

        return redirect(route('foodCategories.index'));
    }

    /**
     * Display the specified FoodCategory.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $foodCategory = $this->foodCategoryRepository->find($id);

        if (empty($foodCategory)) {
            Flash::error('Impossibile trovare la categoria');

            return redirect(route('foodCategories.index'));
        }

        return view('food_categories.show')->with('foodCategory', $foodCategory);
    }

    /**
     * Show the form for editing the specified FoodCategory.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $foodCategory = $this->foodCategoryRepository->find($id);

        if (empty($foodCategory)) {
            Flash::error('Impossibile trovare la categoria');

            return redirect(route('foodCategories.index'));
        }

        $logo = public_path('storage/images/foods/'.NormalizeString($foodCategory->name).'/'.$this->logo);
        $logo_exists = File::exists($logo);

        $background = public_path('storage/images/foods/'.NormalizeString($foodCategory->name).'/'.$this->logo);
        $background_exists = File::exists($background);

        $unitOfMeasures = $this->unitOfMeasureRepository->all([], null, null, ['*'], 'name')->pluck('name', 'id');

        return view('food_categories.edit')->with('foodCategory', $foodCategory)
                                           ->with('logo', $this->logo)
                                           ->with('background', $this->background)
                                           ->with('logo_exists', $logo_exists)
                                           ->with('background_exists', $background_exists)
                                           ->with('unitOfMeasures' , $unitOfMeasures)
                                           ;
    }

    /**
     * Update the specified FoodCategory in storage.
     *
     * @param int $id
     * @param UpdateFoodCategoryRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateFoodCategoryRequest $request)
    {
        $input = $request->all();

        $foodCategory = $this->foodCategoryRepository->find($id);

        if (empty($foodCategory)) {
            Flash::error('Impossibile trovare la categoria');

            return redirect(route('foodCategories.index'));
        }

        if( $foodCategory->name != $input['name'] ):

            rename(public_path('storage/images/foods/'.NormalizeString($foodCategory->name)), public_path('storage/images/foods/'.NormalizeString($input['name'])));

        endif;

        $foodCategory = $this->foodCategoryRepository->update($input, $id);


        if( $request->hasFile('filelogo') ):
            $path = public_path('storage/images/foods/'.NormalizeString($foodCategory->name));
            if( !File::isDirectory($path) ) {
                File::makeDirectory($path, 0775, true); //creates directory
            }

            $path = $request->file('filelogo')->storeAs(
                'public/images/foods/'.NormalizeString($foodCategory->name), $this->logo
            );

            if( $path ):
                $this->foodCategoryRepository->update(['logo' => url('storage/images/foods/'.NormalizeString($foodCategory->name).'/'.$this->logo)], $foodCategory->id);
            endif;

        else:

            $this->foodCategoryRepository->update(['logo' => url('storage/images/foods/'.NormalizeString($foodCategory->name).'/'.$this->logo)], $foodCategory->id);

        endif;

        if( $request->hasFile('filebackground') ):

            /** File type validation */
            $filename = $request->filebackground->getClientOriginalName();
            $extension = pathinfo($filename, PATHINFO_EXTENSION );

            if( $extension !== 'png' ):

                Flash::error('File type not accepted');
                return redirect(route('foodCategories.edit', ['foodCategory' => $foodCategory->id]));

            endif;

            $image = Image::make($request->filebackground->getRealPath())->orientate();
            $path = public_path('storage/images/foods/'.NormalizeString($foodCategory->name).'/'.$this->background);

            if( !File::isDirectory(dirname($path)) ) {

                File::makeDirectory(dirname($path), 0775, true); //creates directory

            }

            if( $image->save($path) ):
                $this->foodCategoryRepository->update(['background' => url('storage/images/foods/'.NormalizeString($foodCategory->name).'/'.$this->background)], $foodCategory->id);
            endif;

        else:

            $this->foodCategoryRepository->update(['background' => url('storage/images/foods/'.NormalizeString($foodCategory->name).'/'.$this->background)], $foodCategory->id);

        endif;

        /** Aggiorno la url di tutti i prodotti a catalogo collegati ad un alimento di questa categoria */
        $shopCatalogs = $this->shopCatalogRepository->havingFoodByFoodCategory($foodCategory->id);

        foreach($shopCatalogs as $shopCatalog):
            $filename = basename($shopCatalog->food_image);
            $shopCatalog->food_image = url('/storage/images/foods/'.NormalizeString($input['name'])) . '/' . $filename;
            $shopCatalog->save();
        endforeach;

        Flash::success('Categoria modificata con successo.');

        return redirect(route('foodCategories.index'));
    }

    /**
     * Remove the specified FoodCategory from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** Prima di eliminare una categoria verifico se almeno un prodotto di questa categoria
         * è presente in un catalogo di un negozio
        */

        $foodCategory = $this->foodCategoryRepository->find($id);

        if (empty($foodCategory)) {
            Flash::error('Impossibile trovare la categoria');
            return redirect(route('foodCategories.index'));
        }

        if( $foodCategory->foods->count() > 0 ):
            Flash::error('Non puoi eliminare questa categoria. Contiene prodotti al suo interno');
            return redirect(route('foods.index'));
        endif;

        /*
        foreach($foodCategory->foods as $food):
            $count = $this->shopCatalogRepository->allQuery(['food_id' => $food->id])->get()->count();
            if ( $count > 0 ) {
                Flash::error('Non puoi eliminare questa categoria. Contiene prodotti presenti in almeno un negozio');
                return redirect(route('foods.index'));
            }
        endforeach;
        */

        $this->foodCategoryRepository->delete($id);

        $path = public_path('storage/images/foods/'.NormalizeString($foodCategory->name).'/');

        File::deleteDirectory($path);

        Flash::success('Categoria eliminata con successo.');

        return redirect(route('foodCategories.index'));
    }


    public function selectForMove($food_category_id){
        $foodCategory = $this->foodCategoryRepository->find($food_category_id);
        $foodCategories = $this->foodCategoryRepository->all()->pluck('name', 'id')->toArray();
        foreach($foodCategories as $k=>$fc):
            if($k == $food_category_id):
                unset($foodCategories[$k]);
                break;
            endif;
        endforeach;

        return view('food_categories.move')->with('foodCategory', $foodCategory)
                                           ->with('foodCategories', $foodCategories);
    }


    public function moveFoods($food_category_id, Request $request){

        $input = $request->all();
        $foods = $this->foodRepository->all(['food_category_id' => $food_category_id]);

        foreach($foods as $food):
            app('App\Http\Controllers\FoodController')->moveImages($food, $input['new_food_category_id']);
            $food->food_category_id = $input['new_food_category_id'];
            $food->save();
        endforeach;


        $foodCategory = $this->foodCategoryRepository->find($input['new_food_category_id']);

        /** Aggiorno la url di tutti i prodotti a catalogo collegati ad un alimento di questa categoria */
        $shopCatalogs = $this->shopCatalogRepository->havingFoodByFoodCategory($input['new_food_category_id']);

        foreach($shopCatalogs as $shopCatalog):
            $filename = basename($shopCatalog->food_image);
            $shopCatalog->food_image = url('/storage/images/foods/'.NormalizeString($foodCategory->name) . '/' . $filename);
            $shopCatalog->save();
        endforeach;

        return redirect(route('foodCategories.index'));
    }

    public function moveShopCatalogs($food_category_id, Request $request){

        $input = $request->all();
        $shopCatalogs = $this->shopCatalogRepository->all(['food_category_id' => $food_category_id]);

        foreach($shopCatalogs as $shopCatalog):
            $shopCatalog->food_category_id = $input['new_food_category_id'];
            $shopCatalog->save();
        endforeach;

        return redirect(route('foodCategories.index'));
    }
}
