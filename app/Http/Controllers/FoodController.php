<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateFoodRequest;
use App\Http\Requests\UpdateFoodRequest;
use App\Repositories\FoodRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Flash;
use Response;
use Image;
use App\Models\FoodCategory;
use App\Models\UnitOfMeasure;
use App\Repositories\FoodCategoryRepository;
use App\Repositories\ShopCatalogRepository;
use App\Repositories\SpecialRepository;
use App\Repositories\UnitOfMeasureRepository;
use Illuminate\Support\Facades\Validator;
use DB;
use Form;
use DataTables;
use Log;

class FoodController extends AppBaseController
{
    /** @var  FoodRepository */
    private $foodRepository,
            $foodCategoryRepository,
            $shopCatalogRepository,
            $specialRepository,
            $unitOfMeasureRepository;

    public function __construct(FoodRepository $foodRepo,
                                FoodCategoryRepository $foodCategoryRepo,
                                ShopCatalogRepository $shopCatalogRepository,
                                SpecialRepository $specialRepository,
                                UnitOfMeasureRepository $unitOfMeasureRepository)
    {
        $this->foodRepository = $foodRepo;
        $this->foodCategoryRepository = $foodCategoryRepo;
        $this->shopCatalogRepository = $shopCatalogRepository;
        $this->specialRepository = $specialRepository;
        $this->unitOfMeasureRepository = $unitOfMeasureRepository;
    }

    /**
     * Display a listing of the Food.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {

        $query_foods = DB::table('foods as f')
            ->join('food_categories as fc', 'f.food_category_id', '=', 'fc.id')
            ->join('unit_of_measures as u', 'f.unit_of_measure_id', '=', 'u.id');

        if ($request->ajax()) {
             // additional users.name search
            if ($searchname = $request->get('searchname')) {
                $query_foods->where(function ($query) use ($searchname) {
                    $query->where('f.name', 'like', "%$searchname%");
                    $query->orWhere('f.description', 'like', "%$searchname%");
                });
            }
            if ($genere = $request->get('genere')) {
                $query_foods->where('fc.id', '=', "$genere");
            }
            if ($tipologia = $request->get('tipologia')) {
                $query_foods->where('f.type', '=', "$tipologia");
            }
            $query_foods = $query_foods->select(['f.id','fc.name as foodCategory','f.name', 'f.description','f.type as foodType','u.name as UnitOfMeasure', "f.image","f.available"]);

            //dd($query_foods->toSql());

            $totalData = $query_foods->count();

            $datatables = Datatables::of($query_foods)

                ->editColumn('category', function($row){
                    return '<span>'.$row->foodCategory.'</span><br /><span><small>'.$row->foodType.'</small></span>';
                })
                ->editColumn('description', function($row){
                    return "<div data-toggle=\"tooltip\" title=\"".$row->description."\">".mb_substr($row->description, 0, 50, "utf-8")." ...</div>";
                })
                ->editColumn('image', function($row){
                    if (empty($row->image))
                        $image = "<img width='100'  src='/images/food.jpg'>";
                    else
                        $image = "<img width='100'  src='".getResizedImage(asset('/storage/images/foods/').'/'.NormalizeString($row->foodCategory).'/'.$row->image,"100x100")."'>";
                    return $image;
                })
                ->editColumn('available', function($row){ return ($row->available) ? "SI" : "NO"; })
                ->addColumn('action', function($row){
                        $btn = Form::open(['route' => ['foods.destroy', $row->id], 'method' => 'delete']);

                        $btn .= '<a href="'.route('foods.edit', [$row->id]).'" class="btn btn-default btn-sm"><i class="fas fa-edit"></i></a>';
                        $btn .= Form::button('<i class="fas fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-sm', 'onclick' => "return confirm('".__('common.Are you sure?')."')"]);

                        $btn .= Form::close();
                        return $btn;
                })
                ->rawColumns(['category', 'description','image','action']);


            return $datatables->make(true);

        }else{

            $totalData = $query_foods->count();

        }


        $obj_food_categories = FoodCategory::select('id','name')->orderBy('name')->get();
        $arr_food_categories = [];
        $arr_food_categories[""] = "Cerca in tutti i generi";
        foreach($obj_food_categories as $fc){
                $arr_food_categories[$fc->id] = $fc->name;
        }

        $arr_food_type = [];
        $arr_food_type[""] = "Cerca in tutte le tipologie";

        /*
        $obj_food_type = DB::table('foods')->select('type')->distinct()->orderBy('type','asc')->get();
        foreach($obj_food_type as $v){
                $arr_food_type[$v->type] = $v->type;
        }
        */

        return view('foods.index')
                ->with("generi",$arr_food_categories)
                ->with("tipologie",$arr_food_type)
                ->with("tot_generi",count($arr_food_categories)-1)
                ->with("tot_tipologie",count($arr_food_type)-1)
                ->with("tot_alimenti",empty($totalData) ? 0 : $totalData)
        ;
    }

    /**
     * Show the form for creating a new Food.
     *
     * @return Response
     */
    public function create()
    {
        $arr_unit_of_measures = $this->unitOfMeasureRepository->all([], null, null, ['*'], 'name')->pluck('name', 'id')->toArray();
        $arr_unit_of_measures = ['' => 'Seleziona...'] + $arr_unit_of_measures;

        $arr_food_categories = $this->foodCategoryRepository->all([], null, null, ['*'], 'name')->pluck('name', 'id');

        $obj_food_type = DB::table('foods')->select('type')->distinct()->orderBy('type','asc')->get();
        $arr_food_type = [];
        foreach($obj_food_type as $v){
            $arr_food_type[$v->type] = $v->type;
        }

        $defaultUom = FoodCategory::select('id','unit_of_measure')->get()->pluck('unit_of_measure', 'id')->toArray();

        $specials = $this->specialRepository->allSpecials();

        return view('foods.create')->with(['arr_unit_of_measures' => $arr_unit_of_measures,
                                            'arr_food_categories' => $arr_food_categories,
                                            'arr_food_type' => $arr_food_type,
                                            'image_type' => 'image/jpeg, image/png',
                                            'defaultUom' => json_encode($defaultUom),
                                            'specials' => $specials
                                            ]);
    }

    /**
     * Store a newly created Food in storage.
     *
     * @param CreateFoodRequest $request
     *
     * @return Response
     */
    public function store(CreateFoodRequest $request)
    {
        $input = $request->all();

        $image = null;

        /*
        if( $request->hasFile('fileimage') ){
            $image_rule = [
                'fileimage' => 'image|mimes:jpg,jpeg|max:10240|dimensions:min_width=646,min_height=430',
            ];

            $image_messages = [
                'fileimage.mimes' => 'L\'immagine deve essere di tipo .jpg',
                'fileimage.max' => 'ATTENZIONE: questo file presenta dei problemi. Le immagini caricate devono rispettare precise caratteristiche. Grazie',
                'fileimage.size' => 'ATTENZIONE: questo file presenta dei problemi. Le immagini caricate devono rispettare precise caratteristiche. Grazie',
                'fileimage.dimensions' => 'ATTENZIONE: l\'immagine che stai caricando presenta dimensioni troppo piccole: ti consigliamo di sceglierne un\'altra'
            ];

            $image_validator = Validator::make(['fileimage' => $input['fileimage']], $image_rule, $image_messages);
            if($image_validator->fails()){
                return back()->withErrors($image_validator);
            }

            /** File type validation *
            $image = Image::make($request->file('fileimage')->getRealPath())->orientate();
            $input['image'] = NormalizeString($input['name']).'.jpg';
        }
        */

        $food = $this->foodRepository->create($input);

        $imageFullPath = null;
        if( !empty($input['output_image_data']) && !empty($input['output_image_type']) ):
            $folderPath = public_path('storage/images/foods/'.NormalizeString($food->foodCategory->name));

            if( !is_dir(public_path('storage/images/foods/'.NormalizeString($food->foodCategory->name))) ):
                mkdir( public_path('storage/images/foods/'.NormalizeString($food->foodCategory->name)) );
            endif;

            if( !File::isDirectory($folderPath) ) {
                File::makeDirectory($folderPath, 0775, true); //creates directory
            }

            $image = $input['output_image_data'];
            $image_parts = explode(";base64,", $image);
            $image_type_aux = explode("image/", $image_parts[0]);
            $image_base64 = base64_decode($image_parts[1]);

            $image_info = explode("/", $input['output_image_type']);
            $extension = $image_info[1];

            $filename = $food->name;
            $filename = NormalizeString($food->name).'.'.$extension;

            $imageFullPath = $folderPath.'/'.$filename;
            file_put_contents($imageFullPath, $image_base64);

            //Generate cropped thumbs image
            $image = Image::make($imageFullPath)->orientate();
            cropImages($image,$imageFullPath, 'food');
            unset($image);
            unlink($imageFullPath);
            //dd(asset('/storage/images/foods/'.NormalizeString($food->foodCategory->name).'/'.$filename));
            $food->image = $filename;
            $food->save();
        endif;

        if( !empty($input['food_specials']) ){
            $food->specials()->sync($input['food_specials']);
        }

        /*
        if( $image ){

            /*
            if( !is_dir(public_path('storage/images/foods/'.NormalizeString($food->foodCategory->name))) ):
                mkdir( public_path('storage/images/foods/'.NormalizeString($food->foodCategory->name)) );
            endif;

            /** Create and store image and image crops *
            $path = public_path('storage/images/foods/'.NormalizeString($food->foodCategory->name).'/'.NormalizeString($food->image).'.jpg');
            if( $image->save($path) ):
                cropImages($image, $path, 'food');
            else:
                Flash::error('Error saving image');
                return redirect(route('foods.edit', ['id' => $food->id]));
            endif;

        }
        */

        Flash::success('Alimento creato con successo.');

        return redirect(route('foods.index'));
    }

    /**
     * Display the specified Food.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $food = $this->foodRepository->find($id);

        if (empty($food)) {
            Flash::error('Impossibile trovare il prodotto');

            return redirect(route('foods.index'));
        }

        return view('foods.show')->with('food', $food);
    }

    /**
     * Show the form for editing the specified Food.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $food = $this->foodRepository->find($id);

        $arr_unit_of_measures = $this->unitOfMeasureRepository->all([], null, null, ['*'], 'name')->pluck('name', 'id')->toArray();
        $arr_unit_of_measures = ['' => 'Seleziona...'] + $arr_unit_of_measures;

        $arr_food_categories = $this->foodCategoryRepository->all([], null, null, ['*'], 'name')->pluck('name', 'id');

        $obj_food_type = DB::table('foods')->select('type')->distinct()->orderBy('type','asc')->get();
        $arr_food_type = [];
        foreach($obj_food_type as $v){
            $arr_food_type[$v->type] = $v->type;
        }
        if (empty($food)) {
            Flash::error('Impossibile trovare il prodotto');

            return redirect(route('foods.index'));
        }

        $image_type = 'image/jpeg, image/png';
        if( !empty($food->image) ){
            $extension = pathinfo($food->image, PATHINFO_EXTENSION );
            switch( $extension ){
                case 'jpeg':
                case 'jpg': $image_type = 'image/jpeg'; break;

                case 'png': $image_type = 'image/png'; break;
            }
        }

        $defaultUom = FoodCategory::select('id','unit_of_measure')->get()->pluck('unit_of_measure', 'id')->toArray();

        $specials = $this->specialRepository->allSpecials();

        return view('foods.edit')->with(['food' => $food,
                                         'arr_unit_of_measures' => $arr_unit_of_measures,
                                         'arr_food_categories' => $arr_food_categories,
                                         'arr_food_type' => $arr_food_type,
                                         'image_type' => $image_type,
                                         'defaultUom' => json_encode($defaultUom),
                                         'specials' => $specials
                                         ]);
    }

    /**
     * Update the specified Food in storage.
     *
     * @param int $id
     * @param UpdateFoodRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateFoodRequest $request)
    {

        $input = $request->all();

        $food = $this->foodRepository->find($id);

        if (empty($food)) {
            Flash::error('Impossibile trovare il prodotto');
            return redirect(route('foods.index'));
        }

        $image = null;

        $update_catalogs = false;

        //if( !$request->hasFile('fileimage')){
        if( empty($input['output_image_data']) ){
            /** If i did not uploaded a new image
             *  but I had already an image
             *  and I changed category
             *  I have to move images in new category folder
             */
            if( !empty($food->image) && $food->food_category_id != $request['food_category_id'] ){
                $this->moveImages( $food, $request['food_category_id'] );
                $update_catalogs = true;
            }
        }

        $food = $this->foodRepository->update($request->all(), $id);

        if( !empty($input['food_specials']) ){
            $food->specials()->sync($input['food_specials']);
        }

        $imageFullPath = null;
        if( !empty($input['output_image_data']) && !empty($input['output_image_type']) ):
            $folderPath = public_path('storage/images/foods/'.NormalizeString($food->foodCategory->name));

            if( !File::isDirectory($folderPath) ) {
                File::makeDirectory($folderPath, 0775, true); //creates directory
            }

            $image = $input['output_image_data'];
            $image_parts = explode(";base64,", $image);
            $image_type_aux = explode("image/", $image_parts[0]);
            $image_base64 = base64_decode($image_parts[1]);

            $image_info = explode("/", $input['output_image_type']);
            $extension = $image_info[1];

            $filename = $food->name;
            $filename = NormalizeString($food->name).'.'.$extension;

            $imageFullPath = $folderPath.'/'.$filename;
            file_put_contents($imageFullPath, $image_base64);

            //Generate cropped thumbs image
            $image = Image::make($imageFullPath)->orientate();
            cropImages($image,$imageFullPath, 'food');
            unset($image);
            unlink($imageFullPath);
            //dd(asset('/storage/images/foods/'.NormalizeString($food->foodCategory->name).'/'.$filename));
            $food->image = $filename;
            $food->save();
        endif;


        if($update_catalogs){
            /** Aggiorno tutte le url delle immagini dei prodotti a catalogo collegati a questo alimento */
            $shopCatalogs = $this->shopCatalogRepository->havingFoodByFoodCategory($food->food_category_id);
            foreach($shopCatalogs as $shopCatalog):
                $filename = basename($shopCatalog->food_image);
                $shopCatalog->food_image = url('/storage/images/foods/'.NormalizeString($food->foodCategory->name) . '/' . $filename);
                $shopCatalog->save();
            endforeach;
        }


        Flash::success('Alimento modificato con successo.');
        return redirect(route('foods.index'));
    }

    /**
     * Remove the specified Food from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** Cerco prodotti collegati nei cataloghi
         *  Se li trovo non posso cancellare l'alimento
         */

        $count = $this->shopCatalogRepository->allQuery(['food_id' => $id])->get()->count();
        if ( $count > 0 ) {
            Flash::error('Non puoi eliminare questo alimento. E\' associato ad almeno un prodotto di un negozio');
            return redirect(route('foods.index'));
        }

        $food = $this->foodRepository->find($id);

        if (empty($food)) {
            Flash::error('Impossibile trovare il prodotto');

            return redirect(route('foods.index'));
        }

        $this->foodRepository->delete($id);

        $this->deleteOldImages($food);

        Flash::success('Alimento eliminato con successo.');

        return redirect(route('foods.index'));
    }



    private function deleteOldImages($food){

        $filename = $food->image;
        //$extension = pathinfo($food->image, PATHINFO_EXTENSION );

        $folder = storage_path('app/public').'/images/foods/'.NormalizeString($food->foodCategory->name);

        $filedel = getOriginalImage($filename);
        if( File::exists($filedel) ) File::delete($filedel);

        /*
        $filedel = $folder.'/'.$filename.'_750x306.'.$extension;
        if( File::exists($filedel) ) File::delete($filedel);

        $filedel = $folder.'/'.$filename.'_310x240.'.$extension;
        if( File::exists($filedel) ) File::delete($filedel);
        */

        $filedel = $folder.'/'.getResizedImage(getOriginalImage($filename), '646x430');
        if( File::exists($filedel) ) File::delete($filedel);

        $filedel = $folder.'/'.getResizedImage(getOriginalImage($filename), '512x340');
        if( File::exists($filedel) ) File::delete($filedel);

        $filedel = $folder.'/'.getResizedImage(getOriginalImage($filename), '230x154');
        if( File::exists($filedel) ) File::delete($filedel);

        $filedel = $folder.'/'.getResizedImage(getOriginalImage($filename), '100x100');
        if( File::exists($filedel) ) File::delete($filedel);

    }

    public function moveImages($food, $newCategory)
    {
        $filename = $food->image; //pathinfo(, PATHINFO_FILENAME);
        //$extension = 'jpg'; //pathinfo(NormalizeString($food->name), PATHINFO_EXTENSION );

        $old_folder = storage_path('app/public').'/images/foods/'.NormalizeString($food->foodCategory->name);

        $new_category = $this->foodCategoryRepository->find($newCategory);

        $new_folder = storage_path('app/public').'/images/foods/'.NormalizeString($new_category->name);

        if( !File::isDirectory($new_folder) ) {
            File::makeDirectory($new_folder, 0775, true); //creates directory
        }

        if( File::exists($old_folder.'/'.$food->image) ){
            File::move($old_folder.'/'.$food->image, $new_folder.'/'.$food->image);
        }

        $filemove = getResizedImage(getOriginalImage($filename), '646x430');

        /*
        dump($old_folder);
        dump($new_folder);
        dd($filemove);
        */

        if( File::exists($old_folder.'/'.$filemove) ) File::move($old_folder.'/'.$filemove, $new_folder.'/'.$filemove);

        $filemove = getResizedImage(getOriginalImage($filename), '512x340');
        if( File::exists($old_folder.'/'.$filemove) ) File::move($old_folder.'/'.$filemove, $new_folder.'/'.$filemove);

        $filemove = getResizedImage(getOriginalImage($filename), '230x154');
        if( File::exists($old_folder.'/'.$filemove) ) File::move($old_folder.'/'.$filemove, $new_folder.'/'.$filemove);

        $filemove = getResizedImage(getOriginalImage($filename), '100x100');
        if( File::exists($old_folder.'/'.$filemove) ) File::move($old_folder.'/'.$filemove, $new_folder.'/'.$filemove);

    }

    public function getFoodTypeList($food_category_id)
    {
        $foodTypeList = $this->foodRepository->getFoodTypeList($food_category_id);
        return response()->json(array('count'=>count($foodTypeList), 'Food Type List' => $foodTypeList));
    }


    public function checkImages($food_category_id){
        $foods = $this->foodRepository->byCategory($food_category_id);
        return view('food_categories.check_images')->with('foods', $foods);
    }
}
