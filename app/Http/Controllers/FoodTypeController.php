<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\FoodRepository;
use App\Repositories\ShopCatalogRepository;

class FoodTypeController extends Controller
{

    private $foodRepository, $shopCatalogRepository;

    public function __construct(FoodRepository $foodRepository, ShopCatalogRepository $shopCatalogRepository)
    {
        $this->foodRepository = $foodRepository;
        $this->shopCatalogRepository = $shopCatalogRepository;
    }

    public function index(){
        $foodTypes = $this->foodRepository->getFoodTypelist();
        $shopCatalogsFoodTypes = $this->shopCatalogRepository->getFoodTypelist();
        return view('food_types.index')->with('foodTypes', $foodTypes)
                                       ->with('shopCatalogsFoodTypes', $shopCatalogsFoodTypes);
    }

    public function update(Request $request)
    {
        $typeList = $request->except(['_method', '_token']);

        $values = [];

        foreach($typeList as $name => $new_name):
            if(substr($name, 0, 4) !== 'old'):
                if(!empty($new_name)):
                    if( substr($name, 0, 5) === 'food-'):
                        $values['food'][$typeList['old-'.$name]] = $new_name;
                    elseif( substr($name, 0, 8) === 'catalog-' ):
                        $values['catalog'][$typeList['old-'.$name]] = $new_name;
                    endif;
                endif;
            endif;
        endforeach;

        if(isset($values['food'])):
            foreach($values['food'] as $name => $new_name):
                $this->foodRepository->updateType($name, $new_name);
            endforeach;
        endif;

        if(isset($values['catalog'])):
            foreach($values['catalog'] as $name => $new_name):
                $this->shopCatalogRepository->updateType($name, $new_name);
            endforeach;
        endif;

        return redirect(route('foodTypes.index'));
    }
}
