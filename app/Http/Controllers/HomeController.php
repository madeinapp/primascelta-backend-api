<?php

namespace App\Http\Controllers;

use App\Repositories\ShopRepository;
use App\Repositories\subscriptionRepository;
use App\Repositories\UserSubscriptionRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{

    private $shopRepository,
            $subscriptionRepository,
            $userSubscriptionRepository;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ShopRepository $shopRepository,
                                subscriptionRepository $subscriptionRepository,
                                UserSubscriptionRepository $userSubscriptionRepository)
    {
        $this->middleware('auth');
        $this->shopRepository = $shopRepository;
        $this->subscriptionRepository = $subscriptionRepository;
        $this->userSubscriptionRepository = $userSubscriptionRepository;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $shops = 1;
        $allowed_shops = 1;

        $user = Auth::user();

        $check_required_fields = false;

        if( $user ){

            if( $user->user_type === 'company' ){

                $subscription = $this->subscriptionRepository->findBySubscription($user->subscription->subscription);

                if( $subscription ):

                    $allowed_shops = $subscription->shops_num;

                    if( empty(Auth::user()->subscription->parent) ){
                        $subscription_id = Auth::user()->subscription->id;
                    }else{
                        $subscription_id = Auth::user()->subscription->parent;
                    }

                    $shops = $this->userSubscriptionRepository->shopsInSubscription($subscription_id);
                endif;

            }

            /** Verifica dei campi obbligatori mancati per il nuovo utente non ancora attivo */
            if( $user->status === 'pending' ):

                if( empty($user->image_url) ):
                    $check_required_fields = true;
                endif;

                if( empty($user->shop->description) ):
                    $check_required_fields = true;
                endif;

                if( $user->shop->paymentMethodsShop->count() === 0 ):
                    $check_required_fields = true;
                endif;

                if( !$user->shop->obj_opening_timetable->monday->active &&
                    !$user->shop->obj_opening_timetable->tuesday->active &&
                    !$user->shop->obj_opening_timetable->wednesday->active &&
                    !$user->shop->obj_opening_timetable->thursday->active &&
                    !$user->shop->obj_opening_timetable->friday->active &&
                    !$user->shop->obj_opening_timetable->saturday->active &&
                    !$user->shop->obj_opening_timetable->sunday->active ):

                    $check_required_fields = true;

                endif;

                if( empty($user->shop->tax_code) && empty($user->shop->tax_pec) ):
                    $check_required_fields = true;
                endif;
            endif;

        }

        return view('home')->with('shops', $shops)
                           ->with('remaining_shops', $allowed_shops-$shops )
                           ->with('check_required_fields', $check_required_fields )
                            ;
    }

    public function ilMioNegozio(){
        return view('ilMioNegozio');
    }

}
