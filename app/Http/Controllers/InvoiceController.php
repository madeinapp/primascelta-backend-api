<?php

namespace App\Http\Controllers;

use App\Repositories\InvoiceRepository;
use Illuminate\Http\Request;
use Invoice;
use Auth;

class InvoiceController extends Controller
{
    private $invoiceRepository;

    public function __construct(InvoiceRepository $invoiceRepository)
    {
        $this->invoiceRepository = $invoiceRepository;
    }

    public function index(){
        if( Auth::user()->role === 'admin'){
            $payments = $this->invoiceRepository->payments();
            return view('payments.index')->with('payments', $payments);
        }else{
            return redirect(route('home'))->withErrors(['msg' => 'Azione non consentita']);
        }
    }
}
