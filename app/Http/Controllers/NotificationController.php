<?php

namespace App\Http\Controllers;

use App\Repositories\UserRepository;
use App\Repositories\OrderRepository;
use App\Repositories\ShopRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{

    private $userRepository,
            $orderRepository,
            $shopRepository;

    public function __construct(UserRepository $userRepository,
                                OrderRepository $orderRepository,
                                ShopRepository $shopRepository)
    {
        $this->userRepository = $userRepository;
        $this->orderRepository = $orderRepository;
        $this->shopRepository = $shopRepository;
    }

    public function index(){
        $user = Auth::user();

        return view('notifications.index', [
            'notifications' => $user->notifications
        ]);
    }

    public function read($notificationId){

        $notification = Auth::user()->notifications()
                                ->where('id', $notificationId)
                                ->first();

        $notification->markAsRead();

        $user_action = null;
        $order = null;
        $user = null;
        $company = null;
        $shop = null;
        $message = null;
        $from = null;
        $password = null;
        $add_trust_points = null;
        $trust_points = null;

        $content = $notification->data;

        $user = $this->userRepository->find($notification->notifiable_id);

        if( isset($content['user']) ):
            $user_action = $this->userRepository->find($content['user']['id']);
        endif;

        if( isset($content['order']) ):
            $order = $this->orderRepository->find($content['order']['id']);
        endif;

        if( isset($content['company']) ):
            $company = $this->userRepository->find($content['company']['id']);
        endif;

        if( isset($content['shop']) ):
            $shop = $this->shopRepository->find($content['shop']['id']);
        endif;

        if( isset($content['message']) ):
            $message = $content['message'];
        endif;

        if( isset($content['from']) ):
            $from = $content['from'];
        endif;

        if( isset($content['password']) ):
            $password = $content['password'];
        endif;

        if( isset($content['add_trust_points']) ):
            $add_trust_points = $this->orderRepository->find($content['add_trust_points']);
        endif;

        if( isset($content['trust_points']) ):
            $trust_points = $this->orderRepository->find($content['trust_points']);
        endif;

        return view('notifications.show')->with('notification', $notification)
                                         ->with('user', $user)
                                         ->with('user_action', $user_action)
                                         ->with('order', $order)
                                         ->with('add_trust_points', $add_trust_points)
                                         ->with('trust_points', $trust_points)
                                         ->with('company', $company)
                                         ->with('shop', $shop)
                                         ->with('message', $message)
                                         ->with('from', $from)
                                         ->with('password', $password)
                                         ;

        /*
        switch($notification->type){
            case 'App\Notifications\OrderSent':
            case 'App\Notifications\OrderConfirmed':
            case 'App\Notifications\OrderConfirmedWithReserve':
            case 'App\Notifications\OrderDelivered':
            case 'App\Notifications\OrderNotDelivered':
            case 'App\Notifications\OrderCancelled':
                $order = $notification->data['order'];
                return redirect(route('orders.edit', ['order' => $order['id']]));
                break;
            default: return redirect(route('notifications.index'));
        }
        */

    }

}
