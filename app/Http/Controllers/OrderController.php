<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateOrderRequest;
use App\Http\Requests\UpdateOrderRequest;
use App\Repositories\OrderRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\OrderChat;
use App\Notifications\MessageReceived;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use PDF;
use Flash;
use Response;
use Validator;
use Storage;
use DataTables;
use DB;
use App\Models\Order;
use Carbon\Carbon;

use App\Notifications\OrderSent;
use App\Notifications\OrderConfirmed;
use App\Notifications\OrderConfirmedWithReserve;
use App\Notifications\OrderDelivered;
use App\Notifications\OrderNotDelivered;
use App\Notifications\OrderCancelled;
use App\Repositories\OrderChatRepository;
use App\Repositories\UserRepository;
use Illuminate\Auth\Events\Validated;
use Illuminate\Mail\Events\MessageSent;


class OrderController extends AppBaseController
{
    /** @var  OrderRepository */
    private $orderRepository, $orderChatRepository, $userRepository;

    public function __construct(OrderRepository $orderRepo, UserRepository $userRepository, OrderChatRepository $orderChatRepository)
    {
        $this->orderRepository = $orderRepo;
        $this->orderChatRepository = $orderChatRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * Display a listing of the Order.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        if( Auth::user()->user_type === 'company' ):

            $orders = DB::table('orders as o')
                                ->join('order_status as os', 'o.order_status_id', '=', 'os.id')
                                ->join('users as u', 'o.user_id', '=', 'u.id')
                                ->join('shops as s', 'o.shop_id', '=', 's.id')
                                ->where('o.shop_id', '=', Auth::user()->shop->id)
                                ->orderByRaw("IFNULL(o.pickup_on_site_within, IFNULL(home_delivery_within, IFNULL(o.pickup_on_site_date, o.home_delivery_date))), IFNULL(o.pickup_on_site_time, o.home_delivery_time)")
                                ;

            if ($request->ajax()) {

                if ($orderStatus = $request->get('order_status')) {
                    $orders = $orders->where('o.order_status_id', $orderStatus);
                }else{
                    $orders = $orders->where('os.name', 'not_confirmed');
                }

                if ($searchname = $request->get('searchname')) {
                    $orders = $orders->where('u.name', 'like', "%$searchname%");
                }

                if ($delivery = $request->get('delivery')) {
                    if( $delivery == 1) {
                        $orders = $orders->where('o.pickup_on_site', 1);
                    }elseif( $delivery == 2) {
                        $orders = $orders->where('o.home_delivery', 1);
                    }elseif( $delivery == 3) {
                        $orders = $orders->where('o.delivery_host_on_site', 1);
                    }
                }

                if ($user = $request->get('user')) {
                    $orders = $orders->where('u.id', $user);
                }

                $orders = $orders->select('o.*', 'os.description as status', 'u.name as user_name', 'u.trust_points');

                $totalData = $orders->count();

                $datatables = Datatables::of($orders)

                    ->editColumn('user_name', function($row){

                        return $row->user_name . '<br/>' . $row->trust_points . ' punti fiducia';

                    })

                    ->addcolumn('total', function($row){

                        return number_format($row->total_price,2,',','.') . ' €';

                    })

                    ->addcolumn('action', function($row){

                        return $row->status.'<br><a href="'.route('orders.edit', [$row->id]).'" class=\'btn btn-primary btn-sm\'><i class="fas fa-edit"></i> Dettaglio</a>';

                    })

                    ->addcolumn('delivery', function($row){

                        $delivery = '';

                        if( $row->pickup_on_site == 1 ):
                            $delivery = 'Ritiro in sede';
                        elseif( $row->home_delivery == 1 ):
                            $delivery = 'Consegna a domicilio';
                            $delivery .= '<br />';
                            $delivery .= '<small>'. $row->home_delivery_address . '</small>';
                        elseif( $row->delivery_host_on_site == 1 ):
                            $delivery = 'Consegna sul posto';
                        endif;

                        $delivery_date = null;

                        if( $row->pickup_on_site == 1 ):
                            $delivery_date = "<br>";
                            if( !empty($row->pickup_on_site_within) ):
                                $delivery_date .= Carbon::parse($row->pickup_on_site_within)->format('d/m/Y');
                                $delivery_date .= "<br>";
                                $delivery_date .= 'alle ' . Carbon::parse($row->pickup_on_site_within)->format('H:i');
                                elseif( !empty($row->pickup_on_site_date) ):
                                $delivery_date .= Carbon::parse($row->pickup_on_site_date)->format('d/m/Y');

                                if( !empty($row->pickup_on_site_time) ):
                                    $delivery_date .= "<br>";
                                    $delivery_date .= 'alle ' . Carbon::parse($row->pickup_on_site_time)->format('H:i');
                                endif;
                            endif;

                        elseif( $row->home_delivery == 1 ):
                            $delivery_date = '<br>';
                            if( !empty($row->home_delivery_within) ):
                                $delivery_date .= Carbon::parse($row->home_delivery_within)->format('d/m/Y');
                                $delivery_date .= "<br>";
                                $delivery_date .= 'alle ' . Carbon::parse($row->home_delivery_within)->format('H:i');
                            elseif( !empty($row->home_delivery_date) ):
                                $delivery_date .= Carbon::parse($row->home_delivery_date)->format('d/m/Y');

                                if( !empty($row->home_delivery_time) ):
                                    $delivery_date .= "<br>";
                                    $delivery_date .= 'alle ' . Carbon::parse($row->home_delivery_time)->format('H:i');
                                endif;
                            endif;

                        elseif( $row->delivery_host_on_site == 1 ):

                            $delivery_date = '<br>';
                            if( !empty($row->delivery_host_on_site_date_within) ):
                                $delivery_date .= Carbon::parse($row->delivery_host_on_site_date_within)->format('d/m/Y');
                                $delivery_date .= "<br>";
                                $delivery_date .= 'alle ' . Carbon::parse($row->delivery_host_on_site_date_within)->format('H:i');

                            elseif( !empty($row->delivery_host_on_site_date) ):
                                $delivery_date .= Carbon::parse($row->delivery_host_on_site_date)->format('d/m/Y');
                                if( !empty($row->delivery_host_on_site_time) ):
                                    $delivery_date .= "<br>";
                                    $delivery_date .= 'alle ' . Carbon::parse($row->delivery_host_on_site_time)->format('H:i');
                                endif;
                            endif;

                        endif;

                        return $delivery . $delivery_date;
                    })

                    ->addcolumn('order_date', function($row){

                        return Carbon::parse($row->created_at)->format('d/m/Y') . '<br>' . ' alle ' . Carbon::parse($row->created_at)->format('H:i');

                    })

                    ->rawColumns(['user_name', 'total', 'action', 'delivery', 'order_date']);


                return $datatables->make(true);

            }else{

                $totalData = $orders->count();

            }

            $order_status = DB::table('order_status as os')
                                ->select('os.id', 'os.description')
                                ->where('description', '!=', 'Bozza')
                                ->where('description', '!=', 'Confermato con riserva')
                                ->get()
                                ->pluck('description', 'id')
                                ->toArray();

            return view('orders.index')
                    ->with("order_status",$order_status)
                    ->with("tot_catalog",empty($totalData) ? 0 : $totalData)
                ;

        endif;
    }

    /**
     * Show the form for creating a new Order.
     *
     * @return Response
     */
    public function create()
    {
        return view('orders.create');
    }

    /**
     * Store a newly created Order in storage.
     *
     * @param CreateOrderRequest $request
     *
     * @return Response
     */
    public function store(CreateOrderRequest $request)
    {
        $input = $request->all();

        $order = $this->orderRepository->create($input);

        Flash::success('Ordine creato con successo.');

        return redirect(route('orders.index'));
    }

    /**
     * Display the specified Order.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $order = $this->orderRepository->find($id);

        if (empty($order)) {
            Flash::error('Impossibile trovare l\'ordine');

            return redirect(route('orders.index'));
        }

        return view('orders.show')->with('order', $order);
    }

    /**
     * Show the form for editing the specified Order.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $order = $this->orderRepository->find($id);

        if (empty($order)) {
            Flash::error('Impossibile trovare l\'ordine');
            return redirect(route('orders.index'));
        }

        $date_cancel = null;
        $pickup_on_site_confirm_within = null;
        $home_delivery_confirm_within = null;
        $delivery_host_on_site_confirm_within = null;

        if( $order->pickup_on_site == 1 ){
            $pickup_on_site_confirm_within = null;
            if( !empty($order->pickup_on_site_within) ):
                $pickup_on_site_confirm_within = $order->pickup_on_site_within;
            else:
                if( !empty($order->pickup_on_site_date) && !empty($order->pickup_on_site_time) ):
                    $pickup_on_site_confirm_within = Carbon::parse($order->pickup_on_site_date->format('Y-m-d') . ' ' . $order->pickup_on_site_time);
                endif;
            endif;
        }elseif( $order->home_delivery == 1 ){
            $home_delivery_confirm_within = null;
            if( !empty($order->home_delivery_within) ):
                $home_delivery_confirm_within = $order->home_delivery_within;
            else:
                if( !empty($order->home_delivery_date) && !empty($order->home_delivery_time) ):
                    $home_delivery_confirm_within = Carbon::parse($order->home_delivery_date->format('Y-m-d') . ' ' . $order->home_delivery_time);
                endif;
            endif;
        }

        if( !empty($order->cancellable_within) ):
            $date_cancel = $order->cancellable_within->format('Y-m-d H:i');
        endif;

        $daysOfWeekDisabled = [];

        $dayNumbers = [
            "monday" => 1,
            "tuesday" => 2,
            "wednesday" => 3,
            "thursday" => 4,
            "friday" => 5,
            "saturday" => 6,
            "sunday" => 0
        ];

        if( $order->pickup_on_site == 1 ):
            foreach( $order->shop->obj_opening_timetable as $weekday => $ot ):
                if(!$ot->active):
                    $daysOfWeekDisabled[] = $dayNumbers[$weekday];
                endif;
            endforeach;
        elseif( $order->home_delivery == 1 ):
            foreach( $order->shop->obj_delivery_opening_timetable as $weekday => $ot ):
                if(!$ot->active):
                    $daysOfWeekDisabled[] = $dayNumbers[$weekday];
                endif;
            endforeach;
        endif;
        $numOfUserOrders = Order::where('user_id', $order->user_id)->where('shop_id', $order->shop_id)->where('order_status_id', '>', 2)->get()->count();

        //$numOfUserOrders = $this->orderRepository->all(['user_id' => $order->user_id, 'shop_id' => $order->shop_id])->count();

        if( $order->shop_id == Auth::user()->shop->id ){
            return view('orders.edit')->with('order', $order)
                                      ->with('date_cancel', $date_cancel)
                                      ->with('pickup_on_site_confirm_within', $pickup_on_site_confirm_within)
                                      ->with('home_delivery_confirm_within', $home_delivery_confirm_within)
                                      ->with('delivery_host_on_site_confirm_within', $delivery_host_on_site_confirm_within)
                                      ->with('days_of_week_disabled', implode(",", $daysOfWeekDisabled))
                                      ->with('numOfUserOrders', $numOfUserOrders)
                                      ;
        }else{
            Flash::error('Non puoi modificare quest\'ordine');
            return redirect(route('orders.index'));
        }
    }


    /**
     * Remove the specified Order from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $order = $this->orderRepository->find($id);

        if (empty($order)) {
            Flash::error('Impossibile trovare l\'ordine');

            return redirect(route('orders.index'));
        }

        $this->orderRepository->delete($id);

        Flash::success('Ordine eliminato con successo.');

        return redirect(route('orders.index'));
    }

    public function sendMessage($order_id, Request $request){

        $input = $request->all();

        $order = $this->orderRepository->find($order_id);

        if( isset($input['message']) && !empty($input['message']) ):

            $messageFrom = Auth::id() === $order->shop->user->id ? 1 : 0;

            $orderChat = $this->orderChatRepository->create([
                'order_id' => $order->id,
                'from' => $messageFrom,
                'message' => $input['message']
            ]);

            if( $orderChat && env('APP_ENV') === 'production' ):
                if (env('APP_ENV') === 'production'):
                    /** Send Notification */
                    $orderUser = $this->userRepository->find($order->user_id);
                    $orderUser->notify(new MessageReceived(Auth::user(), $order, $input['message'], $messageFrom));
                endif;
            endif;
        endif;
    }

    public function pdfHtml($order_id){
        $order = $this->orderRepository->find($order_id);

        if( $order ):
            //if( Auth::id() == $order->shop->user->id || Auth::id() == $order->user_id ):

                $folder = '/storage/invoices/orders/'.$order->user_id;
                $filename = 'Primascelta_'.$order->id.'.pdf';

                $path = public_path($folder.'/'.$filename);

                if( !is_dir(public_path($folder)) ):
                    mkdir(public_path($folder));
                endif;

                $numOfUserOrders = Order::where('user_id', $order->user_id)
                                        ->where('shop_id', $order->shop_id)
                                        ->where('order_status_id', '>', 2)
                                        ->get()
                                        ->count();

                 return view('orders.pdf')->with(['order' => $order,'numOfUserOrders' => $numOfUserOrders]);
            //else:
            //    return response()->json(['msg' => 'Non puoi visualizzare quest\'ordine'], 400);
            //endif;
        else:
            return response()->json(['msg' => 'Ordine non trovato'], 400);
        endif;
    }


    public function pdf($order_id){
        $order = $this->orderRepository->find($order_id);

        if( $order ):
            if( Auth::id() == $order->shop->user->id || Auth::id() == $order->user_id ):

                $folder = '/storage/invoices/orders/'.$order->user_id;
                $filename = 'Primascelta_'.$order->id.'.pdf';

                $path = public_path($folder.'/'.$filename);

                if( !is_dir(public_path($folder)) ):
                    mkdir(public_path($folder));
                endif;

                $numOfUserOrders = Order::where('user_id', $order->user_id)
                                        ->where('shop_id', $order->shop_id)
                                        ->where('order_status_id', '>', 2)
                                        ->get()
                                        ->count();

                PDF::loadView('orders.pdf', ['order' => $order,
                                             'numOfUserOrders' => $numOfUserOrders])
                                             ->save($path);

                return $filename;
            else:
                return response()->json(['msg' => 'Non puoi visualizzare quest\'ordine'], 400);
            endif;
        else:
            return response()->json(['msg' => 'Ordine non trovato'], 400);
        endif;
    }

    public function downloadPDF($user_id, $filename){

        $file=Storage::disk('public')->get('/invoices/orders/'.$user_id.'/'.$filename);

		return (new Response($file, 200))
              ->header('Content-Type', 'application/pdf');
    }
}
