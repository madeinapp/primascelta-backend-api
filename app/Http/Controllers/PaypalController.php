<?php

namespace App\Http\Controllers;

use App\Models\Invoice;
use App\Repositories\SubscriptionRepository;
use App\Repositories\ShopRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Srmklive\PayPal\Services\AdaptivePayments;
use Srmklive\PayPal\Services\ExpressCheckout;
use Auth;
use Carbon\Carbon;
use Laracasts\Flash\Flash;

class PaypalController extends Controller
{
    protected $subscriptionRepository, $provider, $userRepository, $shopRepository;

    private $subscriptions = [];

    public function __construct(SubscriptionRepository $subscriptionRepository, UserRepository $userRepository, ShopRepository $shopRepository) {
        $this->subscriptionRepository = $subscriptionRepository;
        $this->provider = new ExpressCheckout();
        $this->userRepository = $userRepository;
        $this->shopRepository = $shopRepository;

        $this->subscriptions = [];

        $res =$this->subscriptionRepository->all();
        foreach($res as $sub):
            $this->subscriptions[$sub->subscription] = $sub;
        endforeach;

    }

    public function getSubscription($subscription_type, $invoice_id, $user_id, $other_shops){

        switch( $subscription_type ){
            case 'STANDARD_SEMESTRALE':

                    $price = $this->subscriptions['standard']->price_half_yearly * 6 + (($this->subscriptions['standard']->price_half_yearly * 6) * 0.22);

                    return [
                        'items' => [
                            [
                                'name' => 'Standard semestrale' . config('paypal.invoice_prefix') . ' #' . $invoice_id,
                                'price' => $price,
                                'qty' => 1,
                            ],
                        ],

                        // return url is the url where PayPal returns after user confirmed the payment
                        'return_url' => url('/paypal/notify?user_id='.$user_id.'&subscription_type='.$subscription_type.'&other_shops='.$other_shops),
                        'subscription_desc' => 'Abbonamento Standard semestrale ' . config('paypal.invoice_prefix') . ' #' . $invoice_id,
                        // every invoice id must be unique, else you'll get an error from paypal
                        'invoice_id' => config('paypal.invoice_prefix') . '_' . $invoice_id,
                        'invoice_description' => "Acquisto abbonamento STANDARD SEMESTRALE",
                        'cancel_url' => url('/'),
                        // total is calculated by multiplying price with quantity of all cart items and then adding them up
                        // in this case total is 20 because price is 20 and quantity is 1
                        'total' => $price, // Total price of the cart
                    ];

            case 'STANDARD_ANNUALE':

                $price = $this->subscriptions['standard']->price_yearly * 12 + (($this->subscriptions['standard']->price_yearly * 12) * 0.22);

                return [
                    'items' => [
                        [
                            'name' => 'Standard annuale' . config('paypal.invoice_prefix') . ' #' . $invoice_id,
                            'price' => $price, //219.60,
                            'qty' => 1,
                        ],
                    ],

                    // return url is the url where PayPal returns after user confirmed the payment
                    'return_url' => url('/paypal/notify?user_id='.$user_id.'&subscription_type='.$subscription_type.'&other_shops='.$other_shops),
                    'subscription_desc' => 'Abbonamento Standard annuale ' . config('paypal.invoice_prefix') . ' #' . $invoice_id,
                    // every invoice id must be unique, else you'll get an error from paypal
                    'invoice_id' => config('paypal.invoice_prefix') . '_' . $invoice_id,
                    'invoice_description' => "Acquisto abbonamento STANDARD ANNUALE",
                    'cancel_url' => url('/'),
                    // total is calculated by multiplying price with quantity of all cart items and then adding them up
                    // in this case total is 20 because price is 20 and quantity is 1
                    'total' => $price, //219.60, // Total price of the cart
                ];
            case 'PREMIUM_SEMESTRALE':

                $price = $this->subscriptions['premium']->price_half_yearly * 6 + (($this->subscriptions['premium']->price_half_yearly * 6) * 0.22);

                return [
                    'items' => [
                        [
                            'name' => 'Premium semestrale' . config('paypal.invoice_prefix') . ' #' . $invoice_id,
                            'price' => $price,
                            'qty' => 1,
                        ],
                    ],

                    // return url is the url where PayPal returns after user confirmed the payment
                    'return_url' => url('/paypal/notify?user_id='.$user_id.'&subscription_type='.$subscription_type.'&other_shops='.$other_shops),
                    'subscription_desc' => 'Abbonamento Premium semestrale ' . config('paypal.invoice_prefix') . ' #' . $invoice_id,
                    // every invoice id must be unique, else you'll get an error from paypal
                    'invoice_id' => config('paypal.invoice_prefix') . '_' . $invoice_id,
                    'invoice_description' => "Acquisto abbonamento PREMIUM SEMESTRALE",
                    'cancel_url' => url('/'),
                    // total is calculated by multiplying price with quantity of all cart items and then adding them up
                    // in this case total is 20 because price is 20 and quantity is 1
                    'total' => $price, // Total price of the cart
                ];
            case 'PREMIUM_ANNUALE':

                $price = $this->subscriptions['premium']->price_yearly * 12 + (($this->subscriptions['premium']->price_yearly * 12) * 0.22);

                return [
                    'items' => [
                        [
                            'name' => 'Premium semestrale' . config('paypal.invoice_prefix') . ' #' . $invoice_id,
                            'price' => $price,
                            'qty' => 1,
                        ],
                    ],

                    // return url is the url where PayPal returns after user confirmed the payment
                    'return_url' => url('/paypal/notify?user_id='.$user_id.'&subscription_type='.$subscription_type.'&other_shops='.$other_shops),
                    'subscription_desc' => 'Abbonamento Premium semestrale ' . config('paypal.invoice_prefix') . ' #' . $invoice_id,
                    // every invoice id must be unique, else you'll get an error from paypal
                    'invoice_id' => config('paypal.invoice_prefix') . '_' . $invoice_id,
                    'invoice_description' => "Acquisto abbonamento PREMIUM ANNUALE",
                    'cancel_url' => url('/'),
                    // total is calculated by multiplying price with quantity of all cart items and then adding them up
                    // in this case total is 20 because price is 20 and quantity is 1
                    'total' => $price, // Total price of the cart
                ];
            case 'ENTERPRISE_SEMESTRALE':

                $price = $this->subscriptions['enterprise']->price_half_yearly * 6 + (($this->subscriptions['enterprise']->price_half_yearly * 6) * 0.22);

                return [
                    'items' => [
                        [
                            'name' => 'Enterprise semestrale' . config('paypal.invoice_prefix') . ' #' . $invoice_id,
                            'price' => $price,
                            'qty' => 1,
                        ],
                    ],

                    // return url is the url where PayPal returns after user confirmed the payment
                    'return_url' => url('/paypal/notify?user_id='.$user_id.'&subscription_type='.$subscription_type.'&other_shops='.$other_shops),
                    'subscription_desc' => 'Abbonamento Enterprise semestrale ' . config('paypal.invoice_prefix') . ' #' . $invoice_id,
                    // every invoice id must be unique, else you'll get an error from paypal
                    'invoice_id' => config('paypal.invoice_prefix') . '_' . $invoice_id,
                    'invoice_description' => "Acquisto abbonamento ENTERPRISE SEMESTRALE ANNUALE",
                    'cancel_url' => url('/'),
                    // total is calculated by multiplying price with quantity of all cart items and then adding them up
                    // in this case total is 20 because price is 20 and quantity is 1
                    'total' => $price, // Total price of the cart
                ];
            case 'ENTERPRISE_ANNUALE':

                $price = $this->subscriptions['enterprise']->price_yearly * 12 + (($this->subscriptions['enterprise']->price_yearly * 12) * 0.22);

                return [
                    'items' => [
                        [
                            'name' => 'Enterprise semestrale' . config('paypal.invoice_prefix') . ' #' . $invoice_id,
                            'price' => $price,
                            'qty' => 1,
                        ],
                    ],

                    // return url is the url where PayPal returns after user confirmed the payment
                    'return_url' => url('/paypal/notify?user_id='.$user_id.'&subscription_type='.$subscription_type.'&other_shops='.$other_shops),
                    'subscription_desc' => 'Abbonamento Enterprise semestrale ' . config('paypal.invoice_prefix') . ' #' . $invoice_id,
                    // every invoice id must be unique, else you'll get an error from paypal
                    'invoice_id' => config('paypal.invoice_prefix') . '_' . $invoice_id,
                    'invoice_description' => "Acquisto abbonamento ENTERPRISE ANNUALE ANNUALE",
                    'cancel_url' => url('/'),
                    // total is calculated by multiplying price with quantity of all cart items and then adding them up
                    // in this case total is 20 because price is 20 and quantity is 1
                    'total' => $price, // Total price of the cart
                ];
        }

    }

    public function test(Request $request){
        // check if payment is recurring
        $recurring = false;

        // get new invoice id
        $invoice_id = Invoice::count() + 1;

        // Get the cart data
        $cart = $this->getSubscription('STANDARD_SEMESTRALE', $invoice_id, Auth::id(), "");

        // create new invoice
        $invoice = new Invoice();
        $invoice->title = $cart['invoice_description'];
        $invoice->price = $cart['total'];
        $invoice->method_of_payment = 'paypal';
        $invoice->save();

        //dump\($invoice);

        // send a request to paypal
        // paypal should respond with an array of data
        // the array should contain a link to paypal's payment system
        $response = $this->provider->setExpressCheckout($cart, $recurring);

        //dump\($response);

        // if there is no link redirect back with error message
        if (!$response['paypal_link']) {
            dd("KO");
            return redirect('/paypal/')->with(['code' => 'danger', 'message' => 'Something went wrong with PayPal']);
            // For the actual error message dump out $response and see what's in there
        }

        // redirect to paypal
        // after payment is done paypal
        // will redirect us back to $this->expressCheckoutSuccess
        return redirect($response['paypal_link']);
    }


    public function resume(Request $request){

        $input = $request->all();

        if( !isset($input['btn_payment'])) return redirect(route('home'));

        $subscription_type = $input['btn_payment'];
        $subscription_name = ucwords(strtolower(implode(" ", explode("_", $subscription_type))));

        $my_shop = Auth::user()->shop;
        if( $my_shop ):

            $other_shops = [];
            $tot_activities = 1;

            $subscription = explode("_", $subscription_type)[0];
            $subscription = $this->subscriptionRepository->findBySubscription($subscription);

            if( in_array(strtoupper($subscription->subscription), ['PREMIUM', 'ENTERPRISE']) ):

                $tot_activities = $subscription->shops_num;
                $other_shops = $this->shopRepository->otherShops($my_shop->id);

            endif;

            return view('subscriptions.resume')->with('subscription_type', $subscription_type)
                                                ->with('subscription_name', $subscription_name)
                                                ->with('other_shops', $other_shops)
                                                ->with('tot_activities', $tot_activities)
                                                ->with('subscription', $subscription)
                                                ;

        else:

            return redirect(route('home'))->withErrors(['message' => 'Non risulta un\'attività legata a questa utenza!']);

        endif;

    }

    public function payment(Request $request){


        $input = $request->all();

        if( !isset($input['subscription_type'])) return redirect(route('home'));

        $subscription_type = $input['subscription_type'];

        $other_shops = "";

        if( isset($input['other_shops'])):

            $subscription = $this->subscriptionRepository->findBySubscription(explode("_", $subscription_type)[0]);

            $other_shops_allowed = $subscription->shops_num - 1;

            /*
            switch(explode("_", $subscription_type)[0]){
                case 'PREMIUM': $other_shops_allowed = 1; break;
                case 'ENTERPRISE': $other_shops_allowed = 2; break;
            }
            */

            if( count($input['other_shops']) > $other_shops_allowed ):
                Flash::error('Non puoi selezionare più di ' . $other_shops_allowed . ' attività');
                return back();
            endif;

            $other_shops = implode("-", $input['other_shops']);

        endif;

        // check if payment is recurring
        $recurring = false;

        // get new invoice id
        $invoice_id = Invoice::count() + 1;

        // Get the cart data
        $cart = $this->getSubscription($subscription_type, $invoice_id, Auth::id(), $other_shops);

        // create new invoice
        $invoice = new Invoice();
        $invoice->title = $cart['invoice_description'];
        $invoice->price = $cart['total'];
        $invoice->method_of_payment = 'paypal';
        $invoice->user_id = Auth::id();
        $invoice->save();

        // send a request to paypal
        // paypal should respond with an array of data
        // the array should contain a link to paypal's payment system
        $response = $this->provider->setExpressCheckout($cart, $recurring);

        // if there is no link redirect back with error message
        if (!$response['paypal_link']) {

            return redirect('/paypal/')->withErrors(['message' => 'Si è verificato un errore con Paypal']);
            // For the actual error message dump out $response and see what's in there
        }

        // redirect to paypal
        // after payment is done paypal
        // will redirect us back to $this->expressCheckoutSuccess
        return redirect($response['paypal_link']);
    }

    public function notify(Request $request){
        //dump\($request->all());

        $token = $request->get('token');

        $PayerID = $request->get('PayerID');

        $user_id = $request->get('user_id');

        $subscription_type = $request->get('subscription_type');

        $other_shops = $request->get('other_shops');

        $response = $this->provider->getExpressCheckoutDetails($token);
        //dump\("chekout Details");
        //dump\($response);

        if (!in_array(strtoupper($response['ACK']), ['SUCCESS', 'SUCCESSWITHWARNING'])) {
            return redirect('/paypal/')->withErrors(['message' => 'Errore nell\'elaborazione del pagamento']);
        }

        $invoice_id = explode('_', $response['INVNUM'])[1];

        //dump\("invoice_id", $invoice_id);

        $cart = $this->getSubscription($subscription_type, $invoice_id, $user_id, $other_shops);

        // if payment is not recurring just perform transaction on PayPal
        // and get the payment status
        $payment_status = $this->provider->doExpressCheckoutPayment($cart, $token, $PayerID);
        $status = $payment_status['PAYMENTINFO_0_PAYMENTSTATUS'];

        // find invoice by id
        $invoice = Invoice::find($invoice_id);

        // set invoice status
        $invoice->payment_status = $status;

        // save the invoice
        $invoice->save();

        // App\Invoice has a paid attribute that returns true or false based on payment status
        // so if paid is false return with error, else return with success message
        if ($invoice->paid) {

            //Update User Subscription
            $user = $this->userRepository->find($user_id);
            $parent = $user->shop->id;

            $this->updateSubscription($user, $subscription_type, $invoice_id);

            if( !empty($other_shops) ){
                $other_shops = explode(",", $other_shops);

                if( is_array($other_shops) && count($other_shops) > 0 ):

                    foreach($other_shops as $other_shop):

                        $user = null;

                        $shop = $this->shopRepository->find($other_shop);
                        $user = $this->userRepository->find($shop->user_id);
                        if( $user ):
                            $this->updateSubscription($user, $subscription_type, null, $parent);
                        endif;
                    endforeach;
                endif;
            }

            return redirect(route('home'))->with(['message' => 'Il pagamento dell\'ordine ' . $invoice->id . ' è andato a buon fine!']);
        }

        return redirect('/prices/')->withErrors(['message' => 'Si è verificato un errore in fase di pagamento!']);
    }


    private function updateSubscription($user, $subscription_type, $invoice_id = null, $parent = null){
        $subscription = explode('_', $subscription_type);
        $subscription_name = strtolower($subscription[0]);

        $userSubscription = $user->subscription;

        if( $userSubscription->expire_date->isFuture() ):
            if( strtolower($subscription[1]) === 'annuale' ):
                $userSubscription->expire_date = $userSubscription->expire_date->addYear();
            elseif( strtolower($subscription[1]) === 'semestrale' ):
                $userSubscription->expire_date = $userSubscription->expire_date->addMonths(6);
            endif;
        else:
            if( strtolower($subscription[1]) === 'annuale' ):
                $userSubscription->expire_date = Carbon::now()->addYear();
            elseif( strtolower($subscription[1]) === 'semestrale' ):
                $userSubscription->expire_date = Carbon::now()->addMonths(6);
            endif;
        endif;

        $userSubscription->subscription = $subscription_name;
        if( !empty($invoice_id) ){
            $userSubscription->invoice_id = $invoice_id;
        }
        if( !empty($parent) ){
            $userSubscription->parent = $parent;
        }
        $userSubscription->save();
    }
}
