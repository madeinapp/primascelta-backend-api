<?php

namespace App\Http\Controllers;

use App\Repositories\ShopCatalogRepository;
use App\Repositories\ShopOfferRepository;
use App\Repositories\ShopOfferFoodRepository;
use App\Repositories\ShopRepository;
use Illuminate\Http\Request;

class ShareController extends Controller
{
    private $shopRepository,
            $shopCatalogRepository,
            $shopOfferRepository,
            $shopOfferFoodRepository;

    public function __construct(ShopRepository $shopRepository, ShopCatalogRepository $shopCatalogRepository, ShopOfferRepository $shopOfferRepository, ShopOfferFoodRepository $shopOfferFoodRepository)
    {
        $this->shopRepository = $shopRepository;
        $this->shopCatalogRepository = $shopCatalogRepository;
        $this->shopOfferRepository = $shopOfferRepository;
        $this->shopOfferFoodRepository = $shopOfferFoodRepository;
    }

    public function shop($shop_id){
        $shop = $this->shopRepository->find($shop_id);
        if( $shop ){
            $res = [];
            $res['name'] = $shop->name;
            $res['description'] = $shop->description;
            $res['type'] = $shop->shopType;
            $res['image'] = url(getResizedImage(getOriginalImage($shop->user->image_url), '640x320'));

            return response()->json(['success' => $res]);
        }else{
            return response()->json(['error' => "Attività non presente oppure id errato"], 404);
        }
    }

    public function shopCatalog($shop_catalog_id){
        $shopCatalog = $this->shopCatalogRepository->find($shop_catalog_id);

        if( $shopCatalog ){
            $res = [];
            $res['name'] = $shopCatalog->food_name;
            $res['description'] = $shopCatalog->food_description;
            $res['price'] = number_format($shopCatalog->food_price, 2, ",", ".") . ' €' . ' / ' . $shopCatalog->unitOfMeasure->name;
            $res['shop_name'] = $shopCatalog->shop->name;

            $food_image = null;
            if( !empty($shopCatalog->food_image) ){
                $food_image = url(getResizedImage(getOriginalImage($shopCatalog->food_image), '646x430'));
            }else{
                if( $shopCatalog->food && !empty($shopCatalog->food->image) ){
                    $food_image = url(getResizedImage(getOriginalImage($shopCatalog->food->image), '646x430'));
                }
            }

            $res['image'] = $food_image;

            return response()->json(['success' => $res]);
        }else{
            return response()->json(['error' => "Prodotto non presente oppure id errato"], 404);
        }
    }

    public function shopOffer($shop_offer_id){
        $shopOffer = $this->shopOfferRepository->find($shop_offer_id);

        if( $shopOffer ){

            if( $shopOffer->status === 'available' && $shopOffer->start_date->isPast() && $shopOffer->end_date->isFuture() ){

                $res = [];
                $res['name'] = $shopOffer->name;
                $res['description'] = $shopOffer->description;
                $res['start_date'] = $shopOffer->start_date->format('d/m/Y');
                $res['end_date'] = $shopOffer->end_date->format('d/m/Y');
                $res['shop_name'] = $shopOffer->shop->name;

                $tot_prodotti = $shopOffer->shopOfferFoods->count();
                $res['tot_prodotti'] = $tot_prodotti;

                if( $tot_prodotti === 1 ){

                    $image = null;
                    $firstOfferFood = $shopOffer->shopOfferFoods[0];
                    if( !empty($firstOfferFood->shopCatalog->food_image) ){
                        $image = url(getResizedImage(getOriginalImage($firstOfferFood->shopCatalog->food_image), '646x430'));
                    }elseif( !empty($firstOfferFood->shopCatalog->food->image) ){
                        $image = url(getResizedImage(getOriginalImage($firstOfferFood->shopCatalog->food->image), '646x430'));
                    }

                    $res['image'] = $image;
                }elseif( $tot_prodotti > 1 ){
                    $res['image'] = url('/images/offer.png');
                }

                $res['prodotti'] = [];
                foreach($shopOffer->shopOfferFoods as $shopOfferFood){
                    $res['prodotti'][] = $this->getShopOfferFoodArray($shopOfferFood);
                }
            }

            return response()->json(['success' => $res]);
        }else{
            return response()->json(['error' => "Offerta non presente oppure id errato"], 404);
        }
    }

    public function shopOfferFood($shop_offer_food_id){

        $shopOfferFood = $this->shopOfferFoodRepository->find($shop_offer_food_id);

        if( $shopOfferFood ){
            $res = $this->getShopOfferFoodArray($shopOfferFood);
            return response()->json(['success' => $res]);
        }else{
            return response()->json(['error' => "Prodotto dell'offerta non presente oppure id errato"], 404);
        }

    }

    private function getShopOfferFoodArray($shopOfferFood){
        $res = [];
        $res['name'] = $shopOfferFood->food_name;
        $res['description'] = $shopOfferFood->food_description;
        $res['original_price'] = number_format($shopOfferFood->shopCatalog->food_price, 2, ",", ".") . ' €' . ' / ' . $shopOfferFood->shopCatalog->unitOfMeasure->name;
        $res['discounted_price'] = number_format($shopOfferFood->discounted_price, 2, ",", ".") . ' €' . ' / ' . $shopOfferFood->shopCatalog->unitOfMeasure->name;
        $res['discount'] = $shopOfferFood->discount . ' %';
        $res['min_quantity'] = $shopOfferFood->min_quantity . ' ' . $shopOfferFood->shopCatalog->unitOfMeasureBuy->name;
        $shop = $this->shopRepository->find($shopOfferFood->shopCatalog->shop_id);
        $res['shop_name'] = $shop->name;
        $image = null;
        if( !empty($shopOfferFood->shopCatalog->food_image) ){
            $image = url(getResizedImage(getOriginalImage($shopOfferFood->shopCatalog->food_image), '646x430'));
        }elseif( !empty($shopOfferFood->shopCatalog->food->image) ){
            $image = url(getResizedImage(getOriginalImage($shopOfferFood->shopCatalog->food->image), '646x430'));
        }
        $res['image'] = $image;
        return $res;
    }
}
