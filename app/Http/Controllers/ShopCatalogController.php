<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateShopCatalogRequest;
use App\Http\Requests\UpdateShopCatalogRequest;
use App\Repositories\ShopCatalogRepository;
use App\Http\Controllers\AppBaseController;
use App\Repositories\FoodCategoryRepository;
use App\Repositories\FoodRepository;
use App\Repositories\ShopOfferFoodRepository;
use App\Repositories\UnitOfMeasureRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\FoodCategory;
use App\Models\ShopCatalog;
use App\Repositories\ShopOfferRepository;
use App\Repositories\SpecialRepository;
use Carbon\Carbon;
use Flash;
use Response;
use File;
use Image;
use DataTables;
use DB;
use Auth;
use Form;

class ShopCatalogController extends AppBaseController
{
    /** @var  ShopCatalogRepository */
    private $shopCatalogRepository,
            $foodRepository,
            $foodCategoryRepository,
            $unitOfMeasureRepository,
            $specialRepository,
            $shopOfferRepository,
            $shopOfferFoodRepository;

    public function __construct(ShopCatalogRepository $shopCatalogRepo,
                                FoodRepository $foodRepository,
                                FoodCategoryRepository $foodCategoryRepository,
                                UnitOfMeasureRepository $unitOfMeasureRepository,
                                SpecialRepository $specialRepository,
                                ShopOfferRepository $shopOfferRepository,
                                ShopOfferFoodRepository $shopOfferFoodRepository
                                )
    {
        $this->shopCatalogRepository = $shopCatalogRepo;
        $this->foodRepository = $foodRepository;
        $this->foodCategoryRepository = $foodCategoryRepository;
        $this->unitOfMeasureRepository = $unitOfMeasureRepository;
        $this->specialRepository = $specialRepository;
        $this->shopOfferRepository = $shopOfferRepository;
        $this->shopOfferFoodRepository = $shopOfferFoodRepository;
    }

    /**
     * Display a listing of the ShopCatalog.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        if( Auth::user()->user_type === 'company' ):

            $shopCatalogs = DB::table('shop_catalogs as c')
                                ->join('food_categories as fc', 'c.food_category_id', '=', 'fc.id')
                                ->join('unit_of_measures as u', 'c.unit_of_measure_id', '=', 'u.id')
                                ->where('c.shop_id', '=', Auth::user()->shop->id)
                                ->whereNull('deleted_at')
                                ;

            if ($request->ajax()) {

                // additional users.name search
                if ($searchname = $request->get('searchname')) {
                    $shopCatalogs = $shopCatalogs->where('c.food_name', 'like', "%$searchname%");
                }
                if ($genere = $request->get('genere')) {
                    $shopCatalogs = $shopCatalogs->where('fc.id', '=', "$genere");
                }
                if ($tipologia = $request->get('tipologia')) {
                    $shopCatalogs = $shopCatalogs->where('c.food_type', '=', "$tipologia");
                }

                $shopCatalogs = $shopCatalogs->select('fc.name as food_category', 'u.name as unit_of_measure', 'c.*' );

                $totalData = $shopCatalogs->count();

                //dd($shopCatalogs);

                $datatables = Datatables::of($shopCatalogs)

                        ->editColumn('food_name', function($row){

                            $btn = '<input type="checkbox" class="chk_del_shop_catalog form-control" name="del_shop_catalog[]" value="' . $row->id . '" style="display: none; width: 1.5em; vertical-align: middle; margin-right: .4em">';

                            if( !empty($row->food_id) ):
                                $btn .= $row->food_name . " <i class='fas fa-check-circle text-success' title='Da catalogo Primascelta'></i>";
                            else:
                                $btn .= '<span>'.$row->food_name.'</span>';
                            endif;
                            return $btn;
                        })

                        ->editColumn('image', function($row){
                            if (empty($row->food_image))
                                $image = "<img style=\"width: 100%; max-width: 200px;\" src='/images/food.jpg'>";
                            else
                                $image = "<img style=\"width: 100%; max-width: 200px;\" width='100'  src='".getResizedImage(getOriginalImage($row->food_image),"230x154")."'>";
                            return $image;
                        })

                        ->editColumn('category', function($row){

                            return '<strong>' . $row->food_category . '</strong><br>' . $row->food_type;

                        })

                        ->addcolumn('prezzo', function($row){

                            return '<span style="display: none;">'.$row->food_price.'</span>'.number_format($row->food_price,2,',','.') . ' € \ ' . ($row->food_unit_of_measure_quantity > 1 ? $row->food_unit_of_measure_quantity : '') . ' ' . $row->unit_of_measure;

                        })

                        ->addcolumn('active', function($row){


                        })

                        ->addcolumn('action', function($row){
                            $btn =  Form::open(['route' => ['shopCatalogs.destroy', $row->id], 'method' => 'delete']);

                            $btn .= '<div class="actions text-center" style="display: block">';
                            $btn .= '<a href="'.route('shopCatalogs.show', ['shopCatalog' => $row->id]).'" class=\'btn btn-primary btn-sm\' title="ANTEPRIMA"><i class="fas fa-eye"></i></a>';
                            $btn .= '<a href="'.route('shopCatalogs.edit', ['shopCatalog' => $row->id]).'" class=\'btn btn-warning btn-sm\' title="MODIFICA"><i class="fas fa-edit"></i></a>';
                            $btn .= Form::button('<i class="fas fa-trash"></i>', ['class' => 'btn btn-danger btn-sm', 'onclick' => "delAllSelected($row->id);",  'title' => "ELIMINA"]);
                            $btn .= '</div>';

                            if( $row->visible_in_search == 0 ):
                                $btn .= '<a href="'.route('shopCatalogs.active', $row->id).'" class="btn btn-block btn-sm btn-danger mt-1"><i class="fas fa-ban"></i> Non attivo</a>';
                            else:
                                $btn .= '<a href="'.route('shopCatalogs.active', $row->id).'" class="btn btn-block btn-sm btn-success mt-1"><i class="fas fa-check"></i> Attivo</a>';
                            endif;

                            $btn .= Form::close();
                            return $btn;
                        })

                        ->rawColumns(['food_name', 'image', 'prezzo', 'category', 'active', 'action']);

                return $datatables->make(true);

            }else{

                $totalData = $shopCatalogs->count();

            }

            $obj_food_categories = DB::table('shop_catalogs as c')
                                    ->join('food_categories as fc', 'c.food_category_id', '=', 'fc.id')
                                    ->where('c.shop_id', '=', Auth::user()->shop->id)
                                    ->whereNull('deleted_at')
                                    ->select('c.food_category_id', 'fc.name')
                                    ->distinct('c.food_category_id', 'fc.name')
                                    ->orderBy('name')
                                    ->get()
                                    ;

            //$obj_food_categories = FoodCategory::select('id','name')->orderBy('name')->get();
            $arr_food_categories = [];
            $arr_food_categories[""] = "Cerca in tutti i generi";
            foreach($obj_food_categories as $fc){
                $arr_food_categories[$fc->food_category_id] = $fc->name;
            }

            $arr_food_type = [];
            $arr_food_type[""] = "Cerca in tutte le tipologie";


            return view('shop_catalogs.index')
                    ->with("generi",$arr_food_categories)
                    ->with("tipologie",$arr_food_type)
                    ->with("tot_catalog",empty($totalData) ? 0 : $totalData)
                ;
        endif;
    }

    /**
     * Show the form for creating a new ShopCatalog.
     *
     * @return Response
     */
    public function create()
    {
        $foodCategoryList = $this->foodCategoryRepository->getFoodCategoryList();
        $foodCategoryList = ['' => 'Seleziona...'] + $foodCategoryList;

        $catalogCategoryList = $this->shopCatalogRepository->getFoodCategoryList();
        $catalogCategoryList = ['' => 'Seleziona...'] + $catalogCategoryList;

        $unitOfMeasureList = $this->unitOfMeasureRepository->all([], null, null, ['*'], 'name')->pluck('name', 'id')->toArray();
        $unitOfMeasureList = ['' => 'Seleziona...'] + $unitOfMeasureList;


        $visibleList = ["SI","NO"];

        $totFoods = $this->foodRepository->countAll();

        $defaultUom = FoodCategory::select('id','unit_of_measure')->get()->pluck('unit_of_measure', 'id')->toArray();

        $specials = $this->specialRepository->allSpecials();



        return view('shop_catalogs.create')->with(['foodCategoryList' => $foodCategoryList,
                                                    'catalogCategoryList' => $catalogCategoryList,
                                                    'foodTypeList' => [],
                                                    'visibleList' => $visibleList,
                                                    'unitOfMeasureList' => $unitOfMeasureList,
                                                    'totFoods' => $totFoods,
                                                    'defaultUom' => json_encode($defaultUom),
                                                    'relatedUoms' => [],
                                                    'specials' => $specials
                                                    ]);
    }

    /**
     * Store a newly created ShopCatalog in storage.
     *
     * @param CreateShopCatalogRequest $request
     *
     * @return Response
     */
    public function store(CreateShopCatalogRequest $request)
    {
        $input = $request->all();

        $input['food_price'] = tofloat($input['food_price']);

        if( !isset($input['visible_in_search']) ):
            $input['visible_in_search'] = 0;
        endif;

        if( !isset($input['home_delivery']) ):
            $input['home_delivery'] = 0;
        endif;

        if( !isset($input['in_app_shopping']) ):
            $input['in_app_shopping'] = 0;
        endif;


        if( !isset($input['uncertain_weight']) ):
            $input['uncertain_weight'] = 0;
        endif;
        /*
        if( $request->hasFile('fileimage') ):

            $image_rule = [
                'fileimage' => 'image|mimes:jpg,jpeg|max:10240|dimensions:min_width=646,min_height=430',
            ];

            $image_messages = [
                'fileimage.mimes' => 'L\'immagine deve essere di tipo .jpg',
                'fileimage.max' => 'ATTENZIONE: questo file presenta dei problemi. Le immagini caricate devono rispettare precise caratteristiche. Grazie',
                'fileimage.size' => 'ATTENZIONE: questo file presenta dei problemi. Le immagini caricate devono rispettare precise caratteristiche. Grazie',
                'fileimage.dimensions' => 'ATTENZIONE: l\'immagine che stai caricando presenta dimensioni troppo piccole: ti consigliamo di sceglierne un\'altra',
            ];
            $image_validator = Validator::make(['fileimage' => $input['fileimage']], $image_rule, $image_messages);
            if($image_validator->fails()){
                return back()->withInput()->withErrors($image_validator);
            }
        endif;
        */

        $shopCatalog = $this->shopCatalogRepository->create($input);

        if( !empty($input['shop_catalog_specials']) ):
            $shopCatalog->specials()->sync($input['shop_catalog_specials']);
        endif;

        /*
        if( $request->hasFile('fileimage') ):
            /** File type validation *

            $imageName = NormalizeString($input['fileimage']).'_'.time().'.'.$request->fileimage->extension();

            $path = storage_path('app/public/images/shops/'.$input['shop_id']);
            if(!File::isDirectory($path)){
                File::makeDirectory($path, 0777, true, true);
            }

            $request->fileimage->move(storage_path('app/public/images/shops/'.$input['shop_id'].'/'), $imageName);

            //Generate cropped thumbs image
            $path_image_name = storage_path('app/public/images/shops/'.$input['shop_id'].'/').$imageName;
            $image = Image::make($path_image_name)->orientate();
            cropImages($image,$path_image_name, 'food');
            unset($image);
            unlink($path_image_name);
            $shopCatalog->food_image = asset('/storage/images/shops/'.$input['shop_id'].'/'.$imageName);
            $shopCatalog->save();

        endif;
        */

        $imageFullPath = null;
        if( !empty($input['output_image_data']) && !empty($input['output_image_type']) ):
            $folderPath = public_path('storage/images/shops/'.$input['shop_id']);

            if( !File::isDirectory($folderPath) ) {
                File::makeDirectory($folderPath, 0775, true); //creates directory
            }

            $image = $input['output_image_data'];
            $image_parts = explode(";base64,", $image);
            $image_type_aux = explode("image/", $image_parts[0]);
            $image_type = $image_type_aux[1];
            $image_base64 = base64_decode($image_parts[1]);

            $image_info = explode("/", $input['output_image_type']);
            $extension = $image_info[1];

            $filename = $input['output_image_name'];
            $file_parts = pathinfo($filename);
            $filename = NormalizeString($file_parts['basename']).'_'.time().'.'.$extension;

            $imageFullPath = $folderPath.'/'.$filename;
            file_put_contents($imageFullPath, $image_base64);

            //Generate cropped thumbs image
            $image = Image::make($imageFullPath)->orientate();
            cropImages($image,$imageFullPath, 'food');
            unset($image);
            unlink($imageFullPath);
            $shopCatalog->food_image = asset('/storage/images/shops/'.$input['shop_id'].'/'.$filename);
            $shopCatalog->save();
        endif;

        if( isset($input['save-and-create-offer']) ):

            $newOffer = [
                'shop_id' => $shopCatalog->shop_id,
                'status' => 'draft',
                'name' => 'Offerta ' . $shopCatalog->food_name,
                'description' => $shopCatalog->food_description,
                'start_date' => Carbon::now()->timezone('Europe/Rome')->addDay(),
                'end_date' => Carbon::now()->timezone('Europe/Rome')->addDays(2),
            ];

            $offer = $this->shopOfferRepository->create($newOffer);

            if( $offer ):

                $newOfferFood = [
                    'shop_offer_id' => $offer->id,
                    'shop_catalog_id' => $shopCatalog->id,
                    'food_name' => $shopCatalog->food_name,
                    'food_image' => $shopCatalog->food_image,
                    'food_description' => $shopCatalog->food_description,
                    'min_quantity' => 1,
                    'discount' => 10,
                    'discounted_price' => $shopCatalog->food_price - ( $shopCatalog->food_price * 0.1 )
                ];

                if( $this->shopOfferFoodRepository->create($newOfferFood) ):
                    return redirect(route('shopOffers.edit', $offer->id));
                else:
                    return redirect(route('shopCatalogs.index'))->withErrors(['msg' => 'Errore creazione offerta']);
                endif;
            else:
                return redirect(route('shopCatalogs.index'))->withErrors(['msg' => 'Errore creazione offerta']);
            endif;
        else:
            Flash::success('Prodotto creato con successo.');
            return redirect(route('shopCatalogs.index'));
        endif;
    }

    /**
     * Display the specified ShopCatalog.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $shopCatalog = $this->shopCatalogRepository->find($id);

        if (empty($shopCatalog)) {
            Flash::error('Shop Catalog not found');
            return redirect(route('shopCatalogs.index'));
        }

        $foodImage = $shopCatalog->food_image;
        if( !empty($shopCatalog->food_image) ){
            if( dirname($shopCatalog->food_image) == '.'  ){
                $foodImage = url('storage/images/foods/'.NormalizeString($shopCatalog->foodCategory->name).'/'.$shopCatalog->food_image);
            }else{
                $foodImage = $shopCatalog->food_image;
            }
        }else{
            $food = $this->foodRepository->find($shopCatalog->food_id);
            if( $food ){
                $foodImage = url('storage/images/foods/'.NormalizeString($food->foodCategory->name).'/'.$food->image);
            }
        }

        /* Verifico la presenza  */
        $foodImage = getResizedImage(getOriginalImage($shopCatalog->food_image), '646x430');
        $thumbnails = true;
        if( !$foodImage ){
            $thumbnails = false;
            $foodImage = '/images/no-image.png';
        }

        return view('shop_catalogs.show')->with(['shopCatalog' => $shopCatalog,
                                                 'foodImage' => $foodImage,
                                                 'thumbnails' => $thumbnails]);
    }

    /**
     * Show the form for editing the specified ShopCatalog.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $shopCatalog = $this->shopCatalogRepository->find($id);

        if (empty($shopCatalog)) {
            Flash::error('Shop Catalog not found');

            return redirect(route('shopCatalogs.index'));
        }

        $foodCategoryList = $this->foodCategoryRepository->getFoodCategoryList();
        $foodCategoryList = ['' => 'Seleziona...'] + $foodCategoryList;
        //ksort($foodCategoryList);

        $unitOfMeasureList = $this->unitOfMeasureRepository->all([], null, null, ['*'], 'name')->pluck('name', 'id')->toArray();
        $unitOfMeasureList = ['' => 'Seleziona...'] + $unitOfMeasureList;

        $visibleList = ["SI","NO"];
        $foodTypeList = $this->foodRepository->getFoodTypelist($shopCatalog->food_category_id);

        $foodTypeList = array_combine($foodTypeList, $foodTypeList);

        $defaultUom = FoodCategory::select('id','unit_of_measure')->get()->pluck('unit_of_measure', 'id')->toArray();

        $relatedUoms = $this->unitOfMeasureRepository->getRelateds($shopCatalog->unit_of_measure_id);
        $relatedUoms = $this->unitOfMeasureRepository->getUoms($relatedUoms);

        $specials = $this->specialRepository->allSpecials();

        return view('shop_catalogs.edit')->with(['shopCatalog' => $shopCatalog,
                                                 'foodCategoryList' => $foodCategoryList,
                                                 'unitOfMeasureList' => $unitOfMeasureList,
                                                 'visibleList' => $visibleList,
                                                 'foodTypeList' => $foodTypeList,
                                                 'defaultUom' => json_encode($defaultUom),
                                                 'relatedUoms' => $relatedUoms,
                                                 'specials' => $specials
                                                ]);
    }

    /**
     * Update the specified ShopCatalog in storage.
     *
     * @param int $id
     * @param UpdateShopCatalogRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateShopCatalogRequest $request)
    {
        $shopCatalog = $this->shopCatalogRepository->find($id);

        if (empty($shopCatalog)) {
            Flash::error('Prodotto non trovato');
            return redirect(route('shopCatalogs.index'));
        }

        $input = $request->all();

        $input['food_price'] = tofloat($input['food_price']);

        if( !isset($input['visible_in_search']) ):
            $input['visible_in_search'] = 0;
        endif;

        if( !isset($input['home_delivery']) || $input['visible_in_search'] === 0 ):
            $input['home_delivery'] = 0;
        endif;

        if( !isset($input['in_app_shopping']) || $input['visible_in_search'] === 0 ):
            $input['in_app_shopping'] = 0;
        endif;

        if( !isset($input['uncertain_weight']) || $input['uncertain_weight'] === 0 ):
            $input['uncertain_weight'] = 0;
        endif;
        /*
        if( $request->hasFile('fileimage') ):


            $image_rule = [
                'fileimage' => 'image|mimes:jpg,jpeg|max:10240|dimensions:min_width=646,min_height=430',
            ];

            $image_messages = [
                'fileimage.mimes' => 'L\'immagine deve essere di tipo .jpg',
                'fileimage.max' => 'ATTENZIONE: questo file presenta dei problemi. Le immagini caricate devono rispettare precise caratteristiche. Grazie',
                'fileimage.size' => 'ATTENZIONE: questo file presenta dei problemi. Le immagini caricate devono rispettare precise caratteristiche. Grazie',
                'fileimage.dimensions' => 'ATTENZIONE: l\'immagine che stai caricando presenta dimensioni troppo piccole: ti consigliamo di sceglierne un\'altra',
            ];

            $image_validator = Validator::make(['fileimage' => $input['fileimage']], $image_rule, $image_messages);
            if($image_validator->fails()){
                return back()->withInput()->withErrors($image_validator);
            }
        endif;
        */

        $input['food_image'] = $shopCatalog->food_image;

        $shopCatalog = $this->shopCatalogRepository->update($input, $id);

        if( !empty($input['shop_catalog_specials']) ):
            $shopCatalog->specials()->sync($input['shop_catalog_specials']);
        else:
            $shopCatalog->specials()->sync([]);
        endif;

        /*
        if( $request->hasFile('fileimage') ):

            /** File type validation *
            $imageName = NormalizeString($input['fileimage']).'_'.time().'.'.$request->fileimage->extension();

            if( !empty($shopCatalog->food_image) ):
                @unlink(storage_path('app/public/images/shops/'.$input['shop_id'].'/'.basename(getResizedImage($shopCatalog->food_image, '230x154'))));
                @unlink(storage_path('app/public/images/shops/'.$input['shop_id'].'/'.basename(getResizedImage($shopCatalog->food_image, '512x340'))));
                @unlink(storage_path('app/public/images/shops/'.$input['shop_id'].'/'.basename(getResizedImage($shopCatalog->food_image, '646x430'))));
                @unlink(storage_path('app/public/images/shops/'.$input['shop_id'].'/'.basename($shopCatalog->food_image)));
            endif;


            $path = storage_path('app/public/images/shops/'.$input['shop_id']);
            if(!File::isDirectory($path)){
                File::makeDirectory($path, 0777, true, true);
            }

            $request->fileimage->move(storage_path('app/public/images/shops/'.$input['shop_id'].'/'), $imageName);

            //Generate cropped thumbs image
            $path_image_name = storage_path('app/public/images/shops/'.$input['shop_id'].'/').$imageName;
            $image = Image::make($path_image_name)->orientate();
            cropImages($image,$path_image_name,'food');
            unset($image);
            unlink($path_image_name);
            $shopCatalog->food_image = asset('/storage/images/shops/'.$input['shop_id'].'/'.$imageName);
            $shopCatalog->save();

        endif;
        */

        $imageFullPath = null;
        if( !empty($input['output_image_data']) && !empty($input['output_image_type']) ):

            if( !empty($shopCatalog->food_image) ):
                @unlink(storage_path('app/public/images/shops/'.$input['shop_id'].'/'.basename(getResizedImage($shopCatalog->food_image, '100x100'))));
                @unlink(storage_path('app/public/images/shops/'.$input['shop_id'].'/'.basename(getResizedImage($shopCatalog->food_image, '230x154'))));
                @unlink(storage_path('app/public/images/shops/'.$input['shop_id'].'/'.basename(getResizedImage($shopCatalog->food_image, '512x340'))));
                @unlink(storage_path('app/public/images/shops/'.$input['shop_id'].'/'.basename(getResizedImage($shopCatalog->food_image, '646x430'))));
                @unlink(storage_path('app/public/images/shops/'.$input['shop_id'].'/'.basename($shopCatalog->food_image)));
            endif;

            $folderPath = public_path('storage/images/shops/'.$input['shop_id']);

            if( !File::isDirectory($folderPath) ) {
                File::makeDirectory($folderPath, 0775, true); //creates directory
            }

            $image = $input['output_image_data'];
            $image_parts = explode(";base64,", $image);
            $image_type_aux = explode("image/", $image_parts[0]);
            $image_type = $image_type_aux[1];
            $image_base64 = base64_decode($image_parts[1]);

            $image_info = explode("/", $input['output_image_type']);
            $extension = $image_info[1];

            $filename = $input['output_image_name'];
            $file_parts = pathinfo($filename);
            $filename = NormalizeString($file_parts['basename']).'_'.time().'.'.$extension;

            $imageFullPath = $folderPath.'/'.$filename;
            file_put_contents($imageFullPath, $image_base64);

            //Generate cropped thumbs image
            $image = Image::make($imageFullPath)->orientate();
            cropImages($image,$imageFullPath, 'food');
            unset($image);
            unlink($imageFullPath);
            $shopCatalog->food_image = asset('/storage/images/shops/'.$input['shop_id'].'/'.$filename);
            $shopCatalog->save();
        endif;

        Flash::success('Prodotto modificato con successo.');

        return redirect(route('shopCatalogs.index'));
    }

    /**
     * Remove the specified ShopCatalog from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $shopCatalog = $this->shopCatalogRepository->find($id);

        if (empty($shopCatalog)) {
            Flash::error('Prodotto inesistente!');
            return redirect(route('shopCatalogs.index'));
        }

        foreach($shopCatalog->shopOfferFoods as $shopOfferFood):
            if(  $shopOfferFood->shopOffer ):
                Flash::error('Non puoi cancellare il prodotto perché é collegato ad offerte. Elimina prima le offerte');
                return redirect(route('shopCatalogs.index'));

                /*
                if( ($shopOfferFood->shopOffer->status === 'available' && $shopOfferFood->shopOffer->start_date->isPast() && $shopOfferFood->shopOffer->end_date->isFuture()) ||
                    ($shopOfferFood->shopOffer->status === 'available' && $shopOfferFood->shopOffer->start_date->isFuture()) ):

                endif;
                */
            endif;
        endforeach;

        $this->shopCatalogRepository->delete($id);

        Flash::success('Prodotto eliminato con successo.');
        return redirect(route('shopCatalogs.index'));
    }

    public function setActive($shop_catalog_id){
        $shop_catalog = $this->shopCatalogRepository->find($shop_catalog_id);
        if( $shop_catalog->visible_in_search == 1 ){
            $this->shopCatalogRepository->update([
                'visible_in_search' => 0,
                'home_delivery' => 0,
                'in_app_shopping' => 0
            ], $shop_catalog_id);
        }else{
            $this->shopCatalogRepository->update([
                'visible_in_search' => 1,
                'home_delivery' => 1,
                'in_app_shopping' => 1
            ], $shop_catalog_id);
        }
        return redirect(route('shopCatalogs.index'));
    }


    public function multiDestroy(Request $request)
    {

        $input = $request->all();

        $error = '';

        if( isset($input['selected']) && is_array($input['selected']) ):


            foreach($input['selected'] as $shop_catalog_id):

                $shopCatalog = $this->shopCatalogRepository->find($shop_catalog_id);

                if (!empty($shopCatalog)) {

                    $can_delete = true;

                    foreach($shopCatalog->shopOfferFoods as $shopOfferFood):
                        if(  $shopOfferFood->shopOffer ):

                            $can_delete = false;

                            if( !empty($error) ){
                                $error .= PHP_EOL;
                            }

                            $error .= "Non puoi cancellare il prodotto " . $shopCatalog->food_name . " perché ha delle offerte collegate. Elimina prima le offerte.";
                            break;

                        endif;
                    endforeach;

                    if( $can_delete ):

                        $this->shopCatalogRepository->delete($shop_catalog_id);

                    endif;

                }

            endforeach;

        else:

            return response()->json(['status' => 'error', 'msg' => 'Non hai selezionato nessun prodotto']);

        endif;

        if( !empty($error) ){

            $error = 'Si sono verificati i seguenti errori: ' . PHP_EOL . $error;

        }

        return response()->json(['status' => 'ok', 'errors' => $error]);

    }


    public function multiActivate(Request $request){

        $input = $request->all();

        $error = '';

        if( isset($input['selected']) && is_array($input['selected']) ):

            foreach($input['selected'] as $shop_catalog_id):

                $shop_catalog = $this->shopCatalogRepository->find($shop_catalog_id);

                $this->shopCatalogRepository->update([
                    'visible_in_search' => 1,
                    'home_delivery' => 1,
                    'in_app_shopping' => 1
                ], $shop_catalog_id);
            endforeach;

        endif;

        return response()->json(['status' => 'ok']);
    }

    public function multiDisable(Request $request){

        $input = $request->all();

        $error = '';

        if( isset($input['selected']) && is_array($input['selected']) ):

            foreach($input['selected'] as $shop_catalog_id):

                $shop_catalog = $this->shopCatalogRepository->find($shop_catalog_id);

                $this->shopCatalogRepository->update([
                    'visible_in_search' => 0,
                    'home_delivery' => 0,
                    'in_app_shopping' => 0
                ], $shop_catalog_id);
            endforeach;

        endif;

        return response()->json(['status' => 'ok']);
    }
}
