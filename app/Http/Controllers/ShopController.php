<?php

namespace App\Http\Controllers;

use App\Http\Controllers\AppBaseController;

use Illuminate\Http\Request;
use Illuminate\Notifications\Messages\MailMessage;
use App\Http\Requests\CreateShopRequest;
use App\Http\Requests\UpdateProfileCompanyRequest;

use App\Repositories\ShopRepository;
use App\Repositories\ShopTypeRepository;
use App\Repositories\UserRepository;
use App\Repositories\UserSubscriptionRepository;
use App\Repositories\PaymentMethodShopRepository;
use App\Repositories\SubscriptionRepository;
use App\Repositories\AgentRepository;

use App\Models\Invoice;
use App\Models\UserSubscription;
use App\Models\ShopType;
use App\Models\PaymentMethod;
use App\Models\TaxType;
use App\Models\User;
use App\Models\Shop;

use App\Notifications\PasswordChanged;

use Auth;
use File;
use Image;
use Hash;
use Log;
use Flash;
use Response;
use Carbon\Carbon;
use DB;
use Mail;

class ShopController extends AppBaseController
{
    /** @var  ShopRepository */
    private $shopRepository,
            $shopTypeRepository,
            $userRepository,
            $userSubscriptionRepository,
            $paymentMethodShopRepository,
            $subscriptionRepository,
            $agentRepository;

    public function __construct(ShopRepository $shopRepo,
                                ShopTypeRepository $shopTypeRepo,
                                UserRepository $userRepository,
                                UserSubscriptionRepository $userSubscriptionRepository,
                                PaymentMethodShopRepository $paymentMethodShopRepository,
                                SubscriptionRepository $subscriptionRepository,
                                AgentRepository $agentRepository
                                )
    {
        $this->shopRepository = $shopRepo;
        $this->shopTypeRepository = $shopTypeRepo;
        $this->userRepository = $userRepository;
        $this->userSubscriptionRepository = $userSubscriptionRepository;
        $this->paymentMethodShopRepository = $paymentMethodShopRepository;
        $this->subscriptionRepository = $subscriptionRepository;
        $this->agentRepository = $agentRepository;
    }

    /**
     * Display a listing of the Shop.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $shops = $this->shopRepository->all([], null, null, ['*'], 'name');
        return view('shops.index')
            ->with('shops', $shops);
    }

    /**
     * Show the form for creating a new Shop.
     *
     * @return Response
     */
    public function create()
    {
		$agents = $this->agentRepository->all([], null, null, ['id', 'name'], 'name')->pluck('name', 'id')->toArray();
        $payment_methods = $this->shopRepository->getPaymentMethods();
        $arr_payment_methods = [];
        foreach($payment_methods as $payment_method){
            $arr_payment_methods[$payment_method->id] = __($payment_method->name);
        }
        $shop_types = $this->shopRepository->getShopTypes();
        $arr_shop_types = [];
        foreach($shop_types as $shop_type){
            $arr_shop_types[$shop_type->id] = __($shop_type->name);
        }
        $delivery_methods = $this->shopRepository->getDeliveryMethods();
        foreach($delivery_methods as $dm){
            $arr_delivery_methods[$dm->id] = __($dm->name);
        }
		$arr_tax_types = TaxType::orderBy('name')->get()->pluck('name', 'id')->toArray();
		$arr_tax_types[0] = '';
		
        $res = $this->subscriptionRepository->all()->sortBy('position');

        $subscriptions = [];
        foreach($res as $sub):
            $subscriptions[$sub->subscription] = $sub;
        endforeach;
        return view('shops.create')->with(['arr_tax_types' => $arr_tax_types, 'arr_payment_methods' => $arr_payment_methods, 'arr_shop_types' => $arr_shop_types, 'arr_delivery_methods' => $arr_delivery_methods, 'arr_agents' => $agents, 'subscriptions' => $subscriptions]);
    }

    /**
     * Store a newly created Shop in storage.
     *
     * @param CreateShopRequest $request
     *
     * @return Response
     */
    public function store(CreateShopRequest $request)
    {
		exit;
        $input = $request->all();

        $shop = $this->shopRepository->create($input);

        Flash::success('Negozio creato con successo.');

        return redirect(route('shops.index'));
    }
	
	/**
     * Store a newly created Shop in storage only for Admin.
     *
     * @param CreateShopRequest $request
     *
     * @return Response
     */

    public function store_admin(Request $request)
	{
		$input = $request->all();
		//Insert new user
		$user_input['api_token'] = null;
		$user_input['user_type'] = 'company';
		$user_input['password'] = Hash::make($input['new_password']);
		$user_input['status'] = 'active';
		$user_input['name'] = $input['user_name'];
		$user_input['email'] = $input['email'];
		$user_input['phone'] = $input['phone'];
		$user_input['email'] = $input['email'];
		$user_input['birthdate'] = $input['user_birthdate'];
		$user_input['privacy'] = 0;
		/** Check user age */
		if(!empty($user_input['birthdate'])){
			$user_birth = Carbon::createFromFormat('d/m/Y', $user_input['birthdate']);
			$user_age = Carbon::parse($user_birth)->age;
			if($user_age < 18 && $user_input['user_type'] === 'company' ){
				Flash::error('Il titolare di un attività non può essere minorenne');
				return redirect(route('shops.create'))->withInput();
			} else {
				$user_input['birthdate']  = $user_birth;
			}
		}

		try {
			$user = User::Create($user_input);
		} catch (\Exception $e) {
			if(stristr($e->getMessage(), 'users_email_unique')){
				Flash::error('Attenzione! L\'email inserita &egrave; gi&agrave; presente nel database');
			} else {
				Flash::error('Attenzione! ricontrollare i dati:'.$e->getMessage());
			}
			return redirect(route('shops.create'))->withInput();

		}
		//User subscription
		$user_subscription['user_id'] = $user->id;
		$user_subscription['subscription'] = $input['subscription'];
		$user_subscription['expire_date'] = Carbon::now()->addDays(15);
		$user_subscription['submission_date'] = Carbon::now();
		UserSubscription::Create($user_subscription);
		//Insert newshop 
        $shop_input['user_id'] = $user->id;
        $shop_input['agent_id'] = isset($input['agent_id']) ? $input['agent_id'] : null;
        $shop_input['description'] = $input['description'];
        $shop_input['name'] = $input['shop_name'];
        $shop_input['address'] = $input['address'];
        $shop_input['country'] = $input['country'];
        $shop_input['region'] = $input['region'];
        $shop_input['city'] = $input['city'];
        $shop_input['prov'] = $input['prov'];
        $shop_input['route'] = $input['route'];
        $shop_input['street_number'] = $input['street_number'];
        $shop_input['postal_code'] = $input['postal_code'];
        $shop_input['lat'] = $input['lat'];
        $shop_input['long'] = $input['long'];
        $shop_input['whatsapp'] = $input['whatsapp'];
		$shop_input['website'] = $input['website'];
        $shop_input['shop_status'] = $input['shop_status'];
        $shop_input['shop_status_orders'] = $input['shop_status_orders'];
        $shop_input['closed_date_start'] = $input['closed_date_start'];
        $shop_input['closed_date_end'] = $input['closed_date_end'];
        $shop_input['admin_first_password_generated'] = $input['new_password'];
		//Created by admin
        $shop_input['admin_generated'] = 1;
		//-->Timetables handlers
		$opening_timetable = [];
        $delivery_timetable = [];

        $days = ['mon' => 'monday',
                 'tue' => 'tuesday',
                 'wed' => 'wednesday',
                 'thu' => 'thursday',
                 'fri' => 'friday',
                 'sat' => 'saturday',
                 'sun' => 'sunday'];


        foreach($days as $shortday => $day):

            $opening_timetable [$day] = [];

            if( isset($input['open_'.$day.'_active']) && $input['open_'.$day.'_active'] === 'on' ){
                $opening_timetable[$day]['active'] = true;
                $opening_timetable[$day]['first_half'] = [];
                $opening_timetable[$day]['first_half']['opening_time'] = $input['open_'.$shortday.'_mor_open'];
                $opening_timetable[$day]['first_half']['closing_time'] = $input['open_'.$shortday.'_mor_close'];

                $opening_timetable[$day]['second_half'] = [];
                $opening_timetable[$day]['second_half']['opening_time'] = $input['open_'.$shortday.'_aft_open'];
                $opening_timetable[$day]['second_half']['closing_time'] = $input['open_'.$shortday.'_aft_close'];
            }else{
                $opening_timetable[$day]['active'] = false;

                $opening_timetable[$day]['first_half'] = [];
                $opening_timetable[$day]['first_half']['opening_time'] = null;
                $opening_timetable[$day]['first_half']['closing_time'] = null;

                $opening_timetable[$day]['second_half'] = [];
                $opening_timetable[$day]['second_half']['opening_time'] = null;
                $opening_timetable[$day]['second_half']['closing_time'] = null;

            }

            if( isset($input['delivery_'.$day.'_active']) && $input['delivery_'.$day.'_active'] === 'on' ){
                $delivery_timetable [$day] = [];
                $delivery_timetable[$day]['active'] = true;
                $delivery_timetable[$day]['first_half'] = [];
                $delivery_timetable[$day]['first_half']['opening_time'] = $input['delivery_'.$shortday.'_mor_open'];
                $delivery_timetable[$day]['first_half']['closing_time'] = $input['delivery_'.$shortday.'_mor_close'];

                $delivery_timetable[$day]['second_half'] = [];
                $delivery_timetable[$day]['second_half']['opening_time'] = $input['delivery_'.$shortday.'_aft_open'];
                $delivery_timetable[$day]['second_half']['closing_time'] = $input['delivery_'.$shortday.'_aft_close'];
            }else{
                $delivery_timetable [$day] = [];
                $delivery_timetable[$day]['active'] = false;
                $delivery_timetable[$day]['first_half'] = [];
                $delivery_timetable[$day]['first_half']['opening_time'] = null;
                $delivery_timetable[$day]['first_half']['closing_time'] = null;

                $delivery_timetable[$day]['second_half'] = [];
                $delivery_timetable[$day]['second_half']['opening_time'] = null;
                $delivery_timetable[$day]['second_half']['closing_time'] = null;
            }
			
		endforeach;
		//<--  Timetables handlers
        $shop_input['opening_timetable'] = json_encode($opening_timetable);;
        $shop_input['delivery_timetable'] = json_encode($delivery_timetable);
		$shop_input['delivery_address'] = $input['delivery_address'];
		$shop_input['delivery_address_country'] = $input['delivery_address_country'];
		$shop_input['delivery_address_region'] = $input['delivery_address_region'];
		$shop_input['delivery_address_city'] = $input['delivery_address_city'];
		$shop_input['delivery_address_route'] = $input['delivery_address_route'];
		$shop_input['delivery_address_street_number'] = $input['delivery_address_street_number'];
		$shop_input['delivery_address_postal_code'] = $input['delivery_address_postal_code'];
		$shop_input['delivery_address_lat'] = $input['delivery_address_lat'];
		$shop_input['delivery_address_long'] = $input['delivery_address_long'];
		$shop_input['delivery_range_km'] = $input['delivery_range_km'];
		$shop_input['delivery_range_notes'] = $input['delivery_range_notes'];
		$shop_input['delivery_host_on_site'] = $input['delivery_host_on_site'];
		$shop_input['delivery_cancellable_hours_limit'] = !empty($input['delivery_cancellable_hours_limit']) ? $input['delivery_cancellable_hours_limit'] : 1;
		$shop_input['min_trust_points_percentage_bookable'] = $input['min_trust_points_percentage_bookable'];
		$shop_input['tax_business_name'] = !empty($input['tax_business_name']) ? $input['tax_business_name'] : '';
		$shop_input['tax_type_id'] = empty($input['tax_type_id']) ? 12 : $input['tax_type_id'];
		$shop_input['tax_vat'] = $input['tax_vat'];
		$shop_input['tax_fiscal_code'] = $input['tax_fiscal_code'];
		$shop_input['tax_code'] = $input['tax_code'];
		$shop_input['tax_pec'] = $input['tax_pec'];
		$shop_input['tax_address'] = $input['tax_address'];
		$shop_input['tax_address_street_number'] = $input['tax_address_street_number'];
		$shop_input['tax_address_postal_code'] = $input['tax_address_postal_code'];
		$shop_input['tax_address_country'] = $input['tax_address_country'];
		$shop_input['tax_address_region'] = $input['tax_address_region'];
		$shop_input['tax_address_prov'] = $input['tax_address_prov'];
		$shop_input['tax_address_city'] = $input['tax_address_city'];
		$shop_input['tax_address_lat'] = $input['tax_address_lat'];
		$shop_input['tax_address_long'] = $input['tax_address_long'];
		try{
			$shop = Shop::Create($shop_input);
		} catch(\Exception $e){ 
			$user->delete();
			Flash::error('Attenzione! Si è verificato il seguente errore '.$e->getMessage().' ricontrollare i dati:'.$e->getMessage());
			return redirect(route('shops.create'))->withInput();
		}
		if(!empty($shop)){
			
			//-->ADD expire date to free user subscription if agent_id is set
			$subscription_product_num = 0;
			if(!empty($shop_input['agent_id']) && $shop->user->subscription->subscription == 'free'){
				$new_expire_date = Carbon::parse($shop->user->subscription->submission_date)->addMonth();
				$subscription_product_num = 100;
			} else {
				$new_expire_date = $shop->user->subscription->submission_date;
			}
			UserSubscription::where('user_id', $shop->user_id)->update(['expire_date' => $new_expire_date, 'subscription_product_num' => $subscription_product_num]);
			//<--
			
			//Store image
            $filename = null;

            $imageFullPath = null;
            if( !empty($input['output_image_data']) && !empty($input['output_image_type']) ):
                $folderPath = public_path('storage/images/shops/'.$shop->id);
                if( !File::isDirectory($folderPath) ) {
					try {
                    	File::makeDirectory($folderPath, 0775, true); //creates directory
					} catch(\Exception $e){
						$user->delete();
						$shop->delete();
						Flash::error('Attenzione! Si è verificato il seguente errore '.$e->getMessage().' '.$folderPath.' ricontrollare i dati:'.$e->getMessage());
						return redirect(route('shops.create'))->withInput();
					}
                }



                $image = $input['output_image_data'];
                $image_parts = explode(";base64,", $image);
                $image_type_aux = explode("image/", $image_parts[0]);
                $image_type = $image_type_aux[1];
                $image_base64 = base64_decode($image_parts[1]);

                $image_info = explode("/", $input['output_image_type']);
                $extension = $image_info[1];

                $filename = 'avatar.'.$extension;
			
                $imageFullPath = $folderPath.'/'.$filename;
                file_put_contents($imageFullPath, $image_base64);
	            $image = Image::make($imageFullPath);

                cropImages($image, $imageFullPath, 'user');

                $url = "/storage/images/shops/$shop->id";
                $imageFullPath = $url.'/'.$filename;
            endif;
				
			if( $filename ):
				$user->image_url = url($imageFullPath); //url( 'storage/images/users/'.$shop->user_id . '/' . $filename );
				$user->update();
            endif;

			if(!empty($input['shop_type_id'])){
				foreach($input['shop_type_id'] as $shop_type_id):
					$shopType = $this->shopTypeRepository->find($shop_type_id);
					try{
						$shop->shopTypes()->attach($shopType);
					}catch(\Exception $e){

					}
				endforeach;
			}
		
			/** Shop Payment Methods */
            $payment_methods_shop = $this->paymentMethodShopRepository->paymentMethods($shop->id);

            if( $payment_methods_shop ):
                foreach($payment_methods_shop as $pm):
                    if( !in_array($pm->payment_method_id, $input['payment_method_id']) ):
                        $pm->delete();
                    endif;
                endforeach;
            endif;

            $payment_methods_shop = $this->paymentMethodShopRepository->paymentMethods($shop->id)->pluck('payment_method_id')->toArray();

            if( isset($input['payment_method_id']) ):
                foreach( $input['payment_method_id'] as $pm ):
                    if( !in_array($pm, $payment_methods_shop) ):
                        $this->paymentMethodShopRepository->create([
                            'shop_id' => $shop->id,
                            'payment_method_id' => $pm
                        ]);
                    endif;
                endforeach;
            endif;


            /** Delivery Payment Methods */
            $payment_methods_delivery = $this->paymentMethodShopRepository->paymentMethods($shop->id, 'delivery');


            foreach($payment_methods_delivery as $pm):
                if( !in_array($pm->payment_method_id, $input['delivery_payment_method_id']) ):
                    $pm->delete();
                endif;
            endforeach;

            $payment_methods_delivery = $this->paymentMethodShopRepository->paymentMethods($shop->id, 'delivery')->pluck('payment_method_id')->toArray();

            if( isset($input['delivery_payment_method_id']) ):
                foreach( $input['delivery_payment_method_id'] as $pm ):
                    if( !in_array($pm, $payment_methods_delivery) ):
                        $this->paymentMethodShopRepository->create([
                            'shop_id' => $shop->id,
                            'payment_method_id' => $pm,
                            'payment_for' => 'delivery'
                        ]);
                    endif;
                endforeach;
            endif;
		}		
		$shops = $this->shopRepository->all([], null, null, ['*'], 'name');

        Flash::success('Negozio creato con successo!');
        return redirect(route('shops.index'));

		/*
		echo '<pre>';
		print_r($input);exit;
		echo '</pre>';
		*/
	}

    /**
     * Display the specified Shop.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $shop = $this->shopRepository->find($id);

        if (empty($shop)) {
            Flash::error('Shop not found');

            return redirect(route('shops.index'));
        }

        return view('shops.show')->with('shop', $shop);
    }

    /**
     * Show the form for editing the specified Shop.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {

        $shop = $this->shopRepository->find($id);
        $user = $this->userRepository->find($shop->user_id);
        $update = [];

        if( $user->user_type === 'company' ):
            if( empty($user->shop->opening_timetable) ):
                $update['opening_timetable'] = '{"monday":{"active":false,"first_half":{"opening_time":null,"closing_time":null},"second_half":{"opening_time":null,"closing_time":null}},"tuesday":{"active":false,"first_half":{"opening_time":null,"closing_time":null},"second_half":{"opening_time":null,"closing_time":null}},"wednesday":{"active":false,"first_half":{"opening_time":null,"closing_time":null},"second_half":{"opening_time":null,"closing_time":null}},"thursday":{"active":false,"first_half":{"opening_time":null,"closing_time":null},"second_half":{"opening_time":null,"closing_time":null}},"friday":{"active":false,"first_half":{"opening_time":null,"closing_time":null},"second_half":{"opening_time":null,"closing_time":null}},"saturday":{"active":false,"first_half":{"opening_time":null,"closing_time":null},"second_half":{"opening_time":null,"closing_time":null}},"sunday":{"active":false,"first_half":{"opening_time":null,"closing_time":null},"second_half":{"opening_time":null,"closing_time":null}}}';
            endif;

            if( empty($user->shop->delivery_opening_timetable) ):
                $update['delivery_opening_timetable'] = '{"monday":{"active":false,"first_half":{"opening_time":null,"closing_time":null},"second_half":{"opening_time":null,"closing_time":null}},"tuesday":{"active":false,"first_half":{"opening_time":null,"closing_time":null},"second_half":{"opening_time":null,"closing_time":null}},"wednesday":{"active":false,"first_half":{"opening_time":null,"closing_time":null},"second_half":{"opening_time":null,"closing_time":null}},"thursday":{"active":false,"first_half":{"opening_time":null,"closing_time":null},"second_half":{"opening_time":null,"closing_time":null}},"friday":{"active":false,"first_half":{"opening_time":null,"closing_time":null},"second_half":{"opening_time":null,"closing_time":null}},"saturday":{"active":false,"first_half":{"opening_time":null,"closing_time":null},"second_half":{"opening_time":null,"closing_time":null}},"sunday":{"active":false,"first_half":{"opening_time":null,"closing_time":null},"second_half":{"opening_time":null,"closing_time":null}}}';
            endif;

            if( !empty($update) ):
                $this->shopRepository->update($update, $user->shop->id);
                $user = $this->userRepository->find($shop->user_id);
            endif;
        endif;

		if(!empty($user)){
			if(!empty($user->subscription)){
				$user->subscription = $user->subscription;
				$user->subscription->max_producs = UserSubscription::$subscriptions[$user->subscription->subscription];
				//Days to expiration date
				$subscription_end = new Carbon($user->subscription->expire_date);
				$days_left = $subscription_end->diffInDays();
				$hours_left = $subscription_end->diffInHours();
				$user->subscription->expiration_date_info = [
											'in_days' => $days_left,
											'in_hours' => $hours_left,
                                         ];
			}
			if(!empty($user->shop)){
                $user->shop = $user->shop;
                $user->shop_types = ShopType::orderBy('name')->pluck('name', 'id')->toArray();
                $user->payment_method = PaymentMethod::all()->pluck('name', 'id')->toArray();
                $user->tax_type = TaxType::orderBy('name')->get()->pluck('name', 'id')->toArray();
			}
        }

        $res = $this->subscriptionRepository->all();

        $subscriptions = [];
        foreach($res as $sub):
            $subscriptions[$sub->subscription] = $sub;
        endforeach;

        $other_shops_same_subscription = $this->shopRepository->otherShopsSameSubscription($shop->id);
        $other_shops = $this->shopRepository->otherShops($shop->id, $other_shops_same_subscription->pluck('id')->toArray());

        $shopsInSubscription = $this->userSubscriptionRepository->shopsInSubscription($user->subscription->id);


        $agents = $this->agentRepository->all([], null, null, ['id', 'name'], 'name')->pluck('name', 'id')->toArray();
		$field_required = true;
		if($user->shop->admin_generated == 1):
			$field_required = false;
		endif;
		return view('shops.edit')->with('user', $user)
                                 ->with('agents', $agents)
                                 ->with('subscriptions', $subscriptions)
                                             ->with('other_shops', $other_shops)
                                             ->with('other_shops_same_subscription', $other_shops_same_subscription)
                                             ->with('shops_in_subscription', $shopsInSubscription)
                                             ->with('field_required', $field_required)
                                ;

    }

    public function editSubscription($shop_id){
        $shop = $this->shopRepository->find($shop_id);
        $user = $this->userRepository->find($shop->user_id);

        if(!empty($user)){
			if(!empty($user->subscription)){
				$user->subscription = $user->subscription;
				$user->subscription->max_producs = UserSubscription::$subscriptions[$user->subscription->subscription];
				//Days to expiration date
				$subscription_end = new Carbon($user->subscription->expire_date);
				$days_left = $subscription_end->diffInDays();
				$hours_left = $subscription_end->diffInHours();
				$user->subscription->expiration_date_info = [
											'in_days' => $days_left,
											'in_hours' => $hours_left,
                                         ];
			}
			if(!empty($user->shop)){
                $user->shop = $user->shop;
                $user->shop_types = ShopType::orderBy('name')->pluck('name', 'id')->toArray();
                $user->payment_method = PaymentMethod::all()->pluck('name', 'id')->toArray();
                $user->tax_type = TaxType::orderBy('name')->get()->pluck('name', 'id')->toArray();
			}
        }

        $res = $this->subscriptionRepository->all()->sortBy('position');

        $subscriptions = [];
        foreach($res as $sub):
            $subscriptions[$sub->subscription] = $sub;
        endforeach;

        $other_shops_same_subscription = $this->shopRepository->otherShopsSameSubscription($shop->id);
        $other_shops = $this->shopRepository->otherShops($shop->id, $other_shops_same_subscription->pluck('id')->toArray());

        $shopsInSubscription = $this->userSubscriptionRepository->shopsInSubscription($user->subscription->id);

        //dump($shopsInSubscription);
        //dd($subscriptions);
		
		$num_product_default = true;
		//Check if there's a number of product subscription exception
		if($user->subscription->subscription_product_num != 0){
			$num_product_default = false;
		}

        return view('shops.edit_subscription')->with('user', $user)
                                             ->with('subscriptions', $subscriptions)
                                             ->with('num_product_default', $num_product_default)
                                             ->with('other_shops', $other_shops)
                                             ->with('other_shops_same_subscription', $other_shops_same_subscription)
                                             ->with('shops_in_subscription', $shopsInSubscription);
    }

     /**
     * Disable the specified Shop.
     *
     * @param int $id
     *
     * @return Response
     */
    public function disable($id)
    {
        $shop = $this->shopRepository->update(["status"=>"inactive"], $id);

        if( $shop ){
            $user = $this->userRepository->find($shop->user_id);
            $this->userRepository->update(["status"=>"inactive"], $id);
        }

        return redirect(route('shops.index'));
    }

    /**
     * Enable the specified Shop.
     *
     * @param int $id
     *
     * @return Response
     */
    public function enable($id)
    {
        $shop = $this->shopRepository->update(["status"=>"active"], $id);

        if( $shop ){
            $user = $this->userRepository->find($shop->user_id);
            $this->userRepository->update(["status"=>"active"], $id);
        }

        return redirect(route('shops.index'));
    }

    public function updateSubscription(Request $request){

        $input = $request->all();

        if( !isset($input['shop_id']) ){
            Flash::error('Negozio non trovato');
            return redirect(route('shops.index'));
        }
		
		$shop = $this->shopRepository->find($input['shop_id']);
		
		//Update personalized number of product 
		UserSubscription::where('user_id', $shop->user_id)->update(['subscription_product_num' => $input['subscription_product_num']]);

        if( $shop ){

            /** Registrazione Transazione */
            if( !empty($input['subscription_transaction_price']) && !empty($input['subscription_transaction_desc']) ){

                $invoice_id = Invoice::count() + 1;

                $invoice = new Invoice();
                $invoice->title = $input['subscription_transaction_desc'];
                $invoice->price = toFloat($input['subscription_transaction_price']);
                $invoice->method_of_payment = $input['subscription_transaction_mop'];
                $invoice->payment_status = 'Completed';
                $invoice->user_id = $shop->user->id;
                $invoice->save();
                Flash::success('Transazione salvata!');
            }else{
                if( (empty($input['subscription_transaction_price']) && !empty($input['subscription_transaction_desc'])) ||
                    (!empty($input['subscription_transaction_price']) && empty($input['subscription_transaction_desc'])) ){
                        Flash::error('Devi compilare tutti i dati della transazione');
                }
            }

            /** Modifica abbonamento */
            $subscription = $old_subscription = $shop->user->subscription;

            $new_subscription = $this->subscriptionRepository->findBySubscription($input['subscription_id']);
            $shopsSameSubscription = $this->shopRepository->otherShopsSameSubscription($shop->id);

            $future_num_shops = $shopsSameSubscription->count() + count($input['otherShops'] ?? []) - count($input['removeShops'] ?? []) + 1;

            /*
            dump($shopsSameSubscription->count());
            dump(count($input['otherShops'] ?? []));
            dump(count($input['removeShops'] ?? []));
            dump($future_num_shops);
            dd($new_subscription->shops_num);
            */

            if( $future_num_shops > $new_subscription->shops_num ):
                Flash::error('Il numero delle attività è superiore a quello permesso dal tuo abbonamento');
                return redirect(route('shopSubscription.edit', ['shop_id' => $shop->id]));
			endif;
			$submission_date = explode(' ', $shop->user->subscription->submission_date)[0];
			
			if(empty($input['subscription_date'])){
				$expire_date = $shop->user->subscription->expire_date;
				if(($old_subscription->subscription == 'free') && ($input['subscription_id'] == 'standard')){//Passo da basic a standard
					$expire_date = Carbon::createFromFormat('Y-m-d', $submission_date)->addMonth();
				}
				if(($input['subscription_id'] == 'free') && ($old_subscription->subscription != 'free')){//Ritorno a un abbonamento basic
					$expire_date = $shop->user->subscription->submission_date;
				}
			} else {
				$expire_date = $input['subscription_date'];
			}

            $subscription->subscription = $input['subscription_id'];
            $subscription->expire_date = $expire_date;
            $subscription->save();

            if( isset($input['removeShops']) ):
                foreach($input['removeShops'] as $idRemoveShop):
                    $removeShop = $this->shopRepository->find($idRemoveShop);
                    $removeSubscription = $removeShop->user->subscription;
                    $removeSubscription->expire_date = Carbon::now();
                    $removeSubscription->parent = null;
                    $removeSubscription->subscription = 'free';
                    $removeSubscription->save();
                endforeach;
            endif;

            if( isset($input['otherShops']) ):

                /*
                //Scollego tutte le attività collegate allo stesso abbonamento
                $shopsSameSubscription = $this->shopRepository->otherShopsSameSubscription($shop->id);
                foreach($shopsSameSubscription as $otherShop):
                    $subscription = $otherShop->user->subscription;
                    $subscription->expire_date = Carbon::now();
                    $subscription->parent = null;
                    $subscription->subscription = 'trial';
                    $subscription->save();
                endforeach;
                */

                foreach($input['otherShops'] as $otherShop):
                    $oShop = $this->shopRepository->find($otherShop);
                    $oSubscription = $oShop->user->subscription;
                    $oSubscription->subscription = $shop->user->subscription->subscription;
                    $oSubscription->parent = $shop->user->subscription->id;
                    $oSubscription->expire_date = $shop->user->subscription->expire_date;
                    $oSubscription->save();
                endforeach;
            endif;
        }

        Flash::success('Abbonamento salvato!');
        return redirect(route('shopSubscription.edit', ['shop_id' => $shop->id]));
    }

    /**
     * Update the specified Shop in storage.
     *
     * @param int $id
     * @param UpdateProfileCompanyRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProfileCompanyRequest $request)
    {
        $input = $request->all();
		//Get shop to check some values
	
		$shopData = [
            'user_id' => $input['user_id'],
            'agent_id' => isset($input['agent_id']) ? $input['agent_id'] : null,
            //'shop_type_id' => $input['shop_type_id'],
            'name' => $input['shop_name'],
            'website' => $input['website'],
            'address' => $input['address'],
            'country' => $input['country'],
            'route' => $input['route'],
            'city' => $input['city'],
            'prov' => $input['prov'],
            'region' => $input['region'],
            'postal_code' => $input['postal_code'],
            'street_number' => $input['street_number'],
            'lat' => $input['lat'],
            'long' => $input['long'],
            'whatsapp' => $input['whatsapp'],
            'show_phone_on_app' => isset($input['show_phone_on_app']) ? 1 : 0,
            'facebook_page' => $input['facebook_page'],
            'instagram_page' => $input['instagram_page'],
            'description' => $input['description'],
            'opening_timetable' => $input['opening_timetable'],
            'delivery_address' => $input['delivery_address'],
            'delivery_address_country' => $input['delivery_address_country'],
            'delivery_address_route' => $input['delivery_address_route'],
            'delivery_address_city' => $input['delivery_address_city'],
            'delivery_address_region' => $input['delivery_address_region'],
            'delivery_address_postal_code' => $input['delivery_address_postal_code'],
            'delivery_address_street_number' => $input['delivery_address_street_number'],
            'delivery_address_lat' => $input['delivery_address_lat'],
            'delivery_address_long' => $input['delivery_address_long'],
            'delivery_opening_timetable' => $input['delivery_opening_timetable'],
            'home_delivery' => $input['home_delivery'],
            'home_delivery_min' => isset($input['home_delivery_min']) ? tofloat($input['home_delivery_min']) : null,
            'delivery_range_km' => $input['delivery_range_km'],
            'delivery_range_notes' => $input['delivery_range_notes'],
            'delivery_host_on_site' => 0, //$input['delivery_host_on_site'],
            //'delivery_always_free' => ( isset($input['delivery_home_fees']) && $input['delivery_home_fees'] === 'delivery_always_free' ) ? 1 : 0,
            'delivery_forfait_cost_price' => isset($input['delivery_forfait_cost_price']) ? tofloat($input['delivery_forfait_cost_price']) : null,
            'delivery_free_price_greater_than' => isset($input['delivery_free_price_greater_than']) ? tofloat($input['delivery_free_price_greater_than']) : null,
            'delivery_percentage_cost_price' => isset($input['delivery_percentage_cost_price']) ? $input['delivery_percentage_cost_price'] : null,
            'delivery_cancellable_hours_limit' => $input['delivery_cancellable_hours_limit'],
            'min_trust_points_percentage_bookable' => $input['min_trust_points_percentage_bookable'],
            'tax_business_name' => !empty($input['tax_business_name']) ? $input['tax_business_name'] : '',
            'tax_type_id' => $input['tax_type_id'],
            'tax_vat' => $input['tax_vat'],
            'tax_fiscal_code' => $input['tax_fiscal_code'],
            'tax_code' => $input['tax_code'],
            'tax_pec' => $input['tax_pec'],
            'tax_address' => $input['tax_address'],
            'tax_address_country' => $input['tax_address_country'],
            'tax_address_route' => $input['tax_address_route'],
            'tax_address_city' => $input['tax_address_city'],
            'tax_address_prov' => $input['tax_address_prov'],
            'tax_address_region' => $input['tax_address_region'],
            'tax_address_postal_code' => $input['tax_address_postal_code'],
            'tax_address_street_number' => $input['tax_address_street_number'],
            'tax_address_lat' => $input['tax_address_lat'],
            'tax_address_long' => $input['tax_address_long'],
            'shop_status' => $input['shop_status'],
			'admin_test_shop' => $input['admin_test_shop'],
            'shop_status_orders' => $input['shop_status_orders'],
            'closed_date_start' => ($input['shop_status'] === 'Aperto' ? null : $input['closed_date_start']),
            'closed_date_end' => ($input['shop_status'] === 'Aperto' ? null : $input['closed_date_end'])
        ];

        $shop = $this->shopRepository->update($shopData, $id);


        if( $shop ):

            $shop->shopTypes()->detach();
            foreach($input['shop_type_id'] as $shop_type_id):
                $shopType = $this->shopTypeRepository->find($shop_type_id);
                try{
                    $shop->shopTypes()->attach($shopType);
                }catch(\Exception $e){

                }
            endforeach;

            /** Shop Payment Methods */
            $payment_methods_shop = $this->paymentMethodShopRepository->paymentMethods($shop->id);

            if( $payment_methods_shop ):
                foreach($payment_methods_shop as $pm):
                    if( !in_array($pm->payment_method_id, $input['payment_method_id']) ):
                        $pm->delete();
                    endif;
                endforeach;
            endif;

            $payment_methods_shop = $this->paymentMethodShopRepository->paymentMethods($shop->id)->pluck('payment_method_id')->toArray();

            if( isset($input['payment_method_id']) ):
                foreach( $input['payment_method_id'] as $pm ):
                    if( !in_array($pm, $payment_methods_shop) ):
                        $this->paymentMethodShopRepository->create([
                            'shop_id' => $shop->id,
                            'payment_method_id' => $pm
                        ]);
                    endif;
                endforeach;
            endif;


            /** Delivery Payment Methods */
            $payment_methods_delivery = $this->paymentMethodShopRepository->paymentMethods($shop->id, 'delivery');


            foreach($payment_methods_delivery as $pm):
                if( !in_array($pm->payment_method_id, $input['delivery_payment_method_id']) ):
                    $pm->delete();
                endif;
            endforeach;

            $payment_methods_delivery = $this->paymentMethodShopRepository->paymentMethods($shop->id, 'delivery')->pluck('payment_method_id')->toArray();

            if( isset($input['delivery_payment_method_id']) ):
                foreach( $input['delivery_payment_method_id'] as $pm ):
                    if( !in_array($pm, $payment_methods_delivery) ):
                        $this->paymentMethodShopRepository->create([
                            'shop_id' => $shop->id,
                            'payment_method_id' => $pm,
                            'payment_for' => 'delivery'
                        ]);
                    endif;
                endforeach;
            endif;

            /*
            $image = null;
            $filename = null;

            if( $request->hasFile('fileimage') ):
                $path = public_path('storage/images/users/'.$shop->user_id);

                if( !File::isDirectory($path) ) {
                    File::makeDirectory($path, 0775, true); //creates directory
                }

                $image = Image::make($request->file('fileimage')->getRealPath())->orientate();
                $info = pathinfo( $request->file('fileimage')->getClientOriginalName() );
                $filename = 'avatar.'.$info['extension'];
            endif;
            */

            $filename = null;

            $imageFullPath = null;
            if( !empty($input['output_image_data']) && !empty($input['output_image_type']) ):
                $folderPath = public_path('storage/images/shops/'.$shop->id);

                if( !File::isDirectory($folderPath) ) {
                    File::makeDirectory($folderPath, 0775, true); //creates directory
                }



                $image = $input['output_image_data'];
                $image_parts = explode(";base64,", $image);
                $image_type_aux = explode("image/", $image_parts[0]);
                $image_type = $image_type_aux[1];
                $image_base64 = base64_decode($image_parts[1]);

                $image_info = explode("/", $input['output_image_type']);
                $extension = $image_info[1];

                $filename = 'avatar.'.$extension;

                $imageFullPath = $folderPath.'/'.$filename;
                file_put_contents($imageFullPath, $image_base64);

                $image = Image::make($imageFullPath);
                cropImages($image, $imageFullPath, 'user');

                $url = "/storage/images/shops/$shop->id";
                $imageFullPath = $url.'/'.$filename;
            endif;

            $userData = [
                'name' => $input['user_name'],
                'email' => $input['email'],
                'birthdate' => $input['user_birthdate'],
				'phone' => $input['phone']
            ];

            if( $filename ):
                $userData['image_url'] = url($imageFullPath); //url( 'storage/images/users/'.$shop->user_id . '/' . $filename );
            endif;

            if( $input['new_password'] && !empty($input['new_password']) && $input['email'] !== 'demo@demo.it'):
                $new_password = Hash::make($input['new_password']);
                $userData['password'] = $new_password;
            endif;

            if ( $this->userRepository->update($userData, $shop->user_id) ):

                if( isset($userData['password']) ):
                    if (env('APP_ENV') === 'production'):
                        $shop->user->notify(new PasswordChanged($input['new_password']));
                    endif;
                endif;

                /*
                if( $image ):

                    /** Create and store image and image crops *
                    $path = public_path('storage/images/users/'.$shop->user_id.'/'.$filename );
                    if( $image->save($path) ):
                        cropImages($image, $path,'user');
                    else:
                        Flash::error('Errore durante il salvataggio immagine');
                        return redirect(route('shops.edit', ['shop' => $shop->id]));
                    endif;

                endif;
                */
            endif;
			/*
			$message_sent_email = '';
			//Send email
			if(empty($shop_sent_email) && !empty($input['admin_sent_email_shop_created'])):
				$user = User::find($input['user_id']);
        		Mail::to($user->email)->send(new \App\Mail\SendEMailAdminShopRegistered($user, $shop));
				if(count(Mail::failures()) > 0){
					Flash::error('Ci sono stati problem con l\'invio email');
				}
				$message_sent_email = '(Email inviata a '.$user->email.')';
			endif;
			*/




        endif;
		
		//-->ADD expire date to free user subscription if agent_id is set
		$subscription_product_num = 0;
		if(!empty($input['agent_id']) && $shop->user->subscription->subscription == 'free'){
			$new_expire_date = Carbon::parse($shop->user->subscription->submission_date)->addMonth();
			$subscription_product_num = 100;
		} else {
			$new_expire_date = $shop->user->subscription->submission_date;
		}
		UserSubscription::where('user_id', $shop->user_id)->update(['expire_date' => $new_expire_date, 'subscription_product_num' => $subscription_product_num]);
		//<--
		
        session(['current_tab' => intval($input['current_tab'])]);

        Flash::success('Profilo salvato! ');
        return redirect(route('shops.edit', ['shop' => $shop->id]));
    }

    /**
     * Remove the specified Shop from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $shop = $this->shopRepository->find($id);

        if (empty($shop)) {
            Flash::error('Shop not found');

            return redirect(route('shops.index'));
        }

        $user = $shop->user;
		//Delete shop_offers
		DB::table('shop_offers')->where('shop_id', $shop->id)->delete();
        $shop->delete();
        $user->delete();

        return response()->json(['status' => 'ok']);


    }

    /**
     * Get the list of recently searched shope.
     *
     * @param void
     *
     * @return Response
     */
    public function recentlySearched()
    {
    }


    public function getCategoryTypes($shop_id, $food_category_id){

        $food_types = DB::table('shop_catalogs as c')
                        ->where('c.shop_id', '=', $shop_id)
                        ->where('c.food_category_id', '=', $food_category_id)
                        ->whereNull('deleted_at')
                        ->select('c.food_type')
                        ->distinct('c.food_type')
                        ->orderBy('c.food_type')
                        ->get()
                        ->pluck('food_type')
                        ;

        return response()->json(['count' => count($food_types), 'food_types' => $food_types]);

    }

    public function invisible(){
        if( Auth::user()->user_type === 'company' ){
            $shop = Auth::user()->shop;
            $shop->status = 'invisible';
            $shop->save();

            $user = Auth::user();
            $user->status = 'invisible';
            $user->save();
        }
        return redirect()->back();
    }

    public function visible(){
        if( Auth::user()->user_type === 'company' ){
            $shop = Auth::user()->shop;
            $shop->status = 'active';
            $shop->save();

            $user = Auth::user();
            $user->status = 'active';
            $user->save();
        }
        return redirect()->back();
    }

    public function publishRequest() {
        Mail::to('assistenza@primascelta.biz')->send(new \App\Mail\PublishRequest(Auth::user()));
        return redirect()->back()->with('message', 'La tua richiesta di pubblicazione è stata inviata correttamente!');
    }

	//Send registration Email by Admin
	public function sendAdminRegistrationEmail($id) {
		$message_sent_email = '';
		$shop = $this->shopRepository->find($id);
		//Send email
		$user = User::find($shop->user_id);
		Mail::to($user->email)->send(new \App\Mail\SendEMailAdminShopRegistered($user, $shop));
		if(count(Mail::failures()) > 0){
			Flash::error('Ci sono stati problem con l\'invio email');
		}
		$message_sent_email = 'Email inviata a '.$user->email;
        Flash::success($message_sent_email);
		//Update flag Shop sent admin email
		$shop->admin_sent_email_shop_created = 1;
		$shop->update();
        return redirect(route('shops.edit', ['shop' => $shop->id]));
	}
}

