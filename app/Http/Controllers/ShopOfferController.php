<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Http\Requests\CreateShopOfferRequest;
use App\Http\Requests\UpdateShopOfferRequest;
use App\Repositories\ShopOfferRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\ShopOfferFood;
use App\Repositories\FoodCategoryRepository;
use App\Repositories\UnitOfMeasureRepository;
use App\Repositories\ShopCatalogRepository;
use App\Repositories\ShopOfferFoodRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Flash;
use Response;

class ShopOfferController extends AppBaseController
{
    /** @var  ShopOfferRepository */
    private $shopOfferRepository;
    private $shopOfferFoodRepository;
    private $foodCategoryRepository;
    private $unitOfMeasureRepository;
    private $shopCatalogRepository;

    public function __construct(ShopOfferRepository $shopOfferRepository, ShopOfferFoodRepository $shopOfferFoodRepository, FoodCategoryRepository $foodCategoryRepository, UnitOfMeasureRepository $unitOfMeasureRepository, ShopCatalogRepository $shopCatalogRepository)
    {
        $this->shopOfferRepository = $shopOfferRepository;
        $this->shopOfferFoodRepository = $shopOfferFoodRepository;
        $this->foodCategoryRepository = $foodCategoryRepository;
        $this->unitOfMeasureRepository = $unitOfMeasureRepository;
        $this->shopCatalogRepository = $shopCatalogRepository;
    }

    /**
     * Display a listing of the ShopOffer.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $shopOffers = $this->shopOfferRepository->list(Auth::user()->shop->id);

        return view('shop_offers.index')
            ->with('shopOffers', $shopOffers);
    }

    /**
     * Show the form for creating a new ShopOffer.
     *
     * @return Response
     */
    public function create()
    {
        $foodCategoryList = $this->shopCatalogRepository->getFoodCategoryList();
        $foodCategoryList[''] = 'Seleziona...';
        ksort($foodCategoryList);

        $foodCategoryList = $foodCategoryList + ['SELECT_ALL' => 'Mostra tutti i prodotti'];

        $unitOfMeasureList = $this->unitOfMeasureRepository->all([], null, null, ['*'], 'name')->pluck('name', 'id')->toArray();

        return view('shop_offers.create')->with(['foodCategoryList' => $foodCategoryList,
                                                    'foodTypeList' => [],
                                                    'unitOfMeasureList' => $unitOfMeasureList
                                                ]);

    }

    /**
     * Store a newly created ShopOffer in storage.
     *
     * @param CreateShopOfferRequest $request
     *
     * @return Response
     */
    public function store(CreateShopOfferRequest $request)
    {
        $input = $request->all();

        $new_shop_offer = [
            'shop_id' => $input['shop_id'],
            'name' => $input['name'],
            'description' => $input['description'],
            'start_date' => $input['start_date'],
            'end_date' => $input['end_date'],
            'count_purchased' => 0
        ];

        if( isset($input['save']) ){
            $new_shop_offer['status'] = 'draft';
        }elseif( isset($input['save_available']) ){
            $new_shop_offer['status'] = 'available';
        }

        //dd($input);

        $shopOffer = $this->shopOfferRepository->create($new_shop_offer);

        if( $shopOffer ):

            foreach( $input['shop_catalog_id'] as $k => $catalog_id ):

                $shopCatalog = $this->shopCatalogRepository->find($catalog_id);

                $new_shop_offer_food = [
                    'shop_offer_id' => $shopOffer->id,
                    'shop_catalog_id' => $catalog_id,
                    'food_name' => $shopCatalog->food_name,
                    'food_description' => $shopCatalog->food_description,
                    'food_image' => $shopCatalog->food_image,
                    'min_quantity' => $input['min_quantity'][$k],
                    'discount' => $input['discount'][$k],
                    'discounted_price' => $input['discounted_price'][$k]
                ];

                $food_offer_validator = Validator::make($new_shop_offer_food, ShopOfferFood::$rules, ShopOfferFood::$messages);
                if($food_offer_validator->fails()){
                    return back()->withInput()->withErrors($food_offer_validator);
                }

                $this->shopOfferFoodRepository->create($new_shop_offer_food);

            endforeach;

        endif;

        Flash::success('Offerta creata con successo.');

        return redirect(route('shopOffers.index'));
    }

    /**
     * Display the specified ShopOffer.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $shopOffer = $this->shopOfferRepository->find($id);

        if (empty($shopOffer)) {
            Flash::error('Offerta non trovata');

            return redirect(route('shopOffers.index'));
        }

        return view('shop_offers.show')->with('shopOffer', $shopOffer);
    }

    /**
     * Show the form for editing the specified ShopOffer.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $shopOffer = $this->shopOfferRepository->find($id);

        if (empty($shopOffer)) {
            Flash::error('Offerta non trovata');
            return redirect(route('shopOffers.index'));
        }

        $foodCategoryList = $this->shopCatalogRepository->getFoodCategoryList();
        $foodCategoryList[''] = 'Seleziona...';
        ksort($foodCategoryList);

        $foodCategoryList = $foodCategoryList + ['SELECT_ALL' => 'Mostra tutti i prodotti'];

        $unitOfMeasureList = $this->unitOfMeasureRepository->all([], null, null, ['*'], 'name')->pluck('name', 'id')->toArray();

        return view('shop_offers.edit')->with('shopOffer', $shopOffer)
                                        ->with('foodCategoryList', $foodCategoryList)
                                        ->with('foodTypeList', [])
                                        ->with('unitOfMeasureList', $unitOfMeasureList)
                                        ;
    }

    /**
     * Update the specified ShopOffer in storage.
     *
     * @param int $id
     * @param UpdateShopOfferRequest $request
     *
     * @return Response
     */
    public function update($shop_offer_id, UpdateShopOfferRequest $request)
    {

        $input = $request->all();

        $shopOffer = $this->shopOfferRepository->find($shop_offer_id);

        if (empty($shopOffer)) {
            Flash::error('Offerta non trovata');
            return redirect(route('shopOffers.index'));
        }

        /*
        if( $shopOffer->status === 'available' && $shopOffer->start_date->isPast() ):
            return back()->withInput()->withErrors('Non puoi salvare una offerta attiva con una data inizio passata');
        endif;
        */

        $update_shop_offer = [
            'shop_id' => $input['shop_id'],
            'name' => $input['name'],
            'description' => $input['description'],
            'start_date' => $input['start_date'],
            'end_date' => $input['end_date'],
            'count_purchased' => 0
        ];

        $shopOffer = $this->shopOfferRepository->update($update_shop_offer, $shop_offer_id);

        if( $shopOffer ):

            foreach( $input['shop_offer_food_id'] as $k => $shop_offer_food_id ):

                if( $shop_offer_food_id ):
                    $shopOfferFood = $this->shopOfferFoodRepository->find($shop_offer_food_id);

                    if( $shopOfferFood ):

                        $shopCatalog = $this->shopCatalogRepository->find($input['shop_catalog_id'][$k]);

                        $food_offer_validator = Validator::make([
                            'shop_offer_id' => $shopOffer->id,
                            'shop_catalog_id' => $input['shop_catalog_id'][$k],
                            'food_name' => $shopCatalog->food_name,
                            'food_description' => $shopCatalog->food_description,
                            'food_image' => $shopCatalog->food_image,
                            'min_quantity' => $input['min_quantity'][$k],
                            'discount' => $input['discount'][$k],
                            'discounted_price' => $input['discounted_price'][$k]
                        ], ShopOfferFood::$rules, ShopOfferFood::$messages);
                        if($food_offer_validator->fails()){
                            return back()->withInput()->withErrors($food_offer_validator);
                        }

                        $shopOfferFood->shop_catalog_id = $input['shop_catalog_id'][$k];
                        $shopOfferFood->food_name = $shopCatalog->food_name;
                        $shopOfferFood->food_image = $shopCatalog->food_image;
                        $shopOfferFood->food_description = $shopCatalog->food_description;
                        $shopOfferFood->min_quantity = $input['min_quantity'][$k];
                        $shopOfferFood->discount = $input['discount'][$k];
                        $shopOfferFood->discounted_price = $input['discounted_price'][$k];
                        $shopOfferFood->save();
                    endif;

                else:

                    $shopCatalog = $this->shopCatalogRepository->find($input['shop_catalog_id'][$k]);

                    $new_shop_offer_food = [
                        'shop_offer_id' => $shopOffer->id,
                        'shop_catalog_id' => $shopCatalog->id,
                        'food_name' => $shopCatalog->food_name,
                        'food_description' => $shopCatalog->food_description,
                        'food_image' => $shopCatalog->food_image,
                        'min_quantity' => $input['min_quantity'][$k],
                        'discount' => $input['discount'][$k],
                        'discounted_price' => $input['discounted_price'][$k]
                    ];

                    $food_offer_validator = Validator::make($new_shop_offer_food, ShopOfferFood::$rules, ShopOfferFood::$messages);
                    if($food_offer_validator->fails()){
                        return back()->withInput()->withErrors($food_offer_validator);
                    }

                    $this->shopOfferFoodRepository->create($new_shop_offer_food);

                endif;

            endforeach;

        endif;

        Flash::success('Offerta modificata con successo.');
        return redirect(route('shopOffers.index'));
    }

    /**
     * Remove the specified ShopOffer from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $shopOffer = $this->shopOfferRepository->find($id);

        if (empty($shopOffer)) {
            return response()->json(['status' => 'error', 'msg' => "Non è stata trovata l'offerta"]);
        }

        if( $shopOffer->start_date->isFuture() ||
            $shopOffer->status !== 'available' ||
            ($shopOffer->status === 'available' && $shopOffer->end_date->addDays(1)->isPast())):
            $this->shopOfferRepository->delete($id);
        else:
            return response()->json(['status' => 'error', 'msg' => "Non puoi eliminare questa offerta"]);
        endif;

        return response()->json(['status' => 'ok']);
    }


    public function status($id){
        $shopOffer = $this->shopOfferRepository->find($id);

        if( $shopOffer->shop_id == Auth::user()->shop->id ):

            if( $shopOffer->status === 'draft' ):
                $shopOffer->status = 'available';
            elseif( $shopOffer->status === 'available' ):
                $shopOffer->status = 'cancelled';
            elseif( $shopOffer->status === 'cancelled' ):
                $shopOffer->status = 'available';
            endif;

            $shopOffer->save();

            return redirect(route('shopOffers.index'));
        else:
            Flash::error('Operazione non consentita');
            return redirect(route('shopOffers.index'));
        endif;
    }

}
