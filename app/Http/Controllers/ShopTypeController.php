<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateShopTypeRequest;
use App\Http\Requests\UpdateShopTypeRequest;
use App\Repositories\ShopTypeRepository;
use App\Http\Controllers\AppBaseController;
use App\Repositories\ShopRepository;
use Illuminate\Http\Request;
use Flash;
use Response;

class ShopTypeController extends AppBaseController
{
    /** @var  ShopTypeRepository */
    private $shopTypeRepository, $shopRepository;

    public function __construct(ShopTypeRepository $shopTypeRepo, ShopRepository $shopRepository)
    {
        $this->shopTypeRepository = $shopTypeRepo;
        $this->shopRepository = $shopRepository;
    }

    /**
     * Display a listing of the ShopType.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $shopTypes = $this->shopTypeRepository->all();

        return view('shop_types.index')
            ->with('shopTypes', $shopTypes);
    }

    /**
     * Show the form for creating a new ShopType.
     *
     * @return Response
     */
    public function create()
    {
        return view('shop_types.create');
    }

    /**
     * Store a newly created ShopType in storage.
     *
     * @param CreateShopTypeRequest $request
     *
     * @return Response
     */
    public function store(CreateShopTypeRequest $request)
    {
        $input = $request->all();

        $shopType = $this->shopTypeRepository->create($input);

        Flash::success('Tipologia negozio creata con successo.');

        return redirect(route('shopTypes.index'));
    }

    /**
     * Display the specified ShopType.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $shopType = $this->shopTypeRepository->find($id);

        if (empty($shopType)) {
            Flash::error('Shop Type not found');

            return redirect(route('shopTypes.index'));
        }

        return view('shop_types.show')->with('shopType', $shopType);
    }

    /**
     * Show the form for editing the specified ShopType.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $shopType = $this->shopTypeRepository->find($id);

        if (empty($shopType)) {
            Flash::error('Shop Type not found');

            return redirect(route('shopTypes.index'));
        }

        return view('shop_types.edit')->with('shopType', $shopType);
    }

    /**
     * Update the specified ShopType in storage.
     *
     * @param int $id
     * @param UpdateShopTypeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateShopTypeRequest $request)
    {
        $shopType = $this->shopTypeRepository->find($id);

        if (empty($shopType)) {
            Flash::error('Shop Type not found');

            return redirect(route('shopTypes.index'));
        }

        $shopType = $this->shopTypeRepository->update($request->all(), $id);

        Flash::success('Tipologia negozio modificata con successo.');

        return redirect(route('shopTypes.index'));
    }

    /**
     * Remove the specified ShopType from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $shopType = $this->shopTypeRepository->find($id);

        if (empty($shopType)) {
            Flash::error('Shop Type not found');
            return redirect(route('shopTypes.index'));
        }

        $count = $shopType->shops->count();
        if ( $count > 0 ) {
            Flash::error('Non puoi eliminare questa tipologia. E\' associata ad almeno un negozio');
            return redirect(route('shopTypes.index'));
        }

        $this->shopTypeRepository->delete($id);
        Flash::success('Tipologia negozio eliminata con successo.');

        return redirect(route('shopTypes.index'));
    }
}
