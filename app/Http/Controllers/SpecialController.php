<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\SpecialRepository;

class SpecialController extends Controller
{
    private $specialRepository;

    public function __construct(SpecialRepository $specialRepository)
    {
        $this->specialRepository = $specialRepository;
    }

    public function index(){
        $specials = $this->specialRepository->allSpecials();
        return view('specials.index')->with('specials', $specials);
    }

    public function create(){
        return view('specials.create');
    }

    public function store(Request $request){

        $input = $request->except("_token");

        $special = $this->specialRepository->create($input);
        if( $special ):
            return redirect(route('specials.index'))->withMessage(['msg' => 'Caratteristica salvata con successo!']);
        else:
            return redirect(route('specials.index'))->withErrors(['msg' => 'Errore nel salvataggio']);
        endif;

    }

    public function edit($id){
        $special = $this->specialRepository->find($id);
        return view('specials.edit')->with('special', $special);
    }

    public function update($id, Request $request){

        $input = $request->except("_token");

        $special = $this->specialRepository->find($id)->update($input);

        if( $special ):
            return redirect(route('specials.index'))->withMessage(['msg' => 'Caratteristica salvata con successo!']);
        else:
            return redirect(route('specials.index'))->withErrors(['msg' => 'Errore nel salvataggio']);
        endif;

    }

    public function destroy($id){
        $special = $this->specialRepository->find($id);
        if( $special ):
            if( $special->delete() ):
                return redirect(route('specials.index'))->withMessage(['msg' => 'Caratteristica eliminata con sucesso']);
            else:
                return redirect(route('specials.index'))->withErrors(['msg' => 'Errore nella cancellazione']);
            endif;
        else:
            return redirect(route('specials.index'))->withErrors(['msg' => 'Caratteristica non trovata!']);
        endif;
    }
}
