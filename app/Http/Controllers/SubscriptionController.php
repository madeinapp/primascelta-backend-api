<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateSubscriptionRequest;
use App\Models\SubscriptionFeature;
use App\Repositories\SubscriptionRepository;
use Illuminate\Http\Request;
use Flash;
use Auth;

class SubscriptionController extends Controller
{

    private $subscriptionRepository;

    public function __construct(SubscriptionRepository $subscriptionRepository)
    {
        $this->subscriptionRepository = $subscriptionRepository;
    }

    public function prices(){
        if( empty(Auth::user()->subscription->parent) && Auth::user()->shop  ):
            $subscriptions = $this->subscriptionRepository->all()->sortBy('position');
            return view('subscriptions.prices')->with('subscriptions', $subscriptions);
        else:
            if( empty(Auth::user()->shop->tax_vat) ):
                return redirect('home')->withErrors(['msg' => 'Prima di modificare o acquistare il tuo abbonamento devi inserire la tua Partita IVA. Visita il tuo profilo nella sezione Profilo fiscale.']);
            else:
                return redirect('home')->withErrors(['msg' => 'Non puoi modificare il tuo abbonamento con questa utenza! Devi autenticarti con: ' . Auth::user()->subscription->parentSubscription->user->email]);
            endif;
        endif;
    }

    public function index(){
        $subscriptions = $this->subscriptionRepository->all()->sortBy('position');;
        return view('subscriptions.index')->with('subscriptions', $subscriptions);
    }

    public function edit($subscription_id){
        $subscription = $this->subscriptionRepository->find($subscription_id);
        return view('subscriptions.edit')->with('subscription', $subscription);
    }

    public function update(UpdateSubscriptionRequest $request){
        $input = $request->all();
        $subscription = $this->subscriptionRepository->update($input, $input['subscription_id']);
        if( $subscription ){
            Flash::success('Abbonamento modificato con successo');
        }else{
            Flash::error('Si è verificato un errore nel salvataggio dell\'abbonamento');
        }
        return redirect(route('subscriptions.index'));
    }

    public function getFeature($subscription_feature_id){
        $subscriptionFeature = SubscriptionFeature::find($subscription_feature_id);
        if( $subscriptionFeature ):
            return response()->json(['status' => 'ok', 'subscriptionFeature' => $subscriptionFeature]);
        else:
            return response()->json(['status' => 'error']);
        endif;
    }

    public function featureStore(Request $request){
        $input = $request->except('_token');

        $subscriptionFeature = new SubscriptionFeature();
        $subscriptionFeature->subscription_id = $input['subscription_id'];
        $subscriptionFeature->feature_name = $input['feature_name'];
        $subscriptionFeature->feature_note = $input['feature_note'];
        $subscriptionFeature->order = $input['order'];
        $subscriptionFeature->save();

        return response()->json(['status' => 'ok', 'subscriptionFeature' => $subscriptionFeature]);
    }

    public function featureUpdate($subscription_feature_id, Request $request){
        $input = $request->except('_token');

        $subscriptionFeature = SubscriptionFeature::find($subscription_feature_id);
        $subscriptionFeature->subscription_id = $input['subscription_id'];
        $subscriptionFeature->feature_name = $input['feature_name'];
        $subscriptionFeature->feature_note = $input['feature_note'];
        $subscriptionFeature->order = $input['order'];
        $subscriptionFeature->save();

        return response()->json(['status' => 'ok', 'subscriptionFeature' => $subscriptionFeature]);
    }

    public function deleteFeature($subscription_feature_id){
        $subscriptionFeature = SubscriptionFeature::find($subscription_feature_id);
        if( $subscriptionFeature ):
            $subscriptionFeature->delete();
            return response()->json(['status' => 'ok']);
        else:
            return response()->json(['status' => 'error']);
        endif;
    }

}
