<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateUnitOfMeasureRequest;
use App\Http\Requests\UpdateUnitOfMeasureRequest;
use App\Repositories\UnitOfMeasureRepository;
use App\Http\Controllers\AppBaseController;
use App\Repositories\FoodCategoryRepository;
use App\Repositories\FoodRepository;
use App\Repositories\ShopCatalogRepository;
use Illuminate\Http\Request;
use Flash;
use Response;
use Auth;

class UnitOfMeasureController extends AppBaseController
{
    /** @var  UnitOfMeasureRepository */
    private $unitOfMeasureRepository,
            $foodRepository,
            $foodCategoryRepository,
            $shopCatalogRepository;

    public function __construct(UnitOfMeasureRepository $unitOfMeasureRepo,
                                FoodRepository $foodRepository,
                                FoodCategoryRepository $foodCategoryRepository,
                                ShopCatalogRepository $shopCatalogRepository)
    {
        $this->unitOfMeasureRepository = $unitOfMeasureRepo;
        $this->foodRepository = $foodRepository;
        $this->shopCatalogRepository = $shopCatalogRepository;
        $this->foodCategoryRepository = $foodCategoryRepository;
    }

    /**
     * Display a listing of the UnitOfMeasure.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $unitOfMeasures = $this->unitOfMeasureRepository->all([], null, null, ['*'], 'name');
        return view('unit_of_measures.index')
            ->with('unitOfMeasures', $unitOfMeasures);
    }

    /**
     * Show the form for creating a new UnitOfMeasure.
     *
     * @return Response
     */
    public function create()
    {
        $unitOfMeasures = $this->unitOfMeasureRepository->all([], null, null, ['*'], 'name')->pluck('name', 'id');
        return view('unit_of_measures.create')->with('unitOfMeasures', $unitOfMeasures);
    }

    /**
     * Store a newly created UnitOfMeasure in storage.
     *
     * @param CreateUnitOfMeasureRequest $request
     *
     * @return Response
     */
    public function store(CreateUnitOfMeasureRequest $request)
    {
        $input = $request->all();

        $unitOfMeasure = $this->unitOfMeasureRepository->create($input);

        Flash::success('Unità di misura creata con successo.');

        return redirect(route('unitOfMeasures.index'));
    }

    /**
     * Display the specified UnitOfMeasure.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $unitOfMeasure = $this->unitOfMeasureRepository->find($id);

        if (empty($unitOfMeasure)) {
            Flash::error('Unit Of Measure not found');

            return redirect(route('unitOfMeasures.index'));
        }

        return view('unit_of_measures.show')->with('unitOfMeasure', $unitOfMeasure);
    }

    /**
     * Show the form for editing the specified UnitOfMeasure.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $unitOfMeasure = $this->unitOfMeasureRepository->find($id);

        if (empty($unitOfMeasure)) {
            Flash::error('Unit Of Measure not found');

            return redirect(route('unitOfMeasures.index'));
        }

        $unitOfMeasures = $this->unitOfMeasureRepository->all([], null, null, ['*'], 'name')->pluck('name', 'id');

        return view('unit_of_measures.edit')->with('unitOfMeasure', $unitOfMeasure)
                                            ->with('unitOfMeasures', $unitOfMeasures);
    }

    /**
     * Update the specified UnitOfMeasure in storage.
     *
     * @param int $id
     * @param UpdateUnitOfMeasureRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUnitOfMeasureRequest $request)
    {
        $unitOfMeasure = $this->unitOfMeasureRepository->find($id);

        if (empty($unitOfMeasure)) {
            Flash::error('Unit Of Measure not found');

            return redirect(route('unitOfMeasures.index'));
        }

        $unitOfMeasure = $this->unitOfMeasureRepository->update($request->all(), $id);

        Flash::success('Unità di misura modificata con successo.');

        return redirect(route('unitOfMeasures.index'));
    }

    /**
     * Remove the specified UnitOfMeasure from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $unitOfMeasure = $this->unitOfMeasureRepository->find($id);

        if (empty($unitOfMeasure)) {
            return response()->json(['status' => 'error'], 400);
        }

        $foodCategories = $this->foodCategoryRepository->byUnitOfMeasure($id)->count();
        $foods = $this->foodRepository->byUnitOfMeasure($id)->count();
        $shopCatalogs = $this->shopCatalogRepository->byUnitOfMeasure($id)->count();
        $shopCatalogsBuy = $this->shopCatalogRepository->byUnitOfMeasureBuy($id)->count();

        if ( $foodCategories > 0 || $foods > 0 || $shopCatalogs > 0 || $shopCatalogsBuy > 0 ) {
            return response()->json(['status' => 'associated',
                                     'foodCategories' => $foodCategories,
                                     'foods' => $foods,
                                     'shopCatalogs' => $shopCatalogs,
                                     'shopCatalogsBuy' => $shopCatalogsBuy
                                     ]);
        }

        $this->unitOfMeasureRepository->delete($id);
        return response()->json(['status' => 'ok']);
    }
}
