<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\UpdateProfileRequest;
use App\Http\Requests\UpdateProfileCompanyRequest;
use App\Repositories\UserRepository;
use App\Repositories\ShopRepository;
use App\Repositories\ShopTypeRepository;
use App\Models\FoodCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Flash;
use Response;
use Hash;
use Auth;
use File;
use Image;
use URL;
use App\Models\User;
use App\Models\UserSubscription;
use App\Models\PaymentMethod;
use App\Models\ShopType;
use App\Models\TaxType;
use App\Notifications\CompanyEnabled;
use App\Notifications\PrivateEnabled;
use App\Notifications\CompanyDisabled;
use App\Notifications\CompanyRegistered;
use App\Notifications\PrivateDisabled;
use App\Notifications\PrivateRegistered;
use App\Notifications\PasswordChanged;
use App\Repositories\PaymentMethodShopRepository;
use Carbon\Carbon;
use Exception;
use Mail;

class UserController extends AppBaseController
{
    private $userRepository, $shopRepository, $paymentMethodShopRepository;

    public function __construct(UserRepository $userRepository,
                                ShopRepository $shopRepository,
                                ShopTypeRepository $shopTypeRepository,
                                PaymentMethodShopRepository $paymentMethodShopRepository)
    {
        $this->userRepository = $userRepository;
        $this->shopRepository = $shopRepository;
        $this->shopTypeRepository = $shopTypeRepository;
        $this->paymentMethodShopRepository = $paymentMethodShopRepository;
    }

    /**
     * Display a listing of the User.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var User $users */
        $users = User::where("role","user")
                     //->where("user_type","private")
                     ->get();

        return view('users.index')
            ->with('users', $users);
    }

    /**
     * Disable the specified Shop.
     *
     * @param int $id
     *
     * @return Response
     */
    public function disable($id)
    {
        $user = $this->userRepository->find($id);
        if( $user ):
            $this->userRepository->update(['status' => 'inactive'], $user->id);

            if( $user->shop ):
                $shop = $this->shopRepository->find($user->shop->id);
                if( $shop ):
                    $this->shopRepository->update(['status' => 'inactive'], $user->shop->id);

                    if (env('APP_ENV') === 'production'):
                        // Send notification
                        $user->notify(new CompanyDisabled());
                    endif;
                endif;
            else:
                if (env('APP_ENV') === 'production'):
                    // Send notification
                    $user->notify(new PrivateDisabled());
                endif;
            endif;
        endif;

        return back();
    }

    /**
     * Enable the specified Shop.
     *
     * @param int $id
     *
     * @return Response
     */
    public function enable($id)
    {
        $user = $this->userRepository->find($id);
        if( $user ):
            $this->userRepository->update(['status' => 'active'], $user->id);

            if( $user->shop ):
                $shop = $this->shopRepository->find($user->shop->id);
                if( $shop ):
                    $this->shopRepository->update(['status' => 'active'], $user->shop->id);

                    if (env('APP_ENV') === 'production'):
                        // Send notification
                        $user->notify(new CompanyEnabled());
                    endif;
                endif;
            else:
                if (env('APP_ENV') === 'production'):
                    // Send notification
                    $user->notify(new PrivateEnabled());
                endif;
            endif;
        endif;

        return back();
    }

    /**
     * Show the form for creating a new User.
     *
     * @return Response
     */
    public function create()
    {
        $arr_user_type = [];
        $obj_user_type = User::getUserType();

        foreach($obj_user_type as $k => $v){
                $arr_user_type[$k] = __("common.$v");
        }
        $arr_user_status = [];
        $obj_user_status = User::getUserStatus();

        foreach($obj_user_status as $k => $v){
                $arr_user_status[$k] = __("common.$v");
        }
        return view('users.create')->with(['arr_user_type' => $arr_user_type, 'arr_user_status' => $arr_user_status] );
    }

    /**
     * Store a newly created User in storage.
     *
     * @param CreateUserRequest $request
     *
     * @return Response
     */
    public function store(CreateUserRequest $request)
    {
        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        /** @var User $user */
        $user = User::create($input);

        Flash::success('Utente creato con successo.');

        return redirect(route('users.index'));
    }

    /**
     * Display the specified User.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var User $user */
        $user = User::find($id);

        if (empty($user)) {
            Flash::error('User not found');

            return redirect(route('users.index'));
        }

        return view('users.show')->with('user', $user);
    }

    /**
     * Show the form for editing the specified User.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $user = $this->userRepository->find($id);
        return view('users.edit')->with('user', $user);

    }

    /**
     * Update the specified User in storage.
     *
     * @param int $id
     * @param UpdateUserRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUserRequest $request)
    {
        $input = $request->all();

        if( isset($input['save']) ):
            $userData = [
                'name' => $input['name'],
                'email' => $input['email']
            ];

            if( !empty($input['new_password']) && $input['email'] !== 'demo@demo.it'):
                $userData['password'] = Hash::make($input['new_password']);
            endif;
        elseif( isset($input['del-photo']) ):
            $userData = [];
            $userData['image_url'] = null;
        endif;

        if( $this->userRepository->update($userData, $id) ):

            if( isset($userData['password']) ):
                if (env('APP_ENV') === 'production'):
                    $this->userRepository->find($id)->notify(new PasswordChanged($input['new_password']));
                endif;
            endif;

            Flash::success('Profilo salvato!');

            //$previous = app('router')->getRoutes()->match(app('request')->create(URL::previous()))->getName();

            return redirect(route('users.index'));
        endif;
    }

    /**
     * Remove the specified User from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var User $user */
        $user = User::find($id);

        if (empty($user)) {
            Flash::error('User not found');

            return redirect(route('users.index'));
        }

        $user->delete();

        Flash::success('Utente eliminato con successo.');

        return redirect(route('users.index'));
    }

    /**
     * User Detail .
     * private
     *
     * @param User $user
     * @return User
     */
	public function editProfile()
	{
        $id = Auth::user()->id;
        $user = $this->userRepository->find($id);

        $update = [];

        if( $user->user_type === 'company' ):
            if( empty($user->shop->opening_timetable) ):
                $update['opening_timetable'] = '{"monday":{"active":false,"first_half":{"opening_time":null,"closing_time":null},"second_half":{"opening_time":null,"closing_time":null}},"tuesday":{"active":false,"first_half":{"opening_time":null,"closing_time":null},"second_half":{"opening_time":null,"closing_time":null}},"wednesday":{"active":false,"first_half":{"opening_time":null,"closing_time":null},"second_half":{"opening_time":null,"closing_time":null}},"thursday":{"active":false,"first_half":{"opening_time":null,"closing_time":null},"second_half":{"opening_time":null,"closing_time":null}},"friday":{"active":false,"first_half":{"opening_time":null,"closing_time":null},"second_half":{"opening_time":null,"closing_time":null}},"saturday":{"active":false,"first_half":{"opening_time":null,"closing_time":null},"second_half":{"opening_time":null,"closing_time":null}},"sunday":{"active":false,"first_half":{"opening_time":null,"closing_time":null},"second_half":{"opening_time":null,"closing_time":null}}}';
            endif;

            if( empty($user->shop->delivery_opening_timetable) ):
                $update['delivery_opening_timetable'] = '{"monday":{"active":false,"first_half":{"opening_time":null,"closing_time":null},"second_half":{"opening_time":null,"closing_time":null}},"tuesday":{"active":false,"first_half":{"opening_time":null,"closing_time":null},"second_half":{"opening_time":null,"closing_time":null}},"wednesday":{"active":false,"first_half":{"opening_time":null,"closing_time":null},"second_half":{"opening_time":null,"closing_time":null}},"thursday":{"active":false,"first_half":{"opening_time":null,"closing_time":null},"second_half":{"opening_time":null,"closing_time":null}},"friday":{"active":false,"first_half":{"opening_time":null,"closing_time":null},"second_half":{"opening_time":null,"closing_time":null}},"saturday":{"active":false,"first_half":{"opening_time":null,"closing_time":null},"second_half":{"opening_time":null,"closing_time":null}},"sunday":{"active":false,"first_half":{"opening_time":null,"closing_time":null},"second_half":{"opening_time":null,"closing_time":null}}}';
            endif;

            if( !empty($update) ):
                $this->shopRepository->update($update, $user->shop->id);
                $user = $this->userRepository->find($id);
            endif;
        endif;


		if(!empty($user)){
			if(!empty($user->subscription)){
				$user->subscription = $user->subscription;
				$user->subscription->max_producs = UserSubscription::$subscriptions[$user->subscription->subscription];
				//Days to expiration date
				$subscription_end = new Carbon($user->subscription->expire_date);
				$days_left = $subscription_end->diffInDays();
				$hours_left = $subscription_end->diffInHours();
				$user->subscription->expiration_date_info = [
											'in_days' => $days_left,
											'in_hours' => $hours_left,
                                         ];
			}
			if(!empty($user->shop)){
                $user->shop = $user->shop;
                $user->shop_types = ShopType::orderBy('name')->pluck('name', 'id')->toArray();
                $user->payment_method = PaymentMethod::all()->pluck('name', 'id')->toArray();
                $user->tax_type = TaxType::orderBy('name')->get()->pluck('name', 'id')->toArray();
			}
        }
		$field_required = true;
        if( $user->user_type === 'company' ):
			if($user->shop->admin_generated == 1):
				$field_required = false;
			endif;
            return view('users.profile-company')->with(['user' => $user, 'field_required' => $field_required]);
        else:
            return view('users.profile')->with(['user' => $user, 'field_required' => $field_required]);
        endif;
    }

    public function updateProfile(UpdateProfileRequest $request){

        $input = $request->all();

        $user = $this->userRepository->find(Auth::id());

        $userData = [
            'name' => $input['name'],
            'email' => $input['email']
        ];

        if( !empty($input['new_password']) && $input['email'] !== 'demo@demo.it'):
            $userData['password'] = Hash::make($input['new_password']);
        endif;

        /** Check user age */
        if(isset($input['birthdate'])):
            $user_age = Carbon::parse($input['birthdate'])->age;
            if($user_age < 14 && $user->user_type === 'private' && $user->role === 'user'){
                return back()->withInput()->withErrors(['User age' => 'Devi avere almeno 14 anni per utilizzare questa app'], 'birthdate');
            }
        endif;

        if( $this->userRepository->update($userData, Auth::id()) ):
            Flash::success('Profilo salvato!');
            return redirect(route('profile.edit'));
        endif;


    }

    public function updateProfileCompany(UpdateProfileCompanyRequest $request){

        $input = $request->all();

        $shopData = [
            'user_id' => $input['user_id'],
            //'shop_type_id' => $input['shop_type_id'],
            'website' => $input['website'],
            'name' => $input['shop_name'],
            'address' => $input['address'],
            'country' => $input['country'],
            'route' => $input['route'],
            'city' => $input['city'],
            'prov' => $input['prov'],
            'region' => $input['region'],
            'postal_code' => $input['postal_code'],
            'street_number' => $input['street_number'],
            'lat' => $input['lat'],
            'long' => $input['long'],
            'whatsapp' => $input['whatsapp'],
            'show_phone_on_app' => isset($input['show_phone_on_app']) ? 1 : 0,
            'facebook_page' => $input['facebook_page'],
            'instagram_page' => $input['instagram_page'],
            'description' => $input['description'],
            'opening_timetable' => $input['opening_timetable'],
            'delivery_address' => $input['delivery_address'],
            'delivery_address_country' => $input['delivery_address_country'],
            'delivery_address_route' => $input['delivery_address_route'],
            'delivery_address_city' => $input['delivery_address_city'],
            'delivery_address_region' => $input['delivery_address_region'],
            'delivery_address_postal_code' => $input['delivery_address_postal_code'],
            'delivery_address_street_number' => $input['delivery_address_street_number'],
            'delivery_address_lat' => $input['delivery_address_lat'],
            'delivery_address_long' => $input['delivery_address_long'],
            'delivery_opening_timetable' => $input['delivery_opening_timetable'],
            'home_delivery' => $input['home_delivery'],
            'home_delivery_min' => isset($input['home_delivery_min']) ? tofloat($input['home_delivery_min']) : null,
            'delivery_range_km' => $input['delivery_range_km'],
            'delivery_range_notes' => $input['delivery_range_notes'],
            'delivery_host_on_site' => 0, //$input['delivery_host_on_site'],
            //'delivery_always_free' => ( isset($input['delivery_home_fees']) && $input['delivery_home_fees'] === 'delivery_always_free' ) ? 1 : 0,
            'delivery_forfait_cost_price' => isset($input['delivery_forfait_cost_price']) ? tofloat($input['delivery_forfait_cost_price']) : null,
            'delivery_free_price_greater_than' => isset($input['delivery_free_price_greater_than']) ? tofloat($input['delivery_free_price_greater_than']) : null,
            'delivery_percentage_cost_price' => isset($input['delivery_percentage_cost_price']) ? $input['delivery_percentage_cost_price'] : null,
            'delivery_cancellable_hours_limit' => $input['delivery_cancellable_hours_limit'],
            'min_trust_points_percentage_bookable' => $input['min_trust_points_percentage_bookable'],
            'tax_business_name' => !empty($input['tax_business_name']) ? $input['tax_business_name'] : '',
            'tax_type_id' => $input['tax_type_id'],
            'tax_vat' => $input['tax_vat'],
            'tax_fiscal_code' => $input['tax_fiscal_code'],
            'tax_code' => $input['tax_code'],
            'tax_pec' => $input['tax_pec'],
            'tax_address' => $input['tax_address'],
            'tax_address_country' => $input['tax_address_country'],
            'tax_address_route' => $input['tax_address_route'],
            'tax_address_city' => $input['tax_address_city'],
            'tax_address_prov' => $input['tax_address_prov'],
            'tax_address_region' => $input['tax_address_region'],
            'tax_address_postal_code' => $input['tax_address_postal_code'],
            'tax_address_street_number' => $input['tax_address_street_number'],
            'tax_address_lat' => $input['tax_address_lat'],
            'tax_address_long' => $input['tax_address_long'],
            'shop_status' => $input['shop_status'],
            'shop_status_orders' => $input['shop_status_orders'],
            'closed_date_start' => ($input['shop_status'] === 'Aperto' ? null : $input['closed_date_start']),
            'closed_date_end' => ($input['shop_status'] === 'Aperto' ? null : $input['closed_date_end'])
        ];

        //dd($shopData);

        $shop = $this->shopRepository->update($shopData, $input['shop_id']);

        if( $shop ):
            $shop->shopTypes()->detach();
            foreach($input['shop_type_id'] as $shop_type_id):
                $shopType = $this->shopTypeRepository->find($shop_type_id);
                try{
                    $shop->shopTypes()->attach($shopType);
                }catch(\Exception $e){

                }
            endforeach;

            /** Shop Payment Methods */
            $payment_methods_shop = $this->paymentMethodShopRepository->paymentMethods($shop->id);

            if( $payment_methods_shop ):
                foreach($payment_methods_shop as $pm):
                    if( !in_array($pm->payment_method_id, $input['payment_method_id']) ):
                        $pm->delete();
                    endif;
                endforeach;
            endif;

            $payment_methods_shop = $this->paymentMethodShopRepository->paymentMethods($shop->id)->pluck('payment_method_id')->toArray();

            if( isset($input['payment_method_id']) ):
                foreach( $input['payment_method_id'] as $pm ):
                    if( !in_array($pm, $payment_methods_shop) ):
                        $this->paymentMethodShopRepository->create([
                            'shop_id' => $shop->id,
                            'payment_method_id' => $pm
                        ]);
                    endif;
                endforeach;
            endif;

            /** Delivery Payment Methods */
            $payment_methods_delivery = $this->paymentMethodShopRepository->paymentMethods($shop->id, 'delivery');


            foreach($payment_methods_delivery as $pm):
                if( !in_array($pm->payment_method_id, $input['delivery_payment_method_id']) ):
                    $pm->delete();
                endif;
            endforeach;

            $payment_methods_delivery = $this->paymentMethodShopRepository->paymentMethods($shop->id, 'delivery')->pluck('payment_method_id')->toArray();

            if( isset($input['delivery_payment_method_id']) ):
                foreach( $input['delivery_payment_method_id'] as $pm ):
                    if( !in_array($pm, $payment_methods_delivery) ):
                        $this->paymentMethodShopRepository->create([
                            'shop_id' => $shop->id,
                            'payment_method_id' => $pm,
                            'payment_for' => 'delivery'
                        ]);
                    endif;
                endforeach;
            endif;



            /*
            if( $request->hasFile('fileimage') ):
                $path = public_path('storage/images/users/'.$shop->user_id);

                if( !File::isDirectory($path) ) {
                    File::makeDirectory($path, 0775, true); //creates directory
                }

                $image = Image::make($request->file('fileimage')->getRealPath())->orientate();
                $info = pathinfo( $request->file('fileimage')->getClientOriginalName() );
                $filename = 'avatar.'.$info['extension'];
            endif;
            */

            $imageFullPath = null;
            if( !empty($input['output_image_data']) && !empty($input['output_image_type']) ):
                $folderPath = public_path('storage/images/users/'.$shop->user_id);

                if( !File::isDirectory($folderPath) ) {
                    File::makeDirectory($folderPath, 0775, true); //creates directory
                }



                $image = $input['output_image_data'];
                $image_parts = explode(";base64,", $image);
                $image_type_aux = explode("image/", $image_parts[0]);
                $image_type = $image_type_aux[1];
                $image_base64 = base64_decode($image_parts[1]);

                $image_info = explode("/", $input['output_image_type']);
                $extension = $image_info[1];

                $filename = 'avatar.'.$extension;

                $imageFullPath = $folderPath.'/'.$filename;
                file_put_contents($imageFullPath, $image_base64);

                $image = Image::make($imageFullPath);
                cropImages($image, $imageFullPath, 'user');

                $url = "/storage/images/users/$shop->user_id";
                $imageFullPath = $url.'/'.$filename;
            endif;

            $userData = [
                'name' => $input['user_name'],
                'email' => $input['email'],
                'birthdate' => $input['user_birthdate'],
                'phone' => $input['phone'],
            ];

            if( $imageFullPath):
                $userData['image_url'] = url($imageFullPath);
            endif;

            if( $input['new_password'] && $input['email'] !== 'demo@demo.it'):
                $userData['password'] = Hash::make($input['new_password']);
            endif;


            if ( $this->userRepository->update($userData, $shop->user_id) ):
                /*
                if( $image ):

                    /** Create and store image and image crops
                    $path = public_path('storage/images/users/'.$shop->user_id.'/'.$filename );
                    if( $image->save($path) ):
                        cropImages($image, $path,'user');
                    else:
                        Flash::error('Error saving image');
                        return redirect(route('profile.edit'));
                    endif;

                endif;
                */
            endif;

        endif;

        session(['current_tab' => intval($input['current_tab'])]);

        Flash::success('Profilo salvato!');
        return redirect(route('profile.edit'));

    }

    public function testEmails(){

        $users = $this->userRepository->all(['email' => 'flavio.spugna@gmail.com']);
        if( $users ):
            $user = $users[0];

            $order = \App\Models\Order::find(36);

            $user->notify(new \App\Notifications\CompanyRegistered(Auth::user(), $order));

        endif;
        dd("OK");
    }

    public function genPwd($pwd){
        dump($pwd);
        dd(Hash::make($pwd));
    }

    public function cambiaPassword($email){
        $user = User::where('email', $email)->first();
        $user->password = Hash::make('12345678');
        $user->save();
        return redirect(route('home'));
    }

	public function privacyok(Request $request){
		$input = $request->all();
		if(!empty($input['user_id'])){
			$user = User::find($input['user_id']);
			$user->privacy = 1;
			$user->save();
			return true;
		}
		return false;
	}

}
