<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class CheckCompany
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check() && Auth::user()->user_type !== 'company') {
            return redirect('home');
        }

        return $next($request);
    }
}
