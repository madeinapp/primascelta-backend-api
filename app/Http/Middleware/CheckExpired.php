<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\UserSubscription;

class CheckExpired
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = auth()->user();
        if($user):
            if($user->status==='inactive'){
                return response()->json(['errors' => ['Inactive' => 'User is not active']], 400);
            }
            /*
            if(!empty($user->subscription->expire_date)){
                if(!$user->subscription->expire_date->isFuture()){
                    $user->api_token = '';
                    $user->save();
                    return response()->json(['errors' => ['Subscription' => 'Subscription is expired']], 400);
                }
            }
            */
            return $next($request);
        else:
            return response()->json(['errors' => ['Inactive' => 'User is not logged in']], 400);
        endif;
    }
}
