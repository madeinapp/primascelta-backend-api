<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Validator;

class CheckStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        //If the status is not approved redirect to login
        if(Auth::check()){

            $user = Auth::user()->toArray();

            $rules = ['status' => 'in:active,pending,invisible'];
            $messages = ['status.in' => 'Utente non attivo'];

            $validator = Validator::make($user, $rules, $messages);
            if( $validator->fails() ){
                Auth::logout();
                return redirect('login')->withErrors($validator);
            }
        }
        return $response;
    }
}
