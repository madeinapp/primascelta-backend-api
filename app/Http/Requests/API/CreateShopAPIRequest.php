<?php

namespace App\Http\Requests\API;

use App\Http\Requests\API\APIRequest;

use Auth;
use Log;

use App\Models\Shop;
use App\Models\User;
use App\Models\TaxType;

use App\Rules\StrongPassword;
use App\Rules\Timetable;

use Carbon\Carbon;

class CreateShopAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        if( isset($this->tax_type_id) && !empty($this->tax_type_id) ):
            $this->merge([
                'tax_type' => TaxType::find($this->tax_type_id)->name
            ]);
        endif;

        if( !isset($this->website) ):
            $this->merge([
                'website' => null
            ]);
        endif;

        /** Check user age */
        $user_age = Carbon::parse($this->user_birthdate)->age;
        $this->merge([
            'age' => $user_age
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'shop_type_id' => 'required|filled',
            'user_name' => 'required',
            'user_birthdate' => 'required|date',
            'shop_name' => 'required',
            'address' => 'required',
            'phone' => 'nullable|regex:/^[0-9]{8,}$/',
            'whatsapp' => 'required|nullable|regex:/^[0-9]{8,}$/',
            'email' => 'required|email',
            'tax_business_name' => 'required',
            'tax_type_id' => 'required',
            'tax_vat' => 'required|regex:/^[0-9]{11}$/',
            'tax_fiscal_code' => (strtolower($this->tax_type) === 'persona fisica') ? 'required|regex:/^([A-Z]{6}[0-9]{2}[A-Z]{1}[0-9]{2}[A-Z][0-9]{3}[A-Z])$/' : ['required', 'regex:/^([A-Z]{6}[0-9]{2}[A-Z]{1}[0-9]{2}[A-Z][0-9]{3}[A-Z])|(\d{11})$/'],
            'tax_address' => 'required',
            'tax_address_country' => 'required',
            'tax_address_route' => 'required',
            'tax_address_city' => 'required',
            'tax_address_prov' => 'required',
            'tax_address_region' => 'required',
            'tax_address_postal_code' => 'required',
            'tax_address_street_number' => 'required',
            'tax_address_lat' => 'required',
            'tax_address_long' => 'required',
            'age' => 'required|numeric|min:18'

        ];

        return $rules;

    }

    public function messages()
    {
        return [

            'shop_type_id.required' => 'Selezionare la tipologia attività',
            'shop_type_id.filled' => 'Selezionare almeno una tipologia attività',
            'website.url' => 'Inserire una url valida per il sito web es. http://www.nomesito.it',
            'user_name.required' => 'Il nome utente è obbligatorio',
            'user_birthdate.required' => 'La data di nascita è obbligatoria',
            'user_birthdate.date' => 'La data di nascita non è una data valida',
            'shop_name.required' => 'Devi inserire il nome dell\'attività',
            'address.required' => 'Inserire l\'indirizzo dell\'attività',
            'phone.regex' => 'Il telefono non è valido. Sono ammessi solo caratteri numerici senza alcuno spazio',
            'whatsapp.regex' => 'Il numero di cellulare non è valido. Sono ammessi solo caratteri numerici senza alcuno spazio',
            'whatsapp.required' => 'Inserire un numero di cellulare',
            'email.required' => 'L\'email è obbligatoria',
            'email.email' => 'Digitare un indirizzo email valido',
            'facebook_page.url' => 'Digitare una url valida per la pagina Facebook es. https://www.facebook.com/nomepagina',
            'instagram_page.url' => 'Digitare una url valida per la pagina Instagram es. https://www.instagram.com/nomepagina/',
            'description.required' => 'Inserire la descrizione dell\'attività',
            'new_password.confirmed' => 'La password di conferma non corrisponde',
            'new_password.min' => 'La password deve essere di almeno 6 caratteri',
            'tax_fiscal_code.required' => 'Inserire il Codice Fiscale',
            'tax_fiscal_code.regex' => 'Inserire un codice fiscale valido',
            'tax_vat.required' => 'Inserire la Partita IVA',
            'tax_vat.regex' => 'Inserire una partita IVA valida',
            'delivery_free_price_greater_than.regex' => 'Inserire un importo valido per l\'importo della consegna gratuita',
            'delivery_forfait_cost_price.regex' => 'Inserire un importo valido per i costi fissi di consegna',
            'subscription_transaction_price.required_with' => 'Inserire l\'importo della transazione',
            'subscription_transaction_desc.required_with' => 'Inserire la descrizione della transazione',
            'payment_method_ids.required' => 'Selezionare almeno un metodo di pagamento',
            'delivery_payment_method_ids.required_if' => 'Selezionare almeno un metodo di pagamento per la consegna',
            'age.min' => 'Il titolare di un attività non può essere minorenne'
        ];

    }
}
