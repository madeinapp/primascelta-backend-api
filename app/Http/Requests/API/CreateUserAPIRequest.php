<?php

namespace App\Http\Requests\API;

use App\Models\User;
use App\Models\Shop;
use App\Http\Requests\API\APIRequest;
use App\Rules\StrongPassword;

class CreateUserAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if(!auth()->check()){//Registration
            return [
                'email' => 'required|email|unique:users',
                'password' => ['required', 'confirmed', new StrongPassword],
                'facebook_id' => 'nullable|unique:users',
                'birthdate' => 'date|required'
            ];

        } else {//Update
            return [
                'password' =>  ['nullable', 'confirmed', new StrongPassword],
            ];
        }
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return array_merge([
                'email.required'  => 'L\'email è obbligatoria',
                'email.unique' => 'L\'email è già utilizzata',
                'birthdate.date' => 'La data di nascita non è corretta',
                'birthdate.required' => 'La data di nascita è obbligatoria',
                'password.required' => 'La password è obbligatoria',
                'facebook_id.unique' => 'L\'utente Facebook è già in uso',
                'password.confirmed' => 'La password di conferma non corrisponde',
                'password.min' => 'La password deve essere di almeno :min caratteri'
            ], Shop::$messages);
        }
}
