<?php

namespace App\Http\Requests\API;

use App\Models\User;
use App\Models\Shop;
use App\Models\ShopType;
use InfyOm\Generator\Request\APIRequest;
use App\Rules\StrongPassword;
use Log;

class CreateUserShopAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */

     public function withValidator($validator)
     {
        if($validator->fails()){
            $validator->errors()->add('Error', 'Something is wrong with some fields');
            echo $validator->errors()->toJson();//redirect()->route('login');//
            exit;
        }

        $validator->after(function ($validator) {
            //var_dump($this->all());exit;
        });
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome' => 'required|regex:/(\w.+\s).+/',
            'email' => 'required|email|unique:users',
            'tel' => 'required|regex:/^[0-9]{8,}$/',
            'negozio' => 'required|string|min:3',
            'citta' => 'required|string',
            //'street_number' => 'required|string',
            'route' => 'required|string|min:3',
            'locality' => 'required|string|min:2',
            'administrative_area_level_1' => 'required|string',
            'postal_code' => 'required|string|size:5',
            'country' => 'required|string|min:5',
            'lat' => 'required|numeric',
            'long' => 'required|numeric',
            'privacy_2' => 'required|string:on',
            'delivery' => 'required|numeric',
            'description' => 'required|numeric'
        ];

    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'nome.required'          => 'Nome e cognome sono obbligatori',
            'nome.regex'             => 'Digitare nome e cognome',
            'tel.required'           => 'Il telefono è obbligatorio',
            'tel.regex'              => 'Il telefono non è valido',
            'negozio.required'       => 'Il nome dell\'attività è obbligatorio',
            'negozio.min'            => 'Il nome dell\'attività non è valido',
            'citta.required'         => 'L\'indirizzo è obbligatorio',
            //'street_number.required' => 'Il numero civico è obbligatorio, verificare l\'indirizzo',
            'route.required'         => 'La via è obbligatoria',
            'locality.required'      => 'La città è obbligatoria, verificare l\'indirizzo',
            'locality.min'           => 'La città non è valida, verificare l\'indirizzo',
            'administrative_area_level_1.required' => 'Verificare l\'indirizzo',
            'postal_code.required'   => 'Il CAP è obbligatorio, verificare l\'indirizzo',
            'postal_code.size'       => 'Il CAP non è valido',
            'country.required'       => 'Verificare l\'indirizzo',
            'country.min'            => 'Verificare l\'indirizzo',
            'lat.required'           => 'Non è stato possibile ricavare le coordinate della tua attività, verificare l\'indirizzo',
            'lat.float'              => 'Non è stato possibile ricavare le coordinate della tua attività, verificare l\'indirizzo',
            'long.required'          => 'Non è stato possibile ricavare le coordinate della tua attività, verificare l\'indirizzo',
            'long.float'             => 'Non è stato possibile ricavare le coordinate della tua attività, verificare l\'indirizzo',
            'privacy.required'       => 'Devi accettare la pricacy',
            'delivery.required'      => 'Valorizzare il raggio di azione per le vendite',
            'delivery.numeric'       => 'Il raggio di azione per le vendite deve essere un numero che indica i km',
            'description.required'   => 'Selezionare il genere di attività',
            'description.numeric'    => 'Selezionare il genere di attività'
        ];
    }

}
