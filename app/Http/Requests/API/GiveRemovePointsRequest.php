<?php

namespace App\Http\Requests\API;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class GiveRemovePointsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user()->user_type == 'company';
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [            
            'points' => 'required',
            'order_id' => 'required'
        ];
    }


    public function messages()
    {
        return [            
            'points.required' => 'Points value missing',
            'order_id.required' => 'Order id missing'
        ];
    }
}
