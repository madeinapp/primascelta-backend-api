<?php

namespace App\Http\Requests\API;

use Illuminate\Foundation\Http\FormRequest;

class SendMessageAPIRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {        
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [            
            'order_id' => 'required',
            'message' => 'required'
        ];
    }

    public function messages()
    {
        return [            
            'order_id.required' => 'Order ID is missing',
            'message.required' => 'Message is missing'
        ];
    }
}
