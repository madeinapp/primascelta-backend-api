<?php

namespace App\Http\Requests\API;

use InfyOm\Generator\Request\APIRequest;

use Auth;

use App\Models\Shop;
use App\Models\User;
use App\Models\TaxType;

use App\Rules\StrongPassword;
use App\Rules\Timetable;


class UpdateShopAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user()->user_type === 'company' || Auth::user()->role === 'admin';
    }

    protected function prepareForValidation()
    {

        if( User::find(Auth::id())->shop ):
            $this->merge([
                'user_id' => Auth::id(),
                'shop_id' => User::find(Auth::id())->shop->id,
            ]);
        endif;

        $opening_timetable = [];
        $delivery_timetable = [];

        $days = ['mon' => 'monday',
                 'tue' => 'tuesday',
                 'wed' => 'wednesday',
                 'thu' => 'thursday',
                 'fri' => 'friday',
                 'sat' => 'saturday',
                 'sun' => 'sunday'];


        foreach($days as $shortday => $day):

            $opening_timetable [$day] = [];

            if( isset($this->{'open_'.$day.'_active'}) && $this->{'open_'.$day.'_active'} === 'on' ){
                $opening_timetable[$day]['active'] = true;
                $opening_timetable[$day]['first_half'] = [];
                $opening_timetable[$day]['first_half']['opening_time'] = $this->{'open_'.$shortday.'_mor_open'};
                $opening_timetable[$day]['first_half']['closing_time'] = $this->{'open_'.$shortday.'_mor_close'};

                $opening_timetable[$day]['second_half'] = [];
                $opening_timetable[$day]['second_half']['opening_time'] = $this->{'open_'.$shortday.'_aft_open'};
                $opening_timetable[$day]['second_half']['closing_time'] = $this->{'open_'.$shortday.'_aft_close'};
            }else{
                $opening_timetable[$day]['active'] = false;

                $opening_timetable[$day]['first_half'] = [];
                $opening_timetable[$day]['first_half']['opening_time'] = null;
                $opening_timetable[$day]['first_half']['closing_time'] = null;

                $opening_timetable[$day]['second_half'] = [];
                $opening_timetable[$day]['second_half']['opening_time'] = null;
                $opening_timetable[$day]['second_half']['closing_time'] = null;

            }

            if( isset($this->{'delivery_'.$day.'_active'}) && $this->{'delivery_'.$day.'_active'} === 'on' ){
                $delivery_timetable [$day] = [];
                $delivery_timetable[$day]['active'] = true;
                $delivery_timetable[$day]['first_half'] = [];
                $delivery_timetable[$day]['first_half']['opening_time'] = $this->{'delivery_'.$shortday.'_mor_open'};
                $delivery_timetable[$day]['first_half']['closing_time'] = $this->{'delivery_'.$shortday.'_mor_close'};

                $delivery_timetable[$day]['second_half'] = [];
                $delivery_timetable[$day]['second_half']['opening_time'] = $this->{'delivery_'.$shortday.'_aft_open'};
                $delivery_timetable[$day]['second_half']['closing_time'] = $this->{'delivery_'.$shortday.'_aft_close'};
            }else{
                $delivery_timetable [$day] = [];
                $delivery_timetable[$day]['active'] = false;
                $delivery_timetable[$day]['first_half'] = [];
                $delivery_timetable[$day]['first_half']['opening_time'] = null;
                $delivery_timetable[$day]['first_half']['closing_time'] = null;

                $delivery_timetable[$day]['second_half'] = [];
                $delivery_timetable[$day]['second_half']['opening_time'] = null;
                $delivery_timetable[$day]['second_half']['closing_time'] = null;

            }

        endforeach;

        /*
        $address = $this->route . ' ' . $this->street_number .  ( ( !empty(trim($this->route . ' ' . $this->street_number)) && !empty(trim($this->postal_code . ' ' . $this->city . ' ' . $this->country)) ) ? ', ' : '' ) . $this->postal_code . ' ' . $this->city . ' ' . $this->country;
        $delivery_address = $this->delivery_address_route . ' ' . $this->delivery_address_street_number . ( !empty(trim($this->delivery_address_route . ' ' . $this->delivery_address_street_number)) && !empty(trim($this->delivery_address_postal_code . ' ' . $this->delivery_address_city . ' ' . $this->delivery_address_country)) ? ', ' : '') . $this->delivery_address_postal_code . ' ' . $this->delivery_address_city . ' ' . $this->delivery_address_country;
        $tax_address = $this->tax_address_route . ' ' . $this->tax_address_street_number . (!empty($this->tax_address_postal_code . ' ' . $this->tax_address_city . ' ' . $this->tax_address_country) ? ', ' : '') . $this->tax_address_postal_code . ' ' . $this->tax_address_city . ' ' . $this->tax_address_country;
        */

        $this->merge([
            'opening_timetable' => json_encode($opening_timetable),
            'delivery_opening_timetable' => empty($delivery_timetable) ? json_encode($opening_timetable) : json_encode($delivery_timetable),
            'val_opening_timetable' => $opening_timetable,
            'val_delivery_opening_timetable' => empty($delivery_timetable) ? $opening_timetable : $delivery_timetable,
            'delivery_address' => empty($this->delivery_address) ? $this->address : $this->delivery_address,
            'delivery_address_country' => empty($this->delivery_address) ? $this->address_country : $this->delivery_address_country,
            'delivery_address_route' => empty($this->delivery_address) ? $this->address_route : $this->delivery_address_route,
            'delivery_address_city' => empty($this->delivery_address) ? $this->address_city : $this->delivery_address_city,
            'delivery_address_region' => empty($this->delivery_address) ? $this->address_region : $this->delivery_address_region,
            'delivery_address_postal_code' => empty($this->delivery_address) ? $this->address_postal_code : $this->delivery_address_postal_code,
            'delivery_address_street_number' => empty($this->delivery_address) ? $this->address_street_number : $this->delivery_address_street_number,
            'delivery_address_lat' => empty($this->delivery_address) ? $this->address_lat : $this->delivery_address_lat,
            'delivery_address_long' => empty($this->delivery_address) ? $this->address_long : $this->delivery_address_long,
            'delivery_host_on_site' => false, //( isset($this->delivery_host_on_site) ? true : false ),
            'home_delivery' => ( isset($this->home_delivery) ? true : false ),
        ]);

        if( isset($this->tax_type_id) && !empty($this->tax_type_id) ):
            $this->merge([
                'tax_type' => TaxType::find($this->tax_type_id)->name
            ]);
        endif;

        //dd($this->all());

    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'shop_type_id' => 'required',
            'website' => 'nullable|url',
            'user_name' => 'required',
            'user_birthdate' => 'required|date',
            'shop_name' => 'required',
            'address' => 'required',
            'phone' => 'required_without:whatsapp|nullable|regex:/^[0-9]{8,}$/',
            'whatsapp' => 'required_without:phone|nullable|regex:/^[0-9]{8,}$/',
            'email' => 'required|email',
            'facebook_page' => 'nullable|url',
            'instagram_page' => 'nullable|url',
            'description' => 'required',
            'payment_method_id' => 'required',
            'new_password' => ['nullable', 'confirmed', new StrongPassword],
            'tax_fiscal_code' => (strtolower($this->tax_type) === 'persona fisica') ? 'required_without_all:tax_vat,tax_code|nullable|regex:/^([A-Z]{6}[0-9]{2}[A-Z]{1}[0-9]{2}[A-Z][0-9]{3}[A-Z])$/' : ['required_without_all:tax_vat,tax_code', 'nullable', 'regex:/^([A-Z]{6}[0-9]{2}[A-Z]{1}[0-9]{2}[A-Z][0-9]{3}[A-Z])|(\d{11})$/'],
            'tax_vat' => 'required_without_all:tax_fiscal_code,tax_code|nullable|regex:/^[0-9]{11}$/',
            'tax_code' => 'required_without_all:tax_vat,tax_fiscal_code|nullable|size:7|regex:/^\w*[a-zA-Z0]\w*$/',
            'delivery_free_price_greater_than' => 'nullable|regex:/^\d+(\,\d{1,2})?$/',
            'delivery_forfait_cost_price' => 'nullable|regex:/^\d+(\,\d{1,2})?$/',
            'val_opening_timetable.monday' => [new Timetable],
            'val_opening_timetable.tuesday' => [new Timetable],
            'val_opening_timetable.wednesday' => [new Timetable],
            'val_opening_timetable.thursday' => [new Timetable],
            'val_opening_timetable.friday' => [new Timetable],
            'val_opening_timetable.saturday' => [new Timetable],
            'val_opening_timetable.sunday' => [new Timetable],
            'val_delivery_opening_timetable.monday' => [new Timetable],
            'val_delivery_opening_timetable.tuesday' => [new Timetable],
            'val_delivery_opening_timetable.wednesday' => [new Timetable],
            'val_delivery_opening_timetable.thursday' => [new Timetable],
            'val_delivery_opening_timetable.friday' => [new Timetable],
            'val_delivery_opening_timetable.saturday' => [new Timetable],
            'val_delivery_opening_timetable.sunday' => [new Timetable],
            'subscription_transaction_price' => 'nullable|required_with:subscription_transaction_desc',
            'subscription_transaction_desc' => 'nullable|required_with:subscription_transaction_price',
        ];
    }

    public function messages()
    {
        return [

            'shop_type_id' => 'Selezionare la tipologia attività',
            'website.url' => 'Inserire una url valida per il sito web es. http://www.nomesito.it',
            'user_name.required' => 'Il nome utente è obbligatorio',
            'user_birthdate.required' => 'La data di nascita è obbligatoria',
            'user_birthdate.date' => 'La data di nascita non è una data valida',
            'shop_name.required' => 'Devi inserire il nome dell\'attività',
            'address.required' => 'Inserire l\'indirizzo dell\'attività',
            'phone.regex' => 'Il telefono non è valido. Sono ammessi solo caratteri numerici senza alcuno spazio',
            'phone.required_without' => 'Inserire il telefono o un numero di cellulare',
            'whatsapp.regex' => 'Il numero di cellulare non è valido. Sono ammessi solo caratteri numerici senza alcuno spazio',
            'whatsapp.required_without' => 'Inserire il telefono o un numero di cellulare',
            'email.required' => 'L\'email è obbligatoria',
            'email.email' => 'Digitare un indirizzo email valido',
            'facebook_page.url' => 'Digitare una url valida per la pagina Facebook es. https://www.facebook.com/nomepagina',
            'instagram_page.url' => 'Digitare una url valida per la pagina Instagram es. https://www.instagram.com/nomepagina/',
            'description.required' => 'Inserire la descrizione dell\'attività',
            'payment_method_id.required' => 'Selezionare almeno un metodo di pagamento',
            'new_password.confirmed' => 'La password di conferma non corrisponde',
            'new_password.min' => 'La password deve essere di almeno 6 caratteri',
            'tax_fiscal_code.required_without_all' => 'Inserire il Codice Fiscale o in alternativa la Partita IVA o il Codice destinatario',
            'tax_fiscal_code.regex' => 'Inserire un codice fiscale valido',
            'tax_vat.required_without_all' => 'Inserire la Partita IVA o in alternativa il Codice Fiscale o il Codice destinatario',
            'tax_vat.regex' => 'Inserire una partita IVA valida',
            'tax_code.required_without_all' => 'Inserire il Codice destinatario o in alternativa il Codice Fiscale o la Partita IVA',
            'tax_code.size' => 'Il codice destinatario deve essere di 7 caratteri',
            'delivery_free_price_greater_than.regex' => 'Inserire un importo valido per l\'importo della consegna gratuita',
            'delivery_forfait_cost_price.regex' => 'Inserire un importo valido per i costi fissi di consegna',
            'subscription_transaction_price.required_with' => 'Inserire l\'importo della transazione',
            'subscription_transaction_desc.required_with' => 'Inserire la descrizione della transazione',
        ];

    }
}
