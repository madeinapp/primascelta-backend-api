<?php

namespace App\Http\Requests\API;

use App\Models\User;
use InfyOm\Generator\Request\APIRequest;
use App\Rules\StrongPassword;

class UpdateUserShopAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'email' => 'required|email|unique:users',
            'password' => ['required', 'confirmed', new StrongPassword],
            'facebook_id' => 'unique:users',
            'birthdate' => 'date|required'
        ];

        return $rules;
    }
}
