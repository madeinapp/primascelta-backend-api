<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use App\Models\ShopCatalog;
use App\Models\User;
use Illuminate\Validation\Rule;

class CreateShopCatalogRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        if( User::find(Auth::id())->shop ):
            $this->merge([
                'shop_id' => User::find(Auth::id())->shop->id,
                'food_price' => toFloat($this->food_price)
            ]);
        endif;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'shop_id' => 'required',
            'food_category_id' => 'required',
            'food_name' => [
                'required',
                Rule::unique('shop_catalogs')->where(function ($query) {
                    return $query->where('shop_id', $this->shop_id)
                                 ->where('food_name', $this->food_name)
                                 ->whereNull('deleted_at');
                })
            ],
            'food_type' => 'required',
            'food_price' => 'required|numeric|between:0,999999.99',
            'short_description' => 'nullable|max:30'
        ];
    }

     /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return ShopCatalog::$messages;
    }
}
