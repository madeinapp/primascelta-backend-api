<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Shop;
use App\Models\TaxType;
use App\Rules\StrongPassword;
use App\Rules\Timetable;
use Log;
use Carbon\Carbon;
use Illuminate\Validation\Rule;

class UpdateProfileCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user()->user_type === 'company' || Auth::user()->role === 'admin';
    }


    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
		$subscription = null;
		if(Auth::user()->role == 'admin'){
			$user = User::find($this->user_id);
			$subscription = $user->subscription->subscription;
			$admin_generated = $user->shop->admin_generated;
		} else {	
			if(!empty(Auth::user()->subscription)){
				$subscription = Auth::user()->subscription->subscription;
				$admin_generated = Auth::user()->shop->admin_generated;
			}
		}
		
		if( User::find(Auth::id())->shop ):
            $this->merge([
                'user_id' => Auth::id(),
                'shop_id' => User::find(Auth::id())->shop->id,
            ]);
        endif;
		/** Check user age */
        if( $this->user_birthdate ){
            $user_age = Carbon::createFromFormat('d/m/Y', $this->user_birthdate)->age;
            $this->merge([
                'user_birthdate' => Carbon::createFromFormat('d/m/Y', $this->user_birthdate),
                'age' => $user_age
            ]);
		} elseif(!empty($admin_generated)) {
			 $this->merge([
                'user_birthdate' => Carbon::createFromFormat('d/m/Y', date('d/m/Y'))->subYears(18),
                'age' => '18'
            ]);
		}

        $opening_timetable = [];
        $delivery_timetable = [];

        $days = ['mon' => 'monday',
                 'tue' => 'tuesday',
                 'wed' => 'wednesday',
                 'thu' => 'thursday',
                 'fri' => 'friday',
                 'sat' => 'saturday',
                 'sun' => 'sunday'];

        $alert_opening_days = false;
        $alert_delivery_days = false;

        foreach($days as $shortday => $day):

            $opening_timetable [$day] = [];

            if( isset($this->{'open_'.$day.'_active'}) && $this->{'open_'.$day.'_active'} === 'on' ){
                $opening_timetable[$day]['active'] = true;
                $opening_timetable[$day]['first_half'] = [];
                $opening_timetable[$day]['first_half']['opening_time'] = $this->{'open_'.$shortday.'_mor_open'};
                $opening_timetable[$day]['first_half']['closing_time'] = $this->{'open_'.$shortday.'_mor_close'};

                $opening_timetable[$day]['second_half'] = [];
                $opening_timetable[$day]['second_half']['opening_time'] = $this->{'open_'.$shortday.'_aft_open'};
                $opening_timetable[$day]['second_half']['closing_time'] = $this->{'open_'.$shortday.'_aft_close'};

                $alert_opening_days = true;

            }else{
                $opening_timetable[$day]['active'] = false;

                $opening_timetable[$day]['first_half'] = [];
                $opening_timetable[$day]['first_half']['opening_time'] = null;
                $opening_timetable[$day]['first_half']['closing_time'] = null;

                $opening_timetable[$day]['second_half'] = [];
                $opening_timetable[$day]['second_half']['opening_time'] = null;
                $opening_timetable[$day]['second_half']['closing_time'] = null;

            }

            if( isset($this->{'control-delivery-timetable'}) && $this->{'control-delivery-timetable'} === 'on' ){

                $delivery_timetable [$day] = [];
                $delivery_timetable[$day]['active'] = $opening_timetable[$day]['active'];
                $delivery_timetable[$day]['first_half'] = $opening_timetable[$day]['first_half'];
                $delivery_timetable[$day]['first_half']['opening_time'] = $opening_timetable[$day]['first_half']['opening_time'];
                $delivery_timetable[$day]['first_half']['closing_time'] = $opening_timetable[$day]['first_half']['closing_time'];

                $delivery_timetable[$day]['second_half'] = [];
                $delivery_timetable[$day]['second_half']['opening_time'] = $opening_timetable[$day]['second_half']['opening_time'];
                $delivery_timetable[$day]['second_half']['closing_time'] = $opening_timetable[$day]['second_half']['closing_time'];

                $alert_delivery_days = true;

            }else{

                if( isset($this->{'delivery_'.$day.'_active'}) && $this->{'delivery_'.$day.'_active'} === 'on' ){
                    $delivery_timetable [$day] = [];
                    $delivery_timetable[$day]['active'] = true;
                    $delivery_timetable[$day]['first_half'] = [];
                    $delivery_timetable[$day]['first_half']['opening_time'] = $this->{'delivery_'.$shortday.'_mor_open'};
                    $delivery_timetable[$day]['first_half']['closing_time'] = $this->{'delivery_'.$shortday.'_mor_close'};

                    $delivery_timetable[$day]['second_half'] = [];
                    $delivery_timetable[$day]['second_half']['opening_time'] = $this->{'delivery_'.$shortday.'_aft_open'};
                    $delivery_timetable[$day]['second_half']['closing_time'] = $this->{'delivery_'.$shortday.'_aft_close'};

                    $alert_delivery_days = true;
                }else{
                    $delivery_timetable [$day] = [];
                    $delivery_timetable[$day]['active'] = false;
                    $delivery_timetable[$day]['first_half'] = [];
                    $delivery_timetable[$day]['first_half']['opening_time'] = null;
                    $delivery_timetable[$day]['first_half']['closing_time'] = null;

                    $delivery_timetable[$day]['second_half'] = [];
                    $delivery_timetable[$day]['second_half']['opening_time'] = null;
                    $delivery_timetable[$day]['second_half']['closing_time'] = null;
                }

            }

        endforeach;

        /*
        $address = $this->route . ' ' . $this->street_number .  ( ( !empty(trim($this->route . ' ' . $this->street_number)) && !empty(trim($this->postal_code . ' ' . $this->city . ' ' . $this->country)) ) ? ', ' : '' ) . $this->postal_code . ' ' . $this->city . ' ' . $this->country;
        $delivery_address = $this->delivery_address_route . ' ' . $this->delivery_address_street_number . ( !empty(trim($this->delivery_address_route . ' ' . $this->delivery_address_street_number)) && !empty(trim($this->delivery_address_postal_code . ' ' . $this->delivery_address_city . ' ' . $this->delivery_address_country)) ? ', ' : '') . $this->delivery_address_postal_code . ' ' . $this->delivery_address_city . ' ' . $this->delivery_address_country;
        $tax_address = $this->tax_address_route . ' ' . $this->tax_address_street_number . (!empty($this->tax_address_postal_code . ' ' . $this->tax_address_city . ' ' . $this->tax_address_country) ? ', ' : '') . $this->tax_address_postal_code . ' ' . $this->tax_address_city . ' ' . $this->tax_address_country;
        */

        $this->merge([
            'opening_timetable' => json_encode($opening_timetable),
            'delivery_opening_timetable' => json_encode($delivery_timetable),
            'val_opening_timetable' => $opening_timetable,
            'val_delivery_opening_timetable' => $delivery_timetable,
            'delivery_address' => empty($this->delivery_address) ? $this->address : $this->delivery_address,
            'delivery_address_country' => empty($this->delivery_address) || $this->delivery_address == $this->address ? $this->country : $this->delivery_address_country,
            'delivery_address_route' => empty($this->delivery_address) || $this->delivery_address == $this->address ? $this->route : $this->delivery_address_route,
            'delivery_address_city' => empty($this->delivery_address) || $this->delivery_address == $this->address ? $this->city : $this->delivery_address_city,
            'delivery_address_region' => empty($this->delivery_address) || $this->delivery_address == $this->address ? $this->region : $this->delivery_address_region,
            'delivery_address_postal_code' => empty($this->delivery_address) || $this->delivery_address == $this->address ? $this->postal_code : $this->delivery_address_postal_code,
            'delivery_address_street_number' => empty($this->delivery_address) || $this->delivery_address == $this->address ? $this->street_number : $this->delivery_address_street_number,
            'delivery_address_lat' => empty($this->delivery_address) || $this->delivery_address == $this->address ? $this->lat : $this->delivery_address_lat,
            'delivery_address_long' => empty($this->delivery_address) || $this->delivery_address == $this->address ? $this->long : $this->delivery_address_long,
            'delivery_host_on_site' => false, //( isset($this->delivery_host_on_site) ? true : false ),
            'home_delivery' => ( isset($this->home_delivery) ? true : false ),
        ]);

        if( $this->shop_status === 'Aperto' ):
            $this->merge([
                'closed_date_start' => null,
                'closed_date_end' => null
            ]);
        endif;

        if( isset($this->tax_type_id) && !empty($this->tax_type_id) ):
            $this->merge([
                'tax_type' => TaxType::find($this->tax_type_id)->name
            ]);
        endif;

        $this->merge([
            'alert_opening_days' => $alert_opening_days,
            'alert_delivery_days' => $alert_delivery_days
        ]);


    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $email = $this->email;
        $user_id = Auth::user()->user_type === 'company' ? Auth::id() : $this->user_id;
		$subscription = null;
		if(Auth::user()->role == 'admin'){
			$user = User::find($this->user_id);
			$subscription = $user->subscription->subscription;
			$admin_generated = $user->shop->admin_generated;
		} else {	
			if(!empty(Auth::user()->subscription)){
				$subscription = Auth::user()->subscription->subscription;
				$admin_generated = Auth::user()->shop->admin_generated;
			}
		}

        $rules = [
            'shop_type_id' => 'required|array|max:4',
            'website' => 'nullable|url',
            'email' => ['required',
                        'email',
                        Rule::unique('users')->where(function ($query) use($user_id, $email) {
                            return $query->where('email', $email)
                                         ->where('status', 'active')
                                         ->where('id', '!=', $user_id);
                        }),
                    ],
            'user_name' => (!empty($admin_generated)) ? '' : 'required',
            'user_birthdate' => (!empty($admin_generated)) ? '' : 'required|date',
            'shop_name' => 'required',
            'address' => 'required',
            'route' => (!empty($admin_generated)) ? '' : 'required_with:address',
            'delivery_address_route' => Rule::requiredIf($this->delivery_address != $this->address),
            'phone' => (!empty($admin_generated)) ? '' : 'required_without:whatsapp|nullable|regex:/^[0-9]{8,}$/',
            'whatsapp' => (!empty($admin_generated)) ? '' : 'required_without:phone|nullable|regex:/^[0-9]{8,}$/',
            'facebook_page' => 'nullable|url',
            'instagram_page' => 'nullable|url',
            'description' => (!empty($admin_generated)) ? '' : 'required',
            'payment_method_id' => (!empty($admin_generated)) ? '' : 'required',
            'delivery_payment_method_id'=> (!empty($admin_generated)) ? '' : 'required_if:home_delivery,1',
            'new_password' => ['nullable', 'confirmed', new StrongPassword],
            'tax_fiscal_code' => (!empty($admin_generated)) ? '' : ((strtolower($this->tax_type) === 'persona fisica') ? 'required|regex:/^([A-Z]{6}[0-9]{2}[A-Z]{1}[0-9]{2}[A-Z][0-9]{3}[A-Z])$/' : ['required', 'regex:/^([A-Z]{6}[0-9]{2}[A-Z]{1}[0-9]{2}[A-Z][0-9]{3}[A-Z])|(\d{11})$/']),
            'tax_vat' => (!empty($admin_generated)) ? '' : 'required|regex:/^[0-9]{11}$/',
            'tax_code' => (!empty($admin_generated)) ? '' : 'required_without:tax_pec|nullable|size:7|regex:/^\w*[a-zA-Z0]\w*$/',
            'tax_pec' => (!empty($admin_generated)) ? '' : 'required_without:tax_code|nullable|email',
            'delivery_free_price_greater_than' => 'nullable|regex:/^\d+(\,\d{1,2})?$/',
            'delivery_forfait_cost_price' => 'nullable|regex:/^\d+(\,\d{1,2})?$/',
            'val_opening_timetable.monday' => [new Timetable],
            'val_opening_timetable.tuesday' => [new Timetable],
            'val_opening_timetable.wednesday' => [new Timetable],
            'val_opening_timetable.thursday' => [new Timetable],
            'val_opening_timetable.friday' => [new Timetable],
            'val_opening_timetable.saturday' => [new Timetable],
            'val_opening_timetable.sunday' => [new Timetable],
            'val_delivery_opening_timetable.monday' => [new Timetable],
            'val_delivery_opening_timetable.tuesday' => [new Timetable],
            'val_delivery_opening_timetable.wednesday' => [new Timetable],
            'val_delivery_opening_timetable.thursday' => [new Timetable],
            'val_delivery_opening_timetable.friday' => [new Timetable],
            'val_delivery_opening_timetable.saturday' => [new Timetable],
            'val_delivery_opening_timetable.sunday' => [new Timetable],
            'subscription_transaction_price' => 'nullable|required_with:subscription_transaction_desc',
            'subscription_transaction_desc' => 'nullable|required_with:subscription_transaction_price',
            'closed_date_start' => 'nullable|date|required_unless:shop_status,Aperto|after_or_equal:today',
            'closed_date_end' => 'nullable|date|required_unless:shop_status,Aperto|after_or_equal:closed_date_start',
            'age' => (!empty($admin_generated)) ? '' : 'required|numeric|min:18',
            'alert_opening_days' => (!empty($admin_generated)) ? '' : 'required|accepted',
            'alert_delivery_days' => (!empty($admin_generated)) ? '' : 'required|accepted',
            'delivery_range_km' => (!empty($admin_generated)) ? '' : 'required_if:home_delivery,1'
        ];
        //dd($rules);
        return $rules;
    }

    /**
     * Get the validation messages
     *
     * @return array
     */
    public function messages()
    {
        return [

            'shop_type_id.required' => 'Selezionare almeno una tipologia attività',
            'shop_type_id.max' => 'Puoi selezionare massimo 4 tipologie attività',
            'website.url' => 'Inserire una url valida per il sito web es. http://www.nomesito.it',
            'email.required' => 'L\'email è obbligatoria',
            'email.email' => 'Formato email non valido',
            'email.unique' => "L'email risulta già utilizzata",
            'user_name.required' => 'Il nome utente è obbligatorio',
            'user_birthdate.required' => 'La data di nascita è obbligatoria',
            'user_birthdate.date' => 'La data di nascita non è una data valida',
            'shop_name.required' => 'Devi inserire il nome dell\'attività',
            'address.required' => 'Inserire l\'indirizzo dell\'attività',
            'phone.regex' => 'Il telefono non è valido. Sono ammessi solo caratteri numerici senza alcuno spazio',
            'phone.required_without' => 'Inserire il telefono o un numero di cellulare',
            'whatsapp.required_without' => 'Inserire il telefono o un numero di cellulare',
            'whatsapp.regex' => 'Il numero di cellulare non è valido. Sono ammessi solo caratteri numerici senza alcuno spazio',
            'email.required' => 'L\'email è obbligatoria',
            'email.email' => 'Digitare un indirizzo email valido',
            'facebook_page.url' => 'Digitare una url valida per la pagina Facebook es. https://www.facebook.com/nomepagina',
            'instagram_page.url' => 'Digitare una url valida per la pagina Instagram es. https://www.instagram.com/nomepagina/',
            'description.required' => 'Inserire la descrizione dell\'attività',
            'payment_method_id.required' => 'Selezionare almeno un metodo di pagamento',
            'delivery_payment_method_id.required_if' => 'Selezionare almeno un metodo di pagamento per la consegna a domicilio',
            'new_password.confirmed' => 'La password di conferma non corrisponde',
            'new_password.min' => 'La password deve essere di almeno 6 caratteri',
            'tax_fiscal_code.required' => 'Inserire il Codice Fiscale',
            'tax_fiscal_code.regex' => 'Inserire un codice fiscale valido',
            'tax_vat.required' => 'Inserire la Partita IVA',
            'tax_vat.regex' => 'Inserire una partita IVA valida',
            'tax_code.required_without' => 'Inserire il codice univoco o in alternativa la PEC',
            'tax_code.size' => 'Il codice univoco deve essere di 7 caratteri',
            'tax_code.regex' => 'Il codice univoco deve contenere numeri e almeno un carattere',
            'tax_pec.required_without' => 'Inserire la PEC o in alternativa il codice univoco',
            'tax_pec.email' => 'Inserire una PEC valida',
            'delivery_free_price_greater_than.regex' => 'Inserire un importo valido per l\'importo della consegna gratuita',
            'delivery_forfait_cost_price.regex' => 'Inserire un importo valido per i costi fissi di consegna',
            'subscription_transaction_price.required_with' => 'Inserire l\'importo della transazione',
            'subscription_transaction_desc.required_with' => 'Inserire la descrizione della transazione',
            'closed_date_start.date' => 'La data inizio chiusura non ha un formato valido',
            'closed_date_start.required_unless' => 'Inserire la data di inizio chiusura',
            'closed_date_start.after_or_equal' => 'La data di inizio chiusura deve essere a partire da oggi',
            'closed_date_end.date' => 'La data inizio chiusura non ha un formato valido',
            'closed_date_end.required_unless' => 'Inserire la data di fine chiusura',
            'closed_date_end.after_or_equal' => 'La data di fine chiusura deve essere successiva o uguale alla data di inizio',
            'age.required' => 'La data di nascita è obbligatoria',
            'age.min' => 'Il titolare di un attività non può essere minorenne',
            'alert_opening_days.accepted' => 'Devi selezionare almeno un giorno di apertura',
            'alert_delivery_days.accepted' => 'Devi selezionare almeno un giorno per il ritiro',
            'delivery_range_km.required_if' => 'Indicare un raggio minimo di 1Km per la consegna a domicilio'
        ];

    }
}
