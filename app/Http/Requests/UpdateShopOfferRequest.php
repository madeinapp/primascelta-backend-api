<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use App\Models\ShopOffer;
use App\Models\User;
use Illuminate\Validation\Rule;

use Carbon\Carbon;

class UpdateShopOfferRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        if( User::find(Auth::id())->shop ):
            $this->merge([
                'shop_id' => User::find(Auth::id())->shop->id,
            ]);
        endif;

        if( $this->start_date ){
            $this->merge([
                'start_date' => Carbon::createFromFormat('d/m/Y', $this->start_date)
            ]);
        }

        if( $this->end_date ){
            $this->merge([
                'end_date' => Carbon::createFromFormat('d/m/Y', $this->end_date)
            ]);
        }

        $modify = [];

        if( isset($this->discount) ):

            $modify['discount'] = [];

            foreach($this->discount as $k => $val):
                if( is_numeric(str_replace(",",".",$val)) ):
                    $modify['discount'][$k] = tofloat($val);
                else:
                    $modify['discount'][$k] = $val;
                endif;
            endforeach;

        endif;

        if( isset($this->discounted_price) ):

            $modify['discounted_price'] = [];

            foreach($this->discounted_price as $k => $val):
                if( is_numeric(str_replace(",",".",$val)) ):
                    $modify['discounted_price'][$k] = tofloat($val);
                else:
                    $modify['discounted_price'][$k] = $val;
                endif;
            endforeach;

        endif;

        $this->merge($modify);

    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'shop_id' => 'required',
            'start_date' => 'required|date',
            'end_date' => 'required|date|after_or_equal:start_date',
            'name' => [
                'required',
                Rule::unique('shop_offers')->where(function ($query) {
                    return $query->where('shop_id', $this->shop_id)
                                 ->where('name', $this->name)
                                 ->whereNull('deleted_at');
                })->ignore($this->shopOffer)
            ],
            'shop_catalog_id' => 'required|array'
        ];

    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {

        return [
            'start_date.required' => 'Inserire la data inizio',
            'end_date.required' => 'Inserire la data fine',
            'end_date.after_or_equal' => 'La data fine deve essere uguale o successiva alla data inizio',
            'name.required' => 'Nome offerta assente',
            'name.unique' => "Esiste già un'offerta con questo nome",
            'description.required' => 'Inserire una descrizione',
            'shop_catalog_id.required' => 'Selezionare almeno un prodotto dal catalogo',
        ];

    }
}
