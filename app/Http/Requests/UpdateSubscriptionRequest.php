<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Subscription;
use Illuminate\Support\Str;
use Auth;

class UpdateSubscriptionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user()->role === 'admin';
    }


    protected function prepareForValidation()
    {
        $this->merge([
            'description_list' => json_encode(explode(PHP_EOL, $this->description_list)),
            'price_yearly' => tofloat($this->price_yearly),
            'price_half_yearly' => tofloat($this->price_half_yearly),
        ]);

    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Subscription::$rules;
    }

    public function messages()
    {
        return Subscription::$messages;
    }
}
