<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use App\Rules\StrongPassword;

class UpdateUserRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user()->role === 'admin' || Auth::id() === $this->id;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('user');
        $rules = [
          'name'     => 'required',
          'email'    => 'required|email|unique:users,email,'.$id,
          'new_password' =>  ['nullable', 'confirmed', new StrongPassword]
        ];

        return $rules;
    }

    public function messages()
    {
        return [
          'name.required'     => 'Inserire il nome utente',
          'email.required'    => 'Inserire l\'email dell\'utente',
          'email.email'       => 'L\'email non risulta essere del formato corretto',
          'email.unique'      => 'Esistono già utenti con questa email',
          'new_password.confirmed' => 'La password di conferma non corrisponde'
        ];


    }
}
