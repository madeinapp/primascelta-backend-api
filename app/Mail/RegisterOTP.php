<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RegisterOTP extends Mailable
{
    use Queueable, SerializesModels;

    private $otpCode, $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($otpCode, $user)
    {
        $this->otpCode = $otpCode;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Codice di attivazione Primascelta.biz')
                    ->markdown('emails.register_otp')
                    ->with(['otpCode' => $this->otpCode,
                            'user' => $this->user]);
    }
}
