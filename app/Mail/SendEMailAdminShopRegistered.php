<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendEMailAdminShopRegistered extends Mailable
{
    use Queueable, SerializesModels;

    private $user;
    private $shop;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $shop)
    {
        $this->user = $user;
        $this->shop = $shop;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Benvenuto su Primascelta!')
                    ->markdown('emails.welcome_company_admin')
                    ->with(['user' => $this->user, 'shop' => $this->shop]);
    }
}
