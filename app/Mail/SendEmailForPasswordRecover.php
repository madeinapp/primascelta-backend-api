<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendEmailForPasswordRecover extends Mailable
{
    use Queueable, SerializesModels;

    private $otpCode;
    private $email;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email, $otpCode)
    {
        $this->otpCode = $otpCode;
        $this->email = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Recupera password Primascelta.biz')
                    ->markdown('emails.password_recover_otp')->with(['otpCode' => $this->otpCode, 'email' => $this->email]);
    }
}
