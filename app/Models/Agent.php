<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

/**
 * Class Agent
 * @package App\Models
 * @version May 8, 2020, 4:57 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection $shops
 * @property string $name
 * @property string $phone
 * @property string $coupon
 */
class Agent extends Model
{

    public $table = 'agents';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'name',
        'phone',
        'coupon',
        'address',
        'commissions',
        'note'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'phone' => 'string',
        'coupon' => 'string',
        'address' => 'string',
        'commissions' => 'string',
        'note' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'phone' => 'nullable|regex:/^[0-9]{8,}$/',
    ];

    public static $messages = [
        'name.required' => 'Inserire il nome',
        'phone.regex' => 'Il telefono non è valido. Sono ammessi solo caratteri numerici senza alcuno spazio'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function shops()
    {
        return $this->hasMany(\App\Models\Shop::class, 'agent_id');
    }
}
