<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlacklistShop extends Model
{
    public $table = 'blacklist_shops';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'user_id',
        'shop_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'user_id' => 'integer',
        'shop_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'user_id' => 'required',
        'shop_id' => 'required'        
    ];

    public function shop(){
        return $this->belongsTo(\App\Models\Shop::class);
    }

}
