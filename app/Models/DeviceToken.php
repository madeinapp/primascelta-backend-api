<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

class DeviceToken extends Model
{
    //
    public $table = 'device_tokens';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'user_id',
        'token',
        'so',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'user_id' => 'required',
        'token' => 'required',
        'so' => 'required',
    ];

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }
}
