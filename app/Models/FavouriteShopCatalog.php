<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class FavouriteShopCatalog extends Model
{
    public $table = 'favourite_shop_catalogs';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'user_id',
        'shop_catalog_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'user_id' => 'integer',
        'shop_catalog_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'user_id' => 'required',
        'shop_catalog_id' => 'required'        
    ];

    public function shopCatalog(){
        return $this->belongsTo(\App\Models\ShopCatalog::class);
    }
}
