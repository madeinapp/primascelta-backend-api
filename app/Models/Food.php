<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

/**
 * Class Food
 * @package App\Models
 * @version June 22, 2020, 7:30 am UTC
 *
 * @property \App\Models\FoodCategory $foodCategory
 * @property \App\Models\UnitOfMeasure $unitOfMeasure
 * @property integer $food_category_id
 * @property integer $unit_of_measure_id
 * @property string $name
 * @property string $description
 * @property string $type
 * @property string $image
 * @property boolean $available
 */
class Food extends Model
{

    public $table = 'foods';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';




    public $fillable = [
        'food_category_id',
        'unit_of_measure_id',
        'name',
        'description',
        'type',
        'image',
        'available'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'food_category_id' => 'integer',
        'unit_of_measure_id' => 'integer',
        'name' => 'string',
        'description' => 'string',
        'type' => 'string',
        'image' => 'string',
        'available' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'food_category_id' => 'required',
        'unit_of_measure_id' => 'required',
        'name' => 'required',
        'type' => 'required',
        'available' => 'required'
    ];

    public static $messages = [
        'food_category_id.required' => 'Selezionare un genere',
        'unit_of_measure_id.required' => 'Selezionare l\'unità di misura',
        'name.required' => 'Il nome è obbligatorio',
        'type.required' => 'La tipologia è obbligatoria',
        'available.required' => 'La disponibilità è obbligatoria'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function foodCategory()
    {
        return $this->belongsTo(\App\Models\FoodCategory::class, 'food_category_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function unitOfMeasure()
    {
        return $this->belongsTo(\App\Models\UnitOfMeasure::class, 'unit_of_measure_id');
    }

    public function specials()
    {
        return $this->belongsToMany('App\Models\Special', 'food_specials');
    }
}
