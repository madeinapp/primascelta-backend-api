<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

/**
 * Class FoodCategory
 * @package App\Models
 * @version April 26, 2020, 5:01 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection $foods
 * @property \Illuminate\Database\Eloquent\Collection $missingFoods
 * @property string $name
 */
class FoodCategory extends Model
{

    public $table = 'food_categories';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'name',
        'logo',
        'background',
        'unit_of_measure'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'logo' => 'string',
        'background' => 'string',
        'unit_of_measure' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'logo' => 'nullable',
        'background' => 'nullable',
        'unit_of_measure' => 'nullable'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $messages = [
        'name.required' => 'Il nome è obbligatorio',
        'logo.required' => 'Il logo è obbligatorio',
        'background.required' => 'Il background è obbligatorio'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function foods()
    {
        return $this->hasMany(\App\Models\Food::class, 'food_category_id');
    }

    public function shopCatalogs()
    {
        return $this->hasMany(\App\Models\ShopCatalog::class, 'food_category_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function missingFoods()
    {
        return $this->hasMany(\App\Models\MissingFood::class, 'food_category_id');
    }

    public function unitOfMeasure()
    {
        return $this->belongsTo(\App\Models\UnitOfMeasure::class, 'unit_of_measure');
    }
}
