<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FoodSpecial extends Model
{
    protected $table = 'food_specials';

    public $fillable = [
        'food_id',
        'special_id'
    ];

    public $cast = [
        'food_id' => 'integer',
        'special_id' => 'integer'
    ];

    public $rules = [
        'food_id' => 'required',
        'special_id' => 'required'
    ];

    public $messages = [
        'food_id.required' => 'Food id è obbligatorio',
        'special_id.required' => 'Special id è obbligatorio'
    ];
}
