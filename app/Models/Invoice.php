<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{

    protected $table = 'invoices';

    protected $appends = ['paid'];

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $fillable = [
        'title',
        'price',
        'payment_status',
        'user_id',
        'method_of_payment'
    ];

    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'price' => 'float',
        'payment_status' => 'string',
        'user_id' => 'integer',
        'method_of_payment' => 'string'
    ];

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }

    public function getPaidAttribute() {
    	if ($this->payment_status == 'Invalid') {
    		return false;
    	}
    	return true;
    }


}
