<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Log;

/**
 * Class Order
 * @package App\Models
 * @version June 1, 2020, 6:30 pm UTC
 *
 * @property \App\Models\OrderStatus $orderStatus
 * @property \App\Models\Shop $shop
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $orderFoods
 * @property integer $shop_id
 * @property integer $user_id
 * @property integer $order_status_id
 * @property number $total_price
 * @property number $shipping_price
 * @property string $note
 */
class Order extends Model
{

    protected $table = 'orders';

    protected $appends = ['can_trust', 'can_cancel'];

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'shop_id',
        'user_id',
        'order_status_id',
        'total_price',
        'shipping_price',
		'note',
        'pickup_on_site',
        'pickup_on_site_date',
        'pickup_on_site_time',
        'home_delivery',
        'home_delivery_address',
        'home_delivery_country',
        'home_delivery_route',
        'home_delivery_city',
        'home_delivery_region',
        'home_delivery_postal_code',
        'home_delivery_street_number',
        'home_delivery_lat',
        'home_delivery_long',
        'home_delivery_name',
        'home_delivery_phone',
        'home_delivery_date',
        'home_delivery_time',
        'home_delivery_payment_method_id',
        'delivery_host_on_site',
        'delivery_host_on_site_date',
        'delivery_host_on_site_time',
        'delivery_host_on_site_note',
        'delivery_host_on_site_payment_method_id',
        'pickup_on_site_within',
        'home_delivery_within',
        'delivery_host_on_site_within',
        'cancellable_within',
        'confirm_reserve_within',
        'flag_trust_points',
        'created_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'shop_id' => 'integer',
        'user_id' => 'integer',
        'order_status_id' => 'integer',
        'total_price' => 'float',
        'shipping_price' => 'float',
        'note' => 'string',
        'pickup_on_site' => 'integer',
        'pickup_on_site_date' => 'date',
        'pickup_on_site_time' => 'string',
        'home_delivery' => 'integer',
        'home_delivery_address' => 'string',
        'home_delivery_country' => 'string',
        'home_delivery_route' => 'string',
        'home_delivery_city' => 'string',
        'home_delivery_region' => 'string',
        'home_delivery_postal_code' => 'string',
        'home_delivery_street_number' => 'string',
        'home_delivery_lat' => 'string',
        'home_delivery_long' => 'string',
        'home_delivery_name' => 'string',
        'home_delivery_phone' => 'string',
        'home_delivery_date' => 'date',
        'home_delivery_time' => 'string',
        'home_delivery_payment_method_id' => 'string',
        'delivery_host_on_site' => 'integer',
        'delivery_host_on_site_date' => 'date',
        'delivery_host_on_site_time' => 'string',
        'delivery_host_on_site_note' => 'string',
        'delivery_host_on_site_payment_method_id' => 'string',
        'pickup_on_site_within' => 'datetime:Y-m-d H:i:s',
        'home_delivery_within' => 'datetime:Y-m-d H:i:s',
        'delivery_host_on_site_within' => 'datetime:Y-m-d H:i:s',
        'cancellable_within' => 'datetime:Y-m-d H:i:s',
        'confirm_reserve_within' => 'datetime:Y-m-d H:i:s',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'shop_id' => 'required',
        'user_id' => 'required',
        'order_status_id' => 'required',
        'pickup_on_site' => 'nullable',
        'pickup_on_site_date' => 'nullable|required_if:pickup_on_site,1',
        //'pickup_on_site_time' => 'nullable|required_if:pickup_on_site,1',
        'home_delivery' => 'nullable',
        'home_delivery_address' => 'nullable|required_if:home_delivery,1',
        'home_delivery_country' => 'nullable|required_if:home_delivery,1',
        'home_delivery_route' => 'nullable|required_if:home_delivery,1',
        'home_delivery_city' => 'nullable|required_if:home_delivery,1',
        'home_delivery_region' => 'nullable|required_if:home_delivery,1',
        'home_delivery_postal_code' => 'nullable|required_if:home_delivery,1',
        //'home_delivery_street_number' => 'nullable|required_if:home_delivery,1',
        'home_delivery_lat' => 'nullable|required_if:home_delivery,1',
        'home_delivery_long' => 'nullable|required_if:home_delivery,1',
        'home_delivery_name' => 'nullable|required_if:home_delivery,1',
        'home_delivery_phone' => 'nullable|required_if:home_delivery,1',
        'home_delivery_date' => 'nullable|required_if:home_delivery,1',
        //'home_delivery_time' => 'nullable|required_if:home_delivery,1',
        'home_delivery_payment_method_id' => 'nullable|required_if:home_delivery,1',
        /*
        'delivery_host_on_site' => 'nullable',
        'delivery_host_on_site_date' => 'nullable|required_if:delivery_host_on_site,1',
        'delivery_host_on_site_time' => 'nullable|required_if:delivery_host_on_site,1',
        'delivery_host_on_site_note' => 'nullable|required_if:delivery_host_on_site,1',
        'delivery_host_on_site_payment_method_id' => 'nullable|required_if:delivery_host_on_site,1',
        'delivery_host_on_site_within' => 'nullable',
        */
        'pickup_on_site_within' => 'nullable',
        'home_delivery_within' => 'nullable',
        'cancellable_within' => 'nullable',
        'confirm_reserve_within' => 'nullable',
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function orderStatus()
    {
        return $this->belongsTo(\App\Models\OrderStatus::class, 'order_status_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function shop()
    {
        return $this->belongsTo(\App\Models\Shop::class, 'shop_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function orderFoods()
    {
        return $this->hasMany(\App\Models\OrderFood::class, 'order_id');
    }

    public function orderChats()
    {
        return $this->hasMany(\App\Models\OrderChat::class, 'order_id');
    }

    public function getCanTrustAttribute()
    {
        if( Auth::check() ):
            if( Auth::user()->user_type === 'company' ):
                if( $this->orderStatus ):
                    if( in_array($this->orderStatus->name, ['confirmed', 'confirmed_with_reserve']) && $this->flag_trust_points === 'none' ):
                        return 1;
                    else:
                        return 0;
                    endif;
                else:
                    return 0;
                endif;
            else:
                return 0;
            endif;
        else:
            return 0;
        endif;
    }

    public function getCanCancelAttribute()
    {

        $can_cancel = 0;

        if( !Auth::check() ) return 0;

        if( Auth::user()->user_type === 'company' && $this->shop_id == Auth::user()->shop->id ):

            $can_cancel = 1;

        elseif( Auth::user()->user_type === 'private' && $this->user_id == Auth::id() ):

            switch($this->orderStatus->name):

                case 'not_confirmed':
                case 'confirmed':
                case 'confirmed_with_reserve':

                    if( !empty($this->cancellable_within) ):

                        if( $this->cancellable_within->isFuture() ):
                            $can_cancel = 1;
                        endif;
                    endif;

                break;

            endswitch;

        endif;

        return $can_cancel;
    }

    public function getPickupOnSiteTimeAttribute(){
        $appo = $this->attributes['pickup_on_site_time'];
        if( !empty($appo) ):
            $appo = explode(":", $appo);
            if( count($appo) === 3 ):
                unset($appo[2]);
               return implode(":", $appo);
            endif;
        endif;
    }

    public function getHomeDeliveryTimeAttribute(){
        $appo = $this->attributes['home_delivery_time'];
        if( !empty($appo) ):
            $appo = explode(":", $appo);
            if( count($appo) === 3 ):
                unset($appo[2]);
               return implode(":", $appo);
            endif;
        endif;
    }


    public function getPickupHostOnSiteTimeAttribute(){
        $appo = $this->attributes['pickup_host_on_site_time'];
        if( !empty($appo) ):
            $appo = explode(":", $appo);
            if( count($appo) === 3 ):
                unset($appo[2]);
               return implode(":", $appo);
            endif;
        endif;
    }
}
