<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderChat extends Model
{
    //
    public $table = 'order_chats';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

	public $fillable = [
		'order_id',
		'from',
		'message'
	];
	
	/**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'order_id' => 'required',
        'from' => 'required',
		'message' => 'required',
    ];
	
	/**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function order()
    {
        return $this->belongsTo(\App\Models\Order::class, 'order_id');
    }

}
