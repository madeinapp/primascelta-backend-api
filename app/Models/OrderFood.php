<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderFood extends Model
{
    public $table = 'order_foods';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

	public $fillable = [
		'order_id',
		'shop_catalog_id',
		'shop_offer_food_id',
		'food_name',
		'food_image',
		'food_description',
		'quantity',
		'price'
    ];

    protected $casts = [
        'order_id' => 'integer',
        'quantity' => 'integer',
        'food_name' => 'string',
        'price' => 'float'
    ];

	/**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'order_id' => 'required',
        'quantity' => 'required',
        'food_name' => 'required',
        'price' => 'required'
    ];

	/**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function order()
    {
        return $this->belongsTo(\App\Models\Order::class, 'order_id');
    }

    public function shopCatalog()
    {
        return $this->belongsTo(\App\Models\ShopCatalog::class, 'shop_catalog_id');
    }

    public function offerFood()
    {
        return $this->belongsTo(\App\Models\ShopOfferFood::class, 'shop_offer_food_id');
    }
}
