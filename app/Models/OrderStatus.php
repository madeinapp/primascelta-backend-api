<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderStatus extends Model
{
    public $table = 'order_status';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


	public $fillable = [
		'name',
		'description'		
	];
}
