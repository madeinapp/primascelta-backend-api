<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

/**
 * Class PaymentMethod
 * @package App\Models
 * @version May 11, 2020, 5:36 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection $shops
 * @property string $name
 */
class PaymentMethod extends Model
{

    public $table = 'payment_methods';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';




    public $fillable = [
        'name',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'description' => 'text'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required'
    ];

    public static $messages = [
        'name.required' => 'Il nome è obbligatorio'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function shops()
    {
        return $this->belogngsToMany('\App\Models\Shop');
    }
}
