<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

/**
 * Class PaymentMethod
 * @package App\Models
 * @version May 11, 2020, 5:36 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection $shops
 * @property string $name
 */
class PaymentMethodShop extends Model
{

    public $table = 'payment_method_shop';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'shop_id',        
        'payment_method_id',
        'payment_for'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [        
        'shop_id' => 'integer',
        'payment_method_id' => 'integer',
        'payment_for' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'shop_id' => 'required',
        'payment_method_id' => 'required',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function shop()
    {
        return $this->belongsTo(\App\Models\Shop::class, 'shop_id');
    }

    public function payment_method()
    {
        return $this->belongsTo(\App\Models\PaymentMethod::class, 'payment_method_id');
    }
}
