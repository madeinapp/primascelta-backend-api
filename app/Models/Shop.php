<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use Carbon\Carbon;
use Config;

/**
 * Class Shop
 * @package App\Models
 * @version June 1, 2020, 6:53 pm UTC
 *
 * @property \App\Models\ShopType $shopType
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $orders
 * @property \Illuminate\Database\Eloquent\Collection $shopCatalogs
 * @property \Illuminate\Database\Eloquent\Collection $shopOffers
 * @property \Illuminate\Database\Eloquent\Collection $subscriptions
 * @property integer $user_id
 * @property integer $agent_id
 * @property integer $shop_type_id
 * @property string $description
 * @property string $website
 * @property string $facebook_page
 * @property string $instagram_page
 * @property string $opening_timetable
 * @property integer $delivery_range_km
 * @property string $excluded_commons
 * @property integer $min_trust_points
 * @property string $tax_business_name
 * @property string $tax_type
 * @property string $tax_vat
 * @property string $tax_fiscal_code
 * @property string $country
 * @property string $address
 * @property string $route
 * @property string $city
 * @property string $region
 * @property string $postal_code
 * @property string $street_number
 * @property string $lat
 * @property string $long
 * @property string $status
 */
class Shop extends Model
{

    public $table = 'shops';

    protected $appends = ['shopType', 'shop_type', 'products_left', 'obj_opening_timetable', 'obj_delivery_opening_timetable', 'shop_status_closed'];

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'user_id',
        'agent_id',
        'website',
        'name',
        'address',
        'country',
        'route',
        'city',
        'prov',
        'region',
        'postal_code',
        'street_number',
        'lat',
        'long',
        'whatsapp',
        'show_phone_on_app',
        'facebook_page',
        'instagram_page',
        'description',
        'opening_timetable',
        'delivery_on_site',
        'delivery_address',
        'delivery_address_country',
        'delivery_address_route',
        'delivery_address_city',
        'delivery_address_region',
        'delivery_address_postal_code',
        'delivery_address_street_number',
        'delivery_address_lat',
        'delivery_address_long',
        'delivery_opening_timetable',
        'home_delivery',
        'home_delivery_min',
        'delivery_range_km',
        'delivery_range_notes',
        'delivery_host_on_site',
        'delivery_always_free',
        'delivery_free_price_greater_than',
        'delivery_forfait_cost_price',
        'delivery_percentage_cost_price',
        'delivery_cancellable_hours_limit',
        'delivery_host_on_site',
        'min_trust_points_percentage_bookable',
        'tax_business_name',
        'tax_type_id',
        'tax_vat',
        'tax_fiscal_code',
        'tax_code',
        'tax_pec',
        'tax_address',
        'tax_address_country',
        'tax_address_route',
        'tax_address_city',
        'tax_address_prov',
        'tax_address_region',
        'tax_address_postal_code',
        'tax_address_street_number',
        'tax_address_lat',
        'tax_address_long',
        'expire_date',
        'closed_date_start',
        'closed_date_end',
        'shop_status',
        'shop_status_orders',
		'admin_generated',
		'admin_sent_email_shop_created',
		'admin_test_shop',
		'admin_first_password_generated',
        'status',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'agent_id' => 'integer',
        'website' => 'string',
        'whatsapp' => 'string',
        'show_phone_on_app' => 'integer',
        'description' => 'string',
        'facebook_page' => 'string',
        'instagram_page' => 'string',
        'opening_timetable' => 'string',
        'min_trust_points_percentage_bookable' => 'integer',
        'tax_business_name' => 'string',
        'tax_type' => 'string',
        'tax_vat' => 'string',
        'tax_fiscal_code' => 'string',
        'address' => 'string',
        'country' => 'string',
        'route' => 'string',
        'city' => 'string',
        'prov' => 'string',
        'region' => 'string',
        'postal_code' => 'string',
        'street_number' => 'string',
        'lat' => 'string',
        'long' => 'string',
        'status' => 'string',
        'delivery_free_price_greater_than' => 'integer',
        'delivery_forfait_cost_price' => 'integer',
        'delivery_percentage_cost_price' => 'integer',
        'delivery_on_site' => 'integer',
        'home_delivery' => 'integer',
        'delivery_host_on_site' => 'integer',
        'closed_date_start' => 'date',
        'closed_date_end' => 'date',
        'shop_status' => 'string',
        'shop_status_orders' => 'integer',
        'tax_pec' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        //'user_id' => 'required',
        'address' => 'required',
        'tax_business_name' => 'required',
        'tax_vat' => 'required',
        'tax_fiscal_code' => ['required', 'regex:/^([A-Z]{6}[0-9]{2}[A-Z][0-9]{2}[A-Z][0-9]{3}[A-Z])|(\d{11})$/'],
        'tax_type_id' => 'required|integer',
        'name' => 'required',
        'delivery_free_price_greater_than' => 'nullable|regex:/^\d*(\.\d{1,2})?$/',
        'delivery_forfait_cost_price' => 'nullable|regex:/^\d*(\.\d{1,2})?$/',
        'delivery_percentage_cost_price' => 'nullable|integer'
    ];

	/**
     * Validation messages
     *
     * @var array
     */
    public static $messages = [
        //'user_id' => 'required',
        'address.required' => 'L\'indirizzo è obbligatorio',
        'tax_business_name.required' => 'La ragione sociale è obbligatoria',
        'tax_vat.required' => 'La partiva iva è obbligatoria',
        'tax_fiscal_code.required' => 'Il codice fiscale è obbligatorio',
        'tax_fiscal_code.regex' => 'Il codice fiscale non è valido',
        'tax_type_id.required' => 'Selezionare la tipologia di Attività',
        'name.required' => 'Il nome dell\'attività è obbligatorio',
        'delivery_free_price_greater_than.regex' => 'Valore errato',
        'delivery_forfait_cost_price.regex' => 'Valore costi di spedizione errato',
        'delivery_percentage_cost_price.integer' => 'Percentuale costi di spedizione errata',
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function shopTypes()
    {
        //return $this->belongsTo(\App\Models\ShopType::class, 'shop_type_id');
        return $this->belongsToMany(\App\Models\ShopType::class, \App\Models\ShopShopType::class);
    }

    public function getShopTypeAttribute()
    {
        return implode(' - ', $this->shopTypes->pluck('name')->toArray());
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }


    public function agent()
    {
        return $this->belongsTo(\App\Models\Agent::class, 'agent_id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function orders()
    {
        return $this->hasMany(\App\Models\Order::class, 'shop_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function shopCatalogs()
    {
        return $this->hasMany(\App\Models\ShopCatalog::class, 'shop_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function shopOffers()
    {
        return $this->hasMany(\App\Models\ShopOffer::class, 'shop_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function subscriptions()
    {
        return $this->hasMany(\App\Models\Subscription::class, 'shop_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function TaxType()
    {
        return $this->belongsTo(\App\Models\TaxType::class, 'tax_type_id');
    }


    public function PaymentMethodsShop()
    {
        return $this->belongsToMany(\App\Models\PaymentMethod::class, 'payment_method_shop')->whereNull('payment_for');
    }

    public function PaymentMethodsDelivery()
    {
        return $this->belongsToMany(\App\Models\PaymentMethod::class, 'payment_method_shop')->where('payment_for', 'delivery');
    }

    public function getProductsLeftAttribute()
    {
        $catalog = $this->shopCatalogs->count();
        $subscription_type = $this->user->subscription->subscription;
        $diff = null;

        if( $subscription_type === 'trial' ):

            $diff = 1000 - $catalog;

        else:

            $objSubscription = Subscription::where('subscription', $subscription_type)->first();

            if( $objSubscription ):
				$pn = $objSubscription->products_num;
				if(!empty($this->user->subscription->subscription_product_num)){
					$pn = $this->user->subscription->subscription_product_num;
				}
                $diff = $pn - $catalog;
            else:
                return 0;
            endif;

        endif;

        return $diff;
    }

    public function getObjOpeningTimetableAttribute()
    {
        $empty_timetable = json_decode('{"monday":{"active":false,"first_half":{"opening_time":"0:00","closing_time":"18:00"},"second_half":{"opening_time":"12:00","closing_time":"24:00"}},"tuesday":{"active":false,"first_half":{"opening_time":"0:00","closing_time":"18:00"},"second_half":{"opening_time":"12:00","closing_time":"24:00"}},"friday":{"active":false,"first_half":{"opening_time":"0:00","closing_time":"18:00"},"second_half":{"opening_time":"12:00","closing_time":"24:00"}},"saturday":{"active":false,"first_half":{"opening_time":"0:00","closing_time":"18:00"},"second_half":{"opening_time":"12:00","closing_time":"24:00"}},"sunday":{"active":false,"first_half":{"opening_time":"0:00","closing_time":"18:00"},"second_half":{"opening_time":"12:00","closing_time":"24:00"}},"wednesday":{"active":false,"first_half":{"opening_time":"0:00","closing_time":"18:00"},"second_half":{"opening_time":"12:00","closing_time":"24:00"}},"thursday":{"active":false,"first_half":{"opening_time":"0:00","closing_time":"18:00"},"second_half":{"opening_time":"12:00","closing_time":"24:00"}}}');

        if( !empty($this->opening_timetable) ){

            $opening_timetable = (array)json_decode($this->opening_timetable);

            ksort($opening_timetable);
            $opening_timetable = json_decode(json_encode($opening_timetable));

            $weekMap = [
                0 => 'sunday',
                1 => 'monday',
                2 => 'tuesday',
                3 => 'wednesday',
                4 => 'thursday',
                5 => 'friday',
                6 => 'saturday',
            ];

            foreach($weekMap as $weekday){
                if( $opening_timetable->{$weekday}->active === false ){
                    $opening_timetable->{$weekday} = $empty_timetable->{$weekday};
                }
            }

            return $opening_timetable ?? $empty_timetable;
        }else{
            return $empty_timetable;
        }
    }

    public function getObjDeliveryOpeningTimetableAttribute()
    {
        $empty_timetable = json_decode('{"monday":{"active":false,"first_half":{"opening_time":"0:00","closing_time":"18:00"},"second_half":{"opening_time":"12:00","closing_time":"24:00"}},"tuesday":{"active":false,"first_half":{"opening_time":"0:00","closing_time":"18:00"},"second_half":{"opening_time":"12:00","closing_time":"24:00"}},"friday":{"active":false,"first_half":{"opening_time":"0:00","closing_time":"18:00"},"second_half":{"opening_time":"12:00","closing_time":"24:00"}},"saturday":{"active":false,"first_half":{"opening_time":"0:00","closing_time":"18:00"},"second_half":{"opening_time":"12:00","closing_time":"24:00"}},"sunday":{"active":false,"first_half":{"opening_time":"0:00","closing_time":"18:00"},"second_half":{"opening_time":"12:00","closing_time":"24:00"}},"wednesday":{"active":false,"first_half":{"opening_time":"0:00","closing_time":"18:00"},"second_half":{"opening_time":"12:00","closing_time":"24:00"}},"thursday":{"active":false,"first_half":{"opening_time":"0:00","closing_time":"18:00"},"second_half":{"opening_time":"12:00","closing_time":"24:00"}}}');

        if( !empty($this->delivery_opening_timetable) ){

            $delivery_opening_timetable = (array)json_decode($this->delivery_opening_timetable);

            ksort($delivery_opening_timetable);
            $delivery_opening_timetable = json_decode(json_encode($delivery_opening_timetable));

            $weekMap = [
                0 => 'sunday',
                1 => 'monday',
                2 => 'tuesday',
                3 => 'wednesday',
                4 => 'thursday',
                5 => 'friday',
                6 => 'saturday',
            ];

            foreach($weekMap as $weekday){
                if( $delivery_opening_timetable->{$weekday}->active === false ){
                    $delivery_opening_timetable->{$weekday} = $empty_timetable->{$weekday};
                }
            }

            return $delivery_opening_timetable ?? $empty_timetable;
        }else{
            return $empty_timetable;
        }
    }

    public function getShopStatusClosedAttribute(){

        $stato_negozio = Config::get('constants.CHIUSO');

        $weekMap = [
			0 => 'sunday',
			1 => 'monday',
			2 => 'tuesday',
			3 => 'wednesday',
			4 => 'thursday',
			5 => 'friday',
			6 => 'saturday',
		];

        $weekday = $weekMap[Carbon::now()->dayOfWeek];

        $morning_open    = !empty($this->obj_opening_timetable->{$weekday}->first_half->opening_time) ? Carbon::parse($this->obj_opening_timetable->{$weekday}->first_half->opening_time) : null;
        $morning_close   = !empty($this->obj_opening_timetable->{$weekday}->first_half->closing_time) ? Carbon::parse($this->obj_opening_timetable->{$weekday}->first_half->closing_time) : null;
        $afternoon_open  = !empty($this->obj_opening_timetable->{$weekday}->second_half->opening_time) ? Carbon::parse($this->obj_opening_timetable->{$weekday}->second_half->opening_time) : null;
        $afternoon_close = !empty($this->obj_opening_timetable->{$weekday}->second_half->closing_time) ? Carbon::parse($this->obj_opening_timetable->{$weekday}->second_half->closing_time) : null;

        $oggi = Carbon::now();

        $giorno_attivo = $this->obj_opening_timetable->{$weekday}->active;
                                    if( $giorno_attivo ){
                                        if( ($oggi->gt($morning_open) && $oggi->lt($morning_close)) ||
                                            ($oggi->gt($afternoon_open) && $oggi->lt($afternoon_close))
                                            ){
                                            $stato_negozio=Config::get('constants.APERTO');
                                        }else{
                                            $stato_negozio=Config::get('constants.CHIUSO');
                                        }
                                    }else{
                                        $stato_negozio=Config::get('constants.CHIUSO');
                                    }

        return $stato_negozio;

    }
}
