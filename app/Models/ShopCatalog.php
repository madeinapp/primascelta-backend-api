<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Validation\Rule;

use Carbon\Carbon;


/**
 * Class ShopCatalog
 * @package App\Models
 * @version June 23, 2020, 9:51 am UTC
 *
 * @property \App\Models\FoodCategory $foodCategory
 * @property \App\Models\Shop $shop
 * @property \App\Models\UnitOfMeasure $unitOfMeasure
 * @property \Illuminate\Database\Eloquent\Collection $shopOfferFoods
 * @property integer $shop_id
 * @property integer $food_category_id
 * @property integer $unit_of_measure_id
 * @property number $food_unit_of_measure_quantity
 * @property integer $food_id
 * @property string $food_name
 * @property string $food_description
 * @property string $food_type
 * @property number $food_price
 * @property string $food_image
 * @property boolean $visible_in_search
 */
class ShopCatalog extends Model
{

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public $table = 'shop_catalogs';

    protected $appends = ['isOffer'];

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'shop_id',
        'food_category_id',
        'unit_of_measure_id',
        'food_unit_of_measure_quantity',
        'unit_of_measure_buy',
        'unit_of_measure_buy_quantity',
        'food_id',
        'food_name',
        'food_description',
        'food_type',
        'food_price',
        'food_image',
        'visible_in_search',
        'home_delivery',
        'in_app_shopping',
        'short_description',
        'uncertain_weight'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'shop_id' => 'integer',
        'food_category_id' => 'integer',
        'unit_of_measure_id' => 'integer',
        'food_unit_of_measure_quantity' => 'float',
        'unit_of_measure_buy' => 'integer',
        'unit_of_measure_buy_quantity' => 'float',
        'food_id' => 'integer',
        'food_name' => 'string',
        'food_description' => 'string',
        'food_type' => 'string',
        'food_price' => 'numeric',
        'food_image' => 'string',
        'visible_in_search' => 'boolean',
        'home_delivery' => 'boolean',
        'in_app_shopping' => 'boolean',
        'short_description' => 'string',
        'food_category_id' => 'required',
        'food_name' => 'string',
        'food_type' => 'string',
        'food_price' => 'numeric',
        'short_description' => 'string',
        'uncertain_weight' => 'integer'
    ];


	/**
     * Validation messages
     *
     * @var array
     */
    public static $messages = [
        'shop_id.required' => 'L\'utente non risulta collegato a nessuna attività',
        'food_category_id.required' => 'Il genere è obbligatorio',
        'food_name.required' => 'Il nome del prodotto è obbligatorio',
        'food_name.unique' => 'Il nome prodotto è già utilizzato',
        'food_type.required' => 'La tipologia è obbligatoria',
        'food_price.required' => 'Il prezzo è obbligatorio',
        'food_price.between' => 'Il prezzo digitato non è corretto. Il valore massimo accettato è 999.999,99',
        'short_description.max' => 'La descrizione breve deve essere di max 30 caratteri'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function foodCategory()
    {
        return $this->belongsTo(\App\Models\FoodCategory::class, 'food_category_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function shop()
    {
        return $this->belongsTo(\App\Models\Shop::class, 'shop_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function unitOfMeasure()
    {
        return $this->belongsTo(\App\Models\UnitOfMeasure::class, 'unit_of_measure_id');
    }

    public function unitOfMeasureBuy()
    {
        return $this->belongsTo(\App\Models\UnitOfMeasure::class, 'unit_of_measure_buy');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function shopOfferFoods()
    {
        return $this->hasMany(\App\Models\ShopOfferFood::class, 'shop_catalog_id');
    }

    public function food()
    {
        return $this->belongsTo(\App\Models\Food::class, 'food_id');
    }

    public function specials()
    {
        return $this->belongsToMany('App\Models\Special', 'shop_catalog_specials');
    }

    public function getIsOfferAttribute(){
        $shop = Shop::find($this->shop_id);
        foreach( $shop->shopOffers as $shop_offer){
            if( $shop_offer->status === 'available' && $shop_offer->start_date->lte(Carbon::now()) && $shop_offer->end_date->gte(Carbon::now())){
                foreach($shop_offer->shopOfferFoods as $shop_offer_food){
                    if($this->id == $shop_offer_food->shop_catalog_id){
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
