<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShopCatalogSpecial extends Model
{
    protected $table = 'shop_catalog_specials';

    public $fillable = [
        'shop_catalog_id',
        'special_id'
    ];

    public $cast = [
        'shop_catalog_id' => 'integer',
        'special_id' => 'integer'
    ];

    public $rules = [
        'shop_catalog_id' => 'required',
        'special_id' => 'required'
    ];

    public $messages = [
        'shop_catalog_id.required' => 'Food id è obbligatorio',
        'special_id.required' => 'Special id è obbligatorio'
    ];
}
