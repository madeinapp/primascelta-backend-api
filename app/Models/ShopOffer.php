<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ShopOffer
 * @package App\Models
 * @version June 26, 2020, 6:28 pm UTC
 *
 * @property \App\Models\Shop $shop
 * @property \Illuminate\Database\Eloquent\Collection $shopOfferFoods
 * @property integer $shop_id
 * @property string $name
 * @property string $description
 * @property string $image_path
 * @property string $start_date
 * @property string $end_date
 * @property integer $count_purchased
 * @property string $status
 */
class ShopOffer extends Model
{

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public $table = 'shop_offers';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'shop_id',
        'name',
        'description',
        'image_path',
        'start_date',
        'end_date',
        'count_purchased',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'shop_id' => 'integer',
        'name' => 'string',
        'description' => 'string',
        'image_path' => 'string',
        'start_date' => 'date',
        'end_date' => 'date',
        'count_purchased' => 'integer',
        'status' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'shop_id' => 'required',
        'start_date' => 'required|date',
        'end_date' => 'required|date|after_or_equal:start_date'
    ];

    /**
     * Validation messages
     *
     * @var array
     */
    public static $messages = [
        'start_date.required' => 'Inserire la data inizio',
        'end_date.required' => 'Inserire la data fine',
        'end_date.after_or_equal' => 'La data fine deve essere uguale o successiva alla data inizio',
        'name.required' => 'Titolo offerta assente',
        'catalog_id.required' => 'Selezionare almeno un prodotto dal catalogo',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function shop()
    {
        return $this->belongsTo(\App\Models\Shop::class, 'shop_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function shopOfferFoods()
    {
        return $this->hasMany(\App\Models\ShopOfferFood::class, 'shop_offer_id');
    }
}
