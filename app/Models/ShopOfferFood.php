<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ShopOfferFood
 * @package App\Models
 * @version June 26, 2020, 7:54 pm UTC
 *
 * @property \App\Models\ShopCatalog $shopCatalog
 * @property \App\Models\ShopOffer $shopOffer
 * @property integer $shop_offer_id
 * @property integer $shop_catalog_id
 * @property string $food_name
 * @property string $food_image
 * @property string $food_description
 * @property integer $min_quantity
 * @property number $discounted_price
 */
class ShopOfferFood extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public $table = 'shop_offer_foods';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'shop_offer_id',
        'shop_catalog_id',
        'food_name',
        'food_image',
        'food_description',
        'min_quantity',
        'discount',
        'discounted_price',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'shop_offer_id' => 'integer',
        'shop_catalog_id' => 'integer',
        'food_name' => 'string',
        'food_image' => 'string',
        'food_description' => 'string',
        'min_quantity' => 'integer',
        'discount' => 'float',
        'discounted_price' => 'float',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'shop_offer_id' => 'required',
        'shop_catalog_id' => 'required',
        'food_name' => 'required',
        'min_quantity' => 'required',
        'discount' => 'required|numeric|min:0',
        'discounted_price' => 'required|numeric'
    ];


    public static $messages = [
        'shop_offer_id.required' => 'Id offerta assente',
        'shop_catalog_id.required' => 'Id catalogo assente',
        'food_name.required' => 'Nome prodotto assente',
        'min_quantity.required' => 'Quantità minima assente',
        'discount.required' => 'Sconto assente',
        'discount.numeric' => 'Lo sconto deve essere numerico',
        'discount.min' => 'Lo sconto non può essere negativo!',
        'discounted_price.required' => 'Prezzo scontato assente',
        'discounted_price.numeric' => 'Il prezzo scontato deve essere numerico'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function shopCatalog()
    {
        return $this->belongsTo(\App\Models\ShopCatalog::class, 'shop_catalog_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function shopOffer()
    {
        return $this->belongsTo(\App\Models\ShopOffer::class, 'shop_offer_id');
    }
}
