<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShopShopType extends Model
{
    public $table = 'shop_shop_types';

    public $fillable = [
        'shop_id',
        'shop_type_id'
    ];

    public $cast = [
        'shop_id' => 'integer',
        'shop_type_id' => 'integer'
    ];

    public $rules = [
        'shop_id' => 'required',
        'shop_type_id' => 'required'
    ];

    public $messages = [
        'shop_id.required' => 'Food id è obbligatorio',
        'shop_type_id.required' => 'Special id è obbligatorio'
    ];
}
