<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

/**
 * Class ShopType
 * @package App\Models
 * @version May 8, 2020, 10:43 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection $shops
 * @property string $name
 */
class ShopType extends Model
{

    public $table = 'shop_types';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';




    public $fillable = [
        'name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required'
    ];

    public static $messages = [
        'name.required' => 'Il nome è obbligatorio'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function shops()
    {
        return $this->belongsToMany(\App\Models\Shop::class, \App\Models\ShopShopType::class);
    }
}
