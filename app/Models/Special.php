<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Special extends Model
{
    protected $table = 'specials';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'name',
        'icon'
    ];

    public $cast = [
        'name' => 'string',
        'icon' => 'string'
    ];

    public $rules = [
        'name' => 'required'
    ];

    public $messages = [
        'name.required' => 'Il nome è obbligatorio'
    ];

    public function foods()
    {
        return $this->hasMany(App\Models\Food::class);
    }

    public function shopCatalogs()
    {
        return $this->hasMany(App\Models\ShopCatalog::class);
    }
}
