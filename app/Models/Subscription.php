<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    public $table = 'subscriptions';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'description',
        'name',
        'shops_num',
        'shops_note',
        'products_num',
        'products_note',
        'description_list',
        'price_yearly',
        'price_half_yearly'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'subscription' => 'string',
        'name' => 'string',
        'shops_num' => 'integer',
        'shops_note' => 'string',
        'products_num' => 'integer',
        'products_note' => 'string',
        'description_list' => 'string',
        'price_yearly' => 'float',
        'price_half_yearly' => 'float'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'subscription' => 'required',
        'name' => 'required',
        'shops_num' => 'required',
        'products_num' => 'required',
        'description_list' => 'required',
        'price_yearly' => 'required',
        'price_half_yearly' => 'required'
    ];


    public static $messages = [
        'subscription.required' => 'Il codice abbonamento è obbligatorio',
        'name.required' => 'Il nome è obbligatorio',
        'shops_num.required' => 'Il numero delle attività è obbligatorio',
        'products_num.required' => 'Il numero dei prodotti è obbligatorio',
        'description_list.required' => 'La descrizione è obbligatoria',
        'price_yearly.required' => 'Il prezzo annuale è obbligatorio',
        'price_half_yearly.required' => 'Il prezzo semestrale è obbligatorio'
    ];

    public function subscriptionFeatures()
    {
        return $this->hasMany(\App\Models\SubscriptionFeature::class, 'subscription_id')->orderBy('order');
    }
}
