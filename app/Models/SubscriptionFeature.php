<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubscriptionFeature extends Model
{
    public $table = 'subscription_features';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'subscription_id',
        'feature_name',
        'feature_note',
        'order'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'subscription_id' => 'integer',
        'feature_name' => 'string',
        'feature_note' => 'string',
        'order' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'subscription_id' => 'required',
        'feature_name' => 'required'
    ];


    public static $messages = [
        'subscription_id.required' => 'L\'ID abbonamento è oobbligatorio',
        'feature_name.required' => 'Il nome della feature è obbligatorio'
    ];

}
