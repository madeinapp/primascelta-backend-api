<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

/**
 * Class TaxType
 * @package App\Models
 * @version June 10, 2020, 10:43 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection $tax_type
 * @property string $name
 */
class TaxType extends Model
{

    public $table = 'tax_types';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';




    public $fillable = [
        'name',
        'description',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'description' => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function shops()
    {
        return $this->hasMany('\App\Models\Shop', 'tax_type_id', 'id');
    }
}
