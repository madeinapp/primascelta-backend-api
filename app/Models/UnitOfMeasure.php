<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

/**
 * Class UnitOfMeasure
 * @package App\Models
 * @version May 8, 2020, 10:42 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection $foods
 * @property \Illuminate\Database\Eloquent\Collection $shopCatalogs
 * @property string $name
 */
class UnitOfMeasure extends Model
{

    public $table = 'unit_of_measures';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';




    public $fillable = [
        'name',
        'next_unit_of_measure',
        'next_unit_of_measure_quantity',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'next_unit_of_measure' => 'integer',
        'next_unit_of_measure_quantity' => 'integer',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required'
    ];

    public static $messages = [
        'name.required' => 'Il nome è obbligatorio'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function foods()
    {
        return $this->hasMany(\App\Models\Food::class, 'unit_of_measure_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function shopCatalogs()
    {
        return $this->hasMany(\App\Models\ShopCatalog::class, 'unit_of_measure_id');
    }

    public function shopCatalogsBuy()
    {
        return $this->hasMany(\App\Models\ShopCatalog::class, 'unit_of_measure_buy');
    }

    public function foodCategories()
    {
        return $this->hasMany(\App\Models\FoodCategory::class, 'unit_of_measure');
    }

    public function nextUnitOfMeasure()
    {
        return $this->belongsTo(\App\Models\UnitOfMeasure::class, 'next_unit_of_measure');
    }
}
