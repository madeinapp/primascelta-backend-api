<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use App\Rules\StrongPassword;

class User extends Authenticatable
{

    public $table = 'users';

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
                            'email',
                            'password',
                            'facebook_id',
                            'api_token',
                            'otp',
                            'name',
			                'birthdate',
                            'phone',
                            'other_phone',
                            'delivery_address',
                            'delivery_country',
                            'delivery_route',
                            'delivery_city',
                            'delivery_region',
                            'delivery_postal_code',
                            'delivery_street_number',
                            'delivery_lat',
                            'delivery_long',
                            'note_delivery_address',
                            'image_url',
                            'user_type',
                            'trust_points',
                            'role',
                            'status',
                            'privacy',
                        ];

    /**
     * User type
     *
     * @var array
     */
    public static $user_type = ['private' => 'private', 'company' => 'company'];

    /**
     * User status
     *
     * @var array
     */
    public static $user_status = ['active' => 'active', 'inactive' => 'inactive', 'pending' => 'pending'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new \App\Notifications\MailResetPasswordNotification($token));
    }


    public function routeNotificationForOneSignal()
    {
        return $this->device_tokens->pluck('token')->toArray();
    }

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
		'email' => 'required|email|unique:users',
        'password' =>['required|confirmed|min:8'],
        'facebook_id' => 'unique:users',
		'birthdate' => 'date|required'
    ];

    /*
     * The list of user type
     *
     * #var array
     */
    static public function getUserType()
    {
            return self::$user_type;
    }

    /*
     * The list of user status
     *
     * #var array
     */
    static public function getUserStatus()
    {
            return self::$user_status;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     **/
    public function shop()
    {
        return $this->hasOne('\App\Models\Shop', 'user_id', 'id');
    }

	/**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     **/
    public function subscription()
    {
        return $this->hasOne('\App\Models\UserSubscription', 'user_id', 'id');
    }

    public function blacklistShops()
    {
        return $this->belongsToMany('\App\Models\Shop', 'blacklist_shops');
    }

    public function favouriteShops()
    {
        return $this->belongsToMany('\App\Models\Shop', 'favourite_shops');
    }

    public function favouriteShopCatalogs()
    {
        return $this->belongsToMany('\App\Models\ShopCatalog', 'favourite_shop_catalogs');
    }

    public function adminlte_image()
    {
        return (!empty($this->image_url)) ? getResizedImage(getOriginalImage($this->image_url), '100x100') . '?p=' . rand(10000, 99999) : url('/images/Icona-utente_100.png');
    }

    public function adminlte_desc()
    {
       return ($this->role == "admin") ? 'Amministratore del sito' : 'Responsabile di negozio : "' . ($this->shop ? $this->shop->name : '') .'"';
    }

    public function adminlte_profile_url()
    {
        return 'profile/edit';
    }

    public function device_tokens()
    {
        return $this->hasMany(\App\Models\DeviceToken::class, 'user_id');
    }

    public function orders()
    {
        return $this->hasMany(\App\Models\Order::class, 'user_id');
    }

    public function invoices()
    {
        return $this->hasMany(\App\Models\Invoice::class, 'user_id');
    }

    public function delivered()
    {
        return $this->hasMany(\App\Models\Order::class, 'user_id')->where('order_status_id', 6);
    }
}
