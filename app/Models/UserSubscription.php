<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserSubscription extends Model
{

    public $table = 'user_subscriptions';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

	public $fillable = [
		'user_id',
		'subscription',
		'submission_date',
        'expire_date',
        'invoice_id',
        'parent'
	];

	protected $casts = [
		'user_id' => 'integer',
		'subscription' => 'string',
		'submission_date' => 'datetime',
        'expire_date' => 'datetime',
        'invoice_id' => 'integer',
        'parent' => 'integer'
	];

	//Subscription -> max producs
	public static $subscriptions = [
							'free' => null,
							'trial' => null,
							'standard' => 100,
							'premium' => 200,
							'enterprise' => 400,
						];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
	public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }

    public function parentSubscription(){
        return $this->belongsTo(\App\Models\UserSubscription::class, 'parent');
    }
}
