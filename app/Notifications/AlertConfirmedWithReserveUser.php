<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

use NotificationChannels\OneSignal\OneSignalChannel;
use NotificationChannels\OneSignal\OneSignalMessage;
use NotificationChannels\OneSignal\OneSignalButton;
use NotificationChannels\OneSignal\OneSignalWebButton;

class AlertConfirmedWithReserveUser extends Notification
{
    use Queueable;

    private $order;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($order)
    {
        $this->order = $order;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail','database', OneSignalChannel::class];
    }

    public function toDatabase()
    {
        $order = [];
        $order['id'] = $this->order->id;
        $order['created_at'] = $this->order->created_at;
        $order['order_status_id'] = $this->order->created_at;
        $order['shop']['id'] = $this->order->shop->id;
        $order['shop']['name'] = $this->order->shop->name;

        return [
            'order' => $order
        ];
    }

    public function toOneSignal($notifiable)
    {

        return OneSignalMessage::create()
                ->setSubject("Ordine #" . $this->order->id . " cancellato automaticamente")
                ->setBody('Non hai confermato la riserva entro la data/ora indicata: ' . $this->order->confirm_reserve_within->format('d/m/Y') . ' alle ' . $this->order->confirm_reserve_within->format('H:i'))
                ->setParameter('android_channel_id', "faedcca6-5b78-4bb0-9fb1-f7b78ffd38fb")
                ->setParameter('ios_sound', "sound_file.wav")
                ->setData('order', $this->order->id)
                ->setWebButton(
                    OneSignalWebButton::create('link-2')
                        ->text('Clicca qui')
                        ->icon('https://backend.primascelta.biz/images/logo-login.png')
                        ->url('https://backend.primascelta.biz/')
                );

    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {

        return (new MailMessage)
                    ->subject("ORDINE #" . $this->order->id . " CANCELLATO AUTOMATICAMENTE")
                    ->markdown('emails.alert_confirmed_with_reserve', ['user' => $notifiable, 'order' => $this->order])
                    ;

    }
}
