<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

use NotificationChannels\OneSignal\OneSignalChannel;
use NotificationChannels\OneSignal\OneSignalMessage;
use NotificationChannels\OneSignal\OneSignalButton;
use NotificationChannels\OneSignal\OneSignalWebButton;

class AlertNotConfirmed extends Notification
{
    use Queueable;

    private $order;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($order)
    {
        $this->order = $order;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail','database', OneSignalChannel::class];
    }

    public function toDatabase()
    {
        $order = [];
        $order['id'] = $this->order->id;
        $order['created_at'] = $this->order->created_at;
        $order['order_status_id'] = $this->order->created_at;
        $order['shop']['id'] = $this->order->shop->id;
        $order['shop']['name'] = $this->order->shop->name;

        return [
            'order' => $order
        ];
    }

    public function toOneSignal($notifiable)
    {
        if( $this->order->pickup_on_site == 1 ):
            $date_expire = $this->order->pickup_on_site_date->format('d/m/Y') . ' alle ' . $this->order->pickup_on_site_time;
        elseif( $this->order->home_delivery == 1 ):
            $date_expire = $this->order->home_delivery_date->format('d/m/Y') . ' alle ' . $this->order->home_delivery_time;
        elseif( $this->order->delivery_host_on_site == 1 ):
            $date_expire = $this->order->delivery_host_on_site_date->format('d/m/Y') . ' alle ' . $this->order->delivery_host_on_site_time;
        endif;

        return OneSignalMessage::create()
                ->setSubject("Ordine #" . $this->order->id . " non ancora confermato")
                ->setBody('Hai tempo fino al ' . $date_expire)
                ->setParameter('android_channel_id', "faedcca6-5b78-4bb0-9fb1-f7b78ffd38fb")
                ->setParameter('ios_sound', "sound_file.wav")
                ->setData('order', $this->order->id)
                ->setWebButton(
                    OneSignalWebButton::create('link-2')
                        ->text('Clicca qui')
                        ->icon('https://backend.primascelta.biz/images/logo-login.png')
                        ->url('https://backend.primascelta.biz/')
                );

    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {

        return (new MailMessage)
                    ->subject("NON HAI ANCORA CONFERMATO L'ORDINE #" . $this->order->id)
                    ->markdown('emails.alert_not_confirmed', ['user' => $notifiable, 'order' => $this->order])
                    ;

    }
}
