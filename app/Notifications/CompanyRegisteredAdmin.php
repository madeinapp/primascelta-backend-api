<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

use NotificationChannels\OneSignal\OneSignalChannel;
use NotificationChannels\OneSignal\OneSignalMessage;
use NotificationChannels\OneSignal\OneSignalButton;
use NotificationChannels\OneSignal\OneSignalWebButton;

class CompanyRegisteredAdmin extends Notification
{
    use Queueable;

    private $company;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($company)
    {
        $this->company = $company;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database', OneSignalChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject("NUOVA AZIENDA ISCRITTA SU PRIMASCELTA")
                    ->markdown('emails.company_registered_admin', ['company' => $this->company])
                    ;
    }

    /** Send push notification */
    public function toOneSignal($notifiable)
    {
        return OneSignalMessage::create()
            ->setSubject("NUOVA AZIENDA ISCRITTA SU PRIMASCELTA")
            ->setBody("Si è registrato l'utente " . $this->company->name . " titolare dell'attività " . $this->company->shop->name)
            ->setParameter('android_channel_id', "faedcca6-5b78-4bb0-9fb1-f7b78ffd38fb")
            ->setParameter('ios_sound', "sound_file.wav")
            ->setWebButton(
                OneSignalWebButton::create('link-2')
                    ->text('Clicca qui')
                    ->icon('https://backend.primascelta.biz/images/logo-login.png')
                    ->url('https://backend.primascelta.biz/')
            );

    }


    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'company' => $this->company,
            'shop' => $this->company->shop
        ];
    }
}
