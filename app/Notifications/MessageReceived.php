<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

use NotificationChannels\OneSignal\OneSignalChannel;
use NotificationChannels\OneSignal\OneSignalMessage;
use NotificationChannels\OneSignal\OneSignalButton;
use NotificationChannels\OneSignal\OneSignalWebButton;

use App\Models\User;

class MessageReceived extends Notification
{
    use Queueable;


    private $user, $order, $message, $from;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $order, $message, $from)
    {
        $this->user = $user;
        $this->order = $order;
        $this->message = $message;
        $this->from = $from;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database', OneSignalChannel::class];
    }

    public function toDatabase()
    {
        $user = [];
        $order = [];

        $user['id'] = $this->user->id ?? null;
        $user['name'] = $this->user->name ?? null;

        $order['id'] = $this->order->id;
        $order['created_at'] = $this->order->created_at;
        $order['order_status_id'] = $this->order->created_at;
        $order['shop']['id'] = $this->order->shop->id;
        $order['shop']['name'] = $this->order->shop->name;

        return [
            'user' => $user,
            'order' => $order,
            'message' => $this->message,
            'from' => $this->from
        ];
    }

    public function toOneSignal($notifiable)
    {
        $body = ( $this->from == 1 ? $this->order->shop->name : $this->user->name ) . ' ha scritto: "' . $this->message . '"';

        return OneSignalMessage::create()
            ->setSubject("Hai ricevuto un messaggio")
            ->setBody($body)
            ->setData('order', $this->order->id)
            ->setData('user', $this->user->user_type)
            ->setParameter('android_channel_id', "faedcca6-5b78-4bb0-9fb1-f7b78ffd38fb")
            ->setParameter('ios_sound', "sound_file.wav")
            ->setWebButton(
                OneSignalWebButton::create('link-2')
                    ->text('Clicca qui')
                    ->icon('https://backend.primascelta.biz/images/logo-login.png')
                    ->url('https://backend.primascelta.biz/')
            );

    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {

        $notifiableUser = User::find($notifiable->id);

        return (new MailMessage)
                    ->subject("Hai ricevuto un messaggio")
                    ->markdown('emails.message_received', ['user' => $notifiable,
                                                           'user_action' => $this->user,
                                                           'order' => $this->order,
                                                           'message' => $this->message,
                                                           'from' => $this->from ?? 1
                                                           ]);
    }
}
