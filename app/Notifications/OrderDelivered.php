<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

use NotificationChannels\OneSignal\OneSignalChannel;
use NotificationChannels\OneSignal\OneSignalMessage;
use NotificationChannels\OneSignal\OneSignalButton;
use NotificationChannels\OneSignal\OneSignalWebButton;

use App\Models\User;

class OrderDelivered extends Notification
{
    use Queueable;

    private $user, $order, $add_trust_points, $trust_points;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $order, $add_trust_points, $trust_points)
    {
        $this->user = $user;
        $this->order = $order;
        $this->add_trust_points = $add_trust_points;
        $this->trust_points = $trust_points;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database', OneSignalChannel::class];
    }

    public function toDatabase()
    {
        $user = [];
        $order = [];

        $user['id'] = $this->user->id ?? null;
        $user['name'] = $this->user->name ?? null;

        $order['id'] = $this->order->id;
        $order['created_at'] = $this->order->created_at;
        $order['order_status_id'] = $this->order->created_at;
        $order['shop']['id'] = $this->order->shop->id;
        $order['shop']['name'] = $this->order->shop->name;


        return [
            'user' => $user,
            'order' => $order,
            'add_trust_points' => $this->add_trust_points,
            'trust_points' => $this->trust_points
        ];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {

        return (new MailMessage)
                    ->subject("ORDINE #" . $this->order->id . " EVASO CORRETTAMENTE")
                    ->markdown('emails.order_delivered', ['user' => $notifiable,
                                                         'order' => $this->order,
                                                         'add_trust_points' => $this->add_trust_points,
                                                         'trust_points' => $this->trust_points
                                                         ]);
    }


    /** Send push notification */
    public function toOneSignal($notifiable)
    {
        return OneSignalMessage::create()
            ->setSubject("Ciao " . $notifiable->name . ", il tuo ordine #" . $this->order->id . " è stato evaso correttamente.")
            ->setData('order', $this->order->id)
            ->setBody($this->order->shop->name . " ti ringrazia e ti aspetta per la prossima prenotazione!")
            ->setParameter('android_channel_id', "faedcca6-5b78-4bb0-9fb1-f7b78ffd38fb")
            ->setParameter('ios_sound', "sound_file.wav")
            ;

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
