<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

use NotificationChannels\OneSignal\OneSignalChannel;
use NotificationChannels\OneSignal\OneSignalMessage;
use NotificationChannels\OneSignal\OneSignalButton;
use NotificationChannels\OneSignal\OneSignalWebButton;

class OrderModifiedUser extends Notification
{
    use Queueable;

    private $user, $order;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $order)
    {
        $this->user = $user;
        $this->order = $order;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database', OneSignalChannel::class];
    }

    public function toDatabase()
    {
        $user = [];
        $order = [];

        $user['id'] = $this->user->id ?? null;
        $user['name'] = $this->user->name ?? null;

        $order['id'] = $this->order->id;
        $order['created_at'] = $this->order->created_at;
        $order['order_status_id'] = $this->order->created_at;
        $order['shop']['id'] = $this->order->shop->id;
        $order['shop']['name'] = $this->order->shop->name;

        return [
            'user' => $user,
            'order' => $order
        ];
    }

    public function toOneSignal($notifiable)
    {
        return OneSignalMessage::create()
                ->setSubject("Ciao " . $this->user->name . ", l'ordine #" . $this->order->id . " è stato modificato!")
                ->setBody("Ti abbiamo inviato una mail con il riepilogo del tuo ordine")
                ->setData('order', $this->order->id)
                ->setParameter('android_channel_id', "faedcca6-5b78-4bb0-9fb1-f7b78ffd38fb")
                ->setParameter('ios_sound', "sound_file.wav")
                ->setWebButton(
                    OneSignalWebButton::create('link-2')
                        ->text('Clicca qui')
                        ->icon('https://backend.primascelta.biz/images/logo-login.png')
                        ->url('https://backend.primascelta.biz/')
                );

    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject("Modifica dell'ordine #" . $this->order->id)
                    ->markdown('emails.order_modified_user', ['user' => $notifiable, 'order' => $this->order])
                    ;

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
