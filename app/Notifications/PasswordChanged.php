<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

use NotificationChannels\OneSignal\OneSignalChannel;
use NotificationChannels\OneSignal\OneSignalMessage;
use NotificationChannels\OneSignal\OneSignalButton;
use NotificationChannels\OneSignal\OneSignalWebButton;

class PasswordChanged extends Notification
{
    use Queueable;

    private $password;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($password)
    {
        $this->password = $password;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database', OneSignalChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject("Nuova password")
                    ->markdown('emails.password_changed', ['user' => $notifiable, 'password' => $this->password])
                    ;
    }

    public function toOneSignal($notifiable)
    {
        return OneSignalMessage::create()
                ->setSubject("Nuova Password")
                ->setBody("L'amministratore ha cambiato la tua password. Controlla la posta per conoscerla.")
                ->setParameter('android_channel_id', "faedcca6-5b78-4bb0-9fb1-f7b78ffd38fb")
                ->setParameter('ios_sound', "sound_file.wav")
                ->setWebButton(
                    OneSignalWebButton::create('link-2')
                        ->text('Clicca qui')
                        ->icon('https://backend.primascelta.biz/images/logo-login.png')
                        ->url('https://backend.primascelta.biz/')
                );

    }

    public function toDatabase()
    {
        return [
            'password' => $this->password
        ];
    }


    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
