<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

use NotificationChannels\OneSignal\OneSignalChannel;
use NotificationChannels\OneSignal\OneSignalMessage;
use NotificationChannels\OneSignal\OneSignalButton;
use NotificationChannels\OneSignal\OneSignalWebButton;

use App\Models\User;

class RemovedPoints extends Notification
{
    use Queueable;

    private $user, $points, $order;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $points, $order)
    {
        $this->user = $user;
        $this->points = $points;
        $this->order = $order;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database', OneSignalChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {

        $notifiableUser = User::find($notifiable->id);

        return (new MailMessage)
                    ->greeting('Ciao ' . $notifiableUser->name . '!')
                    ->subject("Ti sono stati scalati dei punti")
                    ->line("Ti sono stati scalati " . $this->points . " punti per l'ordine #" . $this->order->id )
                    ->line("Vai sull'APP Primascelta per i dettagli dell'ordine")
                    ;
    }


    /** Send push notification */
    public function toOneSignal($notifiable)
    {
        return OneSignalMessage::create()
            ->setSubject("Ti sono stati scalati " . $this->points . " punti per l'ordine #" . $this->order->id)
            ->setBody("Clicca qui per i dettagli")
            ->setParameter('android_channel_id', "faedcca6-5b78-4bb0-9fb1-f7b78ffd38fb")
            ->setParameter('ios_sound', "sound_file.wav")
            ->setWebButton(
                OneSignalWebButton::create('link-2')
                    ->text('Clicca qui')
                    ->icon('https://backend.primascelta.biz/images/logo-login.png')
                    ->url('https://backend.primascelta.biz/')
            );

    }


    public function toDatabase()
    {
        return [
            'user' => $this->user,
            'points' => $this->points,
            'order' => $this->order
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
