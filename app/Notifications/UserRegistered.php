<?php

/**
 * Notification for Administrator
 * when a new Company or a Private user
 * registration has performed
 */

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

use NotificationChannels\OneSignal\OneSignalChannel;
use NotificationChannels\OneSignal\OneSignalMessage;
use NotificationChannels\OneSignal\OneSignalButton;
use NotificationChannels\OneSignal\OneSignalWebButton;

use App\Models\User;

class UserRegistered extends Notification
{
    use Queueable;

    private $user_registered;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user_registered)
    {
        $this->user_registered = $user_registered;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database', OneSignalChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {

        $notifiableUser = User::find($notifiable->id);

        return (new MailMessage)
                    ->subject("Nuova registrazione " . $this->user_registered->user_type)
                    ->markdown('emails.user_registered', ['user' => $notifiable])
                    ;
    }

    /** Send push notification */
    public function toOneSignal($notifiable)
    {
        return OneSignalMessage::create()
            ->setSubject("Nuova registrazione " . $this->user_registered->user_type)
            ->setBody("Si è registrato l'utente " . $this->user_registered->name)
            ->setParameter('android_channel_id', "faedcca6-5b78-4bb0-9fb1-f7b78ffd38fb")
            ->setParameter('ios_sound', "sound_file.wav")
            ->setWebButton(
                OneSignalWebButton::create('link-2')
                    ->text('Clicca qui')
                    ->icon('https://backend.primascelta.biz/images/logo-login.png')
                    ->url('https://backend.primascelta.biz/')
            );

    }


    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'user' => $this->user_registered
        ];
    }
}
