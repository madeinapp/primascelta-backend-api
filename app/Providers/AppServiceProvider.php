<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Events\Dispatcher;
use JeroenNoten\LaravelAdminLte\Events\BuildingMenu;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Dispatcher $events)
    {

        date_default_timezone_set('Europe/Rome');
        setlocale(LC_MONETARY, 'it_IT');

        if (auth()->check()) {
            config(['adminlte.dashboard_url' => route('home')]);
        }else{
            config(['adminlte.dashboard_url' => '/']);
        }

    }
}
