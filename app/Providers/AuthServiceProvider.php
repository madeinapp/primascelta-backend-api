<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use App\Models\Subscription;


class AuthServiceProvider extends ServiceProvider
{

    private $subscriptionRepository;


    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('is_admin', function ($user) {
            return $user->role == 'admin';
        });

        Gate::define('is_company', function ($user) {
            return $user->user_type == 'company';
        });

        Gate::define('subscription_expired', function ($user) {
            if( $user->status === 'inactive' ):
                return true;
            endif;

            if( $user->role == 'admin' ):
                return false;
            elseif( $user->user_type == 'company' ):

                $subscription = $user->subscription;
				//--->Check for free Subscription
				if($subscription->subscription == 'free'){
					if(!empty($user->shop->agent_id)){
						if( $subscription->expire_date->isFuture() ){
							return false;
						} else {
							return true;
						}
					} else {
						return false;
					}
				}
				//<---
                if( $subscription->expire_date->isFuture() ):
                    return false;
                else:
                    return true;
                endif;

            endif;
        });

        Gate::define('add_to_catalog', function ($user) {

            if( $user->status === 'inactive' ):
                return false;
            endif;

            if( $user->role == 'admin' ):
                return true;
            elseif( $user->user_type == 'company' ):

				$subscription = $user->subscription;
				$subscription_type = $subscription->subscription;
				$objSubscription = Subscription::where('subscription', $subscription_type)->first();

				$catalog = $user->shop->shopCatalogs->count();

				$check_expire = true;
				if($subscription->subscription == 'free' && empty($user->shop->agent_id)){
					$check_expire = false;
				}

				if( $check_expire ):
				   if(	$subscription->expire_date->isFuture() ):


						if( $subscription_type === 'trial' ):

							if( $catalog < 1000):
								return true;
							else:
								return false;
							endif;

						else:
							if( $objSubscription ):
								$pn = $objSubscription->products_num;
								if(!empty($user->subscription->subscription_product_num)){
									$pn = $user->subscription->subscription_product_num;
								}
								if( $catalog < $pn ):
									return true;
								else:
									return false;
								endif;
							else:
								return false;
							endif;

						endif;
					else:
                    	return false;
                	endif;

                    /*
                    switch( $subscription_type ){
                        case 'standard'  : if( $catalog < 100 ): return true; else: return false; endif; break;
                        case 'premium'   : if( $catalog < 200 ): return true; else: return false; endif; break;
                        case 'enterprise': if( $catalog < 400 ): return true; else: return false; endif; break;
                        case 'trial'     : if( $catalog < 1000):  return true; else: return false; endif; break;
                        default: return false;
                    }
                    */

                else:
					if( $objSubscription ):
						$pn = $objSubscription->products_num;
						if(!empty($user->subscription->subscription_product_num)){
							$pn = $user->subscription->subscription_product_num;
						}
						if( $catalog < $pn ):
							return true;
						else:
							return false;
						endif;
					else:
						return false;
					endif;
                endif;
            else:
                return false;
            endif;

        });

		Gate::define('add_offer', function ($user) {
			$subscription = $user->subscription;
			if($subscription->subscription == 'free'){
				return false;
			}
			return true;
		});

        Gate::define('read_notifications', function ($user) {
            return $user->unreadNotifications()->count() > 0;
        });
    }
}
