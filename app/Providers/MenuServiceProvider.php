<?php

namespace App\Providers;

use App\Models\Order;
use App\Repositories\OrderRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;
use JeroenNoten\LaravelAdminLte\Events\BuildingMenu;
use Illuminate\Contracts\Events\Dispatcher;
use Config;

class MenuServiceProvider extends ServiceProvider
{

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(Dispatcher $events, OrderRepository $orderRepository)
    {

        $events->listen(BuildingMenu::class, function (BuildingMenu $event) use ($orderRepository) {

            $user = Auth::user();

            $event->menu->add([
                'key' => 'notifications',
                'text' => 'Notifiche',
                'icon' => 'fas fa-bell notifications-counter',
                'label' => $user->unreadNotifications->count(),
                'label_color' => ( $user->unreadNotifications->count() > 0 ) ? 'success' : 'secondary',
                'topnav_right' => true,
            ]);

            $event->menu->addIn('notifications',
                [
                    'text' => 'Centro notifiche',
                    'icon' => 'fas fa-bell',
                    'icon_color' => '#13bf9e',
                    'url' => '/notifications',
                    'active' => ['/notifications/*'
                ]
            ]);

            $count = 1;

            $max_notifications_show = 10;

            foreach( $user->unreadNotifications as $notification ):

                $event->menu->addIn('notifications',
                    [
                        'text' => notificationName($notification),
                        'icon_color' => 'white',
                        'url' => '/notifications/read/'.$notification->id,
                        'active' => ['/notifications/*'
                    ]
                ]);

                $count++;

                if( $count == $max_notifications_show ):
                    break;
                endif;

            endforeach;

            if( $user->unreadNotifications->count() > $max_notifications_show ):

                $more = ($user->unreadNotifications->count() - $max_notifications_show);

                $text = "Vedi altre " . $more . " notifiche...";
                if( $more == 1 ):
                    $text = "Altre notifiche...";
                endif;

                $event->menu->addIn('notifications',
                    [
                        'text' => $text,
                        'icon_color' => '#13bf9e',
                        'url' => '/notifications',
                        'active' => ['/notifications/*'
                    ]
                ]);
            endif;



            if ($user && $user->user_type === 'company') {

                $ordiniDaLavorare = Order::where('shop_id', $user->shop->id)
                                        ->where('order_status_id', 2)
                                        ->get()
                                        ->count();

                if( $event->menu->itemKeyExists('menu_ordini') && $ordiniDaLavorare > 0 ){
                    $event->menu->remove('menu_ordini');
                    $event->menu->addAfter('menu_offerte', [
                        'key' => 'menu_ordini',
                        'text' => 'Ordini',
                        'label' =>  $ordiniDaLavorare,
                        'label_color' => 'danger',
                        'url'  => '/orders',
                        'icon' => 'fas fa-shopping-cart',
                        'active' => ['orders/*'],
                        'can'     => 'is_company',
                    ]);
                }


            }


        });

    }
}
