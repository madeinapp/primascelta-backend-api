<?php

namespace App\Repositories;

use App\Models\DeliveryMethod;
use App\Repositories\BaseRepository;

/**
 * Class DeliveryMethodRepository
 * @package App\Repositories
 * @version May 22, 2020, 3:09 pm UTC
*/

class DeliveryMethodRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'description'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return DeliveryMethod::class;
    }
}
