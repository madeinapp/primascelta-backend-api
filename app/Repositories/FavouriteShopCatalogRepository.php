<?php

namespace App\Repositories;

use App\Models\FavouriteShopCatalog;
use App\Repositories\BaseRepository;

/**
 * Class UserRepository
 * @package App\Repositories
 * @version May 3, 2020, 1:54 pm UTC
*/

class FavouriteShopCatalogRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'shop_catalog_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return FavouriteShopCatalog::class;
    }
        
}
