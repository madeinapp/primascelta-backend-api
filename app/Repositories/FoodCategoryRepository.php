<?php

namespace App\Repositories;

use App\Models\FoodCategory;
use App\Repositories\BaseRepository;

/**
 * Class FoodCategoryRepository
 * @package App\Repositories
 * @version April 26, 2020, 5:01 pm UTC
*/

class FoodCategoryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return FoodCategory::class;
    }

    public function getFoodCategoryList(){
        return $this->model()::orderBy('name')->orderBy('name')->get()->pluck('name', 'id')->toArray();
    }

    public function byUnitOfMeasure($unit_of_measure_id){
        return $this->model()::where('unit_of_measure', $unit_of_measure_id)->orderBy('name')->get();
    }
}
