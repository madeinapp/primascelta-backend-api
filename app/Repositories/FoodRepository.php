<?php

namespace App\Repositories;

use App\Models\Food;
use App\Repositories\BaseRepository;

/**
 * Class FoodRepository
 * @package App\Repositories
 * @version June 22, 2020, 7:30 am UTC
*/

class FoodRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'food_category_id',
        'unit_of_measure_id',
        'name',
        'description',
        'type',
        'image',
        'available'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Food::class;
    }

    public function allFoods(){
        return $this->model()::with('foodCategory')->orderBy('food_category_id')->get();
    }

    /**
     * Get Food Type List
     */
    public function getFoodTypelist($food_category_id = null)
    {
        $food_type_list = $this->model()::select('type')->distinct();

        if( !empty($food_category_id) ):
            $food_type_list = $food_type_list->where('food_category_id', $food_category_id);
        endif;

        $food_type_list = $food_type_list->orderBy('type', 'ASC')
                                        ->get()
                                        ->pluck('type')
                                        ->toArray();
		return $food_type_list;
    }

    public function updateType ($old, $new)
    {
        $res = $this->model()::where('type', $old)
                             ->update(['type' => $new]);

        return $res;
    }

    public function countAll(){
        return $this->model()::all()->count();
    }

    public function byUnitOfMeasure($unit_of_measure_id){
        return $this->model()::where('unit_of_measure_id', $unit_of_measure_id)->orderBy('name')->get();
    }

    public function byCategory($food_category_id){
        return $this->model()::where('food_category_id', $food_category_id)->orderBy('name')->get();
    }

}
