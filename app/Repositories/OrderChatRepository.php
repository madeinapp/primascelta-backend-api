<?php

namespace App\Repositories;

use App\Models\OrderChat;
use App\Repositories\BaseRepository;

/**
 * Class ShopOfferRepository
 * @package App\Repositories
 * @version June 26, 2020, 6:28 pm UTC
*/

class OrderChatRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'order_id',
        'from',
        'message'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return OrderChat::class;
    }
}
