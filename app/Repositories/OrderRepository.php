<?php

namespace App\Repositories;

use App\Models\Order;
use App\Repositories\BaseRepository;
use Facade\Ignition\QueryRecorder\Query;

use DB;
use Log;

/**
 * Class OrderRepository
 * @package App\Repositories
 * @version June 1, 2020, 6:30 pm UTC
*/

class OrderRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'shop_id',
        'user_id',
        'order_status_id',
        'total_price',
        'shipping_price',
        'note',
        'created_at'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Order::class;
    }

    public function notConfirmed($user_id = null, $shop_id = null, $search = null){

        $query = $this->model()::whereHas('orderStatus', function($orderStatus){
            $orderStatus->where('name', 'not_confirmed');
        });

        if( !empty($user_id) ):
            $query = $query->where('user_id', $user_id);
        elseif(!empty($shop_id)):
            $query = $query->where('shop_id', $shop_id);
        endif;

        if( !empty($search) ):
            if( is_numeric($search) ):
                $query = $query->where('id', 'like', '%'.$search.'%');
            else:
                $query = $query->whereHas('user', function($user) use($search) {
                    $user->where('name', 'like', '%'.$search.'%');
                });
                $query = $query->orWhereHas('orderStatus', function($status) use($search){
                    $status->where('name', 'like', '%'.$search.'%');
                });
            endif;
        endif;

        $query = $query->with('user');
        $query = $query->with(['user' => function($query){
            $query->withCount('delivered');
        }]);
        $query = $query->with('shop');
        $query = $query->with('orderStatus');
        $query = $query->orderBy('created_at', 'DESC');

        return $query->get();
    }

    public function confirmed($user_id = null, $shop_id = null, $search = null){

        $query = $this->model()::whereHas('orderStatus', function($orderStatus){
            $orderStatus->where('name', '!=', 'not_confirmed')
                        ->where('name', '!=', 'draft');
        });

        if( !empty($user_id) ):
            $query = $query->where('user_id', $user_id);
        elseif(!empty($shop_id)):
            $query = $query->where('shop_id', $shop_id);
        endif;

        if( !empty($search) ):
            if( is_numeric($search) ):
                $query = $query->where('id', 'like', '%'.$search.'%');
            else:
                $query = $query->whereHas('user', function($user) use($search) {
                    $user->where('name', 'like', '%'.$search.'%');
                });
                $query = $query->orWhereHas('orderStatus', function($status) use($search){
                    $status->where('name', 'like', '%'.$search.'%');
                });
            endif;
        endif;

        $query = $query->with('user');
        $query = $query->with(['user' => function($query){
            $query->withCount('delivered');
        }]);
        $query = $query->with('shop');
        $query = $query->with('orderStatus');
        $query = $query->orderBy('created_at', 'DESC');

        return $query->get();
    }

    public function list($user_id){

        $query = $this->model->newQuery();

        $query->where('user_id', $user_id)
              ->where('order_status_id', '>', 1)
              ;

        $query = $query->with('user');
        $query = $query->with('shop');
        $query = $query->with('orderStatus');
        $query = $query->orderBy('created_at', 'DESC');

        return $query->get();
    }

    public function listCompany($shop_id){

        $query = $this->model()::where('shop_id', $shop_id)
                                ->where('order_status_id', '>', 1)
                                ;

        $query = $query->with('user');
        $query = $query->with('shop');
        $query = $query->with('orderStatus');

        return $query->get();
    }

    /**
     * Ordini non confermati con data di consegna trascorsa da meno di un giorno
     *
     */
    public function getNotConfirmedExpiredLess24Hours(){


        $pickup_on_site = $this->model()::where('order_status_id', 2)
                                ->where('pickup_on_site', 1)
                                ->whereRaw("NOW() > cancellable_within")
                                ->whereRaw("NOW() < str_to_date(concat(date_format(pickup_on_site_date, '%Y-%m-%d') , ' ' , date_format(pickup_on_site_time, '%H:%i:%s')), '%Y-%m-%d %H:%i:%s') + INTERVAL 24 HOUR")
                                ->select('id')
                                ->get()
                                ->pluck('id')
                                ->toArray()
                                ;

        $home_delivery = $this->model()::where('order_status_id', 2)
                                ->where('home_delivery', 1)
                                ->whereRaw("NOW() > cancellable_within")
                                ->whereRaw("NOW() < str_to_date(concat(date_format(home_delivery_date, '%Y-%m-%d') , ' ' , date_format(home_delivery_time, '%H:%i:%s')), '%Y-%m-%d %H:%i:%s') + INTERVAL 24 HOUR")
                                ->select('id')
                                ->get()
                                ->pluck('id')
                                ->toArray()
                                ;

        $orders = array_merge($pickup_on_site , $home_delivery );

        return $this->model()::whereIn('id', $orders)->get();

    }


    public function getNotConfirmedExpiredMore24Hours(){


        $pickup_on_site = $this->model()::where('order_status_id', 2)
                                ->where('pickup_on_site', 1)
                                ->whereRaw("NOW() > str_to_date(concat(date_format(pickup_on_site_date, '%Y-%m-%d') , ' ' , date_format(pickup_on_site_time, '%H:%i:%s')), '%Y-%m-%d %H:%i:%s') + INTERVAL 24 HOUR")
                                ->select('id')
                                ->get()
                                ->pluck('id')
                                ->toArray()
                                ;

        $home_delivery = $this->model()::where('order_status_id', 2)
                                ->where('home_delivery', 1)
                                ->whereRaw("NOW() > str_to_date(concat(date_format(home_delivery_date, '%Y-%m-%d') , ' ' , date_format(home_delivery_time, '%H:%i:%s')), '%Y-%m-%d %H:%i:%s') + INTERVAL 24 HOUR")
                                ->select('id')
                                ->get()
                                ->pluck('id')
                                ->toArray()
                                ;

        $orders = array_merge($pickup_on_site , $home_delivery );

        return $this->model()::whereIn('id', $orders)->get();

    }


    /**
     * Recupera tutti gli ordini confermati
     * che hanno la data di spedizione trascorsa da meno di 24 ore
     */
    public function getConfirmedExpired(){

        $pickup_on_site = $this->model()::where('order_status_id', 4)
                                ->where('pickup_on_site', 1)
                                ->whereRaw("NOW() > pickup_on_site_within")
                                ->whereRaw("pickup_on_site_within + INTERVAL 24 HOUR > NOW()")
                                ->select('id')
                                ->get()
                                ->pluck('id')
                                ->toArray()
                                ;

        $home_delivery = $this->model()::where('order_status_id', 4)
                                ->where('home_delivery', 1)
                                ->whereRaw("NOW() > home_delivery_within")
                                ->whereRaw("home_delivery_within + INTERVAL 24 HOUR > NOW()")
                                ->select('id')
                                ->get()
                                ->pluck('id')
                                ->toArray()
                                ;

        $orders = array_merge($pickup_on_site , $home_delivery );

        return $this->model()::whereIn('id', $orders)->get();

    }


    public function getConfirmedExpiredMore24Hours(){


        $pickup_on_site = $this->model()::where('order_status_id', 4)
                                ->where('pickup_on_site', 1)
                                ->whereRaw("NOW() > pickup_on_site_within + INTERVAL 24 HOUR")
                                ->select('id')
                                ->get()
                                ->pluck('id')
                                ->toArray()
                                ;

        $home_delivery = $this->model()::where('order_status_id', 4)
                                ->where('home_delivery', 1)
                                ->whereRaw("NOW() > home_delivery_within + INTERVAL 24 HOUR")
                                ->select('id')
                                ->get()
                                ->pluck('id')
                                ->toArray()
                                ;

        $orders = array_merge($pickup_on_site , $home_delivery );

        return $this->model()::whereIn('id', $orders)->get();

    }

    /**
     * Recupera tutti gli ordini confermati con riserva
     * che hanno la data di conferma riserva ormai passata
     * e che devono quindi essere modificati dall'utente o annullati
     */
    public function getConfirmedWithReserveExpired(){

        $pickup_on_site = $this->model()::where('order_status_id', 3)
                                ->where("pickup_on_site", 1)
                                ->whereRaw("NOW() > confirm_reserve_within")
                                ->whereRaw("STR_TO_DATE(concat(DATE_FORMAT(pickup_on_site_date, '%d-%m-%Y'), ' ',  date_format(pickup_on_site_time, '%H:%i')), '%d-%m-%Y %H:%i') > NOW()")
                                ->select('id')
                                ->get()
                                ->pluck('id')
                                ->toArray()
                                ;

        $home_delivery = $this->model()::where('order_status_id', 3)
                                ->where("home_delivery", 1)
                                ->whereRaw("NOW() > confirm_reserve_within")
                                ->whereRaw("STR_TO_DATE(concat(DATE_FORMAT(home_delivery_date, '%d-%m-%Y'), ' ',  date_format(home_delivery_time, '%H:%i')), '%d-%m-%Y %H:%i') > NOW()")
                                ->select('id')
                                ->get()
                                ->pluck('id')
                                ->toArray()
                                ;


        $orders = array_merge($pickup_on_site , $home_delivery );

        return $this->model()::whereIn('id', $orders)->get();

    }

}
