<?php

namespace App\Repositories;
use App\Repositories\BaseRepository;
use App\Models\PaymentMethodShop;
/**
 * Class ShopRepository
 * @package App\Repositories
 * @version June 1, 2020, 6:53 pm UTC
*/

class PaymentMethodShopRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'shop_id',        
        'payment_method_id',
        'payment_for'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PaymentMethodShop::class;
    }

    public function paymentMethods($shop_id, $payment_for = null){
        if( empty($payment_for) ):
            return $this->model()::where('shop_id', $shop_id)->whereNull('payment_for')->get();
        else:
            return $this->model()::where('shop_id', $shop_id)->where('payment_for', $payment_for)->get();
        endif;
    }

    public function search($shop_id, $payment_method_id, $payment_for = null){
        if( empty($payment_for) ):
            return $this->model()::where('shop_id', $shop_id)
                                 ->where('payment_method_id', $payment_method_id)
                                 ->whereNull('payment_for')
                                 ->first();
        else:
            return $this->model()::where('shop_id', $shop_id)
                                 ->where('payment_method_id', $payment_method_id)
                                 ->where('payment_for', $payment_for)
                                 ->first();
        endif;
    }
    
}
