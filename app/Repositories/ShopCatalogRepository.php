<?php

namespace App\Repositories;

use Illuminate\Support\Facades\Auth;
use App\Models\ShopCatalog;
use App\Models\User;
use App\Repositories\BaseRepository;
use DB;

/**
 * Class ShopCatalogRepository
 * @package App\Repositories
 * @version June 23, 2020, 9:51 am UTC
*/

class ShopCatalogRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'shop_id',
        'food_category_id',
        'unit_of_measure_id',
        'food_unit_of_measure_quantity',
        'unit_of_measure_buy',
        'unit_of_measure_buy_quantity',
        'food_id',
        'food_name',
        'food_description',
        'food_type',
        'food_price',
        'food_image',
        'visible_in_search',
        'home_delivery',
        'in_app_shopping',
        'uncertain_weight'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ShopCatalog::class;
    }


    /** Return all shop products */
    public function shopAll(){
        $user = User::find(Auth::id());
        if( $user->shop ):

            $shopCatalog = DB::table('shop_catalogs as c')
                            ->join('food_categories as fc', 'c.food_category_id', '=', 'fc.id')
                            ->join('unit_of_measures as u', 'c.unit_of_measure_id', '=', 'u.id')
                            ->where('c.shop_id', $user->shop->id)
                            ->select('fc.name as food_category', 'u.name as unit_of_measure', 'c.*' )
                            ;

            return $shopCatalog;

        endif;
    }

    public function find($id, $columns = ['*']){
        return $this->model()::where('id', $id)
                                ->with('shop')
                                ->with('shopOfferFoods.shopOffer')
                                ->with('unitOfMeasure')
                                ->with('unitOfMeasureBuy')
                                ->with('specials')
                                ->first()
                                ;
    }

    public function getFoodCategoryList()
    {
        return $this->model()::where('shop_id', Auth::user()->shop->id)
                              ->with(array('foodCategory'=>function($query){
                                    $query->select('id','name');
                                }))
                              ->get()
                              ->pluck('foodCategory.name', 'foodCategory.id')
                              ->toArray()
                              ;
    }

    public function getTypesList($food_category_id)
    {
        $q = $this->model()::select('food_type')
                             ->distinct()
                             ->where('shop_id', Auth::user()->shop->id);

                             if( $food_category_id !== 'SELECT_ALL'):
                                $q = $q->where('food_category_id', $food_category_id);
                             endif;

        $q = $q->get()->pluck('food_type')->toArray();

        return $q;
    }

    public function byUnitOfMeasure($unit_of_measure_id){
        return $this->model()::where('unit_of_measure_id', $unit_of_measure_id)
                             ->orderBy('food_name')->get();
    }

    public function byUnitOfMeasureBuy($unit_of_measure_id){
        return $this->model()::where('unit_of_measure_buy', $unit_of_measure_id)
                             ->orderBy('food_name')->get();
    }

    public function havingFoodByFoodCategory($food_category_id){
        return $this->model()::whereHas('food', function($food) use($food_category_id) {
            $food->where('food_category_id', $food_category_id);
        })
        ->get();
    }

    public function getFoodTypelist($food_category_id = null)
    {
        $food_type_list = $this->model()::select('food_type')->distinct();

        if( !empty($food_category_id) ):
            $food_type_list = $food_type_list->where('food_category_id', $food_category_id);
        endif;

        $food_type_list = $food_type_list->orderBy('food_type', 'ASC')
                                        ->get()
                                        ->pluck('food_type')
                                        ->toArray();
		return $food_type_list;
    }

    public function updateType ($old, $new)
    {
        $res = $this->model()::where('food_type', $old)
                             ->update(['food_type' => $new]);

        return $res;
    }

}
