<?php

namespace App\Repositories;

use App\Models\ShopOfferFood;
use App\Repositories\BaseRepository;

/**
 * Class ShopOfferRepository
 * @package App\Repositories
 * @version June 26, 2020, 6:28 pm UTC
*/

class ShopOfferFoodRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'shop_offer_id',
        'shop_catalog_id',
        'food_name',
        'food_image',
        'food_description',
        'min_quantity',
        'discounted_price'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ShopOfferFood::class;
    }
}
