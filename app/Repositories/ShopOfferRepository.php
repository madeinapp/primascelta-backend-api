<?php

namespace App\Repositories;

use App\Models\ShopOffer;
use App\Repositories\BaseRepository;

/**
 * Class ShopOfferRepository
 * @package App\Repositories
 * @version June 26, 2020, 6:28 pm UTC
*/

class ShopOfferRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'shop_id',
        'name',
        'description',
        'image_path',
        'start_date',
        'end_date',
        'count_purchased',
        'status'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ShopOffer::class;
    }

    public function list($shop_id)
    {
        return $this->model::where('shop_id', $shop_id)->orderBy('start_date', 'DESC')->get();
    }
}
