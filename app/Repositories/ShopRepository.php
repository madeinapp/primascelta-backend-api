<?php

namespace App\Repositories;

use App\Models\Shop;
use App\Models\PaymentMethod;
use App\Models\DeliveryMethod;
use App\Models\ShopType;
use App\Repositories\BaseRepository;
use Auth;
/**
 * Class ShopRepository
 * @package App\Repositories
 * @version June 1, 2020, 6:53 pm UTC
*/

class ShopRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'agent_id',
        'shop_type',
        'shop_type_id',
        'description',
        'website',
        'facebook_page',
        'instagram_page',
        'opening_timetable',
        'delivery_range_km',
        'excluded_commons',
        'min_trust_points',
        'tax_business_name',
        'tax_type',
        'tax_vat',
        'tax_fiscal_code',
        'tax_pec',
        'country',
        'address',
        'route',
        'city',
        'prov',
        'region',
        'postal_code',
        'street_number',
        'lat',
        'long',
        'closed_date_start',
        'closed_date_end',
        'shop_status',
        'shop_status_orders',
        'status'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Shop::class;
    }

    /*
     * @return payment methods list
     */
    public function getPaymentMethods()
    {
	    return PaymentMethod::orderBy('name')->get();
    }

    /*
     * @return idelivery methods list
     */
    public function getDeliveryMethods()
    {
	    return DeliveryMethod::orderBy('name')->get();
    }

    /*
     * @return shop types list
     */
    public function getShopTypes()
    {
    	return ShopType::orderBy('name')->get();
    }

    public function otherShops($shop_id, $other_shops = []){

        $shop = $this->model()::find($shop_id);
        $vat = $shop->tax_vat;

        return $this->model()::where('id', '!=', $shop_id)
                             ->whereNotIn('id', $other_shops)
                             ->where('tax_vat', $vat)
                             ->where('status', 'active')
                             ->get()
                             ;
    }

    public function otherShopsSameSubscription($shop_id){

        $shop = $this->model()::find($shop_id);
        $vat = $shop->tax_vat;

        $parent = $shop->user->subscription->id;

        return $this->model()::where('id', '!=', $shop_id)
                                ->where('status', 'active')
                                ->where('tax_vat', $vat)
                                ->whereHas('user', function($user) use ($parent) {
                                    $user->whereHas('subscription', function($subscription) use($parent) {
                                        $subscription->where('parent', $parent);
                                    });
                                })
                                ->get()
                                ;
    }

    public function shopDetail($shop_id){

        $shop = $this->model()::where('id', $shop_id)
                                ->whereHas('shopCatalogs', function($query){
                                    $query->where('visible_in_search', 1);
                                })
                                ->with('user')
                                ->with("shopTypes")
                                ->with("PaymentMethodsShop")
                                ->with("PaymentMethodsDelivery")
                                ->first();

        return $shop;
    }

}
