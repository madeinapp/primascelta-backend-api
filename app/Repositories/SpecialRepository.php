<?php

namespace App\Repositories;
use App\Models\Special;
use App\Repositories\BaseRepository;

/**
 * Class SpecialRepository
 * @package App\Repositories
 * @version October, 29 2020, 15:10 UTC
*/

class SpecialRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Special::class;
    }

    public function allSpecials(){
        return $this->model()::orderBy('name')->get();
    }
}
