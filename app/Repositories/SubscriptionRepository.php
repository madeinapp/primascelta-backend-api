<?php

namespace App\Repositories;

use App\Models\Subscription;
use App\Repositories\BaseRepository;

/**
 * Class PaymentMethodRepository
 * @package App\Repositories
 * @version May 22, 2020, 3:08 pm UTC
*/

class subscriptionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'description',
        'name',
        'shops_num',
        'shops_note',
        'products_num',
        'products_note',
        'description_list',
        'price_yearly',
        'price_half_yearly'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Subscription::class;
    }

    public function findBySubscription($subscription){
        return $this->model()::where('subscription', strtolower($subscription))->first();
    }
}
