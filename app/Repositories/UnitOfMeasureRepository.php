<?php

namespace App\Repositories;

use App\Models\UnitOfMeasure;
use App\Repositories\BaseRepository;

/**
 * Class UnitOfMeasureRepository
 * @package App\Repositories
 * @version May 8, 2020, 10:42 am UTC
*/

class UnitOfMeasureRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return UnitOfMeasure::class;
    }

    public function getRelateds($unit_of_measure_id, $level = 1){

        $relateds = [$unit_of_measure_id];

        $res = $this->model()::where('next_unit_of_measure', $unit_of_measure_id)->first();

        $prev = null;

        if( $res ):
            $prev = $res->id;
            $relateds[] = $prev;
        endif;

        $others = null;
        if( $prev ):
            $others = $this->getRelateds($prev, $level+1);
            if( $others ):
                $relateds = array_merge($relateds , $others);
            endif;
        endif;

        return array_unique($relateds);
    }

    public function getUoms($uom_ids)
    {
        return $this->model()::whereIn('id', $uom_ids)->get()->pluck('name', 'id')->toArray();
    }
}
