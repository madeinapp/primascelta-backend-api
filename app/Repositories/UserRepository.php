<?php

namespace App\Repositories;

use App\Models\User;
use App\Repositories\BaseRepository;
use DB;

/**
 * Class UserRepository
 * @package App\Repositories
 * @version May 3, 2020, 1:54 pm UTC
*/

class UserRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'email',
        'user_type',
        'role',
        'status',
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return User::class;
    }

    public function findByEmail($email){
        return $this->model()::where('email', $email)->first();
    }

    public function checkOtp($otp){
        return $this->model()::where('otp', $otp)->first();
    }

    public function administrators(){
        return $this->model()::where('role', 'admin')
                             ->where('status', 'active')
                             ->get();
    }

    public function subscriptionExpiring($days){
        return $this->model()::where('role', 'user')
                             ->whereHas('shop', function($shop){
                                $shop->whereIn('status', ['active', 'invisible']);
                             })
                             ->where('user_type', 'company')
                             ->whereIn('status', ['active', 'invisible'])
                             ->whereHas('subscription', function($subscription) use($days) {
								 $subscription
									 ->where(DB::raw("str_to_date(date_format(expire_date, '%Y-%m-%d'), '%Y-%m-%d')"), "!=", DB::raw("str_to_date(date_format(submission_date, '%Y-%m-%d'), '%Y-%m-%d')"))
									 ->where(DB::raw("CURDATE() + INTERVAL $days DAY"), "=", DB::raw("str_to_date(date_format(expire_date, '%Y-%m-%d'), '%Y-%m-%d')"));
                             })
                             ->get();
    }

    public function otpNotValidated(){
        return $this->model()::where('role', 'user')
                             ->whereNotNull('otp')
                             ->where('status', 'pending')
                             ->whereRaw('updated_at < NOW() - interval 5 minute')
                             ->get();
    }
}
