<?php

namespace App\Repositories;

use App\Models\UserSubscription;
use App\Repositories\BaseRepository;

/**
 * Class PaymentMethodRepository
 * @package App\Repositories
 * @version May 22, 2020, 3:08 pm UTC
*/

class UserSubscriptionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'subscription',
        'submission_date',
        'expire_date',
        'invoice_id',
        'parent'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return UserSubscription::class;
    }

    public function shopsInSubscription($subscription_id){
        $subscriptions = $this->model()::where('id', $subscription_id)
                                       ->orWhere('parent', $subscription_id)
                                       ->get();

        $tot = 0;

        if( $subscriptions ):
            foreach($subscriptions as $subscription):
                $user = $subscription->user;
                if( $user->user_type === 'company' && $user->status !== 'inactive' ):
                    if( $user->shop ):
                        $tot++;
                    endif;
                endif;
            endforeach;
        endif;

        return $tot;
    }
}
