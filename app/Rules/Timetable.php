<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Carbon\Carbon;

class Timetable implements Rule
{
    private $error_message; 

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->error_message = [];
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {        
        if( $value['active'] ):
            $days['morning'] = [];
            $days['afternoon'] = [];

            $days['morning']['open'] = Carbon::parse($value['first_half']['opening_time']);
            $days['morning']['close'] = Carbon::parse($value['first_half']['closing_time']);
            
            $days['afternoon']['open'] = Carbon::parse($value['second_half']['opening_time']);
            $days['afternoon']['close'] = Carbon::parse($value['second_half']['closing_time']);

            if( empty($value['first_half']['opening_time']) && 
                empty($value['first_half']['closing_time']) &&
                empty($value['second_half']['opening_time']) && 
                empty($value['second_half']['closing_time'])):                        
            
                $this->error_message[] = 'Non hai valorizzato gli orari';       

            elseif( (
                        (empty($value['first_half']['opening_time']) && !empty($value['first_half']['closing_time'])) ||
                        (!empty($value['first_half']['opening_time']) && empty($value['first_half']['closing_time']))
                    ) 
                    &&
                    (
                        (empty($value['second_half']['opening_time']) && empty($value['second_half']['closing_time'])) || 
                        (!empty($value['second_half']['opening_time']) && !empty($value['second_half']['closing_time'])) 
                    )): 
                
                $this->error_message[] = 'Non puoi inserire solo un orario';                        

            elseif( !empty($value['first_half']['opening_time']) && 
                    !empty($value['first_half']['closing_time']) && 
                $days['morning']['open']->isAfter($days['morning']['close']) ): 
                
                $this->error_message[] = 'L\'orario di chiusura non può precedere l\'orario di apertura';                                    

            // Verifica orario continuato
            elseif( !empty($value['first_half']['opening_time']) && 
                    empty($value['first_half']['closing_time']) && 
                    empty($value['second_half']['opening_time']) && 
                    !empty($value['second_half']['closing_time']) &&
                    $days['morning']['open']->isAfter($days['afternoon']['close']) ):
                                        
                $this->error_message[] = 'L\'orario di chiusura non può precedere l\'orario di apertura';                                            

            endif;

            if( empty($value['first_half']['opening_time']) && 
                empty($value['first_half']['closing_time']) &&
                empty($value['second_half']['opening_time']) && 
                empty($value['second_half']['closing_time'])):                        
            
                $this->error_message[] = 'Non hai valorizzato gli orari';       

            elseif( (
                        (empty($value['second_half']['opening_time']) && !empty($value['second_half']['closing_time'])) ||
                        (!empty($value['second_half']['opening_time']) && empty($value['second_half']['closing_time']))
                    ) 
                    &&
                    (
                        (empty($value['first_half']['opening_time']) && empty($value['first_half']['closing_time'])) ||
                        (!empty($value['first_half']['opening_time']) && !empty($value['first_half']['closing_time']))
                    )): 
                
                $this->error_message[] = 'Non puoi inserire solo un orario';

            elseif( !empty($value['second_half']['opening_time']) && 
                !empty($value['second_half']['closing_time']) && 
                $days['afternoon']['open']->isAfter($days['afternoon']['close']) ): 
                
                $this->error_message[] = 'L\'orario di chiusura non può precedere l\'orario di apertura';                    

            endif;
        endif;

        if( count($this->error_message) > 0 ): 
            return false; 
        else: 
            return true;
        endif;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return implode(", ", array_unique($this->error_message));
    }
}
