<?php
//Normalize string for filename
function NormalizeString($value){
    $result = str_replace(['\'',' '], ['','_'], $value); //$result = str_replace(['\'',' '], ['','_'], iconv('utf-8', 'ascii//TRANSLIT', $value));
    $result = strtolower(trim(preg_replace('/[^a-zA-Z0-9_]/s','',$result), '_'));
    $result = str_replace('__','_',$result);
    $result = str_replace('__','_',$result);
    return $result;
}

function tofloat($num) {
    $dotPos = strrpos($num, '.');
    $commaPos = strrpos($num, ',');
    $sep = (($dotPos > $commaPos) && $dotPos) ? $dotPos :
        ((($commaPos > $dotPos) && $commaPos) ? $commaPos : false);

    if (!$sep) {
        return floatval(preg_replace("/[^0-9]/", "", $num));
    }

    return floatval(
        preg_replace("/[^0-9]/", "", substr($num, 0, $sep)) . '.' .
        preg_replace("/[^0-9]/", "", substr($num, $sep+1, strlen($num)))
    );
}

//Crop images
function cropImages($image, $image_path, $type){
    /**
     * 750 x 306 immagine utente main page
     * 646 x 430 immagine prodotto dettaglio
     * 512 x 340 immagine offerta in slider
     * 310 x 240 immagine utente store card
     * 230 x 154 immagine prodotto lista
     * 100 x 100 immagine prodotto in carrello
     */
	//get filename without extension
    $filename = pathinfo($image_path, PATHINFO_FILENAME);
	//get file extension
    $extension = pathinfo($image_path, PATHINFO_EXTENSION );
    //get dir path
	$folder = dirname($image_path);

    if(!File::isDirectory($folder)) {

        File::makeDirectory($folder, 0775, true); //creates directory

    }

    $image->backup();

    //$image = $image;
    $image->fit(100,100, function ($constraint) {
        $constraint->upsize();
    })->save($folder.'/'.$filename.'_100x100.'.$extension);

    $image->reset();

    switch($type){
        case 'food':    $image->fit(230, 154)->save($folder.'/'.$filename.'_230x154.'.$extension);
                        $image->reset();

                        $image->fit(512, 340)->save($folder.'/'.$filename.'_512x340.'.$extension);
                        $image->reset();

                        $image->fit(646, 430)->save($folder.'/'.$filename.'_646x430.'.$extension);
                        $image->reset();

                        //$image->fit(1292, 860)->save($folder.'/'.$filename.'_1292x860.'.$extension);
                        //$image->reset();

                        /*
                        $image->fit(646, 430, function ($constraint) {
                            $constraint->upsize();
                        })->save($folder.'/'.$filename.'_646x430.'.$extension);
                        */

                        break;

        case 'user':    //$image = $image;
                        $image->fit(640, 320)->save($folder.'/'.$filename.'_640x320.'.$extension);
                        $image->reset();

                        //$image = $image;
                        /*
                        $image->fit(750, 306)->save($folder.'/'.$filename.'_750x306.'.$extension);
                        $image->reset();
                        */

                        break;
    }

    unset($image);

}

function notificationName($notification){

    $content = $notification->data;

    if( isset($content['order']) ):
        $order = $content['order'];
    elseif( isset($content['days'])):
        $days = $content['days'];
    endif;

    switch($notification->type){
        case 'App\Notifications\MailResetPasswordNotification': return 'Inviata richiesta recupero password'; break;
        case 'App\Notifications\RegisterOTP':                   return 'Registrazione OTP'; break;
        case 'App\Notifications\UserRegistered':                return 'Registrazione nuovo utente'; break;
        case 'App\Notifications\CompanyRegisteredAdmin':        return 'Nuova azienda iscritta'; break;
        case 'App\Notifications\CompanyRegistered':             return 'Benvenuto su Primascelta!'; break;
        case 'App\Notifications\PrivateRegistered':             return 'Benvenuto su Primascelta!'; break;
        case 'App\Notifications\CompanyEnabled':                return 'Utente attivato!'; break;
        case 'App\Notifications\CompanyDisabled':               return 'Utente disabilitato'; break;
        case 'App\Notifications\PrivateEnabled':                return 'Utente attivato!'; break;
        case 'App\Notifications\PrivateDisabled':               return 'Utente disabilitato'; break;
        case 'App\Notifications\OrderReceived':                 return 'Ordine ' . $order['id'] . ' da esaminare'; break;
        case 'App\Notifications\OrderSent':                     return 'Ordine ' . $order['id'] . ' inviato'; break;
        case 'App\Notifications\OrderConfirmed':                return 'Ordine ' . $order['id'] . ' confermato'; break;
        case 'App\Notifications\OrderConfirmedWithReserve':     return 'Ordine ' . $order['id'] . ' confermato con riserva'; break;
        case 'App\Notifications\OrderDelivered':                return 'Ordine ' . $order['id'] . ' consegnato'; break;
        case 'App\Notifications\OrderNotDelivered':             return 'Ordine ' . $order['id'] . ' non consegnato'; break;
        case 'App\Notifications\OrderNotDeliveredCopy':         return 'Ordine ' . $order['id'] . ' non consegnato'; break;
        case 'App\Notifications\OrderCancelled':                return 'Ordine ' . $order['id'] . ' annullato'; break;
        case 'App\Notifications\OrderCancelledAuto':            return 'Ordine ' . $order['id'] . ' annullato automaticamente'; break;
        case 'App\Notifications\OrderModified':                 return 'Ordine ' . $order['id'] . ' modificato'; break;
        case 'App\Notifications\OrderModifiedUser':             return 'Ordine ' . $order['id'] . ' modificato'; break;
        case 'App\Notifications\OrderReserveAccepted':          return 'Ordine ' . $order['id'] . ' confermato, modifiche accettate'; break;
        case 'App\Notifications\MessageReceived':               return 'Hai ricevuto un messaggio'; break;
        case 'App\Notifications\AlertNotConfirmed':             return 'Ordine ' . $order['id'] . ' non ancora confermato'; break;
        case 'App\Notifications\AlertConfirmed':                return 'Ordine ' . $order['id'] . ' non ancora evaso'; break;
        case 'App\Notifications\AlertConfirmedWithReserve':     return 'Riserva non confermata per l\'ordine ' . $order['id']; break;
        case 'App\Notifications\AlertConfirmedWithReserveUser': return 'Riserva non confermata per l\'ordine ' . $order['id']; break;
        case 'App\Notifications\PasswordChanged':               return 'Password cambiata'; break;
        case 'App\Notifications\AlertSubscription':             return 'Il tuo abbonamento scade tra ' . $days . ' giorni!'; break;
    }
}

function notificationContent($notification){

    $content = $notification->data;

    switch($notification->type){
        case 'App\Notifications\RegisterOTP':
            return 'Codice di attivazione Primascelta.biz: ' . $content['otp_code'];
            break;

        case 'App\Notifications\CompanyRegisteredAdmin':

            $user = $content['company'];
            $shop = $content['shop'];

            return 'Si è iscritto l\'utente ' . $user['name'] . ' con email ' . $user['email'] . ' e titolare del negozio ' . $shop['name'];
            break;

        case 'App\Notifications\OrderReceived':
        case 'App\Notifications\OrderSent':
        case 'App\Notifications\OrderConfirmed':
        case 'App\Notifications\OrderConfirmedWithReserve':
        case 'App\Notifications\OrderDelivered':
        case 'App\Notifications\OrderNotDelivered':
        case 'App\Notifications\OrderCancelled':
        case 'App\Notifications\OrderModified':
        case 'App\Notifications\OrderModifiedUser':
        case 'App\Notifications\OrderReserveAccepted':

            $user_action = $content['user'];
            $order = $content['order'];

            return [
                'user_action' => $user_action,
                'order' => $order
            ];
            break;

        case 'App\Notifications\OrderCancelledAuto':
            $order = $content['order'];

            return [
                'order' => $order
            ];
            break;


        case 'App\Notifications\UserRegistered':

            $content = '';
            if( isset($content['user']) ){
                $content = 'Nuova registrazione ';
                if( $content['user']['role'] == 'company' ){
                    $content .= 'negozio di ' . $content['user']['name'];
                }else{
                    $content .= 'utente ' . $content['user']['name'];
                }
            }
            return $content;
            break;

        case 'App\Notifications\MessageReceived':

            $user = $content['user'];
            $order = $content['order'];
            $message = $content['message'];

            return $user['name'] . ' ti ha inviato il seguente messaggio: ' . $message . ' relativo all\'ordine #' . $order['id'];
            break;

        case 'App\Notifications\AlertNotConfirmed':
            $order = $content['order'];
            return "L'ordine #" . $order['id'] . " non è stato ancora confermato";
            break;

        case 'App\Notifications\AlertConfirmed':
            $order = $content['order'];
            return "L'ordine #" . $order['id'] . " non è stato ancora evaso";
            break;

        case 'App\Notifications\AlertConfirmedWithReserve':
        case 'App\Notifications\AlertConfirmedWithReserveUser':
            $order = $content['order'];
            return "L'ordine #" . $order['id'] . " è stato cancellato automaticamente perché non è stata confermata la riserva entro i limiti";
            break;

        case 'App\Notifications\PasswordChanged':
            $password = $content['password'];
            return "La tua nuova password è: " . $password;
            break;

        case 'App\Notifications\AlertSubscription':
            $days = $content['days'];
            return "Il tuo abbonamento scade tra $days giorni!";
            break;

        default: return '';
    }
}


function getResizedImage($url,$resize){
    $portion = strrchr($url,'.');
    return str_replace($portion, ("_".$resize.".". substr($portion, 1)), $url);
}

function getOriginalImage($image){

    $check = explode("_", basename($image));
    if( count($check) > 1 ){
        $check2 = explode("x", end($check));
        if( count($check2) > 1 ){
            $filename = str_replace($check[count($check)-1], "", basename($image));
            if( substr($filename, -1) == '_' ){
                $filename = substr($filename, 0, strlen($filename)-1);
            }
            $image = dirname($image) . '/'  . $filename . '.' .  explode('.', end($check))[1];
        }
    }

    return $image;
}

function isHoliday($date){

    // ponte Ognissanti 2018
    //$holidays[] = "2018-11-02";
    //$holidays[] = "2018-11-05";

    // capodanno
    $holidays[] = "01-01";

    //Epifania
    $holidays[] = "01-06";

    //25 aprile
    $holidays[] = "04-25";

    //1 maggio
    $holidays[] = "05-01";

    //2 giugno
    $holidays[] = "06-02";

    //ferragosto
    $holidays[] = "08-15";

    //Tutti i Santi
    $holidays[] = "11-01";

    //Immacolata concezione
    $holidays[] = "12-08";

    //Natale (vigilia e S.Stefano)
    $holidays[] = "12-25";
    $holidays[] = "12-26";

    //Ultimo dell'anno
    $holidays[] = "12-31";

    if( in_array(Carbon\Carbon::parse($date)->format('m-d'), $holidays) ):
        return true;
    else:
        return false;
    endif;
}


function addOsTags($user, $device_token){
    if ($user->email != "demo@demo.it"){

        $playerID = $device_token;

        $fields = array(
            'app_id' => env('ONESIGNAL_APP_ID'),
            'tags' => [
                'user_type' => $user->user_type,
                'city' => ($user->user_type==='company') ?
                        $user->shop->city :
                        $user->delivery_city,
                'prov' => ($user->user_type==='company') ? $user->shop->prov : '',
                'region' => ($user->user_type==='company') ? $user->shop->region : $user->delivery_region,
                'name' => ($user->user_type==='company') ? $user->shop->name : $user->name,
                'shop_type' => ($user->user_type==='company') ? $user->shop->shopType : ''
            ]
        );

        $fields = json_encode($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://onesignal.com/api/v1/players/'.$playerID);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($ch);
        curl_close($ch);

        $resultData = json_decode($response, true);
    }
}

function haversineGreatCircleDistance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo)
{
    $earthRadius = 6371000;

    // convert from degrees to radians
    $latFrom = deg2rad($latitudeFrom);
    $lonFrom = deg2rad($longitudeFrom);
    $latTo = deg2rad($latitudeTo);
    $lonTo = deg2rad($longitudeTo);

    $lonDelta = $lonTo - $lonFrom;
    $a = pow(cos($latTo) * sin($lonDelta), 2) +
      pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
    $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);

    $angle = atan2(sqrt($a), $b);

    return ($angle * $earthRadius) / 1000;  // Return distance in Km
}

function coordsFromAddress($address){
    $key = env('GOOGLE_MAPS_API_KEY');

    // Address $apiKey = '[MY_API_KEY]';
    // Google maps now requires an API key.
    // Get JSON results from this request
    $geo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false&key='.$key);
    $geo = json_decode($geo, true);
    // Convert the JSON to an array
    if (isset($geo['status']) && ($geo['status'] == 'OK')) {
        $latitude = $geo['results'][0]['geometry']['location']['lat'];
        // Latitude
        $longitude = $geo['results'][0]['geometry']['location']['lng'];
        // Longitude

        $res = [];
        $res['lat'] = $latitude;
        $res['lon'] = $longitude;
        return $res;

    }

    return null;
}

function getAddressComponents($address){
    $key = env('GOOGLE_MAPS_API_KEY');

    // Address $apiKey = '[MY_API_KEY]';
    // Google maps now requires an API key.
    // Get JSON results from this request
    $geo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false&key='.$key);
    $geo = json_decode($geo, true);
    // Convert the JSON to an array
    if (isset($geo['status']) && ($geo['status'] == 'OK')) {
        return $geo['results'][0];
    }

    return null;
}
