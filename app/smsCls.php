<?php
ini_set("soap.wsdl_cache_enabled", "0"); // disabling WSDL cache

class smsCls{
    private $conf=array(),$error=array(),$param,$messageResult;
    public $getEngine,$soapEngine;

    function __construct() {
        $this->conf['ip']='http://54.36.76.186';
        $this->conf['dir_base']='/sms/';
        $this->conf['user']='JtJy2RU3TrLoZgqiKfA9';
        $this->conf['pwd']='09ydZavmkv2lqtvZfFJJ';
        $this->conf['ws']='WSGateway/Gateway.asmx?wsdl';
        $this->conf['mittente']='primascelta';

        $this->param=array('userName'=>$this->conf['user'],'password'=>$this->conf['pwd']);
        $this->getEngine=$this->conf['ip'] . $this->conf['dir_base'] . $this->conf['ws'];

        try{
            //commentare riga sotto per disattivare
            $this->soapEngine = new SoapClient($this->conf['ip'].$this->conf['dir_base'].$this->conf['ws']);
        }
        catch (SoapFault $e){
            $this->error[]=$e;
            trigger_error('Errore istanza soap client'.$e, E_USER_WARNING);

        }
    }

    /*
     * conteggio degli sms restanti
     * @param
     * @return number
    */
    public function getStatus(){
        //decommentare return per disattivare il conteggio degli sms
        //return;
        if($this->isError())
            return 0;
        $response = $this->soapEngine->GetUserStatus($this->param);
        if($response->GetUserStatusResult->ErrorID!='None'){
            $this->error[]=$response->GetUserStatusResult->ErrorID.': '.$response->GetUserStatusResult->ErrorDescription;
            trigger_error(' Errore risposta soap'.$response->GetUserStatusResult->ErrorID.': '.$response->GetUserStatusResult->ErrorDescription,E_USER_WARNING);
            return 0;
        }

        return $response->GetUserStatusResult->Credits->CustomerCredit->Credits;
    }

    /*
     * invio di sms
     * @param string $testo       indica il testo del messaggio
     * @param array $destinatari  indica l'array contentente l'elenco di tutti i destinatari (uno o più destinatari)
     * @return boolean            true messaggio inviato, false messaggio non inviato o con errore
     *
     */
    public function inviaSms($testo,$destinatari){
      unset($this->error);
        if(count($destinatari)<=0)
            return;

        $tmp=array();
        foreach ($destinatari AS $index=>$item){
            $tmp['Contact'][]['PortablePhone']=$item;
        }

        $this->param['oAdC']=$this->conf['mittente'];
        $this->param['message']=$testo;
        $this->param['contacts']=$tmp;

        $return=$this->soapEngine->SendMessage1($this->param);

        if($return->SendMessage1Result->ErrorID!='None') {
            $this->error[] = $return->SendMessage1Result->Errors;
            trigger_error('Errore invio sms'.$return->SendMessage1Result->Errors,E_USER_WARNING);
            return false;
        }
        //memorizzo le risposte
        if($return->SendMessage1Result->Sent>0){
          $this->messageResult=$return->SendMessage1Result->MessageSents->Message;
          return true;
        }
        return false;
    }

    /*
     * verifica di errori a seguito di un invio
     * @return boolean    true sono presenti errori, false non ci sono errori
     */
    public  function isError(){return (bool)count($this->error);}

    /*
     * restituisce gli errori generati dal precedente invio
     * @return array    array di errori
     */
    public function getError(){return $this->error;}

    /*
     * restituisce il result dell'ultimo invio sms effettuato
     * @return string
     */
  public function getResult(){return $this->messageResult;}
}
