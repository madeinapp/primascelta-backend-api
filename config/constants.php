<?php

return [
    'orders' => [
        'draft' => 1,
        'not_confirmed' => 2,
        'confirmed_with_reserve' => 3,
        'confirmed' => 4,
        'delivered' => 6,
        'cancelled' => 7
    ],
    'APERTO' => 0,
    'CHIUSO' => 1
];
