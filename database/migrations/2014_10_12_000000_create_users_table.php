<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	Schema::disableForeignKeyConstraints();
        Schema::create('users', function (Blueprint $table) {
			$table->id();
			$table->string('email')->unique();
			$table->timestamp('email_verified_at')->nullable();
			$table->string('password');
			$table->string('facebook_id')->unique()->nullable();
			$table->string('api_token', 80)->unique()->nullable()->default(null);
			$table->string('otp')->integer()->unique()->nullable()->default(null);
			$table->string('name')->nullable()->default('');
			$table->date('birthdate')->nullable()->default(null);
			$table->string('phone')->nullable()->default(null);
			$table->string('other_phone')->nullable();
			$table->text('delivery_address')->nullable();
			$table->string('delivery_country')->nullable();
			$table->text('delivery_route')->nullable();
			$table->string('delivery_city')->nullable();
			$table->string('delivery_region')->nullable();
			$table->string('delivery_postal_code')->nullable();
			$table->string('delivery_street_number')->nullable();
			$table->string('delivery_lat')->nullable();
			$table->string('delivery_long')->nullable();
			$table->text('note_delivery_address')->nullable();
			$table->string('image_url')->nullable();
			$table->enum('user_type',['private','company'])->default('private');
			$table->integer('trust_points')->default(100);
			$table->enum('role', ['user', 'admin'])->default('user');
			$table->enum('status', ['active', 'inactive', 'pending'])->default('pending');
			$table->boolean('privacy')->default(true);
			$table->rememberToken();
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
