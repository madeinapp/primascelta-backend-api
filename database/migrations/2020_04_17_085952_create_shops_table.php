<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shops', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained();
            $table->foreignId('agent_id')->nullable();
            $table->foreignId('shop_type_id')->default(1)->constrained();
            $table->string('website')->nullable();
            $table->text('name');
            $table->text('address')->nullable();
            $table->string('country')->nullable();
            $table->text('route')->nullable();
            $table->string('city')->nullable();
            $table->string('region')->nullable();
            $table->string('postal_code')->nullable();
            $table->string('street_number')->nullable();
            $table->string('lat')->nullable();
            $table->string('long')->nullable();
            $table->text('whatsapp')->nullable();
            $table->string('facebook_page')->nullable();
            $table->string('instagram_page')->nullable();
            $table->text('description')->nullable();
            $table->longText('opening_timetable')->nullable();
            $table->boolean('delivery_on_site')->default(true);
            $table->text('delivery_address')->nullable()->default(null);
            $table->string('delivery_address_country')->nullable();
            $table->text('delivery_address_route')->nullable();
            $table->string('delivery_address_city')->nullable();
            $table->string('delivery_address_region')->nullable();
            $table->string('delivery_address_postal_code')->nullable();
            $table->string('delivery_address_street_number')->nullable();
            $table->string('delivery_address_lat')->nullable();
            $table->string('delivery_address_long')->nullable();
            $table->longText('delivery_opening_timetable')->nullable()->default(null);
            $table->boolean('home_delivery')->default(false);
            $table->integer('delivery_range_km')->nullable();
            $table->text('delivery_range_notes')->nullable();
            $table->boolean('delivery_host_on_site')->default(false);
            $table->boolean('delivery_always_free')->default(false);
            $table->decimal('delivery_free_price_greater_than', 10, 2)->nullable()->default(0);
            $table->decimal('delivery_forfait_cost_price', 10, 2)->nullable()->default(0);
            $table->integer('delivery_percentage_cost_price')->nullable()->default(0);
            $table->integer('delivery_cancellable_hours_limit')->nullable()->default(0);
            $table->integer('min_trust_points_percentage_bookable')->nullable();
            $table->string('tax_business_name');
            $table->integer('tax_type_id');
            $table->string('tax_vat')->nullable()->default(null);
            $table->string('tax_fiscal_code')->nullable()->default(null);
            $table->string('tax_code')->nullable()->default(null);
            $table->string('tax_address')->nullable()->default(null);
            $table->string('tax_address_country')->nullable();
            $table->text('tax_address_route')->nullable();
            $table->string('tax_address_city')->nullable();
            $table->string('tax_address_prov')->nullable();
            $table->string('tax_address_region')->nullable();
            $table->string('tax_address_postal_code')->nullable();
            $table->string('tax_address_street_number')->nullable();
            $table->string('tax_address_lat')->nullable();
            $table->string('tax_address_long')->nullable();
            $table->enum('status', ['active', 'inactive', 'pending'])->default('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shops');
    }
}
