<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFoodsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('foods', function (Blueprint $table) {
            $table->id();
            $table->foreignId('food_category_id')->constrained()->onDelete('cascade')->default(0);
            $table->foreignId('unit_of_measure_id')->constrained()->default(1);
            $table->string('name');
            $table->text('description')->nullable();
            $table->string('type');
	        $table->string('image')->nullable()->default(null);
	        $table->boolean('available')->default(true);
            $table->timestamps();
	        $table->index('name');
	        $table->index('type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('foods');
    }
}
