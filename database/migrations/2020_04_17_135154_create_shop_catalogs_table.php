<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShopCatalogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_catalogs', function (Blueprint $table) {
            $table->id();
            $table->foreignId('shop_id')->constrained();
            $table->foreignId('food_category_id')->constrained();
            $table->foreignId('unit_of_measure_id')->nullable()->default(null)->constrained();
            $table->float('food_unit_of_measure_quantity',8,2)->nullable()->default(null);
            $table->integer('food_id')->nullable();
            $table->string('food_name');
            $table->text('food_description')->nullable()->default(null);
            $table->string('food_type');
            $table->float('food_price',8,2);
            $table->string('food_image')->nullable()->default(null);
            $table->boolean('visible_in_search')->default(true);
            $table->boolean('home_delivery')->default(true);
            $table->boolean('in_app_shopping')->default(true);
            $table->string('short_description', 30)->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_catalogs');
    }
}
