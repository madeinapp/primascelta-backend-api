<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShopOfferFoods extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_offer_foods', function (Blueprint $table) {
			$table->id();
			$table->foreignId('shop_offer_id')->constrained();
			$table->foreignId('shop_catalog_id')->constrained();
			$table->string('food_name');
			$table->string('food_image')->nullable()->default(null);
			$table->text('food_description')->nullable()->default(null);
            $table->integer('min_quantity')->nullable()->default(null);
            $table->decimal('discount',5,2)->nullable();
			$table->float('discounted_price',8,2)->nullable();
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_offer_foods');
    }
}
