<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->foreignId('shop_id')->constrained();
            $table->foreignId('user_id')->constrained();
            $table->unsignedBigInteger('order_status_id');
	    	$table->float('total_price',8,2)->nullable();
            $table->float('shipping_price',8,2)->nullable();
            $table->tinyInteger('pickup_on_site')->nullable();
            $table->date('pickup_on_site_date')->nullable();
            $table->time('pickup_on_site_time')->nullable();
            $table->tinyInteger('home_delivery')->nullable();
            $table->string('home_delivery_address')->nullable();
            $table->string('home_delivery_country')->nullable();
            $table->string('home_delivery_route')->nullable();
            $table->string('home_delivery_city')->nullable();
            $table->string('home_delivery_region')->nullable();
            $table->string('home_delivery_postal_code')->nullable();
            $table->string('home_delivery_street_number')->nullable();
            $table->string('home_delivery_lat')->nullable();
            $table->string('home_delivery_long')->nullable();
            $table->string('home_delivery_name')->nullable();
            $table->string('home_delivery_phone')->nullable();
            $table->date('home_delivery_date')->nullable();
            $table->time('home_delivery_time')->nullable();
            $table->integer('home_delivery_payment_method_id')->nullable();
            $table->tinyInteger('delivery_host_on_site')->nullable();
            $table->date('delivery_host_on_site_date')->nullable();
            $table->time('delivery_host_on_site_time')->nullable();
            $table->text('delivery_host_on_site_note')->nullable();
            $table->integer('delivery_host_on_site_payment_method_id')->nullable();
            $table->datetime('pickup_on_site_within')->nullable();
            $table->datetime('home_delivery_within')->nullable();
            $table->datetime('delivery_host_on_site_within')->nullable();
            $table->datetime('cancellable_within')->nullable();
            $table->datetime('confirm_reserve_within')->nullable();
            $table->enum('flag_trust_points', ['none', 'add', 'subtract'])->default('none')->nullable();
            $table->timestamps();

            $table->foreign('order_status_id')->references('id')->on('order_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
