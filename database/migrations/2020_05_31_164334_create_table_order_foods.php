<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableOrderFoods extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_foods', function (Blueprint $table) {
            $table->id();
            $table->foreignId('order_id')->constrained();
            $table->foreignId('shop_catalog_id')->nullable()->constrained()->default(null);
            $table->foreignId('shop_offer_food_id')->nullable()->constrained()->default(null);
            $table->string('food_name');
            $table->string('food_image');
            $table->text('food_description');
            $table->integer('quantity')->default(1);
            $table->float('price',8,2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_foods');
    }
}
