<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOrdersColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * `pickup_on_site` TINYINT(1) NOT NULL DEFAULT '0',
            `pickup_on_site_date` DATE NULL DEFAULT NULL,
            `pickup_on_site_time` TIME NULL DEFAULT NULL,
            `home_delivery` TINYINT(1) NULL DEFAULT '0',
            `home_delivery_address` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
            `home_delivery_address_country` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
            `home_delivery_address_route` TEXT(65535) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
            `home_delivery_address_city` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
            `home_delivery_address_region` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
            `home_delivery_address_postal_code` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
            `home_delivery_address_street_number` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
            `home_delivery_address_lat` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
            `home_delivery_address_long` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
            `home_delivery_name` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
            `home_delivery_phone` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
            `home_delivery_date` DATE NULL DEFAULT NULL,
            `home_delivery_time` TIME NULL DEFAULT NULL,
            `home_delivery_payment_method_id` BIGINT(20) UNSIGNED NULL DEFAULT NULL,
            `delivery_host_on_site` TINYINT(1) NULL DEFAULT '0',
            `delivery_host_on_site_date` DATE NULL DEFAULT NULL,
            `delivery_host_on_site_time` TIME NULL DEFAULT NULL,
            `delivery_host_on_site_note` TEXT(65535) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
            `delivery_host_on_site_payment_method_id` BIGINT(20) UNSIGNED NULL DEFAULT NULL,
         */
        Schema::table('orders', function (Blueprint $table) {
            $table->boolean('pickup_on_site')->default(0)->after('note');
            $table->date('pickup_on_site_date')->nullable()->after('pickup_on_site');
            $table->time('pickup_on_site_time')->nullable()->after('pickup_on_site_date');
            $table->boolean('home_delivery')->default(0)->after('pickup_on_site_time');
            $table->string('home_delivery_address')->nullable()->after('home_delivery');
            $table->string('home_delivery_country')->nullable()->after('home_delivery_address');
            $table->text('home_delivery_route')->nullable()->after('home_delivery_country');
            $table->string('home_delivery_city')->nullable()->after('home_delivery_route');
            $table->string('home_delivery_region')->nullable()->after('home_delivery_city');
            $table->string('home_delivery_postal_code')->nullable()->after('home_delivery_region');
            $table->string('home_delivery_street_number')->nullable()->after('home_delivery_postal_code');
            $table->string('home_delivery_lat')->nullable()->after('home_delivery_street_number');
            $table->string('home_delivery_long')->nullable()->after('home_delivery_lat');
            $table->string('home_delivery_name')->nullable()->after('home_delivery_long');
            $table->string('home_delivery_phone')->nullable()->after('home_delivery_name');
            $table->date('home_delivery_date')->nullable()->after('home_delivery_phone');
            $table->time('home_delivery_time')->nullable()->after('home_delivery_date');
            $table->unsignedBigInteger('home_delivery_payment_method_id')->nullable()->after('home_delivery_time');
            $table->boolean('delivery_host_on_site')->default(0)->after('home_delivery_payment_method_id');
            $table->date('delivery_host_on_site_date')->nullable()->after('delivery_host_on_site');
            $table->time('delivery_host_on_site_time')->nullable()->after('delivery_host_on_site_date');
            $table->text('delivery_host_on_site_note')->nullable()->after('delivery_host_on_site_time');
            $table->unsignedBigInteger('delivery_host_on_site_payment_method_id')->nullable()->after('delivery_host_on_site_note');

            $table->foreign('home_delivery_payment_method_id')->references('id')->on('payment_methods');
            $table->foreign('delivery_host_on_site_payment_method_id')->references('id')->on('payment_methods');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('pickup_on_site');
            $table->dropColumn('pickup_on_site_date');
            $table->dropColumn('pickup_on_site_time');
            $table->dropColumn('home_delivery');
            $table->dropColumn('home_delivery_address');
            $table->dropColumn('home_delivery_country');
            $table->dropColumn('home_delivery_route');
            $table->dropColumn('home_delivery_city');
            $table->dropColumn('home_delivery_region');
            $table->dropColumn('home_delivery_postal_code');
            $table->dropColumn('home_delivery_street_number');
            $table->dropColumn('home_delivery_lat');
            $table->dropColumn('home_delivery_long');
            $table->dropColumn('home_delivery_name');
            $table->dropColumn('home_delivery_phone');
            $table->dropColumn('home_delivery_date');
            $table->dropColumn('home_delivery_time');
            $table->dropColumn('home_delivery_payment_method_id');
            $table->dropColumn('delivery_host_on_site');
            $table->dropColumn('delivery_host_on_site_date');
            $table->dropColumn('delivery_host_on_site_time');
            $table->dropColumn('delivery_host_on_site_note');
            $table->dropColumn('delivery_host_on_site_payment_method_id');
        });
    }
}
