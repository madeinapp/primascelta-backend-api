<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShopCatalogSpecialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_catalog_specials', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('shop_catalog_id');
            $table->unsignedBigInteger('special_id');

            $table->foreign('shop_catalog_id')->references('id')->on('shop_catalogs');
            $table->foreign('special_id')->references('id')->on('specials');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_catalog_specials');
    }
}
