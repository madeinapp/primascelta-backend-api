<?php

use Illuminate\Database\Seeder;

class AgentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Schema::disableForeignKeyConstraints();
	DB::table('agents')->delete();
        DB::table('agents')->insert(
                                [
                                    'id' => 1,
                                    'name' => 'Agente Test',
                                    'phone' => '123456789',
                                    'coupon' => 'abc123',
                                ]);
    }
}
