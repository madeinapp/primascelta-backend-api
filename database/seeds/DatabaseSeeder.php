<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        $this->call('ShopSeeder');
        $this->call('UserSeeder');
        $this->call('FoodCategorySeeder');
        $this->call('UnitOfMeasuresSeeder');
        $this->call('ShopTypeSeeder');
        $this->call('PaymentMethodsSeeder');
        $this->call('DeliveryMethodsSeeder');
        $this->call('DeliveryMethodShopSeeder');
        $this->call('PaymentMethodShopSeeder');
        $this->call('OrderStatusSeeder');
        $this->call('TaxTypeSeeder');
        $this->call('AgentSeeder');
        $this->call('ShopCatalogSeeder');
        $this->call('UserSubscriptionSeeder');
        $this->call('ShopOfferSeeder');
        $this->call('ShopOfferFoodSeeder');
    }
}
