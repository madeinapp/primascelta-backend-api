<?php

use Illuminate\Database\Seeder;

class DeliveryMethodShopSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
	DB::table('delivery_method_shop')->delete();
        DB::table('delivery_method_shop')->insert(['shop_id' => 1, 'delivery_method_id' => 2]);
        DB::table('delivery_method_shop')->insert(['shop_id' => 1, 'delivery_method_id' => 3]);
    }
}
