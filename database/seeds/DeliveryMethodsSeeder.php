<?php

use Illuminate\Database\Seeder;

class DeliveryMethodsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
	DB::table('delivery_methods')->delete();
	DB::table('delivery_methods')->insert(['id' => 1, 'name' => 'Not Selected']);
	DB::table('delivery_methods')->insert(['id' => 2, 'name' => 'Home delivery']);
	DB::table('delivery_methods')->insert(['id' => 3, 'name' => 'Delivery on site']);
	DB::table('delivery_methods')->insert(['id' => 4, 'name' => 'I\'m in your site. Please deliver here:']);
    }
}
