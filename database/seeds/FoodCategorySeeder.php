<?php

use Illuminate\Database\Seeder;

class FoodCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('food_categories')->delete();
		DB::table('food_categories')->insert(['id' => 1, 'name' => 'VERDURA']);
		DB::table('food_categories')->insert(['id' => 2, 'name' => 'FRUTTA']);
		DB::table('food_categories')->insert(['id' => 3, 'name' => 'PESCE']);
		DB::table('food_categories')->insert(['id' => 4, 'name' => 'CARNE']);
		DB::table('food_categories')->insert(['id' => 5, 'name' => 'PASTA FRESCA']);
		DB::table('food_categories')->insert(['id' => 6, 'name' => 'PIZZA']);
		DB::table('food_categories')->insert(['id' => 7, 'name' => 'DOLCE']);
		DB::table('food_categories')->insert(['id' => 8, 'name' => 'GELATO']);
		DB::table('food_categories')->insert(['id' => 9, 'name' => 'DA BERE']);
		DB::table('food_categories')->insert(['id' => 10, 'name' => 'DA DISPENSA']);
		DB::table('food_categories')->insert(['id' => 11, 'name' => 'PANE']);
		DB::table('food_categories')->insert(['id' => 12, 'name' => 'PIATTO PREPARATO']);
		DB::table('food_categories')->insert(['id' => 13, 'name' => 'DA BAR']);
		DB::table('food_categories')->insert(['id' => 14, 'name' => 'DA FRIGO']);
    }
}
