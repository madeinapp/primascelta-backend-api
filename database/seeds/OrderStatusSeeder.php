<?php

use Illuminate\Database\Seeder;

class OrderStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
	DB::table('order_status')->delete();
        DB::table('order_status')->insert(
                                    [
                                        'id' => 1,
                                        'name' => 'draft',
                                        'description' => '',
                                    ]);
        DB::table('order_status')->insert(
                                    [
                                        'id' => 2,
                                        'name' => 'payed',
                                        'description' => '',
                                    ]);
        DB::table('order_status')->insert(
                                    [
                                        'id' => 3,
                                        'name' => 'prepare',
                                        'description' => '',
                                    ]);
        DB::table('order_status')->insert(
                                    [
                                        'id' => 4,
                                        'name' => 'shipping',
                                        'description' => '',
                                    ]);
        DB::table('order_status')->insert(
                                    [
                                        'id' => 5,
                                        'name' => 'shipped',
                                        'description' => '',
                                    ]);
        DB::table('order_status')->insert(
                                    [
                                        'id' => 6,
                                        'name' => 'suspended',
                                        'description' => '',
                                    ]);
        DB::table('order_status')->insert(
                                    [
                                        'id' => 7,
                                        'name' => 'cancelled',
                                        'description' => '',
                                    ]);
        DB::table('order_status')->insert(
                                    [
                                        'id' => 8,
                                        'name' => 'archived',
                                        'description' => '',
                                    ]);
    }
}
