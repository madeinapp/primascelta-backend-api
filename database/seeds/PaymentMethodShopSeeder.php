<?php

use Illuminate\Database\Seeder;

class PaymentMethodShopSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
	DB::table('payment_method_shop')->delete();
        DB::table('payment_method_shop')->insert(['shop_id' => 1, 'payment_method_id' => 2]);
        DB::table('payment_method_shop')->insert(['shop_id' => 1, 'payment_method_id' => 3]);
        DB::table('payment_method_shop')->insert(['shop_id' => 1, 'payment_method_id' => 4]);
    }
}
