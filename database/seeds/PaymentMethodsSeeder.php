<?php

use Illuminate\Database\Seeder;

class PaymentMethodsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
	DB::table('payment_methods')->delete();
	DB::table('payment_methods')->insert(['id' => 1, 'name' => 'Non selezionato']);
	DB::table('payment_methods')->insert(['id' => 2, 'name' => 'PayPal']);
	DB::table('payment_methods')->insert(['id' => 3, 'name' => 'Carta di Credito']);
	DB::table('payment_methods')->insert(['id' => 4, 'name' => 'Pagamento alla consegna']);
    }
}
