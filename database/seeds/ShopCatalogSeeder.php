<?php

use Illuminate\Database\Seeder;

class ShopCatalogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
		DB::table('shop_catalogs')->delete();
        DB::table('shop_catalogs')->insert([
                                    'id' => 1, 
                                    'shop_id' => 1, 
                                    'food_category_id' => 1, 
                                    'unit_of_measure_id' => 2, 
                                    'food_unit_of_measure_quantity' => '3', 
                                    'food_id' => 753, 
                                    'food_name' => 'Melanzana nera lunga',
                                    'food_description' => 'Colore nero brillante e forma cilindrica allungata, polpa bianca di ottima consistenza Molto versatile poiché che si taglia a nastri che diventano rotolini e involtini da farcire.',
                                    'food_type' => 'DA FRUTTO',
                                    'food_price' => '5.5',
                                    'food_image' => '',
                                    'visible_in_search' => 1,
                                    
                                ]);
        DB::table('shop_catalogs')->insert([
                                    'id' => 2, 
                                    'shop_id' => 1, 
                                    'food_category_id' => 1, 
                                    'unit_of_measure_id' => 2, 
                                    'food_unit_of_measure_quantity' => '3', 
                                    'food_id' => null, 
                                    'food_name' => 'Melanzana bunga bunga',
                                    'food_description' => 'Colore nero brillante e forma cilindrica allungata, polpa bianca di ottima consistenza Molto versatile poiché che si taglia a nastri che diventano rotolini e involtini da farcire.',
                                    'food_type' => 'DA FRUTTO',
                                    'food_price' => '15.5',
                                    'food_image' => '',
                                    'visible_in_search' => 1,
                                    
                                ]);
    }
}
