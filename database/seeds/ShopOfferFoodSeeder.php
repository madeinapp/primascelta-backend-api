<?php

use Illuminate\Database\Seeder;

class ShopOfferFoodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
		DB::table('shop_offer_foods')->delete();
        DB::table('shop_offer_foods')->insert([
									'shop_offer_id' => 1,
									'shop_catalog_id' => 1,
									'food_name' => 'Melanzana nera lunga',
									'food_description' => 'Colore nero brillante e forma cilindrica allungata, polpa bianca di ottima consistenza Molto versatile poiché che si taglia a nastri che diventano rotolini e involtini da farcire.',
									'min_quantity' => 3,
									'discounted_price' => 15,
								]);
        DB::table('shop_offer_foods')->insert([
									'shop_offer_id' => 1,
									'shop_catalog_id' => 2,
									'food_name' => 'Melanzana bunga bunga',
									'food_description' => 'Colore nero brillante e forma cilindrica allungata, polpa bianca di ottima consistenza Molto versatile poiché che si taglia a nastri che diventano rotolini e involtini da farcire.',
									'min_quantity' => 2,
									'discounted_price' => 5,
								]);
        DB::table('shop_offer_foods')->insert([
									'shop_offer_id' => 2,
									'shop_catalog_id' => 2,
									'food_name' => 'Melanzana bunga bunga',
									'food_description' => 'Colore nero brillante e forma cilindrica allungata, polpa bianca di ottima consistenza Molto versatile poiché che si taglia a nastri che diventano rotolini e involtini da farcire.',
									'min_quantity' => 1,
									'discounted_price' => 10,
								]);
    }
}
