<?php

use Illuminate\Database\Seeder;

class ShopOfferSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
		DB::table('shop_offers')->delete();
        DB::table('shop_offers')->insert([
									'id' => 1,
									'shop_id' => 1,
									'name' => 'Cetrioli a gogò',
									'description' => 'Offerta per persone insoddisfatte',
									'start_date' => '2020-07-01',
									'end_date' => '2021-07-01',
									'status' => 'available',
                                ]);
        DB::table('shop_offers')->insert([
									'id' => 2,
									'shop_id' => 1,
									'name' => 'Trionfo di banane',
									'description' => 'Banane per tutti gli utilizzi',
									'start_date' => '2020-07-12',
									'end_date' => '2021-07-12',
									'status' => 'available',
                                ]);
        DB::table('shop_offers')->insert([
									'id' => 3,
									'shop_id' => 1,
									'name' => 'Zucchine giganti',
									'description' => 'Buone e gustose',
									'start_date' => '2020-07-15',
									'end_date' => '2021-07-15',
									'status' => 'available',
                                ]);
    }
}
