<?php

use Illuminate\Database\Seeder;

class ShopSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Schema::disableForeignKeyConstraints();
		DB::table('shops')->delete();
                DB::table('shops')->insert(
                    [
                        'id' => 1,
                        'user_id' => 1,
                        'shop_type_id' => 6,
                        'name' => 'Il cetriolo volante',
                        'description' => 'Test',
                        'tax_business_name' => 'Il cetriolo volante',
                        'tax_vat' => '1236456789',
                        'tax_type_id' => '1',
                        'tax_fiscal_code' => 'ctrlvlnt',
                        'status' => 'active',
                    ]);
                DB::table('shops')->insert(
                    [
                        'id' => 2,
                        'user_id' => 6,
                        'shop_type_id' => 6,
                        'name' => 'Il tuo macellaio',
                        'description' => 'Test',
                        'tax_business_name' => 'Il tuo macellaio',
                        'tax_vat' => '1236456789',
                        'tax_type_id' => '1',
                        'tax_fiscal_code' => 'ctrlvlnt',
                        'status' => 'active',
                    ]);
                DB::table('shops')->insert(
                    [
                        'id' => 3,
                        'user_id' => 8,
                        'shop_type_id' => 2,
                        'name' => 'Test',
                        'description' => 'Test',
                        'tax_business_name' => 'Il tuo macellaio',
                        'tax_vat' => '1236456789',
                        'tax_type_id' => '1',
                        'tax_fiscal_code' => 'ctrlvlnt',
                        'status' => 'active',
						'address' => 'San Francisco, California, Stati Uniti',
						'country' => 'United States',
						'city' => 'San Francisco',
						'region' => 'California',
						'lat' => '37.7749295',
						'long' => '-122.4194155',
						'whatsapp' => '12345678',
						'opening_timetable' => '{"monday":{"active":false,"first_half":{"opening_time":"8:00","closing_time":"12:30"},"second_half":{"opening_time":"12:00","closing_time":"19:00"}},"tuesday":{"active":false,"first_half":{"opening_time":"8:00","closing_time":"12:30"},"second_half":{"opening_time":"12:00","closing_time":"19:00"}},"wednesday":{"active":false,"first_half":{"opening_time":"8:00","closing_time":"12:30"},"second_half":{"opening_time":"12:00","closing_time":"19:00"}},"thursday":{"active":false,"first_half":{"opening_time":"8:00","closing_time":"12:30"},"second_half":{"opening_time":"12:00","closing_time":"19:00"}},"friday":{"active":false,"first_half":{"opening_time":"8:00","closing_time":"12:30"},"second_half":{"opening_time":"12:00","closing_time":"19:00"}},"saturday":{"active":false,"first_half":{"opening_time":"8:00","closing_time":"12:30"},"second_half":{"opening_time":"12:00","closing_time":"19:00"}},"sunday":{"active":false,"first_half":{"opening_time":"8:00","closing_time":"12:30"},"second_half":{"opening_time":"12:00","closing_time":"19:00"}}}',
						'delivery_on_site' => 1,
						'delivery_opening_timetable' => '{"monday":{"active":false,"first_half":{"opening_time":"8:00","closing_time":"12:30"},"second_half":{"opening_time":"12:00","closing_time":"19:00"}},"tuesday":{"active":false,"first_half":{"opening_time":"8:00","closing_time":"12:30"},"second_half":{"opening_time":"12:00","closing_time":"19:00"}},"wednesday":{"active":false,"first_half":{"opening_time":"8:00","closing_time":"12:30"},"second_half":{"opening_time":"12:00","closing_time":"19:00"}},"thursday":{"active":false,"first_half":{"opening_time":"8:00","closing_time":"12:30"},"second_half":{"opening_time":"12:00","closing_time":"19:00"}},"friday":{"active":false,"first_half":{"opening_time":"8:00","closing_time":"12:30"},"second_half":{"opening_time":"12:00","closing_time":"19:00"}},"saturday":{"active":false,"first_half":{"opening_time":"8:00","closing_time":"12:30"},"second_half":{"opening_time":"12:00","closing_time":"19:00"}},"sunday":{"active":false,"first_half":{"opening_time":"8:00","closing_time":"12:30"},"second_half":{"opening_time":"12:00","closing_time":"19:00"}}}',
						'delivery_host_on_site' => 0,
						'delivery_always_free' => 1,
						'tax_business_name' => 'Test',
						'tax_fiscal_code' => 'Testq',
						'tax_code' => 'Testq',
						'tax_address' => 'San Diego, California, Stati Uniti',
						'tax_address_country' => 'United Statea',
                        'tax_address_city' => 'San Diego',
                        'tax_address_prov' => 'LA',
						'tax_address_region' => 'California',

                    ]);
    }
}
