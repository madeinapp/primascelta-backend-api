<?php

use Illuminate\Database\Seeder;

class ShopTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {        
		DB::table('shop_types')->delete();		
		DB::table('shop_types')->insert(['id' => 2, 'name' => 'Bar']);
		DB::table('shop_types')->insert(['id' => 3, 'name' => 'Camping']);
		DB::table('shop_types')->insert(['id' => 4, 'name' => 'Forno / Panificio']);
		DB::table('shop_types')->insert(['id' => 6, 'name' => 'Frutta e Verdura']);
		DB::table('shop_types')->insert(['id' => 7, 'name' => 'Gelateria']);
		DB::table('shop_types')->insert(['id' => 8, 'name' => 'Macelleria']);
		DB::table('shop_types')->insert(['id' => 9, 'name' => 'Mini Market']);
		DB::table('shop_types')->insert(['id' => 10, 'name' => 'Pasta Fresca']);
		DB::table('shop_types')->insert(['id' => 11, 'name' => 'Pasticceria']);
		DB::table('shop_types')->insert(['id' => 12, 'name' => 'Pescheria']);
		DB::table('shop_types')->insert(['id' => 13, 'name' => 'Pizza al taglio']);
		DB::table('shop_types')->insert(['id' => 14, 'name' => 'Pizzeria']);
		DB::table('shop_types')->insert(['id' => 15, 'name' => 'Prodotti bio']);
		DB::table('shop_types')->insert(['id' => 16, 'name' => 'Ristorante']);
		DB::table('shop_types')->insert(['id' => 17, 'name' => 'Tavola calda']);
		DB::table('shop_types')->insert(['id' => 18, 'name' => 'Stabilimento']);
		DB::table('shop_types')->insert(['id' => 19, 'name' => 'Villaggio']);
		DB::table('shop_types')->insert(['id' => 20, 'name' => 'Altra attività']);
    }
}
