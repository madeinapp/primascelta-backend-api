<?php

use Illuminate\Database\Seeder;

class TaxTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
		DB::table('tax_types')->delete();
		DB::table('tax_types')->insert(['id' => 1, 'name' => 'S.r.l.']);
		DB::table('tax_types')->insert(['id' => 2, 'name' => 'S.p.a.']);
    }
}
