<?php

use Illuminate\Database\Seeder;

class UnitOfMeasuresSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
		DB::table('unit_of_measures')->delete();
		DB::table('unit_of_measures')->insert(['id' => 1, 'name' => 'Non selezionato']);
		DB::table('unit_of_measures')->insert(['id' => 2, 'name' => 'Kg']);
		DB::table('unit_of_measures')->insert(['id' => 3, 'name' => 'Hg']);
		DB::table('unit_of_measures')->insert(['id' => 4, 'name' => 'Unita']);
		DB::table('unit_of_measures')->insert(['id' => 5, 'name' => 'Porzione']);
		DB::table('unit_of_measures')->insert(['id' => 6, 'name' => 'Mezza Porzione']);
		DB::table('unit_of_measures')->insert(['id' => 7, 'name' => 'Confezione']);
		DB::table('unit_of_measures')->insert(['id' => 8, 'name' => 'Vaschetta']);
		DB::table('unit_of_measures')->insert(['id' => 9, 'name' => 'Mazzetto']);
		DB::table('unit_of_measures')->insert(['id' => 10, 'name' => 'Unità']);
		DB::table('unit_of_measures')->insert(['id' => 11, 'name' => 'Piccolo - 1 gusto']);
		DB::table('unit_of_measures')->insert(['id' => 12, 'name' => 'Medio - 2 gusti']);
		DB::table('unit_of_measures')->insert(['id' => 13, 'name' => 'Grande - 3 gusti']);
		DB::table('unit_of_measures')->insert(['id' => 14, 'name' => 'Pezzo']);
		DB::table('unit_of_measures')->insert(['id' => 15, 'name' => 'Gr']);
    }
}
