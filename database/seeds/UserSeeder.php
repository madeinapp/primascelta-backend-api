<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
                DB::table('users')->delete();
		DB::table('users')->insert([
                        'id' => 1, 
                        'name' => 'Lino Mazza', 
                        'phone' => '06060606',
                        'user_type' => 'company',
                        'role' => 'admin',
                        'email' => 'linuzzin@gmail.com', 
                        'password' => '$2b$10$o6Qi9FHzu9coq872i04Gluw/hEajCvXeFBosbo7SznzYaLzzZuT7a', 
						'birthdate' => '2000-01-01',
                        'status' => 1, 
						'api_token' => 'b4ff58288a31cb8250903a62c735d4b00605423a9bf50ee08109cd3a94e3dc6c',
                        'created_at' => date("Y-m-d H:i:s"), 
                        'updated_at' => date("Y-m-d H:i:s")
                ]);
		DB::table('users')->insert([
                        'id' => 2, 
                        'name' => 'Emanuele Gennuso', 
                        'phone' => '3403863229',
                        'user_type' => 'private',
                        'role' => 'admin',
                        'email' => 'e.gennuso@gmail.com', 
                        'password' => '$2y$10$GqGFkLxvB.Hkbdq/9OSShObqz./YylQMQbpM8ksyDno2bJZrIRYXi', 
						'birthdate' => '2000-01-01',
                        'status' => 1, 
                        'created_at' => date("Y-m-d H:i:s"), 
                        'updated_at' => date("Y-m-d H:i:s")
                ]);
		DB::table('users')->insert([
                        'id' => 3, 
                        'name' => 'Ankan Biswas', 
                        'phone' => '+918961637988',
                        'user_type' => 'private',
                        'role' => 'admin',
                        'email' => 'discuss@meliodus.org', 
                        'password' => '$2y$10$vZFoaVokBu2/siaUFfRQQuqeT.RHHYmU1h0.r./7lfPd478/IhIAW', 
						'birthdate' => '2000-01-01',
                        'status' => 1, 
                        'created_at' => date("Y-m-d H:i:s"), 
                        'updated_at' => date("Y-m-d H:i:s")
                ]);
		DB::table('users')->insert([
                        'id' => 4, 
                        'name' => 'Giordano', 
                        'phone' => '3392035393',
                        'user_type' => 'private',
                        'role' => 'admin',
                        'email' => 'artisgrafica@gmail.com', 
                        'password' => '$2y$10$CP8bMNN7v34VV2ZRjyCmM.ntdqc0Ct63r.RxfrVOkoNuGFu5djo3K', 
						'birthdate' => '2000-01-01',
                        'status' => 1, 
                        'created_at' => date("Y-m-d H:i:s"), 
                        'updated_at' => date("Y-m-d H:i:s")
                ]);
		DB::table('users')->insert([
				'id' => 5, 
				'name' => 'Alessandro Terracciano', 
				'phone' => '3921971016',
				'user_type' => 'private',
                'role' => 'admin',
				'email' => 'info@dietrolangolo.biz', 
				'password' => '$2y$10$IAB4rMCdGpjwvCJqHuCQe.9qWgsTEFwbpIozyarkYzCzMDy85K14O', 
				'birthdate' => '2000-01-01',
				'status' => 1, 
				'created_at' => date("Y-m-d H:i:s"), 
				'updated_at' => date("Y-m-d H:i:s")
		]);
        DB::table('users')->insert([
            'id' => 6, 
            'name' => 'Carlo Rossi', 
            'phone' => '06060606',
            'user_type' => 'company',
            'role' => 'user',
            'email' => 'carlorossi@gmail.com', 
            'password' => '$2y$12$l0K57znbd2XgUi1waGnWE.3x8ssrnUQyikhMZ5scrPl5m8qJwF0ti',  //R0ss1_2020
            'birthdate' => '2000-01-01',
            'status' => 1, 
            'created_at' => date("Y-m-d H:i:s"), 
            'updated_at' => date("Y-m-d H:i:s")
    ]);
        DB::table('users')->insert([
            'id' => 7, 
            'name' => 'John', 
            'phone' => '06060606',
            'user_type' => 'private',
            'role' => 'user',
            'email' => 'demo@demo.it', 
            'password' => '$2y$10$kU1HC67ORGm4HQvNh9BMfujj970z4LANsstq4nqtG8ku/t.sWlhw.',
			'api_token' => '3598dcde61ad2fd7b73d01972615b2e82a91fdaccf47e2ac92dc69c19b0a1451',
            'birthdate' => '1997-07-15',
			'delivery_address' => 'San Francisco, CA, USA',
			'delivery_country' => 'United States',
			'delivery_route' => 'N/A',
			'delivery_city' => 'San Francisco',
			'delivery_region' => 'California',
			'delivery_street_number' => 'N/A',
			'delivery_lat' => '37.7749295',
			'delivery_long' => '-122.4194155',
			'note_delivery_address' => 'Test',
		    'trust_points' => 100,	
            'status' => 1, 
            'created_at' => date("Y-m-d H:i:s"), 
            'updated_at' => date("Y-m-d H:i:s")
    ]);
        DB::table('users')->insert([
            'id' => 8, 
            'name' => 'John', 
            'phone' => '06060606',
            'user_type' => 'company',
            'role' => 'user',
            'email' => 'company2020@gmail.com', 
            'password' => '$2y$10$Q7YvFVN9kpPWJxCdF1muWuoqXm2k16lFVCFPW4aQOzlDW.YMkr7.G',
			'api_token' => 'c701be4cce639866e4068a237472cb7a6f7c8e7544df49fbc260d284199b7803',
            'birthdate' => '1997-07-15',
			'delivery_address' => 'San Francisco, CA, USA',
			'delivery_country' => 'United States',
			'delivery_route' => 'N/A',
			'delivery_city' => 'San Francisco',
			'delivery_region' => 'California',
			'delivery_street_number' => 'N/A',
			'delivery_lat' => '37.7749295',
			'delivery_long' => '-122.4194155',
			'note_delivery_address' => 'Test',
		    'trust_points' => 100,	
            'status' => 1, 
            'created_at' => date("Y-m-d H:i:s"), 
            'updated_at' => date("Y-m-d H:i:s")
    ]);
    }
}
