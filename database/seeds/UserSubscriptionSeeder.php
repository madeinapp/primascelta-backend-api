<?php

use Illuminate\Database\Seeder;

class UserSubscriptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
		DB::table('user_subscriptions')->delete();
		DB::table('user_subscriptions')->insert(
										[
											'id' => 1,
											'user_id' => 1,
											'subscription' => 'enterprise',
											'submission_date' => '2020-06-25 00:00:01',
											'expire_date' => '2020-08-25 00:00:01'
                                        ]);
        DB::table('user_subscriptions')->insert(
                                        [
                                            'id' => 2,
                                            'user_id' => 6,
                                            'subscription' => 'trial',
                                            'submission_date' => '2020-06-25 00:00:01',
                                            'expire_date' => '2020-08-25 00:00:01'
                                        ]);
                                   

    }
}
