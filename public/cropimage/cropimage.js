var $modal = $('#modal-preview');
var image = document.getElementById('modal-image-preview');
var cropper;

preview_image = (e) => {

    console.log("preview_image");
    console.log("image", image);

    var files = e.target.files;
    var done = function (url) {
        image.src = url;
        $modal.modal('show');
    };
    var reader;
    var file;
    var url;

    if (files && files.length > 0) {
        file = files[0];

        $("#output_image_type").val(file.type);
        $("#output_image_name").val(file.name);

        if (URL) {
            done(URL.createObjectURL(file));
        } else if (FileReader) {
            reader = new FileReader();
            reader.onload = function (e) {
                done(reader.result);
            };
            reader.readAsDataURL(file);
        }
    }
};

$modal.on('shown.bs.modal', function () {

    let aspectRatio = 2;

    if( $('body').hasClass('shop-catalogs') ||
        $('body').hasClass('foods')){
        aspectRatio = 1.5;
    }

    cropper = new Cropper(image, {
        aspectRatio: aspectRatio,
        viewMode: 1,
        zoomable: false,
        autoCropArea: 1,
        preview: '.preview'
    });
}).on('hidden.bs.modal', function () {
    cropper.destroy();
    cropper = null;
});

$("#crop").on("click", function(){

    let html_crop = $("#crop").html();
    $("#crop").html(
        `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Caricamento in corso...`
    );

    let width = 640;
    let height = 320;

    if( $('body').hasClass('shop-catalogs') ||
        $('body').hasClass('foods')){
        width = 646;
        height = 430;
    }

    canvas = cropper.getCroppedCanvas({
        width: width,
        height: height,
    });

    canvas.toBlob(function(blob) {
        url = URL.createObjectURL(blob);
        var reader = new FileReader();
        reader.readAsDataURL(blob);
        reader.onloadend = function() {
            var base64data = reader.result;
            document.getElementById('output_image')
                .setAttribute(
                    'src', base64data
                );
            $("#crop").html(html_crop);
            $("#output_image_data").val(base64data);
            $modal.modal('hide');
        }
    });
})
