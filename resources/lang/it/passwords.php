<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'La tua password è stata modificata!',
    'sent' => 'Ti abbiamo inviato una mail con un link per effettuare il cambio password!',
    'throttled' => 'Troppi tentativi. Attendere...',
    'token' => 'Token non valido.',
    'user' => "Non esiste un utente con l'indirizzo email indicato.",

];
