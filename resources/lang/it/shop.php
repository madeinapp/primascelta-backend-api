<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for most common translation Primascelta
    | messages that we need to display. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

	'shop' => 'negozio',
	'Altra attività' => 'Altra attività',
	'Bar' => 'Bar',
	'Camping' => 'Camping',
	'Forno / Panificio' => 'Forno / Panificio',
	'Frutta e Verdura' => 'Frutta e Verdura',
	'Gelateria' => 'Gelateria',
	'Macelleria' => 'Macelleria',
	'Mini Market' => 'Mini Market',
	'Pasta Fresca' => 'Pasta Fresca',
	'Pasticceria' => 'Pasticceria',
	'Pescheria' => 'Pescheria',
	'Pizza al taglio' => 'Pizza al taglio',
	'Pizzeria' => 'Pizzeria',
	'Prodotti bio' => 'Prodotti bio',
	'Ristorante' => 'Ristorante',
	'Tavola calda' => 'Tavola calda',
	'Stabilimento' => 'Stabilimento',
	'Villaggio' => 'Villaggio',

];
