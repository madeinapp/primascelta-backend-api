<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for most common translation Primascelta
    | messages that we need to display. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

	'user' => 'utente',


];
