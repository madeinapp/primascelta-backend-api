@extends('adminlte::page')

@section('content')
    <section class="content-header">
        <h1>
		   {{  __('common.Agents') }}
        </h1>
    </section>
    <div class="content">        
        <div class="card card-primary">
            <div class="card-body">
                <div class="card-body">
                    {!! Form::open(['route' => 'agents.store']) !!}

                        @include('agents.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
