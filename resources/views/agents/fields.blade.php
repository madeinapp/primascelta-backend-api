<!-- Name Field -->
<div class="form-group ">
    {!! Form::label('name', __('common.name')) !!}
    {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
    @error('name')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
</div>

<!-- Phone Field -->
<div class="form-group ">
    {!! Form::label('phone', __('common.phone')) !!}
    {!! Form::text('phone', null, ['class' => 'form-control', 'required' => 'required']) !!}
    @error('phone')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
</div>

<!-- Coupon Field -->
<div class="form-group ">
    {!! Form::label('coupon', 'Coupon') !!}
    {!! Form::text('coupon', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group ">
    {!! Form::label('address', __('common.address')) !!}
    {!! Form::text('address', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group ">
    {!! Form::label('commissions', __('common.commissions')) !!}
    {!! Form::text('commissions', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group ">
    {!! Form::label('note', __('common.note')) !!}
    {!! Form::textarea('note', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('common.save'), ['class' => 'btn btn-primary btn-full']) !!}
    <a href="{{ route('agents.index') }}" class="btn btn-default btn-full">{{ __('common.cancel') }}</a>
</div>
