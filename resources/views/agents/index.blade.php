@extends('adminlte::page')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">{{  __('common.Agents') }}</h1>
        <div class="pull-right  float-right d-none d-sm-block" >
           <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{{ route('agents.create') }}">{{ __('common.Add New') }}</a>
        </div>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="card card-primary">
            <div class="card-body">
                    @include('agents.table')
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
@endsection

