<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', __('common.name').':') !!}
    <p>{{ $agent->name }}</p>
</div>

<!-- Phone Field -->
<div class="form-group">
    {!! Form::label('phone', __('common.phone').':') !!}
    <p>{{ $agent->phone }}</p>
</div>

<!-- Coupon Field -->
<div class="form-group">
    {!! Form::label('coupon', 'Coupon:') !!}
    <p>{{ $agent->coupon }}</p>
</div>

