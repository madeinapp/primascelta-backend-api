<table class="table" id="agents-table">
    <thead>
        <tr>
            <th>Azioni</th>
            <th>Nome</th>
            <th>Telefono</th>
            <th>Coupon</th>
        </tr>
    </thead>
    <tbody>
    @foreach($agents as $agent)
        <tr>
            <td>
                {!! Form::open(['route' => ['agents.destroy', $agent->id], 'method' => 'delete']) !!}

                <a href="{{ route('agents.edit', [$agent->id]) }}" class='btn btn-default btn-sm'><i class="fas fa-edit"></i></a>
                {!! Form::button('<i class="fas fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-sm', 'onclick' => "return confirm('".__('common.Are you sure?')."')"]) !!}

                {!! Form::close() !!}
            </td>
            <td>{{ $agent->name }}</td>
            <td>{{ $agent->phone }}</td>
            <td>{{ $agent->coupon }}</td>
        </tr>
    @endforeach
    </tbody>
</table>

@section('js')
<script>
$(function () {

    $('#agents-table').DataTable({
        "language": {
            url: "{{ url('js/Italian.json') }}"
        },
        "initComplete": function (settings, json) {
            $("#agents-table").wrap("<div style='overflow:auto; width:100%;position:relative;'></div>");
        },
        paging:   true,
        pageLength: 10,
        columnDefs: [
            { width: "10%", orderable: false, targets: 0 }
        ]
    });

});

</script>
@endsection
