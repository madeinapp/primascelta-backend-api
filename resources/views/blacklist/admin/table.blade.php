<div class="card-header mb-4 p-0">
    <div class="row m-0 p-4 bg-success rounded">
        <div class="col-sm-12 col-md-4">
            <label>Cerca utente</label>
            <input type="text" id="user_filter" name="user_filter" class="form-control" value="{{ old('user_filter') }}">
        </div>
        <div class="col-sm-12 col-md-4">
            <label>Cerca attività</label>
            <input type="text" id="shop_filter" name="shop_filter" class="form-control" value="{{ old('shop_filter') }}">
        </div>
    </div>
</div>
<table class="table table-hover blacklist-table" id="blacklist-company-table">
    <thead>
        <tr>
            <th>Azioni</th>
            <th>Utente</th>
            <th>Attività</th>
        </tr>
    </thead>
</table>

@section('js')
<script>

var table = null;

$(document).ready( function(){

    table = $('#blacklist-company-table').DataTable({
        language: {
            url: "{{ url('js/Italian.json') }}"
        },
        dom: "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'p>r>"+
            "<'row'<'col-sm-12't>>"+
            "<'row'<'col-sm-12 col-md-6'i><'col-sm-12 col-md-6'p>>",
        processing: true,
        serverSide: true,
        stateSave: true,
        paginate: true,
        pageLength: 20,
        ajax: {
            url: `{{ route('blacklist.index') }}`,
            data: function (d) {
                d.user_filter = $('#user_filter').val(),
                d.shop_filter = $('#shop_filter').val()
            }
        },
        "initComplete": function (settings, json) {
            $("#blacklist-company-table").wrap("<div style='overflow:auto; width:100%;position:relative;'></div>");
        },
        columns: [
            {data: 'action', name: 'action', orderable: false, "searchable": false},
            {data: 'user_name', name: 'user_name'},
            {data: 'shop_name', name: 'shop_name'},
        ]
    });

    $("#user_filter").on('keyup', function(){
        table.draw();
    });

    $("#shop_filter").on('keyup', function(){
        table.draw();
    });

});

function remove(shop_id, user_id){
    showConfirm("Blacklist", 'bg-danger', "Rimuovere l'utente selezionato dalla blacklist dell'attività?", execRemove, [shop_id, user_id]);
}

function execRemove(shop_id, user_id){
    $.ajax({
        url: `/blacklist/remove/${shop_id}/${user_id}`,
        data: {
            "_token": "{{ csrf_token() }}"
        },
        type: 'post',
        success: function(data){
            table.draw();
        }
    });
}
</script>
@endsection
