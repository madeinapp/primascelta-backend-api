@extends('adminlte::page')

@section('content')
    <section class="content-header">
        <div class="row">
            <div class="col-md-9">
                <h1 class="pull-left">Utenti in blacklist</h1>
                <small>Da questa pagina puoi rimuovere gli utenti dalla tua blacklist</small>
            </div>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="card card-primary">
            <div class="card-body">
                @include('blacklist.company.table')
            </div>
        </div>
        <div class="text-center">

        {{-- @include('adminlte-templates::common.paginate', ['records' => $orders])  --}}

        </div>
    </div>
@endsection

