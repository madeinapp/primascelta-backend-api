<div class="card-header mb-4 p-0">
    <div class="row m-0 p-4 bg-success rounded">
        <div class="col-sm-12 col-md-4">
            <label>Cerca utente</label>
            <input type="text" id="user_filter" name="user_filter" class="form-control" value="{{ old('user_filter') }}">
        </div>
        <div class="col-sm-12 col-md-4">
            <label style="display: block">&nbsp;</label>
            <button class="btn btn-light">Cerca</button>
        </div>
    </div>
</div>
<table class="table table-hover blacklist-table" id="blacklist-company-table">
    <thead>
        <tr>
            <th>Azioni</th>
            <th>Utente</th>
        </tr>
    </thead>
</table>

@section('js')
<script>

var table = null;

$(document).ready( function(){

    table = $('#blacklist-company-table').DataTable({
        language: {
            url: "{{ url('js/Italian.json') }}"
        },
        dom: "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'p>r>"+
            "<'row'<'col-sm-12't>>"+
            "<'row'<'col-sm-12 col-md-6'i><'col-sm-12 col-md-6'p>>",
        processing: true,
        serverSide: true,
        paginate: true,
        stateSave: true,
        pageLength: 20,
        ajax: {
            url: `{{ route('blacklist.index') }}`,
            data: function (d) {
                d.user_filter = $('#user_filter').val()
            }
        },
        "initComplete": function (settings, json) {
            $("#blacklist-company-table").wrap("<div style='overflow:auto; width:100%;position:relative;'></div>");
        },
        columns: [
            {data: 'action', name: 'action', orderable: false, "searchable": false},
            {data: 'user_name', name: 'user_name'},
        ]
    });

    $("#user_filter").on('keyup', function(){
        table.draw();
    });
});

function remove(shop_id, user_id){
    showConfirm("Blacklist", 'bg-danger', "Vuoi davvero rimuovere l'utente selezionato dalla tua blacklist?", execRemove, [shop_id, user_id]);
}

function execRemove(shop_id, user_id){
    $.ajax({
        url: `/blacklist/remove/${shop_id}/${user_id}`,
        type: 'post',
        data: {
            "_token": "{{ csrf_token() }}"
        },
        success: function(data){
            table.draw();
        }
    });
}
</script>
@endsection
