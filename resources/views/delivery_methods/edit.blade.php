@extends('adminlte::page')

@section('content')
    <section class="content-header">
        <h1>
            Delivery Method
        </h1>
   </section>
   <div class="content">
       {{-- @include('adminlte-templates::common.errors') --}}
       <div class="card card-primary">
           <div class="card-body">
               <div class="card-body">
                   {!! Form::model($deliveryMethod, ['route' => ['deliveryMethods.update', $deliveryMethod->id], 'method' => 'patch']) !!}

                        @include('delivery_methods.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection
