<!-- Name Field -->
<div class="form-group ">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
    @error('name')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
</div>

<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Salva', ['class' => 'btn btn-primary btn-full']) !!}
    <a href="{{ route('deliveryMethods.index') }}" class="btn btn-default btn-full">Annulla</a>
</div>
