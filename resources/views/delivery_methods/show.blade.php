@extends('adminlte::page')

@section('content')
    <section class="content-header">
        <h1>
            {{__('common.Delivery Method') }}
        </h1>
    </section>
    <div class="content">
        <div class="card card-primary">
            <div class="card-body">
                <div class="row" style="padding-left: 20px">
                    @include('delivery_methods.show_fields')
                    <a href="{{ route('deliveryMethods.index') }}" class="btn btn-default">{{ __('common.Back') }}</a>
                </div>
            </div>
        </div>
    </div>
@endsection
