<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', __('common.name').':') !!}
    <p>{{ __('common.'.$deliveryMethod->name) }}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', __('common.description').':') !!}
    <p>{{ $deliveryMethod->description }}</p>
</div>

