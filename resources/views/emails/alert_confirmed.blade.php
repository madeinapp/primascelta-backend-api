@component('mail::message')
Ciao <strong><i>{{ $user->name }}</i></strong>,<br>
l'ordine #{{ $order->id }} non è stato ancora evaso.
@if( $order->pickup_on_site == 1 )
@if( !empty($order->pickup_on_site_date) )
<p>La data/ora di consegna richiesta per il <strong>ritiro in sede</strong> era il {!! $order->pickup_on_site_date->format('d/m/Y') !!} ma l'ordine ancora non è stato evaso.</p>
@endif
@elseif($order->home_delivery == 1)
@if( !empty($order->home_delivery_date) )
<p>La data/ora di consegna richiesta per la <strong>consegna a domicilio</strong> era il {!! $order->home_delivery_date->format('d/m/Y') !!} ma l'ordine ancora non è stato evaso.</p>
@endif
@elseif($order->delivery_host_on_site == 1)
@if(!empty($order->delivery_host_on_site_date))
<p>La data/ora di consegna richiesta per la <strong>consegna sul posto</strong> era il {!! $order->delivery_host_on_site_date->format('d/m/Y') !!} ma l'ordine ancora non è stato evaso.</p>
@endif
@endif
<br>
<p style="text-align: center">
@component('mail::button', ['url' => url('/orders/'.$order->id.'/edit'), 'color' => 'success'])
Vai all'ordine
@endcomponent
</p>
@endcomponent
