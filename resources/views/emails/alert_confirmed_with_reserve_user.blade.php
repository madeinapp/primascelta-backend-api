@component('mail::message')
Ciao <strong><i>{{ $user->name }}</i></strong>,<br>
l'ordine #{{ $order->id }} è stato cancellato automaticamente.
@if( !empty($order->confirm_reserve_within) )
<p>Il tempo limite per la conferma della richiesta era il {!! $order->confirm_reserve_within->format('d/m/Y') !!} alle {!! $order->confirm_reserve_within->format('H:i') !!}</p>
<br>
@endif
<p>Ti sono stati scalati 3 punti fiducia</p>
<br>
<p style="text-align: center">
@component('mail::button', ['url' => url('/orders/'.$order->id.'/edit'), 'color' => 'success'])
Vai all'ordine
@endcomponent
</p>
@endcomponent
