@component('mail::message')
Ciao <strong><i>{{ $user->name }}</i></strong>,<br>
l'ordine #{{ $order->id }} non è stato ancora confermato.
@if( $order->pickup_on_site == 1 )
@if(!empty($order->pickup_on_site_date))
<p>La data/ora di consegna richiesta per il <strong>ritiro in sede</strong> è il {!! $order->pickup_on_site_date->format('d/m/Y') !!}</p>
@endif
@elseif($order->home_delivery == 1)
@if(!empty($order->home_delivery_date))
<p>La data/ora di consegna richiesta per la <strong>consegna a domicilio</strong> è il {!! $order->home_delivery_date->format('d/m/Y') !!}</p>
@endif
@elseif($order->delivery_host_on_site == 1)
@if(!empty($order->delivery_host_on_site_date))
<p>La data/ora di consegna richiesta per la <strong>consegna sul posto</strong> è il {!! $order->delivery_host_on_site_date->format('d/m/Y') !!}</p>
@endif
@endif
<br>
<p style="text-align: center">
@component('mail::button', ['url' => url('/orders/'.$order->id.'/edit'), 'color' => 'success'])
Vai all'ordine
@endcomponent
</p>
@endcomponent
