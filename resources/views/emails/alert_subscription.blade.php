@component('mail::message')
Ciao <strong><i>{{ $user->name }}</i></strong>,<br>
il tuo abbonamento scade tra {{ $days }} giorni.
<br>
<p style="text-align: center">
@component('mail::button', ['url' => url('/prices'), 'color' => 'success'])
Clicca qui per rinnovarlo!
@endcomponent
</p>
@endcomponent
