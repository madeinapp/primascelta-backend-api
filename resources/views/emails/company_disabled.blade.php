@component('mail::message')
Ciao <strong><i>{{ $user->name }}</i></strong>,<br>
ci spiace comunicarti che questo account è stato cancellato per una delle seguenti ragioni:
<ul>
<li>ragioni di sicurezza (accessi sospetti o spam)</li>
<li>comportamento non conforme al regolamento della comunity di Primascelta</li>
</ul>
@endcomponent
