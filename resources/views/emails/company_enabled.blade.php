@component('mail::message')
Ciao <strong><i>{{ $user->name }}</i></strong>,<br>
la verifica del tuo account è andata a buon fine!
@component('mail::panel')
Il tuo username è:
<strong>{{ $user->email }}</strong>
@endcomponent
<p>
Da ora il tuo {{ $user->shop->name }} è pubblicato su Primascelta e tutti i tuoi prodotti risultano acquistabili.
<br>Ricorda che puoi modificare e arricchire il tuo catalogo prodotti in qualunque momento.
<br><strong>Consiglio: non appena ti senti pronto invita i tuoi clienti a scaricare l'app Primascelta utilizzando il tasto INVITA I TUOI CLIENTI.</strong>
</p>
<p>
Utilizzare Primascelta è davvero facile, tuttavia per utilizzarlo al massimo delle sue potenzialità  ti invitiamo a vedere <a href="https://vimeopro.com/alessandroterracciano/tutorial-primascelta">i nostri tutorial</a>.
</p>
<br>
@if( $user->subscription->subscription == 'trial')
<p style="text-align: center">
@if(!empty($user->subscription->expire_date))
Il tuo periodo di prova gratuita terminerà il {{ $user->subscription->expire_date->format('d/m/Y') }}
@endif
</p>
@else
<p style="text-align: center">
@if(!empty($user->subscription->expire_date))
Il tuo abbonamento {{ $user->subscription->subscription }} terminerà il {{ $user->subscription->expire_date->format('d/m/Y') }}
@endif
</p>
@endif
<br>
<p style="text-align: center">
<a href="https://backend.primascelta.biz/prices">Prolunga subito un abbonamento</a>
</p>
@endcomponent
