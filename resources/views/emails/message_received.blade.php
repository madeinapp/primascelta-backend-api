@component('mail::message')
Ciao <strong><i>{{ $user->name }}</i></strong>,<br>
hai ricevuto un messaggio da @if( $from ) {{ $order->shop->name }} @else {{ $order->user->name }} @endif relativo all'ordine <strong>#{{ $order->id }}</strong>:
@component('mail::panel')
<i>{{ $message }}</i>
@endcomponent
<br>
@component('mail::button', ['url' => url('/orders/'.$order->id.'/edit'), 'color' => 'success'])
Vai all'ordine
@endcomponent
@endcomponent
