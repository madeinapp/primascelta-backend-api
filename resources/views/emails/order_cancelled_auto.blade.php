@component('mail::message')
Ciao <strong><i>{{ $user->name }}</i></strong>,<br>
L'ordine #{{ $order->id }} è stato cancellato automaticamente per scadenza dei tempi.
@endcomponent
