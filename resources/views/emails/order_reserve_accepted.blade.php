@component('mail::message')
Ciao <strong><i>{{ $user->name }}</i></strong>,<br>
il cliente {{ $order->user->name }} ha accettato le tua modifiche all'ordine #{{ $order->id }}.
@endcomponent
