@component('mail::message')
Ciao <strong><i>{{ $user->name }}</i></strong>,<br>
hai inviato correttamente il tuo ordine numero #{{ $order->id }}.
<br>
<p>
<strong>Riepilogo ordine</strong>
<ul>
<li>
Tipo di spedizione:
@if( $order->pickup_on_site == 1 )
Ritiro in sede
@elseif( $order->home_delivery == 1 )
Consegna a domicilio
@elseif( $order->delivery_host_on_site == 1 )
Consegna sul posto
@endif
</li>
<li>
@if( $order->pickup_on_site == 1 )
Data e ora del ritiro:
{{ $order->pickup_on_site_date->format('d/m/Y') }}
@elseif( $order->home_delivery == 1 )
Data e ora della spedizione:
{{ $order->home_delivery_date->format('d/m/Y') }}
@elseif( $order->delivery_host_on_site == 1 )
Data e ora in cui vi verrà consegnato:
{{ $order->delivery_host_on_site_date->format('d/m/Y') }}
@endif
</li>
@if( $order->home_delivery == 1 )
<li>Indirizzo di spedizione:<br>
{{ $order->home_delivery_route }},
{{ $order->home_delivery_street_number }}<br>
{{ $order->home_delivery_postal_code }} - {{ $order->home_delivery_city }}<br>
{{ $order->home_delivery_region }}, {{ $order->home_delivery_country }}
</li>
@endif
<li>Totale prodotti: {{ $order->orderFoods->count() }}</li>
@if( $order->home_delivery == 1 )
<li>Spese di spedizione: {{ number_format($order->shipping_price, 2, ',', '.') }} &euro;</li>
@endif
<li>TOTALE: {{ number_format($order->total_price, 2, ',', '.') }} &euro;</li>
</ul>
</p>
@endcomponent
