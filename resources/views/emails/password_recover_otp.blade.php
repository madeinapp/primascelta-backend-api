@component('mail::message')
Ecco il codice  da inserire nella pagina al link sottostante per completare la procedura di cambio password
@component('mail::panel')
<strong>{{ $otpCode }}</strong>
<a href="https://www.primascelta.biz/cambio-password-app?otpcode={{ $otpCode }}&email={{ $email }}">Cambia password</a>
@endcomponent
@endcomponent
