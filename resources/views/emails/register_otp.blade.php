@component('mail::message')
Ciao <strong><i>{{ $user->name }}</i></strong>,<br>
<p>
Ecco il codice di attivazione da inserire nella nostra applicazione per completare la registrazione.
</p>
@component('mail::panel')
<strong>{{ $otpCode }}</strong>
@endcomponent
<br>
<br>
<p>
<strong>ATTENZIONE: </strong><br/>
<small><i>Questo messaggio è generato automaticamente, La preghiamo di non rispondere a questa email.</i></small>
</p>
@endcomponent
