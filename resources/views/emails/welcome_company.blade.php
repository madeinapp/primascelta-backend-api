@component('mail::message')
@php
$user_subscription = $user->subscription->subscription;
if(($user_subscription == 'free') && !empty($user->shop->agent_id)){
	$user_subscription = 'standard';
}
$num_product = !empty($user->subscription->subscription_product_num) ? $user->subscription->subscription_product_num : DB::table('subscriptions')->where('subscription', $user_subscription)->value('products_num');
@endphp
Ciao <strong><i>{{ $user->name }}</i></strong>,<br/>
grazie per esserti unito a noi: ti diamo il benvenuto!
@component('mail::panel')
Il tuo username è:
<strong>{{ $user->email }}</strong>
@endcomponent
<p>
Per garantire a pieno una esperienza appagante tua e dei tuoi clienti la nostra redazione avrà il piacere
di <strong>verificare questo account</strong> contattandoti personalmente a breve prima di renderlo pubblico.
</p>
<p>
Il tuo account Primascelta, se pure non ancora visible dagli utenti, è attivo a tutti gli effetti
In attesa che sia verificato tinvitiamo a:
<ul>
<li>Completare eventuali sezioni relative alla tua attività</li>
<li>Creare il tuo catalogo prodotti: più ne metterai, maggiori saranno i clienti che potrai accontentare</li>
</ul>
</p>
@if($user->subscription->subscription == 'free')
<p align="center">
	@if(!empty($user->shop->agenti_id))
Il tuo <strong>Abbonamento Standard </strong><u>&egrave; in prova gratuita fino al {{ $user->subscription->expire_date->format('d/m/Y') }}, non prevede commissioni sui tuoi ordini</u>.
<p>Puoi gestire un catalogo fino ad un massimo di {{ $num_product }} prodotti.</p><br>
Al termine della prova potrai decidere se sottoscrivere un abbonamento a pagamento oppure passare all'abbonamento Basic totalmente gratuito e senza commissioni.<br>
<div align="center"><a href="https://www.primascelta.biz/prezzi/">Vedi tutti i piani di abbonamento</a></div>
	@else
Il tuo <strong>Abbonamento Basic </strong>&egrave; <u>gratuito</u>, <u>non prevede commissioni sui tuoi ordini </u>e<u> non scade.</u> Ti consente la gestione di un catalogo fino ad un massimo di <strong>{{ $num_product }} prodotti.</strong>
<p>Puoi decidere di arricchire il tuo catalogo di molti altri prodotti e funzionalit&agrave; aggiuntive in qualunque momento!.</p>
<div align="center"><a href="https://www.primascelta.biz/prezzi/">Vedi tutti i piani di abbonamento</a></div>
	@endif
</p>
@else
<br>
<p style="text-align: center">
Il tuo periodo di <strong>prova gratuita terminerà</strong> il {{ $user->subscription->expire_date->format('d/m/Y') }}
</p>
<br>
<p style="text-align: center">
<a href="http://tutorialprimascelta.vivivideo.it/">Prolunga subito un abbonamento</a>
</p>
@endif
<br>
<p>
P.S.) Utilizzare Primascelta è davvero facile, tuttavia per utilizzarlo al massimo delle sue potenzialità  ti invitiamo a vedere <a href="https://vimeopro.com/alessandroterracciano/tutorial-primascelta">i nostri tutorial</a>.
</p>
@endcomponent
