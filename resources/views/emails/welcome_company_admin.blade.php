@component('mail::message')
@php
$user_subscription = $user->subscription->subscription;
if(($user_subscription == 'free') && !empty($user->shop->agent_id)){
	$user_subscription = 'standard';
}
$num_product = !empty($user->subscription->subscription_product_num) ? $user->subscription->subscription_product_num : DB::table('subscriptions')->where('subscription', $user_subscription)->value('products_num');
@endphp
Benvenuto <strong><i>{{ $user->name }}</i></strong>,<br/>
grazie per esserti unito alla famiglia dei nostri esercenti <strong>Primascelta</strong>.
Siamo gli unici a prevedere un abbonamento basic gratuito e senza alcuna commissione sulle tue vendite!
<br>
La nostra redazione, convinti di farti una cosa gradita, ha creato
un tuo primo catalogo prodotti provvisorio e ha pubblicato la tua 
attivit&agrave; che &egrave; quindi raggiungibile dai tuoi clienti (ovviamente
puoi renderla invisibile o cancellarti in qualunque momento).
<br>
Per iniziare a ricevere ordini ed utilizzare al meglio <strong>Primascelta</strong>
ti invitiamo a:
<ol>
 <li><a href="https://www.primascelta.biz/scaricaprimascelta">Scaricare <strong>Primascelta</strong></a> dagli store e installarlo su smartphone o tabled che avrai sempre disponibile in negozio;
 <li>Accedere come esercente con i tuoi dati;
	@component('mail::panel')
	Il tuo username &egrave;: <strong>{{ $user->email }}</strong><br>
	La tua password &egrave;: <strong>{{ $shop->admin_first_password_generated }}</strong>
	@endcomponent
 </li>
 <li>Controllare il catalogo prodotti online e modificarlo a tuo piacimento;</li>
 <li>Arricchire il catalogo con altri prodotti;</li>
 <li>Controllare e completare eventuali info ziendali nel tuo Account;</li>
 <li>Contattare la nostra redazione per effettuare 
     un "ordine demo" ed essere pronto a ricevere gli ordini veri!
 </li>
</ol>
<p align="center">
Utilizzare <strong>Primascelta</strong> è davvero facile, tuttavia per utilizzarla al massimo delle sue potenzialità  ti invitiamo a vedere <a href="https://vimeopro.com/alessandroterracciano/tutorial-primascelta">i nostri tutorial</a>.
</p>
<p style="text-align: center">
@if(empty($shop->agent_id))
Il tuo <strong>abbonamento basic</strong> ti consente la gestione di una catalogo fino a <strong>{{ $num_product }} prodotti.<u>E' gratuito e non scade</u></strong>
@else
Il tuo <strong>abbonamento Standard</strong> ti consente la gestione di una catalogo fino a <strong>{{ $num_product }} prodotti.
E' in prova gratuita fino al {{ $user->subscription->expire_date}}</strong>. A scadenza deciderai tu se fare l'abbonamento o passare all'abbonamento <strong>Basic</strong> completamente <strong>gratuito</strong>
(con il quale puoi gestire 15 prodotti)
@endif
</p>
<br>
<p style="text-align: center">
<a href="https://www.primascelta.biz/prezzi">Vedi tutti gli abbonamenti disponibili</a>
</p>
@endcomponent
