@component('mail::message')
Ciao <strong><i>{{ $user->name }}</i></strong>,<br>
ti diamo il benvenuto: Il tuo account è attivo!
@component('mail::panel')
Il tuo username è:
<strong>{{ $user->email }}</strong>
@endcomponent
<p>
Puoi iniziare ad utilizzare Primascelta per la prenotazione e l'acquisto di alimenti di prima qualità.
</p>
<p>
Primascelta è un app diversa da tutte le altre, infatti non prevede transazioni on line. Prenoterai tutti i tuoi prodotti e li pagherai al momento del ritiro in sede o alla consegna a domicilio.
Rispettare sempre gli impegni presi con il tuo esercente risulta quindi fondamentale a consolidare un rapporto di stima e fiducia reciproca.
</p>
<p>
Cerca i tuoi fornitori di fiducia su Primascelta ed effettua i tuoi ordini.
</p>
<p style="text-align: center">
Utilizzare Primascelta è davvero facile, tuttavia per utilizzarlo al massimo delle sue potenzialità  ti invitiamo a vedere <a href="https://vimeopro.com/alessandroterracciano/tutorial-privati">i nostri tutorial</a>.
</p>
@endcomponent
