@extends('adminlte::page')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">{{ $foods[0]->foodCategory->name }}</h1>
    </section>
    <div class="content">
        <div class="container">
            @foreach($foods as $food)
            <div class="row">
                <div class="col-12">
                    <div class="card card-primary">
                        <div class="card-header">
                            <a href="{{ route('foods.edit', [$food->id]) }}" target="_blank">{{ $food->name }}</a>
                        </div>
                        <div class="card-body" style="background-color: #ccc">
                            <div class="row">
                                <div class="col-lg-12">
                                    100x100<br/>
                                    <img class="m-1" src="{{ getResizedImage(url('storage/images/foods/'.NormalizeString($food->foodCategory->name).'/'.$food->image), '100x100') }}">
                                    <br/>
                                    @php
                                    $res = null;
                                    $img = public_path('/storage/images/foods/'.NormalizeString($food->foodCategory->name).'/'.getResizedImage($food->image, '100x100'));
                                    $res = @getimagesize($img);
                                    @endphp

                                    @if( !$res || $res[0] !== 100 || $res[1] !== 100 )
                                    <span class="text-danger">Errore</span>
                                    @endif

                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    230x154<br/>
                                    <img class="m-1" src="{{ getResizedImage(url('storage/images/foods/'.NormalizeString($food->foodCategory->name).'/'.$food->image), '230x154') }}">
                                    <br/>
                                    @php
                                    $res = null;
                                    $img = public_path('/storage/images/foods/'.NormalizeString($food->foodCategory->name).'/'.getResizedImage($food->image, '230x154'));
                                    $res = @getimagesize($img);
                                    @endphp

                                    @if( !$res || $res[0] !== 230 || $res[1] !== 154 )
                                    <span class="text-danger">Errore</span>
                                    @endif
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    512x340<br/>
                                    <img class="m-1" src="{{ getResizedImage(url('storage/images/foods/'.NormalizeString($food->foodCategory->name).'/'.$food->image), '512x340') }}">
                                    <br/>
                                    @php
                                    $res = null;
                                    $img = public_path('/storage/images/foods/'.NormalizeString($food->foodCategory->name).'/'.getResizedImage($food->image, '512x340'));
                                    $res = @getimagesize($img);
                                    @endphp

                                    @if( !$res || $res[0] !== 512 || $res[1] !== 340 )
                                    <span class="text-danger">Errore</span>
                                    @endif
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    646x430<br/>
                                    <img class="m-1" src="{{ getResizedImage(url('storage/images/foods/'.NormalizeString($food->foodCategory->name).'/'.$food->image), '646x430') }}">
                                    <br/>
                                    @php
                                    $res = null;
                                    $img = public_path('/storage/images/foods/'.NormalizeString($food->foodCategory->name).'/'.getResizedImage($food->image, '646x430'));
                                    $res = @getimagesize($img);
                                    @endphp
                                    @if( !$res || $res[0] !== 646 || $res[1] !== 430 )
                                    <span class="text-danger">Errore</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
@endsection
