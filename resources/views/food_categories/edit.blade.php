@extends('adminlte::page')

@section('content')
    <section class="content-header">
        <h1>
			Genere alimentare
        </h1>
   </section>
   <div class="content">
       <div class="card card-primary">
           <div class="card-body">
               <div class="card-body">
                   {!! Form::model($foodCategory, ['route' => ['foodCategories.update', $foodCategory->id], 'method' => 'patch', 'files' => true]) !!}

                        @include('food_categories.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection

@section('js')
<script>

    function preview_background(event)
    {
        var reader = new FileReader();
        reader.onload = function()
        {
        var output = document.getElementById('output_background');
        output.src = reader.result;
        }
        reader.readAsDataURL(event.target.files[0]);
    }
</script>
@endsection
