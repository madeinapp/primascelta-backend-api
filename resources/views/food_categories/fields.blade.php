<!-- Name Field -->
<div class="row">
    <div class="form-group col-md-12">
        {!! Form::label('name', __('common.name')) !!}
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
        @error('name')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>

    <!-- Image Field -->
    <div class="form-group col-md-6">
        {!! Form::label('filelogo', __('common.logo') . ' (Immagine .png, Altezza: 80px, Larghezza: auto)') !!}
        <div style="clear: both"></div>
        {!! Form::file('filelogo', [ 'accept' => 'image/png', 'onchange' => 'preview_logo(event)']) !!}
        <div style="clear: both"></div>
        @if( $logo_exists )
        <img id="output_logo" class="thumbnail" src="{{ asset('/storage/images/foods/'.NormalizeString($foodCategory->name).'/'.$logo) }}?p={{ time() }}" style="max-width: 200px; margin-top: 10px">
        @endif
    </div>

    <!-- Image Field -->
    <div class="form-group col-md-6">
        {!! Form::label('filebackground', __('common.background') . ' (Immagine .png, Altezza: 120px, Larghezza: 1024px)') !!}
        <div style="clear: both"></div>
        {!! Form::file('filebackground', ['accept' => 'image/png', 'onchange' => 'preview_background(event)']) !!}
        <div style="clear: both"></div>
        @if( $background_exists )
        <img id="output_background" class="thumbnail" src="{{ asset('/storage/images/foods/'.NormalizeString($foodCategory->name).'/'.$background) }}?p={{ time() }}" style="max-width: 200px; margin-top: 10px">
        @else
        <img id="output_background" style="margin-top: 10px; max-width: 200px">
        @endif
    </div>

    <div class="form-group col-md-12">
        {!! Form::label('unit_of_measure', 'Unità di misura di default') !!}
        {!! Form::select('unit_of_measure', $unitOfMeasures, $foodCategory->unit_of_measure ?? null, ['class' => 'form-control']) !!}
    </div>

    <!-- Submit Field -->
    <div class="form-group col-md-6">
        {!! Form::submit(__('common.save'), ['class' => 'btn btn-primary btn-full']) !!}
        <a href="{{ route('foodCategories.index') }}" class="btn btn-default btn-full">Annulla</a>
    </div>
</div>
