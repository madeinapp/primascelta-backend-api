@extends('adminlte::page')

@section('content')
    <div class="content">
        <div class="card card-primary">
            <div class="card-body">
                <form action="/foodCategories/{{$foodCategory->id}}/moveFoods" method="POST">
                    <h3>
                        Sposta alimenti della categoria {{ $foodCategory->name }}
                    </h3>
                    <p>
                        Questa azione sposta tutti gli alimenti Primascelta di una categoria verso un'altra categoria.<br/>
                        Se dei negozi hanno prodotti collegati a questi alimenti, le immagini dei prodotti verranno automaticamente puntate al nuovo percorso.
                        <br>
                        NB: Questa azione non modifca la categoria dei prodotti a catalogo.
                    </p>
                    <div class="form-group">
                        <label for="new_food_category_id">Nuova categoria</label>
                        {!!  Form::select('new_food_category_id', $foodCategories, null, ['id'=>'new_food_category_id', 'class' => 'form-control']) !!}
                    </div>
                    <div class="form-group">
                        @if($foodCategory->foods->count() > 0)
                            <button class="btn btn-success">Sposta {{ $foodCategory->foods->count() }} alimenti</button>
                        @else
                            <span>Nessun alimento da spostare</span>
                        @endif
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="content">
        <div class="card card-primary">
            <div class="card-body">
                <form action="/foodCategories/{{$foodCategory->id}}/moveShopCatalogs" method="POST">
                    <h3>
                        Sposta i prodotti a catalogo della categoria {{ $foodCategory->name }} dei negozi
                    </h3>
                    <p>
                        Questa azione modifica la categoria dei prodotti a catalogo.
                    </p>
                    <div class="form-group">
                        <label for="new_food_category_id">Nuova categoria</label>
                        {!!  Form::select('new_food_category_id', $foodCategories, null, ['id'=>'new_food_category_id', 'class' => 'form-control']) !!}
                    </div>
                    <div class="form-group">
                        @if($foodCategory->shopCatalogs->count() > 0)
                            <button class="btn btn-success">Sposta {{ $foodCategory->shopCatalogs->count() }} prodotti</button>
                        @else
                            <span>Nessun prodotto a catalogo da spostare</span>
                        @endif
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
