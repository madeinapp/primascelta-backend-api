@extends('adminlte::page')

@section('content')
    <section class="content-header">
        <h1>
            Food Category
        </h1>
    </section>
    <div class="content">
        <div class="card card-primary">
            <div class="card-body">
                <div class="row" style="padding-left: 20px">
                    @include('food_categories.show_fields')
                    <a href="{{ route('foodCategories.index') }}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
