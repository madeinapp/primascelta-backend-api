<table class="table" id="foodCategories-table">
    <thead>
        <tr>
            <th>{{ __('common.action') }}</th>
            <th>{{ __('common.name') }}</th>
            <th>{{ __('common.unit_of_measure') }}</th>
            <th>{{ __('common.logo') }}</th>
            <th>{{ __('common.background') }}</th>
            <th>Alimenti</th>
            <th>Catalogo</th>
        </tr>
    </thead>
    <tbody>
    @foreach($foodCategories as $foodCategory)
        <tr>
            <td>
                {!! Form::open(['route' => ['foodCategories.destroy', $foodCategory->id], 'method' => 'delete']) !!}
                    <a href="{{ route('foodCategories.edit', [$foodCategory->id]) }}" class='btn btn-default btn-sm'><i class="fas fa-edit"></i></a>
                    {!! Form::button('<i class="fas fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-sm', 'onclick' => "return confirm('".__('common.AreYouSureDeleteFoodCategory')."')"]) !!}

                    <a href="/foodCategories/{{ $foodCategory->id }}/check/images" class="btn btn-sm btn-warning"><i class="fa fa-image"></i></a>
                    <a href="/foodCategories/{{ $foodCategory->id }}/move" class="btn btn-sm btn-success"><i class="fas fa-exchange-alt"></i></a>
                {!! Form::close() !!}
            </td>
            <td>{{ $foodCategory->name }}</td>
            <td>{{ $foodCategory->unitOfMeasure->name ?? '' }}</td>
            <td>
                @if( \Illuminate\Support\Facades\File::exists( public_path('storage/images/foods/'.NormalizeString($foodCategory->name).'/_logo.png') ) )
                <img class="thumbnail" src="{{ asset('storage/images/foods/'.NormalizeString($foodCategory->name).'/\_logo.png') }}?p={{ time() }}" style="max-width: 100px">
                @endif
            </td>
            <td>
                @if( \Illuminate\Support\Facades\File::exists( public_path('storage/images/foods/'.NormalizeString($foodCategory->name).'/_background.png') ) )
                <img class="thumbnail" src="{{ asset('storage/images/foods/'.NormalizeString($foodCategory->name).'/\_background.png') }}?p={{ time() }}" style="max-width: 100px" title="Immagini dei prodotti di questa categoria">
                @endif
            </td>
            <td>{{ $foodCategory->foods->count() }}</td>
            <td>{{ $foodCategory->shopCatalogs->count() }}</td>
        </tr>
    @endforeach
    </tbody>
</table>

@section('js')
<script>
$(function () {

    var datatable = $('#foodCategories-table').DataTable({
        "language": {
            url: "{{ url('js/Italian.json') }}"
        },
        paging:   true,
        pageLength: 10,
        stateSave: true,
        columnDefs: [
            { width: "20%", orderable: false, targets: 0 },
            { width: "35%", orderable: true, targets: 1 },
            { width: "25%", orderable: true, targets: 2 },
            { width: "15%", orderable: false, targets: 3 },
            { width: "15%", orderable: false, targets: 4 }
        ],
        "initComplete": function (settings, json) {
            $("#foodCategories-table").wrap("<div style='overflow:auto; width:100%;position:relative;'></div>");
        },
    });

});
</script>
@endsection
