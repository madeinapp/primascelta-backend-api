@extends('adminlte::page')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">{{__('common.food_types') }}</h1>        
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="card card-primary">
            <div class="card-body">
                    @include('food_types.table')
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
@endsection

