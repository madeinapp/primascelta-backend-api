{!! Form::open(['route' => ['foodTypes.update'], 'method' => 'PUT']) !!}
    <ul class="nav nav-tabs nav-profile">
        <li class="nav-item active">
            <a class="nav-link active" data-toggle="tab" href="#foods"><i class="fas fa-fw fa fa-utensils"></i> Alimenti</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#shop-catalogs"><i class="fas fa-fw fa fa-apple-alt "></i> Cataloghi</a>
        </li>

    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane active" id="foods">
            <table class="table" id="foodTypes-table">
                <thead>
                    <tr>
                        <th>{{ __('common.name') }}</th>
                        <th>{{ __('common.new_name') }}</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($foodTypes as $foodType)
                    <tr>
                        <td>{{ $foodType }}</td>
                        <td>
                            {!! Form::hidden('old-food-'.str_replace(" ", "-", $foodType), $foodType) !!}
                            {!! Form::text('food-'.str_replace(" ", "-", $foodType), null, ['class' => 'form-control']) !!}
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
        <div class="tab-pane" id="shop-catalogs">
            <table class="table" id="foodTypes-table">
                <thead>
                    <tr>
                        <th>{{ __('common.name') }}</th>
                        <th>{{ __('common.new_name') }}</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($shopCatalogsFoodTypes as $foodType)
                    <tr>
                        <td>{{ $foodType }}</td>
                        <td>
                            {!! Form::hidden('old-catalog-'.str_replace(" ", "-", $foodType), $foodType) !!}
                            {!! Form::text('catalog-'.str_replace(" ", "-", $foodType), null, ['class' => 'form-control']) !!}
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>

    {{ Form::submit('Salva tipologie',  ['class' => 'btn btn-success btn-full']) }}
{!! Form::close() !!}

@section('js')
<script>
$(function () {
    $('#foodCategories-table').DataTable({
        "language": {
            url: "{{ url('js/Italian.json') }}"
        },
        paging:   true,
        pageLength: 10,
        stateSave: true,
        columnDefs: [
            { orderable: false, targets: -1 }
        ],
        "initComplete": function (settings, json) {
            $("#foodCategories-table").wrap("<div style='overflow:auto; width:100%;position:relative;'></div>");
        },
    });
});
</script>
@endsection
