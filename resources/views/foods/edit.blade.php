@extends('adminlte::page')

@section('css')
<link rel="stylesheet" type="text/css" href="/fileinput/normalize.css" />
<link rel="stylesheet" type="text/css" href="/fileinput/component.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.css"/>
<script>(function(e,t,n){var r=e.querySelectorAll("html")[0];r.className=r.className.replace(/(^|\s)no-js(\s|$)/,"$1js$2")})(document,window,0);</script>
@endsection

@section('content')
    <section class="content-header">
        <h1>Alimento</h1>
   </section>
   <div class="content">

        @include('flash::message')

       <div class="card card-primary">
           <div class="card-body">
               <div class="card-body">
                   {!! Form::model($food, ['route' => ['foods.update', $food->id], 'method' => 'patch', 'files' => true]) !!}

                        @include('foods.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
   @include('users.modal_preview')
@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.js"></script>
<script src="/cropimage/cropimage.js"></script>
<script src="/fileinput/custom-file-input.js"></script>
<script>

    let defaultUom = JSON.parse($("#defaultUom").val());

    $(document).ready(function(){
        $("#food_category_id").on('change', function(){
            setDefaultUom();
        });
    });

    function setDefaultUom(){
        let food_category_id = $("#food_category_id").val();
        $("#unit_of_measure_id").val(defaultUom[food_category_id]);
    }

    /*
    function preview_image(event)
    {
        var reader = new FileReader();
        reader.onload = function()
        {
        var output = document.getElementById('output_image');
        output.src = reader.result;
        }
        reader.readAsDataURL(event.target.files[0]);
    }
    */
</script>
@endsection
