<div class="row">

    <!-- Name Field -->
    <div class="form-group col-sm-6 @error('name') has-error @enderror">
        {!! Form::label('name', __('common.name')) !!}
        {!! Form::text('name', old('name', $food->name ?? ''), ['class' => 'form-control', 'required' => true]) !!}
        @error('name')
            <div class="alert alert-danger mt-1">{{ $message }}</div>
        @enderror
    </div>

    <!-- Description Field -->
    <div class="form-group col-sm-12 col-lg-12">
        {!! Form::label('description', __('common.description')) !!}
        {!! Form::textarea('description', old('description', $food->description ?? ''), ['class' => 'form-control']) !!}
    </div>


    <!-- Food Category Id Field -->
    <div class="form-group col-12 col-sm-12 col-md-6 col-lg-6">
        {!! Form::label('food_category_id', __('common.food_category')) !!}
        {!! Form::select('food_category_id', $arr_food_categories, old('food_category_id'), ['class' => 'form-control']) !!}
    </div>

    <!-- Type Field -->
    <div class="form-group col-12 col-sm-12 col-md-6 col-lg-6 @error('type') has-error @enderror">
        {!! Form::label('type', 'TIPOLOGIA') !!}
        <input type="text" list="type_list" name="type" value="{{ isset($food) ? $food->type : null }}" class="form-control" required/>
        <datalist id="type_list">
            @foreach($arr_food_type as $food_type)
                <option>{{ $food_type }}</option>
            @endforeach
        </datalist>
        @error('type')
            <div class="alert alert-danger mt-1">{{ $message }}</div>
        @enderror
    </div>

    <!-- Unit Of Measure Field -->
    <div class="form-group col-sm-6 col-md-6 col-lg-6 @error('unit_of_measure_id') has-error @enderror">
        {!! Form::label('unit_of_measure_id', __('common.unit_of_measure')) !!}
        {!! Form::select('unit_of_measure_id', $arr_unit_of_measures, null, ['id' => 'unit_of_measure_id', 'class' => 'form-control', 'required' => true]) !!}
        @error('unit_of_measure_id')
            <div class="alert alert-danger mt-1">{{ $message }}</div>
        @enderror
    </div>
</div>
<div class="row">
    <div class="form-group col-12 col-sm-12 col-md-6 col-lg-6">
        {!! Form::label('fileimage', __('common.image')) !!}
        <i class="fas fa-info-circle" data-toggle="tooltip" data-placement="top"
            title="Puoi modificare l'immagine del prodotto a tuo piacimento"></i>
        <div style="clear: both"></div>

        @php

        if( !empty($food->image) ){

            $foodImage = $food->image;
            if( dirname($food->image) == '.' ){
                $foodImage = url('storage/images/foods/'.NormalizeString($food->foodCategory->name).'/'.$food->image);
            }

            /* Verifico la presenza */
            $foodImage = getResizedImage(getOriginalImage($foodImage), '646x430');

            $textButton = 'Seleziona una nuova immagine';
        }else{
            $textButton = 'Seleziona immagine';
        }
        @endphp

        <div class="box">
            <p id="img-suggestion" class="text-primary">Puoi cambiare questa immagine in qualsiasi momento.<br/>File ammessi > .jpg - Max 10 mb - dimensioni minime di 646x430</p>
            <input type="file" name="fileimage" accept="image/jpeg" id="fileimage" class="js inputfile inputfile-1" data-multiple-caption="{count} immagine selezionata" />
            <label for="fileimage" class="lbl-fileimage"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span id="btn-fileimage">{{ $textButton }}</span></label>
        </div>

        <div style="clear: both"></div>

        @if( old('food_image', $foodImage ?? null) )
            <img id="output_image" src="{{ old('food_image', $foodImage ?? null) }}" style="width: 100%; margin-top: 10px;">
        @else
            <img id="output_image" src="/images/no-image.png" style="width: 100%; margin-top: 10px;" />
        @endif
        <input type="hidden" id="output_image_type" name="output_image_type" value="">
        <input type="hidden" id="output_image_name" name="output_image_name" value="">
        <input type="hidden" id="output_image_data" name="output_image_data" value="">

        @error('fileimage')
            <div class="alert alert-danger mt-1">{{ $message }}</div>
        @enderror

        {!! Form::hidden('food_image', null, ['id' => 'food_image']) !!}
    </div>
    {{--
    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
        <!-- Image Field -->
        <div class="form-group">
            {!! Form::label('fileimage', __('common.image')) !!}
            <div style="clear: both"></div>
            <span class="text-success"><p id="img-suggestion" class="text-primary">Puoi cambiare questa immagine in qualsiasi momento. L'immagine verrà ritagliata in un formato 3:2 File ammessi > .jpg - Max 10 mb - dimensioni minime di 646x430</p></span>
            <br>
            {!! Form::file('fileimage', ['accept' => $image_type, 'onchange' => 'preview_image(event)']) !!}
            <div style="clear: both"></div>

            @if( old('food_image', $food->image ?? null) )
                <img id="output_image" src="{{ asset('/storage/images/foods/'.NormalizeString($food->foodCategory->name).'/'.$food->image) }}" class="thumbnail mt-1" style="width: 100%">
            @else
                <img id="output_image" style="margin-top: 10px; width: 400px"/>
            @endif

            {!! Form::hidden('food_image', null, ['id' => 'food_image']) !!}

            @error('fileimage')
                <div class="alert alert-danger mt-1">{{ $message }}</div>
            @enderror
        </div>
    </div>
     --}}

    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
        <div class="form-group">
            {!! Form::label('food_specials[]', 'Caratteristiche speciali') !!}
            {!! Form::select('food_specials[]', $specials->pluck('name', 'id'), old('food_specials[]', isset($food) ? $food->specials()->pluck('special_id')->toArray() ?? null : null), ['class' => 'form-control', 'multiple' => true, 'style' => 'height: 600px']) !!}
            <small class="press-ctrl">Tenere premuto CTRL per selezionare più caratteristiche speciali</small>
        </div>
    </div>
</div>

<div class="row">
    <!-- Available Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('available', __('common.available')) !!}
        <label class="checkbox-inline">
            {!! Form::hidden('available', 0) !!}
            {!! Form::checkbox('available', '1', null) !!}
        </label>
    </div>

    <input type="hidden" id="defaultUom" value="{{ $defaultUom }}">

    <!-- Submit Field -->
    <div class="form-group col-sm-12">
        {!! Form::submit(__('common.save'), ['class' => 'btn btn-primary btn-full']) !!}
        <a href="{{ route('foods.index') }}" class="btn btn-default btn-full">{{ __('common.cancel') }}</a>
    </div>
</div>
