<!-- Food Category Id Field -->
<div class="form-group">
    {!! Form::label('food_category_id', 'Food Category Id:') !!}
    <p>{{ $food->food_category_id }}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{{ $food->name }}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{{ $food->description }}</p>
</div>

<!-- Type Field -->
<div class="form-group">
    {!! Form::label('type', 'Type:') !!}
    <p>{{ $food->type }}</p>
</div>

<!-- Unit Of Measure Field -->
<div class="form-group">
    {!! Form::label('unit_of_measure', 'Unit Of Measure:') !!}
    <p>{{ $food->unit_of_measure }}</p>
</div>

<!-- Image Field -->
<div class="form-group">
    {!! Form::label('image', 'Image:') !!}
    <p>{{ $food->image }}</p>
</div>

<!-- Available Field -->
<div class="form-group">
    {!! Form::label('available', 'Available:') !!}
    <p>{{ $food->available }}</p>
</div>

