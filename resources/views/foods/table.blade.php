<div class="card card-primary">
    <div class="card-header" style="background-color:#13bf9e !important;margin-bottom:20px ">
        <div class="row">
            <div class="col-sm-12 col-md-4">
                <label>Cerca per genere ({{$tot_generi}})</label>
                {!! Form::select('genere', $generi, null, ['id' => 'food_category', 'class' => 'form-control']) !!}
            </div>
            <div class="col-sm-12 col-md-4">
                <label>Cerca per tipologia (<span id="tot_tipologie">{{ $tot_tipologie }}</span>)</label>
                {!! Form::select('tipologia', $tipologie, null, ['id' => 'food_type', 'class' => 'form-control']) !!}
            </div>
            <div class="col-sm-12 col-md-4">
                <label>Cerca per parola chiave:</label>
                <input type="search" class="form-control " placeholder="cerca per parola chiave" aria-controls="agents-table" name="searchname" id="searchname">
            </div>
        </div>
    </div>
</div>

<table class="table" id="foods-table" style="width: 100% !important">
    <thead>
        <tr>
            <th style="width: 150px">{{__('common.action')}}</th>
            <th>{{__('common.name')}}</th>
            <th>{{__('common.image')}}</th>
            <th>{{__('common.description')}}</th>
            <th>{{__('common.food_category')}}</th>
            <th>UdM</th>
            <th><span title="Disponibile">Disp.</span></th>
            <th>fc</th>
        </tr>
    </thead>
</table>

@section('js')
<script>

var intervalSearch = null;
var table = null;

$(function () {

    table = $('#foods-table').DataTable({
        "language": {
            url: "{{ url('js/Italian.json') }}"
        },
        dom: "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'p>r>"+
            "<'row'<'col-sm-12't>>"+
            "<'row'<'col-sm-12 col-md-6'i><'col-sm-12 col-md-6'p>>",
        processing: true,
        serverSide: true,
        stateSave: true,
        stateSave: true,
        "initComplete": function (settings, json) {
            $("#foods-table").wrap("<div style='overflow:auto; width:100%;position:relative;'></div>");
        },
        //paginate: true,
        //pageLength: 10,
        //ajax: '{{ route('foods.index') }}',
        ajax: {
            url: '{{ route('foods.index') }}',
            data: function (d) {
                d.searchname = $('#searchname').val(),
                d.genere = $('select[name=genere]').val(),
                d.tipologia = $('select[name=tipologia]').val()
            }
        },
        columns: [
            // {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'action', name: 'action', orderable: false, searchable: false, width: "10%"},
            {data: 'name', name: 'f.name', width: 200},
            {data: 'image', name: 'f.image', orderable: false, searchable: false},
            {data: 'description', name: 'f.description'},
            {data: 'category',  name: 'category', searchable: false, orderData: 7},
            {data: 'UnitOfMeasure', name: 'u.name', searchable: false},
            {data: 'available', name: 'f.available', searchable: false},
            {data: 'foodCategory', name: 'foodCategory', searchable: false, visible: false},
        ]
    });

    $('[data-toggle="tooltip"]').tooltip();

    $("#searchname").keyup(function(){
        if( intervalSearch !== null ){
            clearInterval(intervalSearch);
        }

        intervalSearch = setInterval(function(){
            clearInterval(intervalSearch);
            search();
        }, 500, table);
    });

    $("select[name=genere]").change(function(){
        table.draw();
        loadFoodType($(this).val(), 'food_type');
    });

    $("select[name=tipologia]").change(function(){
        table.draw();
    });

});

search = () => {
    table.search( $("#searchname").val() ).draw();
}


loadFoodType = (food_category_id, element_id, sel_value = null) => {

    $('#'+element_id).empty();

    if( food_category_id != '' ){

        $.ajax({
            url: '/food/'+food_category_id+'/types',
            data: {
                "_token": "{{ csrf_token() }}",
            },
            type: 'GET',
            dataType: 'json',
            statusCode: {
                401: function() {
                    document.location.href = '/login';
                },
                419: function() {
                    document.location.href = '/login';
                }
            },
            success: function(res){
                var element = document.getElementById(element_id);
                var option = document.createElement("option");
                option.text = 'Cerca in tutte le tipologie';
                option.value = '';
                element.add(option);

                $("#tot_tipologie").text(res.count);

                for(var i=0; i<res.count; i++){
                    option = document.createElement("option");
                    option.text = res['Food Type List'][i];
                    option.value = res['Food Type List'][i];
                    element.add(option);

                    if( sel_value ){
                        if( option.text === sel_value ){
                            element.selectedIndex = (i+1);
                        }
                    }

                }
            }
        });

    }else{

        var element = document.getElementById(element_id);
        var option = document.createElement("option");
        option.text = 'Cerca in tutte le tipologie';
        option.value = '';
        element.add(option);

        $("#tot_tipologie").text(0);

    }
}

</script>
@endsection
