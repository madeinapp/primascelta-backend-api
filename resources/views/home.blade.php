@extends('adminlte::page')

@section('title', 'Primascelta')

@section('content_header')

@stop

@section('content')

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul class="pl-0">
            @foreach ($errors->all() as $error)
                <li style="list-style-type: none">{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    @if(Session::has('message'))
    <p class="alert alert-success">{{ Session::get('message') }}</p>
    @endif
@php
if(Auth::user()->role != 'admin'):
$user_subscription = Auth::user()->subscription->subscription;
if(($user_subscription == 'free') && !empty(Auth::user()->shop->agent_id)){
	$user_subscription = 'standard';
}
$num_product = !empty(Auth::user()->subscription->subscription_product_num) ? Auth::user()->subscription->subscription_product_num : DB::table('subscriptions')->where('subscription', $user_subscription)->value('products_num');
endif;
@endphp
    <div class="card-body">
        <div class="col-12">
            <div class="card">
                <div class="card-body text-center landing-message">
                    @if( Auth::user()->user_type === 'company' )
                        @php
                            $catalog = Auth::user()->shop->shopCatalogs->count();
                            $subscription_type = Auth::user()->subscription->subscription;
                            $diff = null;
							$subscription_free_extended = (($subscription_type != 'trial' && !empty(Auth::user()->shop->agent_id)) ? true : false);
                            $products_num = 100;
                            if( $subscription_type != 'trial' ):
                                $products_num = \App\Models\Subscription::where('subscription', $subscription_type)->first()->products_num;
                            endif;
                            switch( $subscription_type ){
                                case 'standard'  : $diff = $products_num - $catalog; break;
                                case 'premium'   : $diff = $products_num - $catalog; break;
                                case 'enterprise': $diff = $products_num - $catalog; break;
                                case 'trial'     : $diff = $products_num - $catalog; break;
								case 'free'	     : !empty(Auth::user()->shop->agent_id) ? $diff = 100 - $catalog : $diff = $products_num - $catalog; break;
                                default: return 0;
                            }

                            if($diff < 0) $diff = 0;
							$expire_date = '';
							if(!empty(Auth::user()->subscription->expire_date)):
								$arr_date = explode(' ', Auth::user()->subscription->expire_date);
								$expire_date = $arr_date[0];
							endif;
                        @endphp
						@if( Auth::user()->subscription->subscription == 'free')
							<p>Hai attivo un' <strong>abbonamento @php echo(!$subscription_free_extended ? 'Basic' : 'Standard'); @endphp</strong> che ti consente la gestione di un catalogo <strong>fino a {{ $num_product }} prodotti</strong></p>
							<p>Il tuo <strong>abbonamento @php echo(!$subscription_free_extended ? 'Basic' : 'Standard'); @endphp</strong> <u>&egrave; @php echo(!$subscription_free_extended ? 'gratuito</u>, <u>non prevede commissioni</u> sugli ordini e <u>non scade.</u>' : 'in prova gratuita</u>, non prevede commissioni sugli ordini e <strong> scade il '.$expire_date.'</strong>'); @endphp</p>
						@elseif( Auth::user()->subscription->expire_date->diffInDays(\Carbon\Carbon::now()) > 0 && Auth::user()->subscription->expire_date->isFuture()  || ($subscription_type == 'free' && !$subscription_free_extended))
                            @if( Auth::user()->status == "pending" )
                            <p class="text-danger">
                                Il tuo account è attivo ma non ancora pubblico.
                                <br>Nell'interesse di tutta la comunità Primascelta sarai contattato a breve per la verifica di questo account.
                            </p>
                            @endif
							<p>
							Hai attivo un <strong>abbonamento {{ ucfirst($subscription_type) }}</strong> 
							@if($subscription_free_extended || $subscription_type != 'free')
								che scade il <strong>{{ Auth::user()->subscription->expire_date->format('d/m/Y') }}</strong>
							</p>
                            <p>Ti restano ancora <strong>{{ Auth::user()->subscription->expire_date->diffInDays(\Carbon\Carbon::now()) }}</strong> giorni di abbonamento</p>
							@else
							</p>
							@endif
                        @elseif( Auth::user()->subscription->expire_date->diffInDays(\Carbon\Carbon::now()) == 0 )
                            <p class="text-warning">Il tuo abbonamento {{ ucfirst($subscription_type) }} scade <strong>oggi</strong></p>
                        @elseif( Auth::user()->subscription->expire_date->diffInDays(\Carbon\Carbon::now()) > 0 && Auth::user()->subscription->expire_date->isPast() )
                            <p class="text-danger">Il tuo abbonamento {{ ucfirst($subscription_type) }} <strong>è scaduto</strong></p>
                        @endif

                        @if( empty(Auth::user()->subscription->parent) )
                            <p>
                                Hai attivato <strong>{{ $shops }}
                                @if($shops===1)<span>sede</span>@else<span>sedi</span>@endif.</strong>
                                    @if( $remaining_shops >= 1 )
                                    Ne puoi attivare ancora {{ $remaining_shops }}
                                    @else
                                    Con questo abbonamento non puoi attivare altre sedi
                                    @endif
                            </p>
                            <p>In questo momento hai nel catalogo <strong>{{ Auth::user()->shop->shopCatalogs->count() }} prodotti.</strong>
                                @can('add_to_catalog')
                                Puoi inserire ancora <strong>{{ $diff }}</strong>
                                @endcan
                            </p>
                            <p>
                                <a href="/prices">Desidero vedere i piani di abbonamento disponibili</a>
                            </p>
                        @else
                            @if( Auth::user()->subscription && Auth::user()->subscription->parentSubscription && Auth::user()->subscription->parentSubscription->user && Auth::user()->subscription->parentSubscription->user->shop )
                            <p class="text-warning">
                                Questa attività è associata allo stesso abbonamento di
                                <strong>{{ Auth::user()->subscription->parentSubscription->user->shop->name }}</strong>.
                                <br>
                                Devi autenticarti con <strong><i>{{ Auth::user()->subscription->parentSubscription->user->email }}</i></strong> per modificare l'abbonamento.
                            </p>
                            @else
                                Questa attività è associata a un altro abbonamento
                            @endif
                        @endif

                        @if( Auth::user()->shop &&
                            Auth::user()->shop->shop_status !== 'Aperto' &&
                            (!empty(Auth::user()->shop->closed_date_end) && Auth::user()->shop->closed_date_end->isPast())
                            )

                            <p class="text-danger">ATTENZIONE: Il tuo negozio risulta ancora chiuso ma la data di fine chiusura è ormai trascorsa. Vai nel tuo profilo per modificarla.</p>

                        @endif

                        @if( Auth::user()->status === 'pending' )
                            @if( Auth::user()->shop->shopCatalogs->count() < 10 || $check_required_fields )
                                <p><strong>Ti manca pochissimo per richiedere la pubblicazione della tua attività</strong></p>
                            @endif
                            @if( Auth::user()->shop->shopCatalogs->count() < 10 )
                                <div class="card bg-danger">
                                    <span class="text-light" style="padding: 20px">
                                        <i class="fas fa-exclamation-circle" style="display: block; margin-bottom: 20px; font-size: 40px"></i>
                                        <strong>Carica un minimo di 10 prodotti nel catalogo</strong>
                                        <br/>
                                        <a href="{{ route('shopCatalogs.index') }}" class="text-light">Clicca qui</a>
                                    </span>
                                </div>
                            @endif
                            @if( $check_required_fields )
                                <div class="card bg-danger">
                                    <span class="text-light" style="padding: 20px">
                                        <i class="fas fa-exclamation-circle" style="display: block; margin-bottom: 20px; font-size: 40px"></i>
                                        <strong>Completa alune informazioni mancanti</strong>
                                        <br/>
                                        <a href="{{ route('profile.edit') }}" class="text-light">Clicca qui</a>
                                    </span>
                                </div>
                            @endif
                            @if( Auth::user()->shop->shopCatalogs->count() >= 10 && !$check_required_fields )
                            <a href="/publish/request" class="text-light">
                                <div class="card bg-success">
                                    <span class="text-light" style="padding: 20px">
                                        <i class="fas fa-check" style="display: block; margin-bottom: 20px; font-size: 40px"></i>
                                        Voglio essere pubblicato
                                    </span>
                                </div>
                            </a>
                            @endif
                        @endif

                    @endif

                    <div class="text-center">
                        <a href="/?video=true" id="video-link"  class="btn btn-success btn-lg my-4"><i class="fas fa-crown"></i> Impara a usare Primascelta in 60"</a>
                    </div>
                </div>
            </div>

            @if( Auth::user()->user_type == 'company' && Auth::user()->shop->status === 'active' )
            <div class="card">
                <div class="card-body bg-success text-center">
                    <form action="/shop/invisible">
                        <p>SEI VISIBILE NELLE RICERCHE</p>
                        <button class="btn btn-danger">Rendimi invisibile</button>
                    </form>
                </div>
            </div>
            @endif

            @if( Auth::user()->user_type == 'company' && Auth::user()->shop->status === 'invisible' )
            <div class="card">
                <div class="card-body bg-danger text-center">
                    <form action="/shop/visible">
                        <p>SEI INVISIBILE NELLE RICERCHE</p>
                        <button class="btn btn-success">Rendimi visibile</button>
                    </form>
                </div>
            </div>
            @endif

        </div>
    </div>
@stop
