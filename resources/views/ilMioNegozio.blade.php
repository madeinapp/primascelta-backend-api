@extends('adminlte::page')

@section('title', 'Primascelta')

@section('content_header')

@stop

@section('content')
<section class="content-header"><h1>IL MIO NEGOZIO</h1></section>
    <div class="card-body">
        <div class="col-12">
            <div class="card">
                <div class="card-body landing-message">


                                   <p>
                                   Se desideri verificare in tempo reale come i tuoi clienti vedono il tuo negozio basta compiere queste semplici operazioni:
                                   </p>
                                   <ul>
                                   <li>Effettua il logout</li>
                                   <li>Effettua il login autenticandoti con  queste credenziali: <br>UserName: <strong>demo@demo.it</strong> <br> Password: <strong>Demo2021!</strong></li>
                                   </ul>
                                   <p>Ovviamente a questo utente sono inibite le operazioni di acquisto.</p>
                                   <p>Successivamente ti basterà rientrare con i tuoi dati di accesso esercente.</p>

                </div>
            </div>
        </div>
    </div>
@stop
