

<li class="treeview {{ Request::is('users*') ? 'active' : '' }}">
		<a href="#"><i class="fa fa-link"></i> <span>{{ __('common.Settings') }}</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
        <ul class="treeview-menu">
		    <li><a href="{!! route('unitOfMeasures.index') !!}"><i class="fa fa-circle"></i><span>&nbsp;{{__('common.unit_of_measure')}}</a></li>
		    <li><a href="{!! route('shopTypes.index') !!}"><i class="fa fa-circle"></i><span>&nbsp;{{__('common.Shop Types')}}</a></li>
            	    <li><a href="{!! route('foodCategories.index') !!}"><i class="fa fa-circle"></i><span>&nbsp;{{__('common.food_categories')}}</span></a></li>
            	    <li><a href="{!! route('paymentMethods.index') !!}"><i class="fa fa-circle"></i><span>&nbsp;{{__('common.Payment Methods')}}</span></a></li>
            	    <li><a href="{!! route('deliveryMethods.index') !!}"><i class="fa fa-circle"></i><span>&nbsp;{{__('common.Delivery Methods')}}</span></a></li>
		</ul>
</li>
</li>
<li class="{{ Request::is('users*') ? 'active' : '' }}">
    <a href="{!! route('users.index') !!}"><i class="fa fa-user"></i><span>{{__('common.users')}}</span></a>
    <a href="{!! route('shops.index') !!}"><i class="fa fa-opencart"></i><span>&nbsp;{{__('common.shops')}}</span></a>
    <a href="{!! route('agents.index') !!}"><i class="fa fa-user"></i><span>{{__('common.Agents')}}</span></a>
    <a href="{!! route('foods.index') !!}"><i class="fa fa-apple"></i><span>&nbsp;{{__('common.foods')}}</span></a>
    <a href="{!! route('orders.index') !!}"><i class="fa fa-credit-card"></i><span>&nbsp;{{__('common.Orders')}}</span></a>
</li>

<!-- li class="{{ Request::is('users*') ? 'active' : '' }}">
    <a href="{!! route('users.index') !!}"><i class="fa fa-user"></i><span>Users</span></a>
</li -->
