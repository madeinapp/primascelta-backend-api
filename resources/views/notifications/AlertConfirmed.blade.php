Ciao <strong><i>{{ $user->name }}</i></strong>,<br>
l'ordine #{{ $order->id }} non è stato ancora evaso.
@if( $order->pickup_on_site == 1 )
<p>La data/ora di consegna richiesta per il <strong>ritiro in sede</strong> era il {!! $order->pickup_on_site_date->format('d/m/Y') !!}</p>
@elseif($order->home_delivery == 1)
<p>La data/ora di consegna richiesta per la <strong>consegna a domicilio</strong> era il {!! $order->home_delivery_date->format('d/m/Y') !!}</p>
@elseif($order->delivery_host_on_site == 1)
<p>La data/ora di consegna richiesta per la <strong>consegna sul posto</strong> era il {!! $order->delivery_host_on_site_date->format('d/m/Y') !!}</p>
@endif
<br>
<p style="text-align: center">
<a class="btn btn-success" href="{{ url('/orders/'.$order->id.'/edit') }}">Vai all'ordine</a>
</p>
