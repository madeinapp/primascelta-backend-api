Ciao <strong><i>{{ $user->name }}</i></strong>,<br>
l'ordine #{{ $order->id }} è stato cancellato automaticamente.
<p>Il tempo limite per la conferma della richiesta era il {!! $order->confirm_reserve_within->format('d/m/Y') !!} alle {!! $order->confirm_reserve_within->format('H:i') !!}</p>
<br>
<p style="text-align: center">
<a class="btn btn-success" href="{{ url('/orders/'.$order->id.'/edit') }}">Vai all'ordine</a>
</p>
