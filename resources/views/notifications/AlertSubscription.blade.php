Ciao <strong><i>{{ $user->name }}</i></strong>,<br>
il tuo abbonamento scade tra {{ $days }} giorni.
<br>
<p style="text-align: center">
<a class="btn btn-success" href="{{ url('/prices') }}">Clicca qui per rinnovarlo</a>
</p>
