<p>Una nuova azienda si è appena iscritto su Primascelta!</p>
<p>
Verificare il seguente account il prima possibile: <br>
<ul>
<li>Email utente: {{ $company->email }}</li>
<li>Nome utente: {{ $company->name }}</li>
<li>Telefono: {{ $company->phone }}</li>
<li>WhatsApp: {{ $company->shop->whatsapp }}</li>
<li>Sito web: {{ $company->shop->website }}</li>
<li>Partita IVA: {{ $company->shop->tax_vat }}</li>
<li>Codice fiscale: {{ $company->shop->tax_fiscal_code }}</li>
<li>Codice destinatario: {{ $company->shop->tax_code }}</li>
</ul>
</p>
