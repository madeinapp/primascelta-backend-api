Ciao <strong><i>{{ $user->name }}</i></strong>,<br>
hai ricevuto un messaggio da @if( $from ) {{ $order->shop->name }} @else {{ $order->user->name }} @endif relativo all'ordine <strong> #{{ $order->id }}</strong>:
<section class="bg-success p-4 m-4 rounded">
<i>{{ $message }}</i>
</section>
