Ciao <strong><i>{{ $user->name }}</i></strong>,<br>
@if( $user_action->id == $order->user_id && $user_action->id == $user->id )
hai cancellato l'ordine #{{ $order->id }}.
@elseif( $user_action->id == $order->user_id && $user_action->id != $user->id )
l'utente {{ $order->user->name }} ha cancellato l'ordine #{{ $order->id }}.
@elseif( $user_action->id == $order->shop->user->id && $user_action->id == $user->id )
hai cancellato l'ordine #{{ $order->id }}.
@elseif( $user_action->id == $order->shop->user->id && $user_action->id != $user->id )
il negozio {{ $order->shop->name }} ha cancellato l'ordine #{{ $order->id }}.
@endif
@if(!is_null($add_trust_points) && $add_trust_points==1)
@if($trust_points==1)
<p style="text-align: right"><i>Ti è stato accreditato 1 punto fiducia.</i></p>
@elseif($trust_points>1)
<p style="text-align: right"><i>Ti sono stati accreditati {!! $trust_points !!} punti fiducia.</i></p>
@endif
@elseif(!is_null($add_trust_points) && $add_trust_points==0)
@if($trust_points==1)
<p style="text-align: right"><i>Ti è stato scalato 1 punto fiducia.</i></p>
@elseif($trust_points>1)
<p style="text-align: right"><i>Ti sono stati scalati {!! $trust_points !!} punti fiducia.</i></p>
@endif
@else
<p style="text-align: right"><i>Il tuo saldo punti fiducia resta invariato</i></p>
@endif
<p style="text-align: center">
<a class="btn btn-success" href="{{ url('/orders/'.$order->id.'/edit') }}">Vai all'ordine</a>
</p>
