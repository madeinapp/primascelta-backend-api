Ciao <strong><i>{{ $user->name }}</i></strong>,<br>
L'ordine #{{ $order->id }} è stato cancellato automaticamente per scadenza dei tempi.
<p style="text-align: center">
<a class="btn btn-success" href="{{ url('/orders/'.$order->id.'/edit') }}">Vai all'ordine</a>
</p>
