Ciao <strong><i>{{ $user->name }}</i></strong>,<br>
il tuo ordine #{{ $order->id }} è stato confermato integralmente!
@if( $order->pickup_on_site == 1 )
<p>Puoi ritirarlo presso {!! $order->shop->name ?? '' !!} nei tempi e nei modi concordati.</p>
@elseif($order->home_delivery == 1 || $order->delivery_host_on_site == 1)
<p>Ti verrà consegnato da {!! $order->shop->name ?? '' !!} nei tempi e nei modi concordati.</p>
@endif
<br>
<p>
Per qualunque comunicazione in merito a questa prenotazione contatta subito {{ $order->shop->name ?? " l'attività " }}
<br>
<ul>
@if( !empty($order->shop->user->phone) ) <li>Telefono: {!! $order->shop->user->phone ?? "" !!}</li> @endif
@if( !empty($order->shop->whatsapp) && $order->shop->show_phone_on_app == 1  ) <li>WhatsApp: {!! $order->shop->whatsapp ?? "" !!}</li> @endif
</ul>
</p>
<p style="text-align: center">
<a class="btn btn-success" href="{{ url('/orders/'.$order->id.'/edit') }}">Vai all'ordine</a>
</p>
