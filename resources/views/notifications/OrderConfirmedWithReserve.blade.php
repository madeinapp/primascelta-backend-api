Ciao <strong><i>{{ $user->name }}</i></strong>,<br>
il tuo ordine #{{ $order->id }} è stato <strong>ACCETTATO PARZIALMENTE</strong>.
<p>
Per proseguire ti invitiamo a vedere le modifiche suggerite da {!! $order->shop->name !!}.
</p>
<br>
<p>
Per qualunque comunicazione in merito a questa prenotazione contatta subito {{ $order->shop->name ?? " l'attività " }}
<br>
<ul>
@if( !empty($user->phone) ) <li>Telefono: {!! $user->phone ?? "" !!}</li> @endif
@if( !empty($user->shop->whatsapp)  && $user->shop->show_phone_on_app == 1 ) <li>WhatsApp: {!! $user->shop->whatsapp ?? "" !!}</li> @endif
</ul>
</p>
<p style="text-align: center">
<a class="btn btn-success" href="{{ url('/orders/'.$order->id.'/edit') }}">Vai all'ordine</a>
</p>
