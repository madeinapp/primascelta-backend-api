Ciao <strong><i>{{ $user->name }}</i></strong>,<br>
il tuo ordine #{{ $order->id }} è stato evaso correttamente.
<p>{!! $order->shop->name !!} ti ringrazia ti aspetta per la prossima prenotazione!</p>
@if(!is_null($add_trust_points) && $add_trust_points==1)
@if($trust_points==1)
<p style="text-align: right"><i>Ti è stato accreditato 1 punto fiducia.</i></p>
@elseif($trust_points>1)
<p style="text-align: right"><i>Ti sono stati accreditati {!! $trust_points !!} punti fiducia.</i></p>
@endif
@elseif(!is_null($add_trust_points) && $add_trust_points==0)
@if($trust_points==1)
<p style="text-align: right"><i>Ti è stato scalato 1 punto fiducia.</i></p>
@elseif($trust_points>1)
<p style="text-align: right"><i>Ti sono stati scalati {!! $trust_points !!} punti fiducia.</i></p>
@endif
@else
<p style="text-align: right"><i>Il tuo saldo punti fiducia resta invariato</i></p>
@endif
<br>
<p>
Per qualunque comunicazione in merito a questa prenotazione contatta subito {{ $order->shop->name ?? " l'attività " }}
<br>
<ul>
@if( !empty($user->phone) ) <li>Telefono: {!! $user->phone ?? "" !!}</li> @endif
@if( !empty($user->shop->whatsapp)  && $user->shop->show_phone_on_app == 1) <li>WhatsApp: {!! $user->shop->whatsapp ?? "" !!}</li> @endif
</ul>
</p>
<p style="text-align: center">
<a class="btn btn-success" href="{{ url('/orders/'.$order->id.'/edit') }}">Vai all'ordine</a>
</p>
