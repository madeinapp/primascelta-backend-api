Ciao <strong><i>{{ $user->name }}</i></strong>,<br>
il cliente {{ $order->shop->user->name }} ha accettato le tua modifiche all'ordine #{{ $order->id }}.
<p style="text-align: center">
<a class="btn btn-success" href="{{ url('/orders/'.$order->id.'/edit') }}">Vai all'ordine</a>
</p>
