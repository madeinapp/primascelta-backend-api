Ciao <strong><i>{{ $user->name }}</i></strong>,<br>
l'amministratore ha cambiato la tua password di accesso a Primascelta.
<p>
Ecco le tue nuove credenziali di accesso:
</p>
<section class="bg-success p-4 m-4 rounded">
Username: <strong>{{ $user->email }}</strong><br>
Password: <strong>{{ $password }}</strong>
</section>
<p>
Ti consigliamo di autenticarti con la tua nuova password e cambiarla con una che conosci solamente tu e che ricorderai più facilmente.
</p>
<p style="font-size: .9em">
Conserva la tua password in un luogo sicuro, perché hai la responsabilità di tutte le attività associate al tuo account.
Se sospetti che altre persone stiano utilizzando il tuo account, comunicacelo contattando l'assistenza.
</p>
