Ciao <strong><i>{{ $user->name }}</i></strong>,<br>
Il tuo account <i>{{ $user->email }}</i> è stato abilitato.
<p>
Da questo momento potrai nuovamente effettuare ordini su Primascelta!
</p>
