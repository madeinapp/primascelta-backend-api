Ciao <strong><i>{{ $user->name }}</i></strong>,<br>
ti diamo il benvenuto: Il tuo account è attivo!
<section class="bg-success p-4 m-4 rounded">
Il tuo username è:
<strong>{{ $user->email }}</strong>
</section>
<p>
Puoi iniziare ad utilizzare Primascelta per la prenotazione e l'acquisto di alimenti di prima qualità.
Cerca i tuoi fornitori di fiducia su Primascelta ed effettua i tuoi ordini.
</p>
<p style="text-align: center">
Utilizzare Primascelta è davvero facile, tuttavia per utilizzarlo al massimo delle sue potenzialità  ti invitiamo a vedere <a href="https://vimeopro.com/alessandroterracciano/tutorial-privati">i nostri tutorial</a>.
</p>
