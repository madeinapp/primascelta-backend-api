@extends('adminlte::page')

@section('content')
    <section class="content-header">
        <h1 class="pull-left"><i class="fas fa-bell"></i> Centro notifiche</h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>
        <div class="form-group text-right">
            @if( $notifications->count() )

                <a class="btn btn-sm btn-success" style="color: white" onclick="readAll()"><i class="fa fa-envelope-open"></i> Segna tutte come lette</a>
                <a class="btn btn-sm btn-danger" style="color: white" onclick="delAll()"><i class="fa fa-trash"></i> Elimina tutte</a>

            @endif
        </div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="card card-primary">
            <div class="card-body">
                @include('notifications.table')
            </div>
        </div>
        <div class="text-center">

        </div>
    </div>
@endsection

@section('js')
<script>

$(document).ready(function(){

    $('#notifications-table').DataTable({
        "language": {
            url: "{{ url('js/Italian.json') }}"
        },
        paging:   true,
        stateSave: true,
        "initComplete": function (settings, json) {
            $("#notifications-table").wrap("<div style='overflow:auto; width:100%;position:relative;'></div>");
        },
        pageLength: 10,
        columnDefs: [
            { orderable: false, targets: -1 }
        ]
    });

});

readAll = () => {

    $.ajax({
        url: '/notifications/read/all',
        data: {
            "_token": "{{ csrf_token() }}"
        },
        type: 'PUT',
        dataType: 'json',
        statusCode: {
            401: function() {
                document.location.href = '/login';
            },
            419: function() {
                document.location.href = '/login';
            }
        },
        success: function(data){
            if( data.status == 'ok' ){
                document.location.reload();
            }
        }
    });

}

mark = (type, notification_id) => {

    $.ajax({
        url: '/notifications/'+type+'/'+notification_id,
        data: {
            "_token": "{{ csrf_token() }}"
        },
        type: 'PUT',
        dataType: 'json',
        statusCode: {
            401: function() {
                document.location.href = '/login';
            },
            419: function() {
                document.location.href = '/login';
            }
        },
        success: function(data){
            if( data.status == 'ok' ){
                document.location.reload();
            }
        }
    });

}


delNotification = (notification_id) => {
    showConfirm("Notifiche", 'bg-danger', "Eliminare la notifica selezionata?", execDelNotification, [notification_id]);
}

execDelNotification = (notification_id) => {
    $.ajax({
        url: '/notifications/'+notification_id,
        data: {
            "_token": "{{ csrf_token() }}"
        },
        type: 'DELETE',
        dataType: 'json',
        statusCode: {
            401: function() {
                document.location.href = '/login';
            },
            419: function() {
                document.location.href = '/login';
            }
        },
        success: function(data){
            if( data.status == 'ok' ){
                document.location.reload();
            }
        }
    });
}

delAll = () => {
    showConfirm("Notifiche", 'bg-danger', "Eliminare in modo permanente tutte le notifiche?", execDelAllNotifications, []);
}

execDelAllNotifications = () => {
    $.ajax({
        url: '/notifications/all',
        data: {
            "_token": "{{ csrf_token() }}"
        },
        type: 'DELETE',
        dataType: 'json',
        statusCode: {
            401: function() {
                document.location.href = '/login';
            },
            419: function() {
                document.location.href = '/login';
            }
        },
        success: function(data){
            if( data.status == 'ok' ){
                document.location.reload();
            }
        }
    });
}
</script>
@endsection
