@extends('adminlte::page')

@section('content')
    <div class="content">
        <div class="card">
            <div class="card-header">
                <h3>{{ notificationName($notification) }}</h3>
            </div>
            <div class="card-body">
                @php
                $template_name = explode("\\", $notification->type);
                $view_name = "notifications.".$template_name[count($template_name)-1];
                @endphp

                @if(View::exists($view_name))
                    @include($view_name)
                @endif
                <p class="text-right">
                    <small>{{ $notification->created_at->format('d/m/Y H:i') }}</small>
                </p>
            </div>
            <div class="card-footer"><a href="{{ route('notifications.index') }}" class="btn btn-light">Indietro</a></div>
        </div>
    </div>
@endsection
