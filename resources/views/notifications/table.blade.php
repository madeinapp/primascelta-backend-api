<table class="table" id="notifications-table">
    <thead>
        <tr>
            <th></th>
            <th>Tipo notifica</th>
            <th>Data ricezione</th>
            <th>Data lettura</th>
            <th>Azioni</th>
        </tr>
    </thead>
    <tbody>
        @foreach($notifications as $notification)
        <tr data-notification-id="{{ $notification->id }}">
            <td>
                @if($notification->read_at)
                    <a class="btn btn-sm btn-light" style="color: #13bf9e" onclick="mark('unread', '{{ $notification->id }}')"><i class="fas fa-envelope-open"></i></a>
                @else
                    <a href="/notifications/read/{{ $notification->id }}" class="btn btn-sm btn-light" style="color: #ffc107" ><i class="fas fa-envelope"></i></a>
                @endif

            </td>
            <td>{{ notificationName($notification) }}</td>
            <td>{{ $notification->created_at->format('d/m/Y H:i:s') }}</td>
            <td>
                @if($notification->read_at)
                {{ $notification->read_at->format('d/m/Y H:i:s') }}
                @endif
            </td>
            <td>

                    <a class="btn btn-sm btn-danger" style="color: white" title="Elimina" onClick="delNotification('{{ $notification->id }}')"><i class="fa fa-trash"></i></a>

            </td>
        </tr>
        @endforeach
    </tbody>
</table>
