@extends('adminlte::page')

@section('css')
<style>
.punti-fiducia-list li div {
display: flex;
align-items: center;
font-size: 0.9em;
}

.punti-fiducia-list input {
margin-right: 12px;
min-width: 30px;
min-height: 30px;
appearance: none;
border-radius: 100%;
background-color: #fff;
border: 2px solid #ccc;
}

.punti-fiducia-list input:checked {
background-color: #ffc124;
border: 2px solid #ccc;
padding: 3px;
outline: none;
background-clip: content-box;
}
.punti-fiducia-list label {
margin-bottom: 0;
}

.colored--option {
    margin-bottom: 10px;
}
.colored-label {
    background-repeat: no-repeat;
    padding: 20px;
    z-index: 2;
    display: block !important;
    margin-top: -27px;
}
.colored-text{
    display: block;
    z-index: 1;
    padding-left: 30px;
    margin-left: 10px;
}

.label--green {
    background-image: url('/images/faccine/face-green.png');
}

.label--yellow {
    background-image: url('/images/faccine/face-yellow.png');
}

.label--red {
    background-image: url('/images/faccine/face-red.png');
}

.label--black {
    background-image: url('/images/faccine/face-black.png');

}

.txt--green {
    background-color: #7bed8d;
}

.txt--yellow {
    background-color: #ffe266;
}

.txt--red {
    background-color: #ef3737;
}

.txt--black {
    background-color: #000;
    color:#FFF;
}

.border--green {
    border: 3px solid #7bed8d;
}

.border--yellow {
    border: 3px solid #ffe266;
}

.border--red {
    border: 3px solid #ef3737;
}

.border--black {
    border: 3px solid #000;
}

.punti-fiducia-list li {
    padding: 8px;
    margin: 12px 0;
    border-radius: 8px;
    background-color: #eaeaea;
    font-size: 15px;
}
</style>
@endsection

@section('content')

   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="card card-primary">
            {!! Form::open(['id' => 'frm_update_order', 'route' => ['orders.store', $order->id], 'method' => 'post']) !!}

                @php
                $header_bg = '';
                @endphp

                @if( $order->orderStatus->name === 'confirmed' )
                    @php $header_bg = 'bg-info'; @endphp
                @elseif( $order->orderStatus->name === 'confirmed_with_reserve' )
                    @php $header_bg = 'bg-warning'; @endphp
                @elseif( $order->orderStatus->name === 'cancelled' )
                    @php $header_bg = 'bg-danger'; @endphp
                @elseif( $order->orderStatus->name === 'delivered' )
                    @php $header_bg = 'bg-success'; @endphp
                @elseif( $order->orderStatus->name === 'not_delivered' )
                    @php $header_bg = 'bg-secondary'; @endphp
                @endif

                <div class="card-header {{ $header_bg }}">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 text-center text-md-left">
                            DETTAGLIO ORDINE #{{ $order->id }}
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 text-center text-md-right">
                            @switch( $order->orderStatus->name )
                                @case('confirmed') <span class="badge badge-primary" style="font-size: 18px;text-transform: uppercase;font-weight:bold">ORDINE CONFERMATO</span> @break
                                @case('confirmed_with_reserve') <span class="badge badge-warning"  style="font-size: 18px;text-transform: uppercase;font-weight:bold">ORDINE CONFERMATO PARZIALMENTE</span> @break
                                @case('delivered') <span class="badge badge-success"  style="font-size: 18px;text-transform: uppercase;font-weight:bold">ORDINE EVASO</span> @break
                                @case('not_confirmed') <span class="badge badge-secondary"  style="font-size: 18px;text-transform: uppercase;font-weight:bold">
                                @if( $numOfUserOrders === 0 )
                                    NUOVO ORDINE DA ESAMINARE
                                @else
                                    ORDINE DA ESAMINARE
                                @endif
                                </span> @break
                                @case('cancelled') <span class="badge badge-danger"  style="font-size: 18px;text-transform: uppercase;font-weight:bold">ORDINE ANNULLATO</span> @break
                                @default {{ $order->orderStatus->description }}
                            @endswitch
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    @include('orders.fields')
                </div>
                <div class="card-footer pt-4">
                    <!-- Submit Field -->
                    <div class="form-group">
                        {{-- Check subscription is not expired --}}
                        @if( Auth::user()->user_type === 'company' && Auth::user()->shop->id == $order->shop_id )
                            @if( $order->orderStatus->name === 'not_confirmed' )

                                <button type="submit" name="btn_save" value="confirm" id="confirm" class="btn btn-lg btn-success btn-full">Conferma</button>
                                <button type="submit" name="btn_save" value="confirm_with_reserve" id="confirm_with_reserve" class="btn btn-lg btn-warning btn-full">Conferma con riserva</button>

                            @elseif( $order->orderStatus->name === 'confirmed' )
                                <h4>L'ordine <strong>#{{ $order->id }}</strong> di &euro; {{ number_format($order->total_price, 2, ",", ".") }} effettuato da {{ $order->user->name }} è in attesa di un tuo esito</h4>
                                <ul class="mb-2 px-0 punti-fiducia-list" style="list-style-type: none;">
                                    <li class="border--green">
                                        <div class="colored--option">
                                            <input type="radio" name="delivery_option" id="delivered_add_points" value="delivered_add_points">
                                            <label for="delivered_add_points">ORDINE EVASO CORRETTAMENTE</label>
                                        </div>
                                        <div class="colored-text txt--green">AL CLIENTE VANNO <strong> 1 PUNTI FIDUCIA</strong></div>
                                        <div class="colored-label label--green"></div>
                                    </li>
                                    <li  class="border--yellow">
                                        <div class="colored--option">
                                            <input type="radio" name="delivery_option" id="not_delivered" value="not_delivered"> <label for="not_delivered">
                                            <label for="not_delivered">ORDINE ANNULLATO IN TEMPO</label>
                                        </div>
                                        {{--
                                        <div class="colored--option">
                                            <input type="radio" name="delivery_option" id="delivered" value="delivered">
                                            <label for="delivered">FUORI TEMPO MA CLIENTE PERDONATO</label>
                                        </div>
                                         --}}
                                        <div class="colored-text txt--yellow">AL CLIENTE VANNO <strong> 0 PUNTI FIDUCIA</strong></div>
                                        <div class="colored-label label--yellow"></div>
                                    </li>
                                    @if( ($order->pickup_on_site && $pickup_on_site_confirm_within->isPast()) ||
                                         ($order->home_delivery && $home_delivery_confirm_within->isPast()) )
                                    <li  class="border--red">
                                        <div class="colored--option">
                                            <input type="radio" name="delivery_option" id="not_delivered_subtract_points" value="not_delivered_subtract_points">
                                            <label for="not_delivered_subtract_points">ORDINE ANNULLATO FUORI TEMPO O NON RITIRATO</label>
                                        </div>
                                        <div class="colored--option">
                                            <input type="radio" name="delivery_option" id="delivered_subtract_points" value="delivered_subtract_points">
                                            <label for="delivered_subtract_points">ORDINE EVASO FUORI TEMPO</label>
                                        </div>
                                        <div class="colored-text txt--red">AL CLIENTE VANNO <strong> -3 PUNTI FIDUCIA</strong></strong></div>
                                        <div class="colored-label label--red"></div>
                                    </li>
                                    <li  class="border--black">
                                        <div class="colored--option">
                                            <input type="radio" name="delivery_option" id="not_delivered_subtract_points_and_blacklist" value="not_delivered_subtract_points_and_blacklist">
                                            <label for="not_delivered_subtract_points_and_blacklist">METTI IN BLACKLIST IL CLIENTE</label>
                                        </div>
                                        <div style="display: block;align-items: center;text-align: center;font-weight: bold;">PER AQUISTARE DA ME DOVRA' RECARSI IN SEDE</div>
                                        <div class="colored-text txt--black">AL CLIENTE VANNO <strong> -3 PUNTI FIDUCIA</strong></div>
                                        <div class="colored-label label--black"></div>
                                    </li>
                                    @endif
                                </ul>
                                <input type="hidden" name="order_id" value="{{ $order->id }}">
                                <button type="submit" name="btn_save" value="save" id="save" class="btn btn-success btn-full" disabled>Salva e concludi l'ordine</button>

                            @endif

                            @if( !in_array($order->orderStatus->name, ['confirmed', 'cancelled', 'delivered', 'not_delivered']) )
                                <button type="submit" name="btn_save" value="cancel" id="cancel" class="btn btn-lg btn-danger btn-full">Annulla ordine</button>
                            @endif
                        @endif

                        <a href="{{ route('orders.index') }}" class="btn btn-lg btn-default btn-full">Indietro</a>
                    </div>
                </div>
            {!! Form::close() !!}
       </div>
   </div>

    @php
    $pickup_on_site_within = null;
    $home_delivery_within = null;
    $delivery_host_on_site_within = null;
    $cancellable_within = null;
    $confirm_reserve_within = null;
    if( !empty($order->pickup_on_site_within) ){
        $pickup_on_site_within = $order->pickup_on_site_within->format('Y-m-d H:i');
    }
    if( !empty($order->home_delivery_within) ){
        $home_delivery_within = $order->home_delivery_within->format('Y-m-d H:i');
    }
    if( !empty($order->delivery_host_on_site_within) ){
        $delivery_host_on_site_within = $order->delivery_host_on_site_within->format('Y-m-d H:i');
    }
    if( $order->cancellable_within ){
        $cancellable_within = $order->cancellable_within->format('Y-m-d H:i');
    }
    if( $order->confirm_reserve_within ){
        $confirm_reserve_within = $order->confirm_reserve_within->format('Y-m-d H:i');
    }
    @endphp

    @include('orders.modal_confirm')
    @include('orders.modal_confirm_with_reserve')
    @include('orders.modal_cancel')

@endsection

@section('js')
<script>
    $(document).ready(function(){

        let buttonClicked = null;

        $(".messages-container").animate({ scrollTop: $('.messages-container').prop("scrollHeight")}, 1000);

        $("#send_message").on('keyup', function(){
            if( $(this).val() !== '' ){
                $("#btn_send_message").prop("disabled", false);
            }else{
                $("#btn_send_message").prop("disabled", "disabled");
            }
        });

        $("input[name=delivery_option]").on("click", function(){
            $('#save').prop("disabled", false);
        });

        $('button[type=submit]').on('click', function(){
            buttonClicked = $(this).attr('id');
        });

        $("#frm_update_order").on('submit', function(e){
            e.preventDefault();

            let date_cancel = "{{ $date_cancel }}";
            let pickup_on_site_confirm_within = "{{ $pickup_on_site_confirm_within ? $pickup_on_site_confirm_within->format('Y-m-d H:i') : '' }}";
            let home_delivery_confirm_within = "{{ $home_delivery_confirm_within ? $home_delivery_confirm_within->format('Y-m-d H:i') : '' }}";
            //let delivery_host_on_site_confirm_within = "{{ $delivery_host_on_site_confirm_within }}";

            pickup_on_site_confirm_within = pickup_on_site_confirm_within !== "" ? moment(pickup_on_site_confirm_within, "YYYY-MM-DD HH:mm").format('DD/MM/YYYY HH:mm') : moment().format('DD/MM/YYYY HH:mm');
            home_delivery_confirm_within = home_delivery_confirm_within !== "" ? moment(home_delivery_confirm_within, "YYYY-MM-DD HH:mm").format('DD/MM/YYYY HH:mm') : moment().format('DD/MM/YYYY HH:mm');
            //delivery_host_on_site_confirm_within !== "" ? delivery_host_on_site_confirm_within : moment();

            switch(buttonClicked){
                case 'confirm':
                    $("#modal_confirm .modal-header").removeClass('bg-warning');
                    $("#modal_confirm .btn-save").removeClass('bg-warning');

                    $("#modal_confirm .form-group-confirm-reserve").hide();
                    $("#modal_confirm .btn-save").attr('onclick', 'confirmOrder({{ $order->id }}, {{ $order->shop->id }})');
                    $("#modal_confirm").modal('show');
                    break;

                case 'confirm_with_reserve':

                    if( $('#modal_confirm_with_reserve #pickup_on_site_within').length ){
                        $('#modal_confirm_with_reserve #pickup_on_site_within').datetimepicker({
                            locale: 'it',
                            ignoreReadonly: true,
                            minDate: moment(),
                            stepping: 15,
                            daysOfWeekDisabled: [{{ $days_of_week_disabled }}]
                        });
                        $('#modal_confirm_with_reserve #pickup_on_site_within').val(pickup_on_site_confirm_within);

                    }

                    if( $('#modal_confirm_with_reserve #home_delivery_within').length ){
                        $('#modal_confirm_with_reserve #home_delivery_within').datetimepicker({
                            locale: 'it',
                            ignoreReadonly: true,
                            minDate: moment(),
                            stepping: 15,
                            daysOfWeekDisabled: [{{ $days_of_week_disabled }}]
                        });
                        $('#modal_confirm_with_reserve #home_delivery_within').val( home_delivery_confirm_within );
                    }

                    /*
                    if( $('#modal_confirm_with_reserve #delivery_host_on_site_within').length ){
                        $('#modal_confirm_with_reserve #delivery_host_on_site_within').datetimepicker({
                            locale: 'it',
                            ignoreReadonly: true,
                            defaultDate: delivery_host_on_site_confirm_within,
                            minDate: moment(),
                            daysOfWeekDisabled: [{{ $days_of_week_disabled }}]
                        });
                    }
                    */
                    /*
                    console.log("date cancel", moment(date_cancel).format("DD/MM/YYYY hh:mm"));
                    $('#modal_confirm_with_reserve #cancellable_within').datetimepicker({
                        locale: 'it',
                        ignoreReadonly: true,
                        defaultDate: moment(date_cancel).format("DD/MM/YYYY hh:mm"),
                        minDate: moment()
                    });
                    console.log("DOPO")
                    */

                    $('#modal_confirm_with_reserve #confirm_reserve_within').datetimepicker({
                        locale: 'it',
                        ignoreReadonly: true,
                        stepping: 15,
                        minDate: moment()
                    });
                    $('#modal_confirm_with_reserve #confirm_reserve_within').val(moment(date_cancel).format("DD/MM/YYYY hh:mm"));

                    $("#modal_confirm_with_reserve .modal-header").removeClass('bg-primary');
                    $("#modal_confirm_with_reserve .btn-save").removeClass('bg-primary');
                    $("#modal_confirm_with_reserve .form-group-confirm-reserve").show();
                    $("#modal_confirm_with_reserve .btn-save").attr('onclick', 'confirmWithReserve({{ $order->id }}, {{ $order->shop->id }})');
                    $("#modal_confirm_with_reserve").modal('show');
                    break;

                case 'cancel':
                    $("#modal_cancel .btn-cancel").attr('onclick', 'cancelOrder({{ $order->id }})');
                    $("#modal_cancel .btn-cancel").text("Annulla ordine");
                    $("#modal_cancel").modal('show');
                    break;

                case 'save':
                    $('#save').prop("disabled", true);
                    $('#cancel').prop("disabled", true);

                    $("#save").html(
                        `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Salvataggio in corso...`
                    );

                    processOrder("{{ $order->id }}")
                    break;
            }

        });

    });

    function confirmOrder(order_id, shop_id){

        $("#modal_confirm button").prop("disabled", true);

        // add spinner to button
        $("#modal_confirm .btn-save").html(
            `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Salvataggio in corso...`
        );

        let data = {
                "_token": "{{ csrf_token() }}",
                action: "confirmed",
                order_id: order_id,
                shop_id: shop_id,
                message: $("#modal_confirm #message").val()
            };

        $("#modal_confirm .alert").each(function(k, val){
            $(this).text('');
            $(this).hide();
        });

        $.ajax({
            url: "{{ route('orders.store') }}",
            data: data,
            type: 'POST',
            dataType: 'json',
            success: function(data){
                if( data.success ){
                    document.location.href="/orders";
                }
            },
            error: function(data){

                $("#modal_confirm .modal-footer button").prop('disabled', false);

                if( data.responseJSON ){

                    let response = data.responseJSON.errors;

                    for(let i = 0; i<Object.keys(response).length; i++){
                        $("#modal_confirm .form-group-"+Object.keys(response)[i]).addClass('has-error');
                        $("#modal_confirm .alert-"+Object.keys(response)[i]).text(response[Object.keys(response)[i]]);
                        $("#modal_confirm .alert-"+Object.keys(response)[i]).show();
                    }
                }
            },
            complete: function(){
                $("#modal_confirm button").prop("disabled", false);

                // add spinner to button
                $("#modal_confirm .btn-save").html('Conferma');
            }
        });

    }

    function confirmWithReserve(order_id, shop_id){

        let pickup_on_site_within = null;
        let orig_pickup_on_site_within = null;
        let orig_home_delivery_within = null;
        if( $("#modal_confirm_with_reserve #pickup_on_site_within").length ){
            pickup_on_site_within = $("#modal_confirm_with_reserve #pickup_on_site_within").val();
            if( pickup_on_site_within !== '' ){
                pickup_on_site_within = moment(pickup_on_site_within, "DD/MM/YYYY HH:mm");
                orig_pickup_on_site_within = $("#orig_pickup_on_site_within").val();
                if( orig_pickup_on_site_within !== null ){
                    orig_pickup_on_site_within = moment(orig_pickup_on_site_within, "YYYY-MM-DD HH:mm");
                }
            }else{
                alert("La data Ritiro è obbligatoria!");
                return;
            }
        }

        let home_delivery_within = null;
        if( $("#modal_confirm_with_reserve #home_delivery_within").length ){
            home_delivery_within = $("#modal_confirm_with_reserve #home_delivery_within").val();
            if( home_delivery_within !== '' ){
                home_delivery_within = moment(home_delivery_within, "DD/MM/YYYY HH:mm");
                orig_home_delivery_within = $("#orig_home_delivery_within").val();
                if( orig_home_delivery_within !== null ){
                    orig_home_delivery_within = moment(orig_home_delivery_within, "YYYY-MM-DD HH:mm");
                }
            }else{
                alert("La data Ritiro è obbligatoria!");
                return;
            }
        }

        let delivery_host_on_site_within = null;
        if( $("#modal_confirm_with_reserve #delivery_host_on_site_within").length ){
            delivery_host_on_site_within = $("#modal_confirm_with_reserve #delivery_host_on_site_within").val();
            if( delivery_host_on_site_within !== '' ){
                delivery_host_on_site_within = moment(delivery_host_on_site_within, "DD/MM/YYYY HH:mm");
            }else{
                alert("La data Ritiro è obbligatoria!");
                return;
            }
        }

        /*
        let cancellable_within = $("#modal_confirm_with_reserve #cancellable_within").val();
        if( cancellable_within !== '' ){
            cancellable_within = moment(cancellable_within, "DD/MM/YYYY HH:mm");
        }
        */

        let confirm_reserve_within = $("#modal_confirm_with_reserve #confirm_reserve_within").val();
        if( confirm_reserve_within !== '' ){
            confirm_reserve_within = moment(confirm_reserve_within, "DD/MM/YYYY HH:mm");
        }else{
            alert("La data di conferma della riserva è obbligatoria!");
            return;
        }

        let message = $("#modal_confirm_with_reserve #message").val().trim();

        if( message.length === 0 ){
            if( pickup_on_site_within !== null ){
                if( orig_pickup_on_site_within.format('DD/MM/YYYY HH:mm') !== pickup_on_site_within.format('DD/MM/YYYY HH:mm') ){
                    message += `\nNOTE DI SERVIZIO: La nuova data di ritiro è il ${pickup_on_site_within.format('DD/MM/YYYY HH:mm')}`;
                }
            }

            if( home_delivery_within !== null ){
                if( orig_home_delivery_within.format('DD/MM/YYYY HH:mm') !== home_delivery_within.format('DD/MM/YYYY HH:mm') ){
                    message += `\nNOTE DI SERVIZIO: La nuova data di consegna è il ${home_delivery_within.format('DD/MM/YYYY HH:mm')}`;
                }
            }
        }

        $("#modal_confirm_with_reserve .alert").each(function(k, val){
            $(this).text('');
            $(this).hide();
        });

        $("#modal_confirm_with_reserve button").prop("disabled", true);

        // add spinner to button
        $("#modal_confirm_with_reserve .btn-save").html(
            `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Salvataggio in corso...`
        );

        $("#modal_confirm_with_reserve .modal-footer button").prop('disabled', true);

        $.ajax({
            url: "{{ route('orders.store') }}",
            data: {
                "_token": "{{ csrf_token() }}",
                action: "confirmed_with_reserve",
                order_id: order_id,
                shop_id: shop_id,
                pickup_on_site: pickup_on_site_within !== null ? 1 : 0,
                home_delivery: home_delivery_within !== null ? 1 : 0,
                pickup_on_site_date: pickup_on_site_within !== null ? pickup_on_site_within.format('YYYY-MM-DD') : null,
                pickup_on_site_time: pickup_on_site_within !== null ? pickup_on_site_within.format('HH:mm') : null,
                pickup_on_site_within: pickup_on_site_within !== null ? pickup_on_site_within.format('YYYY-MM-DD HH:mm') : null,
                home_delivery_date: home_delivery_within !== null ? home_delivery_within.format('YYYY-MM-DD') : null,
                home_delivery_time: home_delivery_within !== null ? home_delivery_within.format('HH:mm') : null,
                home_delivery_within: home_delivery_within !== null ? home_delivery_within.format('YYYY-MM-DD HH:mm') : null,
                confirm_reserve_within: confirm_reserve_within.format('YYYY-MM-DD HH:mm'),
                message: message
            },
            type: 'POST',
            dataType: 'json',
            success: function(data){
                if( data.success ){
                    document.location.href="/orders";
                }
            },
            error: function(data){

                console.log("errore", data);

                $("#modal_confirm_with_reserve .modal-footer button").prop('disabled', false);

                if( data.responseJSON ){

                    let response = data.responseJSON.errors;

                    if( response.confirm_reserve_within || response.pickup_on_site_within ){
                        for(let i = 0; i<Object.keys(response).length; i++){
                            $("#modal_confirm_with_reserve .form-group-"+Object.keys(response)[i]).addClass('has-error');
                            $("#modal_confirm_with_reserve .alert-"+Object.keys(response)[i]).text(response[Object.keys(response)[i]]);
                            $("#modal_confirm_with_reserve .alert-"+Object.keys(response)[i]).show();
                        }
                    }else{
                        console.log("Mostro errore", response);
                        $("#modal_confirm_with_reserve .alert-check").html("<small>"+response+"</small>");
                        $("#modal_confirm_with_reserve .alert-check").show();
                    }
                }
            },
            complete: function(){
                $("#modal_confirm_with_reserve button").prop("disabled", false);

                // add spinner to button
                $("#modal_confirm_with_reserve .btn-save").html('Conferma');
            }
        });
    }

    function cancelOrder(order_id, action=null){

        $("#modal_cancel button").prop("disabled", true);

        // add spinner to button
        $("#modal_cancel .btn-cancel").html(
            `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Operazione in corso...`
        );

        $.ajax({
            url: "{{ route('orders.store') }}",
            data: {
                "_token": "{{ csrf_token() }}",
                action: action !== null ? action : "cancelled",
                order_id: "{{ $order->id }}",
                message: $("#modal_cancel #message").val()
            },
            type: 'POST',
            dataType: 'json',
            success: function(data){
                if( data.success ){
                    document.location.href="/orders";
                }
            }
        });
    }

    function processOrder(order_id){

        let action = $("input[name=delivery_option]:checked").val();

        if( action === "not_delivered_subtract_points" ||
            action === "delivered_subtract_points" ||
            action === "not_delivered_subtract_points_and_blacklist"
            ){

                $("#save").prop("disabled", false);
                $("#save").html(`Salva e concludi l'ordine`);
                $("#modal_cancel .btn-cancel").text("Annulla ordine");
                if( action === "delivered_subtract_points" ){
                    $("#modal_cancel .modal-title").text("ORDINE EVASO FUORI TEMPO");
                    $("#modal_cancel .btn-cancel").text("Evadi ordine");
                }else if( action === "not_delivered_subtract_points" ){
                    $("#modal_cancel .modal-title").text("ORDINE ANNULLATO FUORI TEMPO O NON RITIRATO");
                }else if( action === "not_delivered_subtract_points_and_blacklist" ){
                    $("#modal_cancel .modal-title").text("METTI IN BLACKLIST IL CLIENTE");
                }

                $("#modal_cancel .btn-cancel").attr("onclick", "cancelOrder({{ $order->id }}, '"+action+"')");
                $("#modal_cancel").modal('show');

        }else{
            $.ajax({
                url: "{{ route('orders.store') }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    action: action,
                    order_id: "{{ $order->id }}"
                },
                type: 'POST',
                dataType: 'json',
                success: function(data){
                    if( data.success ){
                        document.location.href="/orders";
                    }
                }
            });
        }
    }

    function sendMessage(order_id){

        $("#btn_send_message").prop("disabled", true);

        // add spinner to button
        $("#btn_send_message").html(
            `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Invio in corso...`
        );

        let message = $("#send_message").val();

        $.ajax({
            url: '/send_message',
            data: {
                "_token": "{{ csrf_token() }}",
                message: message,
                order_id: order_id
            },
            type: 'POST',
            dataType: 'json',
            complete: function(){
                document.location.reload();
            }
        });

    }

    function openPdf(user_id, url){

        let originalHtml = $("#download-pdf-text").html();

        $("#download-pdf-text").html(`<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Download in corso...`);

        $.ajax({
            url: url,
            type: 'get',
            success: function(filename){
                $("#download-pdf-text").html(`<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Apertura in corso...`);
                window.open(
                    `/storage/invoices/orders/${user_id}/${filename}`,
                    '_blank'
                );
            },
            complete: function(){
                $("#download-pdf-text").html(originalHtml);
            }
        })
    }

</script>
@endsection

