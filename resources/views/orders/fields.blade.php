<div class="row">
    <div class="col-12 col-sm-12 col-md-6 col-lg-6 bg-light p-4">

        <div class="row">
            <div class="form-group col-md-5">

                {!! Form::label(null, 'Data invio: ') !!} {{ $order->created_at->format('d/m/Y') }} <br>
                {!! Form::label(null, 'Ora invio: ') !!} {{ $order->created_at->format('H:i') }}

                <br>

                <div class="form-group bg-secondary rounded text-center p-2">
                    <span style="display: block">{!! Form::label(null, 'Totale') !!}</span>
                    <span class="mt-0 pt-0 font-weight-bold" style="font-size: 2em">
                    {{ number_format($order->total_price, 2, ',', '.') }} &euro;
                    </span>
                </div>
            </div>
            <div class="form-group col-md-7 text-center">
                {!! Form::label(null, 'Cliente ', ['class' => 'mb-0 pb-0']) !!}

                @if( !empty(explode("?",$order->user->image_url)[0]) )
                <img src="{{ getResizedImage(getOriginalImage($order->user->image_url), '100x100') }}" class="rounded mx-auto d-block">
                @endif

                <ul class="mt-0 pt-0 px-0" style="list-style-type: none">
                    <li>{{ $order->user->name }}</li>
                    @if( $numOfUserOrders === 0 )
                    <li class="text-danger"><strong>E' LA PRIMA VOLTA <br>CHE ACQUISTA DA TE</strong></li>
                    @endif
                    <li><i class="fa fa-envelope text-secondary"></i><a href="mailto: {{ $order->user->email }}" class="mx-2 text-secondary">{{ $order->user->email }}</a></li>
                    <li><i class="fa fa-phone text-secondary"></i><a href="tel: {{ $order->home_delivery === 1 ? $order->home_delivery_phone ?? $order->user->phone : $order->user->phone }}" class="mx-2 text-secondary">{{ $order->home_delivery === 1 ? $order->home_delivery_phone ?? $order->user->phone : $order->user->phone }}</a></li>
                    <li>
                        <span class="badge
                        @if( $order->shop->min_trust_points_percentage_bookable < $order->user->trust_points )
                        badge-success
                        @else
                        badge-danger
                        @endif
                         p-1 mt-1">Punti fiducia<br><h2 class="py-0 my-0">{{ $order->user->trust_points }}</h2></span>
                    </li>
                </ul>

            </div>
        </div>

        <div class="row">
            <div class="col-md-12 text-center">
                <button id="download-pdf" onclick="javascript:window.open('https://backend.primascelta.biz/storage/invoices/orders/{{ $order->id}}')" class="btn btn-sm btn-light">

                    <img src="{{ url('/images/visualizza.png') }}" target="_blank" style="height: 50px; vertical-alignment: middle">
                    <div id="download-pdf-text" style="display: inline-block;" class="my-auto">
                        <span style="display: block"><strong>VISUALIZZA <br>RICEVUTA ORDINE</strong></span>

                        <!--span style="display: block"><i>formato A4</i></span -->
                    </div>

                </button>
                <button id="download-pdf" onclick="openPdf({{ $order->user_id }}, '{{ route('orders.pdf', [$order->id]) }}')" class="btn btn-sm btn-light">

                    <img src="{{ url('/images/pdf.png') }}" target="_blank" style="height: 50px; vertical-alignment: middle">
                    <div id="download-pdf-text" style="display: inline-block;" class="my-auto">
                        <span style="display: block"><strong>SCARICA o STAMPA <br>RICEVUTA ORDINE</strong></span>

                        <!--span style="display: block"><i>formato A4</i></span -->
                    </div>

                </button>
            </div>
        </div>

    </div>
    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
        @php
        $bg_delivery = 'bg-warning';
        $delivery_date = null;

        if( $order->pickup_on_site === 1 && !empty($order->pickup_on_site_date) && !empty($order->pickup_on_site_time)):
            $delivery_date = Carbon\Carbon::parse($order->pickup_on_site_date->format('Y-m-d') . ' ' . Carbon\Carbon::parse($order->pickup_on_site_time)->format('H:i'));
            //$delivery_date = $order->pickup_on_site_date;
        elseif( $order->home_delivery === 1 && !empty($order->home_delivery_date) && !empty($order->home_delivery_time) ):
            $delivery_date = Carbon\Carbon::parse($order->home_delivery_date->format('Y-m-d') . ' ' . Carbon\Carbon::parse($order->home_delivery_time)->format('H:i'));
            //$delivery_date = $order->home_delivery_date;
        elseif($order->delivery_host_on_site === 1 && !empty($order->delivery_host_on_site_date) && !empty($order->delivery_host_on_site_time) ):
            $delivery_date = Carbon\Carbon::parse($order->delivery_host_on_site_date->format('Y-m-d') . ' ' . Carbon\Carbon::parse($order->delivery_host_on_site_time)->format('H:i'));
            //$delivery_date = $order->delivery_host_on_site_date;
        endif;

        if( $delivery_date ):
            if( $order->order_status_id < 5 && $delivery_date->isPast() ):
                $bg_delivery = 'bg-danger';
            endif;
        endif;
        @endphp

        <section class="{{ $bg_delivery }} p-4 my-4">
            <h3 class="text-center"><i class="fas fa-shipping-fast mb-2"></i>
                Modalità di
                @if( $order->pickup_on_site == 1 )
                ritiro
                @else
                spedizione
                @endif
            </h3>

            @if( $order->home_delivery == 1 )
            <div class="row mb-2 text-left text-md-center">
                <!-- Shipping Price Field -->
                <div class="col-12">
                    {!! Form::label(null, 'SPESE DI SPEDIZIONE', ['class' => 'mb-0 d-block']) !!}
                    {{ number_format($order->shipping_price, 2, ',', '.') }} &euro;
                </div>
            </div>
            @endif

            <div class="row mb-2 text-left text-md-center">
                <!-- Shipping Price Field -->
                <div class="col-12">
                    {!! Form::label(null, 'METODO DI SPEDIZIONE', ['class' => 'mb-0']) !!}
                    <br>
                    @if( $order->pickup_on_site === 1 )
                    Ritiro in sede
                    @elseif( $order->home_delivery === 1 )
                    Consegna a domicilio
                    @elseif( $order->delivery_host_on_site === 1 )
                    Consegna sul posto
                    @endif
                </div>
            </div>

            <div class="row mb-2 text-left text-md-center">
                <div class="col-12">

                    @if( $order->pickup_on_site === 1 )
                        {!! Form::label(null, 'DATA RITIRO', ['class' => 'mb-0']) !!}
                    @elseif( $order->home_delivery === 1 )
                        {!! Form::label(null, 'DATA SPEDIZIONE', ['class' => 'mb-0']) !!}
                    @elseif( $order->delivery_host_on_site === 1 )
                        {!! Form::label(null, 'DATA / ORA CONSEGNA', ['class' => 'mb-0']) !!}
                    @endif
                    <br>
                    @if( $order->pickup_on_site === 1 )
                        @if( !empty($order->pickup_on_site_within) )
                            @if( $order->pickup_on_site_within->isToday()  )
                                Oggi
                            @elseif( $order->pickup_on_site_within->isTomorrow() )
                                Domani
                            @elseif( $order->pickup_on_site_within->isYesterday() )
                                Ieri
                            @else
                                {{ $order->pickup_on_site_within->format('d/m/Y') }}
                            @endif
                            alle {{ Carbon\Carbon::parse($order->pickup_on_site_within)->format('H:i') }}
                        @elseif( !empty($order->pickup_on_site_date) )
                            @if( $order->pickup_on_site_date->isToday()  )
                                Oggi alle {{ Carbon\Carbon::parse($order->pickup_on_site_time)->format('H:i') }}
                            @elseif( $order->pickup_on_site_date->isTomorrow() )
                                Domani alle {{ Carbon\Carbon::parse($order->pickup_on_site_time)->format('H:i') }}
                            @elseif( $order->pickup_on_site_date->isYesterday() )
                                Ieri alle {{ Carbon\Carbon::parse($order->pickup_on_site_time)->format('H:i') }}
                            @else
                                {{ $order->pickup_on_site_date->format('d/m/Y') }}
                            @endif
                            alle {{ Carbon\Carbon::parse($order->pickup_on_site_time)->format('H:i') }}
                        @endif
                    @elseif( $order->home_delivery === 1 )
                        @if( !empty($order->home_delivery_within) )
                            @if( $order->home_delivery_within->isToday()  )
                                Oggi
                            @elseif( $order->home_delivery_within->isTomorrow() )
                                Domani
                            @elseif( $order->home_delivery_within->isYesterday() )
                                Ieri
                            @else
                                {{ $order->home_delivery_within->format('d/m/Y') }}
                            @endif
                            alle {{ Carbon\Carbon::parse($order->home_delivery_within)->format('H:i') }}
                        @elseif( !empty($order->home_delivery_date))
                            @if( $order->home_delivery_date->isToday()  )
                                Oggi
                            @elseif( $order->home_delivery_date->isTomorrow() )
                                Domani
                            @elseif( $order->home_delivery_date->isYesterday() )
                                Ieri
                            @else
                                {{ $order->home_delivery_date->format('d/m/Y') }}
                            @endif
                            alle {{ Carbon\Carbon::parse($order->home_delivery_time)->format('H:i') }}
                        @endif
                    @elseif( $order->delivery_host_on_site === 1 )
                        @if( !empty($order->delivery_host_on_site_within) )
                            @if( $order->delivery_host_on_site_within->isToday()  )
                                Oggi
                            @elseif( $order->delivery_host_on_site_within->isTomorrow() )
                                Domani
                            @elseif( $order->delivery_host_on_site_within->isYesterday() )
                                Ieri
                            @else
                                {{ $order->delivery_host_on_site_within->format('d/m/Y') }}
                            @endif
                            alle {{ Carbon\Carbon::parse($order->delivery_host_on_site_within)->format('H:i') }}
                        @elseif( !empty($order->delivery_host_on_site_date) )
                            @if( $order->delivery_host_on_site_date->isToday()  )
                                Oggi
                            @elseif( $order->delivery_host_on_site_date->isTomorrow() )
                                Domani
                            @elseif( $order->delivery_host_on_site_date->isYesterday() )
                                Ieri
                            @else
                                {{ $order->delivery_host_on_site_date->format('d/m/Y') }}
                            @endif
                            alle {{ Carbon\Carbon::parse($order->home_delivery_time)->format('H:i') }}
                        @endif
                    @endif
                </div>
            </div>

            <div class="row mb-2 text-left text-md-center">
                @if( $order->home_delivery === 1 )
                <div class="col-12">
                    {!! Form::label(null, 'INDIRIZZO DI SPEDIZIONE', ['class' => 'mb-0']) !!}
                    <br>
                    {{ $order->home_delivery_route }}
                    @if( !empty($order->home_delivery_route) && !empty($order->home_delivery_street_number) )
                    ,&nbsp;
                    @endif
                    {{ $order->home_delivery_street_number }}<br>

                    {{ $order->home_delivery_postal_code }}
                    @if( !empty($order->home_delivery_postal_code) && !empty($order->home_delivery_city) )
                    &nbsp;-&nbsp;
                    @endif
                    {{ $order->home_delivery_city }}<br>

                    {{ $order->home_delivery_region }}
                    @if( !empty($order->home_delivery_region) && !empty($order->home_delivery_country) )
                    &nbsp;,&nbsp;
                    @endif
                    {{ $order->home_delivery_country }}<br>
                </div>
                @endif
            </div>

            <div class="row mb-2 text-left text-md-center">
                @if( $order->order_status_id == 3 && !empty($order->confirm_reserve_within))
                <div class="col-12">
                    {!! Form::label(null, 'CONFERMA RISERVA ENTRO', ['class' => 'mb-0']) !!}
                    <br>
                    {{ $order->confirm_reserve_within->format('d/m/Y') }} alle {{ $order->confirm_reserve_within->format('H:i') }}
                </div>
                @endif

                @if( !empty($order->cancellable_within) )
                <div class="col-12">
                    {!! Form::label(null, 'ANNULLABILE ENTRO', ['class' => 'mb-0']) !!}
                    <br>
                    {{ $order->cancellable_within->format('d/m/Y') }} alle {{ $order->cancellable_within->format('H:i') }}
                </div>
                @endif
            </div>
        </section>
    </div>
</div>

<!-- Note Field -->
{{--
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('note', 'Note') !!}
    {!! Form::textarea('note', null, ['class' => 'form-control', 'rows' => 3]) !!}
</div>
--}}

<section class="mt-4">
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
                <tr class="text-center bg-success">
                    <th colspan="4">PRODOTTI ORDINATI</th>
                </tr>
                <tr>
                    <th>Nome</th>
                    <th>Immagine</th>
                    <th>Q.tà</th>
                    <th>Prezzo</th>
                </tr>
            </thead>
            <tbody>
            @foreach( $order->orderFoods as $orderFood )
            <tr>
                <td>{{ $orderFood->food_name }}</td>
                <td>
                    @if( !empty($orderFood->food_image) )
                    <img src="{{ getResizedImage(getOriginalImage($orderFood->food_image), '230x154') }}" style="width: 100px">
                    @elseif( !empty($orderFood->shop_catalog_id) )
                        @if( !empty($orderFood->shopCatalog->food_image) )
                        <img src="{{ getResizedImage(getOriginalImage($orderFood->shopCatalog->food_image), '230x154') }}" style="width: 100px">
                        @elseif( !empty($orderFood->shopCatalog->food) )
                        <img src="{!! asset('/storage/images/foods/'.NormalizeString($orderFood->shopCatalog->food->foodCategory->name).'/'.getResizedImage(getOriginalImage($orderFood->shopCatalog->food->image), '230x154')) !!}" style="width: 100px">
                        @endif
                    @elseif( !empty($orderFood->shop_offer_food_id) )
                        @if( !empty($orderFood->offerFood->food_image) )
                        <img src="{{ $orderFood->offerFood->food_image }}" style="width: 100px">
                        @elseif( !empty($orderFood->offerFood->shopCatalog) )
                            @if( !empty($orderFood->offerFood->shopCatalog->food_image) )
                            <img src="{{ getResizedImage(getOriginalImage($orderFood->offerFood->shopCatalog->food_image), '230x154') }}" style="width: 100px">
                            @elseif( !empty($orderFood->offerFood->shopCatalog->food) )
                            <img src="{!! asset('/storage/images/foods/'.NormalizeString($orderFood->offerFood->shopCatalog->food->foodCategory->name).'/'.getResizedImage(getOriginalImage($orderFood->offerFood->shopCatalog->food->image), '230x154')) !!}" style="width: 100px">
                            @endif
                        @endif
                    @endif
                </td>
                <td>
                    @if( isset($orderFood->shopCatalog) )
                        {{ $orderFood->quantity * $orderFood->shopCatalog->unit_of_measure_buy_quantity }} {{ $orderFood->shopCatalog->unitOfMeasureBuy->name }}
                    @elseif( isset($orderFood->offerFood) && isset($orderFood->offerFood->shopCatalog) )
                        {{ $orderFood->quantity * $orderFood->offerFood->shopCatalog->unit_of_measure_buy_quantity }} {{ $orderFood->offerFood->shopCatalog->unitOfMeasureBuy->name }}
                    @endif
                </td>
                <td>
                    {{ number_format($orderFood->price, 2, ',', '.') }} &euro;
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</section>

@if( $order->orderChats )
<section class="bg-light p-4">
    <h3><i class="fas fa-comments"></i> Messaggi</h3>
    <div class="row">
        <div class="col-md-7">
            @include('orders.messages')
        </div>
        <div class="col-md-5">
            {{-- Check user subscription --}}
            @can('add_to_catalog')
                <div class="form-group">
                    <label form="send_message">Invia un messaggio a {{ $order->user->name }}</label>
                    <textarea id="send_message" name="send_message" class="form-control" rows="6"></textarea>
                </div>
                <div class="form-group">
                    <button id="btn_send_message" class="btn btn-success float-right" onClick="sendMessage({{ $order->id }})" disabled>Invia messaggio</button>
                </div>
            @endcan
        </div>
    </div>
</section>

@endif
