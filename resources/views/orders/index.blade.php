@extends('adminlte::page')

@section('content')
    <section class="content-header">
        <div class="row">
            <div class="col-md-9">
                <h1 class="pull-left">Ordini</h1>
            </div>

            <div style="width=100%;text-align:right">
                <button class="btn btn-success" onclick="self.location.reload()"><i class="fa fa-retweet" aria-hidden="true"></i> Aggiorna pagina</button></div>
            </div>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="card card-primary">
            <div class="card-body">
                @include('orders.table')
            </div>
        </div>
        <div class="text-center">

        {{-- @include('adminlte-templates::common.paginate', ['records' => $orders])  --}}

        </div>
    </div>
@endsection

