@if($order->orderChats->count() > 0)
<div class="messages-container d-flex flex-column" style="max-height: 400px; overflow: auto; background-color: #efefef">
    @foreach ($order->orderChats as $orderChat)
        @if( $orderChat->from == 0 )
            <div class="d-flex flex-row justify-content-start">
                <div class="user-message rounded bg-light m-2 p-2" style="max-width: 20em">
                    <span><strong>{{ $order->user->name }}</strong></span><br />
                    {!! nl2br($orderChat->message) !!}<br />
                    <span class="float-right"><small style="font-size: .6em">
                        <i>
                        @if( $orderChat->created_at->isToday() )
                        {{ $orderChat->created_at->format('H:i') }}</small>
                        @else
                        {{ $orderChat->created_at->format('d/m/Y H:i') }}</small>
                        @endif
                        </i>
                    </span>
                </div>
            </div>
        @elseif( $orderChat->from == 1 )
            <div class="d-flex flex-row justify-content-end">
                <div class="user-message rounded bg-success m-2 p-2 " style="max-width: 20em">
                    <span><strong>{{ $order->shop->user->name }}</strong></span>
                    <br />
                    {!! nl2br($orderChat->message) !!}<br />
                    <span class="float-right"><small style="font-size: .6em">
                        <i>
                        @if( $orderChat->created_at->isToday() )
                        {{ $orderChat->created_at->format('H:i') }}</small>
                        @else
                        {{ $orderChat->created_at->format('d/m/Y H:i') }}</small>
                        @endif
                        </i>
                    </span>
                </div>
            </div>
        @endif
    @endforeach
</div>
@else
    <p class="font-weight-bold text-danger text-center m-4">
        <i class="fas fa-comment-slash"></i> Non ci sono messaggi
    </p>
@endif
