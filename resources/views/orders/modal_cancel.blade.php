<div id="modal_cancel" class="modal" tabindex="-1" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h5 class="modal-title">Annulla ordine</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-md-12">
                        <label for="message">E' consigliabile scrivere una motivazione al tuo cliente</label>
                        <textarea class="form-control" id="message" row="3"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Indietro</button>
                <button type="button" class="btn btn-danger btn-cancel">Annulla ordine</button>
            </div>

        </div>
    </div>
</div>
