<div id="modal_confirm" class="modal" tabindex="-1" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-success">
                <h5 class="modal-title">Conferma</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-md-12">
                        <label for="message">Se lo desideri puoi lasciare un messaggio al cliente</label>
                        <textarea class="form-control" id="message" row="3"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Indietro</button>
                <button type="button" class="btn btn-primary btn-save">Conferma</button>
            </div>

        </div>
    </div>
</div>
