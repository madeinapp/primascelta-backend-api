<div id="modal_confirm_with_reserve" class="modal" tabindex="-1" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-warning">
                <h5 class="modal-title">Conferma con riserva</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    @if( $order->pickup_on_site == 1 )
                    <div class="form-group col-md-12 form-group-pickup_on_site_within">
                        <label for="pickup_on_site_within" style="font-weight: normal !important;width: 100%;"><strong>Ritiro in sede </strong>richiesto dal cliente</label>
                        <div class="input-group date" data-target-input="nearest">
                            <input type="text" id="pickup_on_site_within" name="pickup_on_site_within" class="form-control datetimepicker-input" data-target="#pickup_on_site_within" placeholder="gg/mm/aaaa oo:mm" required="true" readonly />
                            <input type="hidden" id="orig_pickup_on_site_within" value="{{ $pickup_on_site_confirm_within }}">
                            <div class="input-group-append" data-target="#pickup_on_site_within" data-toggle="datetimepicker">
                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>
                            <br>
                        </div>
                        <span class="alert alert-danger alert-pickup_on_site_within p-0" style="display: none;"></span>
                        <span class="alert alert-danger alert-check p-0" style="display: none;"></span>
                        <label style="font-weight: normal !important;text-align: center;width: 100%;"><i>Per proporgli una modifica clicca sul calendario</i></label>
                    </div>
                    @endif
                    @if( $order->home_delivery == 1 )
                    <div class="form-group col-md-12 form-group-home_delivery_within">
                        <label for="home_delivery_within" style="font-weight: normal !important;width: 100%;"><strong>Consegna a domicilio </strong>richiesta dal cliente</label>
                        <div class="input-group date" data-target-input="nearest">
                            <input type="text" id="home_delivery_within" name="home_delivery_within" class="form-control datetimepicker-input" data-target="#home_delivery_within" placeholder="gg/mm/aaaa oo:mm" required="true" readonly  />
                            <input type="hidden" id="orig_home_delivery_within" value="{{ $home_delivery_confirm_within }}">
                            <div class="input-group-append" data-target="#home_delivery_within" data-toggle="datetimepicker">
                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>
                            <br>
                        </div>
                        <span class="alert alert-danger alert-home_delivery_within p-0" style="display: none;"></span>
                        <label style="font-weight: normal !important;text-align: center;width: 100%;"><i>Per proporgli una modifica clicca sul calendario</i></label>
                    </div>
                    @endif
                    @if( $order->delivery_host_on_site == 1 )
                    <div class="form-group col-md-12 form-group-delivery_host_on_site_within">
                        <label for="delivery_host_on_site_within">Consegna sul posto entro</label>
                        <div class="input-group date" data-target-input="nearest">
                            <input type="text" id="delivery_host_on_site_within" name="delivery_host_on_sitedelivery_host_on_site_within" class="form-control datetimepicker-input" data-target="#delivery_host_on_site_within" placeholder="gg/mm/aaaa oo:mm" required="true" readonly />
                            <div class="input-group-append" data-target="#delivery_host_on_site_within" data-toggle="datetimepicker">
                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>
                            <br>
                        </div>
                        <span class="alert alert-danger alert-delivery_host_on_site_within p-0" style="display: none;"></span>
                        <label style="font-weight: normal !important;text-align: center;width: 100%;"><i>Per proporgli una modifica clicca sul calendario</i></label>
                    </div>
                    @endif
                    {{--
                    <div class="form-group col-md-12 form-group-cancellable_within">
                        <label for="cancellable_within">Annullabile entro</label>
                        <div class="input-group date" data-target-input="nearest">
                            <input type="text" id="cancellable_within" name="cancellable_within" class="form-control datetimepicker-input" data-target="#cancellable_within" placeholder="gg/mm/aaaa oo:mm" required="true" />
                            <div class="input-group-append" data-target="#cancellable_within" data-toggle="datetimepicker">
                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>
                            <br>
                        </div>
                        <span class="alert alert-danger alert-cancellable_within p-1" style="display: none;"></span>
                    </div>
                     --}}
                    <div class="form-group col-md-12 form-group-confirm_reserve_within">
                        <label for="home_delivery_within" style="font-weight: normal !important;width: 100%;"><strong>Ordine annullabile </strong>dal cliente entro</label>
                        <div class="input-group date" data-target-input="nearest">
                            <input type="text" id="confirm_reserve_within" name="confirm_reserve_within" class="form-control datetimepicker-input" data-target="#confirm_reserve_within" placeholder="gg/mm/aaaa oo:mm" required readonly />
                            <div class="input-group-append" data-target="#confirm_reserve_within" data-toggle="datetimepicker">
                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>
                            <br>
                        </div>
                        <span class="alert alert-danger alert-confirm_reserve_within p-0" style="display: none;"></span>
                        <label style="font-weight: normal !important;text-align: center;width: 100%;"><i>Per proporgli una modifica clicca sul calendario</i></label>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="message" style="text-align:center;font-size:0.8em;width:100%">Scrivi un messaggio al cliente che espliciti la tua modifica<br>di un prodotto. Puoi semplicemente scrivere "cambio <br>data" o "prodotto non più disponibile"</label>
                        <textarea class="form-control" id="message" row="3"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Indietro</button>
                <button type="button" class="btn btn-warning btn-save">Conferma con riserva</button>
            </div>

        </div>
    </div>
</div>
