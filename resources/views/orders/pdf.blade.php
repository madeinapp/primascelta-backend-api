<html lang="en"><head>
        <meta charset="UTF-8">
        <title>Primascelta: Ordine #{{ $order->id }}</title>
        <style>

                @media print {
                    @page {
                        size: 58mm 280mm;
                    }
                    html, body {
                        width: 58mm;
                        margin: 0px auto;
                        font-size: 12px;
                        font-family: Arial;
                    }
                    /* ... the rest of the rules ... */
                }

                 @page  { margin: 0px auto; size: 58mm 280mm;}



            			body {
							width: 58mm;
                            margin: 0px auto;
							font-size: 12px;
							font-family: Arial;

						}

						.container {
							padding: 6px;
							text-align:center;

						}
						.container div {
							padding: 6px;
							text-align:center;
						}


						.block {
							background: #ddd;
							border-radius: 4px;
							margin: 6px;
							padding: 6px;
							text-align: center;
							line-height:15px;
						}

						ul {
							list-style: none;
							padding: 0;
						}
						.products-list {
							padding:0 !important;
							text-align: center;
						}
						.product {
							text-align: center;
						}



						.product__footer div {
							padding: 0 6px;
							border-right: 1px solid #aaa;
						}

						.product__footer div:last-child {
							border-right: none;
						}

						h2, h3, h4 {
							margin: 1px;
						}
						.total-cost{}


						.total-cost strong.price{
							font-size: 1.5em;
						}
						.total-cost i{
							font-size: 0.8em;
							font-weight:bold;
						}
        </style>

    </head>

    <body cz-shortcut-listen="true">
			<div class="container">
				  	<img width="207" height="40" src="https://backend.primascelta.biz/images/logo_stampa.jpg" />
					<div>ORDINE <strong>#{{ $order->id }}</strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;del <strong><i>{{ $order->created_at->format('d/m/Y') }}</i></strong></div>
					<div><strong>Stato</strong>
                        @switch( $order->orderStatus->name )
                            @case('confirmed') ORDINE CONFERMATO @break
                            @case('confirmed_with_reserve') ORDINE ACCETTATO PARZIALMENTE @break
                            @case('delivered') ORDINE EVASO @break
                            @case('not_confirmed')
                                @if( $numOfUserOrders === 1 )
                                    NUOVO ORDINE DA ESAMINARE
                                @else
                                    ORDINE DA ESAMINARE
                                @endif
                            @break
                            @case('cancelled') ORDINE ANNULLATO @break
                            @default {{ $order->orderStatus->description }}
						@endswitch
					</div>
					<div><strong>Annullabile entro il</strong><br>
						<strong>{{ $order->cancellable_within->format('d/m/Y') }}</strong>
						alle <strong>{{ $order->cancellable_within->format('H:i') }}</strong><br />

					</div>


					<div class="block">
						<h3>C L I E N T E</h3>
						<div>
						@if( $order->user->name )
							<strong>{{ $order->user->name }}</strong><br />
						@endif
						@if( $order->user->address )
							{{ $order->user->address }}<br />
						@endif
						@if( $order->user->phone )
							{{ $order->home_delivery === 1 ? $order->home_delivery_phone ?? $order->user->phone : $order->user->phone }}<br />
						@endif
						@if( $order->user->email )
							{{ $order->user->email }}
						@endif
						</div>
					</div>

					<div class="block">
						<h3>C O N S E G N A</h3>
						<div>
						@if( $order->pickup_on_site == 1 )
							<strong>Ritiro in sede</strong><br />

							@if( $order->pickup_on_site_within )
								Il <strong>{{ $order->pickup_on_site_within->format('d/m/Y') }}</strong>
							@elseif( $order->pickup_on_site_date )
								Il <strong>{{ $order->pickup_on_site_date->format('d/m/Y') }}</strong>
							@endif

							@if( $order->pickup_on_site_within )
								alle <strong>{{ $order->pickup_on_site_within->format('H:i') }}</strong><br />
							@elseif( $order->pickup_on_site_time )
								alle <strong>{{ $order->pickup_on_site_time }}</strong><br />
							@endif
							@if( !empty($order->shop->delivery_address) )
								in {{ $order->shop->delivery_address }}<br />
							@else
								in {{ $order->shop->address }}<br />
							@endif

						@elseif( $order->home_delivery == 1 )
							<strong>Consegna a domicilio</strong><br />


							@if( $order->home_delivery_within )
								Il <strong> {{ $order->home_delivery_within->format('d/m/Y') }}</strong>
							@elseif( $order->home_delivery_date )
								Il <strong> {{ $order->home_delivery_date->format('d/m/Y') }}</strong>
							@endif

							@if( $order->home_delivery_within )
								alle <strong> {{ $order->home_delivery_within->format('H:i') }}</strong><br />
							@elseif( $order->home_delivery_time )
								alle <strong> {{ $order->home_delivery_time }}</strong><br />
							@endif

							@if( !empty($order->home_delivery_address) )
								<br>in {{ $order->home_delivery_address }}
							@endif
						@elseif( $order->delivery_host_on_site == 1 )
							Consegna sul posto
						@endif
						</div>

					</div>

					<div class="products-list">
						<ul>

							@foreach( $order->orderFoods as $orderFood )
							<li class="product">
								<h4>{{ $orderFood->food_name }}</h4>
								<div class="product__footer">
									<div>
										@if( !empty($orderFood->shop_catalog_id) )
										@if( $orderFood->shopCatalog )
											{{ number_format($orderFood->shopCatalog->food_price, 2, ',', '.') }} &euro; / {{ $orderFood->shopCatalog->unitOfMeasure->name }}
										@endif
									@elseif( !empty($orderFood->shop_offer_food_id) )
										@if( $orderFood->offerFood )
											{{ number_format($orderFood->offerFood->discounted_price, 2, ',', '.') }} &euro; / {{ $orderFood->offerFood->shopCatalog->unitOfMeasure->name }}
										@endif
									@endif
									&nbsp;&nbsp;|&nbsp;&nbsp;Q.tà {{ $orderFood->quantity }}
									&nbsp;&nbsp;|&nbsp;&nbsp;{{ number_format($orderFood->price, 2, ',', '.') }} &euro;

									<font color="#ccc">____________</font>
									</div>
								</div>
							</li>
							@endforeach
						</ul>
					</div>
					<div class="delivery-cost">
						SPEDIZIONE
						@if( $order->shipping_price == "0.00" )
						GRATUITA
						@else
						{{ number_format($order->shipping_price, 2, ',', '.') }} &euro;
						@endif
					</div>
					<div class="total-cost">
						<strong>TOTALE DA PAGARE</strong> <strong class="price">{{ number_format($order->total_price, 2, ',', '.') }} &euro;</strong>
						<br><i>documento privo di validità fiscale</i>
					</div>
					<div>
					@if($order->orderChats->count() > 0)
							@foreach ($order->orderChats as $orderChat)
								@if( $orderChat->from == 0 )
									{{ $order->user->name }} :
									<i>
									@if( $orderChat->created_at->isToday() )
									{{ $orderChat->created_at->format('H:i') }}
									@else
									{{ $orderChat->created_at->format('d/m/Y H:i') }}
									@endif
									</i>
								@elseif( $orderChat->from == 1 )
									{{ $order->shop->user->name }}
									<i>
									@if( $orderChat->created_at->isToday() )
									{{ $orderChat->created_at->format('H:i') }}
									@else
									{{ $orderChat->created_at->format('d/m/Y H:i') }}
									@endif
									</i>
								@endif
								{!! nl2br($orderChat->message) !!}
							@endforeach
						</div>
						@endif
					</div>
					<div class=" block">

	                  <h2>{{ $order->shop->name}}</h2>
						<div>
						@if( !empty($order->shop->delivery_address) )
							{{ $order->shop->delivery_address }}<br />
						@else
								{{ $order->shop->address }}<br />
						@endif
						@if( !empty($order->shop->user->phone) )
						{{ $order->shop->user->phone }} /
						@endif
						{{ $order->shop->user->whatsapp }}<br>
						{{ $order->shop->user->email }}

						</div>
					</div>
					<div >
						<img width="200" src="https://backend.primascelta.biz/images/qr.jpg"/>
					</div>
	        </div>
				</body>
</html>
