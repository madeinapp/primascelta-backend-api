@extends('adminlte::page')

@section('content')
    <section class="content-header">
        <h1>
            Dettaglio Ordine
        </h1>
    </section>
    <div class="content">
        <div class="card card-primary">
            <div class="card-body">
                <div class="row" style="padding-left: 20px">
                    @include('orders.show_fields')
                    <a href="{{ route('orders.index') }}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
