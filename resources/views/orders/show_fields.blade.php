<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'Cliente') !!}
    <p>{{ $order->user->name }}</p>
</div>

<!-- Order Status Id Field -->
<div class="form-group">
    {!! Form::label('order_status_id', 'Order Status Id:') !!}
    <p>{{ $order->order_status_id }}</p>
</div>

<!-- Total Price Field -->
<div class="form-group">
    {!! Form::label('total_price', 'Total Price:') !!}
    <p>{{ $order->total_price }}</p>
</div>

<!-- Shipping Price Field -->
<div class="form-group">
    {!! Form::label('shipping_price', 'Shipping Price:') !!}
    <p>{{ $order->shipping_price }}</p>
</div>

<!-- Note Field -->
<div class="form-group">
    {!! Form::label('note', 'Note:') !!}
    <p>{{ $order->note }}</p>
</div>

