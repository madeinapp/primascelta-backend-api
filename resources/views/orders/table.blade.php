
<div class="card-header mb-4 p-0">
    <div class="row m-0 p-4 bg-success rounded">
        <div class="col-sm-12 col-md-4">
            <label>Cerca per stato ordine</label>
            {!! Form::select('order_status_id', $order_status, null, ['id' => 'order_status_id', 'class' => 'form-control', 'disabled' => 'disabled']) !!}
        </div>
        <div class="col-sm-12 col-md-4">
            <label>Cerca per tipo di spedizione</label>
            {!! Form::select('delivery', ['TUTTI', 'Ritiro in sede', 'Consegna a domicilio', 'Consegna sul posto'], null, ['id' => 'delivery', 'class' => 'form-control', 'disabled' => 'disabled']) !!}
        </div>
    </div>
</div>

<table class="table table-hover orders-table" id="orders-table">
    <thead>
        <tr>
            <th>Azioni</th>
            <th>Num.</th>
            <th>Cliente</th>
            <th>Totale</th>
            <th>Data ordine</th>
            <th>Consegna</th>
        </tr>
    </thead>
</table>

@section('js')
<script>

var table = null;

$(document).ready( function(){
    setTimeout(function(){
        self.location.reload();
    },30000);
    table = $('#orders-table').DataTable({
        language: {
            url: "{{ url('js/Italian.json') }}"
        },
        dom: "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'p>r>"+
            "<'row'<'col-sm-12't>>"+
            "<'row'<'col-sm-12 col-md-6'i><'col-sm-12 col-md-6'p>>",
        processing: true,
        serverSide: true,
        stateSave: true,
        paginate: true,
        pageLength: 20,
        ajax: {
            url: `{{ route('orders.index') }}`,
            data: function (d) {
                d.searchname = $('#searchname').val(),
                d.order_status = $('#order_status_id').val()
                d.delivery = $('#delivery').val()
            }
        },
        "initComplete": function (settings, json) {
            $("#orders-table").wrap("<div style='overflow:auto; width:100%;position:relative;'></div>");
        },
        columns: [
            {data: 'action', name: 'action', orderable: false, "searchable": false},
            {data: 'id', name: 'o.id', orderable: true},
            {data: 'user_name', name: 'user_name'},
            {data: 'total', name: 'total',  orderable: true, "searchable": false},
            {data: 'order_date', name: 'order_date', "searchable": false},
            {data: 'delivery', name: 'delivery', "searchable": false}
        ]
    });

    table.on( 'draw', function () {
        $("#order_status_id").prop("disabled", false);
        $("#delivery").prop("disabled", false);
        $("#searchname").prop("disabled", false);
        if( $("#searchname").val() !== '' ){
            $("#searchname").focus();
        }
    } );

    $("#order_status_id").on('change', function(){
        $("#order_status_id").prop("disabled", true);
        $("#delivery").prop("disabled", true);
        $("#searchname").prop("disabled", true);
        table.draw();
    });

    $("#searchname").on('keyup', function(){
        if( $("#searchname").val().length >= 3 || $("#searchname").val().length === 0 ){
            $("#order_status_id").prop("disabled", true);
            $("#delivery").prop("disabled", true);
            $("#searchname").prop("disabled", true);
            table.draw();
        }
    });

    $("#delivery").on('change', function(){
        $("#order_status_id").prop("disabled", true);
        $("#delivery").prop("disabled", true);
        $("#searchname").prop("disabled", true);
        table.draw();
    });

});

</script>
@endsection
