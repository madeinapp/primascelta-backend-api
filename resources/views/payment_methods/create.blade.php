@extends('adminlte::page')

@section('content')
    <section class="content-header">
        <h1>
            Payment Method
        </h1>
    </section>
    <div class="content">
        {{-- @include('adminlte-templates::common.errors') --}}
        <div class="card card-primary">
            <div class="card-body">
                <div class="card-body">
                    {!! Form::open(['route' => 'paymentMethods.store']) !!}

                        @include('payment_methods.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
