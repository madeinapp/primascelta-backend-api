@extends('adminlte::page')

@section('content')
    <section class="content-header">
        <h1>
            {{ __('common.Payment Method') }}
        </h1>
   </section>
   <div class="content">
       {{-- @include('adminlte-templates::common.errors') --}}
       <div class="card card-primary">
           <div class="card-body">
               <div class="card-body">
                   {!! Form::model($paymentMethod, ['route' => ['paymentMethods.update', $paymentMethod->id], 'method' => 'patch']) !!}

                        @include('payment_methods.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection
