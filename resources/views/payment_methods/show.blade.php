@extends('adminlte::page')

@section('content')
    <section class="content-header">
        <h1>
           {{ __('common.Payment Method')}}
        </h1>
    </section>
    <div class="content">
        <div class="card card-primary">
            <div class="card-body">
                <div class="row" style="padding-left: 20px">
                    @include('payment_methods.show_fields')
                    <a href="{{ route('paymentMethods.index') }}" class="btn btn-default">{{__('common.Back')}}</a>
                </div>
            </div>
        </div>
    </div>
@endsection
