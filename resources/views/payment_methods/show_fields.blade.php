<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', __('common.name').':') !!}
    <p>{{ __('common.'.$paymentMethod->name) }}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', __('common.description').':') !!}
    <p>{{ $paymentMethod->description }}</p>
</div>

