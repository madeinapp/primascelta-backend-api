<table class="table" id="paymentMethods-table">
    <thead>
        <tr>
            <th>{{ __('common.action') }}</th>
            <th>{{ __('common.name') }}</th>
            <th>{{ __('common.description') }}</th>
        </tr>
    </thead>
    <tbody>
    @foreach($paymentMethods as $paymentMethod)
        <tr>
            <td>
                {!! Form::open(['route' => ['paymentMethods.destroy', $paymentMethod->id], 'method' => 'delete']) !!}

                    <a href="{{ route('paymentMethods.edit', [$paymentMethod->id]) }}" class='btn btn-default btn-sm'><i class="fas fa-edit"></i></a>
                    {!! Form::button('<i class="fas fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-sm', 'onclick' => "return confirm('Sei sicuro?')"]) !!}

                {!! Form::close() !!}
            </td>
            <td>{{ $paymentMethod->name }}</td>
            <td>{{ $paymentMethod->description }}</td>
        </tr>
    @endforeach
    </tbody>
</table>

@section('js')
<script>
$(function () {
    $('#paymentMethods-table').DataTable({
        "language": {
            url: "{{ url('js/Italian.json') }}"
        },
        paging:   true,
        "initComplete": function (settings, json) {
            $("#paymentMethods-table").wrap("<div style='overflow:auto; width:100%;position:relative;'></div>");
        },
        pageLength: 10,
        stateSave: true,
        columnDefs: [
            { width: "10%", orderable: false, targets: 0 }
        ]
    });
});
</script>
@endsection
