@extends('adminlte::page')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Pagamenti</h1>
        <div class="pull-right  float-right d-none d-sm-block" >
           <!--a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('users.create') !!}">Aggiungi nuovo</a-->
        </div>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="card card-primary">
            <div class="card-body">
                <table id="table-payments" class="table table-striped table-hover">
                    <thead>
                        <th>Data</th>
                        <th>Utente</th>
                        <th>Attività</th>
                        <th>Causale</th>
                        <th>Importo</th>
                        <th>Metodo di pagamento</th>
                    </thead>
                    <tbody>
                        @foreach($payments as $payment)
                        <tr>
                            <td><span style="display: none">{{ $payment->created_at->format('YmdHi') }}</span>{{ $payment->created_at->format('d/m/Y H:i') }}</td>
                            <td>@if( $payment->user) {{ $payment->user->name }} @endif</td>
                            <td>@if( $payment->user && $payment->user->shop ) <a href="{{ route('shops.edit', ['shop' => $payment->user->shop->id]) }}">{{ $payment->user->shop->shopType }} {{ $payment->user->shop->name }}</a> @endif</td>
                            <td>{{ $payment->title }}</td>
                            <td>{{ number_format($payment->price, 2, ',', '.') }} &euro;</td>
                            <td>{{ $payment->method_of_payment }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('js')
<script>
    $(document).ready(function(){
        $('#table-payments').DataTable({
            "language": {
                url: "{{ url('js/Italian.json') }}"
            },
            "paging":   true,
            "initComplete": function (settings, json) {
                $("#table-payments").wrap("<div style='overflow:auto; width:100%;position:relative;'></div>");
            },
            "bInfo": true,
            stateSave: true,
            "order": [[ 0, "desc" ]],
            "columnDefs": [
                { type: 'date-euro', targets: 0 }
            ]
        });
    });
</script>
@endsection
