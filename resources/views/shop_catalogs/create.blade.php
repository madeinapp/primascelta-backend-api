@extends('adminlte::page')

@section('css')
<link rel="stylesheet" type="text/css" href="/fileinput/normalize.css" />
<link rel="stylesheet" type="text/css" href="/fileinput/component.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.css"/>
<script>(function(e,t,n){var r=e.querySelectorAll("html")[0];r.className=r.className.replace(/(^|\s)no-js(\s|$)/,"$1js$2")})(document,window,0);</script>

<style>
    :required{
        border: 1px solid orange;
    }
</style>
@endsection

@section('content')
	<div class="modal fade" id="modal-danger" style="display: none;" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content bg-danger">
            <div class="modal-header">
              <h4 class="modal-title">Attenzione!</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div class="modal-body">
              <p>Per gestire le offerte occorre avere un piano di abbonamento a pagamento.</p>
			  <p><a href="https://backend.primascelta.biz/prices" target="_blank"><strong><u>Desidero vedere i piani di abbonamento disponibili</u></strong></a></p>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-outline-light" data-dismiss="modal">Chiudi</button>
              <!--button type="button" class="btn btn-outline-light">Save changes</button-->
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
    <section class="content-header">
        <h1>
            Aggiungi prodotto al tuo catalogo
        </h1>
    </section>
    <div class="content">

        @php
            $show_form = false;
        @endphp

        @if ($errors->any())
        @php
            $show_form = true;
        @endphp

        <div class="alert alert-danger">
            Si è verificato un errore
        </div>
        @endif

        <div class="card card-primary">

            <div id="new-catalog-header" class="card-header">
                @if( old('food_id', null) === null )
                <img src="/images/crea_catalogo.png" width="70" align="left">
                Ti guideremo passo passo alla creazione del tuo catalogo. <br>Prima di creare un tuo prodotto da principio <b>cercalo nel nostro catalogo</b>: potrai personalizzarlo o eliminarlo in qualsiasi momento
                @else
                <div class="row m-0">
                    <div class="col-3 col-sm-3 col-md-1 col-lg-1">
                        <h1 class="display-4" style="display: inline-block; margin: 0">
                            <i class="fa fa-check"></i>
                        </h1>
                    </div>
                    <div class="col-9 col-sm-9 col-md-10 col-lg-10">
                        Complimenti: per aggiungere questo prodotto al tuo catalogo ti basterà compilare i campi evidenziati in giallo
                        (i soli obbligatori). Ricorda che, in qualsiasi momento, puoi modificare anche gli altri campi pre compilati da noi.
                    </div>
                </div>
                @endif
            </div>
            <div class="card-body">

                @if( !$show_form )
                <button class="btn btn-success" id="btn-search-catalog" onclick="openSearchFoodModal()" style="display: flex; align-items:center;">
                    <span style="padding-right: 6px">Mi semplifico le cose, il mio prodotto da inserire lo cerco nel vostro catalogo</span><i class="fas fa-angle-right"></i>
                </button><br/>
                <button class="btn btn-warning mt-2" id="btn-ins-catalog" onclick="emptyProduct()"  style="display: flex; align-items:center;">
                    <span style="padding-right: 6px">Intendo aggiungere il nuovo prodotto inserendolo da zero</span><i class="fas fa-angle-right"></i>
                </button>
                @endif

                <div id="frm-shop-catalog" style="display: @if( $show_form) block @else none @endif">
                    {!! Form::open(['route' => 'shopCatalogs.store', 'files' => true]) !!}

                        @include('shop_catalogs.fields')

                        <button type="submit" class="btn btn-success btn-full" name="save" value="save">Salva</button>
					@if(Auth::user()->subscription->subscription != 'free')
                        <button type="submit" class="btn btn-success btn-full" name="save-and-create-offer" value="save-and-create-offer">Salva e crea un'offerta</button>
					@else
                        <button type="submit" class="btn btn-success btn-full" name="save-and-create-offer" value="save-and-create-offer" data-toggle="modal" data-target="#modal-danger" onClick="return false;">Salva e crea un'offerta</button>
					@endif
                        <a href="{{ route('shopCatalogs.index') }}" class="btn btn-link btn-full" >Annulla</a>

                    {!! Form::close() !!}
                </div>

            </div>
        </div>
    </div>

    @include('shop_catalogs.modal_search_food')
    @include('shop_catalogs.modal_search_catalog')
    @include('shop_catalogs.modal_uom')
    @include('users.modal_preview')
@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.js"></script>
<script src="/cropimage/cropimage.js"></script>
<script src="/fileinput/custom-file-input.js"></script>
<script>

    let interval_name , interval_category, interval_type = null;
    let food_data = [];
    let selected_foods = [];
    let prezzo_pacco_convenienza = 0;
    let defaultUom = null;

    openSearchFoodModal = () => {
        food_data = [];
        $("#food_search_results").html('');
        $("#frm-search-food")[0].reset();
        $('#search_food_type').empty();
        $("#modal-search-food").modal('show');
    }

    creaPacco = () => {
        food_data = [];
        selected_foods = [];
        $("#food_description").val('');
        $("#food_price").val('');
        $("#catalog_search_results").html('');
        $("#frm-search-catalog")[0].reset();
        $('#search_food_type').empty();
        $("#modal-search-catalog").modal('show');
    }

    emptyProduct = () => {
        $("#btn-search-catalog").hide();
        $("#btn-ins-catalog").hide();
        $("#food_category_id_hidden").remove();
        $("#food_category_id_span").remove();
        $("#food_type_hidden").remove();
        $("#food_type_span").remove();
        $("#frm-shop-catalog").show();
    }

    $(document).ready(function(){

        $("#frm-search-food").on("submit", function(e){
            e.preventDefault();
        });

        if( $("#defaultUom").length ){
            defaultUom = JSON.parse($("#defaultUom").val());
        }

        /*
        $("input").on('change', function(){
            changeBackgroundRequired($(this));
        });
        */

        loadFoodType($("#search_food_category").val(), 'search_food_type');

        $("#search_food_name").on('keyup', function(){
            clearInterval(interval_name);

            var food_name = $("#search_food_name").val();
            var category_id = $("#search_food_category").val();
            var type = $("#search_food_type").val();

            if( category_id === '' && type === null && food_name.length < 3 ){
                return false;
            }

            interval_name = setInterval(function(){
                clearInterval(interval_name);
                searchFood();
            }, 500);
        });

        $("#search_food_category").on('change', function(){
            loadFoodType($(this).val(), 'search_food_type');
            searchFood();
        });

        $("#search_food_type").on('change', function(){
            searchFood();
        });


        $("#search_catalog_name").on('keyup', function(){
            clearInterval(interval_name);

            var food_name = $("#search_catalog_name").val();
            var category_id = $("#search_catalog_category").val();
            var type = $("#search_catalog_type").val();

            if( category_id === '' && type === null && food_name.length < 3 ){
                return false;
            }

            interval_name = setInterval(function(){
                clearInterval(interval_name);
                searchCatalog();
            }, 500);
        });

        $("#search_catalog_category").on('change', function(){
            loadCatalogType($(this).val(), 'search_catalog_type');
            searchCatalog();
        });

        $("#search_catalog_type").on('change', function(){
            searchCatalog();
        });

        $("#food_category_id").on('change', function(){
            loadFoodType($(this).val(), 'food_type');
            if( defaultUom !== null ){
                setDefaultUom();
                loadRelatedsUom($("#unit_of_measure_id").val(), "{{ old('unit_of_measure_buy', null) }}");
            }
        });

        loadRelatedsUom($("#unit_of_measure_id").val(), "{{ old('unit_of_measure_buy', null) }}");

        $("#unit_of_measure_id").on('change', function(){
            loadRelatedsUom($(this).val());
        });

        $("#compact_mode").on('click', function(){
            if( $(this).is(':checked') ){
                $(".food-description").hide();
                $(".food-img").hide();
                $(".food-type").hide();
            }else{
                $(".food-description").show();
                $(".food-img").show();
                $(".food-type").show();
            }
        });

        $("#modal-search-food").on("hidden.bs.modal", function () {
            $("#compact_mode").prop("checked", false);
            $(".compact-mode-container").hide();
        });

    });

    loadRelatedsUom = (unit_of_measure_id, default_val) => {

        let element_id = "unit_of_measure_buy";
        $(`#${element_id}`).empty();

        $.ajax({
            url: '/related_uoms/'+unit_of_measure_id,
            data: {
                "_token": "{{ csrf_token() }}",
            },
            type: 'GET',
            dataType: 'json',
            statusCode: {
                401: function() {
                    document.location.href = '/login';
                },
                419: function() {
                    document.location.href = '/login';
                }
            },
            success: function(res){

                let element = document.getElementById(element_id);

                if( res.related_uoms ){
                    $.each(res.related_uoms, function(k, val){
                        let option = document.createElement("option");
                        option.text = val;
                        option.value = k;
                        element.add(option);
                    });
                }else{
                    let option = document.createElement("option");
                    option.text = $("#unit_of_measure_id option:selected").text();
                    option.value = $("#unit_of_measure_id option:selected").val();
                    element.add(option);
                }

                if( default_val > 0 ){
                    $(`#${element_id}`).val(default_val);
                }
            }
        });
    }

    modalUom = () => {
        $("#modal-uom").modal('show');
    }

    setDefaultUom = () => {
        let food_category_id = $("#food_category_id").val();
        $("#unit_of_measure_id").val(defaultUom[food_category_id]);
    }

    loadFoodType = (food_category_id, element_id, sel_value = null) => {

        console.log("loadFoodType", food_category_id);
        console.log("element_id", element_id);
        console.log("sel_value", sel_value);

        $('#'+element_id).empty();

        if( food_category_id != '' ){

            $.ajax({
                url: '/food/'+food_category_id+'/types',
                data: {
                    "_token": "{{ csrf_token() }}",
                },
                type: 'GET',
                dataType: 'json',
                statusCode: {
                    401: function() {
                        document.location.href = '/login';
                    },
                    419: function() {
                        document.location.href = '/login';
                    }
                },
                success: function(res){
                    $(`#${element_id}`).html('');
                    if( $(`#${element_id}_list` ).length ){
                        element_id = `${element_id}_list`;
                    }
                    $(`input[name=${element_id}]`).val('');
                    var options = '<option value="">Seleziona...</option>';

                    for(var i=0; i<res.count; i++){
                        options += `<option value="${res['Food Type List'][i]}">${res['Food Type List'][i]}</option>`;
                    }

                    $(`#${element_id}`).html(options);
                    if( sel_value ){
                        $(`#${element_id}`).val(sel_value);

                        /*
                        if( option.text === sel_value ){
                            element.selectedIndex = (i+1);
                        }
                        */
                    }

                }
            });

        }else{

            var element = document.getElementById(element_id);
            var option = document.createElement("option");
            option.text = 'Seleziona...';
            option.value = '';
            element.add(option);

        }
    }

    searchFood = () => {
        var food_name = $("#search_food_name").val();
        var category_id = $("#search_food_category").val();
        var type = $("#search_food_type").val();

        if( category_id === '' && type === null && food_name.length < 3 ){
            return false;
        }

        $(".modal-body-header").hide();

        var params = {
            "_token": "{{ csrf_token() }}"
        };

        if( food_name !== '') params['food_name'] = food_name;
        if( category_id !== '') params['category_id'] = category_id;
        if( type !== '') params['type'] = type;

        var row_food = "<tr>";
        row_food += "<td class='text-center'>Ricerca in corso...</td>";
        row_food += "</tr>";
        $("#food_search_results").html(row_food);

        $.ajax({
            url: '/food/search',
            data: params,
            type: 'GET',
            dataType: 'json',
            statusCode: {
                401: function() {
                    document.location.href = '/login';
                },
                419: function() {
                    document.location.href = '/login';
                }
            },
            success: function(res){

                $("#food_search_results").html('');

                $("#compact_mode").prop("checked", false);

                if( res.count > 0 ){

                    $(".compact-mode-container").show();

                    food_data = res["Food list"];

                    for(var i=0; i<res.count; i++){
                        var row = res['Food list'][i];

                        if( selected_foods.indexOf(row.id) < 0 ){
                            var row_food = `<tr id="row_food_${row.id}" class="row-search-result">`;

                            var image = "<img width='100'  src='/images/food.jpg'>";
                            if ( row.image !== '' ){
                                image = "<img width='100'  src='"+row.image+"'>";
                            }

                            row_food += "<td style='width: 20px;'><button type='button' class='btn btn-sm btn-success' title='Aggiungi' onClick='addFood("+i+")'><i class='fa fa-plus'></i></i></button></td>";
                            row_food += "<td class='text-left food-name'><strong>"+row.name+"</strong><div class='food-description'><small>"+((row.description!=null)?row.description:'')+"</small></div></td>";
                            row_food += "<td class='food-img' style='width: 110px;'>"+image+"</td>";
                            row_food += "<td class='text-left food-type'><small><strong>"+row.food_category.name.toLowerCase().split(' ').join('_')+"</strong><br>"+row.type+"</small></td>";
                            row_food += "</tr>";
                            $("#food_search_results").append(row_food);
                        }
                    }
                }else{
                    var row_food = "<tr>";
                    row_food += "<td class='text-center'>Spiacenti ma la tua ricerca non ha prodotto risultati. Prova con un altro termine</td>";
                    row_food += "</tr>";
                    $("#food_search_results").append(row_food);
                }

            }
        });
    }

    addFood = (food_id) => {

        /** Nascondo i bottoni e mostro il form */
        $("#btn-search-catalog").hide();
        $("#btn-ins-catalog").hide();
        $("#frm-shop-catalog").show();

        let food = food_data[food_id];

        food_data = [];

        /* Nascondo la modale e valorizzo il form */
        $("#modal-search-food").modal('hide');
        $("#food_id").val(food.id);
        $("#food_category_id").val(food.food_category_id);
        $("#food_name").val(food.name);
        $("#food_description").val(food.description);
        $("#unit_of_measure_id").val(food.unit_of_measure_id);

        /* Immagine prodotto */
        $("#output_image").attr('src', food.other_images['512x340']);
        $("#food_image").val(food.other_images['512x340']);
        $("#img-suggestion").html("Puoi cambiare questa immagine in qualsiasi momento. L'immagine verrà ritagliata in un formato 3:2 File ammessi > .jpg - Max 10 mb - dimensioni minime di 646x430");
        $("#btn-fileimage").text("Seleziona una nuova immagine")

        //loadFoodType(food.food_category_id, 'food_type', food.type);

        /* Se seleziono un alimento dal catalogo
        *  rimuovo la possibilità di cambiare genere e tipologia.
        *  Quindi elimino le select e mostro solo il testo
        *  e allo stesso tempo valorizzo delle input hidden con i valori selezionati
        */
        $("#food_category_id_hidden").val(food.food_category_id);
        $("#food_type_hidden").val(food.type);

        $("#food_category_id_span").text($("#food_category_id option:selected").text());
        $("#food_type_span").text(food.type);

        $("#food_category_id").prop("required", false);
        $("#food_type").prop("required", false);
        $("#food_category_id").remove();
        $("#food_type").remove();

        $("#food_category_id_span").show();
        $("#food_type_span").show();

        if( $("#food_category_id_span").text() !== 'BOX DI PRODOTTI' ){
            $("#btn-crea-pacco-convenienza").remove();
        }

        $.each(food.specials, function(k, val){
            $("#shop_catalog_specials option[value="+val.id+"]").attr('selected', true);
        });

        loadRelatedsUom(food.unit_of_measure_id);

        $("#new-catalog-header").html('<div class="row m-0"><div class="col col-auto"><h1 class="display-4" style="display: inline-block; margin: 0"><i class="fa fa-check"></i></h1></div><div class="col">Complimenti: per aggiungere questo prodotto al tuo catalogo ti basterà compilare i campi evidenziati in giallo (i soli obbligatori). Ricorda che, in qualsiasi momento, puoi modificare anche gli altri campi pre compilati da noi.</div></div>');

    }

    loadCatalogType = (food_category_id, element_id, sel_value = null) => {

        $('#'+element_id).empty();

        if( food_category_id != '' ){

            $.ajax({
                url: '/food/'+food_category_id+'/types',
                data: {
                    "_token": "{{ csrf_token() }}",
                },
                type: 'GET',
                dataType: 'json',
                statusCode: {
                    401: function() {
                        document.location.href = '/login';
                    },
                    419: function() {
                        document.location.href = '/login';
                    }
                },
                success: function(res){
                    var element = document.getElementById(element_id);
                    var option = document.createElement("option");
                    option.text = 'Seleziona...';
                    option.value = '';
                    element.add(option);
                    for(var i=0; i<res.count; i++){
                        option = document.createElement("option");
                        option.text = res['Food Type List'][i];
                        option.value = res['Food Type List'][i];
                        element.add(option);

                        if( sel_value ){
                            if( option.text === sel_value ){
                                element.selectedIndex = (i+1);
                            }
                        }

                    }
                }
            });

        }else{

            var element = document.getElementById(element_id);
            var option = document.createElement("option");
            option.text = 'Seleziona...';
            option.value = '';
            element.add(option);

        }
    }

    searchCatalog = () => {
        var food_name = $("#search_catalog_name").val();
        var category_id = $("#search_catalog_category").val();
        var type = $("#search_catalog_type").val();

        if( category_id === '' && type === null && food_name.length < 3 ){
            return false;
        }

        $(".modal-body-header").hide();

        var params = {
            "_token": "{{ csrf_token() }}"
        };

        if( food_name !== '') params['food_name'] = food_name;
        if( category_id !== '') params['food_category_id'] = category_id;
        if( type !== '') params['food_type'] = type;

        var row_food = "<tr>";
        row_food += "<td class='text-center'>Ricerca in corso...</td>";
        row_food += "</tr>";
        $("#catalog_search_results").html(row_food);

        $.ajax({
            url: '/shopCatalogs/search',
            data: params,
            type: 'GET',
            dataType: 'json',
            statusCode: {
                401: function() {
                    document.location.href = '/login';
                },
                419: function() {
                    document.location.href = '/login';
                }
            },
            success: function(res){

                $("#catalog_search_results").html('');

                $("#compact_mode").prop("checked", false);

                if( res.count > 0 ){

                    $(".compact-mode-container").show();

                    food_data = res["Catalog list"];

                    for(var i=0; i<res.count; i++){
                        var row = res['Catalog list'][i];
                        if( selected_foods.indexOf(row.id) < 0 ){
                            var row_food = `<tr id="row_food_${row.id}" class="row-search-result">`;

                            var image = "<img width='100'  src='/images/food.jpg'>";
                            if ( row.image !== '' ){
                                image = "<img width='100'  src='"+row.food_image+"'>";
                            }

                            row_food += "<td style='width: 20px;'><button type='button' class='btn btn-sm btn-success' title='Aggiungi' onClick='addCatalog("+i+")'><i class='fa fa-plus'></i></i></button></td>";
                            row_food += "<td class='text-left food-name'><strong>"+row.food_name+"</strong><div class='food-description'><small>"+((row.food_description!=null)?row.food_description:'')+"</small></div></td>";
                            row_food += "<td class='food-img' style='width: 110px;'>"+image+"</td>";
                            row_food += "<td class='text-left food-type'><small><strong>"+row.food_category.name.toLowerCase().split(' ').join('_')+"</strong><br>"+row.food_type+"</small></td>";
                            row_food += "</tr>";
                            $("#catalog_search_results").append(row_food);
                        }
                    }
                }else{
                    var row_food = "<tr>";
                    row_food += "<td class='text-center'>Spiacenti ma la tua ricerca non ha prodotto risultati. Prova con un altro termine</td>";
                    row_food += "</tr>";
                    $("#catalog_search_results").append(row_food);
                }

            }
        });
    }

    addCatalog = (food_id) => {
        let food = food_data[food_id];
        selected_foods.push(food.id);
        let description = $("#food_description").val();
        description += ((description !== '') ? '\n' : 'Questo Box contiene tutti i seguenti prodotti:\n') + '1 x ' + food.food_name + ` (${parseFloat(food.food_price, 2).toFixed(2)} €)`;
        prezzo_pacco_convenienza = Math.round(prezzo_pacco_convenienza + parseFloat(food.food_price), 2);
        $("#food_description").val(description);
        $(`#catalog_search_results #row_food_${food.id}`).remove();
        $("#food_price").val(prezzo_pacco_convenienza.toFixed(2));
    }

    /*
    preview_image = (event) => {
        var reader = new FileReader();
        reader.onload = function()
        {
            var output = document.getElementById('output_image');
            output.src = reader.result;
        }
        reader.readAsDataURL(event.target.files[0]);
        $("#food_image").val(event.target.files[0]);
    }
    */
</script>
@endsection
