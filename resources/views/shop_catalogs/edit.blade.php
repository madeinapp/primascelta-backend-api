@extends('adminlte::page')

@section('css')
<link rel="stylesheet" type="text/css" href="/fileinput/normalize.css" />
<link rel="stylesheet" type="text/css" href="/fileinput/component.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.css"/>
<script>(function(e,t,n){var r=e.querySelectorAll("html")[0];r.className=r.className.replace(/(^|\s)no-js(\s|$)/,"$1js$2")})(document,window,0);</script>

<style>
    input[value=""]:required{
        background-color: yellow;
    }
    :required{
        border: 1px solid orange;
    }
</style>

@endsection

@section('classes_body', 'shop-catalogs shop-catalogs-edit')

@section('content')
    <section class="content-header">
        <h1>
            Modifica prodotto
        </h1>
   </section>
   <div class="content">

       @if ($errors->any())
       <div class="alert alert-danger">
            @if( $errors->count() === 1 )
            Si è verificato {{ $errors->count() }} errore
            @else
            Si sono verificati {{ $errors->count() }} errori
            @endif
            {{--
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
             --}}
       </div>
       @endif

       <div class="card card-primary">
           <div class="card-body">
               <div class="card-body">
                   {!! Form::model($shopCatalog, ['route' => ['shopCatalogs.update', $shopCatalog->id], 'method' => 'patch', 'files' => true]) !!}

                        @include('shop_catalogs.fields')

                        <button type="submit" class="btn btn-success btn-full" name="save">Salva</button>

                        <a href="{{ route('shopCatalogs.index') }}" class="btn btn-link btn-full" >Annulla</a>

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>

   @include('shop_catalogs.modal_uom')
   @include('users.modal_preview')
@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.js"></script>
<script src="/cropimage/cropimage.js"></script>
<script src="/fileinput/custom-file-input.js"></script>
<script>

    let defaultUom = null;

    $(document).ready(function(){

        if( $("#defaultUom").length ){
            defaultUom = JSON.parse($("#defaultUom").val());
        }

        if($("#food_category_id").length){
            loadFoodType($("#food_category_id").val(), 'food_type','{{ $shopCatalog->food_type }}');
        }

        $("#food_category_id").on('change', function(){
            loadFoodType($(this).val(), 'food_type');
            if( defaultUom !== null ){
                setDefaultUom();
            }
        });

        loadRelatedsUom($("#unit_of_measure_id").val(), "{{ old('unit_of_measure_buy', $shopCatalog->unit_of_measure_buy) }}");

        $("#unit_of_measure_id").on('change', function(){
            loadRelatedsUom($(this).val());
        });

    });

    loadRelatedsUom = (unit_of_measure_id, default_val) => {

        let element_id = "unit_of_measure_buy";
        $(`#${element_id}`).empty();

        $.ajax({
            url: '/related_uoms/'+unit_of_measure_id,
            data: {
                "_token": "{{ csrf_token() }}",
            },
            type: 'GET',
            dataType: 'json',
            statusCode: {
                401: function() {
                    document.location.href = '/login';
                },
                419: function() {
                    document.location.href = '/login';
                }
            },
            success: function(res){

                if( res.related_uoms ){
                    let element = document.getElementById(element_id);
                    $.each(res.related_uoms, function(k, val){
                        let option = document.createElement("option");
                        option.text = val;
                        option.value = k;
                        element.add(option);
                    });
                }
                if( parseInt(default_val) > 0 ){
                    $(`#${element_id}`).val(default_val);
                }
            }
        });
    }

    modalUom = () => {
        $("#modal-uom").modal('show');
    }

    setDefaultUom = () => {
        let food_category_id = $("#food_category_id").val();
        $("#unit_of_measure_id").val(defaultUom[food_category_id]);
        alert("OK");
        loadRelatedsUom(defaultUom[food_category_id]);
    }

    loadFoodType = (food_category_id, element_id, sel_value = null) => {

        $('#'+element_id).empty();

        if( food_category_id != '' ){

            $.ajax({
                url: '/food/'+food_category_id+'/types',
                data: {
                    "_token": "{{ csrf_token() }}",
                },
                type: 'GET',
                dataType: 'json',
                statusCode: {
                    401: function() {
                        document.location.href = '/login';
                    },
                    419: function() {
                        document.location.href = '/login';
                    }
                },
                success: function(res){
                    let element = document.getElementById(element_id);
                    let option = document.createElement("option");
                    option.text = 'Seleziona...';
                    option.value = '';
                    element.add(option);
                    for(let i=0; i<res.count; i++){
                        option = document.createElement("option");
                        option.text = res['Food Type List'][i];
                        option.value = res['Food Type List'][i];
                        element.add(option);
                    }

                    if( sel_value ){
                        $(element).val(sel_value);
                        /*
                        if( option.text === sel_value ){
                            element.selectedIndex = (i+1);
                        }
                        */
                    }
                }
            });

        }else{

            var element = document.getElementById(element_id);
            var option = document.createElement("option");
            option.text = 'Seleziona...';
            option.value = '';
            element.add(option);

        }
    }

    /*
    preview_image = (event) => {
        var reader = new FileReader();
        reader.onload = function()
        {
        var output = document.getElementById('output_image');
        output.src = reader.result;
        }
        console.log(document.getElementById('fileimage').target.files[0]);
        reader.readAsDataURL(event.target.files[0]);
        $("#food_image").val('');
    }
    */
</script>
@endsection
