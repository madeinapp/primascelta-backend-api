<div class="row">

    <!-- Food Name Field -->
    <div class="form-group col-md-12">
        {!! Form::label('food_name', __('common.name')) !!}
        <i class="fas fa-info-circle" data-toggle="tooltip" data-placement="top"
            title="Puoi modificare il nome del prodotto a tuo piacimento. Nel nome incontrerai a volte il termine 'da definire'. Sostituiscilo con il nome esatto o la marca del tuo prodotto. Es. Latte da definire - bottiglia da 2lt diventerà Latte parmalat - bottiglia da 2lt"></i>
        {!! Form::text('food_name', old('food_name', $shopCatalog->food_name ?? null), ['class' => 'form-control', 'maxlength' => 255, 'required' => 'required']) !!}
        @error('food_name')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>

    <!-- Food Description Field -->
    <div class="form-group col-sm-12 col-lg-12">
        {!! Form::label('short_description', 'Puoi promuovere il tuo prodotto con max. 30 caratteri') !!}
        {!! Form::text('short_description', old('short_description', $shopCatalog->short_description ?? null), ['class' => 'form-control', 'maxlength' => 30, 'placeholder' => 'Scegli una frase ad effetto che metta in evidenza un punto di forza di questo prodotto. Es: Colti freschi stamattina!']) !!}
    </div>

    <!-- Food Description Field -->
    <div class="form-group col-sm-12 col-lg-12">
        {!! Form::label('food_description', __('common.description')) !!}
        <i class="fas fa-info-circle" data-toggle="tooltip" data-placement="top" title="Puoi modificare la descrizione del prodotto a tuo piacimento"></i>
        {!! Form::textarea('food_description', old('food_description', $shopCatalog->food_description ?? null), ['class' => 'form-control', 'rows' => 6, 'placeholder'=> 'La descrizione NON è obbligatoria. Tuttavia, nella grande maggior parte dei prodotti, è molto consigliata.']) !!}

        @if( empty($shopCatalog->food_id) )
        <button id="btn-crea-pacco-convenienza" type="button" class="btn btn-warning btn-sm" onclick="creaPacco()"><i class="fas fa-gift"></i> Aiutami a creare un Box di prodotti</button>
        @endif
    </div>
</div>

<div class="row">
    <!-- Food Image Field -->
    <div class="form-group col-12 col-sm-12 col-md-6 col-lg-6">
        {!! Form::label('fileimage', __('common.image')) !!}
        <i class="fas fa-info-circle" data-toggle="tooltip" data-placement="top"
            title="Puoi modificare l'immagine del prodotto a tuo piacimento"></i>
        <div style="clear: both"></div>

        @php

        if( !empty($shopCatalog->food_image) ){

            $foodImage = $shopCatalog->food_image;
            if( dirname($shopCatalog->food_image) == '.' ){
                $foodImage = url('storage/images/foods/'.NormalizeString($shopCatalog->foodCategory->name).'/'.$shopCatalog->food_image);
            }

            /* Verifico la presenza */
            $foodImage = getResizedImage(getOriginalImage($foodImage), '646x430');

            $textButton = 'Seleziona una nuova immagine';
        }else{
            $textButton = 'Seleziona immagine';
        }
        @endphp

        <div class="box">
            <p id="img-suggestion" class="text-primary">Puoi cambiare questa immagine in qualsiasi momento.<br/>File ammessi > .jpg - Max 10 mb - dimensioni minime di 646x430</p>
            <input type="file" name="fileimage" accept="image/jpeg" id="fileimage" class="js inputfile inputfile-1" data-multiple-caption="{count} immagine selezionata" />
            <label for="fileimage" class="lbl-fileimage"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span id="btn-fileimage">{{ $textButton }}</span></label>
        </div>

        <div style="clear: both"></div>

        @if( old('food_image', $foodImage ?? null) )
            <img id="output_image" src="{{ old('food_image', $foodImage ?? null) }}" style="width: 100%; margin-top: 10px;">
        @else
            <img id="output_image" src="/images/no-image.png" style="width: 100%; margin-top: 10px;" />
        @endif
        <input type="hidden" id="output_image_type" name="output_image_type" value="">
        <input type="hidden" id="output_image_name" name="output_image_name" value="">
        <input type="hidden" id="output_image_data" name="output_image_data" value="">

        @error('fileimage')
            <div class="alert alert-danger mt-1">{{ $message }}</div>
        @enderror

        {!! Form::hidden('food_image', null, ['id' => 'food_image']) !!}
    </div>

    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
        <div class="form-group">
            {!! Form::label('shop_catalog_specials[]', 'Caratteristiche speciali') !!}
            {!! Form::select('shop_catalog_specials[]', $specials->pluck('name', 'id'), old('food_specials[]', isset($shopCatalog) ? $shopCatalog->specials()->pluck('special_id')->toArray() ?? null : null), ['id' => 'shop_catalog_specials', 'class' => 'form-control', 'multiple' => true, 'style' => 'height: 600px']) !!}
            <small class="press-ctrl">Tenere premuto CTRL per selezionare più caratteristiche speciali</small>
        </div>
    </div>
</div>

<hr>

<div class="row">

    <!-- Food Category Id Field -->
    <div class="form-group col-md-6">
        {!! Form::label('food_category_id', __('common.food_category')) !!}
        @if( (!isset($shopCatalog) && empty(old('food_id'))) || (isset($shopCatalog) && empty($shopCatalog->food) ))
            {!! Form::select('food_category_id', $foodCategoryList, old('food_category_id', $shopCatalog->food_category_id ?? null), ['id' => 'food_category_id', 'class' => 'form-control', 'required' => 'required']) !!}
            <input type="hidden" id="defaultUom" value="{{ $defaultUom }}">
            @if( !isset($shopCatalog) )
                <input type="hidden" id="food_category_id_hidden" name="food_category_id" value="{{ old('food_category_id') }}">
                <br><span id="food_category_id_span"></span>
            @endif
        @elseif(!empty(old('food_id')) || (isset($shopCatalog) && !empty($shopCatalog->food)) )
            <br>
            {{ $foodCategoryList[old('food_category_id', $shopCatalog->food_category_id ?? null)] }}
            <input type="hidden" name="food_category_id" value="{{ old('food_category_id', $shopCatalog->food_category_id ?? null) }}" class="form-control">
        @endif
    </div>

    <!-- Food Type Field -->
    <div class="form-group col-md-6">
        {!! Form::label('food_type', __('common.food_type')) !!}
        @if( (!isset($shopCatalog) && empty(old('food_id', null))) || (isset($shopCatalog) && empty($shopCatalog->food) ))

            {!! Form::select('food_type', $foodTypeList, old('food_type', $shopCatalog->food_type ?? ''), ['class' => 'form-control', 'required' => 'required']) !!}

            {{--
            <input type="text" list="food_type_list" id="food_type" name="food_type" value="{{ old('food_type', $shopCatalog->food_type ?? '') }}" class="form-control" required/>
            <datalist id="food_type_list">
                @foreach($foodTypeList as $food_type)
                    <option>{{ $food_type }}</option>
                @endforeach
            </datalist>
             --}}

            @if( !isset($shopCatalog) )
                <input type="hidden" id="food_type_hidden" name="food_type" value="{{ old('food_type', null) }}">
                <br><span id="food_type_span"></span>
            @endif
        @elseif(!empty(old('food_id', null)) || (isset($shopCatalog) && !empty($shopCatalog->food)) )
            <br>
            {{ old('food_type', $shopCatalog->food_type ?? null) }}
            <input type="hidden" name="food_type" value="{{  old('food_type', $shopCatalog->food_type ?? null) }}">
        @endif
    </div>
</div>

<hr>
<div class="row">
<p style="text-align: center;margin: 0 auto;color: blue;">Scopri come scegliere correttamente i generi e le tipologie dei tuoi prodotti.<br>
    <a style="color: blue;font-weight:bold" href="/images/TUTORIAL_GENERI.pdf" target="_blank">Guarda gli esempi</a>
    </p>
</div>

<section class="my-4 p-4 rounded" style="background-color: lightblue">
    <div class="row my-2 p-2 rounded" style="border: 2px solid #fff">
        <!-- Food Price Field -->
        <div class="form-group col-md-12 col-lg-4">
            <label for="food_price">Prezzo <i style="display: inline-block" class="fas fa-info-circle" data-toggle="tooltip" data-placement="top"
                title="Il prezzo si riferisce alla quantità e all'unità di riferimento. Es: In Filetto 12€ / 1Kg indica il valore 12"></i></label>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">&euro;</span>
                </div>
                {!! Form::number('food_price', old('food_price', $shopCatalog->food_price ?? null), ['id' => 'food_price', 'class' => 'form-control', 'required' => 'required', 'min' => 0.01, 'max' => 999999.99, 'step' => 'any']) !!}
            </div>
            @error('food_price')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <!-- Food Unit Of Measure Quantity Field -->
        <div class="form-group col-md-12 col-lg-4">
            <label for="food_unit_of_measure_quantity">Quantità di riferimento del prezzo <i class="fas fa-info-circle" data-toggle="tooltip" data-placement="top"
                title="Indica la quantità che intendiamo legare al prezzo e all'unità di riferimento del prezzo stesso. Es: In Filetto 12€ / 1Kg indica il valore 1"></i></label>
            {!! Form::number('food_unit_of_measure_quantity', old('food_unit_of_measure_quantity', $shopCatalog->food_unit_of_measure_quantity ?? 1), ['min' => 1, 'class' => 'form-control', 'required' => 'required']) !!}
        </div>

        <!-- Unit Of Measure Id Field -->
        <div class="form-group col-md-12 col-lg-4">
            <label for="unit_of_measure_id">Unità di riferimento del prezzo <i class="fas fa-info-circle" data-toggle="tooltip" data-placement="top"
                title="Indica l'unità di misura che intendiamo legare al prezzo e alla quantità di riferimento del prezzo stesso. Es: In Filetto 12€ / 1Kg indica Kg"></i></label>
            {!! Form::select('unit_of_measure_id', $unitOfMeasureList, old('unit_of_measure_id', $shopCatalog->unit_of_measure_id ?? null), ['id' => 'unit_of_measure_id', 'class' => 'form-control', 'required' => 'required']) !!}
        </div>
    </div>

    <div class="row my-2 ">
        <div class="col-lg-4"></div>
        <div class="col-lg-8 p-2 rounded" style="border: 2px solid #fff">
            <div class="row">
                <!-- Food Unit Of Measure Quantity Field -->
                <div class="form-group col-md-12 col-lg-6">
                    <label for="unit_of_measure_buy_quantity">
                        Quantità minima e multipla d'acquisto
                        <i class="fas fa-info-circle" data-toggle="tooltip" data-placement="top"
                        title="Indica allo stesso tempo il minimo e il multiplo che il cliente potrà acquistare.
                        Es: decido che del mio Filetto
                        che vendo a 12€ 1kg il cliente non ne può ordinare meno di 2 hg e allo stesso tempo il suo ordine
                        può essere variato di 2hg alla volta."></i>
                    </label>

                    {!! Form::number('unit_of_measure_buy_quantity', old('unit_of_measure_buy_quantity', $shopCatalog->unit_of_measure_buy_quantity ?? 1), ['min' => 1, 'class' => 'form-control', 'required' => 'required']) !!}
                </div>

                <!-- Unit Of Measure Id Field -->
                <div class="form-group col-md-12 col-lg-6">
                    <label for="unit_of_measure_buy">
                        Unità di misura per l'acquisto
                        <i class="fas fa-info-circle" data-toggle="tooltip" data-placement="top"
                        title="Unità di riferimento per la quantità multipla/minima di acquisto. Indica l'unità di riferimento
                        che intendiamo legare alla quantità multipla/minima. Es: decido che del mio Filetto che vendo a
                        12€ / 1kg il cliente non ne può ordinare meno di 2 hg per volta e il cliente non ne può ordinare
                        meno di 2 hg"></i>
                    </label>
                    {!! Form::select('unit_of_measure_buy', $relatedUoms, old('unit_of_measure_buy', $shopCatalog->unit_of_measure_buy ?? null), ['id' => 'unit_of_measure_buy', 'class' => 'form-control', 'required' => 'required']) !!}
                </div>
            </div>
        </div>
    </div>
</section>

<div class="row">

    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
        <!-- Visible In Search Field -->
        <div class="form-group col-md-12">
            <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                <input type="checkbox" class="custom-control-input" name="visible_in_search" id="visible_in_search"
                    value="1" @if( ( isset($shopCatalog) && $shopCatalog->visible_in_search ) || !isset($shopCatalog) )
                checked
                @endif

                >
                {!! Form::label('visible_in_search', 'Attivo', ['class' => 'custom-control-label'])
                !!}
                <i class="fas fa-info-circle" data-toggle="tooltip" data-placement="top"
                    title="Potreste desiderare rendere momentaneamente non visibile il prodotto perchè non disponibile in magazzino"></i>
            </div>
        </div>

        <!-- Home delivery -->

        <div class="form-group col-md-12">
            <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                <input type="checkbox" class="custom-control-input" name="home_delivery" id="home_delivery" value="1"
                @if( ( isset($shopCatalog) && $shopCatalog->home_delivery ) || !isset($shopCatalog) )
                checked
                @endif
                >
                <label for="home_delivery" class="custom-control-label">
                    {{ __('common.home_delivery_product') }}
                    <i class="fas fa-info-circle" data-toggle="tooltip" data-placement="top"
                    title="Potete decidere che, nonostante voi facciate consegne a domicilio, questo podotto fosse acquistabile solo in sede"></i>
                </label>
            </div>
        </div>


        <!-- Home delivery -->
        <div class="form-group col-md-12">
            <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                <input type="checkbox" class="custom-control-input" name="in_app_shopping" id="in_app_shopping" value="1"
                    @if( ( isset($shopCatalog) && $shopCatalog->in_app_shopping ) || !isset($shopCatalog) )
                checked
                @endif
                >
                <label for="in_app_shopping" class="custom-control-label">
                    {{ __('common.in_app_shopping') }}
                    <i class="fas fa-info-circle" data-toggle="tooltip" data-placement="top"
                    title="Potete decidere che questo podotto non sia prenotabile. In questo caso il cliente saprà che potrà scoprirne la disponibilità solo recandosi in sede"></i>
                </label>
            </div>
        </div>
    </div>

    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
        <div class="form-group col-md-12">
            <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                <input type="checkbox" class="custom-control-input" name="uncertain_weight" id="uncertain_weight" value="1"
                    @if( isset($shopCatalog) && $shopCatalog->uncertain_weight )
                checked
                @endif
                >
                <label for="uncertain_weight" class="custom-control-label">
                    Questo prodotto prevede una unità/pezzatura minima, pertanto il peso da  voi ordinato risulterà puramente indicativo
                    <i class="fas fa-info-circle" data-toggle="tooltip" data-placement="top"
                    title="Il cliente verrà avvisato con questa dicitura all'interno della scheda prodotto. Si consiglia di spuntarla quando il prodotto in questione viene venduto a peso ma prevede delle unità singole indivisibili. Es.: un cocomero, una fettina panata, un cavolo etc, etc."></i>
                </label>
            </div>
        </div>
    </div>
</div>

<div class="row">
    {!! Form::hidden('food_id', null, ['id' => 'food_id']) !!}

    @if( isset($shopCatalog) )
    {!! Form::hidden('shop_catalog_id', $shopCatalog->id) !!}
    @endif

</div>
