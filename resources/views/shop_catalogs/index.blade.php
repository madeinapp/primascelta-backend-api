@extends('adminlte::page')

@section('content')
	<div class="modal fade" id="modal-danger" style="display: none;" aria-hidden="true">
		<div class="modal-dialog">
		  <div class="modal-content bg-danger">
			<div class="modal-header">
			  <h4 class="modal-title">Attenzione!</h4>
			  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">×</span>
			  </button>
			</div>
			<div class="modal-body">
			  Hai inserito {{ Auth::user()->shop->shopCatalogs->count() }} prodotti. <strong>Hai esaurito il numero di prodotti previsti da questo abbonamento per il tuo catalogo. Per inserire un altro prodotto puoi cancellarne uno di quelli esisteni o passare ad un abbonamento superiore.</strong>
			  <p><a href="https://backend.primascelta.biz/prices" target="_blank"><strong><u>Desidero vedere i piani di abbonamento disponibili</u></strong></a></p>
			</div>
			<div class="modal-footer justify-content-between">
			  <button type="button" class="btn btn-outline-light" data-dismiss="modal">Chiudi</button>
			  <!--button type="button" class="btn btn-outline-light">Save changes</button-->
			</div>
		  </div>
		  <!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
    <div class="content padding-bottom-plus">

        <section class="content-header">
            <h1 class="pull-left" style="display: inline-block">Il mio catalogo</h1>
            <p>
                Hai inserito {{ Auth::user()->shop->shopCatalogs->count() }} prodotti.
                @can('add_to_catalog', Auth::user())
                Ne puoi inserire ancora {{ Auth::user()->shop->products_left }}.
				@else
				Hai inserito {{ Auth::user()->shop->shopCatalogs->count() }} prodotti. <strong>Hai esaurito il numero di prodotti previsti da questo abbonamento per il tuo catalogo. Per inserire un altro prodotto puoi cancellarne uno di quelli esisteni o passare ad un abbonamento superiore.</strong>
                @endcan
            </p>
        </section>

        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="card card-primary">
            @include('shop_catalogs.table')
        </div>

    </div>
@endsection

