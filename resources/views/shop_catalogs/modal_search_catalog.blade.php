<div class="modal fade" id="modal-search-catalog" aria-hidden="false" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-success">
                <div class="row">
                    <div class="col-11">
                        <h4 class="modal-title"><i class="fas fa-gift"></i> Crea Box di prodotti</h4>
                        Per inserire comodamente nella descrizione tutti i prodotti che compongono questo box ti basterà aggiungerli uno ad uno.
                        Potrai poi modificare tutto o in parte l'elenco ma anche togliere i prezzi o aggiungere altro testo a tuo piacimento.
                    </div>
                    <div class="col-1">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <form id="frm-search-catalog" action="">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                {!! Form::label('search_catalog_name', 'Cerca un prodotto') !!}
                                {!! Form::text('search_catalog_name', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                {!! Form::label('search_catalog_category', 'Genere') !!}
                                {!! Form::select('search_catalog_category', $catalogCategoryList, null,  ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                {!! Form::label('search_catalog_type', 'Categoria') !!}
                                {!! Form::select('search_catalog_type', [], null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>
                </form>
                <div class="compact-mode-container text-right mb-2" style="display: none;">
                    <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                        <input type="checkbox" class="custom-control-input" id="compact_mode">
                        {!! Form::label('compact_mode', 'Visualizzazione compatta', ['class' => 'custom-control-label']) !!}
                    </div>
                </div>
                <div style="max-height: calc(100vh - 460px); overflow: auto;">
                    <div class="table-responsive">
                        <table class="table">
                            <tbody id="catalog_search_results"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
