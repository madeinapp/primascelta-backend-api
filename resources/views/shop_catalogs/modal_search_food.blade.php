<div class="modal fade" id="modal-search-food" aria-hidden="false" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-success">
                <h4 class="modal-title">Cerca nel nostro catalogo</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body modal-body-header">
                <img src="/images/scegli_catalogo.png" width="100" align="left">Siamo gli unici a offrirti un catalogo pronto di <b>{{ $totFoods }} prodotti</b> con foto e descrizione. Dovrai solo scegliere quelli che vendi e indicare il prezzo. Potrai poi personalizzarli come vuoi. <strong>Effettua la ricerca scrivendo il nome del prodotto al singolare, al plurale e usa la parola chiave.</strong>
            </div>
            <div class="modal-body">
                <form id="frm-search-food" action="">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                {!! Form::label('search_food_name', 'Cerca un prodotto') !!}
                                {!! Form::text('search_food_name', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                {!! Form::label('search_food_category', 'Genere') !!}
                                {!! Form::select('search_food_category', $foodCategoryList, null,  ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                {!! Form::label('search_food_type', 'Categoria') !!}
                                {!! Form::select('search_food_type', [], null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>
                </form>
                <div class="compact-mode-container text-right mb-2" style="display: none;">
                    <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                        <input type="checkbox" class="custom-control-input" id="compact_mode">
                        {!! Form::label('compact_mode', 'Visualizzazione compatta', ['class' => 'custom-control-label']) !!}
                    </div>
                </div>
                <div style="max-height: calc(100vh - 410px); overflow: auto;">
                    <div class="table-responsive">
                        <table class="table">
                            <tbody id="food_search_results"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
