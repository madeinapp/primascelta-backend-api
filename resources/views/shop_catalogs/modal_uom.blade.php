<div class="modal fade" id="modal-uom" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-secondary">
                <h4 class="modal-title">Come scegliere l'unità di misura giusta</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body bg-secondary text-center p-4">

                <h1 class="m-0"><i class="far fa-hand-point-down" style="color: gold"></i></i></h1>

                Scegli l'unità di misura tenendo conto delle possibilità di acquisto che vuoi dare ai tuoi clienti
                in merito alla quantità di prodotto da inserire nel carrello.
                <br>
                Es:
                <i>
                    Se intendi dare la possibilità di acquistare 1,6 Kg di carne come unità dovrai scegliere Hg
                    in modo tale che il tuo cliente potrà comporre il suo carrello con 16 Hg fino ad arrivare a 1,6 Kg.
                    <br>
                    Se invece scegliessi come unità di misura Kg i tuoi clienti potrebbero acquistare solo 1 Kg, 2 Kg, 3 Kg e così via.
                </i>

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
