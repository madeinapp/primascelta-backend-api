@extends('adminlte::page')

@section('content')
    <section class="content-header">
        <h1>
            {{ $shopCatalog->shop->name }}
        </h1>
    </section>
    <div class="content">
        <div class="card card-primary">
            <div class="card-body">
                <div class="row" style="padding-left: 20px">
                    @include('shop_catalogs.show_fields')
                </div>
            </div>
        </div>

        <div class="form-group mb-2">
            <a href="{{ route('shopCatalogs.index') }}" class="btn btn-default">Indietro</a>
        </div>
    </div>
@endsection
