    <!-- Food Image Field -->
    <div class="form-group col-12 col-sm-12 col-md-6 col-lg-6 text-center">
        @if( !empty($shopCatalog->short_description) )
        <span class="alert alert-warning p-1">
            <i>{{ $shopCatalog->short_description }}</i>
        </span>
        @endif
        <div style="clear: both"></div>
        <img src="{{ $foodImage }}" style="width: 100%; margin-top: 10px;"><br />
    </div>

    <div class="col-12 col-sm-12 col-md-6 col-lg-6 p4">
        <div class="row">
            <div class="col-12" style="background-color: #eee; border-radius: 20px; padding: 30px;">

                <div class="row">
                    <div class="col-12">
                        <span><small><i>Nome prodotto</i></small></span>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <h3 class="font-weight-bold">{{ $shopCatalog->food_name }}</h3>
                        {{ $shopCatalog->food_description }}
                    </div>
                </div>

                <hr>

                <div class="row mt-4">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                        <div class="row m-0 p-0">
                            <div class="col-8 m-0 p-0">Genere di prodotto</div>
                            <div class="col-4 m-0 p-0"><strong>{{ $shopCatalog->foodCategory->name }}</strong></div>
                        </div>
                        <div class="row m-0 p-0">
                            <div class="col-8 m-0 p-0">Tipologia di prodotto</div>
                            <div class="col-4 m-0 p-0"><strong>{{ $shopCatalog->food_type }}</strong></div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 mt-2 mb-0 text-right">
                        <div class="row p-0 m-0">
                            <div class="col-12 p-0 m-0">Prezzo</div>
                        </div>
                        <div class="row p-0 m-0">
                            <div class="col-12 p-0 m-0" style="font-size: 1.5em; font-weight: 600">
                                {{ number_format($shopCatalog->food_price, 2, ',', '.') }} &euro; /
                                @if( $shopCatalog->food_unit_of_measure_quantity > 1 )
                                {{ $shopCatalog->food_unit_of_measure_quantity }}
                                @endif
                                {{ $shopCatalog->unitOfMeasure->name }}
                            </div>
                        </div>
                        <div class="row p-0 m-0">
                            <div class="col-12 p-0 m-0">
                                <small>Quantità minima di acquisto: <strong><strong>{{ $shopCatalog->unit_of_measure_buy_quantity }}{{ $shopCatalog->unitOfMeasureBuy->name }}</strong></small>
                            </div>
                        </div>
                    </div>
                </div>

                <hr>

                @if( $shopCatalog->specials()->count() > 0 )
                <div class="row mt-4">
                    <div class="col-12">
                        <span><strong>Caratteristiche speciali</strong></span>
                        <p style="font-weight: 100">
                            {{ implode(" / ", $shopCatalog->specials->pluck('name')->toArray()) }}
                        </p>
                    </div>
                </div>
                @endif

                <div class="row my-1">

                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 text-center">

                        @if( $shopCatalog->visible_in_search )
                        <small><span class="text-success px-2" style="padding: 4px">Attivo</span></small>
                        @else
                        <small><span class="text-danger px-2" style="padding: 4px">Non attivo</span></small>
                        @endif

                        @if( $shopCatalog->home_delivery )
                        <small><span class="text-success px-2" style="padding: 4px">Consegnabile a domicilio</span></small>
                        @else
                        <small><span class="text-danger px-2" style="padding: 4px">Non consegnabile a domicilio</span></small>
                        @endif

                        @if( $shopCatalog->in_app_shopping )
                        <small><span class="text-success px-2" style="padding: 4px">Acquistabile dall'app</span></small>
                        @else
                        <small><span class="text-danger px-2" style="padding: 4px">Non acquistabile dall'app</span></small>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
