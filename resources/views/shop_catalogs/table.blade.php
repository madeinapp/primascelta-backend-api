<div class="card-header m-0 p-0">
    <div class="row m-0 p-2 bg-success">
        <div class="col-sm-12 col-md-4">
            <label>Cerca per genere</label>
            {!! Form::select('genere', $generi, null, ['id' => 'food_category', 'class' => 'form-control']) !!}
        </div>
        <div class="col-sm-12 col-md-4">
            <label>Cerca per tipologia</label>
            {!! Form::select('tipologia', $tipologie, null, ['id' => 'food_type', 'class' => 'form-control']) !!}
        </div>
        <div class="col-sm-12 col-md-4">
            <label>Cerca per parola chiave:</label>
            <input type="search" class="form-control " placeholder="cerca per parola chiave" aria-controls="agents-table" name="searchname" id="searchname">
        </div>
    </div>
</div>
<div class="card-body">
    @can('add_to_catalog', Auth::user())
        <a class="btn btn-block btn-lg btn-add btn-add-catalog bg-success d-none d-sm-none d-md-block mb-4" style="display:flex;align-items:center;" href="{{ route('shopCatalogs.create') }}"><span style="width:100%">+</span></a>
    @else
        @if( Auth::user()->shop && Auth::user()->shop->products_left <= 0)
        	<a class="btn btn-block btn-lg btn-add btn-add-catalog bg-success d-none d-sm-none d-md-block mb-4" style="display:flex;align-items:center;" data-toggle="modal" data-target="#modal-danger" href="#"><span style="width:100%">+</span></a>
        @endif

        @can('subscription_expired', Auth::user())
            <div class="text-center my-4">
                <a href="/prices" class="text-danger" style="font-weight: bold">
                    IL TUO ABBONAMENTO RISULTA SCADUTO<br/>
                    PER AGGIUNGERE UN NUOVO PRODOTTO RINNOVA ORA. VEDI I PIANO DI ABBONAMENTO DISPONIBILI
                </a>
            </div>
        @endif
    @endcan

    @if( $tot_catalog == 0 )
        <p class="text-center text-success font-weight-bold">
            AL MOMENTO NON HAI PRODOTTI NEL CATALOGO
        </p>
    @else
        <table class="table table-hover shop-catalogs-table" id="shopCatalogs-table">
            <thead>
                <tr>
                    <th style="text-align:center;text-indent:20px">Azioni</th>
                    <th>{{ __('common.image') }}</th>
                    <th>{{ __('common.name') }}</th>
                    <th>{{ __('common.food_category') }}</th>
                    <th>{{ __('common.price') }}</th>
                    <th>Vis</th>
                    <th>Food price</th>
                </tr>
            </thead>
        </table>
    @endif

    @can('add_to_catalog', Auth::user())
        <a class="btn btn-block btn-lg btn-add btn-add-catalog bg-success mb-4" style="align-items:center;" href="{{ route('shopCatalogs.create') }}"><span style="width:100%">+</span></a>
    @else
        @if( Auth::user()->shop && Auth::user()->shop->products_left <= 0)
        	<a class="btn btn-block btn-lg btn-add btn-add-catalog bg-success d-none d-sm-none d-md-block mb-4" style="display:flex;align-items:center;" data-toggle="modal" data-target="#modal-danger" href="#"><span style="width:100%">+</span></a>
        @endif

        @can('subscription_expired', Auth::user())
            <div class="text-center my-4">
                <a href="/prices" class="text-danger" style="font-weight: bold">
                    IL TUO ABBONAMENTO RISULTA SCADUTO<br/>
                    PER AGGIUNGERE UN NUOVO PRODOTTO RINNOVA ORA. VEDI I PIANO DI ABBONAMENTO DISPONIBILI
                </a>
            </div>
        @endif
    @endcan
</div>

@section('js')
<script>

var intervalSearch = null;
var table = null;

$(function () {

    table = $('#shopCatalogs-table').DataTable({
        language: {
            url: "{{ url('js/Italian.json') }}"
        },
        dom: "<'row'<'col-sm-12 col-md-4'<'row'<'col-12 col-sm-12 col-md-6 text-md-left text-center mb-2 mb-sm-2 mb-md-0'<'multiselect pl-2'>><'col-12 col-sm-12 col-md-6 text-md-left text-center mb-4 mb-sm-4 mb-md-0'<'options-multiselect'>>>><'col-sm-12 col-md-4'l><'col-sm-12 col-md-4'p>r>"+
            "<'row'<'col-sm-12't>>"+
            "<'row'<'col-sm-12 col-md-6'i><'col-sm-12 col-md-6'p>>",
        processing: true,
        serverSide: true,
        stateSave: true,
        "initComplete": function (settings, json) {
            $("#shopCatalogs-table").wrap("<div style='overflow:auto; width:100%;position:relative;'></div>");
        },
        //paginate: true,
        //pageLength: 10,
        //ajax: '{{ route('foods.index') }}',
        ajax: {
            url: `{{ route('shopCatalogs.index') }}`,
            data: function (d) {
                d.searchname = $('#searchname').val(),
                d.genere = $('select[name=genere]').val(),
                d.tipologia = $('select[name=tipologia]').val()
            }
        },
        "columnDefs": [
            { className: "dt-body-nowrap", "targets": -1 }
        ],
        columns: [
            // {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            //{data: 'active', name: 'active', "searchable": false,},
            {data: 'action', name: 'action', orderable: true,  orderData: 6, "searchable": false},
            {data: 'image', name: 'image', orderable: false, "searchable": false},
            {data: 'food_name', name: 'c.food_name'},
            {data: 'category', name: 'food_category', "searchable": false},
            {data: 'prezzo', name: 'prezzo', orderData: 7},
            {data: 'visible_in_search', name: 'c.visible_in_search', visible: false},
            {data: 'food_price', name: 'c.food_price', visible: false}
        ]
    });

    table.on( 'draw.dt', function () {
        $("div.multiselect").html('<button class="btn btn-link btn-sm" onclick="multiSelection()" style="color: blue">Selezione multipla</a>');
    } );

    $("#searchname").keyup(function(){
        if( intervalSearch !== null ){
            clearInterval(intervalSearch);
        }

        intervalSearch = setInterval(function(){
            clearInterval(intervalSearch);
            search();
        }, 500, table);
    });

    $("select[name=genere]").change(function(){
        $("#food_type").val("");
        table.draw();
        loadFoodType($(this).val(), 'food_type');
    });

    $("select[name=tipologia]").change(function(){
        table.draw();
    });

});

multiSelection = () => {
    //$("div.actions").hide();
    $(".chk_del_shop_catalog").css('display', 'inline-block');
    $("div.multiselect").html('<input type="checkbox" class="form-control" style="width: 1.5em; display: inline-block; vertical-align: middle;" id="select_all"><button class="btn btn-link text-danger btn-sm" onclick="cancelMultiselection()" style="color: #000">Annulla selezione multipla</a>');
    selectAllEvent();
    selectEvent();
}

cancelMultiselection = () => {
    $(".options-multiselect").html('')
    $(".chk_del_shop_catalog").hide();
    //$("div.actions").show();
    $("div.multiselect").html('<button class="btn btn-link btn-sm" onclick="multiSelection()" style="color: blue">Selezione multipla</a>');
}

selectAllEvent = () => {
    $("#select_all").on("change", function(e){
        console.log("$(this).is(':checked')", $(this).is(':checked'));
        if( $(this).is(':checked') ){
            $(".chk_del_shop_catalog").prop("checked", true);
            $(".options-multiselect").html(`<button class="btn btn-sm btn-danger" onclick="delAllSelected()" title="ELIMINA TUTTI I PRODOTTI SELEZIONATI"><i class="fa fa-trash"></i></button>
                                            <button class="btn btn-sm btn-success" onclick="activeAllSelected()" title="ATTIVA TUTTI I PRODOTTI SELEZIONATI"><i class="fa fa-check"></i></button>
                                            <button class="btn btn-sm btn-danger" onclick="disableAllSelected()" title="DISABILITA TUTTI I PRODOTTI SELEZIONATI"><i class="fas fa-ban"></i></button>`)
        }else{
            $(".chk_del_shop_catalog").prop("checked", false);
            $(".options-multiselect").html('')
        }
    });
}

selectEvent = () => {

    $(".chk_del_shop_catalog").on('change', function(){

        let show_butttons = false;

        $(".chk_del_shop_catalog").each(function(k, val){
            if( $(this).is(":checked") ){
                show_butttons = true;
            }
        });

        if( show_butttons ){
            $(".options-multiselect").html(`<button class="btn btn-sm btn-danger" onclick="delAllSelected()" title="ELIMINA TUTTI I PRODOTTI SELEZIONATI"><i class="fa fa-trash"></i></button>
                                            <button class="btn btn-sm btn-success" onclick="activeAllSelected()" title="ATTIVA TUTTI I PRODOTTI SELEZIONATI"><i class="fa fa-check"></i></button>
                                            <button class="btn btn-sm btn-danger" onclick="disableAllSelected()" title="DISABILITA TUTTI I PRODOTTI SELEZIONATI"><i class="fas fa-ban"></i></button>`)
        }else{
            $(".options-multiselect").html('')
        }

    });
}

search = () => {
    table.search( $("#searchname").val() ).draw();
}

loadFoodType = (food_category_id, element_id, sel_value = null) => {

    $('#'+element_id).empty();

    if( food_category_id != '' ){

        $.ajax({
            url: '/shop/{{ Auth::user()->shop->id }}/food_category/'+food_category_id+'/types',
            data: {
                "_token": "{{ csrf_token() }}"
            },
            type: 'GET',
            dataType: 'json',
            statusCode: {
                401: function() {
                    document.location.href = '/login';
                },
                419: function() {
                    document.location.href = '/login';
                }
            },
            success: function(res){
                var element = document.getElementById(element_id);
                var option = document.createElement("option");
                option.text = 'Cerca in tutte le tipologie';
                option.value = '';
                element.add(option);

                $("#tot_tipologie").text(res.count);

                for(var i=0; i<res.count; i++){
                    option = document.createElement("option");
                    option.text = res['food_types'][i];
                    option.value = res['food_types'][i];
                    element.add(option);

                    if( sel_value ){
                        if( option.text === sel_value ){
                            element.selectedIndex = (i+1);
                        }
                    }

                }
            }
        });

    }else{

        var element = document.getElementById(element_id);
        var option = document.createElement("option");
        option.text = 'Cerca in tutte le tipologie';
        option.value = '';
        element.add(option);

        $("#tot_tipologie").text(0);

    }
}

getSelected = () => {
    let arr_selected = [];

    $(".chk_del_shop_catalog").each(function(k, val){
        if( $(this).is(':checked') ){

            arr_selected.push($(this).val());

        }
    });

    return arr_selected;
}

delAllSelected = (shop_catalog_id = null) => {

    let msg = 'Intendi eliminare definitivamente tutti i prodotti selezionati?';
    if( shop_catalog_id !== null ){
        msg = 'Intendi eliminare definitivamente il prodotto selezionato?'
    }

    showConfirm("Elimina prodotto", 'bg-danger', msg, execDelAllSelected, [shop_catalog_id]);

}

execDelAllSelected = (shop_catalog_id) => {

    let arr_selected = [];
    if( shop_catalog_id === null ){
        arr_selected = getSelected();
    }else{
        arr_selected.push(shop_catalog_id);
    }

    console.log("arr_selected", arr_selected);

    $.ajax({
        url: '/shopCatalogs/multi/delete',
        data: {
            "_token": "{{ csrf_token() }}",
            selected: arr_selected,
        },
        type: 'DELETE',
        dataType: 'json',
        statusCode: {
            401: function() {
                document.location.href = '/login';
            },
            419: function() {
                document.location.href = '/login';
            }
        },
        success: function(data){
            if( data.status === 'ok' ){
                if( data.errors !== '' ){
                    showInfo("Elimina prodotto", 'bg-danger', data.errors);
                }

                if( (arr_selected.length === 1 && data.errors === '') || arr_selected.length > 1 ){
                    document.location.reload();
                }
            }
        }
    });

}

activeAllSelected = () => {
    showConfirm("Catalogo", 'bg-danger', "Intendi attivare tutti i prodotti selezionati?", execActiveAllSelected, []);
}

execActiveAllSelected = () => {
    let arr_selected = getSelected();

    $.ajax({
        url: '/shopCatalogs/multi/activate',
        data: {
            "_token": "{{ csrf_token() }}",
            selected: arr_selected,
        },
        type: 'POST',
        dataType: 'json',
        statusCode: {
            401: function() {
                document.location.href = '/login';
            },
            419: function() {
                document.location.href = '/login';
            }
        },
        success: function(data){
            if( data.status === 'ok' ){
                document.location.reload();
            }
        }
    });
}

disableAllSelected = () => {
    showConfirm("Catalogo", 'bg-danger', "Intendi disabilitare tutti i prodotti selezionati?", execDisableAllSelected, []);
}

execDisableAllSelected = () => {
    let arr_selected = getSelected();

    $.ajax({
        url: '/shopCatalogs/multi/disable',
        data: {
            "_token": "{{ csrf_token() }}",
            selected: arr_selected,
        },
        type: 'POST',
        dataType: 'json',
        statusCode: {
            401: function() {
                document.location.href = '/login';
            },
            419: function() {
                document.location.href = '/login';
            }
        },
        success: function(data){
            if( data.status === 'ok' ){
                document.location.reload();
            }
        }
    });
}
</script>
@endsection
