@extends('adminlte::page')

@section('content')
    <section class="content-header">
        <h1>
            Nuova Offerta
        </h1>
    </section>
    <div class="content">

        @if ($errors->any())
        <div class="alert alert-danger">
            @if( $errors->count() === 1 )
            Si è verificato {{ $errors->count() }} errore
            @else
            Si sono verificati {{ $errors->count() }} errori
            @endif
       </div>
       @endif

       {{--
       @if ($errors->any())
       {{ dump($errors) }}
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $k => $error)
                    <li>{{ $k  }} - {{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
         --}}

        <div class="card card-primary">
            <div class="card-header">
                <img src="/images/crea_offerta.png" width="100" align="left">Puoi mettere in offerta un singolo prodotto oppure selezionare più prodotti, nel secondo caso ti proporremo la foto del "pacco offerta". Potrai stabilire il prezzo scontato del prodotto, una sua quantità minima di acquisto e la durata dell'offerta
            </div>
            <div class="card-header bg-secondary" style="border-top-left-radius: 0; border-top-right-radius: 0;">
                <div class="row mt-1 mb-0">
                    <div class="col-12 pt-2 pb-2 text-center text-light">
                        Riempi tutti i campi e personalizza l'offerta a tuo piacimento. Puoi scegliere il prezzo scontato e ti calcoliamo lo sconto in automatico. O viceversa
                    </div>
                </div>
                <div class="row mt-0 mb-2">
                    <div class="col-12 text-center">
                        <button class="btn btn-light" onclick="showModal()" style="margin: 0 auto; display: block"><i class="fa fa-plus"></i> Aggiungi un prodotto</button>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="card-body">

                    {!! Form::open(['route' => 'shopOffers.store']) !!}

                        @include('shop_offers.fields')

                        {!! Form::submit('Salva come bozza', ['name' => 'save', 'class' => 'btn btn-success btn-full']) !!}

                        {!! Form::submit('Attiva subito', ['name' => 'save_available', 'class' => 'btn btn-primary btn-full']) !!}

                        <a href="{{ route('shopOffers.index') }}" class="btn btn-link btn-full">Annulla</a>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    @include('shop_offers.modal_search_catalog')
@endsection

@section('js')
    <script>

        var interval_name , interval_category, interval_type = null;
        var catalog_data = [];
        var selected_products = [];

        showModal = () => {
            catalog_data = [];
            $("#catalog_search_results").html('');
            $("#frm-search-catalog")[0].reset();
            $('#search_catalog_type').empty();
            $("#modal-search-catalog").modal('show');
        }

        $(document).ready(function(){

            $("#start_date").datetimepicker({
                locale: 'it',
                format: "DD/MM/YYYY",
                ignoreReadonly: true
            });

            $("#end_date").datetimepicker({
                locale: 'it',
                format: "DD/MM/YYYY",
                ignoreReadonly: true
            });


            $("#frm-search-catalog").on("submit", function(e){
                e.preventDefault();
            });

            $("#search_catalog_name").on('keyup', function(){
                clearInterval(interval_name);
                interval_name = setInterval(function(){
                    clearInterval(interval_name);

                    var product_name = $("#search_catalog_name").val();

                    if( product_name.length === 0 || product_name.length >= 3 ){
                        searchCatalog();
                    }
                }, 500);
            });

            $("#search_catalog_category").on('change', function(){
                loadFoodType($(this).val(), 'search_catalog_type');
                searchCatalog();
            });

            $("#search_catalog_type").on('change', function(){
                searchCatalog();
            });


            $("#catalog_category_id").on('change', function(){
                loadCatalogType($(this).val(), 'catalog_type');
            });

            discountEvents();

            $("#compact_mode").on('click', function(){
                if( $(this).is(':checked') ){
                    $(".food-description").hide();
                    $(".food-img").hide();
                    $(".food-type").hide();
                }else{
                    $(".food-description").show();
                    $(".food-img").show();
                    $(".food-type").show();
                }
            });

            $("#modal-search-catalog").on("hidden.bs.modal", function () {
                $("#compact_mode").prop("checked", false);
                $(".compact-mode-container").hide();
            });

        });

        discountEvents = () => {
            $(".discount").on('keyup', function(e){
                var id = $(this).attr('id');
                var discount = $(this).val();
                getDiscountedPrice(id, discount);
            });

            $(".discount").on('change', function(e){
                var id = $(this).attr('id');
                var discount = $(this).val();
                getDiscountedPrice(id, discount);
            });

            $(".discounted-price").on('keyup', function(e){
                var id = $(this).attr('id');
                var discounted_price = $(this).val();
                getDiscount(id, discounted_price);
            });

            $(".discounted-price").on('change', function(e){
                var id = $(this).attr('id');
                var discounted_price = $(this).val();
                getDiscount(id, discounted_price);
            });
        }

        getDiscountedPrice = (id, discount) => {
            if( discount !== '' ) discount.replace(',','.');
            var shopCatalog_id = id.split('_')[1];
            var food_price = $("#food_price_"+shopCatalog_id).val();
            if( typeof(food_price) === 'undefined' )
            {
                var food_price = '0,00';
            }

            var discounted_price = parseFloat(food_price.replace(',','.')) - ( parseFloat(food_price.replace(',','.')) * parseFloat(discount) / 100);
            console.log(discounted_price);
            $("#discounted-price_"+shopCatalog_id).val(isNaN(discounted_price) ? '' : discounted_price.toFixed(2).toString());
        }

        getDiscount = (id, discounted_price) => {
            if( discounted_price !== '' ) discounted_price.replace(',','.');
            var shopCatalog_id = id.split('_')[1];
            var food_price = $("#food_price_"+shopCatalog_id).val();
            if( typeof(food_price) === 'undefined' )
            {
                var food_price = '0,00';
            }
            var discount = ( ( parseFloat(food_price) - parseFloat(discounted_price) ) * 100 ) / parseFloat(food_price);
            $("#discount_"+shopCatalog_id).val(isNaN(discount) ? '' : discount.toFixed(2).toString());
        }

        loadFoodType = (food_category_id, element_id, sel_value = null) => {

            $('#'+element_id).empty();

            if( food_category_id != '' ){

                $.ajax({
                    url: '/shopCatalogs/type-list/'+food_category_id,
                    data: {
                        "_token": "{{ csrf_token() }}",
                    },
                    type: 'GET',
                    dataType: 'json',
                    statusCode: {
                        401: function() {
                            document.location.href = '/login';
                        },
                        419: function() {
                            document.location.href = '/login';
                        }
                    },
                    success: function(res){
                        var element = document.getElementById(element_id);
                        var option = document.createElement("option");
                        option.text = 'Seleziona...';
                        option.value = '';
                        element.add(option);
                        for(var i=0; i<res.count; i++){
                            option = document.createElement("option");
                            option.text = res['Food Type List'][i];
                            option.value = res['Food Type List'][i];
                            element.add(option);

                            if( sel_value ){
                                if( option.text === sel_value ){
                                    element.selectedIndex = (i+1);
                                }
                            }

                        }
                    }
                });

            }else{

                var element = document.getElementById(element_id);
                var option = document.createElement("option");
                option.text = 'Seleziona...';
                option.value = '';
                element.add(option);

            }
        }

        searchCatalog = () => {
            var catalog_name = $("#search_catalog_name").val();
            var category_id = $("#search_catalog_category").val();
            var type = $("#search_catalog_type").val();

            var params = {
                "_token": "{{ csrf_token() }}",
                ids: selected_products
            };

            if( catalog_name !== '') params['food_name'] = catalog_name;
            if( category_id !== '') params['food_category_id'] = category_id;
            if( type !== '') params['food_type'] = type;

            var row_catalog = "<tr>";
                row_catalog += "<td class='text-center'>Ricerca in corso...</td>";
                row_catalog += "</tr>";
            $("#catalog_search_results").html(row_catalog);

            $.ajax({
                url: '/shopCatalogs/search',
                data: params,
                type: 'GET',
                dataType: 'json',
                statusCode: {
                    401: function() {
                        document.location.href = '/login';
                    },
                    419: function() {
                        document.location.href = '/login';
                    }
                },
                success: function(res){

                    $("#catalog_search_results").html('');

                    $("#compact_mode").prop("checked", false);

                    if( res.count > 0 ){

                        $(".compact-mode-container").show();

                        catalog_data = res["Catalog list"];

                        for(var i=0; i<res.count; i++){
                            var row = res['Catalog list'][i];

                            var row_catalog = '<tr class="row-search-result" id="tr_catalog_'+row.id+'">';

                            var image = "<img width='100'  src='/images/food.jpg'>";
                            if( row.food_image ){
                                if ( row.food_image !== '' && row.food_image !== null ){
                                    console.log("image", row.food_image, row.food_image.indexOf('/'));
                                    if( row.food_image.indexOf('/') >= 0 ){
                                        image = "<img width='100'  src='"+row.food_image+"'>";
                                    }else{
                                        image = "<img width='100'  src='/storage/images/foods/"+row.food_category.name.toLowerCase().split(' ').join('_')+"/"+row.food_image+"'>";
                                    }
                                }
                            }

                            row_catalog += "<td style='width: 20px;'><button type='button' class='btn btn-sm btn-success' title='Aggiungi' onClick='addCatalog("+i+")'><i class='fa fa-plus'></i></i></button></td>";
                            row_catalog += "<td class='text-left food-name'><strong>"+row.food_name+"</strong><div class='food-description'><small>"+row.food_description+"</small></div></td>";
                            row_catalog += "<td class='food-img' style='width: 110px;'>"+image+"</td>";
                            row_catalog += "<td class='text-left food-type'><small><strong>"+row.food_category.name.toLowerCase().split(' ').join('_')+"</strong><br>"+row.food_type+"</small></td>";
                            row_catalog += "</tr>";
                            $("#catalog_search_results").append(row_catalog);
                            console.log("row_catalog", row_catalog);
                        }
                    }else if( typeof res.error !== "undefined" && res.error !== '' ){
                        var row_catalog = "<tr>";
                            row_catalog += "<td class='text-center'>"+res.error+"</td>";
                            row_catalog += "</tr>";
                        $("#catalog_search_results").append(row_catalog);
                    }else{
                        var row_catalog = "<tr>";
                            row_catalog += "<td class='text-center'>Spiacenti ma la tua ricerca non ha prodotto risultati. Prova con un altro termine</td>";
                            row_catalog += "</tr>";
                        $("#catalog_search_results").append(row_catalog);
                    }

                }
            });
        }

        addCatalog = (catalog_id) => {

            var catalog = catalog_data[catalog_id];

            console.log("catalog_data", catalog_data);

            selected_products.push(catalog.id);

            $('#tr_catalog_'+catalog.id).remove();

            var row_catalog = "<tr>";
            var image = "/images/food.jpg";
            if ( catalog.food_image !== '' && catalog.food_image !== null ){
                if( catalog.food_image.indexOf('/') >= 0 ){
                    image = catalog.food_image;
                }else{
                    image = '/storage/images/foods/'+catalog.food_category.name.toLowerCase().split(' ').join('_')+"/"+catalog.food_image;
                }
            }

            var row_catalog =  `<tr id="tr_shop_offer_food_`+catalog.id+`">
                                    <td class="text-center"><a target="_blank" href="/shopCatalogs/`+catalog.id+`/edit">`+catalog.food_name+`</a>
                                        <input type="hidden" name="shop_offer_food_id[]" value="">
                                        <input type="hidden" name="shop_catalog_id[]" value="`+catalog.id+`">
                                        <input type="hidden" name="shop_catalog_name[]" value="`+catalog.food_name+`">
                                        <input type="hidden" name="unit_of_measure_buy[]" value="`+catalog.unit_of_measure_buy.id+`">
                                        <input type="hidden" name="unit_of_measure_buy_quantity[]" value="`+catalog.unit_of_measure_buy_quantity+`">

                                        <br>

                                        <img src="`+image+`" style="width: 100%; max-width: 400px;"><input type="hidden" name="image[]" value="`+image+`">

                                        <br>

                                        <button type="button" class="btn btn-danger btn-sm btn-block mt-1" onClick="delRowShopOfferFood(`+catalog.id+`)"><i class="fa fa-trash"></i>  Elimina</td>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <label>Q.tà minima per avere lo sconto</label>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="basic-addon1">Multipli di `+catalog.unit_of_measure_buy_quantity+' '+catalog.unit_of_measure_buy.name+`</span>
                                                    <input type="hidden" name="unit_of_measure[]" value="`+catalog.unit_of_measure_buy.name+`">
                                                </div>
                                                <input type="number" id="min_quantity" name="min_quantity[]" min="0" value="1" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Prezzo di listino</label>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="basic-addon1">&euro;</span>
                                                </div>
                                                <input type="text" id="food_price_`+catalog.id+`" name="food_price[]" value="`+parseFloat(catalog.food_price).toFixed(2).toString().replace('.',',')+`" class="form-control"  style="min-width: 50px;" readonly>
                                                <div class="input-group-append">
                                                    <span class="input-group-text" id="basic-addon1">`+catalog.unit_of_measure.name+`</span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Sconto</label>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="basic-addon1">%</span>
                                                </div>
                                                <input type="number" id="discount_`+catalog.id+`" name="discount[]" value="" class="form-control discount" min="0.01" max="100" step="any">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Prezzo scontato</label>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="basic-addon1">&euro;</span>
                                                </div>
                                                <input type="number" id="discounted-price_`+catalog.id+`" name="discounted_price[]" value="" class="form-control discounted-price" placeholder="Prezzo" aria-label="Prezzo" aria-describedby="basic-addon1" required="true" min="0.01" max="999999.99" step="any">
                                            </div>
                                        </div>
                                    </td>
                                </tr>`;

            $("#table_products").append(row_catalog);

            discountEvents();
        }

        delRowShopOfferFood = (id) => {
            showConfirm("Catalogo", 'bg-danger', "Intendi eliminare in modo permanente il prodotto dall'offerta?", execDelRowShopOfferFood, [id]);
        }

        execDelRowShopOfferFood = (id) => {
            removeA(selected_products,id);
            $("#tr_shop_offer_food_"+id).remove();
        }

        delShopOfferFood = () => {

        }

        function removeA(arr) {
            var what, a = arguments, L = a.length, ax;
            while (L > 1 && arr.length) {
                what = a[--L];
                while ((ax= arr.indexOf(what)) !== -1) {
                    arr.splice(ax, 1);
                }
            }
            return arr;
        }

    </script>
@endsection
