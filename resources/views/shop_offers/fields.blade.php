<div class="row">
    <div class="col-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row">
            <!-- Start Date Field -->
            <div class="form-group col-12 col-sm-12 col-md-3 col-lg-3">
                {!! Form::label('start_date', 'Data Inizio') !!}
                {{-- 'min' => (isset($shopOffer) && !empty($shopOffer->start_date)) ? '' : \Carbon\Carbon::now()->format('Y-m-d') --}}
                {{-- Form::text('start_date', ( isset($shopOffer->start_date) && !empty($shopOffer->start_date ) ) ? $shopOffer->start_date->format('Y-m-d') : null, ['class' => 'form-control','id'=>'start_date', 'required' => true]) --}}
                <div class="input-group date" data-target-input="nearest">
                    <input type="hidden" id="start_date_value" value="{{ (isset($shopOffer->start_date) && !empty($shopOffer->start_date)) ? $shopOffer->start_date->format('d/m/Y') : null }}">
                    <input type="text" id="start_date"
                                       name="start_date"
                                       class="form-control datetimepicker-input"
                                       data-target="#start_date"
                                       placeholder="gg/mm/aaaa"
                                       required="true"
                                       readonly />
                    <div class="input-group-append" data-target="#start_date" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div>
                    <br>
                </div>
                @error('start_date')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>

            <!-- End Date Field -->
            <div class="form-group col-12 col-sm-12 col-md-3 col-lg-3 @error('end_date') has-error @endif">
                {!! Form::label('end_date', 'Data Fine') !!}
                {{-- 'min' => (isset($shopOffer) && !empty($shopOffer->end_date)) ? '' :  \Carbon\Carbon::now()->format('Y-m-d') --}}
                {{-- Form::text('end_date', (isset($shopOffer->end_date) && !empty($shopOffer->end_date)) ? $shopOffer->end_date->format('Y-m-d') : null, ['class' => 'form-control','id'=>'end_date', 'required' => true]) --}}
                <div class="input-group date" data-target-input="nearest">
                    <input type="hidden" id="end_date_value" value="{{ (isset($shopOffer->end_date) && !empty($shopOffer->end_date)) ? $shopOffer->end_date->format('d/m/Y') : null }}">
                    <input type="text" id="end_date"
                                       name="end_date"
                                       class="form-control datetimepicker-input"
                                       data-target="#end_date"
                                       placeholder="gg/mm/aaaa"
                                       required="true"
                                       readonly />
                    <div class="input-group-append" data-target="#end_date" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div>
                    <br>
                </div>
                @error('end_date')
                    <div class="alert alert-danger mt-1">{{ $message }}</div>
                @enderror
            </div>

            <!-- Name Field -->
            <div class="form-group col-12 col-sm-12 col-md-6 col-lg-6 @error('name') has-error @endif">
                {!! Form::label('name', 'Nome offerta') !!}
                {!! Form::text('name', null, ['class' => 'form-control', 'required' => true]) !!}
                @error('name')
                    <div class="alert alert-danger mt-1">{{ $message }}</div>
                @enderror
            </div>

            <!-- Description Field -->
            <div class="form-group col-12">
                {!! Form::label('description', 'Descrizione offerta') !!}
                {!! Form::textarea('description', null, ['class' => 'form-control', 'rows' => 6]) !!}
                @error('description')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
        </div>
    </div>

    <div class="col-12 col-sm-12 col-md-12 col-lg-12">

        <div class="table-responsive">
            <table class="table" id="table_products">
                <thead>
                    <th>Prodotto</th>
                    <th></th>
                </thead>

                @error('shop_catalog_id')
                <tbody class="bg-danger">
                    <tr>
                        <td colspan="5" class="text-light">{{ $message }}</td>
                    </tr>
                </tbody>
                @enderror

                @if( isset(Session::getOldInput()['shop_catalog_name']) )
                <tbody>

                    @error('shop_catalog_id')
                        <tr>
                            <td colspan="5" class="has-error text-danger">{{ $message }}</td>
                        </tr>
                    @enderror

                    @foreach(Session::getOldInput()['shop_catalog_name'] as $k => $shop_catalog_name)
                    <tr class="shop_offer_food" id="tr_shop_offer_food_{!! Session::getOldInput()['shop_catalog_id'][$k] !!}">
                        <td class="text-center">
                            <a target="_blank" href="/shopCatalogs/{{ Session::getOldInput()['shop_catalog_id'][$k] }}/edit">{{ Session::getOldInput()['shop_catalog_name'][$k] }}</a>
                            <br>
                            <img src="{!! Session::getOldInput()['image'][$k] !!}" style="width: 100%; max-width: 400px;">

                            <input type="hidden" name="image[]" value="{!! Session::getOldInput()['image'][$k] !!}">
                            <input type="hidden" name="shop_offer_food_id[]" value="{!! Session::getOldInput()['shop_offer_food_id'][$k] !!}">
                            <input type="hidden" name="shop_catalog_id[]" value="{!! Session::getOldInput()['shop_catalog_id'][$k] !!}">
                            <input type="hidden" name="shop_catalog_name[]" value="{!! Session::getOldInput()['shop_catalog_name'][$k] !!}">
                            <input type="hidden" name="unit_of_measure[]" value="{!! Session::getOldInput()['unit_of_measure'][$k] !!}">
                            <input type="hidden" name="unit_of_measure_buy[]" value="{!! Session::getOldInput()['unit_of_measure_buy'][$k] !!}">

                            <br>

                            @if( !empty(Session::getOldInput()['shop_offer_food_id'][$k]) )
                                <button type="button" class="btn btn-danger btn-sm btn-block mt-1" onClick="delShopOfferFood({!! Session::getOldInput()['shop_offer_food_id'][$k] !!})"><i class="fa fa-trash"></i> Elimina</td>
                            @else
                                <button type="button" class="btn btn-danger btn-sm btn-block mt-1" onClick="delRowShopOfferFood({!! Session::getOldInput()['shop_catalog_id'][$k] !!})"><i class="fa fa-trash"></i> Elimina</td>
                            @endif
                        </td>
                        <td>
                            <div class="form-group">
                                <label>Q.tà minima per avere lo sconto</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="qta_minima_1">Multipli di {!! Session::getOldInput()['unit_of_measure_buy_quantity'][$k] !!} {!! Session::getOldInput()['unit_of_measure_buy'][$k] !!}</span>
                                    </div>
                                    <input type="number" id="min_quantity" name="min_quantity[]" min="0" step="0" value="{{ Session::getOldInput()['min_quantity'][$k] }}" class="form-control" required></td>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Prezzo di listino</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">&euro;</span>
                                    </div>
                                    <input type="text" id="food_price_{{ Session::getOldInput()['shop_catalog_id'][$k] }}" name="food_price[]" value="{{ Session::getOldInput()['food_price'][$k] }}" class="form-control"  style="min-width: 50px;" readonly>
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="basic-addon2">{{ Session::getOldInput()['unit_of_measure'][$k] }}</span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Sconto</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">%</span>
                                    </div>
                                    <input type="number" id="discount_{{ Session::getOldInput()['shop_catalog_id'][$k] }}" name="discount[]" value="{{ Session::getOldInput()['discount'][$k] }}" class="form-control discount" min="1" max="100" step="any">
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Prezzo scontato</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">&euro;</span>
                                    </div>
                                    <input type="number" id="discounted-price_{{ Session::getOldInput()['shop_catalog_id'][$k] }}" name="discounted_price[]" value="{{ Session::getOldInput()['discounted_price'][$k] }}" class="form-control discounted-price" placeholder="Prezzo" aria-label="Prezzo" aria-describedby="basic-addon1" required="true" min="0.01" max="999999.99" step="any">
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                @else
                    @if( isset($shopOffer) )
                        @foreach($shopOffer->shopOfferFoods as $shopOfferFoods)
                        @if( isset($shopOfferFoods->shopCatalog) )
                        <tr class="shop_offer_food" id="tr_shop_offer_food_{{ $shopOfferFoods->id }}">
                            <td class="text-center">
                                <a target="_blank" href="/shopCatalogs/{{ $shopOfferFoods->shop_catalog_id }}/edit">{{ $shopOfferFoods->food_name }}</a>

                                <br>

                                @php
                                $image = "/images/food.jpg";

                                if( empty($shopOfferFoods->food_image) ):
                                    if( $shopOfferFoods->shopCatalog ):
                                        if( !empty($shopOfferFoods->shopCatalog->food_image) ):
                                            $image = $shopOfferFoods->shopCatalog->food_image;
                                        else:
                                            if( $shopOfferFoods->shopCatalog->food ):
                                                $image = asset('/storage/images/foods/'.NormalizeString($shopOfferFoods->shopCatalog->food->foodCategory->name).'/'.$shopOfferFoods->shopCatalog->food->image);
                                            endif;
                                        endif;
                                    endif;
                                else:
                                    $image = $shopOfferFoods->food_image;
                                endif;
                                @endphp

                                @if( $image )
                                    <img src="{!! $image !!}" style="width: 100%; max-width: 400px;">
                                @endif

                                <input type="hidden" name="image[]" value="{!! $image !!}">
                                <input type="hidden" name="shop_offer_food_id[]" value="{!! $shopOfferFoods->id !!}">
                                <input type="hidden" name="shop_catalog_id[]" value="{!! $shopOfferFoods->shop_catalog_id !!}">
                                <input type="hidden" name="shop_catalog_name[]" value="{!! $shopOfferFoods->food_name !!}">
                                <input type="hidden" name="unit_of_measure[]" value="{!! $shopOfferFoods->shopCatalog->unitOfMeasure->name !!}">
                                <input type="hidden" name="unit_of_measure_buy[]" value="{!! $shopOfferFoods->shopCatalog->unitOfMeasureBuy->name !!}">
                                <input type="hidden" name="unit_of_measure_buy_quantity[]" value="{!! $shopOfferFoods->shopCatalog->unit_of_measure_buy_quantity !!}">

                                <br>

                                <button type="button" class="btn btn-danger btn-sm btn-block mt-1" onClick="delShopOfferFood({{ $shopOfferFoods->id }})"><i class="fa fa-trash"></i> Elimina</td>
                            </td>
                            <td>
                                <div class="form-group">
                                    <label>Q.tà minima per avere lo sconto</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="qta_minima_2">Multipli di {!! $shopOfferFoods->shopCatalog->unit_of_measure_buy_quantity !!} {!! $shopOfferFoods->shopCatalog->unitOfMeasureBuy->name !!}</span>
                                        </div>
                                        <input type="number" id="min_quantity" name="min_quantity[]" min="0" value="{!! $shopOfferFoods->min_quantity !!}" class="form-control" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Prezzo di listino</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">&euro;</span>
                                        </div>
                                        <input type="text" id="food_price_{{ $shopOfferFoods->shopCatalog->id }}" name="food_price[]" value="{!! number_format($shopOfferFoods->shopCatalog->food_price, 2, '.','') !!}" class="form-control"  style="min-width: 50px;" readonly>
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="basic-addon2">{{ $shopOfferFoods->shopCatalog->unitOfMeasure->name }}</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>Sconto</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">%</span>
                                        </div>
                                        <input type="number" id="discount_{{ $shopOfferFoods->shopCatalog->id }}" name="discount[]" value="{!! !empty($shopOfferFoods->discount) ? number_format($shopOfferFoods->discount, 2, '.','') : '' !!}" class="form-control discount" min="1" max="100" step="any">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>Prezzo scontato</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">&euro;</span>
                                        </div>
                                        <input type="number" id="discounted-price_{{ $shopOfferFoods->shopCatalog->id }}" name="discounted_price[]" value="{!! !empty($shopOfferFoods->discounted_price) ? number_format($shopOfferFoods->discounted_price, 2, '.','') : '' !!}" class="form-control discounted-price" placeholder="Prezzo" aria-label="Prezzo" aria-describedby="basic-addon1" required="true" min="0.01" max="999999.99" step="any">
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @else
                        <tr>
                            <td>
                                <button type="button" class="btn btn-danger btn-sm" onClick="delShopOfferFood({{ $shopOfferFoods->id }})"><i class="fa fa-trash"></i></td>
                            </td>
                            <td colspan="4">Prodotto del catalogo non trovato!</td>
                        </tr>
                        @endif
                        @endforeach
                    @endif
                @endif
            </table>
        </div>
    </div>
</div>
