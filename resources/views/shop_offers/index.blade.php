@extends('adminlte::page')
@section('content')
@if(Auth::user()->subscription->subscription != 'free')
    <div class="content padding-bottom-plus">

        <section class="content-header">
            <h1 class="pull-left">Le tue offerte</h1>
            <div class="row">
                <div class="col-md-12">
                    <div class="accordion" id="accordionExample">
                        <h2 class="m-0">
                            <button class="btn btn-link btn-block text-left mx-0 px-0" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                <i class="far fa-list-alt"></i> Legenda
                                <i class="fas fa-info-circle" data-toggle="tooltip" data-placement="top" title="Per ogni offerta sono previsti 5 stati a seconda delle tue esigenze. Puoi cambiarli facilmente cliccando sul bottone nella colonna stato oppure dal dettaglio dell'offerta"></i>
                            </button>
                        </h2>
                        <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                            <div class="table-responsive">
                                <table class="table legenda">
                                    <tbody>
                                        <tr><td class="p-1"><strong>Offerta in bozza</strong> > non è attivata, pertanto non visibile ne' tantomeno programmata</td></tr>
                                        <tr><td class="p-1 table-warning"><strong>Offerta programmata</strong> > è attivata ma diventerà automaticamente visibile e ordinabile solo alla data prevista e fino alla data di scadenza</td></tr>
                                        <tr><td class="p-1 table-success"><strong>Offerta in corso</strong> > è attiva a tutti gli effetti, visibile e quindi ordinabile dagli utenti</td></tr>
                                        <tr><td class="p-1 table-danger"><strong>Offerta sospesa</strong> > è sospesa e pertanto non visibile e non ordinabile</td></tr>
                                        <tr><td class="p-1 table-secondary"><strong>Offerta scaduta</strong> > essendo trascorsa la data di scadenza l'offerta è stata automaticamente resa non più visibile</td></tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <div class="clearfix"></div>

        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        <div class="clearfix"></div>
        <div class="card card-primary">
            <div class="card-body">
                @include('shop_offers.table')
            </div>
        </div>
        <div class="text-center">

        </div>
    </div>
@else
<div class="modal-danger" align="center">
  <p><strong>Per gestire le offerte occorre avere un piano di abbonamento a pagamento.</strong></p>
  <p><a href="https://backend.primascelta.biz/prices/" target="_blank"><strong><u>Desidero vedere i piani di abbonamento disponibili</u></strong></a></p>
</div>
@endif
@endsection

@section('js')
<script>
$(function () {

    $('#shopOffers-table').DataTable({
        "language": {
            url: "{{ url('js/Italian.json') }}"
        },
        paging:   true,
        "initComplete": function (settings, json) {
            $("#shopOffers-table").wrap("<div style='overflow:auto; width:100%;position:relative;'></div>");
        },
        pageLength: 10,
        stateSave: true,
        order: [[ 3, "desc" ]],
        columnDefs: [
            { width: "10%", orderable: false, targets: 0 },
            { width: "150px", orderable: false, targets: 1 }
        ]
    });
});

deleteOffer = (shop_offer_id) => {
    showConfirm("Offerta", 'bg-danger', "Intendi cancellare in modo permanente questa offerta?", execDeleteOffer, [shop_offer_id]);
}

execDeleteOffer = (shop_offer_id) => {
    $.ajax({
        url: `/shopOffers/${shop_offer_id}`,
        data: {
            "_token": "{{ csrf_token() }}"
        },
        type: 'DELETE',
        dataType: 'json',
        statusCode: {
            401: function() {
                document.location.href = '/login';
            },
            419: function() {
                document.location.href = '/login';
            }
        },
        success: function(data){
            console.log(data);
            if( data.status == 'ok' ){
                document.location.href = "/shopOffers";
            }else if( data.status == 'error' ){
                alert(data.msg);
            }
        }
    });
}
</script>
@endsection
