<div class="modal fade" id="modal-search-catalog" aria-hidden="true" data-backdrop="static" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-success">
                <h4 class="modal-title">Aggiungi un nuovo prodotto a questa offerta</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="frm-search-catalog">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                {!! Form::label('search_catalog_name', 'Cerca un prodotto nel tuo catalogo') !!}
                                {!! Form::text('search_catalog_name', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                {!! Form::label('search_catalog_category', 'Genere') !!}
                                {!! Form::select('search_catalog_category', $foodCategoryList, null,  ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                {!! Form::label('search_catalog_type', 'Categoria') !!}
                                {!! Form::select('search_catalog_type', [], null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>
                </form>
                <div class="compact-mode-container text-right" style="display: none;">
                    <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                        <input type="checkbox" class="custom-control-input" id="compact_mode">
                        {!! Form::label('compact_mode', 'Visualizzazione compatta', ['class' => 'custom-control-label']) !!}
                    </div>
                </div>
                <div style="max-height: 300px; overflow: auto;">
                    <div class="table-responsive">
                        <table class="table">
                            <tbody id="catalog_search_results"></tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Chiudi</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
