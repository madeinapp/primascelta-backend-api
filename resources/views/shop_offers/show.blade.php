@extends('adminlte::page')

@section('content')
    <div class="content">
        <div class="card card-primary">
            @php
            $bgcolor = '';
            $order_status = '';
            if( $shopOffer->status === 'available' && $shopOffer->start_date->isFuture() ):
                $bgcolor='bg-warning';
                $order_status = 'Offerta programmata';
            elseif( $shopOffer->status === 'available' && $shopOffer->start_date->isPast() && $shopOffer->end_date->addDays(1)->isFuture() ):
                $bgcolor='bg-success';
                $order_status = 'Offerta in corso';
            elseif( $shopOffer->end_date->addDays(1)->isPast() ):
                $bgcolor='bg-secondary';
                $order_status = 'Offerta scaduta';
            elseif( $shopOffer->status === 'cancelled' ):
                $bgcolor='bg-danger';
                $order_status = 'Offerta sospesa';
            endif
            @endphp
            <div class="card-header {{ $bgcolor }}">
                {{ $order_status }}
            </div>
            <div class="card-body">

                @include('shop_offers.show_fields')

                <a href="{{ route('shopOffers.index') }}" class="btn btn-default">Indietro</a>
            </div>
        </div>
    </div>
@endsection
