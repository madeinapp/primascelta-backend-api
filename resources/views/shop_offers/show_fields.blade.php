<div class="row">

    <!-- Start Date Field -->
    <div class="form-group col-6 col-sm-6 col-md-3 col-lg-3">
        {!! Form::label('start_date', 'Data Inizio') !!}
        <p>{{ $shopOffer->start_date->format('d/m/Y') }}</p>
    </div>

    <!-- End Date Field -->
    <div class="form-group col-6 col-sm-6 col-md-3 col-lg-3">
        {!! Form::label('end_date', 'Data Fine') !!}
        <p>{{ $shopOffer->end_date->format('d/m/Y') }}</p>
    </div>

    <!-- Status Field -->
    <div class="form-group col-6 col-sm-6 col-md-6 col-lg-6">
        {!! Form::label('status', 'Nome offerta') !!}
        <br>
        <p>{{ $shopOffer->name }}</p>
    </div>

    <!-- Description Field -->
    <div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
        {!! Form::label('description', 'Descrizione') !!}
        <p>{{ $shopOffer->description }}</p>
    </div>

</div>

<section>
    <h3>Prodotti in offerta</h3>
    <div class="row">
    @foreach($shopOffer->shopOfferFoods as $shopOfferFood)
    <div class="col-12 col-sm-12 col-md-6 col-lg-4">
        <div class="card">
            <div class="card-header bg-success">
                <strong>{{ $shopOfferFood->food_name }}</strong>
            </div>

            <div class="card-body text-center">
                @php
                $image = "/images/food.jpg";

                if( empty($shopOfferFood->food_image) ):
                    if( $shopOfferFood->shopCatalog ):
                        if( !empty($shopOfferFood->shopCatalog->food_image) ):
                            $image = $shopOfferFood->shopCatalog->food_image;
                        else:
                            if( $shopOfferFood->shopCatalog->food ):
                                $image = asset('/storage/images/foods/'.NormalizeString($shopOfferFood->shopCatalog->food->foodCategory->name).'/'.$shopOfferFood->shopCatalog->food->image);
                            endif;
                        endif;
                    endif;
                else:
                    $image = $shopOfferFood->food_image;
                endif;
                @endphp

                <img src="{{ $image }}" style="width: 100px">

                <br>

                <strong>Q.ta minima: </strong>:
                @if( $shopOfferFood->shopCatalog )
                {{ $shopOfferFood->min_quantity }} {{ $shopOfferFood->shopCatalog->unitOfMeasureBuy->name }}
                @else
                {{ $shopOfferFood->min_quantity }}
                @endif

                <br>

                <strong>Prezzo di listino</strong>:
                @if( $shopOfferFood->shopCatalog && !empty($shopOfferFood->shopCatalog->food_price) )
                &euro; {{ number_format($shopOfferFood->shopCatalog->food_price, 2, ',', '.') }}
                @endif

                <br>

                <strong>Prezzo scontato</strong>:
                &euro; {{ number_format($shopOfferFood->discounted_price, 2, ',', '.') }}

                <br>

                <strong>Sconto</strong>:
                @if( $shopOfferFood->shopCatalog && !empty($shopOfferFood->shopCatalog->food_price) )
                @php
                $discount = round(100 - (($shopOfferFood->discounted_price * 100) / $shopOfferFood->shopCatalog->food_price), 2);
                @endphp
                {{ number_format($discount, 2, ',', '.') }} %
                @endif
            </div>
        </div>
    </div>
    @endforeach
    </div>

</section>
