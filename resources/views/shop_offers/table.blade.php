@can('add_to_catalog')
	@can('add_offer')
<a class="btn btn-add btn-add-offer bg-success btn-lg d-none d-sm-none d-md-block mb-4" href="{{ route('shopOffers.create') }}">+</a>
	@else
		<center class="alert alert-danger">
			<strong>Per gestire le offerte occorre avere un piano di abbonamento a pagamento</strong>
			<p><i><u><a target="_blank" href="https://www.primascelta.biz/prezzi/">Vedi i piani di abbonamento</a></u></i></p>
		</center>
	@endcan
@endcan
@if( count($shopOffers) == 0 )
        <p class="text-center text-success font-weight-bold">
            AL MOMENTO NON HAI OFFERTE IN CORSO
        </p>
    @else
<table class="table table-hover shop-offers-table" id="shopOffers-table">
    <thead>
        <tr>
            <th>Azioni</th>
            <th>Immagine</th>
            <th>Nome</th>
            <th>Inizio</th>
            <th>Fine</th>
        </tr>
    </thead>
    <tbody>
    @foreach($shopOffers as $shopOffer)
        <tr
        @if( $shopOffer->status === 'available' && $shopOffer->start_date->isPast() && $shopOffer->end_date->addDays(1)->isFuture() )
        class="table-success"
        @elseif( $shopOffer->status === 'available' && $shopOffer->start_date->isFuture() )
        class="table-warning"
        @elseif( $shopOffer->status === 'available' && $shopOffer->end_date->addDays(1)->isPast() )
        class="table-secondary"
        @elseif( $shopOffer->status === 'cancelled' )
        class="table-danger"
        @endif
        >
            <td>
                <div class="m-2">
                    <div class="text-center" style="display: block; min-width: 130px !important;">
                        <a href="{{ route('shopOffers.show', [$shopOffer->id]) }}" class='btn btn-primary btn-sm' title="ANTEPRIMA"><i class="fa fa-eye"></i></a>
                        <a href="{{ route('shopOffers.edit', [$shopOffer->id]) }}" class='btn btn-warning btn-sm' title="MODIFICA"><i class="fas fa-edit"></i></a>
                        @if( ($shopOffer->status === 'available' && $shopOffer->start_date->isFuture()) ||
                            ($shopOffer->status === 'available' && $shopOffer->end_date->addDays(1)->isPast()) ||
                            $shopOffer->status === 'cancelled' ||
                            $shopOffer->status === 'draft' )
                            {!! Form::button('<i class="fas fa-trash"></i>', ['class' => 'btn btn-danger btn-sm', 'onclick' => "deleteOffer($shopOffer->id)", 'title' => "ELIMINA"]) !!}
                        @endif
                    </div>

                    @if($shopOffer->status === 'draft')
                    <a href="{{ route('shopOffers.status', ['shopOffer' => $shopOffer->id]) }}" class="btn btn-sm btn-block mt-1">
                        <i class="fas fa-times"></i> Bozza
                    </a>
                    @elseif($shopOffer->status === 'available' && $shopOffer->start_date->isFuture())
                    <a href="{{ route('shopOffers.status', ['shopOffer' => $shopOffer->id]) }}" class="btn btn-sm btn-block btn-warning mt-1" >
                        <i class="fas fa-check"></i> Programmata
                    </a>
                    @elseif($shopOffer->status === 'available' && $shopOffer->start_date->isPast() && $shopOffer->end_date->addDays(1)->isFuture())
                    <a href="{{ route('shopOffers.status', ['shopOffer' => $shopOffer->id]) }}" class="btn btn-sm btn-block btn-success mt-1">
                        <i class="fas fa-check"></i> Attiva
                    </a>
                    @elseif(  $shopOffer->end_date->addDays(1)->isPast() )
                    <a href="{{ route('shopOffers.status', ['shopOffer' => $shopOffer->id]) }}" class="btn btn-sm btn-block btn-secondary mt-1">
                        <i class="fas fa-stopwatch"></i> Scaduta
                    </a>
                    @elseif($shopOffer->status === 'cancelled')
                    <a href="{{ route('shopOffers.status', ['shopOffer' => $shopOffer->id]) }}" class="btn btn-sm btn-block btn-danger mt-1">
                        <i class="fas fa-times-circle"></i> Sospesa
                    </a>
                    @endif
                </div>
            </td>
            <td>
                @if( $shopOffer->shopOfferFoods->count() > 1 )
                    <img src="/images/offer.png" style="width: 100%">
                @elseif( $shopOffer->shopOfferFoods->count() === 1 )
                    @if( !empty($shopOffer->shopOfferFoods[0]->food_image) )
                        <img src="{{ getResizedImage(getOriginalImage($shopOffer->shopOfferFoods[0]->food_image), '230x154') }}" style="width: 100%">
                    @elseif( $shopOffer->shopOfferFoods[0]->shopCatalog )
                        @if( !empty($shopOffer->shopOfferFoods[0]->shopCatalog->food_image) )
                            <img src="{{ getResizedImage(getOriginalImage($shopOffer->shopOfferFoods[0]->shopCatalog->food_image), '230x154') }}" style="width: 100%">
                        @elseif( $shopOffer->shopOfferFoods[0]->shopCatalog->food )
                            <img src="{{ getResizedImage(getOriginalImage(asset('/storage/images/foods/'.NormalizeString($shopOffer->shopOfferFoods[0]->shopCatalog->food->foodCategory->name).'/'.$shopOffer->shopOfferFoods[0]->shopCatalog->food->image)), '230x154') }}" style="width: 100%">
                        @endif
                    @endif
                @endif
            </td>
            <td>{{ $shopOffer->name }}</td>
            <td><span style="display: none">{{ $shopOffer->start_date->format('Ymd') }}</span>{{ $shopOffer->start_date->format('d/m/Y') }}</td>
            <td><span style="display: none">{{ $shopOffer->end_date->format('Ymd') }}</span>{{ $shopOffer->end_date->format('d/m/Y') }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
 @endif
@can('add_to_catalog')
	@can('add_offer')
<a class="btn btn-add btn-add-offer bg-success btn-lg d-md-block mt-4" href="{{ route('shopOffers.create') }}">+</a>
	@endcan
@endcan
