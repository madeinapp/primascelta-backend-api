@extends('adminlte::page')

@section('content')
    <section class="content-header">
        <h1>
			{{ __('common.Shop Types') }}
        </h1>
    </section>
    <div class="content">
        {{-- @include('adminlte-templates::common.errors') --}}
        <div class="card card-primary">
            <div class="card-body">
                <div class="card-body">
                    {!! Form::open(['route' => 'shopTypes.store']) !!}

                        @include('shop_types.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
