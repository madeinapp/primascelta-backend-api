@extends('adminlte::page')

@section('content')
    <section class="content-header">
        <h1>
			{{ __('common.Shop Types') }}
        </h1>
   </section>
   <div class="content">
       {{-- @include('adminlte-templates::common.errors') --}}
       <div class="card card-primary">
           <div class="card-body">
               <div class="card-body">
                   {!! Form::model($shopType, ['route' => ['shopTypes.update', $shopType->id], 'method' => 'patch']) !!}

                        @include('shop_types.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection
