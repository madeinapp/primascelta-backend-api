<!-- Name Field -->
<div class="form-group ">
    {!! Form::label('name', __('common.name').':') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
    @error('name')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('common.save'), ['class' => 'btn btn-primary btn-full']) !!}
    <a href="{{ route('shopTypes.index') }}" class="btn btn-default btn-full">{{ __('common.cancel') }}</a>
</div>
