@extends('adminlte::page')

@section('content')
    <section class="content-header">
        <h1>
			{{ __('common.Shop Types') }}
        </h1>
    </section>
    <div class="content">
        <div class="card card-primary">
            <div class="card-body">
                <div class="row" style="padding-left: 20px">
                    @include('shop_types.show_fields')
                    <a href="{{ route('shopTypes.index') }}" class="btn btn-default">{{ __('common.Back') }}</a>
                </div>
            </div>
        </div>
    </div>
@endsection
