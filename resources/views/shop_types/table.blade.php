<table class="table" id="shopTypes-table">
    <thead>
        <tr>
            <th>{{ __('common.action') }}</th>
            <th>{{ __('common.name') }}</th>
        </tr>
    </thead>
    <tbody>
    @foreach($shopTypes as $shopType)
        <tr>
            <td>
                {!! Form::open(['route' => ['shopTypes.destroy', $shopType->id], 'method' => 'delete']) !!}

                    <a href="{{ route('shopTypes.edit', [$shopType->id]) }}" class='btn btn-default btn-sm'><i class="fas fa-edit"></i></a>
                    {!! Form::button('<i class="fas fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-sm', 'onclick' => "return confirm('".__('common.Are you sure?')."')"]) !!}

                {!! Form::close() !!}
            </td>
            <td>{{ $shopType->name }}</td>
        </tr>
    @endforeach
    </tbody>
</table>

@section('js')
<script>
$(function () {
    $('#shopTypes-table').DataTable({
        "language": {
            url: "{{ url('js/Italian.json') }}"
        },
        paging:   true,
        "initComplete": function (settings, json) {
            $("#shopTypes-table").wrap("<div style='overflow:auto; width:100%;position:relative;'></div>");
        },
        pageLength: 10,
        stateSave: true,
        columnDefs: [
            { width: "10%", orderable: false, targets: 0 }
        ]
    });
});
</script>
@endsection
