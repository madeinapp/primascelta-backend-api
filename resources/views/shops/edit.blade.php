@extends('adminlte::page')

@section('css')
<link rel="stylesheet" type="text/css" href="/fileinput/normalize.css" />
<link rel="stylesheet" type="text/css" href="/fileinput/component.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.css"/>
<script>(function(e,t,n){var r=e.querySelectorAll("html")[0];r.className=r.className.replace(/(^|\s)no-js(\s|$)/,"$1js$2")})(document,window,0);</script>

<style>
    .row-day{
        padding: 20px 0;
    }
    .form-delivery-address{
        margin: 20px 0 20px 0;
    }

    .tab-pane{
        background-color: white;
        margin: 0;
        padding: 40px;
        width: 100%;
    }

    .nav-profile .nav-link{
        background-color: #ffc107;
        border: 1px solid #666;
        color: #000;
    }

    .nav-profile .nav-link.active{
        background-color: #13bf9e;
        border: 1px solid #666;
        color: #fff;
    }

    .nav-profile .nav-link.error{
        background-color: #dc3545;
        border: 1px solid #666;
        color: #000;
    }

    .nav-profile .nav-link.error.active{
        background-color: #dc3545;
        border: 1px solid #666;
        color: #fff;
    }

    .slider .tooltip {
        display: none !important;
    }

    .bg-default{
        background-color: #ddd;
    }

</style>
@endsection

@section('content')
    <section class="content-header">
        <span style="font-size: 1.5em">{{ $user->shop->name }}</span>
        <br /><span style="font-size: 1.2em"><i>{{ $user->shop->shopType }}</i></span>
        <div class="pull-right  float-right d-none d-sm-block" >
           <!--a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('users.create') !!}">Aggiungi nuovo</a-->
        </div>
    </section>
    <div class="content">

        <div class="clearfix"></div>

        @include('flash::message')

        {{--
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        --}}

        <div class="clearfix"></div>

        {!! Form::open(array('url' => route('shops.update', ['shop' => $user->shop->id]), 'method' => 'PUT', 'files' => true)) !!}

        @csrf

        @php
        $expire_date = @\Carbon\Carbon::parse(old('subscription_date', $user->subscription->expire_date->format('Y-m-d')));

        $corporate_data_error = false;
        $opening_timetable_error = false;
        $delivery_error = false;
        $tax_error = false;
        $subscription_error = false;
        $password_error = false;
        $accounting_error = false;

        if (    $errors->has('shop_type_id') || $user->shop->shopTypes->count() === 0 ||
                $errors->has('website') ||
                $errors->has('whatsapp') ||
                $errors->has('user_name') ||
                $errors->has('user_birthdate') ||
                $errors->has('shop_name') ||
                $errors->has('address') ||
                $errors->has('route') ||
                $errors->has('phone') ||
                $errors->has('whatsapp') ||
                $errors->has('email') ||
                $errors->has('facebook_page') ||
                $errors->has('instagram_page') ||
                $errors->has('description') ||
                $errors->has('payment_method_id') ||
                empty($user->image_url) ||
                empty($user->shop->whatsapp) ||
                empty($user->shop->description) ||
                $user->shop->paymentMethodsShop->count() === 0 ):
            $corporate_data_error = 'error';
        endif;

        if ( $errors->has('val_opening_timetable.monday') ||
             $errors->has('val_opening_timetable.tuesday') ||
             $errors->has('val_opening_timetable.wednesday') ||
             $errors->has('val_opening_timetable.thursday') ||
             $errors->has('val_opening_timetable.friday') ||
             $errors->has('val_opening_timetable.saturday') ||
             $errors->has('val_opening_timetable.sunday') ||
             (
                !$user->shop->obj_opening_timetable->monday->active &&
                !$user->shop->obj_opening_timetable->tuesday->active &&
                !$user->shop->obj_opening_timetable->wednesday->active &&
                !$user->shop->obj_opening_timetable->thursday->active &&
                !$user->shop->obj_opening_timetable->friday->active &&
                !$user->shop->obj_opening_timetable->saturday->active &&
                !$user->shop->obj_opening_timetable->sunday->active
             ) ||
             $errors->has('closed_date_start') ||
             $errors->has('closed_date_end') ||
             $errors->has('alert_opening_days')):
            $opening_timetable_error = 'error';
        endif;

        if ( $errors->has('val_delivery_opening_timetable.monday') ||
             $errors->has('val_delivery_opening_timetable.tuesday') ||
             $errors->has('val_delivery_opening_timetable.wednesday') ||
             $errors->has('val_delivery_opening_timetable.thursday') ||
             $errors->has('val_delivery_opening_timetable.friday') ||
             $errors->has('val_delivery_opening_timetable.saturday') ||
             $errors->has('val_delivery_opening_timetable.sunday') ||
             $errors->has('delivery_address_route') ||
             $errors->has('alert_delivery_days') ||
             $errors->has('delivery_payment_method_id') ||
             $errors->has('delivery_cost_error') ||
             ($user->shop->home_delivery==1 && $user->shop->PaymentMethodsDelivery->count() == 0)
             ):

            $delivery_error = 'error';
        endif;

        if ( $errors->has('tax_fiscal_code') ||
             $errors->has('tax_vat') ||
             $errors->has('tax_code') ||
             $errors->has('tax_pec') ||
             (empty($user->shop->tax_code) && empty($user->shop->tax_pec)) ):
            $tax_error = 'error';
        endif;

        if ( $errors->has('delivery_free_price_greater_than') ||
             $errors->has('delivery_forfait_cost_price') ):
            $delivery_error = 'error';
        endif;

        if ( $errors->has('new_password') ):
            $password_error = 'error';
        endif;


        $current_tab = intval(old('current_tab', 0));

        if( $current_tab === 0 ):
            $current_tab = session()->get('current_tab') ?? 0;
        endif;
        @endphp

        <!-- Nav tabs -->
        <ul class="nav nav-tabs nav-profile">
            <li class="nav-item">
                <a class="nav-link @if( $current_tab === 0 ) active @endif @if($corporate_data_error) error @endif" data-toggle="tab" onclick="setCurrentTab(0)" href="#home"><i class="fas fa-store-alt"></i> Dati aziendali</a>
            </li>
            <li class="nav-item">
                <a class="nav-link @if( $current_tab === 1 ) active @endif @if($opening_timetable_error) error @endif" data-toggle="tab" onclick="setCurrentTab(1)" href="#menu1"><i class="fas fa-clock"></i> Orari di apertura</a>
            </li>
            <li class="nav-item">
                <a class="nav-link @if( $current_tab === 2 ) active @endif @if($delivery_error) error @endif" data-toggle="tab" onclick="setCurrentTab(2)" href="#menu2"><i class="fas fa-shipping-fast"></i> Consegna e ritiro prodotti</a>
            </li>
            <li class="nav-item">
                <a class="nav-link @if( $current_tab === 3 ) active @endif @if($tax_error) error @endif" data-toggle="tab" onclick="setCurrentTab(3)" href="#menu3"><i class="fas fa-money-bill-wave"></i> Profilo fiscale</a>
            </li>
            <li class="nav-item">
                <a class="nav-link @if( $current_tab === 4 ) active @endif @if($password_error) error @endif" data-toggle="tab" onclick="setCurrentTab(4)" href="#menu4"><i class="fas fa-key"></i> Password</a>
            </li>
@if(Auth::user()->role == 'admin')
            <li class="nav-item">
                <a class="nav-link @if( $current_tab === 5 ) active @endif @if($accounting_error) error @endif" data-toggle="tab" onclick="setCurrentTab(5)" href="#menu5"><i class="fas fa-address-card"></i> Accounting</a>
            </li>
@endif
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div class="tab-pane @if( $current_tab === 0 ) active @endif" id="home">
                @include('users.corporate_data')
            </div>

            <div class="tab-pane @if( $current_tab === 1 ) active @endif" id="menu1">
                @include('users.opening_timetable')
            </div>

            <div class="tab-pane @if( $current_tab === 2 ) active @endif" id="menu2">
                @include('users.delivery')
            </div>

            <div class="tab-pane @if( $current_tab === 3 ) active @endif" id="menu3">
                @include('users.tax')
            </div>
            <div class="tab-pane @if( $current_tab === 4 ) active @endif" id="menu4">
                @include('users.change_password')
            </div>
@if(Auth::user()->role == 'admin')
            <div class="tab-pane @if( $current_tab === 5 ) active @endif" id="menu5">
                @include('users.accounting_admin')
            </div>
@endif
        </div>

        <div>
            <input type="hidden" name="user_id" value="{!! $user->id !!}">
            <input type="hidden" id="current_tab" name="current_tab" value="{{ $current_tab }}">
            <button id="btn-save-shop" class="btn btn-success btn-full" style="margin: 40px 0">Salva</button>
@if((Auth::user()->role == 'admin') && ($user->shop->admin_generated == 1))
			<button type="button" id="btn-send-email" class="btn btn-success btn-full" style="margin: 40px 0" onClick="sendAdminRegistrationEmail({{$user->shop->id}})">Invia email</button>
@endif
            <button type="button" id="btn-delete-shop" onClick="delShop({{$user->shop->id}})" class="btn btn-danger btn-full" style="margin: 40px 0">Elimina</button>
        </div>

        {!! Form::close() !!}
    </div>

    @include('users.modal_preview')

@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.js"></script>
<script src="/cropimage/cropimage.js"></script>
<script>

    $(document).ready(function(){

        $("#user_birthdate").datetimepicker({
            locale: 'it',
            format: "DD/MM/YYYY",
            ignoreReadonly: true
        });
        $("#user_birthdate").val($("#user_birthdate_value").val());

        $('#btn-save-shop').on('click', function () {
            $('input:invalid').each(function () {

                // Find the tab-pane that this element is inside, and get the id
                var $closest = $(this).closest('.tab-pane');
                var id = $closest.attr('id');

                // Find the link that corresponds to the pane and have it show
                $('.nav a[href="#' + id + '"]').tab('show');

                // Only want to do it once
                return false;
            });
        });

        $("#btn-address-copy").on('click', function(){
            $('#tax_address_components #country').val($('#shop_address_components #country').val());
            $('#tax_address_components #administrative_area_level_1').val($('#shop_address_components #administrative_area_level_1').val());
            $('#tax_address_components #locality').val($('#shop_address_components #locality').val());
            $('#tax_address_components #administrative_area_level_2').val($('#shop_address_components #administrative_area_level_2').val());
            $('#tax_address_components #route').val($('#shop_address_components #route').val());
            $('#tax_address_components #street_number').val($('#shop_address_components #street_number').val());
            $('#tax_address_components #postal_code').val($('#shop_address_components #postal_code').val());
            $('#tax_address_components #lat').val($('#shop_address_components #lat').val());
            $('#tax_address_components #long').val($('#shop_address_components #long').val());
        });

        $("#show-password").on('click', function(){
            console.log($(this).prop('checked'));
            if( $(this).prop('checked') ){
                $("#new_password").prop('type', 'text');
                $("#new_password_confirmation").prop('type', 'text');
            }else{
                $("#new_password").prop('type', 'password');
                $("#new_password_confirmation").prop('type', 'password');
            }
        });

        $( ".time-slider" ).each(function(k, val){

            let id = $(this).attr('id');
            let half = id.split('_')[2];
            let time1 = $(`input[name=${id}_open]`).val();
            let time2 = $(`input[name=${id}_close]`).val();

            let val1 = (half === 'mor') ? 0 : 720;
            let val2 = (half === 'mor') ? 1080 : 1440;

            if( time1.indexOf(":") > 0 ){
                val1 = parseInt(time1.split(":")[0]) * 60 + parseInt(time1.split(":")[1]);
            }else{
                if( half === 'mor' ){
                    time1 = '00:00';
                    time2 = '18:00';
                }else{
                    time1 = '12:00';
                    time2 = '24:00';
                }
            }

            if( time2.indexOf(":") > 0 ){
                val2 = parseInt(time2.split(":")[0]) * 60 + parseInt(time2.split(":")[1]);
            }

            $(`#lbl_${id}`).text(time1 + " - " + time2);

            $( this ).slider({
                range: true,
                min: (half === 'mor') ? 0 : 720,
                max: (half === 'mor') ? 1080 : 1440,
                step: 15,
                value: [ val1, val2 ],
            }).on('slide', function(ev){

                getTimes($(this).attr('id'), ev.value[0], ev.value[1]);

            }).on('slideStop', function(ev){

                getTimes($(this).attr('id'), ev.value[0], ev.value[1]);

            });

        });


        $('.slider-delivery').slider({
            min: 1,
            max: 15,
            value: {!! empty($user->shop->delivery_range_km) ? 1 : $user->shop->delivery_range_km !!},
            formatter: function(value) {
                return value + ' Km';
            }
        });

        $('.slider-trust-points').slider({
            min: 1,
            max: 100,
            value: {!! empty($user->shop->min_trust_points_percentage_bookable) ? 60 : $user->shop->min_trust_points_percentage_bookable !!},
            formatter: function(value) {
                return value;
            }
        });

        $('.slider-trust-points').slider().on('slide', function(ev){
            var newVal = $('.slider-trust-points').data('slider').getValue();
            $("#lbl_min_trust_points").html("LE OFFERTE SARANNO PRENOTABILI SOLO DAI CLIENTI CON UN MINIMO DI <span style=\"color: #13bf9e\">" + newVal + "</span> PUNTI FIDUCIA");
        });

        $('.slider-trust-points').slider().on('slideStop', function(ev){
            var newVal = $('.slider-trust-points').data('slider').getValue();
            $("#lbl_min_trust_points").html("LE OFFERTE SARANNO PRENOTABILI SOLO DAI CLIENTI CON UN MINIMO DI <span style=\"color: #13bf9e\">" + newVal + "</span> PUNTI FIDUCIA");
        });

        $('input[name="delivery_home_fees"]').on('change', function(){
            switch( $(this).val() ){
                case 'delivery_always_free':                $("#delivery_free_price_greater_than").val('');
                                                            $("#delivery_forfait_cost_price").val('');
                                                            $("#delivery_percentage_cost_price").val('');
                                                            break;

                case 'delivery_free_price_greater_than':    $("#delivery_forfait_cost_price").val('');
                                                            $("#delivery_percentage_cost_price").val('');
                                                            break;

                case 'delivery_forfait_cost_price':         $("#delivery_free_price_greater_than").val('');
                                                            $("#delivery_percentage_cost_price").val('');
                                                            break;

                case 'delivery_percentage_cost_price':      $("#delivery_free_price_greater_than").val('');
                                                            $("#delivery_forfait_cost_price").val('');
                                                            break;

            }
        });

        $("#control-delivery-address").on('click', function(){
            var checked = $(this).prop('checked');
            if( checked ){
                $('input[name="delivery_address"]').val('');
                $('input[name="delivery_address_country"]').val('');
                $('input[name="delivery_address_route"]').val('');
                $('input[name="delivery_address_city"]').val('');
                $('input[name="delivery_address_region"]').val('');
                $('input[name="delivery_address_postal_code"]').val('');
                $('input[name="delivery_address_street_number"]').val('');
                $('input[name="delivery_address_lat"]').val('');
                $('input[name="delivery_address_long"]').val('');
                $(".form-delivery-address").css('display', 'none');
            }else{
                $(".form-delivery-address").css('display', 'block');
            }
        });

        $("#control-delivery-timetable").on('click', function(){
            var checked = $(this).prop('checked');
            if( checked ){
                $(".form-delivery-timetable").css('display', 'none');
                $(".row-delivery-monday").find('input').val('');
                $(".row-delivery-tuesday").find('input').val('');
                $(".row-delivery-wednesday").find('input').val('');
                $(".row-delivery-thursday").find('input').val('');
                $(".row-delivery-friday").find('input').val('');
                $(".row-delivery-saturday").find('input').val('');
                $(".row-delivery-sunday").find('input').val('');

                $("#control-delivery-monday").attr('checked', false);
                $("#control-delivery-tuesday").attr('checked', false);
                $("#control-delivery-wednesday").attr('checked', false);
                $("#control-delivery-thursday").attr('checked', false);
                $("#control-delivery-friday").attr('checked', false);
                $("#control-delivery-saturday").attr('checked', false);
                $("#control-delivery-sunday").attr('checked', false);

                $(".row-delivery-monday").css('display', 'none');
                $(".row-delivery-tuesday").css('display', 'none');
                $(".row-delivery-wednesday").css('display', 'none');
                $(".row-delivery-thursday").css('display', 'none');
                $(".row-delivery-friday").css('display', 'none');
                $(".row-delivery-saturday").css('display', 'none');
                $(".row-delivery-sunday").css('display', 'none');

            }else{
                $(".form-delivery-timetable").css('display', 'block');
            }
        });

        $("#control-home-delivery").on('click', function(){
            var checked = $(this).prop('checked');
            if( checked ){
                $(".form-home-delivery").css('display', 'block');
            }else{
                $(".form-home-delivery").css('display', 'none');
            }
        });


        $('.control-open-day').on('click', function(){
            var day = $(this).data('day');
            var checked = $(this).prop('checked');

            if( checked ){
                $(".row-open-"+day).css('display', 'flex');
            }else{
                $(".row-open-"+day).css('display', 'none');
                $(".row-open-"+day).find('input').val('');
            }
        });

        $('.control-delivery-day').on('click', function(){
            var day = $(this).data('day');
            var checked = $(this).prop('checked');

            if( checked ){
                $(".row-delivery-"+day).css('display', 'flex');
            }else{
                $(".row-delivery-"+day).css('display', 'none');
                $(".row-delivery-"+day).find('input').val('');
            }
        });

        $("#chk_delivery_forfait_cost_price").on('click', function(){
            var checked = $(this).prop('checked');
            if(checked){
                $("#delivery_forfait_cost_price").show();
                $("input[name=delivery_percentage_cost_price]").val('');
                $("#delivery_percentage_cost_price").hide();
                $("#chk_delivery_percentage_cost_price").prop("checked", false);
            }else{
                $("input[name=delivery_forfait_cost_price]").val('');
                $("#delivery_forfait_cost_price").hide();
            }
        });

        $("#chk_delivery_percentage_cost_price").on('click', function(){
            var checked = $(this).prop('checked');
            if( !checked ){
                $("input[name=delivery_percentage_cost_price]").val('');
                $("#delivery_percentage_cost_price").hide();
            }else{
                $("#delivery_percentage_cost_price").show();
                $("input[name=delivery_forfait_cost_price]").val('');
                $("#delivery_forfait_cost_price").hide();
                $("#chk_delivery_forfait_cost_price").prop("checked", false);
            }
        });

        $("#chk_delivery_free_price_greater_than").on('click', function(){
            var checked = $(this).prop('checked');
            if( !checked ){
                $("#delivery_free_price_greater_than").val('');
                $("#delivery_free_price_greater_than").hide();
            }else{
                $("#delivery_free_price_greater_than").show();
            }
        });

        if( $('#table-payments') ){
            $('#table-payments').DataTable({
                "language": {
                    url: "{{ url('js/Italian.json') }}"
                },
                paging: true,
                stateSave: true,
                "initComplete": function (settings, json) {
                    $("#table-payments").wrap("<div style='overflow:auto; width:100%;position:relative;'></div>");
                },
                "bInfo": false
            });
        }

        $("#shop_status").on('click', function(){
            let shopStatus = $(this).val();
            if( shopStatus !== 'Aperto' ){
                $("#row-closing-dates").show();
                $("#row-shop-status").removeClass('bg-success');
                $("#row-shop-status").addClass('bg-warning');
            }else{
                $("#row-closing-dates").hide();
                $("#row-shop-status").removeClass('bg-warning');
                $("#row-shop-status").addClass('bg-success');
            }
        });
    });

    delShop = (shop_id) => {
        showConfirm("Elimina negozio", 'bg-danger', "Rimuovere l'attività selezionata? ATTENZIONE: Questa azione è irreversibile e i dati andranno persi definitivamente!", execRemove, [shop_id]);
    }

    execRemove = (shop_id) => {
        $.ajax({
            url: `/shops/${shop_id}`,
            data: {
                "_token": "{{ csrf_token() }}"
            },
            type: 'DELETE',
            dataType: 'json',
            statusCode: {
                401: function() {
                    document.location.href = '/login';
                },
                419: function() {
                    document.location.href = '/login';
                }
            },
            success: function(data){
                if( data.status == 'ok' ){
                    document.location.href="/shops"
                }
            }
        });
    }

	sendAdminRegistrationEmail = (shop_id) => {
        showConfirm("Invia email negozio", 'bg-danger', "Inviare email registrazione a questo negozio?", execSendAdminRegistrationEmail, [shop_id]);
	}

	execSendAdminRegistrationEmail = (shop_id) => {
		document.location.href=`/shops/sendAdminRegistrationEmail/${shop_id}`
	}

    getTimes = (id, time1, time2) => {
        var hours1 = ("0" + Math.floor(time1 / 60)).substr(-2);
        var minutes1 = ("0" + time1 % 60).substr(-2);

        var hours2 = ("0" + Math.floor(time2 / 60)).substr(-2);
        var minutes2 = ("0" + time2 % 60).substr(-2);

        $(`input[name=${id}_open]`).val(hours1 + ":" + minutes1);
        $(`input[name=${id}_close]`).val(hours2 + ":" + minutes2);

        $(`#lbl_${id}`).text(hours1 + ":" + minutes1 + " - " + hours2 + ":" + minutes2);
    }

    setCurrentTab = (val) => {
        $("#current_tab").val(val);
    }

    /*
    preview_image = (event) => {
        var reader = new FileReader();
        reader.onload = function()
        {
        var output = document.getElementById('output_image');
        output.src = reader.result;
        }
        reader.readAsDataURL(event.target.files[0]);
        $("#food_image").val('');

        $("#img-suggestion").text("Puoi cambiare l'immagine del profilo in qualsiasi momento");
        $("#btn-fileimage").text("Seleziona una nuova immagine");
    }
    */

    // This sample uses the Autocomplete widget to help the user select a
    // place, then it retrieves the address components associated with that
    // place, and then it populates the form fields with those details.
    // This sample requires the Places library. Include the libraries=places
    // parameter when you first load the API. For example:
    // <script
    // src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

    var placeSearch, autocomplete, autocomplete2, autocomplete3;

    var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        administrative_area_level_2: 'short_name',
        country: 'long_name',
        postal_code: 'short_name',
        lat: 'lat',
        long: 'long'
    };

    function initAutocomplete() {
        // Create the autocomplete object, restricting the search predictions to
        // geographical location types.
        var options = {
            types: ['establishment', 'geocode']
        };

        autocomplete  = new google.maps.places.Autocomplete(document.getElementById('citta'), options);
        autocomplete2 = new google.maps.places.Autocomplete(document.getElementById('delivery_citta'), options);
        //autocomplete3 = new google.maps.places.Autocomplete(document.getElementById('tax_citta'), options);

        // Avoid paying for data that you don't need by restricting the set of
        // place fields that are returned to just the address components.
        autocomplete.setFields(['address_component']);
        autocomplete2.setFields(['address_component']);
        //autocomplete3.setFields(['address_component']);

        // When the user selects an address from the drop-down, populate the
        // address fields in the form.

        autocomplete.addListener('place_changed', fillInAddress);
        autocomplete2.addListener('place_changed', fillInAddress2);
        //autocomplete3.addListener('place_changed', fillInAddress3);
    }

    function fillInAddress() {
        var container = document.getElementById('shop_address_components');
        for (var component in componentForm) {
            container.querySelector('.'+component).value = '';
            container.querySelector('.'+component).disabled = false;
        }
        var place = autocomplete.getPlace();

        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (componentForm[addressType]) {
                var val = place.address_components[i][componentForm[addressType]];
                container.querySelector('.'+addressType).value = val;
            }
        }
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': document.getElementById('citta').value }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                container.querySelector(".lat").value = results[0].geometry.location.lat();
                container.querySelector(".long").value = results[0].geometry.location.lng();
            }
        });
    }

    function fillInAddress2() {
        var container = document.getElementById('delivery_address_components');
        for (var component in componentForm) {
            console.log("component", component);
            container.querySelector('.'+component).value = '';
            container.querySelector('.'+component).disabled = false;
        }
        var place = autocomplete2.getPlace();
        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (componentForm[addressType]) {
                var val = place.address_components[i][componentForm[addressType]];
                container.querySelector('.'+addressType).value = val;
            }
        }
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': document.getElementById('delivery_citta').value }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                container.querySelector(".lat").value = results[0].geometry.location.lat();
                container.querySelector(".long").value = results[0].geometry.location.lng();
            }
        });
    }

    function fillInAddress3() {
        var container = document.getElementById('tax_address_components');
        for (var component in componentForm) {
            console.log("component", component);
            container.querySelector('.'+component).value = '';
            container.querySelector('.'+component).disabled = false;
        }
        var place = autocomplete3.getPlace();
        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (componentForm[addressType]) {
                var val = place.address_components[i][componentForm[addressType]];
                container.querySelector('.'+addressType).value = val;
            }
        }
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': document.getElementById('tax_citta').value }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                container.querySelector(".lat").value = results[0].geometry.location.lat();
                container.querySelector(".long").value = results[0].geometry.location.lng();
            }
        });
    }

    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyCOLy_YdwFPimPjEzpXY6nQwDRUSlMHZsg&libraries=places&callback=initAutocomplete';
    document.body.appendChild(script);
</script>
@endsection
