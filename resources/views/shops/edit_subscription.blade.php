@extends('adminlte::page')

@section('content')
<h3>{{ $user->shop->name }}</h3>

@php
    $expire_date = @\Carbon\Carbon::parse(old('subscription_date', $user->subscription->expire_date->format('Y-m-d')));
@endphp

<div class="clearfix"></div>

@include('flash::message')

{!! Form::open(array('url' => route('shopSubscription.update'), 'method' => 'post', 'id' => 'frm-edit-shop-subscription')) !!}

<input type="hidden" name="shop_id" value="{{ $user->shop->id }}">

<div class="row">

    {{--
    <div class="col-12">
        User id: {{ $user->id }}<br/>
        Shop id: {{ $user->shop->id }}<br/>
        User subscription id: {{ $user->subscription->id }}<br/>
        Partita IVA: {{ $user->shop->tax_vat }}
    </div>
     --}}

    <div class="form-group col-12 col-sm-12 col-md-6 col-lg-6">
        <label for="subscription_id">Abbonamento</label>
        <select id="subscription_id" name="subscription_id" class="form-control">
            @foreach($subscriptions as $subscription)
            <option value="{{ $subscription->subscription }}" @if( $subscription->subscription === old('subscription_id', $user->subscription->subscription) ) selected @endif>{{ $subscription->name.' ('.$subscription->products_num.' prodotti)' }}</option>
            @endforeach
        </select>
    </div>
@if($user->subscription->subscription != 'free' || !empty($user->shop->agent_id))
    <div class="form-group col-12 col-sm-12 col-md-6 col-lg-6 @if( $expire_date->isPast() ) has-error @endif">
        <label for="subscription_date">Data di scadenza</label>
        <input type="date" class="form-control @if( $expire_date->isPast() ) has-error @endif" id="subscription_date" name="subscription_date" value="{{ old('subscription_date', $user->subscription->expire_date->format('Y-m-d')) }}">
        @if( $expire_date->isPast() )
            <div class="alert alert-danger mt-1">L'abbonamento è scaduto</div>
        @endif
    </div>
@endif
	@php
		if($num_product_default){
			$product_num = 0;
		} else {
			$product_num = $user->subscription->subscription_product_num;
		}	
	@endphp
	<div class="form-group col-12 col-sm-12 col-md-6 col-lg-6">
        <label for="subscription_product_num">Numero prodotti personalizzato per l'abbonamento selezionato (inserire zero per il default)</label>
        <input type="number" class="form-control" id="subscription_product_num" name="subscription_product_num" value="{{ old('subscription_product_num', $product_num) }}">
    </div>

    @if( $other_shops_same_subscription->count() > 0 )
    <div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
        <label for="other_shops_same_subscription">Altre attività incluse nell'abbonamento</label>
        <table class="table table-bordered table-hover">
            <thead class="bg-secondary">
                <tr>
                    <th>Rimuovi</th>
                    <th>Nome attività</th>
                    <th>Indirizzo</th>
                </tr>
            </thead>
            <tbody>
            @foreach($other_shops_same_subscription as $otherShop)
            <tr>
                <td><input type="checkbox" name="removeShops[]" value="{{ $otherShop->id }}"></td>
                <td>{{ $otherShop->name }}</td>
                <td>{{ $otherShop->address }}</td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    @endif
</div>

@php
$remaining = 0;
if( $user->subscription->subscription !== 'trial' ){
    $remaining = $subscriptions[$user->subscription->subscription]->shops_num - $shops_in_subscription; //$other_shops_same_subscription->count() -1;
}
@endphp

@if( $user->status === 'active' )
    @if( $remaining > 0 && count($other_shops) > 0)
    <section class="bg-warning p-4 my-4 rounded">
        <div class="row">
            <div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
                <h3><strong>Puoi aggiungere {{ $remaining }} attività nello stesso abbonamento</strong></h3>

                <p>Seleziona attività</p>
                <table class="table table-striped bg-light">

                    @foreach($other_shops as $otherShop)
                    <tr>
                        <td><input type="checkbox" id="shop_id_{{ $otherShop->id }}" name="otherShops[]" value="{{ $otherShop->id }}"></td>
                        <td>{{ $otherShop->name }}</td>
                        <td>{{ $otherShop->address }}</td>
                    </tr>
                    @endforeach

                </table>

            </div>
        </div>
    </section>
    @elseif( $remaining > 0 && count($other_shops) === 0)
        <div class="text-center">
            <span class="badge badge-pill badge-warning my-4"><i class="fas fa-exclamation-triangle"></i> Puoi ancora associare {{ $remaining }} attività a questo abbonamento</span>
        </div>
    @elseif( $remaining === 0 )
        <div class="text-center">
            <span class="badge badge-pill badge-danger my-4"><i class="fas fa-exclamation-triangle"></i> Hai raggiunto il numero massimo di attività che puoi associare a questo abbonamento</span>
        </div>
    @endif
@endif

<section class="bg-primary p-2 rounded">
    <h3>Registra transazione</h3>
    <div class="row">
        <div class="form-group col-6">
            <label for="subscription_transaction_price">Importo</label>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1">&euro;</span>
                </div>
                <input type="number" id="subscription_transaction_price" name="subscription_transaction_price" value="{{ old('subscription_transaction_price', '') }}" class="form-control" min="0.01" max="999999.99" step="any">
            </div>
            @error('subscription_transaction_price')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group col-6">
            <label for="subscription_transaction_mop">Metodo di pagamento</label>
            <select id="subscription_transaction_mop" name="subscription_transaction_mop" class="form-control">
                <option value="bonifico" @if( old('subscription_transaction_mop', 'bonifico') === 'bonifico') selected @endif>Bonifico</option>
                <option value="contanti" @if( old('subscription_transaction_mop', 'bonifico') === 'contanti') selected @endif>Contanti</option>
            </select>
        </div>

        <div class="form-group col-12">
            <label for="subscription_transaction_desc">Causale</label>
            <input type="text" id="subscription_transaction_desc" name="subscription_transaction_desc" value="{{ old('subscription_transaction_desc', '') }}" class="form-control">
            @error('subscription_transaction_desc')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
    </div>
    <p>
        <small>Per registrare la transazione compilare tutti i campi</small>
    </p>
</section>

<div class="mt-4 text-center">
    <Button type="submit" class="btn btn-success">Salva</Button>
</div>

{!! Form::close() !!}

@if( $user->invoices )
    <section class="bg-secondary mt-4 p-2 rounded">
        <h3>Storico transazioni</h3>
        <div class="table-responsive">
            <table id="table-payments" class="table table-striped table-hover bg-light">
                <thead>
                    <th>Data</th>
                    <th>Utente</th>
                    <th>Causale</th>
                    <th>Importo</th>
                    <th>Metodo di pagamento</th>
                </thead>
                <tbody>
                    @foreach($user->invoices as $payment)
                    <tr>
                        <td>{{ $payment->created_at->format('d/m/Y H:i') }}</td>
                        <td>@if( $payment->user) {{ $payment->user->name }} @endif</td>
                        <td>{{ $payment->title }}</td>
                        <td>{{ number_format($payment->price, 2, ',', '.') }} &euro;</td>
                        <td>{{ $payment->method_of_payment }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </section>
@endif

@endsection
