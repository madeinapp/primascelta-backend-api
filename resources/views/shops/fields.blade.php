<!-- User Id Field -->
<!--div class="form-group col-sm-6">
    {!! Form::label('user_id', 'User Id:') !!}
    {!! Form::number('user_id', null, ['class' => 'form-control']) !!}
</div-->
<div class="row">
<!-- Tax Business Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tax_business_name', __('common.Tax Business Name').':') !!}
    {!! Form::text('tax_business_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Shop Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('type', __('common.Shop Types').':') !!}
    {!! Form::select('shop_type_id', $arr_shop_types, null, ['class' => 'form-control']) !!}
</div>

<!-- Tax Vat Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tax_vat', __('common.Tax Vat').':') !!}
    {!! Form::text('tax_vat', null, ['class' => 'form-control']) !!}
</div>

<!-- Tax Fiscal Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tax_fiscal_code', __('common.Tax Fiscal Code').':') !!}
    {!! Form::text('tax_fiscal_code', null, ['class' => 'form-control']) !!}
</div>

<!-- Tax Address Field -->
<div class="form-group col-sm-6 col-lg-6">
    {!! Form::label('tax_address', __('common.Tax Address').':') !!}
    {!! Form::textarea('tax_address', null, ['class' => 'form-control', 'rows'=>2]) !!}
</div>

<!-- Min Trust Point Field -->
<div class="form-group col-sm-6">
    {!! Form::label('min_trust_point', __('common.Min Trust Point').':') !!}
    {!! Form::number('min_trust_point', null, ['class' => 'form-control']) !!}
</div>
<!-- Description Field -->
<div class="form-group col-sm-6 col-lg-6">
    {!! Form::label('description', __('common.description').':') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control', 'rows' => 3]) !!}
</div>

<!-- Delivery Methods Field -->
<div class="form-group col-sm-6">
    {!! Form::label('delivery_methods', __('common.Delivery Methods').':') !!}
    {!! Form::select('delivery_methods[]', $arr_delivery_methods, null, ['multiple' => 'multiple', 'class' => 'form-control']) !!}
</div>

<!-- Delivery Range Km Field -->
<div class="form-group col-sm-6">
    {!! Form::label('delivery_range_km', __('common.Delivery Range Km').':') !!}
    {!! Form::number('delivery_range_km', null, ['class' => 'form-control']) !!}
</div>

<!-- Excluded Commons Field -->
<div class="form-group col-sm-6 col-lg-6">
    {!! Form::label('excluded_commons', __('common.Excluded Commons').':') !!}
    {!! Form::textarea('excluded_commons', null, ['class' => 'form-control', 'rows' => 2]) !!}
</div>

<!-- Payment Methods Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_methods', __('common.Payment Methods').':') !!}
    {!! Form::select('payment_method_id', $arr_payment_methods, null, ['class' => 'form-control']) !!}
</div>

<!-- Website Field -->
<div class="form-group col-sm-6 col-lg-6">
    {!! Form::label('website', __('common.Website').':') !!}
    {!! Form::textarea('website', null, ['class' => 'form-control', 'rows' => 1]) !!}
</div>

<!-- Facebook Page Field -->
<div class="form-group col-sm-6">
    {!! Form::label('facebook_page', __('common.Facebook Page').':') !!}
    {!! Form::text('facebook_page', null, ['class' => 'form-control']) !!}
</div>

<!-- Instagram Page Field -->
<div class="form-group col-sm-6">
    {!! Form::label('instagram_page', __('common.Instagram Page').':') !!}
    {!! Form::text('instagram_page', null, ['class' => 'form-control']) !!}
</div>

<!-- Opening Timetable Field -->
<div class="form-group col-sm-6 col-lg-6">
    {!! Form::label('opening_timetable', __('common.Opening Timetable').':') !!}
    {!! Form::textarea('opening_timetable', null, ['class' => 'form-control', 'rows' => 1]) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::text('status', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('common.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('shops.index') }}" class="btn btn-default btn-full">{{ __('common.cancel') }}</a>
</div>
</div>
