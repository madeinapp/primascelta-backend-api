@extends('adminlte::page')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">{{ __('common.shops') }}</h1>
@if(Auth::user()->role == 'admin')
        <div class="pull-right  float-right d-none d-sm-block" >
           <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{{ route('shops.create') }}">{{ __('common.Add New') }}</a>
		</div>
@endif
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        {{--
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        --}}

        <div class="clearfix"></div>
        <div class="card card-primary">
            <div class="card-body">
                    @include('shops.table')
            </div>
        </div>
        <div class="text-center">

        </div>
    </div>
@endsection

