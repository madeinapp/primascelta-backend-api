@extends('adminlte::page')

@section('content')
    <section class="content-header">
        <h1>
            Shop
        </h1>
    </section>
    <div class="content">
        <div class="card card-primary">
            <div class="card-body">
                <div class="row" style="padding-left: 20px">
                    @include('shops.show_fields')
                    <a href="{{ route('shops.index') }}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
