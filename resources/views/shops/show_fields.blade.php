<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{{ $shop->user_id }}</p>
</div>

<!-- Tax Business Name Field -->
<div class="form-group">
    {!! Form::label('tax_business_name', 'Tax Business Name:') !!}
    <p>{{ $shop->tax_business_name }}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{{ $shop->description }}</p>
</div>

<!-- Checkout Delivery Note Field -->
<div class="form-group">
    {!! Form::label('checkout_delivery_note', 'Checkout Delivery Note:') !!}
    <p>{{ $shop->checkout_delivery_note }}</p>
</div>

<!-- Delivery Methods Field -->
<div class="form-group">
    {!! Form::label('delivery_methods', 'Delivery Methods:') !!}
    <p>{{ $shop->delivery_methods }}</p>
</div>

<!-- Website Field -->
<div class="form-group">
    {!! Form::label('website', 'Website:') !!}
    <p>{{ $shop->website }}</p>
</div>

<!-- Facebook Page Field -->
<div class="form-group">
    {!! Form::label('facebook_page', 'Facebook Page:') !!}
    <p>{{ $shop->facebook_page }}</p>
</div>

<!-- Instagram Page Field -->
<div class="form-group">
    {!! Form::label('instagram_page', 'Instagram Page:') !!}
    <p>{{ $shop->instagram_page }}</p>
</div>

<!-- Opening Timetable Field -->
<div class="form-group">
    {!! Form::label('opening_timetable', 'Opening Timetable:') !!}
    <p>{{ $shop->opening_timetable }}</p>
</div>

<!-- Home Delivery Field -->
<div class="form-group">
    {!! Form::label('home_delivery', 'Home Delivery:') !!}
    <p>{{ $shop->home_delivery }}</p>
</div>

<!-- Delivery Range Km Field -->
<div class="form-group">
    {!! Form::label('delivery_range_km', 'Delivery Range Km:') !!}
    <p>{{ $shop->delivery_range_km }}</p>
</div>

<!-- Excluded Commons Field -->
<div class="form-group">
    {!! Form::label('excluded_commons', 'Excluded Commons:') !!}
    <p>{{ $shop->excluded_commons }}</p>
</div>

<!-- Payment Methods Field -->
<div class="form-group">
    {!! Form::label('payment_methods', 'Payment Methods:') !!}
    <p>{{ $shop->payment_methods }}</p>
</div>

<!-- Min Trust Point Field -->
<div class="form-group">
    {!! Form::label('min_trust_point', 'Min Trust Point:') !!}
    <p>{{ $shop->min_trust_point }}</p>
</div>


<!-- Type Field -->
<div class="form-group">
    {!! Form::label('type', 'Type:') !!}
    <p>{{ $shop->type }}</p>
</div>

<!-- Tax Vat Field -->
<div class="form-group">
    {!! Form::label('tax_vat', 'Tax Vat:') !!}
    <p>{{ $shop->tax_vat }}</p>
</div>

<!-- Tax Fiscal Code Field -->
<div class="form-group">
    {!! Form::label('tax_fiscal_code', 'Tax Fiscal Code:') !!}
    <p>{{ $shop->tax_fiscal_code }}</p>
</div>

<!-- Tax Address Field -->
<div class="form-group">
    {!! Form::label('tax_address', 'Tax Address:') !!}
    <p>{{ $shop->tax_address }}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $shop->status }}</p>
</div>

