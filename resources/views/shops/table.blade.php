<div class="table-responsive">
    <table class="table" id="shops-table">
        <thead>
            <tr>
                <th >{{ __('common.action') }}</th>
                <th>{{ __('common.Owner') }}</th>
                <th>Abbonamento</th>
                <th>{{ __('common.Shop Types') }}</th>
                <th>Nome attività</th>
                <th>Comune</th>
                <th>Prov</th>
                <th>Generato</th>
                <th>Avvisato</th>
                <th>Test</th>
                <th>Prod.</th>
                <th>{{ __('common.Agent') }}</th>
                <th>Stato</th>
            </tr>
        </thead>
        <tbody>
        @foreach($shops as $shop)
            <tr>
                <td>
                    {!! Form::open(['route' => ['shops.destroy', $shop->id], 'method' => 'delete']) !!}

                        <a href="{{ route('shops.edit', [$shop->id]) }}" class='btn btn-default btn-sm m-1' title="Modifica Attività"><i class="fas fa-edit"></i></a>
                        <a href="{{ route('shopSubscription.edit', [$shop->id]) }}" class='btn btn-default btn-sm m-1' title="Abbonamento"><i class="fas fa-credit-card"></i></a>

                        @if ($shop->user->status == "active" || $shop->user->status == "pending")
                            <a href="user/disable/{{ $shop->user->id }}" class='btn btn-danger btn-sm m-1'><i class="fas fa-user-times"></i></a>
                        @endif

                        @if ($shop->user->status == "inactive" || $shop->user->status == "pending")
                            <a href="user/enable/{{ $shop->user->id }}" class='btn btn-success btn-sm m-1'><i class="fas fa-user-check green"></i></a>
                        @endif

                    {!! Form::close() !!}
                </td>
                <td>{{ $shop->user->name }}</td>
                <td>
                    {{ $shop->user->subscription->subscription }}
                    <br>
                    <span @if( $shop->user->subscription->expire_date->isPast() ) class="text-danger" @endif>
                        <small>
                            <strong>
								@if($shop->user->subscription->subscription != 'free' || ($shop->user->subscription->subscription == 'free' && !empty($shop->agent_id)))
	                                @if( $shop->user->subscription->expire_date->isPast() )
    	                            Scaduto
        	                        @else
            	                    Scade
                	                @endif il {{ $shop->user->subscription->expire_date->format('d/m/Y') }}
								@endif
                            </strong>
                        </small>
                    </span>
                </td>
                <td>{{ $shop->shopType }}</td>
                <td>{{ $shop->name }}</td>
                <td>{{ $shop->city }}</td>
                <td>{{ $shop->prov }}</td>
                <td>{{ empty($shop->admin_generated) ? 'NO' : 'SI' }}</td>
                <td>{{ empty($shop->admin_sent_email_shop_created) ? 'NO' : 'SI' }}</td>
                <td>{{ empty($shop->admin_test_shop) ? 'NO' : 'SI' }}</td>
                <td>{{ $shop->shopCatalogs->count() }}</td>
                <td>
                    @if( $shop->agent )
                    {{ $shop->agent->name }}
                    @endif
                </td>
                <td>
                    @if( $shop->status === 'active' )
                        <span class="badge badge-success"><i class="fas fa-check"></i> Attivo</span>
                    @elseif( $shop->status === 'pending' )
                        <span class="badge badge-warning"><i class="fa fa-clock"></i> In attesa</span>
                    @elseif( $shop->status === 'inactive' )
                        <span class="badge badge-danger"><i class="fas fa-times-circle"></i> Disabilitato</span>
                    @elseif( $shop->status === 'invisible' )
                        <span class="badge badge-light"><i class="fas fa-user-secret"></i> Invisibile</span>
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
@section('js')
<script>
$(function () {
    $('#shops-table').DataTable({
        "language": {
            url: "{{ url('js/Italian.json') }}"
        },
        "paging":   true,
        "initComplete": function (settings, json) {
            $("#shops-table").wrap("<div style='overflow:auto; width:100%;position:relative;'></div>");
        },
        pageLength: 10,
        stateSave: true,
        columnDefs: [
            { width: "10%", orderable: false, targets: 0 },
            { width: "5%", orderable: true, targets: 5 },
            { width: "5%", orderable: true, targets: 6 }
        ]
    });
});
</script>
@endsection
