@extends('adminlte::page')

@section('content')
    <section class="content-header">
        <h1>Caratteristiche speciali</h1>
    </section>
    <div class="content">
        {{-- @include('adminlte-templates::common.errors') --}}
        <div class="card card-primary">
            <div class="card-body">
                <div class="card-body">
                    {!! Form::open(['route' => 'specials.store', 'files' => true]) !!}

                        @include('specials.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
