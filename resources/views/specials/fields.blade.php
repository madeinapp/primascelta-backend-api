<div class="form-group">
    {!! Form::label('name', 'Nome') !!}
    {!! Form::text('name', old('name', $special->name ?? ''), ['class' => 'form-control', 'placeholder' => 'Inserisci una caratteristica speciale di un alimento. Es: Senza glutine', 'required' => 'required']) !!}
</div>

<button type="submit" class="btn btn-success btn-full">Salva</button>
<a href="{{ route('specials.index') }}" class="btn btn-light btn-full">Annulla</a>
