@extends('adminlte::page')

@section('content')
    <section class="content-header">
        <h1 class="pull-left" style="display: inline-block">Caratteristiche speciali</h1>
        <div class="text-right" style="width: 100%;">
            <a class="btn btn-primary" style="margin-top: -10px; margin-bottom: 5px" href="{{ route('specials.create') }}">Aggiungi caratteristica</a>
        </div>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="card card-primary">
            <div class="card-body">
                @include('specials.table')
            </div>
        </div>
    </div>
@endsection
