<table id="specials-table" class="table table-striped table-hover">
    <thead>
        <th>Azioni</th>
        <th>Nome</th>
    </thead>
    <tbody>
        @foreach($specials as $special)
        <tr>
            <td>
                {!! Form::open(['route' => ['specials.destroy', $special->id], 'method' => 'delete']) !!}
                    <a href="{{ route('specials.edit', ['special' => $special->id]) }}" class="btn btn-light btn-sm">
                        <i class="fa fa-edit"></i>
                    </a>
                    {!! Form::button('<i class="fas fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-sm', 'onclick' => "return confirm('Sei sicuro?')"]) !!}
                {!! Form::close() !!}
            </td>
            <td>{{ $special->name }}</td>
        </tr>
        @endforeach
    </tbody>
</table>

@section('js')
<script>
$(function () {
    $('#specials-table').DataTable({
        "language": {
            url: "{{ url('js/Italian.json') }}"
        },
        paging:   true,
        "initComplete": function (settings, json) {
            $("#deliveryMethods-table").wrap("<div style='overflow:auto; width:100%;position:relative;'></div>");
        },
        pageLength: 10,
        stateSave: true,
        columnDefs: [
            { width: "10%", orderable: false, targets: 0 }
        ]
    });
});
</script>
@endsection
