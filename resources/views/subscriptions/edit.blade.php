@extends('adminlte::page')

@section('content')
<div class="content">
    <section class="content-header">
        <h1 class="pull-left">Abbonamenti</h1>
    </section>
    <div class="clearfix"></div>

    @include('flash::message')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="clearfix"></div>

    <div class="card card-primary">
        {!! Form::open(array('url' => route('subscriptions.update', ['subscription' => $subscription->id]), 'method' => 'PUT')) !!}

        @csrf

        <div class="card-body">
            <div class="row">
                <div class="form-group col-12 col-sm-12 col-md-12 col-lg-12">
                    <label for="name">Nome</label>
                    <input type="text" class="form-control" id="name" name="name" value="{{ old("name", $subscription->name) }}" required>
                </div>
                <div class="form-group col-12 col-sm-6 col-md-2 col-lg-2">
                    <label for="shops_num">Numero attività</label>
                    <input type="number" class="form-control" id="shops_num" name="shops_num" value="{{ old("shops_num", $subscription->shops_num) }}" required>
                </div>
                <div class="form-group col-12 col-sm-6 col-md-10 col-lg-10">
                    <label for="shops_note">Nota attività</label>
                    <textarea class="form-control" id="shops_note" name="shops_note">{{ old('shops_note', $subscription->shops_note) }}</textarea>
                </div>
                <div class="form-group col-12 col-sm-6 col-md-2 col-lg-2">
                    <label for="products_num">Numero prodotti</label>
                    <input type="number" class="form-control" id="products_num" name="products_num" value="{{ old("products_num", $subscription->products_num) }}" required>
                </div>
                <div class="form-group col-12 col-sm-6 col-md-10 col-lg-10">
                    <label for="products_note">Nota prodotti</label>
                    <textarea class="form-control" id="products_note" name="products_note">{{ old('products_note', $subscription->products_note) }}</textarea>
                </div>
                <div class="form-group col-12 col-sm-6 col-md-3 col-lg-3">
                    <label>Canone mensile per abbonamento annuale</label>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">&euro;</span>
                        </div>
                        <input type="text" id="price_yearly" name="price_yearly" value="{{ old('price_yearly', number_format($subscription->price_yearly, 2, ',', '.') ) }}" class="form-control" required>
                    </div>
                </div>
                <div class="form-group col-12 col-sm-6 col-md-3 col-lg-3">
                    <label>Canone mensile per abbonamento  semestrale</label>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">&euro;</span>
                        </div>
                        <input type="text" id="price_half_yearly" name="price_half_yearly" value="{{ old('price_half_yearly', number_format($subscription->price_half_yearly, 2, ',', '.') ) }}" class="form-control" required>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <h2 style="display: inline-block">Caratteristiche abbonamento</h2>
                    <div class="form-group">
                        <button type="button" class="btn btn-success pull-right" onclick="addSubscriptionFeature({{ $subscription->id }})">
                            <i class="fa fa-plus"></i> Aggiungi
                        </button>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>Ord.</th>
                                    <th style="width: 30%">Caratteristica</th>
                                    <th>Nota</th>
                                    <th style="width: 120px">Azioni</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($subscription->subscriptionFeatures as $feature)
                                <tr>
                                    <td>{{ $feature->order }}</td>
                                    <td>{{ $feature->feature_name }}</td>
                                    <td>{{ $feature->feature_note }}</td>
                                    <td>
                                        <a onclick="updateSubscriptionFeature({{ $feature->id }})" class="btn btn-sm btn-default"><i class="fa fa-edit"></i></a>
                                        <button type="button" class="btn btn-danger btn-sm" onclick="delSubscriptionFeature({{ $feature->id }})"><i class="fa fa-trash"></i></button>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <input type="hidden" name="subscription_id" value="{{ $subscription->id }}">
            <input type="hidden" name="subscription" value="{{ $subscription->subscription }}">
            <button type="submit" class="btn btn-success btn-full">Salva</button>
            <a href="{{ route('subscriptions.index') }}" class="btn btn-default btn-full">Annulla</a>
        </div>

        {!! Form::close() !!}
    </div>
</div>

@include('subscriptions.modal_subscription_feature')

@endsection

@section('js')
<script>
    $(document).ready(function(){

        $("#frm-subscription-feature").on('submit', function(e){
            e.preventDefault();

            let subscription_id = $("#frm-subscription-feature #subscription_id").val();
            let subscription_feature_id = $("#frm-subscription-feature #subscription_feature_id").val();
            let feature_name    = $("#frm-subscription-feature #feature_name").val();
            let feature_note    = $("#frm-subscription-feature #feature_note").val();
            let order    = $("#frm-subscription-feature #order").val();
            let url = '/subscriptions/feature/save';
            let type = 'POST';

            if( subscription_feature_id !== '' ){
                url = `/subscriptions/feature/${subscription_feature_id}`;
                type = 'PUT';
            }

            $.ajax({
                url: url,
                data: {
                    "_token": "{{ csrf_token() }}",
                    subscription_id: subscription_id,
                    feature_name: feature_name,
                    feature_note: feature_note,
                    order: order
                },
                type: type,
                dataType: 'json',
                statusCode: {
                    401: function() {
                        document.location.href = '/login';
                    },
                    419: function() {
                        document.location.href = '/login';
                    }
                },
                success: function(data){
                    console.log("data");
                    if( data.status == 'ok' ){
                        document.location.reload();
                    }
                }
            });
        });

    });

    function addSubscriptionFeature(subscription_id){
        $("#modal-subscription-feature #subscription_id").val(subscription_id);
        $("#modal-subscription-feature #subscription_feature_id").val('');
        $("#modal-subscription-feature #feature_name").val('');
        $("#modal-subscription-feature #feature_note").val('');
        $("#modal-subscription-feature #order").val('');
        $("#modal-subscription-feature .modal-title").text("Inserisci caratteristica");
        $("#modal-subscription-feature").modal('show');
    }

    function updateSubscriptionFeature(subscription_feature_id){
        $.ajax({
                url: `/subscriptions/feature/${subscription_feature_id}`,
                data: {
                    "_token": "{{ csrf_token() }}",
                },
                type: 'GET',
                dataType: 'json',
                statusCode: {
                    401: function() {
                        document.location.href = '/login';
                    },
                    419: function() {
                        document.location.href = '/login';
                    }
                },
                success: function(data){
                    if( data.status == 'ok' ){
                        console.log(data.subscriptionFeature);
                        $("#modal-subscription-feature #subscription_id").val(data.subscriptionFeature.subscription_id);
                        $("#modal-subscription-feature #subscription_feature_id").val(data.subscriptionFeature.id);
                        $("#modal-subscription-feature #feature_name").val(data.subscriptionFeature.feature_name);
                        $("#modal-subscription-feature #feature_note").val(data.subscriptionFeature.feature_note);
                        $("#modal-subscription-feature #order").val(data.subscriptionFeature.order);
                        $("#modal-subscription-feature .modal-title").text("Aggiorna caratteristica");
                        $("#modal-subscription-feature").modal('show');
                    }
                }
            });
    }

    function delSubscriptionFeature(subscription_feature_id){
        showConfirm("Abbonamento", 'bg-danger', "Eliminare la voce selezionata?", execDelSubscriptionFeature, [subscription_feature_id]);
    }

    function execDelSubscriptionFeature(subscription_feature_id){
        $.ajax({
            url: `/subscriptions/feature/${subscription_feature_id}`,
            data: {
                "_token": "{{ csrf_token() }}",
            },
            type: 'DELETE',
            dataType: 'json',
            statusCode: {
                401: function() {
                    document.location.href = '/login';
                },
                419: function() {
                    document.location.href = '/login';
                }
            },
            success: function(data){
                if( data.status == 'ok' ){
                    document.location.reload();
                }
            }
        });
    }
</script>
@endsection
