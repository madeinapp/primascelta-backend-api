@extends('adminlte::page')

@section('content')
<div class="content">
    <section class="content-header">
        <h1 class="pull-left">Abbonamenti</h1>
    </section>
    <div class="clearfix"></div>

    @include('flash::message')

    <div class="clearfix"></div>

    <div class="card card-primary">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead class="bg-warning">
                        <tr>
                            <th>Nome</th>
                            <th>Num. Attività</th>
                            <th>Num. Prodotti</th>
                            <th>Canone mensile per abbonamento annuale</th>
                            <th>Canone mensile per abbonamento semestrale</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($subscriptions as $subscription)
                        <tr>
                            <td>{{ $subscription->name }}</td>
                            <td>{{ $subscription->shops_num }}</td>
                            <td>{{ $subscription->products_num }}</td>
                            <td>{{ number_format($subscription->price_yearly, 2, ',', '.') }} &euro;</td>
                            <td>{{ number_format($subscription->price_half_yearly, 2, ',', '.') }} &euro;</td>
                            <td><a href="{{ route('subscriptions.edit', ['subscription' => $subscription->id]) }}" class="btn btn-default"><i class="fa fa-edit"></i></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
