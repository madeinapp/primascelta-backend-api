<div class="modal fade" id="modal-subscription-feature" aria-hidden="true" data-backdrop="static" style="display: none;">
    <div class="modal-dialog modal-lg">
        <form id="frm-subscription-feature" action="">
            <div class="modal-content">
                <div class="modal-header bg-success">
                    <h4 class="modal-title">Inserisci caratteristica</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-2 col-lg-2">
                            <div class="form-group">
                                {!! Form::label('order', 'Ordinamento') !!}
                                {!! Form::number('order', null, ['class' => 'form-control', 'required' => 'required', 'min' => 1]) !!}
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-10 col-lg-10">
                            <div class="form-group">
                                {!! Form::label('feature_name', 'Nome caratteristica') !!}
                                {!! Form::text('feature_name', null, ['class' => 'form-control', 'required' => 'required']) !!}
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="form-group">
                                {!! Form::label('feature_note', 'Nota') !!}
                                {!! Form::textarea('feature_note', null,  ['class' => 'form-control', 'rows' => 4]) !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="subscription_id" value="">
                    <input type="hidden" id="subscription_feature_id" value="">
                    <button type="submit" class="btn btn-success btn-full">Salva</button>
                </div>
            </div>
        </form>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
