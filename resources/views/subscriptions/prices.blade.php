@extends('adminlte::page')

@section('css')
<style>
    .btn-primary:hover,
    .btn-primary:active,
    .btn-primary:visited,
    .btn-primary:focus {
        background-color: darkblue !important;
        color: white !important;
    }

    .btn-primary {
        color: white !important;
        background-color: steelblue !important;
        width: 100%;
    }


    .btn-outline-primary:hover,
    .btn-outline-primary:active,
    .btn-outline-primary:visited,
    .btn-outline-primary:focus {
        background-color: lightblue !important;
        border-color: steelblue !important;
        color: #000 !important;
    }

    .btn-outline-primary {
        color: steelblue !important;
        background-color: white !important;
        width: 100%;
    }

    .btn-outline-warning:hover,
    .btn-outline-warning:active,
    .btn-outline-warning:visited,
    .btn-outline-warning:focus {
        background-color: lightyellow !important;
        color: #000 !important;
    }

    .btn-outline-success:hover,
    .btn-outline-success:active,
    .btn-outline-success:visited,
    .btn-outline-success:focus {
        background-color: lightgreen !important;
        color: #000 !important;
    }

    .text-primary{
        color: steelblue !important;
    }

    .card.my-subscription{
        border: 2px solid green;
    }

    .btn-buy-subscription{
        font-size: .9em;
        font-weight: 600;
    }

    .btn-buy-subscription .total{
        font-size: .8em;
        font-weight: 100;
    }
</style>
@endsection

@section('content')
@php
$can_purchase = true;
if(!empty(Auth::user()->shop)){
	if(empty(Auth::user()->shop->tax_business_name) ||
		empty(Auth::user()->shop->tax_type_id) ||
		empty(Auth::user()->shop->tax_vat) ||
		empty(Auth::user()->shop->tax_fiscal_code) ||
		(empty(Auth::user()->shop->tax_code) && empty(Auth::user()->shop->tax_pec)) ||
		empty(Auth::user()->shop->tax_address_country) ||
		empty(Auth::user()->shop->tax_address_route) ||
		empty(Auth::user()->shop->tax_address_city) ||
		empty(Auth::user()->shop->tax_address_prov) ||
		empty(Auth::user()->shop->tax_address_region) ||
		empty(Auth::user()->shop->tax_address_postal_code) ||
		empty(Auth::user()->shop->tax_address_street_number) || 
		empty(Auth::user()->birthdate) || 
		empty(Auth::user()->shop->whatsapp) || 
		empty(Auth::user()->name)  
	  )
		{
			$can_purchase = false;
		}
}
$user_subscription = Auth::user()->subscription->subscription;
if($user_subscription == 'free' && !empty(Auth::user()->shop->agent_id)){
	$user_subscription = 'standard';
}
@endphp
<div class="modal fade" id="modal-danger" style="display: none;" aria-hidden="true">
	<div class="modal-dialog">
	  <div class="modal-content bg-danger">
		<div class="modal-header">
		  <h4 class="modal-title">Attenzione!</h4>
		  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">×</span>
		  </button>
		</div>
		<div class="modal-body">
		  <p>Per poter procedere con il pagamento accertati di aver compilato le seguenti informazioni del tuo account:</p>
			<ul>
              <li>Dati fiscali della tua azienda</li>
			  <li>Nome del titolare</li>
              <li>Data di nascita del titolare</li>
              <li>Cellulare del titolare</li>
			</ul>
		</div>
		<div class="modal-footer justify-content-between">
		  <button type="button" class="btn btn-outline-light" data-dismiss="modal">Chiudi</button>
		  <p><a href="/profile/edit"><strong><u>Vai alla sezione account</u></strong></a></p>
		</div>
	  </div>
	  <!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
  </div>
<div class="content">
    <div class="clearfix"></div>

    @include('flash::message')

    <div class="clearfix"></div>

    <div class="card card-primary">
        <div class="card-body">
            <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
                <h1 class="display-4">I nostri prezzi</h1>
                <p class="lead">Scegli l’abbonamento annuale e risparmi oltre il 25%!</p>
            </div>

            <div class="container">
                <form action="/paypal/resume" method="GET">

                    @csrf

                    <div class="card-deck mb-3 text-center">

                        @foreach($subscriptions as $subscription)

                        @php
                        $my_subscription = '';
                        if( $user_subscription == $subscription->subscription ):
                            $my_subscription = 'my-subscription';
                        endif;
                        @endphp

                        <div class="card mb-4 box-shadow {{ $my_subscription }}">
                            <div class="card-header">
                                @php
                                if($subscription->subscription === 'free'){
                                    $icon = 'fas fa-car-side';
                                    $bgcolor = '';
                                    $btncolor = 'primary';
                                }
                                if($subscription->subscription === 'standard'){
                                    $icon = 'far fa-paper-plane';
                                    $bgcolor = 'text-primary';
                                    $btncolor = 'primary';
                                }elseif($subscription->subscription === 'premium'){
                                    $icon = 'fas fa-plane';
                                    $bgcolor = 'text-warning';
                                    $btncolor = 'warning';
                                }elseif($subscription->subscription === 'enterprise'){
                                    $icon = 'fas fa-rocket';
                                    $bgcolor = 'text-success';
                                    $btncolor = 'success';
                                }
                                @endphp
                                <h2 class="my-0" style="font-weight: 888">{{ $subscription->name }}</h2>
                                <h2><i class="{{ $icon }} {{ $bgcolor }}"></i></h2>
                                @if( $my_subscription !== '' )
                                <span class="badge badge-success"><i class="fa fa-check-circle"></i> Il mio abbonamento</span>
                                @else
                                &nbsp;
                                @endif
                            </div>
                            <div class="card-body">
                                <table class="table mt-3 mb-0">
                                    <tr class="p-0">
                                        <td class="p-1 border-top-0">
                                            <span class="{{ $bgcolor }}">{{ $subscription->shops_num }}</span>@if( $subscription->shops_num == 1 ) SEDE @else SEDI @endif ATTIVITA’
                                            @if( !empty($subscription->shops_note) )
                                            <i class="fas fa-info-circle" data-toggle="tooltip" data-placement="top" title="{{ $subscription->shops_note }}"></i>
                                            @endif
                                        </td>
                                    </tr>
                                    <tr class="p-0">
                                        <td class="p-1">
                                            <small>fino a </small> <span class="{{ $bgcolor }}">{{ $subscription->products_num }}</span> prodotti
                                            @if( !empty($subscription->products_note) )
                                            <i class="fas fa-info-circle" data-toggle="tooltip" data-placement="top" title="{{ $subscription->products_note }}"></i>
                                            @endif
                                        </td>
                                    </tr>
                                </table>
                                <table class="table table-striped mt-0 mb-4">
                                    @foreach($subscription->subscriptionFeatures as $feature)
                                    <tr class="p-0">
                                        <td class="p-1">
                                            {!! $feature->feature_name !!}
                                            @if( !empty($feature->feature_note) )
                                                <i class="fas fa-info-circle" data-toggle="tooltip" data-placement="top" title="{{ $feature->feature_note }}"></i>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </table>
                                @if($subscription->subscription !== 'free')
                                <button type="submit" onClick="{{ empty($can_purchase) ? 'blockLink();return false;' : '' }}"  name="btn_payment" value="{{ strtoupper($subscription->subscription) }}_SEMESTRALE" class="btn btn-lg btn-block btn-buy-subscription btn-outline-{{ $btncolor }}">Acquisto Abbonamento Semestrale <br /> {{ number_format($subscription->price_half_yearly, 2, ',', '.') }} &euro;/mese + IVA<br /><span class="total">( totale {{ number_format($subscription->price_half_yearly * 6, 2, ',', '.') }} &euro; + IVA )</span></button>
                                <button type="submit" onClick="{{ empty($can_purchase) ? 'blockLink();return false;' : '' }}" name="btn_payment" value="{{ strtoupper($subscription->subscription) }}_ANNUALE" class="btn btn-lg btn-block btn-buy-subscription btn-{{ $btncolor }}">Acquisto Abbonamento Annuale <br /> {{ number_format($subscription->price_yearly, 2, ',', '.') }} &euro;/mese + IVA<br /><span class="total">( totale {{ number_format($subscription->price_yearly * 12, 2, ',', '.') }} &euro; + IVA )</span></button>
                                @endif
                            </div>
                        </div>
                        @endforeach

                    </div>
                </form>
            </div>
        </div>
        <div class="text-center">
            <p>
                @php {{  }}{{  }}
                $body = "Richiedo la modifica del mio abbonamento per l'attivita " . Auth::user()->shop->name . ".%0A%0AUtente: " . Auth::user()->name . " - email: " . Auth::user()->email . "%0A%0A(Descrivi la modifica che vuoi apportare al tuo abbonamento)";
                @endphp
                <a href="mailto:abbonamenti@primascelta.biz?subject=Richiesta modifica abbonamento&body={{$body}}" target="_blank">Desidero modificare il mio abbonamento</a>
            </p>
        </div>
    </div>
	<script>
		function blockLink(){
			$("#modal-danger").modal('show');
		}
	</script>
    @endsection
