@extends('adminlte::page')

@section('css')
<style>
.table thead th{
    border-top: 0 !important;
}
</style>
@endsection

@section('content')
<div class="content">
    <div class="clearfix"></div>

    @include('flash::message')

    <div class="clearfix"></div>

    <div class="card card-primary">
        <div class="card-body">
            <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
                <h1>Stai acquistando un abbonamento<br /><strong>{{ $subscription_name }}</strong></h1>

            </div>

            <div class="container">

                <strong>La tua attività</strong>
                <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <td style="width: 200px">Nome</td>
                            <td>{!! Auth::user()->shop->name !!}</td>
                        </tr>
                        <tr>
                            <td>Partita IVA</td>
                            <td>{!! Auth::user()->shop->tax_vat !!}</td>
                        </tr>
                    </table>
                </div>

                <div class="row">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <strong>Il tuo attuale abbonamento</strong>
                        <div class="table-responsive">
                            <table class="table">
                                <tr>
                                    <td style="width: 200px">Tipologia</td>
                                    <td>{!! ucfirst(Auth::user()->subscription->subscription) !!}</td>
                                </tr>
                                <tr>
                                    <td>Scadenza</td>
                                    <td>{!! Auth::user()->subscription->expire_date->format('d/m/Y') !!}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <strong>L'abbonamento che stai acquistando</strong>
                        <div class="table-responsive">
                            <table class="table">
                                <tr>
                                    <td style="width: 200px">Tipologia</td>
                                    <td>{!! ucfirst(strtolower(implode(" ", explode("_", $subscription_type)))) !!}</td>
                                </tr>
                                <tr>
                                    <td>Nuova Scadenza</td>
                                    <td>
                                        @php
                                        $subscription_length = explode("_", $subscription_type)[1];
                                        @endphp
                                        @if( $subscription_length === 'SEMESTRALE' )
                                            @if( Auth::user()->subscription->expire_date->isFuture() )
                                                {!! Auth::user()->subscription->expire_date->addMonths(6)->format('d/m/Y') !!}
                                            @else
                                                {!! Carbon\Carbon::now()->addMonths(6)->format('d/m/Y') !!}
                                            @endif
                                        @elseif( $subscription_length === 'ANNUALE' )
                                            @if( Auth::user()->subscription->expire_date->isFuture() )
                                                {!! Auth::user()->subscription->expire_date->addYear()->format('d/m/Y') !!}
                                            @else
                                                {!! Carbon\Carbon::now()->addYear()->format('d/m/Y') !!}
                                            @endif
                                        @endif
                                    </td>
                                </tr>
                                @php
                                if( $subscription_length === 'SEMESTRALE' ):
                                    $price = $subscription->price_half_yearly * 6;
                                elseif( $subscription_length === 'ANNUALE' ):
                                    $price = $subscription->price_yearly * 12;
                                endif;
                                $iva = $price * 0.22;
                                $totale = $price + $iva;
                                @endphp
                                <tr>
                                    <td>Prezzo</td>
                                    <td>
                                        {{ number_format( $price, 2, ',', '.') }} &euro;
                                    </td>
                                </tr>
                                <tr>
                                    <td>IVA</td>
                                    <td>
                                        {{ number_format( $iva, 2, ',', '.') }} &euro;
                                    </td>
                                </tr>
                                <tr>
                                    <td>TOTALE</td>
                                    <td>
                                        <strong>{{ number_format( $totale, 2, ',', '.') }} &euro;</strong>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

                <form id="frm_payment" action="/paypal/payment" method="POST">
                    @csrf

                    @if( empty(Auth::user()->subscription->parent) && count($other_shops) > 0 )
                        <section class="bg-secondary p-4 rounded">
                        @if( ($tot_activities-1) === 1 )
                        <strong>Puoi acquistare questo abbonamento per un'altra attività</strong><br />
                        <small>Seleziona da questo elenco l'altra attività</small>
                        @else
                        <strong>Puoi acquistare questo abbonamento per altre {{ ($tot_activities-1) }} attività</strong><br />
                        <small>Seleziona da questo elenco le altre attività</small>
                        @endif

                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th style="width: 100px">Seleziona</th>
                                    <th>Nome attività</th>
                                    <th>Indirizzo</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($other_shops as $other_shop)
                                    <tr>
                                        <td><input type="checkbox" name="other_shops[]" value="{{ $other_shop->id }}"></td>
                                        <td>{{ $other_shop->name }}</td>
                                        <td>{{ $other_shop->address }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        </section>
                    @else
                        <section class="bg-secondary">
                            <h3>Non puoi attivare questo abbonamento per altre attività.</h3>
                        </section>
                    @endif

                <hr>

                <p>
                    Caro <strong>{{ Auth::user()->name }}</strong> <br />
                    di seguito trovi le modalità di pagamento che puoi utilizzare per effettuare il saldo di <strong>{{ number_format( $totale, 2, ',', '.') }} &euro;</strong><br/>
                    per il tuo abbonamento <strong>{{ $subscription_name }}</strong>
                </p>

                <hr>

                    <input type="hidden" name="subscription_type" value="{{ $subscription_type }}">

                    <h2>PayPal / Carta di credito</h2>
                    <p>
                        Puoi versare la tua quota tramite PayPal usando il tuo conto oppure semplicemente usando la tua carta di credito.<br/>
                        Una volta completato il versamento della quota ti arriverà una mail di conferma all'indirizzo.
                    </p>
                    <input type="image" id="submit_payment" type="submit" src="https://www.paypalobjects.com/webstatic/mktg/logo-center/logo_paypal_sicuro_sito.png" alt="Paga con PayPal" /><br />

                </form>

                <hr>

                <h2>Bonifico bancario</h2>
                <p>
                    Per versare la tua quota tramite bonifico bancario utilizza le seguenti coordinate:<br/><br/>
                    <strong>Gruppo Ubi Banca</strong><br/>
                    Intestato a <strong>Terracciano srl</strong><br/>
                    IBAN: <strong>IT31I0306939240100000009646</strong><br/>
                    SWIFT/BIC: <strong>BLOPIT22</strong><br/>
                    Causale: <strong>Primascelta - Abbonamento {{ $subscription_name }} di {{ Auth::user()->name }}</strong><br/>
                    Importo: <strong>{{ number_format( $totale, 2, ',', '.') }} &euro;</strong>
                    <br />
                    <br/><span class="text-danger"><i class="fas fa-exclamation-circle"></i> Nota bene: non dimenticare di specificare il tuo nome nella causale.</span>
                    <br />
                    (Per velocizzare l'attivazione del tuo abbonamento, inviaci una email a abbonamenti@primascelta.biz)<br/>
                </p>

                <hr>

                <a href="{{ route('home') }}" class="btn btn-block btn-link">Annulla</a>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
    $(document).ready(function(){
        $("#frm_payment").on('submit', function(){
            $('#submit_payment').prop('disabled', true);
        });
    });
</script>
@endsection
