@extends('adminlte::page')

@section('content')
    <section class="content-header">
        <h1>
            Unità di misura
        </h1>
   </section>
   <div class="content">
       {{-- @include('adminlte-templates::common.errors') --}}
       <div class="card card-primary">
           <div class="card-body">
               <div class="card-body">
                   {!! Form::model($unitOfMeasure, ['route' => ['unitOfMeasures.update', $unitOfMeasure->id], 'method' => 'patch']) !!}

                        @include('unit_of_measures.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection
