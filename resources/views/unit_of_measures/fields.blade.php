<!-- Name Field -->
<div class="form-group ">
    {!! Form::label('name', __('common.name')) !!}
    {!! Form::text('name', old('name', $unitOfMeasure->name ?? null), ['class' => 'form-control']) !!}
    @error('name')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group ">
    {!! Form::label('next_unit_of_measure', 'Successiva') !!}
    {!! Form::select('next_unit_of_measure', ['' => 'NESSUNA'] + $unitOfMeasures->toArray(), old('next_unit_of_measure', $unitOfMeasure->next_unit_of_measure ?? null), ['class' => 'form-control']) !!}
    @error('next_unit_of_measure')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group ">
    {!! Form::label('next_unit_of_measure_quantity', 'Q.tà successiva') !!}
    {!! Form::text('next_unit_of_measure_quantity', old('next_unit_of_measure_quantity', $unitOfMeasure->next_unit_of_measure_quantity ?? null), ['class' => 'form-control']) !!}
    @error('next_unit_of_measure_quantity')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('common.save'), ['class' => 'btn btn-primary btn-full']) !!}
    <a href="{{ route('unitOfMeasures.index') }}" class="btn btn-default btn-full">{{ __('common.cancel') }}</a>
</div>
