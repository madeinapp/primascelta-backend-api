@extends('adminlte::page')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">{{ __('common.unit_of_measure') }}</h1>
        <div class="pull-right  float-right d-none d-sm-block" >
           <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{{ route('unitOfMeasures.create') }}">{{ __('common.Add New') }}</a>
        </div>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="card card-primary">
            <div class="card-body">
                @include('unit_of_measures.table')
            </div>
        </div>
    </div>

    @include('unit_of_measures.modal_replace_uom')

@endsection

