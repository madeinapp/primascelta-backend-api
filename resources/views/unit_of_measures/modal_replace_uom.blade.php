<div class="modal fade" id="modal-replace-uom" aria-hidden="true" data-backdrop="static" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-warning">
                <h4 class="modal-title">Sostituisci Unità di Misura</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p id="uom_message"></p>
                <div class="form-group">
                    <label for="replace_uom">Sostituisci con questa unità di misura</label>
                    <select id="replace_uom" class="form-control"></select>

                    <div class="mt-2">
                        <input type="checkbox" id="del_after_replace"> Elimina l'unità di misura dopo la sostituzione
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <input type="hidden" id="old_uom">
                <button type="button" class="btn btn-warning" onclick="replaceUom()">Sostituisci</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Chiudi</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
