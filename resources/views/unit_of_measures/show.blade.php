@extends('adminlte::page')

@section('content')
    <section class="content-header">
        <h1>
            {{ __('common.unit_of_measure') }}
        </h1>
    </section>
    <div class="content">
        <div class="card card-primary">
            <div class="card-body">
                <div class="row" style="padding-left: 20px">
                    @include('unit_of_measures.show_fields')
                    <a href="{{ route('unitOfMeasures.index') }}" class="btn btn-default">{{ __('common.Back') }}</a>
                </div>
            </div>
        </div>
    </div>
@endsection
