<div class="table-responsive">
    <table class="table" id="unitOfMeasures-table">
        <thead>
            <tr>
                <th>{{ __('common.action') }}</th>
                <th>{{__('common.name') }}</th>
                <th>U.tà Sup.</th>
                <th>Q.tà per unità superiore</th>
                <th>Generi Alimentari</th>
                <th>Tot. Alimenti</th>
                <th>Tot. Prodotti a catalogo</th>
            </tr>
        </thead>
        <tbody>
        @foreach($unitOfMeasures as $unitOfMeasure)
            <tr>
                <td>
                    <a href="{{ route('unitOfMeasures.edit', [$unitOfMeasure->id]) }}" class='btn btn-default btn-sm'><i class="fas fa-edit"></i></a>
                    {!! Form::button('<i class="fas fa-trash"></i>', ['type' => 'button', 'class' => 'btn btn-danger btn-sm', 'onclick' => "deleteUom($unitOfMeasure->id);"]) !!}
                </td>
                <td>{{ $unitOfMeasure->name }}</td>
                <td>{{ $unitOfMeasure->nextUnitOfMeasure->name ?? '' }}</td>
                <td>{{ $unitOfMeasure->next_unit_of_measure_quantity }}</td>
                <td>{{ $unitOfMeasure->foodCategories->count() }}</td>
                <td>{{ $unitOfMeasure->foods->count() }}</td>
                <td>{{ $unitOfMeasure->shopCatalogs->count() }} + {{ $unitOfMeasure->shopCatalogsBuy->count() }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
@section('js')
<script>
$(function () {
    $('#unitOfMeasures-table').DataTable({
        "language": {
            url: "{{ url('js/Italian.json') }}"
        },
        "paging":   true,
        "initComplete": function (settings, json) {
            $("#unitOfMeasures-table").wrap("<div style='overflow:auto; width:100%;position:relative;'></div>");
        },
        pageLength: 10,
        stateSave: true,
        columnDefs: [
            { width: "10%", orderable: false, targets: 0 },
            { width: "25%", orderable: true, targets: 1 }
        ]
    });
});

function execDeleteUom(unit_of_measure_id){
    $.ajax({
        url: `/unitOfMeasures/${unit_of_measure_id}`,
        data: {
            "_token": "{{ csrf_token() }}"
        },
        type: "DELETE",
        dataType: "json",
        statusCode: {
            401: function() {
                document.location.href = '/login';
            },
            419: function() {
                document.location.href = '/login';
            }
        },
        success: function(data){
            if( data.status == 'ok' ){
                document.location.reload();
            }else if( data.status == 'associated' ){

                var food_categories = data.foodCategories;
                var foods_found = data.foods;
                var catalog_found = data.shopCatalogs;
                var catalog_buy_found = data.shopCatalogsBuy;

                $.ajax({
                    url: `/unitOfMeasures/getList`,
                    data: {
                        "_token": "{{ csrf_token() }}"
                    },
                    type: "GET",
                    dataType: "json",
                    statusCode: {
                        401: function() {
                            document.location.href = '/login';
                        },
                        419: function() {
                            document.location.href = '/login';
                        }
                    },
                    success: function(data){

                        var options = '';
                        $.each(data, function(k, val){
                            if( val.id != unit_of_measure_id ){
                                options += `<option value="${val.id}">${val.name}</option>`;
                            }
                        });

                        $("#modal-replace-uom #old_uom").val(unit_of_measure_id);
                        $("#modal-replace-uom #replace_uom").html(options);
                        $("#modal-replace-uom #uom_message").text(`L'unità di misura selezionata risulta associata a ${food_categories} generi alimentari, ${foods_found} alimenti, ${catalog_found} prodotti a catalogo e in ${catalog_buy_found} prodotti a catalogo come unità di misura minima e multipla d'acquisto`)
                        $("#modal-replace-uom").modal('show');
                    }
                })
            }
        },
        error: function(){
            alert("Si è verificato un errore durante la cancellazione");
        }
    })
}

function deleteUom(unit_of_measure_id){
    showConfirm("Unità di misura", 'bg-danger', "Eliminare definitivamente l'unità di misura selezionata?", execDeleteUom, [unit_of_measure_id]);
}

function replaceUom(){
    var old_uom = $("#modal-replace-uom #old_uom").val();
    var replace_uom = $("#modal-replace-uom #replace_uom").val();
    var flag_delete = $("#modal-replace-uom #del_after_replace").prop("checked");

    console.log(old_uom, replace_uom, flag_delete);

    $.ajax({
        url: `/unitOfMeasures/replace`,
        data: {
            "_token": "{{ csrf_token() }}",
            old_uom: old_uom,
            replace_uom: replace_uom,
            flag_delete: flag_delete
        },
        type: "POST",
        dataType: "json",
        statusCode: {
            401: function() {
                document.location.href = '/login';
            },
            419: function() {
                document.location.href = '/login';
            }
        },
        success: function(data){
            if( data.status === 'ok' ){
                document.location.reload();
            }
        }
    });
}
</script>
@endsection
