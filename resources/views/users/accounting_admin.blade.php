<div class="form-group col-12 col-sm-12 col-md-6 col-lg-6">
	{!! Form::label('admin_test_shop', 'Test') !!}
    {!! Form::select('admin_test_shop', [1 => 'SI', 0 => 'NO'], !empty($user) ? $user->shop->admin_test_shop : 0, ['class' => 'form-control']) !!}
</div>

