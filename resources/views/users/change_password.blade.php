<p>
    La password deve essere di almeno 8 caratteri e includere maiuscole, minuscole, numeri e simboli.
</p>

<div class="form-group col-12 col-sm-12 col-md-6 col-lg-6 @if($errors->has('new_password')) has-error @endif">
    {!! Form::label('new_password', 'Nuova password') !!}
    <input id="password" style="display:none" type="password" name="password">
    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1"><i class="fas fa-key"></i></span>
        </div>
        {!! Form::password('new_password', ['class' => 'form-control', 'id' => 'new_password', 'autocomplete' => 'new-password']) !!}
    </div>
    @error('new_password')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
</div>

<div class="form-group col-12 col-sm-12 col-md-6 col-lg-6">
    {!! Form::label('new_password_confirmation', 'Conferma nuova password') !!}
    {!! Form::password('new_password_confirmation', ['class' => 'form-control', 'id' => 'new_password_confirmation']) !!}
</div>

<div class="form-group col-12 col-sm-12 col-md-6 col-lg-6">
	<input type="checkbox" id="show-password"> Mostra password
</div>

@if(Auth::user()->role == 'admin'  && !empty($user->shop->admin_first_password_generated))
	<div class="form-group col-12 col-sm-12 col-md-6 col-lg-6">
	 <label for="old_password"> Password attuale (admin)</label>
	 <div name="old_password">{{ $user->shop->admin_first_password_generated }}</div>
	</div>
@endif
