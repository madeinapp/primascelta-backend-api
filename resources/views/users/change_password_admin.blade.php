<p>
    La password deve essere di almeno 8 caratteri e includere maiuscole, minuscole, numeri e simboli.
</p>

<div class="form-group col-12 col-sm-12 col-md-6 col-lg-6 @if($errors->has('new_password')) has-error @endif">
    {!! Form::label('new_password', 'Password') !!}
    <input id="password" style="display:none" type="password" name="password">
    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1"><i class="fas fa-key"></i></span>
        </div>
        {!! Form::password('new_password', ['class' => 'form-control', 'id' => 'new_password', 'autocomplete' => 'new-password', 'required' => true]) !!}
    </div>
    <div class="alert alert-danger">La password è obbligatoria</div>
</div>

<input type="checkbox" id="show-password"> Mostra password
