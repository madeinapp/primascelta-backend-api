@php
$userImage = null;
if( !empty($user->image_url) ){
    $userImage = $user->image_url . '?p=' . rand(10000,99999);
    $textButton = 'Seleziona una nuova immagine';
}else{
    $textButton = 'Seleziona immagine';
}
@endphp

<div class="row">
    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
        <div class="card card-primary">
            <div class="card-body">
                @if( !$userImage )
                    <p id="img-suggestion" class="text-danger"><strong>Inserisci la foto della tua attività, sarà quella che vedranno i tuoi clienti. File ammessi > .jpg - Max 10 mb - dimensioni minime di 640x320</strong></p>
                @else
                    <p id="img-suggestion" class="text-primary">Puoi cambiare l'immagine del profilo in qualsiasi momento. File ammessi > .jpg - Max 10 mb - dimensioni minime di 640x320</p>
                @endif

                <input type="file" name="fileimage" id="fileimage" accept="image/*" onchange="preview_image(event)" class="js inputfile inputfile-1" data-multiple-caption="{count} immagine selezionata" />
                <label for="fileimage" class="lbl-fileimage"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span id="btn-fileimage">{{ $textButton }}</span></label>

                <div style="clear: both"></div>
                @if( $userImage )
                    <img id="output_image" src="{!! $userImage !!}" style="width: 100%;">
                @else
                    <img id="output_image" style="width: 100%;">
                @endif
                <input type="hidden" id="output_image_type" name="output_image_type" value="">
                <input type="hidden" id="output_image_name" name="output_image_name" value="">
                <input type="hidden" id="output_image_data" name="output_image_data" value="">
            </div>
        </div>

        <div class="form-group @if($errors->has('shop_type_id')) has-error @endif">
            {!! Form::label('shop_type_id', 'Scegli la tipologia della tua attvità') !!}
            {!! Form::select('shop_type_id[]', $user->shop_types, $user->shop->shopTypes()->pluck('id')->toArray(), ['class' => 'form-control', 'multiple' => true, 'required' => true, 'placeholder' => 'Seleziona la tipologia aziendale']) !!}
            @error('shop_type_id')
                <div class="alert alert-danger mt-1">{{ $message }}</div>
            @else
                @if($user->shop->shopTypes->count() == 0)
                    <div class="alert alert-danger mt-1">Selezionare almeno una tipologia attività</div>
                @endif
            @enderror
            <small class="press-ctrl">Premi CTRL per selezionare più tipologie</small>
        </div>

        <div class="form-group @if($errors->has('shop_name')) has-error @endif">
            {!! Form::label('shop_name', 'Nome attività') !!}
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1"><i class="fas fa-store-alt"></i></span>
                </div>
                {!! Form::text('shop_name', old('shop_name', $user->shop->name), ['class' => 'form-control', 'placeholder' => 'Inserisci il nome dell\'attività', 'required' => true]) !!}
            </div>
            @error('shop_name')
                <div class="alert alert-danger">{{ $message }}</div>
            @else
                @if( empty($user->shop->name) )
                    <div class="alert alert-danger">Inserire il nome attività</div>
                @endif
            @enderror
        </div>

        <div class="form-group @if($errors->has('description')) has-error @endif">
            {!! Form::label('description', 'Descrizione') !!} (Max 500 caratteri)
            {!! Form::textarea('description', $user->shop->description, ['class' => 'form-control', 'placeholder' => 'Presenta in breve la tua azienda', 'rows' => 6, 'maxlength' => 500, 'required' => $field_required]) !!}
            @error('description')
                <div class="alert alert-danger">{{ $message }}</div>
            @else
                @if( empty($user->shop->description) && $field_required)
                    <div class="alert alert-danger p-1 mt-0">La descrizione è obbligatoria</div>
                @endif
            @enderror
        </div>

        @if( isset($agents) )
        <div class="form-group @if($errors->has('description')) has-error @endif">
            {!! Form::label('agent_id', 'Agente') !!}
            {!! Form::select('agent_id', $agents, $user->shop->agent_id, ['class' => 'form-control', 'placeholder' => 'Seleziona un agente...', 'required' => false]) !!}
            @error('description')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        @endif
    </div>

    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
        <input type="hidden" name="facebook_page" id="facebook_page" value="">
        <input type="hidden" name="instagram_page" id="instagram_page" value="">
        <div class="form-group @if($errors->has('address') || $errors->has('route')) has-error @endif">
            {!! Form::label('citta', 'Indirizzo, completo di numero civico') !!}
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1"><i class="fas fa-map-marked-alt"></i></span>
                </div>
                {!! Form::text('address', old('address', $user->shop->address), ['id' => 'citta', 'class' => 'form-control', 'placeholder' => 'Inserisci l\'indirizzo dell\'attività completo di numero civico', 'required' => true]) !!}
                <div id="shop_address_components">
                    {!! Form::hidden('country', old('country',$user->shop->country), ['id' => 'country', 'class' => 'form-control country']) !!}
                    {!! Form::hidden('region', old('region',$user->shop->region), ['id' => 'administrative_area_level_1','class' => 'form-control administrative_area_level_1']) !!}
                    {!! Form::hidden('city', old('city',$user->shop->city), ['id' => 'locality', 'class' => 'form-control locality']) !!}
                    {!! Form::hidden('prov', old('prov',$user->shop->prov), ['id' => 'administrative_area_level_2', 'class' => 'form-control administrative_area_level_2']) !!}
                    {!! Form::hidden('route', old('route',$user->shop->route), ['id' => 'route', 'class' => 'form-control route']) !!}
                    {!! Form::hidden('street_number', old('street_number',$user->shop->street_number), ['id' => 'street_number', 'class' => 'form-control street_number']) !!}
                    {!! Form::hidden('postal_code', old('postal_code',$user->shop->postal_code), ['id' => 'postal_code', 'class' => 'form-control postal_code']) !!}
                    {!! Form::hidden('lat', old('lat',$user->shop->lat), ['id' => 'lat', 'class' => 'form-control lat']) !!}
                    {!! Form::hidden('long', old('long',$user->shop->long), ['id' => 'long', 'class' => 'form-control long']) !!}
                </div>
            </div>
            @error('address')
                <div class="alert alert-danger">{{ $message }}</div>
            @else
                @if(empty($user->shop->address))
                    <div class="alert alert-danger">L'indirizzo è obbligatorio</div>
                @endif
            @enderror
            @error('route')
                <div class="alert alert-danger">L'indirizzo risulta incompleto</div>
            @enderror
        </div>

        <div class="form-group @if($errors->has('phone')) has-error @endif">
            {!! Form::label('phone', 'Telefono attività') !!}
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1"><i class="fas fa-phone"></i></span>
                </div>
                {!! Form::text('phone', $user->phone, ['class' => 'form-control', 'placeholder' => 'Telefono attività', 'required' => true]) !!}
            </div>
            @error('phone')
                <div class="alert alert-danger">{{ $message }}</div>
            @else
                @if(empty($user->phone))
                    <div class="alert alert-danger">Il telefono attività è obbligatorio</div>
                @endif
            @enderror
        </div>

        <div class="form-group @if($errors->has('whatsapp')) has-error @endif">
            {!! Form::label('whatsapp', 'Telefono cellulare') !!}

            <input type="checkbox" id="show_phone_on_app" name="show_phone_on_app" class="ml-3" @if( old('show_phone_on_app', $user->shop->show_phone_on_app) ) checked @endif>
            <label for="show_phone_on_app"><small>Mostra sull'app</small></label>

            <div class="input-group mb-3">
                <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1"><i class="fas fa-mobile-alt"></i></span>
                </div>
                {!! Form::text('whatsapp', $user->shop->whatsapp, ['class' => 'form-control', 'placeholder'=> 'Cellulare di un responsabile/attività', 'required' => $field_required]) !!}
            </div>
            @error('whatsapp')
                <div class="alert alert-danger">{{ $message }}</div>
            @else
                @if(empty($user->shop->whatsapp) && $field_required)
                    <div class="alert alert-danger">Il telefono cellulare è obbligatorio</div>
                @endif
            @enderror
        </div>

        <div class="form-group @if($errors->has('email')) has-error @endif">
            {!! Form::label('email', 'Email') !!}
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1"><i class="far fa-envelope"></i></span>
                </div>
                {!! Form::email('email', $user->email, ['class' => 'form-control', 'placeholder' => 'Scrivi la email di un responsabile', 'required' => true]) !!}
            </div>
            @error('email')
                <div class="alert alert-danger">{{ $message }}</div>
                @if(empty($user->email))
                    <div class="alert alert-danger">L'email è obbligatoria</div>
                @endif
            @enderror
        </div>

        <div class="form-group @if($errors->has('user_name')) has-error @endif">
            {!! Form::label('user_name', 'Nome e Cognome di un titolare legale') !!}
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1"><i class="fa fa-user"></i></span>
                </div>
                {!! Form::text('user_name', old('user_name', $user->name), ['class' => 'form-control', 'placeholder' => 'Nome del responsabile', 'required' => $field_required]) !!}
            </div>
            @error('user_name')
                <div class="alert alert-danger">{{ $message }}</div>
                @if(empty($user->name))
                    <div class="alert alert-danger">Inserire il titolare legale</div>
                @endif
            @enderror
        </div>

        <div class="form-group @if($errors->has('user_birthdate') || $errors->has('age')) has-error @endif">
            {!! Form::label('user_birthdate', 'Data di nascita del titolare') !!}
            <div class="input-group mb-3">
                <div class="input-group-prepend" data-target="#user_birthdate" data-toggle="datetimepicker">
                    <span class="input-group-text" id="basic-addon1"><i class="far fa-calendar-alt"></i></span>
                </div>
                {{-- Form::date('user_birthdate', $user->birthdate, ['class' => 'form-control', 'placeholder' => 'Data di nascita del responsabile']) --}}

                <input type="hidden" id="user_birthdate_value" value="{{ (isset($user->birthdate) && !empty($user->birthdate)) ? Carbon\Carbon::parse($user->birthdate)->format('d/m/Y') : null }}">
                <input type="text" id="user_birthdate"
                                    name="user_birthdate"
                                    class="form-control datetimepicker-input"
                                    data-target="#user_birthdate"
                                    placeholder="gg/mm/aaaa"
@if($field_required)
                                    required="true"
@endif
                                    readonly />
            </div>
            @error('user_birthdate')
                <div class="alert alert-danger">{{ $message }}</div>
            @else
                @if(empty($user->birthdate) && $field_required)
                    <div class="alert alert-danger">Inserire la data di nascita del titolare</div>
                @endif
            @enderror
            @error('age')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>

        <div class="form-group @if($errors->has('website')) has-error @endif">
            {!! Form::label('website', 'Sito web') !!}
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1"><i class="fas fa-globe"></i></span>
                </div>
                {!! Form::text('website', $user->shop->website, ['class' => 'form-control', 'placeholder' => 'https://www.ilmionegozio.it']) !!}
            </div>
            @error('website')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>

        <div class="form-group @if($errors->has('payment_method_id')) has-error @endif">
            {!! Form::label('payment_method_id', 'Metodi di pagamento accettati in sede') !!}
            <div class="input-group mb-0">
                <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1"><i class="far fa-credit-card"></i></span>
                </div>
                {!! Form::select('payment_method_id[]', $user->payment_method, $user->shop->PaymentMethodsShop()->get()->pluck('id')->toArray(), ['class' => 'form-control', 'multiple' => true, 'required' => true]) !!}
            </div>
            @error('payment_method_id')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            @if( $user->shop->paymentMethodsShop->count() === 0 )
                <div class="alert alert-danger p-1 mt-0">Selezionare almeno un metodo di pagamento</div>
            @endif
            <small class="press-ctrl">Premi CTRL per selezionare più metodi di pagamento</small>
        </div>
    </div>
</div>
