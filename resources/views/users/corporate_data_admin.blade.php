<div class="row">
    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
        <div class="card card-primary">
            <div class="card-body">
				<p id="img-suggestion" class="text-primary">
				  <strong>Inserisci la foto della tua attività, sarà quella che vedranno i tuoi clienti. <br>File ammessi > .jpg - Max 10 mb - dimensioni minime di 640x320</strong>
                </p>

                <input type="file" name="fileimage" id="fileimage" accept="image/*" onchange="preview_image(event)" class="js inputfile inputfile-1" data-multiple-caption="{count} immagine selezionata" required />
                <label for="fileimage" class="lbl-fileimage"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span id="btn-fileimage">Seleziona immagine</span></label>
				<div class="alert alert-danger">L'immagine è obbligatoria</div>
                <div style="clear: both"></div>
                <img id="output_image" style="width: 100%;">
                <input type="hidden" id="output_image_type" name="output_image_type" value="">
                <input type="hidden" id="output_image_name" name="output_image_name" value="">
                <input type="hidden" id="output_image_data" name="output_image_data" value="">
            </div>
        </div>

        <div class="form-group @if($errors->has('shop_type_id')) has-error @endif">
            {!! Form::label('shop_type_id', 'Scegli la tipologia della tua attvità') !!}
            {!! Form::select('shop_type_id[]', $arr_shop_types, null, ['class' => 'form-control', 'multiple' => true, 'required' => true, 'placeholder' => 'Seleziona la tipologia aziendale']) !!}
            @error('shop_type_id')
                <div class="alert alert-danger mt-1">{{ $message }}</div>
            @enderror
            <small class="press-ctrl">Premi CTRL per selezionare più tipologie</small>
        </div>

        <div class="form-group @if($errors->has('shop_name')) has-error @endif">
            {!! Form::label('shop_name', 'Nome attività') !!}
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1"><i class="fas fa-store-alt"></i></span>
                </div>
                {!! Form::text('shop_name', null, ['class' => 'form-control', 'placeholder' => 'Inserisci il nome dell\'attività', 'required' => true]) !!}
            </div>
            @error('shop_name')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>

        <div class="form-group @if($errors->has('description')) has-error @endif">
            {!! Form::label('description', 'Descrizione') !!} (Max 500 caratteri)
            {!! Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Presenta in breve la tua azienda', 'rows' => 6, 'maxlength' => 500]) !!}
            @error('description')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>

        @if( isset($arr_agents) )
        <div class="form-group @if($errors->has('description')) has-error @endif">
            {!! Form::label('agent_id', 'Agente') !!}
            {!! Form::select('agent_id', $arr_agents, null, ['class' => 'form-control', 'placeholder' => 'Seleziona un agente...']) !!}
            @error('description')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        @endif
    </div>

    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
        <input type="hidden" name="facebook_page" id="facebook_page" value="">
        <input type="hidden" name="instagram_page" id="instagram_page" value="">
        <div class="form-group @if($errors->has('address') || $errors->has('route')) has-error @endif">
            {!! Form::label('citta', 'Indirizzo, completo di numero civico') !!}
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1"><i class="fas fa-map-marked-alt"></i></span>
                </div>
                {!! Form::text('address', null, ['id' => 'citta', 'class' => 'form-control', 'placeholder' => 'Inserisci l\'indirizzo dell\'attività completo di numero civico', 'required' => true]) !!}
                <div id="shop_address_components">
                    {!! Form::hidden('country', null, ['id' => 'country', 'class' => 'form-control country']) !!}
                    {!! Form::hidden('region', null, ['id' => 'administrative_area_level_1','class' => 'form-control administrative_area_level_1']) !!}
                    {!! Form::hidden('city', null, ['id' => 'locality', 'class' => 'form-control locality']) !!}
                    {!! Form::hidden('prov', null, ['id' => 'administrative_area_level_2', 'class' => 'form-control administrative_area_level_2']) !!}
                    {!! Form::hidden('route', null, ['id' => 'route', 'class' => 'form-control route']) !!}
                    {!! Form::hidden('street_number', null, ['id' => 'street_number', 'class' => 'form-control street_number']) !!}
                    {!! Form::hidden('postal_code', null, ['id' => 'postal_code', 'class' => 'form-control postal_code']) !!}
                    {!! Form::hidden('lat', null, ['id' => 'lat', 'class' => 'form-control lat']) !!}
                    {!! Form::hidden('long', null, ['id' => 'long', 'class' => 'form-control long']) !!}
                </div>
            </div>
            @error('address')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            @error('route')
                <div class="alert alert-danger">L'indirizzo risulta incompleto</div>
            @enderror
        </div>

        <div class="form-group @if($errors->has('phone')) has-error @endif">
            {!! Form::label('phone', 'Telefono attività') !!}
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1"><i class="fas fa-phone"></i></span>
                </div>
                {!! Form::text('phone', null, ['class' => 'form-control', 'placeholder' => 'Telefono attività', 'required' => true]) !!}
            </div>
            @error('phone')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>

        <div class="form-group @if($errors->has('whatsapp')) has-error @endif">
            {!! Form::label('whatsapp', 'Telefono cellulare') !!}

            <input type="checkbox" id="show_phone_on_app" name="show_phone_on_app" class="ml-3">
            <label for="show_phone_on_app"><small>Mostra sull'app</small></label>

            <div class="input-group mb-3">
                <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1"><i class="fas fa-mobile-alt"></i></span>
                </div>
                {!! Form::text('whatsapp', null, ['class' => 'form-control', 'placeholder'=> 'Cellulare di un responsabile/attività']) !!}
            </div>
            @error('whatsapp')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>

        <div class="form-group @if($errors->has('email')) has-error @endif">
            {!! Form::label('email', 'Email') !!}
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1"><i class="far fa-envelope"></i></span>
                </div>
                {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Scrivi la email di un responsabile', 'required' => true]) !!}
            </div>
            @error('email')
                <div class="alert alert-danger">{{ $message }}</div>
                <div class="alert alert-danger">L'email è obbligatoria</div>
            @enderror
        </div>

        <div class="form-group @if($errors->has('user_name')) has-error @endif">
            {!! Form::label('user_name', 'Nome e Cognome di un titolare legale') !!}
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1"><i class="fa fa-user"></i></span>
                </div>
                {!! Form::text('user_name', null, ['class' => 'form-control', 'placeholder' => 'Nome del responsabile']) !!}
            </div>
            @error('user_name')
                <div class="alert alert-danger">{{ $message }}</div>
                <div class="alert alert-danger">Inserire il titolare legale</div>
            @enderror
        </div>

        <div class="form-group @if($errors->has('user_birthdate') || $errors->has('age')) has-error @endif">
            {!! Form::label('user_birthdate', 'Data di nascita del titolare') !!}
            <div class="input-group mb-3">
                <div class="input-group-prepend" data-target="#user_birthdate" data-toggle="datetimepicker">
                    <span class="input-group-text" id="basic-addon1"><i class="far fa-calendar-alt"></i></span>
                </div>

                <input type="text" id="user_birthdate"
                                    name="user_birthdate"
                                    class="form-control datetimepicker-input"
                                    data-target="#user_birthdate"
                                    placeholder="gg/mm/aaaa"
                                    readonly />
            </div>
            @error('user_birthdate')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            @error('age')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>

        <div class="form-group @if($errors->has('website')) has-error @endif">
            {!! Form::label('website', 'Sito web') !!}
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1"><i class="fas fa-globe"></i></span>
                </div>
                {!! Form::text('website', null, ['class' => 'form-control', 'placeholder' => 'https://www.ilmionegozio.it']) !!}
            </div>
            @error('website')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>

        <div class="form-group @if($errors->has('payment_method_id')) has-error @endif">
            {!! Form::label('payment_method_id', 'Metodi di pagamento accettati in sede') !!}
            <div class="input-group mb-0">
                <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1"><i class="far fa-credit-card"></i></span>
                </div>
                {!! Form::select('payment_method_id[]', $arr_payment_methods, null, ['class' => 'form-control', 'multiple' => true]) !!}
            </div>
            @error('payment_method_id')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            <small class="press-ctrl">Premi CTRL per selezionare più metodi di pagamento</small>
        </div>
		<div class="form-group">
			<label for="subscription_id">Abbonamento</label>
			<select id="subscription" name="subscription" class="form-control" required>
				<option value="">---</option>
				@foreach($subscriptions as $subscription)
				<option value="{{ $subscription->subscription }}">{{ $subscription->name.' ('.$subscription->products_num.' prodotti)' }}</option>
				@endforeach
			</select>
		</div>
    </div>
</div>
