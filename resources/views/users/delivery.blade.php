<div class="row">
    <div class="col-12 col-sm-12 col-md-12 col-lg-12">
        <h5>Si può ritirare la merce presso
            <i class="fas fa-info-circle" data-toggle="tooltip" data-placement="top" title="Puoi decidere di far ritirare i tuoi prodotti presso la tua sede principale e/o presso una sede secondaria"></i>
        </h5>
        <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
            <input type="checkbox" class="custom-control-input control-delivery-address" id="control-delivery-address" name="control-delivery-address"
            @if( ( trim($user->shop->address) !== '' && trim($user->shop->delivery_address) !== '' && $user->shop->address === $user->shop->delivery_address) || trim($user->shop->delivery_address) == '' || old('control-delivery-address') === 'on')
            checked
            @endif
            >
            {!! Form::label('control-delivery-address', 'Coincide con la sede aziendale', ['class' => 'custom-control-label']) !!}
        </div>
        <div class="form-delivery-address" style="display:
            @if( Request::has('delivery_address_route') || ($user->shop->address !== '' && $user->shop->delivery_address !== '' && $user->shop->address === $user->shop->delivery_address)  || trim($user->shop->delivery_address) == '' || old('control-delivery-address') === 'on' )
            none
            @else
            block
            @endif
            ">
            {!! Form::label('delivery_citta', 'Indirizzo di ritiro, completo di numero civico') !!}
            <div class="input-group @error('delivery_address_route') has-error @endif">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1"><i class="fas fa-map-marked-alt"></i></span>
                </div>
                {!! Form::text('delivery_address', old('delivery_address', $user->shop->delivery_address), ['id' => 'delivery_citta', 'class' => 'form-control', 'placeholder' => 'Inserisci l\'indirizzo di ritiro con il civico']) !!}
                <div id="delivery_address_components">
                    {!! Form::hidden('delivery_address_country', old('delivery_address_country',$user->shop->delivery_address_country), ['id' => 'country', 'class' => 'form-control country']) !!}
                    {!! Form::hidden('delivery_address_region', old('delivery_address_region',$user->shop->delivery_address_region), ['id' => 'administrative_area_level_1','class' => 'form-control administrative_area_level_1']) !!}
                    {!! Form::hidden('prov', null, ['id' => 'administrative_area_level_2', 'class' => 'form-control administrative_area_level_2']) !!}
                    {!! Form::hidden('delivery_address_city', old('delivery_address_city',$user->shop->delivery_address_city), ['id' => 'locality', 'class' => 'form-control locality']) !!}
                    {!! Form::hidden('delivery_address_route', old('delivery_address_route',$user->shop->delivery_address_route), ['id' => 'route', 'class' => 'form-control route']) !!}
                    {!! Form::hidden('delivery_address_street_number', old('delivery_address_street_number',$user->shop->delivery_address_street_number), ['id' => 'street_number', 'class' => 'form-control street_number']) !!}
                    {!! Form::hidden('delivery_address_postal_code', old('delivery_address_postal_code',$user->shop->delivery_address_postal_code), ['id' => 'postal_code', 'class' => 'form-control postal_code']) !!}
                    {!! Form::hidden('delivery_address_lat', old('delivery_address_lat',$user->shop->delivery_address_lat), ['id' => 'lat', 'class' => 'form-control lat']) !!}
                    {!! Form::hidden('delivery_address_long', old('delivery_address_long',$user->shop->delivery_address_long), ['id' => 'long', 'class' => 'form-control long']) !!}
                </div>
            </div>
            @error('delivery_address_route')
                <div class="alert alert-danger">L'indirizzo non risulta valido</div>
            @enderror
        </div>

        <hr>

        {{-- Delivery timetable --}}
        <h5>
            Orari per il ritiro della merce
            <i class="fas fa-info-circle" data-toggle="tooltip" data-placement="top" title="Puoi decidere di far ritirare i tuoi prodotti sempre quando sei aperto o solo in determinati orari"></i>
        </h5>

        <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
            <input type="checkbox" class="custom-control-input control-delivery-timetable" id="control-delivery-timetable" name="control-delivery-timetable"
            @if( (empty(old('_token')) && json_encode($user->shop->obj_opening_timetable) === json_encode($user->shop->obj_delivery_opening_timetable)) || old('control-delivery-timetable') === 'on')
            checked
            @endif
            >
            {!! Form::label('control-delivery-timetable', 'Coincidono con i miei orari di apertura', ['class' => 'custom-control-label']) !!}
        </div>

        @error('alert_delivery_days')
            <div class="alert alert-danger p-1 mt-0">{{ $message }}</div>
        @enderror

        <div class="form-delivery-timetable mt-2" style="display:
            @if( (empty(old('_token')) && json_encode($user->shop->obj_opening_timetable) === json_encode($user->shop->obj_delivery_opening_timetable)) || old('control-delivery-timetable') === 'on')
            none
            @else
            block
            @endif
            ">

            @php
            $arr_days = ['monday','tuesday','wednesday','thursday','friday','saturday','sunday'];
            $arr_days_it = ['LUNEDI', 'MARTEDI', 'MERCOLEDI', 'GIOVEDI', 'VENERDI', 'SABATO', 'DOMENICA'];
            @endphp

            @foreach($arr_days as $k => $day)

                @php
                $short_day = substr($day, 0, 3);
                @endphp

                <div class="row row-check-day">
                    <div class="col-12">
                        <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                            <input type="checkbox" class="custom-control-input control-delivery-day" id="control-delivery-{{ $day }}" name="delivery_{{ $day }}_active" data-day="{{ $day }}"
                            @if( (empty(old('_token')) && json_encode($user->shop->obj_opening_timetable) !== json_encode($user->shop->obj_delivery_opening_timetable) && $user->shop->obj_delivery_opening_timetable->{$day}->active) || old('delivery_'.$day.'_active') === 'on')
                            checked
                            @endif
                            >
                            {!! Form::label('control-delivery-'.$day, $arr_days_it[$k], ['class' => 'custom-control-label']) !!}
                        </div>
                    </div>
                </div>
                <div class="row row-day bg-default row-delivery-{{ $day }}" style="display:
                    @if( (empty(old('_token')) &&
                        json_encode($user->shop->obj_opening_timetable) !== json_encode($user->shop->obj_delivery_opening_timetable) &&
                        $user->shop->obj_delivery_opening_timetable->{$day}->active) ||
                        old('delivery_'.$day.'_active') === 'on' )
                    flex
                    @else
                    none
                    @endif
                    ">
                    <div class="col-6">
                        {!! Form::label($short_day.'_mor_del', 'Mattina') !!}
                        <div class="row">
                            <div class="col-12 text-center px-4">
                                <label for="delivery_{{ $short_day }}_mor" ><i class="fa fa-clock"></i> <span id="lbl_delivery_{{ $short_day }}_mor"></span></label>
                                <input type="text" id="delivery_{{ $short_day }}_mor" class="time-slider" readonly style="border:0; color:#f6931f; font-weight:bold;">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                {!! Form::hidden('delivery_'.$short_day.'_mor_open', $user->shop->obj_delivery_opening_timetable->{$day}->first_half->opening_time ?? '00:00') !!}
                            </div>
                            <div class="col-6">
                                {!! Form::hidden('delivery_'.$short_day.'_mor_close', $user->shop->obj_delivery_opening_timetable->{$day}->first_half->closing_time ?? '18:00') !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        {!! Form::label($short_day.'_aft_del', 'Pomeriggio') !!}
                        <div class="row">
                            <div class="col-12 text-center px-4">
                                <label for="delivery_{{ $short_day }}_aft" ><i class="fa fa-clock"></i> <span id="lbl_delivery_{{ $short_day }}_aft"></span></label>
                                <input type="text" id="delivery_{{ $short_day }}_aft" class="time-slider" readonly style="border:0; color:#f6931f; font-weight:bold;">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                {!! Form::hidden('delivery_'.$short_day.'_aft_open', $user->shop->obj_delivery_opening_timetable->{$day}->second_half->opening_time ?? '12:00') !!}
                            </div>
                            <div class="col-6">
                                {!! Form::hidden('delivery_'.$short_day.'_aft_close', $user->shop->obj_delivery_opening_timetable->{$day}->second_half->closing_time ?? '24:00') !!}
                            </div>
                        </div>
                    </div>
                </div>
                @error('val_delivery_opening_timetable.'.$day)
                    <div class="alert alert-danger p-1 mt-0">{{ $message }}</div>
                @enderror

                <hr>

            @endforeach

        </div>
        {{-- End delivery timetable --}}

        <hr>

        <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
            <input type="checkbox" class="custom-control-input control-home-delivery" name="home_delivery" id="control-home-delivery"
            @if( (empty(old('_token')) && $user->shop->home_delivery) || old('home_delivery') === 'on' )
            checked
            @endif
            >
            {!! Form::label('control-home-delivery', 'In questo momento effettuo consegne a domicilio', ['class' => 'custom-control-label']) !!}
        </div>

        <div class="row form-home-delivery text-center" style="margin-top: 40px; display:
            @if( (empty(old('_token')) && $user->shop->home_delivery) || old('home_delivery') === 'on' || ( !empty(old('_token')) && !empty(old('home_delivery'))) )
            block
            @else
            none
            @endif
            ">

            <div class="row">
                <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="form-group">
                        <label for="raggio">Consegno entro un raggio di </label>
                        <div class="input-group mb-3">
                            {!! Form::number('delivery_range_km', $user->shop->delivery_range_km, ['id' => 'raggio', 'class' => 'form-control', 'placeholder' => 'Km', 'min' => 1, 'required' => $user->shop->home_delivery == 1 ? true : false ]) !!}
                            <div class="input-group-append">
                                <span class="input-group-text" id="raggio-post">KM</span>
                            </div>
                        </div>
                        @error('delivery_range_km')
                            <div class="alert alert-danger p-1 mt-0">{{ $message }}</div>
                        @else
                            @if( $user->shop->home_delivery == 1 && empty($user->shop->delivery_range_km) )
                                <div class="alert alert-danger p-1 mt-0">Indicare un raggio minimo di 1Km per la consegna a domicilio</div>
                            @endif
                        @enderror
                        <a href="https://www.calcmaps.com/it/map-radius/" target="_blank"><small>Per stabilire consapevolmente il tuo raggio di consegna puoi aiutarti cliccando qui</small></a>
                    </div>

                    <div class="form-group">
                        {!! Form::textarea('delivery_range_notes', $user->shop->delivery_range_notes, ['class' => 'form-control', 'placeholder' => 'Scrivi qui eventuali limitazioni o dettagli in merito al raggio delle tue consegne a domicilio. es. Non consegno nelle ZTL', 'rows' => 3]) !!}
                    </div>
                </div>
                <div class="col-12 col-sm-12 offset-md-1 offset-lg-1 col-md-7 col-lg-7">
                    <div class="form-group text-center" >
                        <h3>Costi per la mia consegna a domicilio</h3>

                        <div class="row" style="border-bottom: 1px solid #ced4da; margin: 10px 0">
                            <div class="col-8 text-left">

                                {!! Form::label('home_delivery_min', 'Ordine minimo per la consegna',['class' => 'mb-0']) !!}
                                <br class="m-0">
                                <small class="mt-0 text-secondary">Lasciare vuoto il campo se non esiste un minimo</small>

                            </div>
                            <div class="col-4">
                                <div id="home_delivery_min">
                                    <div class="input-group mb-3">
                                        {!! Form::number('home_delivery_min', $user->shop->home_delivery_min, ['class' => 'form-control', 'min' => 0, 'step' => 'any' ]) !!}
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="basic-addon1">&euro;</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="border-bottom: 1px solid #ced4da; margin: 10px 0">
                            <div class="col-8 text-left">
                                <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                                    <input type="checkbox" class="custom-control-input" id="chk_delivery_forfait_cost_price"
                                    @if( $user->shop->delivery_forfait_cost_price > 0 )
                                    checked
                                    @endif
                                    >
                                    {!! Form::label('chk_delivery_forfait_cost_price', 'Costi fissi di consegna', ['class' => 'custom-control-label']) !!}
                                    <span class="text-secondary" style="display: block;"><small>Lasciare la barra rossa se la consegna è sempre gratuita</small></span>
                                </div>
                            </div>
                            <div class="col-4">
                                <div id="delivery_forfait_cost_price" style="display: @if( $user->shop->delivery_forfait_cost_price > 0 ) block @else none @endif">
                                    <div class="input-group mb-3">
                                        {!! Form::text('delivery_forfait_cost_price', number_format($user->shop->delivery_forfait_cost_price, 2, ',', '.'), ['id' => 'delivery_forfait_cost_price', 'class' => 'form-control', 'min' => 0]) !!}
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="basic-addon1">&euro;</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="border-bottom: 1px solid #ced4da; margin: 10px 0">
                            <div class="col-8 text-left">
                                <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                                    <input type="checkbox" class="custom-control-input" id="chk_delivery_percentage_cost_price"
                                    @if( $user->shop->delivery_percentage_cost_price > 0 )
                                    checked
                                    @endif
                                    >
                                    {!! Form::label('chk_delivery_percentage_cost_price', 'Consegna al costo variabile pari a', ['class' => 'custom-control-label']) !!}
                                    <span class="text-secondary" style="display: block;"><small>Lasciare la barra rossa se la consegna è sempre gratuita</small></span>
                                </div>
                            </div>
                            <div class="col-4">
                                <div id="delivery_percentage_cost_price" style="display: @if( $user->shop->delivery_percentage_cost_price > 0 ) block @else none @endif">
                                    <div class="input-group mb-3">
                                        {!! Form::number('delivery_percentage_cost_price', $user->shop->delivery_percentage_cost_price, ['class' => 'form-control', 'min' => 0]) !!}
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="basic-addon1">% ordine</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="border-bottom: 1px solid #ced4da; margin: 10px 0">
                            <div class="col-8 text-left">
                                <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                                    <input type="checkbox" class="custom-control-input" id="chk_delivery_free_price_greater_than"
                                    @if( $user->shop->delivery_free_price_greater_than )
                                    checked
                                    @endif
                                    >
                                    {!! Form::label('chk_delivery_free_price_greater_than', 'Consegna gratuita per ordini superiori a', ['class' => 'custom-control-label']) !!}
                                    <span class="text-secondary" style="display: block;"><small>Lasciare la barra rossa se la consegna è sempre gratuita</small></span>
                                </div>
                            </div>
                            <div class="col-4">
                                <div id="delivery_free_price_greater_than" style="display: @if( $user->shop->delivery_free_price_greater_than ) block @else none @endif">
                                    <div class="input-group mb-3">
                                        {!! Form::text('delivery_free_price_greater_than', number_format($user->shop->delivery_free_price_greater_than, 2, ',', '.'), ['class' => 'form-control', 'min' => 0]) !!}
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="basic-addon1">&euro;</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                        {!! Form::label('delivery_payment_method_id', 'Metodi di pagamento accettati per la consegna a domicilio') !!}
                        @error('delivery_payment_method_id')
                            <div class="alert alert-danger p-1 mt-0">{{ $message }}</div>
                        @else
                            @if($user->shop->home_delivery == 1 && $user->shop->PaymentMethodsDelivery->count()==0)
                                <div class="alert alert-danger p-1 mt-0">Selezionare almeno un metodo di pagamento per la consegna a domicilio</div>
                            @endif
                        @enderror
                        <div class="input-group mb-0">
                            <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="far fa-credit-card"></i></span>
                            </div>
                            {!! Form::select('delivery_payment_method_id[]', $user->payment_method, $user->shop->PaymentMethodsDelivery()->get()->pluck('id')->toArray(), ['class' => 'form-control', 'multiple' => true]) !!}
                        </div>
                        <small class="press-ctrl">Premi CTRL per selezionare più metodi di pagamento</small>
                    </div>
                </div>
            </div>
        </div>

        <input type="hidden" id="delivery_host_on_site" name="delivery_host_on_site" value="0">

        {{--
        <hr>

        <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
            <input type="checkbox" class="custom-control-input control-delivery-on-site" name="delivery_host_on_site" id="control-delivery-on-site" value="1"
            @if( old('delivery_host_on_site', $user->shop->delivery_host_on_site) )
            checked
            @endif
            >
            {!! Form::label('control-delivery-on-site', 'Effettuo consegne ai miei ospiti sul posto', ['class' => 'custom-control-label']) !!}
            <i class="fas fa-info-circle" data-toggle="tooltip" data-placement="top" title="Una modalità di consegna particolarmente adatta per Bar, Camping, Ristoranti o altre attività che portano gli ordini, in tempo reale, direttamente in sede."></i>
        </div>
         --}}

        <hr>

        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 text-center">
                {!! Form::label('delivery_cancellable_hours_limit', 'Limiti per annullare gli ordini senza penalità') !!}
                <div class="row">
                    <div class="col-12 col-sm-12 offset-md-5 col-md-2 offset-lg-5 col-lg-2">
                        <div class="form-group text-center">
                            <i class="fas fa-info-circle" data-toggle="tooltip" data-placement="top" title="Indica il limite temporale entro il quale i tuoi clienti potranno annullare i loro ordini senza vedersi scalare punti fiducia"></i>
                            <div class="input-group mb-3">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon1"><i class="fas fa-clock"></i></span>
                                </div>
                                {!! Form::text('delivery_cancellable_hours_limit', $user->shop->delivery_cancellable_hours_limit, ['class' => 'form-control', 'required']) !!}
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">ore</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 text-center">
                        SI CONSIGLIA DI IMPOSTARE UN VALORE DI ALMENO UN'ORA
                    </div>
                </div>
            </div>
        </div>

        <hr>

        <div class="form-group text-center">
            <h5>
                Affidabilità dei clienti in Punti Fiducia
                <i class="fas fa-info-circle" data-toggle="tooltip" data-placement="top" title="I punti fiducia servono a misurare l'affidabilità del privato che acquista. Dotazione inizialmente 100 punti fiducia. Se il Privato non intende portare a termine un ordine, potrà annullarlo senza problemi purchè avvisi l'azienda entro i limiti temporali previsti. Se mancherà di farlo l'azienda avrà facoltà di sottrargli 3 punti fiducia. Oppure di perdonarlo. Al contrario, ogni volta che un Privato onorerà l'ordine come promesso, gli verrà accreditato 1 punto fiducia."></i>
            </h5>
            <div style="clear: both"></div>
            <i class="far fa-handshake" style="font-size: 40px"></i>

            <label for="min_trust_points_percentage_bookable" id="lbl_min_trust_points">LE OFFERTE SARANNO PRENOTABILI SOLO DAI CLIENTI CON UN MINIMO DI <span style="color: #13bf9e">{{ ( empty($user->shop->min_trust_points_percentage_bookable) ? 60 : $user->shop->min_trust_points_percentage_bookable ) }}</span> PUNTI FIDUCIA</label>
            <div class="slider-success" style="padding: 0 40px">
                <input type="text" value="" name="min_trust_points_percentage_bookable" class="slider slider-horizontal slider-trust-points form-control">
            </div>

            <small>Consigliamo un valore minimo di 90 punti fiducia. Potrai modificarlo in qualunque momento</small>
        </div>

    </div>
</div>
