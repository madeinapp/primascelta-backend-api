<div class="row">
    <div class="col-12 col-sm-12 col-md-12 col-lg-12">
        <h5>Si può ritirare la merce presso
            <i class="fas fa-info-circle" data-toggle="tooltip" data-placement="top" title="Puoi decidere di far ritirare i tuoi prodotti presso la tua sede principale e/o presso una sede secondaria"></i>
        </h5>
        <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
            <input type="checkbox" class="custom-control-input control-delivery-address" id="control-delivery-address" name="control-delivery-address" checked>
            {!! Form::label('control-delivery-address', 'Coincide con la sede aziendale', ['class' => 'custom-control-label']) !!}
        </div>
        <div class="form-delivery-address" style="display:none">
            {!! Form::label('delivery_citta', 'Indirizzo di ritiro, completo di numero civico') !!}
            <div class="input-group @error('delivery_address_route') has-error @endif">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1"><i class="fas fa-map-marked-alt"></i></span>
                </div>
                {!! Form::text('delivery_address', old('delivery_address', null), ['id' => 'delivery_citta', 'class' => 'form-control', 'placeholder' => 'Inserisci l\'indirizzo di ritiro con il civico']) !!}
                <div id="delivery_address_components">
                    {!! Form::hidden('delivery_address_country', old('delivery_address_country',null), ['id' => 'country', 'class' => 'form-control country']) !!}
                    {!! Form::hidden('delivery_address_region', old('delivery_address_region',null), ['id' => 'administrative_area_level_1','class' => 'form-control administrative_area_level_1']) !!}
                    {!! Form::hidden('prov', null, ['id' => 'administrative_area_level_2', 'class' => 'form-control administrative_area_level_2']) !!}
                    {!! Form::hidden('delivery_address_city', old('delivery_address_city',null), ['id' => 'locality', 'class' => 'form-control locality']) !!}
                    {!! Form::hidden('delivery_address_route', old('delivery_address_route',null), ['id' => 'route', 'class' => 'form-control route']) !!}
                    {!! Form::hidden('delivery_address_street_number', old('delivery_address_street_number',null), ['id' => 'street_number', 'class' => 'form-control street_number']) !!}
                    {!! Form::hidden('delivery_address_postal_code', old('delivery_address_postal_code',null), ['id' => 'postal_code', 'class' => 'form-control postal_code']) !!}
                    {!! Form::hidden('delivery_address_lat', old('delivery_address_lat',null), ['id' => 'lat', 'class' => 'form-control lat']) !!}
                    {!! Form::hidden('delivery_address_long', old('delivery_address_long',null), ['id' => 'long', 'class' => 'form-control long']) !!}
                </div>
            </div>
            @error('delivery_address_route')
                <div class="alert alert-danger">L'indirizzo non risulta valido</div>
            @enderror
        </div>

        <hr>

        {{-- Delivery timetable --}}
        <h5>
            Orari per il ritiro della merce
            <i class="fas fa-info-circle" data-toggle="tooltip" data-placement="top" title="Puoi decidere di far ritirare i tuoi prodotti sempre quando sei aperto o solo in determinati orari"></i>
        </h5>

        <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
            <input type="checkbox" class="custom-control-input control-delivery-timetable" id="control-delivery-timetable" name="control-delivery-timetable">
            {!! Form::label('control-delivery-timetable', 'Coincidono con i miei orari di apertura', ['class' => 'custom-control-label']) !!}
        </div>

        @error('alert_delivery_days')
            <div class="alert alert-danger p-1 mt-0">{{ $message }}</div>
        @enderror

        <div class="form-delivery-timetable mt-2" style="display:block">
            @php
            $arr_days = ['monday','tuesday','wednesday','thursday','friday','saturday','sunday'];
            $arr_days_it = ['LUNEDI', 'MARTEDI', 'MERCOLEDI', 'GIOVEDI', 'VENERDI', 'SABATO', 'DOMENICA'];
            @endphp

            @foreach($arr_days as $k => $day)

                @php
                $short_day = substr($day, 0, 3);
                @endphp

                <div class="row row-check-day">
                    <div class="col-12">
                        <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                            <input type="checkbox" class="custom-control-input control-delivery-day" id="control-delivery-{{ $day }}" name="delivery_{{ $day }}_active" data-day="{{ $day }}"
                            >
                            {!! Form::label('control-delivery-'.$day, $arr_days_it[$k], ['class' => 'custom-control-label']) !!}
                        </div>
                    </div>
                </div>
                <div class="row row-day bg-default row-delivery-{{ $day }}" style="display:none">
                    <div class="col-6">
                        {!! Form::label($short_day.'_mor_del', 'Mattina') !!}
                        <div class="row">
                            <div class="col-12 text-center px-4">
                                <label for="delivery_{{ $short_day }}_mor" ><i class="fa fa-clock"></i> <span id="lbl_delivery_{{ $short_day }}_mor"></span></label>
                                <input type="text" id="delivery_{{ $short_day }}_mor" class="time-slider" readonly style="border:0; color:#f6931f; font-weight:bold;">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                {!! Form::hidden('delivery_'.$short_day.'_mor_open', '00:00') !!}
                            </div>
                            <div class="col-6">
                                {!! Form::hidden('delivery_'.$short_day.'_mor_close', '18:00') !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        {!! Form::label($short_day.'_aft_del', 'Pomeriggio') !!}
                        <div class="row">
                            <div class="col-12 text-center px-4">
                                <label for="delivery_{{ $short_day }}_aft" ><i class="fa fa-clock"></i> <span id="lbl_delivery_{{ $short_day }}_aft"></span></label>
                                <input type="text" id="delivery_{{ $short_day }}_aft" class="time-slider" readonly style="border:0; color:#f6931f; font-weight:bold;">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                {!! Form::hidden('delivery_'.$short_day.'_aft_open', '12:00') !!}
                            </div>
                            <div class="col-6">
                                {!! Form::hidden('delivery_'.$short_day.'_aft_close', '24:00') !!}
                            </div>
                        </div>
                    </div>
                </div>
                @error('val_delivery_opening_timetable.'.$day)
                    <div class="alert alert-danger p-1 mt-0">{{ $message }}</div>
                @enderror

                <hr>

            @endforeach

        </div>
        {{-- End delivery timetable --}}

        <hr>

        <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
            <input type="checkbox" class="custom-control-input control-home-delivery" name="home_delivery" id="control-home-delivery"
            >
            {!! Form::label('control-home-delivery', 'In questo momento effettuo consegne a domicilio', ['class' => 'custom-control-label']) !!}
        </div>

        <div class="row form-home-delivery text-center" style="margin-top: 40px; display:none
            ">

            <div class="row">
                <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="form-group">
                        <label for="raggio">Consegno entro un raggio di </label>
                        <div class="input-group mb-3">
                            {!! Form::number('delivery_range_km', null, ['id' => 'raggio', 'class' => 'form-control', 'placeholder' => 'Km', 'min' => 1 ]) !!}
                            <div class="input-group-append">
                                <span class="input-group-text" id="raggio-post">KM</span>
                            </div>
                        </div>
                        @error('delivery_range_km')
                            <div class="alert alert-danger p-1 mt-0">{{ $message }}</div>
                        @else
                                <div class="alert alert-danger p-1 mt-0">Indicare un raggio minimo di 1Km per la consegna a domicilio</div>
                        @enderror
                        <a href="https://www.calcmaps.com/it/map-radius/" target="_blank"><small>Per stabilire consapevolmente il tuo raggio di consegna puoi aiutarti cliccando qui</small></a>
                    </div>

                    <div class="form-group">
                        {!! Form::textarea('delivery_range_notes', null, ['class' => 'form-control', 'placeholder' => 'Scrivi qui eventuali limitazioni o dettagli in merito al raggio delle tue consegne a domicilio. es. Non consegno nelle ZTL', 'rows' => 3]) !!}
                    </div>
                </div>
                <div class="col-12 col-sm-12 offset-md-1 offset-lg-1 col-md-7 col-lg-7">
                    <div class="form-group text-center" >
                        <h3>Costi per la mia consegna a domicilio</h3>

                        <div class="row" style="border-bottom: 1px solid #ced4da; margin: 10px 0">
                            <div class="col-8 text-left">

                                {!! Form::label('home_delivery_min', 'Ordine minimo per la consegna',['class' => 'mb-0']) !!}
                                <br class="m-0">
                                <small class="mt-0 text-secondary">Lasciare vuoto il campo se non esiste un minimo</small>

                            </div>
                            <div class="col-4">
                                <div id="home_delivery_min">
                                    <div class="input-group mb-3">
                                        {!! Form::number('home_delivery_min', null, ['class' => 'form-control', 'min' => 0, 'step' => 'any' ]) !!}
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="basic-addon1">&euro;</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="border-bottom: 1px solid #ced4da; margin: 10px 0">
                            <div class="col-8 text-left">
                                <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                                    <input type="checkbox" class="custom-control-input" id="chk_delivery_forfait_cost_price"
                                    >
                                    {!! Form::label('chk_delivery_forfait_cost_price', 'Costi fissi di consegna', ['class' => 'custom-control-label']) !!}
                                    <span class="text-secondary" style="display: block;"><small>Lasciare la barra rossa se la consegna è sempre gratuita</small></span>
                                </div>
                            </div>
                            <div class="col-4">
                                <div id="delivery_forfait_cost_price" style="display:block">
                                    <div class="input-group mb-3">
                                        {!! Form::text('delivery_forfait_cost_price', null, ['id' => 'delivery_forfait_cost_price', 'class' => 'form-control', 'min' => 0]) !!}
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="basic-addon1">&euro;</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="border-bottom: 1px solid #ced4da; margin: 10px 0">
                            <div class="col-8 text-left">
                                <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                                    <input type="checkbox" class="custom-control-input" id="chk_delivery_percentage_cost_price"
                                    >
                                    {!! Form::label('chk_delivery_percentage_cost_price', 'Consegna al costo variabile pari a', ['class' => 'custom-control-label']) !!}
                                    <span class="text-secondary" style="display: block;"><small>Lasciare la barra rossa se la consegna è sempre gratuita</small></span>
                                </div>
                            </div>
                            <div class="col-4">
                                <div id="delivery_percentage_cost_price" style="display:block">
                                    <div class="input-group mb-3">
                                        {!! Form::number('delivery_percentage_cost_price', null, ['class' => 'form-control', 'min' => 0]) !!}
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="basic-addon1">% ordine</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="border-bottom: 1px solid #ced4da; margin: 10px 0">
                            <div class="col-8 text-left">
                                <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                                    <input type="checkbox" class="custom-control-input" id="chk_delivery_free_price_greater_than"
                                    >
                                    {!! Form::label('chk_delivery_free_price_greater_than', 'Consegna gratuita per ordini superiori a', ['class' => 'custom-control-label']) !!}
                                    <span class="text-secondary" style="display: block;"><small>Lasciare la barra rossa se la consegna è sempre gratuita</small></span>
                                </div>
                            </div>
                            <div class="col-4">
                                <div id="delivery_free_price_greater_than" style="display:block">
                                    <div class="input-group mb-3">
                                        {!! Form::text('delivery_free_price_greater_than', null, ['class' => 'form-control', 'min' => 0]) !!}
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="basic-addon1">&euro;</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                        {!! Form::label('delivery_payment_method_id', 'Metodi di pagamento accettati per la consegna a domicilio') !!}
                        @error('delivery_payment_method_id')
                            <div class="alert alert-danger p-1 mt-0">{{ $message }}</div>
                        @else
                                <div class="alert alert-danger p-1 mt-0">Selezionare almeno un metodo di pagamento per la consegna a domicilio</div>
                        @enderror
                        <div class="input-group mb-0">
                            <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="far fa-credit-card"></i></span>
                            </div>
                            {!! Form::select('delivery_payment_method_id[]', $arr_payment_methods, null, ['class' => 'form-control', 'multiple' => true]) !!}
                        </div>
                        <small class="press-ctrl">Premi CTRL per selezionare più metodi di pagamento</small>
                    </div>
                </div>
            </div>
        </div>

        <input type="hidden" id="delivery_host_on_site" name="delivery_host_on_site" value="0">

        {{--
        <hr>

        <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
            <input type="checkbox" class="custom-control-input control-delivery-on-site" name="delivery_host_on_site" id="control-delivery-on-site" value="1"
            >
            {!! Form::label('control-delivery-on-site', 'Effettuo consegne ai miei ospiti sul posto', ['class' => 'custom-control-label']) !!}
            <i class="fas fa-info-circle" data-toggle="tooltip" data-placement="top" title="Una modalità di consegna particolarmente adatta per Bar, Camping, Ristoranti o altre attività che portano gli ordini, in tempo reale, direttamente in sede."></i>
        </div>
         --}}

        <hr>

        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 text-center">
                {!! Form::label('delivery_cancellable_hours_limit', 'Limiti per annullare gli ordini senza penalità') !!}
                <div class="row">
                    <div class="col-12 col-sm-12 offset-md-5 col-md-2 offset-lg-5 col-lg-2">
                        <div class="form-group text-center">
                            <i class="fas fa-info-circle" data-toggle="tooltip" data-placement="top" title="Indica il limite temporale entro il quale i tuoi clienti potranno annullare i loro ordini senza vedersi scalare punti fiducia"></i>
                            <div class="input-group mb-3">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon1"><i class="fas fa-clock"></i></span>
                                </div>
                                {!! Form::text('delivery_cancellable_hours_limit', null, ['class' => 'form-control']) !!}
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">ore</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 text-center">
                        SI CONSIGLIA DI IMPOSTARE UN VALORE DI ALMENO UN'ORA
                    </div>
                </div>
            </div>
        </div>

        <hr>

        <div class="form-group text-center">
            <h5>
                Affidabilità dei clienti in Punti Fiducia
                <i class="fas fa-info-circle" data-toggle="tooltip" data-placement="top" title="I punti fiducia servono a misurare l'affidabilità del privato che acquista. Dotazione inizialmente 100 punti fiducia. Se il Privato non intende portare a termine un ordine, potrà annullarlo senza problemi purchè avvisi l'azienda entro i limiti temporali previsti. Se mancherà di farlo l'azienda avrà facoltà di sottrargli 3 punti fiducia. Oppure di perdonarlo. Al contrario, ogni volta che un Privato onorerà l'ordine come promesso, gli verrà accreditato 1 punto fiducia."></i>
            </h5>
            <div style="clear: both"></div>
            <i class="far fa-handshake" style="font-size: 40px"></i>

            <label for="min_trust_points_percentage_bookable" id="lbl_min_trust_points">LE OFFERTE SARANNO PRENOTABILI SOLO DAI CLIENTI CON UN MINIMO DI <span style="color: #13bf9e">80</span> PUNTI FIDUCIA</label>
            <div class="slider-success" style="padding: 0 40px">
                <input type="text" value="80" name="min_trust_points_percentage_bookable" class="slider slider-horizontal slider-trust-points form-control">
            </div>

            <small>Consigliamo un valore minimo di 90 punti fiducia. Potrai modificarlo in qualunque momento</small>
        </div>

    </div>
</div>
