@extends('adminlte::page')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Profilo</h1>
        <div class="pull-right  float-right d-none d-sm-block" >
           <!--a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('users.create') !!}">Aggiungi nuovo</a-->
        </div>
    </section>
    <div class="content">

        <div class="clearfix"></div>

        @include('flash::message')

        {{--
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        --}}

        <div class="clearfix"></div>

        {!! Form::open(array('url' => route('users.update', ['user' => $user->id]), 'method' => 'PUT')) !!}

        @csrf

        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                <div class="card card-primary">
                    <div class="card-body">
                        <div class="row">
                            <div class="form-group col-12 col-sm-12 col-md-6 col-lg-6 text-left">
                                @if( !empty($user->image_url) )
                                <div class="text-center" style="display: inline-block">
                                    <img src="{{ url($user->image_url) }}" style="width: 200px"><br/>
                                    <button type="submit" class="btn btn-danger btn-sm mt-2" name="del-photo" value="del-photo"><i class="fa fa-trash"></i> Elimina immagine</button>
                                </div>
                                @endif
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-12 col-sm-12 col-md-6 col-lg-6 @error('name') has-error @endif">
                                {!! Form::label('name', 'Nome') !!}
                                {!! Form::text('name', $user->name, ['class' => 'form-control']) !!}
                                @error('name')
                                    <div class="alert alert-danger mt-1">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group col-12 col-sm-12 col-md-6 col-lg-6 @error('email') has-error @endif">
                                {!! Form::label('email', 'Email') !!}
                                {!! Form::email('email', $user->email, ['class' => 'form-control']) !!}
                                @error('email')
                                    <div class="alert alert-danger mt-1">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group col-12 col-sm-12 col-md-6 col-lg-6">
                                {!! Form::label('phone', 'Telefono') !!}<br>
                                {{ $user->phone }}
                            </div>

                            <div class="form-group col-12 col-sm-12 col-md-6 col-lg-6">
                                {!! Form::label('address', 'Indirizzo') !!}<br>
                                {{ $user->delivery_route }}@if( !empty($user->delivery_route) && !empty($user->delivery_stree_number) ), @endif
                                {{ $user->delivery_stree_number }}<br>
                                {{ $user->delivery_postal_code }} @if( !empty($user->delivery_postal_code) && !empty($user->delivery_city) ) - @endif {{ $user->delivery_city }}<br>
                                {{ $user->delivery_region }}
                            </div>

                            <div class="form-group col-12 col-sm-12 col-md-6 col-lg-6">
                                {!! Form::label('birthdate', 'Data di nascita') !!}<br>
                                @if( !empty($user->birthdate) )
                                {{ Carbon\Carbon::parse($user->birthdate)->format('d/m/Y') }}
                                @endif
                            </div>

                            <div class="form-group col-12 col-sm-12 col-md-6 col-lg-6">
                                {!! Form::label('email', 'Data iscrizione') !!}<br>
                                {{ $user->created_at->format('d/m/Y') }}
                            </div>

                            <div class="form-group col-12 col-sm-12 col-md-6 col-lg-6">
                                {!! Form::label('trust_points', 'Punti fiducia') !!}<br>
                                {{ $user->trust_points }}
                            </div>

                            <div class="form-group col-12 col-sm-12 col-md-6 col-lg-6">
                                {!! Form::label('orders_count', 'Ordini effettuati') !!}<br>
                                {{ $user->orders->count() }}
                            </div>

                        </div>

                        <section style="background-color: lightblue; padding: 40px;">
                            <h3>Cambio password</h3>

                            <div class="form-group">
                                {!! Form::label('new_password', 'Nuova password') !!}
                                {!! Form::password('new_password', ['class' => 'form-control']) !!}
                                @error('new_password')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                {!! Form::label('new_password_confirmation', 'Conferma password') !!}
                                {!! Form::password('new_password_confirmation', ['class' => 'form-control']) !!}
                            </div>
                        </section>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-success btn-full" name="save" value="save">Salva</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
