<!-- Name Field -->
<div class="form-group ">
    {!! Form::label('name', __('common.name')) !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group ">
    {!! Form::label('email', __('common.email')) !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Phone Field -->
<div class="form-group ">
    {!! Form::label('phone', __('common.phone')) !!}
    {!! Form::text('phone', null, ['class' => 'form-control']) !!}
</div>

<!-- Other Phone Field -->
<div class="form-group ">
    {!! Form::label('other_phone', __('common.other_phone')) !!}
    {!! Form::text('other_phone', null, ['class' => 'form-control']) !!}
</div>

<!-- Delivery Address Field -->
<div class="form-group ">
    {!! Form::label('delivery_address', __('common.delivery_address')) !!}
    {!! Form::text('delivery_address', null, ['class' => 'form-control']) !!}
</div>

<!-- Image Path Field -->
<div class="form-group ">
    {!! Form::label('image_path', __('common.image')) !!}
    {!! Form::text('image_path', null, ['class' => 'form-control']) !!}
</div>

<!-- User Type Field -->
<div class="form-group ">
    {!! Form::label('user_type', __('common.user_type')) !!}
    {!! Form::select('user_type', $arr_user_type, null, ['class' => 'form-control']) !!}
</div>

<!-- User Type Status -->
<div class="form-group ">
    {!! Form::label('status', __('common.status')) !!}
    {!! Form::select('status', $arr_user_status, null, ['class' => 'form-control']) !!}
</div>

<!-- trust points Field -->
<div class="form-group col-sm-12">
    {!! Form::label('trust_points', __('common.trust_points')) !!}
    {!! Form::number('trust_points', '') !!}
</div>


<!-- Password Field -->
<div class="form-group ">
    {!! Form::label('password', __('common.password')) !!}
    {!! Form::password('password', ['class' => 'form-control']) !!}
</div>

<!-- Confirmation Password Field -->
<div class="form-group ">
      {!! Form::label('password', __('common.password_confirmation')) !!}
    {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('common.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('users.index') !!}" class="btn btn-default">{{ __('common.cancel') }}</a>
</div>
