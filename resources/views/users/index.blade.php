@extends('adminlte::page')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Utenti</h1>
        <div class="pull-right  float-right d-none d-sm-block" >
           <!--a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('users.create') !!}">Aggiungi nuovo</a-->
        </div>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="card card-primary">
            <div class="card-body">
                    @include('users.table')
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
@endsection

