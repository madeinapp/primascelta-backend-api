<div class="modal fade" id="modal-preview" tabindex="-1" role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-success">
                <h5 class="modal-title" id="modalLabel">Ritaglia l'immagine come preferisci</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body" style="padding: 0;" >
                <div class="img-container text-center" style="padding: 0; height: 400px;">
                    <img id="modal-image-preview" src="" style="height: 100%; margin: 0 auto;">
                    <div class="preview" style="position: absolute; top: -9999px"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Annulla</button>
                <button type="button" class="btn btn-primary" id="crop"><i class="fas fa-crop"></i> Ritaglia</button>
            </div>
        </div>
    </div>
</div>
