
<h3>Definisci i tuoi orari e giorni di chiusura settimanali</h3>
@include('users.shop_status')

@php
$arr_days = ['monday','tuesday','wednesday','thursday','friday','saturday','sunday'];
$arr_days_it = ['LUNEDI', 'MARTEDI', 'MERCOLEDI', 'GIOVEDI', 'VENERDI', 'SABATO', 'DOMENICA'];
@endphp

@error('alert_opening_days')
    <div class="alert alert-danger p-1 mt-0">{{ $message }}</div>
@enderror

@if( (!$user->shop->obj_opening_timetable->monday->active &&
     !$user->shop->obj_opening_timetable->tuesday->active &&
     !$user->shop->obj_opening_timetable->wednesday->active &&
     !$user->shop->obj_opening_timetable->thursday->active &&
     !$user->shop->obj_opening_timetable->friday->active &&
     !$user->shop->obj_opening_timetable->saturday->active &&
     !$user->shop->obj_opening_timetable->sunday->active) && !$errors->has('alert_opening_days') )
    <div class="alert alert-danger p-1 mt-0">Selezionare almeno un giorno di apertura</div>
@endif

@foreach($arr_days as $k => $day)

    @php
    $short_day = substr($day, 0, 3);
    @endphp

    <div class="row">
        <div class="col-12">
            <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                <input type="checkbox" class="custom-control-input control-open-day" id="control-open-{{ $day }}" name="open_{{ $day }}_active" data-day="{{ $day }}"
                @if( (empty(old('_token')) && $user->shop->obj_opening_timetable->{$day}->active) || old('open_'.$day.'_active') === 'on')
                checked
                @endif
                >
                {!! Form::label('control-open-'.$day, $arr_days_it[$k], ['class' => 'custom-control-label']) !!}
            </div>
        </div>
    </div>

    <div class="row row-day row-open-{{ $day }} bg-default rounded p-4 mb-4" style="display:
        @if( (empty(old('_token')) && $user->shop->obj_opening_timetable->{$day}->active) || old('open_'.$day.'_active') === 'on' )
        flex
        @else
        none
        @endif
        ">
        <div class="col-6 text-center px-2 ">
            {!! Form::label($short_day.'_mor_ope', 'Mattina') !!}
            <div class="row">
                <div class="col-12 text-center px-4">
                    <label for="open_{{ $short_day }}_mor" ><i class="fa fa-clock"></i> <span id="lbl_open_{{ $short_day }}_mor"></span></label>
                    <input type="text" id="open_{{ $short_day }}_mor" class="time-slider" readonly style="border:0; color:#f6931f; font-weight:bold;">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-6">
                    {!! Form::hidden('open_'.$short_day.'_mor_open', old('open_'.$short_day.'_mor_open', $user->shop->obj_opening_timetable->{$day}->first_half->opening_time ?? '00:00')) !!}
                </div>
                <div class="form-group col-6">
                    {!! Form::hidden('open_'.$short_day.'_mor_close', old('open_'.$short_day.'_mor_close', $user->shop->obj_opening_timetable->{$day}->first_half->closing_time ?? '18:00')) !!}
                </div>
            </div>
        </div>
        <div class="col-6 text-center  px-2 ">
            {!! Form::label($short_day.'_aft_ope', 'Pomeriggio') !!}
            <div class="row">
                <div class="col-12 text-center px-4">
                    <label for="open_{{ $short_day }}_aft"><i class="fa fa-clock"></i> <span id="lbl_open_{{ $short_day }}_aft"></span></label>
                    <input type="text" id="open_{{ $short_day }}_aft" class="time-slider" readonly style="border:0; color:#f6931f; font-weight:bold;">
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    {!! Form::hidden('open_'.$short_day.'_aft_open', old('open_'.$short_day.'_aft_open', $user->shop->obj_opening_timetable->{$day}->second_half->opening_time ?? '12:00')) !!}
                </div>
                <div class="col-6">
                    {!! Form::hidden('open_'.$short_day.'_aft_close', old('open_'.$short_day.'_aft_close', $user->shop->obj_opening_timetable->{$day}->second_half->closing_time ?? '24:00')) !!}
                </div>
            </div>
        </div>
    </div>
    @error('val_opening_timetable.'.$day)
        <div class="alert alert-danger p-1 mt-0">{{ $message }}</div>
    @enderror

    <hr>

@endforeach
