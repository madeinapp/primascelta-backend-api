@extends('adminlte::page')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Profilo</h1>
        <div class="pull-right  float-right d-none d-sm-block" >
           <!--a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('users.create') !!}">Aggiungi nuovo</a-->
        </div>
    </section>
    <div class="content">

        <div class="clearfix"></div>

        @include('flash::message')

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="clearfix"></div>

        {!! Form::open(array('url' => route('profile.update'), 'method' => 'post', 'files' => true)) !!}

        @csrf

        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                <div class="card card-primary">
                    <div class="card-body">
                        <div class="form-group">
                            {!! Form::label('name', 'Nome') !!}
                            {!! Form::text('name', $user->name, ['class' => 'form-control']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('email', 'Email') !!}
                            {!! Form::email('email', $user->email, ['class' => 'form-control']) !!}
                        </div>

                        <section style="background-color: lightblue; padding: 40px;">
                            <h3>Cambio password</h3>

                            <div class="form-group">
                                {!! Form::label('new_password', 'Nuova password') !!}
                                <input id="password" style="display:none" type="password" name="password">
                                {!! Form::password('new_password', ['class' => 'form-control', 'autocomplete' => 'new-password']) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label('new_password_confirmation', 'Conferma password') !!}
                                {!! Form::password('new_password_confirmation', ['class' => 'form-control']) !!}
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                <button type="submit" class="btn btn-success btn-full">Salva</button>
            </div>
        </div>
    </div>
@endsection
