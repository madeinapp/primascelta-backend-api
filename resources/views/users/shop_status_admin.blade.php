<div id="row-shop-status" class="row bg-success rounded p-4 mb-4">
    <div class="col-12">
        <h3><i class="fas fa-store-slash"></i> Chiusura negozio</h3>
        <p>Usa questo box per aggiornare le tue ferie o altre eventuali chiusure</p>

        <div class="row">
            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                <div class="form-group">
                    {!! Form::label('shop_status', 'Stato attività') !!}
                    {{ old('shop_status') }}
                    {!! Form::select('shop_status', [
                        'Aperto' => 'Aperto',
                        'Chiuso' => 'Chiuso',
                        'Chiuso per ferie' => 'Chiuso per ferie',
                        'Chiuso per lavori' => 'Chiuso per lavori'
                    ], 'Aperto', ['id' => 'shop_status', 'class' => 'form-control']) !!}
                </div>
            </div>

            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                <div id="grp-shop-status-orders" class="form-group">
                    {!! Form::label('shop_status_orders', 'Ricezione degli ordini') !!}
                    {!! Form::select('shop_status_orders', [
                        '1' => 'I tuoi clienti possono effettuare ordini',
                        '0' => 'I tuoi clienti non possono effettuare ordini',
                    ], old('shop_status_orders', null), ['id' => 'shop_status_orders', 'class' => 'form-control']) !!}
                </div>
            </div>
        </div>

        <div id="row-closing-dates" class="row" style="display: none">
            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                <div class="form-group">
                    <label for="closed_date_start">Chiuso dal giorno:</label>
                    <input type="date" id="closed_date_start" name="closed_date_start" class="form-control" value="">
                </div>
                @error('closed_date_start')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                <div class="form-group">
                    <label for="closed_date_start">al giorno:</label>
                    <input type="date" id="closed_date_end" name="closed_date_end" class="form-control" value="">
                </div>
                @error('closed_date_end')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
        </div>

        @if( Auth::user()->shop &&
            Auth::user()->shop->shop_status !== 'Aperto' &&
            Auth::user()->shop->closed_date_end->isPast()
            )

            <div class="alert alert-danger">ATTENZIONE: Il tuo negozio risulta ancora chiuso ma la data di fine chiusura è ormai trascorsa</div>

        @endif
    </div>
</div>
