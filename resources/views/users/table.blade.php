<div class="table-responsive">
    <table class="table" id="users-table">
        <thead>
        <tr>
            <th>Azioni</th>
            <th>Nome e cognome</th>
            <th>Email</th>
            <th>Città</th>
            <th>Stato</th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            <tr>
                <td>
                    {!! Form::open(['route' => ['users.destroy', $user->id], 'method' => 'delete']) !!}

                        <a href="{!! route('users.edit', [$user->id]) !!}" class='btn btn-default btn-sm'><i class="fas fa-edit"></i></a>

                        @if ($user->status == "active" || $user->status == "pending")
                        <a href="user/disable/{{ $user->id }}" class='btn btn-danger btn-sm'><i class="fas fa-user-times"></i></a>
                        @endif

                        @if ($user->status == "inactive" || $user->status == "pending")
                        <a href="user/enable/{{ $user->id }}" class='btn btn-success btn-sm'><i class="fas fa-user-check green"></i></a>
                        @endif

                    {!! Form::close() !!}
                </td>
                <td>{!! $user->name !!}</td>
                <td>{!! $user->email !!}</td>
                <td>{!! $user->delivery_city !!}</td>
                <td>
                    @if( $user->status === 'active' )
                    <span class="badge badge-success"><i class="fas fa-check"></i> Attivo</span>
                    @elseif( $user->status === 'pending' )
                    <span class="badge badge-warning"><i class="fa fa-clock"></i> In attesa</span>
                    @elseif( $user->status === 'inactive' )
                    <span class="badge badge-danger"><i class="fas fa-times-circle"></i> Disabilitato</span>
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
@section('js')
<script>
$(function () {
    $('#users-table').DataTable({
        "language": {
            url: "{{ url('js/Italian.json') }}"
        },
        "paging":   true,
        "initComplete": function (settings, json) {
            $("#users-table").wrap("<div style='overflow:auto; width:100%;position:relative;'></div>");
        },
        pageLength: 10,
        stateSave: true,
        columnDefs: [
            { orderable: false, targets: -1 }
        ]
    });
});
</script>
@endsection
