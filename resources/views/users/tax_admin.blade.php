
<div class="row">
    <div class="col-12 col-sm-12 col-md-6 col-lg-6 mb-3">
        <div class="form-group @if($errors->has('tax_business_name')) has-error @endif">
            {!! Form::label('tax_business_name', 'Ragione sociale ( o Nome e Cognome se Persona fisica)') !!}
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1"><i class="fas fa-money-check"></i></span>
                </div>
                {!! Form::text('tax_business_name', old('tax_business_name', null), ['class' => 'form-control', 'placeholder' => 'Tipologia azienda']) !!}
            </div>
        </div>

        <div class="form-group @if($errors->has('tax_type_id')) has-error @endif">
            {!! Form::label('tax_type_id', 'Tipologia aziendale') !!}
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1"><i class="fas fa-money-check"></i></span>
                </div>
                {!! Form::select('tax_type_id', $arr_tax_types, old('tax_type_id', null), ['class' => 'form-control', 'placeholder' => 'Seleziona una tipologia']) !!}
            </div>
        </div>

        <div class="form-group @if($errors->has('tax_vat')) has-error @endif">
            {!! Form::label('tax_vat', 'Partita IVA') !!}
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1"><i class="fas fa-money-check"></i></span>
                </div>
                {!! Form::text('tax_vat', old('tax_vat', null), ['class' => 'form-control', 'maxlength' => 11 ]) !!}
            </div>
            @error('tax_vat')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>

        <div class="form-group @if($errors->has('tax_fiscal_code')) has-error @endif">
            {!! Form::label('tax_fiscal_code', 'Codice fiscale') !!}
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1"><i class="fas fa-money-check"></i></span>
                </div>
                {!! Form::text('tax_fiscal_code', old('tax_fiscal_code',null), ['class' => 'form-control', 'maxlength' => 16]) !!}
            </div>
            @error('tax_fiscal_code')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            <!-- button type="button" id="btn-vat-copy" class="btn btn-sm btn-block btn-secondary mt-3 mx-0"><i class="far fa-copy"></i> Copia Partita Iva</button -->
        </div>
        <fieldset style="border: 1px solid orange; border-radius: 10px; margin: 0; padding: 10px">
            <div>
                <div class="form-group @if($errors->has('tax_code')) has-error @endif">
                    {!! Form::label('tax_code', 'Codice univoco a 7 cifre') !!}
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1"><i class="fas fa-money-check"></i></span>
                        </div>
                        {!! Form::text('tax_code', old('tax_code', null), ['class' => 'form-control', 'maxlength' => 7]) !!}
                    </div>
                    @error('tax_code')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group @if($errors->has('tax_pec')) has-error @endif">
                    {!! Form::label('tax_pec', 'PEC') !!}
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1"><i class="far fa-envelope"></i></span>
                        </div>
                        {!! Form::email('tax_pec', old('tax_pec', null), ['class' => 'form-control']) !!}
                    </div>
                    @error('tax_pec')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                    <div class="alert alert-danger p-1 mt-0">Compilare almeno uno dei 2 campi</div>
            </div>
        </fieldset>
    </div>
    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
        <fieldset style="border: 1px solid #cecece; border-radius: 10px; padding: 10px">

            <h3 style="text-align: center">Profilo fiscale</h3>

            <div class="form-group">

                {!! Form::hidden('tax_address', null) !!}


                <div id="tax_address_components">
                    {!! Form::label('tax_address_route', 'Indirizzo') !!}
                    {!! Form::text('tax_address_route', old('tax_address_route', null), ['id' => 'route', 'class' => 'form-control route']) !!}

                    {!! Form::label('tax_address_street_number', 'Numero civico') !!}
                    {!! Form::text('tax_address_street_number', old('tax_address_street_number', null), ['id' => 'street_number', 'class' => 'form-control street_number']) !!}

                    {!! Form::label('tax_address_postal_code', 'CAP') !!}
                    {!! Form::text('tax_address_postal_code', old('tax_address_postal_code', null), ['id' => 'postal_code', 'class' => 'form-control postal_code', 'maxlength' => 5]) !!}

                    {!! Form::label('tax_address_country', 'Nazione') !!}
                    {!! Form::text('tax_address_country', old('tax_address_country', null), ['id' => 'country', 'class' => 'form-control country']) !!}

                    {!! Form::label('tax_address_region', 'Regione') !!}
                    {!! Form::text('tax_address_region', old('tax_address_region', null), ['id' => 'administrative_area_level_1','class' => 'form-control administrative_area_level_1']) !!}

                    {!! Form::label('tax_address_prov', 'Provincia') !!}
                    {!! Form::text('tax_address_prov', old('tax_address_prov', null), ['id' => 'administrative_area_level_2', 'class' => 'form-control prov', 'maxlength' => 2]) !!}

                    {!! Form::label('tax_address_city', 'Città') !!}
                    {!! Form::text('tax_address_city', old('tax_address_city', null), ['id' => 'locality', 'class' => 'form-control locality']) !!}

                    {!! Form::hidden('tax_address_lat', null, ['id' => 'lat', 'class' => 'form-control lat']) !!}
                    {!! Form::hidden('tax_address_long', null, ['id' => 'long', 'class' => 'form-control long']) !!}

                    <button type="button" id="btn-address-copy" class="btn btn-sm btn-block btn-secondary mt-3 mx-0"><i class="far fa-copy"></i> Copia indirizzo sede attività</button>
                </div>
            </div>
        </fieldset>
    </div>
</div>
