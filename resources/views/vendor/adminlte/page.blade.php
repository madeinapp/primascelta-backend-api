@extends('adminlte::master')

@php
$route = Request::route()->getName();
$arrRoute = explode(".", $route);
$class_routes = "";
$appo = "";
foreach($arrRoute as $k => $route):
    if( !empty($appo) ):
        $appo .= "-";
    endif;

    $appoRoute = explode("-", $route);
    $classRoute = "";
    foreach ($appoRoute as $key => $value) {
        preg_match_all('/((?:^|[A-Z])[a-z]+)/',$value,$res);
        $res = array_map("unserialize", array_unique(array_map("serialize", $res)));
        $classRoute = strtolower(implode("-", $res[0]));
    }

    $appo .= $classRoute;

    if( !empty($class_routes) ):
        $class_routes .= " ";
    endif;
    $class_routes .= $appo;
endforeach;
@endphp

@inject('layoutHelper', \JeroenNoten\LaravelAdminLte\Helpers\LayoutHelper)

@if($layoutHelper->isLayoutTopnavEnabled())
    @php( $def_container_class = 'container' )
@else
    @php( $def_container_class = 'container-fluid' )
@endif

@section('adminlte_css')
    @stack('css')
    @yield('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/css/tempusdominus-bootstrap-4.min.css" />
@stop

<!-- Privacy Modal -->
<div class="modal fade" id="staticBackdropPrivacy" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel"><strong>BENVENUTO UTENTE AZIENDA</strong></h5>
      </div>
      <div class="modal-body">
<p>       Questa utenza gratuita è stata creata dalla nostra redazione di Primascelta.</p>
<p>		Il sotoscritto, titolare dell'Azienda Esercente, prendo visione ed accetto i seguenti:: </p>
		<p align="center"><a href="https://www.primascelta.biz/termini-e-condizioni" target="_blank" onClick="window.open('https://www.primascelta.biz/termini-e-condizioni','termini_e_condizioni','resizable,height=600,width=800'); return false;">TERMINI E CONDIZIONI DEL SERVIZIO</a></p>

<p align="center">		<a href="https://www.primascelta.biz/privacy-policy" target="_blank" onClick="window.open('https://www.primascelta.biz/privacy-policy','privacy','resizable,height=600,width=800'); return false;">PRIVACY POLICY</a> </p>
      </div>
      <div class="modal-footer">
        <!--button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button-->
        <button type="button" class="btn btn-primary" onCLick="PrivacyOK({{ Auth::user()->id }})">Accetto</button>
      </div>
    </div>
  </div>
</div>

@section('classes_body', $layoutHelper->makeBodyClasses() . ' ' . $class_routes)

@section('body_data', $layoutHelper->makeBodyData())

@section('body')

    <div class="wrapper">

        {{-- Top Navbar --}}
        @if($layoutHelper->isLayoutTopnavEnabled())
            @include('adminlte::partials.navbar.navbar-layout-topnav')
        @else
            @include('adminlte::partials.navbar.navbar')
        @endif

        {{-- Left Main Sidebar --}}
        @if(!$layoutHelper->isLayoutTopnavEnabled())
            @include('adminlte::partials.sidebar.left-sidebar')
        @endif

        {{-- Content Wrapper --}}
        <div class="content-wrapper {{ config('adminlte.classes_content_wrapper') ?? '' }}">

            {{-- Content Header --}}
            <div class="content-header">
                <div class="{{ config('adminlte.classes_content_header') ?: $def_container_class }}">
                    @yield('content_header')
                </div>
            </div>

            {{-- Main Content --}}
            <div class="content">
                <div class="{{ config('adminlte.classes_content') ?: $def_container_class }}">
                    @yield('content')
                </div>
            </div>

        </div>

        {{-- Footer --}}
        @hasSection('footer')
            @include('adminlte::partials.footer.footer')
        @endif

        {{-- Right Control Sidebar --}}
        @if(config('adminlte.right_sidebar'))
            @include('adminlte::partials.sidebar.right-sidebar')
        @endif

    </div>

    @include('modals.modalConfirm')
    @include('modals.modalInfo')

@stop

@section('adminlte_js')
    <script src="{{ asset('vendor/adminlte/dist/js/adminlte.min.js') }}"></script>
    <script>
        /* Enable bootstrap tooltip everywhere*/
        $(function () {

            apriAssistenza();

            $('[data-toggle="tooltip"]').tooltip()

            setRequiredBackground();
            setTimeout(function(){
                setRequiredBackground();
            }, 500);

            $("input[type=text], input[type=number], input[type=email], textarea, select").each(function(k, val){
                $(this).on('change', function(){
                    setRequiredBackground();
                });
            });

            if (! mobileAndTabletCheck() && getParameterByName('video')){
                window.open('https://www.primascelta.biz/impara_primascelta/','share-dialog','width=640,height=360')
            }

        })
        function getParameterByName(name, url = window.location.href) {
            name = name.replace(/[\[\]]/g, '\\$&');
            var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, ' '));
        }
        function setRequiredBackground(el){
            $("input[type=text], input[type=number], input[type=email], textarea, select").each(function(k, val){

                let el = $(this);

                if( el.prop('required') ){
                    el.css('border-color', 'orange');
                }

                if( el.prop('required') && el.val() === "" ){
                    el.css('background-color', 'yellow');
                }else{
                    el.css('background-color', 'white');
                }
            });
        }

        function showConfirm(title, bgHeader, message, callback, callback_params){
            $("#modalConfirm .modal-title").text(title);
            $("#modalConfirm .modal-header").addClass(bgHeader);
            $("#modalConfirm .modal-body").text(message);
            $("#modalConfirm .btn-save").on('click', () => { hideConfirm(); callback(...callback_params); });
            $("#modalConfirm").modal('show');

            $("#modalConfirm").on('hidden.bs.modal', function (e) {
                $("#modalConfirm .btn-save").off('click');
            });
        }

        function hideConfirm() {
            $("#modalConfirm .btn-save").off('click');
            $("#modalConfirm").modal('hide');
        }

        function showInfo(title, bgHeader, message){
            $("#modalInfo .modal-title").text(title);
            $("#modalInfo .modal-header").addClass(bgHeader);
            $("#modalInfo .modal-body").text(message);
            $("#modalInfo").modal('show');
        }

        window.mobileAndTabletCheck = function() {
            let check = false;
            (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
                return check;
        };

        function apriAssistenza(){
            if (! mobileAndTabletCheck()){
                $("#conoscere-link").attr("href", "https://www.primascelta.biz/fatti-conoscere/");
                $("#tutorial-link").attr("href", "https://tutorialprimascelta.vivivideo.it");
                $("#assistenza-link").attr("href", "https://www.primascelta.biz/assistenza/");
            }else{
                $("#conoscere-link").attr("href", "?conoscere=true");
                $("#tutorial-link").attr("href", "?tutorial=true");
                $("#assistenza-link").attr("href", "?assistenza=true");
            }
        }

    </script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/js/tempusdominus-bootstrap-4.min.js"></script>
    <script src="{{ mix('js/all.js') }}"></script>
	@if(!Auth::user()->privacy && Auth::user()->shop->admin_sent_email_shop_created && Auth::user()->role != 'admin')
	<script>
		$('#staticBackdropPrivacy').modal('show');	
		function PrivacyOK(user_id) {
			$.ajax({
				url:"/privacyok",
				data:{'user_id':user_id},
				type: 'POST',
				success:function() {
					$('#staticBackdropPrivacy').modal('hide');	
				}
			});
		}
	</script>
	@endif
    @stack('js')
    @yield('js')
@stop
