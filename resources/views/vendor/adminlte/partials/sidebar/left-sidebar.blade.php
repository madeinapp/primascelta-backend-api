<aside class="main-sidebar {{ config('adminlte.classes_sidebar', 'sidebar-dark-primary elevation-4') }}" style="background-color:#13bf9e !important">

    {{-- Sidebar brand logo --}}
    @if(config('adminlte.logo_img_xl'))
        @include('adminlte::partials.common.brand-logo-xl')
    @else
        @include('adminlte::partials.common.brand-logo-xs')
    @endif

    {{-- Sidebar menu --}}
    <div class="sidebar">
        <nav class="mt-2">
            @if( Auth::user()->user_type === 'company' )
			@php
			$subscription_type = Auth::user()->subscription->subscription;
			$subscription_free_extended = (($subscription_type != 'trial' && !empty(Auth::user()->shop->agent_id)) ? true : false);
			@endphp
            {{--
            @php
            $catalog = Auth::user()->shop->shopCatalogs->count();
            $subscription_type = Auth::user()->subscription->subscription;
            $diff = null;
			$subscription_free_extended = (($subscription_type != 'trial' && !empty(Auth::user()->shop->agent_id)) ? true : false);

            switch( $subscription_type ){
                case 'standard'  : $diff = 100 - $catalog; break;
                case 'premium'   : $diff = 200 - $catalog; break;
                case 'enterprise': $diff = 400 - $catalog; break;
                case 'trial'     : $diff = 1000 - $catalog; break;
				case 'free'	     : !empty(Auth::user()->shop->agent_id) ? $diff = 100 - $catalog : $diff = Auth::user()->subscription->product_num - $catalog; break;
                default: return 0;
            }
            @endphp
            --}}
            <ul class="nav nav-pills nav-sidebar flex-column" style="margin-bottom: 10px; border-radius: 5px; border: 1px solid #fff; background: #fff">
                @if( Auth::user()->subscription->expire_date->isFuture() || ($subscription_type == 'free' && !$subscription_free_extended))
                <li class="nav-item active"><a class="nav-link " href="/home"><p>Abbonamento<span class="badge badge-success right">{{ ucfirst(Auth::user()->subscription->subscription) }}</span></p></a></li>
                @else
                <li class="nav-item active"><a class="nav-link " href="/home"><p>Abbonamento<span class="badge badge-danger right">Scaduto</span></p></a></li>
                @endcan
                {{--
                <li class="nav-item active"><a class="nav-link " href="javascript:void(0)"><p>Scade il<span class="badge badge-success right">{{ Auth::user()->subscription->expire_date->format('d/m/Y') }}</span></p></a></li>
                <li class="nav-item active"><a class="nav-link " href="javascript:void(0)"><p>Prodotti restanti<span class="badge badge-success right">{{ $diff }}</span></p></a></li>
                 --}}
            </ul>
            @endif
            <ul class="nav nav-pills nav-sidebar flex-column {{ config('adminlte.classes_sidebar_nav', '') }}"
                data-widget="treeview" role="menu"
                @if(config('adminlte.sidebar_nav_animation_speed') != 300)
                    data-animation-speed="{{ config('adminlte.sidebar_nav_animation_speed') }}"
                @endif
                @if(!config('adminlte.sidebar_nav_accordion'))
                    data-accordion="false"
                @endif>
                {{-- Configured sidebar links --}}
                @each('adminlte::partials.sidebar.menu-item', $adminlte->menu('sidebar'), 'item')
            </ul>
            <hr size="2" color="#FFF">
            <ul class="nav nav-pills nav-sidebar flex-column" >
                @if( Auth::user()->shop )
                <li class="nav-item ">
                    <a class="nav-link" href="?share={{Auth::user()->shop->id}}">
                        <i class="fas fa-share-alt "></i>
                        <p>Invia i tuoi clienti </p>
                    </a>
                </li>
                @endif
                @if(Auth::user()->shop)
                <div id="share-buttons">
                    <!-- Facebook -->
                    <a href="#" onclick="window.open('http://www.facebook.com/sharer.php?u=https://www.primascelta.biz/social-share/?idshop={{Auth::user()->shop->id}}','share-dialog','width=626,height=436')" target="_blank">
                        <img src="https://simplesharebuttons.com/images/somacro/facebook.png" alt="Facebook" />
                    </a>
                    <!-- Twitter -->
                    <a href="#" onclick="window.open('https://twitter.com/share?url=https://www.primascelta.biz/social-share/?idshop={{Auth::user()->shop->id}}&amp;text=Primascelta.biz&amp;hashtags=primascelta','share-dialog','width=626,height=436')" target="_blank">
                        <img src="https://simplesharebuttons.com/images/somacro/twitter.png" alt="Twitter" />
                    </a>
                     <!-- LinkedIn -->
                     <a href="#" onclick="window.open('http://www.linkedin.com/shareArticle?mini=true&amp;url=https://www.primascelta.biz/social-share/?idshop={{Auth::user()->shop->id}}','share-dialog','width=626,height=436')" target="_blank">
                        <img src="https://simplesharebuttons.com/images/somacro/linkedin.png" alt="LinkedIn" />
                    </a>
                </div>
                @endif
                <li class="nav-item ">
                    <a id="conoscere-link" class="nav-link" style="cursor: pointer" href="">
                        <i class="fas fa-bullhorn "></i>
                        <p>Fatti conoscere</p>
                    </a>
                </li>
                <li class="nav-item ">
                    <a id="tutorial-link" class="nav-link" style="cursor: pointer" href="">
                        <i class="fas fa-laptop "></i>
                        <p>Tutorial</p>
                    </a>
                </li>
                <li class="nav-item ">
                    <a id="assistenza-link" class="nav-link" style="cursor: pointer" href="">
                        <i class="fas fa-hands-helping "></i>
                        <p>Assistenza</p>
                    </a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link"  href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <i class="fas fa-power-off"></i>
                        <p>Logout</p>
                    </a>
                </li>
            </ul>
        </nav>
        <div style="color:#FFF;position:fixed;bottom:0;padding-left:10px;font-size:12px"> <img src="/images/logo-text-white.png" width="120" alt="Primascelta">&nbsp;&nbsp;&nbsp; ver. 1.2<br>un'esclusiva di Terracciano srl</div>
    </div>

<style>
@media (min-width:1281px) {

    #share-buttons {
        margin-left: 140px;
        /*margin-top: -42px;
        margin-bottom: 10px;*/
        display:block !important;
        z-index:9999;
    }
}
#share-buttons{
    display:none;
}

#share-buttons img {
    width: 25px;
    padding: 1px;
    border: 0;
    box-shadow: 0;
    display: inline;
}

.nav-item .fas{
    width: 28px;
}
</style>
</aside>
