<tr>
<td class="header">
<a href="{{ $url }}" style="display: inline-block;">
@if (trim($slot) === 'Laravel')
<img src="https://backend.primascelta.biz/images/logo-primascelta.png" class="logo" alt="Primascelta">
@else
{{ $slot }}
@endif
</a>
</td>
</tr>
