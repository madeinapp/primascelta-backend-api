@component('mail::layout')
{{-- Header --}}
@slot('header')
@component('mail::header', ['url' => config('app.url')])
<img src="https://backend.primascelta.biz/images/logo-primascelta-email.png"  alt="Primascelta">
@endcomponent
@endslot

{{-- Body --}}
{{ $slot }}

{{-- Subcopy --}}
@isset($subcopy)
@slot('subcopy')
@component('mail::subcopy')
{{ $subcopy }}
@endcomponent
@endslot
@endisset

<br>
<br>
<br>

<p style="text-align: right">
    <i>
        Un saluto dal nostro Team<br>
        {{ config('app.name') }}
    </i>
</p>

<br>

<p style="text-align: center; font-size: 10px">
Questo messaggio è generato automaticamente, La preghiamo di non rispondere a questa email.
</p>

{{-- Footer --}}
@slot('footer')
@component('mail::footer')

<p>
    Seguici per rimanere sempre aggiornato sulle novità di Primascelta
    <br><br>
    <a href="https://www.facebook.com/primascelta.biz" style="padding: 0 5px;"><img src="https://backend.primascelta.biz/images/facebook.png?p={{ rand(1000,9999) }}"></a>
    <a href="https://www.instagram.com/primascelta.biz/" style="padding: 0 5px;"><img src="https://backend.primascelta.biz/images/instagram.png?p={{ rand(1000,9999) }}"></a>
    <a href="https://www.youtube.com/channel/UCtGzz88u0FBUxypBJntzWtg" style="padding: 0 5px;"><img src="https://backend.primascelta.biz/images/youtube.png?p={{ rand(1000,9999) }}"></a>
</p>

Ricevi questa mail perché sei iscritto su Primascelta.biz
<br>
<span style="font-size: 10px">Primascelta è un esclusiva di Terracciano srl</span>
@endcomponent
@endslot
@endcomponent
