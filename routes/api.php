<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Models\Order;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Version 1 api/v1
Route::prefix('v1')->group(function() {

    Route::middleware('verifyCsrfToken')->group(function() {

        Route::get('test', function(){
            $order = Order::find(5);
            dd(app('\App\Http\Controllers\API\OrderAPIController')->setCancellableWithin($order));
        } );

        //DEVELOP api/v1/develop
        Route::prefix('develop')->group(function() {
            //Change user Status
            Route::middleware('auth:api')->post('/user/change_status', 'UserAPIController@change_status');
        });

        Route::prefix('shop')->group(function() {
            //Tax Type list
            Route::get('/taxtype-list', 'ShopAPIController@getTaxTypeList')->withoutMiddleware('auth:api');
            //Shop Type list
            Route::get('/type-list', 'ShopAPIController@getShopTypeList')->withoutMiddleware('auth:api');
            Route::get('/type-list-distinct', 'ShopAPIController@getShopTypeListDistinct')->withoutMiddleware('auth:api');
            //Shop Payment methods
            Route::get('/payment-method-list', 'ShopAPIController@getShopPaymentMethodList')->withoutMiddleware('auth:api');
        });

        //AUTH api/v1/auth
        Route::prefix('auth')->group(function() {

            //Bulk registration from Website (with shop info included)
            Route::post('/bulk_register', 'UserShopAPIController@store');
            //Check user email
            Route::post('/check-user-email', 'UserAPIController@checkUserEmail');
            //Send OTP to user
            Route::post('/send-otp-to-user', 'UserAPIController@sendUserOtp');
            //User Private registration
            Route::post('/register', 'UserAPIController@register');
            //User Company registration (with Shop)
            Route::post('/register-company', 'UserAPIController@registerCompany');
            /*XXX REMOVE
            //User registration step 2
            Route::post('/register2', 'UserAPIController@store');
            */
            //Send OTP
            Route::post('/send-otp', 'UserAPIController@sendOtp');
            //Validate OTP
            Route::post('/validate-otp', 'UserAPIController@validateOtp');
            //Login
            Route::post('/login', 'UserAPIController@login');
            //Logout
            Route::middleware('auth:api', 'check.expired')->post('/logout', 'UserAPIController@logout');
            //User
            Route::middleware('auth:api', 'check.expired')->get('/user', function (Request $request) {
                return $request->user();
            });
            //Password recover request
            Route::post('/password-recover', 'UserAPIController@passwordRecover');
            //Password change
            Route::post('/password-change', 'UserAPIController@passwordChange');

            Route::post('/user/update_phone', 'UserAPIController@updatePhone');
            Route::post('/shop/update_whatsapp', 'ShopAPIController@updateWhatsapp');

        });

        //PROFILE api/v1/user
        Route::middleware('auth:api', 'check.expired')->prefix('user')->group(function() {
            //Give Trust Points
            Route::post('/{id}/give_points', 'UserAPIController@givePoints');
            //Remove Trust Points
            Route::post('/{id}/remove_points', 'UserAPIController@removePoints');
            //send message
            Route::post('/send_message', 'UserAPIController@sendMessage');
            //Profile list
            Route::get('/list', 'UserAPIController@index');
            //Profile list
            Route::get('/{id?}', 'UserAPIController@show');
            //Profile Update
            Route::post('/store', 'UserAPIController@store');
        });


        //Shops api/v1/shop with auth
        Route::middleware('auth:api', 'check.expired')->prefix('shop')->group(function() {

            //Shop store
            Route::post('/store', 'UserAPIController@storeCompany');
        //Shop search
            Route::post('/search', 'ShopAPIController@search');
            //Shop detail
            Route::get('/{id}', 'ShopAPIController@show');
            //Shop list
            Route::get('/', 'ShopAPIController@index');
            //Shop Update
            //Route::post('/store', 'ShopAPIController@store');
            //Shop list nearby
            //Route::get('/nearby/{long}/{lat}', 'ShopAPIController@nearby');
            //Shop recently searched
            //Route::get('/recently-searched', 'ShopAPIController@recentlySearched');
            //Shop filter
            //Route::get('/filter/{distance}', 'ShopAPIController@filter');
            //Shop Most purchased XXX
            //Route::get('/most-purchased', 'ShopAPIController@mostPurchased');
        });

        //Shop catalogs api/v1/shop-catalog
        Route::middleware('auth:api', 'check.expired')->prefix('shop-catalog')->group(function() {
            //Catalog list
            Route::get('/list', 'ShopCatalogAPIController@index');
            Route::get('/list/{id}', 'ShopCatalogAPIController@index');
            Route::get('search', 'ShopCatalogAPIController@search');

            //Types List
            Route::get('/type-list/{food_category_id?}', 'ShopCatalogAPIController@getTypesList');
            //Category list
            Route::get('/category-list', 'ShopCatalogAPIController@getFoodCategoryList');

            //Catalog get
            Route::get('/{id}', 'ShopCatalogAPIController@show');
            //Catalog store
            Route::post('/store', 'ShopCatalogAPIController@store');
            //Catalog update
            Route::post('/update/{id}', 'ShopCatalogAPIController@update');
            //Catalog set visible_in_search
            Route::post('/set-visible-in-search', 'ShopCatalogAPIController@setVisibleInSearch');
            //Catalog delete
            Route::delete('/delete/{id}', 'ShopCatalogAPIController@destroy');

        });

        //Offers api/v1/shop-offer
        Route::middleware('auth:api', 'check.expired')->prefix('shop-offer')->group(function() {
            //Offer list
            Route::get('/list/{id?}', 'ShopOfferAPIController@index');
            //Offer get
            Route::get('/{id}', 'ShopOfferAPIController@show');
            //Offer get-food
            Route::get('/get-food/{id}', 'ShopOfferAPIController@showFood');
            //Offer store
            Route::post('/store', 'ShopOfferAPIController@store');
            //Offer set visible_in_search
            Route::post('/set-status', 'ShopOfferAPIController@setStatus');
            //Offer delete
            Route::delete('/delete/{id}', 'ShopOfferAPIController@destroy');
        });

        //Foods api/v1/food
        Route::middleware('auth:api', 'check.expired')->prefix('food')->group(function() {
            //Food unit of measure list
            Route::get('/unit-of-measure-list', 'FoodAPIController@getFoodUnitOfMeasureList');
            //Food category list
            Route::get('/category-list', 'FoodAPIController@getFoodCategoryList');

            Route::get('/type-list/{food_category_id?}', 'FoodAPIController@getFoodTypeList');

            Route::get('/specials', 'FoodAPIController@specials');

            //Food list
            Route::get('/search', 'FoodAPIController@search');
            //Food get
            Route::get('/{id}', 'FoodAPIController@show');

        });


        //ORDERS api/v1/orders
        Route::middleware('auth:api', 'check.expired')->prefix('order')->group(function() {
            //Order Store
            Route::post('/store', 'OrderAPIController@store');
            //Svuota carrello
            Route::post('/empty_cart', 'OrderAPIController@emptyCart');

            //Order list
            Route::get('/show', 'OrderAPIController@show');

            Route::get('/not_confirmed', 'OrderAPIController@notConfirmed');

            Route::get('/confirmed', 'OrderAPIController@confirmed');

            Route::get('/search', 'OrderAPIController@search');

            Route::get('/pdf/{order}', 'OrderAPIController@pdf');

            Route::get('/cancel_copy/{order}', 'OrderAPIController@cancelCopy');
        });


        Route::middleware('auth:api', 'check.expired')->prefix('favourite_shops')->group(function() {
            //Get List of User Favourite Shops
            Route::get('/index', 'FavouriteShopController@index');

            //Store User Favourite Shop
            Route::post('/store', 'FavouriteShopController@store');

            //Remove User Favourite Shop
            Route::delete('/destroy', 'FavouriteShopController@destroy');
        });

        Route::middleware('auth:api', 'check.expired')->prefix('favourite_shop_catalogs')->group(function() {
            //Get List of User Favourite Shops
            Route::get('/index', 'FavouriteShopCatalogController@index');

            //Store User Favourite Shop
            Route::post('/store', 'FavouriteShopCatalogController@store');

            //Remove User Favourite Shop
            Route::delete('/destroy', 'FavouriteShopCatalogController@destroy');
        });


        Route::middleware('auth:api', 'check.expired')->prefix('notifications')->group(function() {

            Route::put('/read/all',                 'NotificationAPIController@readAll');
            Route::put('/read/{notification_id}',   'NotificationAPIController@read');
            Route::put('/unread/{notification_id}', 'NotificationAPIController@unread');
            Route::delete('/all',                   'NotificationAPIController@destroyAll');
            Route::delete('/{notification_id}',     'NotificationAPIController@destroy');

        });

    });
});
