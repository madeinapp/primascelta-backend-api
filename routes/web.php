<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/password/{pwd}', 'UserController@genPwd');
//Route::get('/user/cambia_password/{email}', 'UserController@cambiaPassword');
//Route::get('test_emails', 'UserController@testEmails');

Route::get('/', 'HomeController@index')->middleware('auth');

Auth::routes(['verify' => true]);

Route::get('/artigiano/{command}', 'ArtigianoController@showSingle')->name('ArtigianoSingle');
Route::get('/artigiano/{command}/{param}', 'ArtigianoController@show')->name('Artigiano');

Route::post('/password-change', 'UserController@passwordChange');

Route::group(['prefix' => 'share', 'as' => 'share.'], function() {
    Route::get('/shop/{shop_id}', 'ShareController@shop');
    Route::get('/shop_catalog/{shop_catalog_id}', 'ShareController@shopCatalog');
    Route::get('/shop_offer/{shop_offer_id}', 'ShareController@shopOffer');
    Route::get('/shop_offer_food/{shop_offer_food_id}', 'ShareController@shopOfferFood');
});

/*
Route::get('/phpinfo', function(){ phpinfo(); });
*/

Route::get('/storage/invoices/orders/{order}/{filename}', 'OrderController@downloadPDF');
Route::get('/storage/invoices/orders/{order}', 'OrderController@pdfHtml');

Route::group(['middleware' => ['auth', 'check.status']], function () {

    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('/il-mio-negozio', 'HomeController@ilMioNegozio')->name('ilMioNegozio');
    Route::get('/prices', 'SubscriptionController@prices')->name('prices');

    Route::get('profile/edit',              ['as' => 'profile.edit',           'uses' => 'UserController@editProfile']);
    Route::post('profile/update',           ['as' => 'profile.update',         'uses' => 'UserController@updateProfile']);

    Route::group(['prefix' => 'foodTypes', 'as' => 'foodTypes.'], function() {
        Route::get('/', ['as' => 'index', 'uses' => 'FoodTypeController@index']);
        Route::put('/update', ['as' => 'update', 'uses' => 'FoodTypeController@update']);
    });

    Route::get('/food/search', 'API\FoodAPIController@search');

    Route::get('/food/{food_category_id}/types', 'FoodController@getFoodTypeList');

    Route::get('/related_uoms/{unit_of_measure}', 'API\ShopCatalogAPIController@getRelatedUoms');

    Route::post('/orders/{order}/sendMessage', ['as' => 'orders.send_message', 'uses' => 'OrderController@sendMessage']);

    Route::group(['prefix' => 'notifications', 'as' => 'notifications.'], function() {
        Route::get('',                          ['as' => 'index',       'uses' => 'NotificationController@index']);
        Route::get('/read/{notification_id}',   ['as' => 'unread',      'uses' => 'NotificationController@read']);
        Route::put('/read/all',                 ['as' => 'unread',      'uses' => 'API\NotificationAPIController@readAll']);
        Route::put('/read/{notification_id}',   ['as' => 'read',        'uses' => 'API\NotificationAPIController@read']);
        Route::put('/unread/{notification_id}', ['as' => 'unread',      'uses' => 'API\NotificationAPIController@unread']);
        Route::delete('/all',                   ['as' => 'destroyAll',  'uses' => 'API\NotificationAPIController@destroyAll']);
        Route::delete('/{notification_id}',     ['as' => 'destroy',     'uses' => 'API\NotificationAPIController@destroy']);
    });

    Route::group(['prefix' => 'blacklist', 'as' => 'blacklist.'], function() {
        Route::get('/',                              ['as' => 'index',  'uses' => 'BlacklistController@index']);
        Route::post('/remove/{shop_id}/{user_id}',   ['as' => 'remove', 'uses' => 'BlacklistController@remove']);
    });

    Route::group(['middleware' => ['auth', 'check.admin']], function () {

        Route::resource('foodCategories', 'FoodCategoryController');

        Route::resource('foods', 'FoodController');
        Route::get('/foodCategories/{food_category_id}/check/images', 'FoodController@checkImages');
        Route::get('/foodCategories/{food_category_id}/move', 'FoodCategoryController@selectForMove');
        Route::post('/foodCategories/{food_category_id}/moveFoods', 'FoodCategoryController@moveFoods');
        Route::post('/foodCategories/{food_category_id}/moveShopCatalogs', 'FoodCategoryController@moveShopCatalogs');

        Route::resource('specials', 'SpecialController');

        Route::resource('shopTypes', 'ShopTypeController');

        Route::resource('agents', 'AgentController');

        Route::resource('paymentMethods', 'PaymentMethodController');

        Route::resource('deliveryMethods', 'DeliveryMethodController');

        Route::get('unitOfMeasures/getList', 'API\UnitOfMeasuresAPIController@index');
        Route::post('unitOfMeasures/replace', 'API\UnitOfMeasuresAPIController@replace');
        Route::resource('unitOfMeasures', 'UnitOfMeasureController');

        Route::resource('shops', 'ShopController');
		Route::post('shop/store_admin', ['as' => 'shops.store_admin', 'uses' => 'ShopController@store_admin']);
        Route::get('shop/enable/{id}', 'ShopController@enable');
        Route::get('shop/disable/{id}', 'ShopController@disable');
    	Route::get('/shops/sendAdminRegistrationEmail/{id}', 'ShopController@sendAdminRegistrationEmail');

        Route::get('shop_subscription/{shop_id}', ['as' => 'shopSubscription.edit', 'uses' => 'ShopController@editSubscription']);
        Route::post('shop_subscription/update', ['as' => 'shopSubscription.update', 'uses' => 'ShopController@updateSubscription']);

        Route::resource('users', 'UserController');
        Route::get('user/enable/{id}', 'UserController@enable');
        Route::get('user/disable/{id}', 'UserController@disable');

        Route::group(['prefix' => 'subscriptions', 'as' => 'subscriptions.'], function() {
            Route::get('/',                                ['as' => 'index',        'uses' => 'SubscriptionController@index']);
            Route::get('/{subscription}',                  ['as' => 'edit',         'uses' => 'SubscriptionController@edit']);
            Route::put('/update',                          ['as' => 'update',       'uses' => 'SubscriptionController@update']);
            Route::post('/feature/save',                   ['as' => 'feature.save', 'uses' => 'SubscriptionController@featureStore']);
            Route::put('/feature/{feature_id}',            ['as' => 'feature.update', 'uses' => 'SubscriptionController@featureUpdate']);
            Route::get('/feature/{feature_id}',            ['as' => 'feature', 'uses' => 'SubscriptionController@getFeature']);
            Route::delete('/feature/{feature_id}',  ['as' => 'feature.delete', 'uses' => 'SubscriptionController@deleteFeature']);
        });
    });


    Route::group(['middleware' => ['auth', 'check.company']], function () {

        Route::post('profile/update_company',   ['as' => 'profile.update_company', 'uses' => 'UserController@updateProfileCompany']);

        Route::get('shop/{shop_id}/food_category/{food_category_id}/types', 'ShopController@getCategoryTypes');

        Route::get('/shop/invisible', 'ShopController@invisible');
        Route::get('/shop/visible', 'ShopController@visible');

        Route::get('/publish/request', 'ShopController@publishRequest');

        //Route::resource('shopCatalogs', 'ShopCatalogController');
        Route::group(['prefix' => 'shopCatalogs', 'as' => 'shopCatalogs.'], function() {
            Route::post('',                                 ['as' => 'store',   'uses' => 'ShopCatalogController@store'])->middleware('can:add_to_catalog');
            Route::get('',                                  ['as' => 'index',   'uses' => 'ShopCatalogController@index']);
            Route::get('create',                            ['as' => 'create',  'uses' => 'ShopCatalogController@create'])->middleware('can:add_to_catalog');
            Route::get('search',                            ['as' => 'search',  'uses' => 'API\ShopCatalogAPIController@search']);
            Route::get('/type-list/{food_category_id?}',    ['as' => 'types',   'uses' => 'API\ShopCatalogAPIController@getTypesList']);
            Route::patch('{shopCatalog}',                   ['as' => 'update',  'uses' => 'ShopCatalogController@update']);
            Route::delete('{shopCatalog}',                  ['as' => 'destroy', 'uses' => 'ShopCatalogController@destroy']);
            Route::get('{shopCatalog}/active',              ['as' => 'active',    'uses' => 'ShopCatalogController@setActive']);
            Route::get('{shopCatalog}',                     ['as' => 'show',    'uses' => 'ShopCatalogController@show']);
            Route::get('{shopCatalog}/edit',                ['as' => 'edit',    'uses' => 'ShopCatalogController@edit']);

            Route::group(['prefix' => 'multi', 'as' => 'multi.'], function() {

                Route::delete('delete', ['as' => 'delete',   'uses' => 'ShopCatalogController@multiDestroy']);
                Route::post('activate', ['as' => 'activate', 'uses' => 'ShopCatalogController@multiActivate']);
                Route::post('disable',  ['as' => 'active',   'uses' => 'ShopCatalogController@multiDisable']);

            });
        });

        //Route::resource('shopOffers', 'ShopOfferController');
        Route::group(['prefix' => 'shopOffers', 'as' => 'shopOffers.'], function() {
            Route::post('',                   ['as' => 'store',    'uses' => 'ShopOfferController@store']);
            Route::get('status/{shopOffer}',  ['as' => 'status',   'uses' => 'ShopOfferController@status']);
            Route::get('',                    ['as' => 'index',    'uses' => 'ShopOfferController@index']);
            Route::get('create',              ['as' => 'create',   'uses' => 'ShopOfferController@create']);
            Route::patch('{shopOffer}',       ['as' => 'update',   'uses' => 'ShopOfferController@update']);
            Route::delete('{shopOffer}',      ['as' => 'destroy',  'uses' => 'ShopOfferController@destroy']);
            Route::get('{shopOffer}',         ['as' => 'show',     'uses' => 'ShopOfferController@show']);
            Route::get('{shopOffer}/edit',    ['as' => 'edit',     'uses' => 'ShopOfferController@edit']);
        });

        Route::group(['prefix' => 'shopOfferFoods', 'as' => 'shopOfferFoods.'], function() {
            Route::delete('{shopCatalog}',    ['as' => 'destroy',    'uses' => 'API\ShopOfferFoodAPIController@destroy']);
        });

        Route::group(['prefix' => 'orders', 'as' => 'orders.'], function() {
            Route::get('/', ['as' => 'index', 'uses' => 'OrderController@index']);
            Route::get('{order}/edit', ['as' => 'edit', 'uses' => 'OrderController@edit']);
            Route::get('/{order}/pdf', ['as' => 'pdf', 'uses' => 'OrderController@pdf']);
            Route::post('store', ['as' => 'store', 'uses' => 'API\OrderAPIController@store']);
            Route::delete('{order}', ['as' => 'destroy', 'uses' => 'OrderController@destroy']);
        });
    });

    Route::get('payments', ['as' => 'payments', 'uses' => 'InvoiceController@index'])->middleware('can:is_admin');

    Route::group(['prefix' => 'paypal', 'as' => 'paypal.'], function() {
        Route::get('test',      ['as' => 'test', 'uses' => 'PaypalController@test']);
        Route::get('resume',   ['as' => 'resume', 'uses' => 'PaypalController@resume']);
        Route::post('payment',  ['as' => 'payment', 'uses' => 'PaypalController@payment']);
        Route::get('notify',    ['as' => 'notify', 'uses' => 'PaypalController@notify']);
    });


    Route::post('/send_message', 'API\UserAPIController@sendMessage');

    Route::post('/privacyok', 'UserController@privacyok');

});
